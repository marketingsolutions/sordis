<?php

namespace frontend\listeners;

use common\utils\Checker;
use modules\profiles\common\models\Profile;
use modules\rpc\common\models\RpcAccount;
use ms\loyalty\api\common\models\Log;
use ms\loyalty\prizes\payments\common\models\Payment;
use yii\base\Event;
use yii\base\Exception;
use yii\web\Response;

class CodesControllerListener
{
    public static function beforeCreate(Event $event)
    {
        try {
            if (\Yii::$app->request->pathInfo == 'codes/api-v3/codes/activate') {
                $profile_id = \Yii::$app->request->post('profile_id');
                /** @var Log $log */
                $log = \Yii::$app->params['log'];

                if ($profile = Profile::findOne($profile_id)) {
                    $code = \Yii::$app->request->post('code');

                    /** @var RpcAccount $acc */
                    if (!empty($code) && ($acc = RpcAccount::findOne(['code' => $code]))) {
                        $acc->profile_id = $profile_id;
                        $acc->try_code = $code;
                        $acc->try_at = (new \DateTime('now'))->format('Y-m-d H:i:s');
                        $acc->try_shutdown = false;
                        $acc->save(false);

                        $log->soap_request = "rpc_shutdown";
                        $log->save(false);
                        $output = 'Выключение удаленки';

                        if (true) {
                            $shell = shell_exec("net rpc shutdown -S 5.189.167.162 -U'ZAKAZPODARKA\a.zinoviev%ooo000)O' -t 1 -f");
                            # $shell = 'TEST-MODE';
                            $output .= ': ' . $shell;
                            $log->updateAttributes(['soap_response' => $output]);
                            $acc->try_result = $output;
                            $acc->try_shutdown = true;
                            $acc->save(false);
                        }

                        $info = "[rpc_shutdown] {$output} | user:{$acc->user} | code:{$acc->code} | at:{$acc->try_at}";
                        \Yii::error($info);

                        $return = ['result' => 'FAIL', 'errors' => ['code' => $output]];
                        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                        \Yii::$app->response->content = json_encode($return, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
                        \Yii::$app->response->statusCode = 400;
                        \Yii::$app->response->send();
                    }
                }
            }
        }
        catch (\Exception $e) {
            # throw $e;
        }
    }
}