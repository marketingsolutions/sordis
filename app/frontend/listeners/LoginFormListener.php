<?php

namespace frontend\listeners;
use modules\profiles\common\models\Profile;
use ms\loyalty\identity\phonesEmails\frontend\forms\LoginForm;
use yii\base\Event;


/**
 * Class LoginFormListener
 */
class LoginFormListener
{
    public static function whenAfterValidate(Event $event)
    {
        /** @var LoginForm $sender */
        $sender = $event->sender;
        $identity = $sender->getIdentity();

        if ($identity === null) {
            return null;
        }
        /** @var Profile $profile */
        $profile = $identity->getProfile();

        if ($profile !== null && $profile->blocked_at != null) {
            $sender->addError('email', 'Данный участник заблокирован');
            $sender->addError('phone_mobile_local', 'Данный участник заблокирован');
        }
    }
}