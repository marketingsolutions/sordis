<?php

namespace frontend\listeners;

use common\utils\Checker;
use ms\loyalty\prizes\payments\common\models\Payment;
use yii\base\Event;
use yii\base\Exception;
use yii\web\Response;

class PaymentsControllerListener
{
	public static function beforeCreate(Event $event)
	{
		try {
			if (\Yii::$app->request->pathInfo != 'payments/api/payments/create') {
				return;
			}

			$postData = \Yii::$app->request->post();

			if (empty($postData['amount'])) {
				return;
			}

			$amount = $postData['amount'];
			$companyBalance = Checker::companyBalance();

			if ($amount > $companyBalance) {
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				\Yii::$app->response->content = '[{"field":"amount","message":"Призовой фонд исчерпан, конвертация баллов невозможна."}]';
				\Yii::$app->response->statusCode = 422;
				\Yii::$app->response->send();
			}
		}
		catch (\Exception $e) {

		}
	}

	public static function beforeInsert(Event $event)
	{
		/** @var Payment $payment */
		$payment = $event->sender;

		$amount = $payment->amount;
		$companyBalance = Checker::companyBalance();

		if ($amount > $companyBalance) {
			throw new Exception('Платеж не может быть проведен из-за недостатка средств на счете клиента');
		}
	}
}