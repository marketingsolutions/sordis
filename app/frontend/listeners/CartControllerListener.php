<?php

namespace frontend\listeners;

use common\utils\Checker;
use yii\base\Event;
use yii\web\Response;

class CartControllerListener
{
    public static function cartAdd(Event $event)
    {
		try {
			/** @var \ms\loyalty\catalog\frontend\forms\AddToCartForm $sender */
			$sender = $event->sender;

			$qty = intval($sender->quantity);
			$nominal = intval($sender->nominal);
			$sum = $qty * $nominal;

			$companyBalance = Checker::companyBalance();

			if ($sum > $companyBalance) {
				$sender->addError('nominal', 'Призовой фонд исчерпан, конвертация баллов невозможна.');
			}
		}
		catch (\Exception $e) {

		}
    }
}