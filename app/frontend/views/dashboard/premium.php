<?php
use yii\grid\GridView;


$this->title = 'Мои премии';
$this->params['header'] = 'Мои премии';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row container">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'label' => '№ заказа',
                        ],
                        [
                            'attribute' => 'created_at',
                            'label' => 'Дата списания баллов',
                            'format' => ['date', 'php:d.m.Y H:i'],
                        ],
                        [
                            'attribute' => 'bonus_sum',
                            'label' => 'Сумма потраченных баллов',
                        ],
                        [
                            'attribute' => 'premium_sum',
                            'label' => 'Сумма премии',
                        ],
                        [
                            'attribute' => 'remainder_sum',
                            'label' => 'Остаток',
                        ],
                        [
                            'format' => 'raw',
                            'label' => 'детализация',
                            'value' => function($model){
                                return "<a target='_blank' href='/dashboard/premium-history?order_id=".$model->id."'>подробнее</a>";
                            }
                        ],
                    ]
                ]); ?>
            </div>
        </div>
    </div>
</div>
