<?php
use yii\grid\GridView;




$this->title = 'Детализация';
$this->params['header'] = 'Детализация';

$this->params['breadcrumbs'][] = [
        'template' => "<li><b>{link}</b></li>\n", //  шаблон для этой ссылки
        'label' => 'Мои премии', // название ссылки
        'url' => ['/dashboard/premium'] // сама ссылка;
    ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row container">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'document_id',
                            'label' => 'Номер документа',
                        ],
                        [
                            'attribute' => 'premium_sum',
                            'label' => 'Сумма потраченных баллов',
                        ],
                        [
                            'attribute' => 'document_number',
                            'label' => 'Дополнительное соглашение (№, дата)',
                        ],
                        [
                            'attribute' => 'document_act',
                            'label' => 'Акт (номер, дата) ',
                        ],
                    ]
                ]); ?>
            </div>
        </div>
    </div>
</div>

