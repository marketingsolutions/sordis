<?php

use ms\loyalty\catalog\frontend\widgets\CatalogDashboard;
use ms\loyalty\ozon\frontend\widgets\OzonDashboard;
use ms\loyalty\prizes\payments\frontend\widgets\PaymentsDashboard;
use modules\sales\frontend\widgets\SalesDashboard;
use modules\profiles\common\models\Profile;
use modules\sales\common\models\Distributor;
use modules\sales\common\models\SalePoint;
use modules\profiles\frontend\widgets\DiscountPremiumDistrib;
/**
 * @var \yii\web\View $this
 * @var \modules\profiles\common\models\Profile $profile
 * @var \modules\profiles\common\models\Dealer $dealer
 * @var \modules\profiles\common\models\Action $action
 */

$this->params['header'] = 'Тратить баллы';
$this->params['breadcrumbs'][] = 'Тратить баллы';
?>

<div class="container">
<!--	<h3>Перевод на электронные кошельки и мобильные телефоны</h3>-->
</div>
<?= PaymentsDashboard::widget() ?>

<?php if ($profile->role == Profile::ROLE_DISTRIBUTOR): ?>
    <div class="container">
    <!--Скидки/премии только у роли дистрибьютор-->
    </div>
    <?= DiscountPremiumDistrib::widget()?>
<?php endif;?>


<div class="container">
<!--	<h3>Электронные Подарочные Сертификаты</h3>-->
</div>
<?= CatalogDashboard::widget() ?>
