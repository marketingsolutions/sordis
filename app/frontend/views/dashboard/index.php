<?php

use ms\loyalty\catalog\frontend\widgets\CatalogDashboard;
use ms\loyalty\prizes\payments\frontend\widgets\PaymentsDashboard;
use modules\sales\frontend\widgets\SalesDashboard;
use modules\profiles\common\models\Profile;
use modules\sales\common\models\Distributor;
use modules\sales\common\models\SalePoint;
use modules\profiles\frontend\widgets\AddRtt;
use ms\files\attachments\common\utils\FileUtil;
use modules\bonuses\common\models\TempBonuses;
use modules\bonuses\common\models\TempConfirmedBonuses;
use modules\profiles\backend\models\ProfileTransactionSearch;

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\common\models\Profile $profile
 * @var \modules\profiles\common\models\Dealer $dealer
 * @var \modules\profiles\common\models\Action $action
 */

$this->params['header'] = 'Личный кабинет';
$this->params['breadcrumbs'][] = 'Личный кабинет';

$this->registerCssFile('/css/owl.carousel.css');
$this->registerCssFile('/css/owl.theme.default.css');
$js = <<<JS
$( document ).ready(function() {
    /*Подсазки для кнопок добавления и удаления РТТ*/
    $('#btn-add-rtt').tooltip();
    $('#btn-dell-rtt').tooltip();
    
    /*Проверка соответствия ЕГАИС и ИНН, а так же проверка на занятость точки*/
    $('body').on('input', 'input', function(e) {
        var str1='-code_egais';
        var str2='-inn';
        var inputId = $(this).attr('id').replace(str1, '').replace(str2, '');
        var egais = $('#'+inputId+str1).val();
        var inn = $('#'+inputId+str2).val();
         // console.log(egais);
         // console.log(inn);
         // console.log("!!!!!");
        if(typeof egais != "undefined" && typeof inn != "undefined" && egais.replace('_', '').length == 12 && inn.replace('_', '').length ==10){
             $.ajax(
                    {
                      type: 'POST',
                      url: '/site/egais-inn-address',
                      data: {"egais": egais, "inn": inn},
                      success: function(data) {
                                console.log(data);
                                if(data){
                                    $('#'+inputId+'-name').val('');
                                    $('#'+inputId+'-name').val(data.name);
                                    $('#'+inputId+'-address').val('');    
                                    $('#'+inputId+'-address').val(data.address);
                                    $("#reg-errors li").remove();
                                }                                       
                    },
                    error:   function(xhr) {
                        e.preventDefault(); 
                        var data = xhr.responseJSON;
                        if ("errors" in data) {
                            $('#result_bonus').text('0');
                            $('#'+inputId+'-name').val('');        
                            $('#'+inputId+'-address').val('');           
                            $("#reg-errors li").remove();
                            for (var i = 0; i < data.errors.length; i++) {
                                $("#reg-errors").append("<li>" + data.errors[i] + "</li>");
                            }
                        }
                    }
                });
        }
     });
    
    /*Запрещаем отправку формы, если*/
    $('body').on('click', '.go', function(e){
        var isError = $('#reg-errors > li').text();
        if(isError){
            e.preventDefault();
        }
    });
});

JS;
$this->registerJs($js);
?>
<?php
echo \modules\profiles\frontend\widgets\ProfileBonusesSum::widget();
?>
<div class="row">
  <div class="col-md-5 col-sm-6 ">
    <div class="bonuses--user-name"><?php echo $profile->last_name . " " . $profile->first_name . " " . $profile->middle_name ?></div>
    <div class="bonuses--user-role"><?= Profile::getDashboardRealRole()[$profile->role] ?></div>
      <?php if ($profile->role == Profile::ROLE_MANAGER_TT): ?>
        <div class="bonuses--modal-btn">
        <span id="btn-add-rtt"
              title="Если в процессе регистрации Вы указали  не все свои торговые точки, то Вы можете заполнить заявку на их добавление. После рассмотрения данной заявки модератором портала и успешного подтверждения, Вы увидите их в списке своих торговых точек."
              data-toggle="modal" data-target=".bd-example-modal-lg" class="add_my_rtt">Добавить ТТ</span>
        </div>
      <?php endif; ?>
  </div>
</div>
</div><!--закрывает div из profile-bonuses-sum.php-->

<div class="container">
  <div class="row">
    <div class="col-md-8">
        <?php if ($profile->role == Profile::ROLE_DISTRIBUTOR): ?>
          <h2>Дистрибьютор</h2>
            <?php foreach (Distributor::getDistribData($profile->id) as $item): ?>
            <div class="sales-point">
              <div class="point-coin distr_akb"><?= SalePoint::getAkbs($item['code_egais']) ? SalePoint::getAkbs($item['code_egais']) : 0; ?></div>
              <div class="point-block">
                <div class="point-block--title"><?= $item['name'] ?></div>
                <div class="point-block--info">
                  <span><?= $item['address'] ?></span>
                  <span>Код ЕГАИС: <?= $item['code_egais'] ?>, ИНН: <?= $item['code_inn'] ?></span>
                  <span>КПП: <?= $item['code_kpp'] ?>, Номер лицензии: <?= $item['license'] ?></span>
                </div>
                  <?php $distribRtt = Distributor::getDistribRtt($item['distrib_id']); ?>
                  <?php if ($distribRtt): ?>
                    <a href="/sales/sales/my-rtt?id=<?= $item['distrib_id'] ?>">Показать все РТТ этого дистрибьютора</a>
                  <?php endif; ?>
              </div>
            </div>
            <?php endforeach; ?>

        <?php elseif ($profile->role == Profile::ROLE_MANAGER_TT): ?>
          <h2>Торговые точки</h2>
            <?php foreach (SalePoint::getRttData($profile->id) as $item): ?>
            <div class="sales-point">
            <span  data-toggle="modal" data-idsalepoint="<?= $item['ids'] ?>," id="btn-dell-rtt"
                   title="Если при регистрации Вы ввели данные не по своей торговой точке, либо торговая точка закрылась/отсоединилась, то Вы можете заполнить заявку на их удаление."
                   data-target=".bd-example-modal-lg2" class="close-point">×</span>
              <div class="point-coin">
                  <?= SalePoint::getPhotoCoin($item['egais']) ? "<img src='".SalePoint::getPhotoCoin($item['egais'])->photoUrl."'>" : "<img src='/images/0.jpg'/>"; ?>
              </div>
              <div class="point-block">
                <div class="point-block--title"><?= $item['sale_point_name'] ?></div>
                <div class="point-block--time">Часы работы РТТ <?= $item['time_from'] ?> - <?= $item['time_to'] ?></div>
                <div class="point-block--info">
                  <span><?= $item['sale_point_adderss'] ?></span>
                  <span>Код ЕГАИС: <?= $item['egais'] ?>, ИНН: <?= $item['sale_point_inn'] ?></span>
                  <span>КПП: <?= $item['sale_point_kpp'] ?>, Номер лицензии: <?= $item['license'] ?></span>
                </div>
              </div>
            </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>

    <div class="col-md-4 col-news">
      <h2>Последние новости</h2>
        <?php if (!empty($sliderNews)): ?>
          <div class="news-carousel owl-carousel owl-theme">
              <?php foreach ($sliderNews as $news): ?>
                <div class="news-card">
                  <!--        <img src="/images/news-img.jpg" class="news-card--image">-->
                  <span class="news-card--title">
              <?= $news['title'] ?>
            </span>
                  <span class="news-card--announce">
              <?php $new = strip_tags($news['content']) ?>
              <?= mb_strimwidth($new, 0, 255) ?> ....
            </span>
                  <a href="/news/news/index?id=<?= $news['id'] ?>" class="news-card--button">Подробнее</a>
                </div>
              <?php endforeach; ?>
          </div>
        <?php else: ?>
          <span class="news-card--announce">Список новостей пуст...</span>
        <?php endif; ?>
    </div>
  </div>
</div>


<!--Детализация по баллам РТТ-->
<?php if ($profile->role == Profile::ROLE_MANAGER_TT): ?>
  <div class="container">
    <div class="row">

      <!--Начисленные баллы-->
      <div class="col-md-12 mb-75 block-bb2" id="award-points">
        <h2>Начисленные баллы</h2>
        <div class="points-block">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading-1">
              <div class="panel-title points-block--name">
                <a class="collapsed points-link" role="button" data-toggle="collapse" href="#collapse-1"
                   aria-expanded="false" aria-controls="collapseOne">
                    <?php echo $profile->last_name . " " . $profile->first_name . " " . $profile->middle_name ?>
                </a>
                <small class="pink">Участник-владелец ТТ</small>
              </div>
              <div class="points-block--total">
                <small>Всего начислено баллов</small>
                <span class="pink"><?= TempBonuses::calculateBonusSum($bonusType = null, $period = null, $profile->phone_mobile, $codeEGAIS = null) ?></span>
              </div>
            </div>
            <div id="collapse-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-1">

              <div class="panel-body">
                <div class="points-table">
                    <?php foreach (TempBonuses::getCurrentPeriod() as $period): ?>
                      <div class="points-table--header">
                        <div class="points-table--td1">Начисленные баллы за месяц, в том числе</div>
                        <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                          г.
                        </div>
                        <div class="points-table--td3"><?= TempBonuses::calculateBonusSum($bonusType = null, $period, $profile->phone_mobile, $codeEGAIS = null) ?></div>
                      </div>
                      <div class="points-table--body">
                        <div class="points-table--line">
                          <div class="points-table--td1">основные баллы</div>
                          <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                            г.
                          </div>
                          <div class="points-table--td3"><?= TempBonuses::calculateBonusSum(TempBonuses::BASIC_BONUS, $period, $profile->phone_mobile, $codeEGAIS = null) ?></div>
                        </div>
                        <div class="points-table--line">
                          <div class="points-table--td1">целевые баллы</div>
                          <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                            г.
                          </div>
                          <div class="points-table--td3"><?= TempBonuses::calculateBonusSum(TempBonuses::TARGET_BONUS, $period, $profile->phone_mobile, $codeEGAIS = null) ?></div>
                        </div>
                        <div class="points-table--line">
                          <div class="points-table--td1">призовые баллы</div>
                          <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                            г.
                          </div>
                          <div class="points-table--td3"><?= TempBonuses::calculateBonusSum(TempBonuses::SOCIAL_BONUS, $period, $profile->phone_mobile, $codeEGAIS = null) ?></div>
                        </div>
                      </div>
                    <?php endforeach; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--Начисленные баллы =КОНЕЦ=-->
      <!--Детализация по начисленным баллам-->
      <div class="col-md-12 mb-75 block-bb2">
        <h2>Детализация начисленных баллов</h2>
          <?php $i = 1; ?>
          <?php foreach (SalePoint::getRttData($profile->id) as $item): ?>
              <?php $i++; ?>
            <div class="points-block">
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading-2">
                  <div class="panel-title points-block--name">
                    <a class="collapsed points-link pink" role="button" data-toggle="collapse"
                       href="#collapse-<?= $i ?>"
                       aria-expanded="false" aria-controls="collapseTwo">
                        <?= $item['sale_point_name'] ?>
                    </a>
                    <small class="black"><?= $item['sale_point_adderss'] ?> <br/>Код ЕГАИС:
                        <?= $item['egais'] ?>
                    </small>
                  </div>
                  <div class="points-block--total">
                    <small>Всего начислено баллов по ТТ</small>
                    <span class="pink"><?= TempBonuses::calculateBonusSum($bonusType = null, $period = null, $profile->phone_mobile, $item['egais']) ?></span>
                  </div>
                </div>
                <div id="collapse-<?= $i ?>" class="panel-collapse collapse" role="tabpanel"
                     aria-labelledby="heading-2">

                  <div class="panel-body">
                    <div class="points-table">
                        <?php foreach (TempBonuses::getCurrentPeriod() as $period): ?>
                          <div class="points-table--header">
                            <div class="points-table--td1">Начисленные баллы за месяц, в том числе</div>
                            <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                              г.
                            </div>
                            <div class="points-table--td3"><?= TempBonuses::calculateBonusSum($bonusType = null, $period, $profile->phone_mobile, $item['egais']) ?></div>
                          </div>
                          <div class="points-table--body">
                            <div class="points-table--line">
                              <div class="points-table--td1">основные баллы</div>
                              <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                                г.
                              </div>
                              <div class="points-table--td3"><?= TempBonuses::calculateBonusSum(TempBonuses::BASIC_BONUS, $period, $profile->phone_mobile, $item['egais']) ?></div>
                            </div>
                            <div class="points-table--line">
                              <div class="points-table--td1">целевые баллы</div>
                              <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                                г.
                              </div>
                              <div class="points-table--td3"><?= TempBonuses::calculateBonusSum(TempBonuses::TARGET_BONUS, $period, $profile->phone_mobile, $item['egais']) ?></div>
                            </div>
                            <div class="points-table--line">
                              <div class="points-table--td1">призовые баллы</div>
                              <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                                г.
                              </div>
                              <div class="points-table--td3"><?= TempBonuses::calculateBonusSum(TempBonuses::SOCIAL_BONUS, $period, $profile->phone_mobile, $item['egais']) ?></div>
                            </div>
                          </div>
                        <?php endforeach; ?>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>

      </div>
      <!--Детализация по начисленным баллам =Конец=-->
      <!--Подтвержденные баллы итоговые-->
      <div class="col-md-12 mb-75 block-bb1" id="confirm-points">
        <h2>Подтверждённые баллы</h2>

        <div class="points-block pbv2">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading-4">
              <div class="panel-title points-block--name">
                <a class="collapsed points-link" role="button" data-toggle="collapse" href="#collapse2-4"
                   aria-expanded="false" aria-controls="collapseTwo">
                    <?php echo $profile->last_name . " " . $profile->first_name . " " . $profile->middle_name ?>
                </a>
                <small class="pink">Участник-владелец ТТ</small>
              </div>
              <div class="points-block--right">
                <div class="points-block--total">
                  <small>Всего подтверждено баллов</small>
                  <span class="pink"><?= TempConfirmedBonuses::calculateBonusSum($bonusType = null, $period = null, $profile->phone_mobile, $codeEGAIS = null) ?></span>
                </div>
                <div class="points-block--total">
                  <small>Всего потрачено баллов</small>
                  <span class="pink"><?=TempConfirmedBonuses::calculateSpendBonusesSum($period=null, $profile->purse->id)?></span>
                </div>
                <div class="points-block--total">
                  <small>Остаток баллов</small>
                  <span class="pink"><?= $profile->purse->balance ?></span>
                    <?php
                    //Для вычисления остатка баллов ТТ
                    //$itogOstatokTT = $profile->purse->balance;
                    $p = TempConfirmedBonuses::getCurrentPeriod();
                    $itogOstatokTT = TempConfirmedBonuses::getAllBonusSum($p, $profile->id)-TempConfirmedBonuses::calculateSpendBonusesSum($period=null, $profile->purse->id);
                    ?>
                </div>
              </div>
            </div>

            <div id="collapse2-4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-4">
              <div class="panel-body">
                <!--По месяцам-->
                <div class="points-table">
                    <?php foreach (TempConfirmedBonuses::getCurrentPeriod() as $period): ?>

                        <?php $itogOstatokTT = $itogOstatokTT -
                            TempConfirmedBonuses::calculateBonusSum(TempConfirmedBonuses::BASIC_BONUS, $period, $profile->phone_mobile, $codeEGAIS = null)-
                            TempConfirmedBonuses::calculateBonusSum(TempConfirmedBonuses::TARGET_BONUS, $period, $profile->phone_mobile, $codeEGAIS = null)-
                            TempConfirmedBonuses::calculateBonusSum(TempConfirmedBonuses::SOCIAL_BONUS, $period, $profile->phone_mobile, $codeEGAIS = null)+
                            TempConfirmedBonuses::calculateSpendBonusesSum($period, $profile->purse->id)?>
                      <div class="points-table--header">
                        <div class="points-table--td1">Подтверждённые баллы за месяц, в том числе</div>
                        <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                          г.
                        </div>
                        <div class="points-table--td3"><?= TempConfirmedBonuses::calculateBonusSum($bonusType = null, $period, $profile->phone_mobile, $codeEGAIS = null) ?></div>
                      </div>
                      <div class="points-table--body">
                         <div class="points-table--line">
                            <div class="points-table--td1">остаток на начало периода</div>
                            <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                                  г.
                            </div>
                           <div class="points-table--td3"><?= $itogOstatokTT ?></div>
                          </div>
                        <div class="points-table--line">
                          <div class="points-table--td1">основные баллы</div>
                          <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                            г.
                          </div>
                          <div class="points-table--td3"><?= TempConfirmedBonuses::calculateBonusSum(TempConfirmedBonuses::BASIC_BONUS, $period, $profile->phone_mobile, $codeEGAIS = null) ?></div>
                        </div>
                        <div class="points-table--line">
                          <div class="points-table--td1">целевые баллы</div>
                          <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                            г.
                          </div>
                          <div class="points-table--td3"><?= TempConfirmedBonuses::calculateBonusSum(TempConfirmedBonuses::TARGET_BONUS, $period, $profile->phone_mobile, $codeEGAIS = null) ?></div>
                        </div>
                        <div class="points-table--line">
                          <div class="points-table--td1">призовые баллы</div>
                          <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                            г.
                          </div>
                          <div class="points-table--td3"><?= TempConfirmedBonuses::calculateBonusSum(TempConfirmedBonuses::SOCIAL_BONUS, $period, $profile->phone_mobile, $codeEGAIS = null) ?></div>
                        </div>
                          <div class="points-table--line">
                              <div class="points-table--td1">потрачено баллов за период</div>
                              <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                                  г.
                              </div>
                              <div class="points-table--td3"><?= TempConfirmedBonuses::calculateSpendBonusesSum($period, $profile->purse->id) ?></div>
                          </div>
                      </div>
                    <?php endforeach; ?>
                  <!--По месяцам =КОНЕЦ=-->
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="col-md-12 block-bb1">
        <h2>Подтверждённые баллы по ТТ</h2>

        <!---Подтвержденные по ТТ-->
        <!--Разбираем все РТТ-->
          <?php $j = 0; ?>
          <?php foreach (SalePoint::getRttData($profile->id) as $item): ?>
              <?php $j++; ?>
            <div class="points-block pbv2">
              <div class="panel panel-default">


                <div class="panel-heading" role="tab" id="heading-6">
                  <div class="panel-title points-block--name">
                    <a class="collapsed points-link pink" role="button" data-toggle="collapse"
                       href="#collapse3-<?= $j ?>"
                       aria-expanded="false" aria-controls="collapseTwo">
                        <?= $item['sale_point_name'] ?>
                    </a>
                    <small class="black"><?= $item['sale_point_adderss'] ?>
                      <br/>Код ЕГАИС: <?= $item['egais'] ?></small>
                  </div>
                  <div class="points-block--right">
                    <div class="points-block--total">
                      <small>Всего подтверждено баллов</small>
                      <span class="pink"><?= TempConfirmedBonuses::calculateBonusSum($bonusType = null, $period = null, $profile->phone_mobile, $item['egais']) ?></span>
                    </div>
                    <div class="points-block--total">
                      <small></small>
                      <span class="pink"></span>
                    </div>
                    <div class="points-block--total">
                      <small></small>
                      <span class="pink"></span>
                    </div>
                  </div>
                </div>

                <div id="collapse3-<?= $j ?>" class="panel-collapse collapse" role="tabpanel"
                     aria-labelledby="heading-6">
                  <div class="panel-body">
                    <div class="points-table">
                        <?php foreach (TempConfirmedBonuses::getCurrentPeriod() as $period): ?>
                          <div class="points-table--header">
                            <div class="points-table--td1">Подтвержденные баллы за месяц, в том числе</div>
                            <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                              г.
                            </div>
                            <div class="points-table--td3"><?= TempConfirmedBonuses::calculateBonusSum($bonusType = null, $period, $profile->phone_mobile, $item['egais']) ?></div>
                          </div>
                          <div class="points-table--body">
                            <div class="points-table--line">
                              <div class="points-table--td1">основные баллы</div>
                              <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                                г.
                              </div>
                              <div class="points-table--td3"><?= TempConfirmedBonuses::calculateBonusSum(TempConfirmedBonuses::BASIC_BONUS, $period, $profile->phone_mobile, $item['egais']) ?></div>
                            </div>
                            <div class="points-table--line">
                              <div class="points-table--td1">целевые баллы</div>
                              <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                                г.
                              </div>
                              <div class="points-table--td3"><?= TempConfirmedBonuses::calculateBonusSum(TempConfirmedBonuses::TARGET_BONUS, $period, $profile->phone_mobile, $item['egais']) ?></div>
                            </div>
                            <div class="points-table--line">
                              <div class="points-table--td1">призовые баллы</div>
                              <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                                г.
                              </div>
                              <div class="points-table--td3"><?= TempConfirmedBonuses::calculateBonusSum(TempConfirmedBonuses::SOCIAL_BONUS, $period, $profile->phone_mobile, $item['egais']) ?></div>
                            </div>
                          </div>
                        <?php endforeach; ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
      </div>
    </div>
  </div>
  </div>
<?php endif; ?>
<!--Детализация для участников Дистрибьюторов-->
<?php if ($profile->role == Profile::ROLE_DISTRIBUTOR): ?>

  <div class="container">
    <div class="row">
      <!--Начисленные баллы Дистр-->
      <div class="col-md-12 mb-75 block-bb2" id="award-points">
        <h2>Начисленные баллы</h2>
        <div class="points-block">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading-1">
              <div class="panel-title points-block--name">
                <a class="collapsed points-link" role="button" data-toggle="collapse" href="#collapse-1"
                   aria-expanded="false" aria-controls="collapseOne">
                    <?php echo $profile->last_name . " " . $profile->first_name . " " . $profile->middle_name ?>
                </a>
                <small class="pink">Участник-Дисрибьютор</small>
              </div>
              <div class="points-block--total">
                <small>Всего начислено баллов</small>
                <span class="pink"><?= TempBonuses::calculateBonusSum($bonusType = null, $period = null, $profile->phone_mobile, $codeEGAIS = null) ?></span>
              </div>
            </div>
            <div id="collapse-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-1">

              <div class="panel-body">
                <div class="points-table">
                    <?php foreach (TempBonuses::getCurrentPeriod() as $period): ?>
                      <div class="points-table--header">
                        <div class="points-table--td1">Начисленные баллы за месяц, в том числе</div>
                        <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                          г.
                        </div>
                        <div class="points-table--td3"><?= TempBonuses::calculateBonusSum($bonusType = null, $period, $profile->phone_mobile, $codeEGAIS = null) ?></div>
                      </div>
                      <div class="points-table--body">
                        <div class="points-table--line">
                          <div class="points-table--td1">основные баллы</div>
                          <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                            г.
                          </div>
                          <div class="points-table--td3"><?= TempBonuses::calculateBonusSum(TempBonuses::BASIC_BONUS, $period, $profile->phone_mobile, $codeEGAIS = null) ?></div>
                        </div>
                        <div class="points-table--line">
                          <div class="points-table--td1">целевые баллы</div>
                          <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                            г.
                          </div>
                          <div class="points-table--td3"><?= TempBonuses::calculateBonusSum(TempBonuses::TARGET_BONUS, $period, $profile->phone_mobile, $codeEGAIS = null) ?></div>
                        </div>
                      </div>
                    <?php endforeach; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--Начисленные баллы Дистр =КОНЕЦ=-->
      <!--Подтвержденные баллы Дистр-->
      <div class="col-md-12 mb-75 block-bb1" id="confirm-points">
        <h2>Подтвержденные баллы</h2>
        <div class="points-block pbv2">
          <div class="panel panel-default">


            <div class="panel-heading" role="tab" id="heading-6">
              <div class="panel-title points-block--name">
                <a class="collapsed points-link pink" role="button" data-toggle="collapse" href="#collapse7-7"
                   aria-expanded="false" aria-controls="collapseTwo">
                    <?php echo $profile->last_name . " " . $profile->first_name . " " . $profile->middle_name ?>
                </a>
                <small class="black">
                  <br/>Участник -Дистрибьютор
                </small>
              </div>
              <div class="points-block--total">
                <small>Всего подтвержденных баллов за весь период участия</small>
                <span class="pink"><?= TempConfirmedBonuses::calculateBonusSum($bonusType = null, $period = null, $profile->phone_mobile, $codeEGAIS = null) ?></span>
              </div>
              <div class="points-block--total">
                <small>Всего потрачено баллов за весь период участия</small>
                <span class="pink"><?=ProfileTransactionSearch::getOutBalance($profile->id)?></span>
              </div>
              <div class="points-block--total">
                <small>Итого остаток доступный к использованию</small>
                <span class="pink"><?=$profile->purse->balance?></span>

                 <?php
                    //Для вычисления остатка баллов
                    $itogOstatok = $profile->purse->balance;

                    ?>

              </div>
            </div>

            <div id="collapse7-7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-6">
              <div class="panel-body">
                  <?php foreach (TempConfirmedBonuses::getCurrentPeriod() as $period): ?>
                    <div class="points-table">
                      <div class="points-table--header">
                        <div class="points-table--td1">Баллы и остаток баллов на начало периода</div>
                        <div class="points-table--td2">Период</div>
                        <div class="points-table--td3">Баллы</div>
                      </div>
                      <div class="points-table--body">
                        <div class="points-table--line">
                          <div class="points-table--td1">Остаток баллов на начало периода</div>
                          <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                            г.
                          </div>

                            <?php $itogOstatok = $itogOstatok - TempConfirmedBonuses::calculateBonusSum(TempConfirmedBonuses::BASIC_BONUS, $period, $profile->phone_mobile, $codeEGAIS = null) - TempConfirmedBonuses::calculateBonusSum(TempConfirmedBonuses::TARGET_BONUS, $period, $profile->phone_mobile, $codeEGAIS = null)+ProfileTransactionSearch::getOutBalance($profile->id, $period);?>
                          <div class="points-table--td3"><?=$itogOstatok?></div>
                        </div>
                        <div class="points-table--line">
                          <div class="points-table--td1">основные баллы</div>
                          <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                            г.
                          </div>
                          <div class="points-table--td3"><?= TempConfirmedBonuses::calculateBonusSum(TempConfirmedBonuses::BASIC_BONUS, $period, $profile->phone_mobile, $codeEGAIS = null) ?></div>
                        </div>
                        <div class="points-table--line">
                          <div class="points-table--td1">целевые баллы</div>
                          <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                            г.
                          </div>
                          <div class="points-table--td3"><?= TempConfirmedBonuses::calculateBonusSum(TempConfirmedBonuses::TARGET_BONUS, $period, $profile->phone_mobile, $codeEGAIS = null) ?></div>
                        </div>
                        <div class="points-table--line">
                            <div class="points-table--td1">потрачено баллов за период</div>
                            <div class="points-table--td2"><?= TempBonuses::getMonthName(date("m", strtotime($period))) ?> <?= date("Y", strtotime($period)) ?>
                                  г.
                            </div>
                            <div class="points-table--td3"><?= ProfileTransactionSearch::getOutBalance($profile->id, $period) ?></div>
                        </div>
                      </div>
                    </div>
                  <?php endforeach; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--Подтвержденные баллы Дистр =КОНЕЦ=-->
    </div>
  </div>
<?php endif; ?>
<!--Детализация для участников Дистрибьюторов-->

<?= AddRtt::widget() ?>
