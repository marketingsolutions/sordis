<?php
use yii\grid\GridView;




$this->title = 'Детализация';
$this->params['header'] = 'Детализация';
$this->params['breadcrumbs'][] = [
    'template' => "<li><b>{link}</b></li>\n", //  шаблон для этой ссылки
    'label' => 'Мои скидки', // название ссылки
    'url' => ['/dashboard/discount'] // сама ссылка;
];;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row container">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'document_id',
                            'label' => '№ заказа',
                        ],
                        [
                            'attribute' => 'discount_sum',
                            'label' => 'Сумма компенсации',
                        ],
                        [
                            'attribute' => 'document_number',
                            'label' => '№ накладной',
                        ],
                        [
                            'attribute' => 'document_created_at',
                            'label' => 'Дата накладной',
                            'format' => ['date', 'php:d.m.Y H:i'],
                        ]
                    ]
                ]); ?>
            </div>
        </div>
    </div>
</div>

