<?php

use ms\loyalty\theme\frontend\widgets\InterfaceWidgets;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

/**
 * @var \yii\web\View $this
 * @var string $content
 */

$loyaltyAsset = \ms\loyalty\theme\frontend\assets\FrontendLoyaltyTheme::register($this);
$assetBaseUrl = $loyaltyAsset->baseUrl;
\frontend\assets\AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>"/>
  <meta name="robots" content="noindex,nofollow">
  <!-- mobile settings -->
  <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0"/>
  <!--[if IE]>
  <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

  <title><?= Html::encode($this->title) ?></title>
  <?= Html::csrfMetaTags() ?>
  <?php $this->head() ?>
</head>
<body oncopy="return false"
      class="<?= ArrayHelper::getValue($this->params, 'body-class-options', 'smoothscroll enable-animation') ?> <?= ArrayHelper::getValue($this->params, 'body-class', 'body-default') ?>">
<?php $this->beginBody() ?>
<div class="wrapper">

  <div id="header" style="background-color: #233767;"
       class="header <?= ArrayHelper::getValue($this->params, 'header-class-options', 'header--transparent header--no-border header--top header-md') ?> clearfix noshadow">
    <header class="header__top-nav">
      <div class="container">
        <button class="btn header__btn-mobile" data-toggle="collapse" data-target=".header__nav-main-collapse">
          <i class="fa fa-bars"></i>
        </button>

        <a class="header__logo pull-left" href="<?= Url::home() ?>">
          <img class="logo-header"
               src="<?= ArrayHelper::getValue($this->params, 'header-logo', $assetBaseUrl . '/images/logo_sp_gray.png') ?>"
               alt=""/>
        </a>

        <div class="navbar-collapse pull-right header__nav-main-collapse collapse">
          <nav class="header__nav-main">
            <?php if (isset($this->blocks['main-menu'])): ?>
              <?= $this->blocks['main-menu'] ?>
            <?php else: ?>
              <ul class="nav nav-pills">
                <?= ArrayHelper::getValue($this->blocks, 'main-menu-before-items', '') ?>
                <?= ArrayHelper::getValue($this->blocks, 'main-menu-items', InterfaceWidgets::show(InterfaceWidgets::GROUP_MAIN_MENU_ITEMS)) ?>
                <?= ArrayHelper::getValue($this->blocks, 'main-menu-after-items', '') ?>
              </ul>
            <?php endif ?>
          </nav>
        </div>
      </div>
    </header>
  </div>

  <?php if (isset($this->params['breadcrumbs'])): ?>
    <section class="page-header page-header--xs">
      <div class="container">
        <?= Breadcrumbs::widget([
          'links' => $this->params['breadcrumbs'],
        ]) ?>
      </div>
    </section>
  <?php endif ?>

  <?php if (isset($this->blocks['index-section'])): ?>
    <?= $this->blocks['index-section'] ?>
  <?php else: ?>
    <section class="index-slider index-slider--fullheight" id="index-section"
             style="<?= ArrayHelper::getValue($this->params, 'slider-style', 'background-image: url(/images/bg2.jpg);') ?>">
      <?= $content ?>
    </section>
  <?php endif ?>

  <footer class="bottom-footer <?= ArrayHelper::getValue($this->params, 'footer-class-options', 'bottom-footer--light') ?>">
    <div class="bottom-footer__copyright">
      <div class="container">
        <ul class="pull-right nomargin list-inline mobile-block">
          <li>
            <?php if (isset($this->blocks['bottom-copyright'])): ?>
              <?= $this->blocks['bottom-copyright'] ?>
            <?php else: ?>
              ООО "Бизнес решения"<br>
              <!-- Телефон горячей линии: 8 800 500-82-46<br>
               (с 9 до 18 по московскому времени)-->
            <?php endif ?>
          </li>
        </ul>
        <?php if (isset($this->blocks['bottom-logos'])): ?>
          <?= $this->blocks['bottom-logos'] ?>
        <?php else: ?>
          <img class="bottom-footer__logo"
               src="<?= ArrayHelper::getValue($this->params, 'bottom-logo-1', $assetBaseUrl . '/images/logo_ms_bottom.png') ?>">
        <?php endif ?>

      </div>
    </div>
  </footer>
</div>

<a href="#" id="toTop"></a>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
