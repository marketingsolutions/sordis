<?php
use frontend\assets\AppAsset;
use ms\loyalty\theme\frontend\widgets\InterfaceWidgets;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
?>
<?php $this->beginPage() ?>
<?php $this->beginBody() ?>
	<?= $content ?>
<?php $this->endBody() ?>
<?php $this->endPage() ?>
