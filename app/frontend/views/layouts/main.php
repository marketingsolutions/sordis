<?php

use frontend\widgets\Alerts;
use marketingsolutions\thumbnails\Thumbnail;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use ms\loyalty\theme\frontend\widgets\InterfaceWidgets;
use modules\profiles\common\models\Profile;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
\frontend\assets\AppAsset::register($this);

//Для переадресация для Менеджера ТО
$isTO = Yii::$app->session->get('managerTo');

/**
 * By default this layout renders layout from theme module
 */

$this->title = 'СистемаПромо';

$loyaltyAsset = \ms\loyalty\theme\frontend\assets\FrontendLoyaltyTheme::register($this);
$assetBaseUrl = $loyaltyAsset->baseUrl;



?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>"/>
  <meta name="robots" content="noindex,nofollow">
  <!-- mobile settings -->
  <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0"/>
  <!--[if IE]>
  <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

  <title><?= Html::encode($this->title) ?></title>

  <?= Html::csrfMetaTags() ?>
  <?php $this->head() ?>
</head>
<body oncopy="return false"
      class="<?= ArrayHelper::getValue($this->params, 'body-class-options', 'smoothscroll enable-animation boxed') ?> <?= ArrayHelper::getValue($this->params, 'body-class', 'body-default') ?>">

<?php if(isset($isTO) && $isTO && Profile::redirectManagerTO()):?>
    <?php Yii::$app->response->redirect(['dashboard/index']);?>
<?php endif;?>



<?php $this->beginBody() ?>

<?php if (ArrayHelper::getValue($this->params, 'enable-top-bar', true)): ?>

<?php endif ?>

<?php if (isset(\Yii::$app->user->identity->profile)): ?>
<div id="header" class="header ferma_header">
  <?php else: ?>
  <div id="header" class="header noauth-header">
    <?php endif; ?>
    <header class="header__top-nav container">
      <div class="top-wrap">
        <button class="btn header__btn-mobile" data-toggle="collapse" data-target=".header__nav-main-collapse">
          <i class="fa fa-bars"></i>
        </button>

        <?= InterfaceWidgets::show(InterfaceWidgets::GROUP_HEADER_RIGHT) ?>

        <a href="<?= Url::home() ?>" class="logo-link">
          <?php if (Yii::$app->user->id): ?>
            <img class="logo-header" src="/images/logo_top.png" alt=""/>
          <?php endif; ?>
        </a>
        <?php if (isset(\Yii::$app->user->identity->profile)): ?>
        <div class="menu-block">
          <div class="navbar-collapse header__nav-main-collapse collapse nav_new">
            <nav class="header__nav-main new_nav">
              <?php else: ?>
              <div class="navbar-collapse header__nav-main-collapse collapse">
                <nav class="header__nav-main">
                  <?php endif; ?>
                  <?php if (isset($this->blocks['main-menu'])): ?>
                    <?= $this->blocks['main-menu'] ?>
                  <?php else: ?>
                  <?php if (isset(\Yii::$app->user->identity->profile)): ?>
                  <ul class="nav nav-pills_user">
                    <?php else: ?>
                    <ul class="nav nav-pills">
                      <?php endif; ?>
                      <?= ArrayHelper::getValue($this->blocks, 'main-menu-before-items', '') ?>
                      <?= ArrayHelper::getValue($this->blocks, 'main-menu-items', InterfaceWidgets::show(InterfaceWidgets::GROUP_MAIN_MENU_ITEMS)) ?>
                      <?= ArrayHelper::getValue($this->blocks, 'main-menu-after-items', '') ?>
                    </ul>
                    <?php endif ?>
                </nav>
              </div>

              <!--лк-->
              <div id="topBar" style="font-size: 14px; " class="top-bar">
                <div class="user-name">
                  <?= ArrayHelper::getValue($this->blocks, 'top-bar-before-items', '') ?>
                  <?= ArrayHelper::getValue($this->blocks, 'top-bar-items', InterfaceWidgets::show(InterfaceWidgets::GROUP_TOP_BAR_ITEMS)) ?>
                  <?= ArrayHelper::getValue($this->blocks, 'top-bar-after-items', '') ?>
                </div>
              </div>
              <!--лк  --КОНЕЦ-->
          </div>
        </div>
    </header>
  </div>

  <?php if (isset($this->params['breadcrumbs']) || isset($this->params['header'])): ?>
    <section class="page-header <?= ArrayHelper::getValue($this->params, 'page-header-class-options', '') ?>">
      <div class="container">
        <?php if (isset($this->params['breadcrumbs'])): ?>
          <?= Breadcrumbs::widget([
            'options' => [
              'class' => ArrayHelper::getValue($this->params, 'breadcrumb-class', 'breadcrumb' . (isset($this->params['header']) ? ' breadcrumb--right' : '')),
            ],
            'links' => $this->params['breadcrumbs'],
          ]) ?>
        <?php endif ?>
        <?php if (isset($this->params['header'])): ?>
          <h1 class="page-header__h1"><?= $this->params['header'] ?></h1>
        <?php endif ?>
      </div>
    </section>
  <?php endif ?>

  <section class="content">
    <div class="container fon_content mb-75">
      <?= \ms\loyalty\theme\frontend\widgets\Alerts::widget() ?>
      <?= $content ?>
    </div>
  </section>

  <?php if (isset(\Yii::$app->user->identity->profile)): ?>
  <footer>
    <div class="ferma_futer">
      <?php else: ?>
      <footer class="bottom-footer">
        <div class="bottom-footer__copyright">
          <div class="container">
            <?php endif; ?>
            <ul class="pull-right nomargin list-inline mobile-block">
              <li style="color: white;">
                <?php if (isset($this->blocks['bottom-copyright'])): ?>
                  <?= $this->blocks['bottom-copyright'] ?>
                <?php else: ?>
                  <!--Телефон горячей линии: 8 800 500-82-46<br>
                  (с 9 до 18 по московскому времени)-->
                <?php endif ?>
              </li>
            </ul>
            <?php if (isset($this->blocks['bottom-logos'])): ?>
              <?= $this->blocks['bottom-logos'] ?>
            <?php else: ?>
              <img class="bottom-footer__logo" alt=""
                   src="<?= ArrayHelper::getValue($this->params, 'bottom-logo-2', $assetBaseUrl . '/images/logo_sp_bottom.png') ?>">
            <?php endif ?>
          </div>
      </footer>
      <a href="#" id="toTop"></a>

      <?php $this->endBody() ?>
      <?php
      $this->registerJsFile('/js/owl.carousel.min.js');
      $js = <<<JS
      //Переадресация
      var allow = $('#isAllow').val();
      if(allow == 1){
         document.location.href="/dashboard/index"; 
      }
      
        $(document).ready(function(){
          $('.news-carousel').owlCarousel({
            loop:true, //Зацикливаем слайдер
            margin:0, //Отступ от элемента справа в 50px
            nav:true, //Отключение навигации
            autoplay:true, //Автозапуск слайдера
            smartSpeed:1000, //Время движения слайда
            autoplayTimeout:10000, //Время смены слайда
            responsive:{ //Адаптивность. Кол-во выводимых элементов при определенной ширине.
                0:{items:1},
                600:{items:1},
                1000:{items:1}
            }
          });
          if($('#header').hasClass('noauth-header')){
            $('body').addClass('noauth');
          }
          
          $('.quick-cart').parents('.nav-pills').removeClass('hidden');
            $('.quick-cart').click(function() {
              if($('.quick-cart__badge').html() === '0'){
              $('.quick-cart__footer .btn').html('Ваша корзина пуста');
            }
          });          
        });
JS;
      $this->registerJs($js);
      ?>
</body>
</html>
<?php $this->endPage() ?>

