<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 */

$this->params['body-class'] = 'main';
$this->title = 'Система Промо';

$css = <<<CSS
body .wrapper, #index-section {
	height:     100%;
	min-height: 100%;
}
.bottom-footer, .bottom-footer__copyright {
    color: white !important;
}
#loginform {
	padding: 30px 10px 10px 10px;
	max-width: 480px;
    position: fixed;
    bottom: 120px;
    right: 100px;
    display: block;
    z-index: 1000;
}
#loginform, #loginform > div, #loginform > div > div {
	background: none;
}
#loginform {
	color: white;
	background: #B7BCBF !important;
	opacity: 0.8;
	border-radius: 5px;
	border: 2px solid white;
}
CSS;
$this->registerCss($css);
?>


<div class="col-md-5 col-md-offset-2 index_form" id="loginform">
	<div class="panel panel-default2 panel2">
		<div class="panel-body2">
            <?php $form = \yii\bootstrap\ActiveForm::begin(['layout' => 'horizontal'/*, 'action'=>'/identity/auth/login'*/]) ?>

			<p align="center">Вход для участников:</p>


            <?= $form->field($model, 'phone_mobile_local')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '+7 999 999-99-99',
            ]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'rememberMe')->checkbox() ?>

			<div class="form-group">
				<div class="col-sm-7 col-sm-offset-3">
                    <?= Html::submitButton('Войти', ['class' => 'btn btn-primary']) ?>
					<a href="/identity/auth/remind">Забыли свой пароль?</a>
				</div>
			</div>

            <?php \yii\bootstrap\ActiveForm::end() ?>
		</div>
	</div>
</div>
