<?php
use ms\loyalty\feedback\common\models\Faq;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $faqsDataProvider
 */
\yii\bootstrap\BootstrapPluginAsset::register($this);

if ($faqsDataProvider->query->exists() == false) {
    return;
}
?>

<div class="col-md-6">

    <h1>Часто задаваемые вопросы</h1>

    <?= \yii\widgets\ListView::widget([
        'dataProvider' => $faqsDataProvider,
        'itemView' => '_faq_item',
        'layout' => "{items}\n{pager}",
        'options' => [
            'class' => 'panel-group',
            'id' => 'accordion-faq'
        ]
    ]) ?>
</div>
