<?php
use ms\loyalty\feedback\common\models\Category;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \ms\loyalty\feedback\frontend\forms\MessageForm $model
 */

$this->title = 'Помощь';
$this->params['header'] = 'Помощь';
$this->params['breadcrumbs'][] = 'Помощь';
?>

<div class="row container">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <p>Задайте Ваш вопрос с помощью формы, мы ответим на него в ближайшее время</p>
                <div class="pull-right">
                    <?php if(isset(\Yii::$app->user->identity->profile)):?>
                    <a href="/profiles/profile/message">Мои сообщения</a>
                    <?php endif;?>
                </div>
                <?php $form = ActiveForm::begin([
                    'method' => 'post',
                ]) ?>

                <?php if (Category::find()->exists()): ?>
                    <?= $form->field($model, 'category_id')->dropDownList($model->getCategoryIdValues()) ?>
                <?php endif ?>
                <?= $form->field($model, 'name')->textInput() ?>
                <?= $form->field($model, 'email')->input('email') ?>
                <?= $form->field($model, 'phone_mobile_local', [
                    'errorOptions' => ['class' => 'help-block', 'encode' => false],
                ])->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '+7 999 999-99-99',
                ]) ?>
                <?= $form->field($model, 'content')->textarea(['rows' => 5]) ?>

                <div class="row form-group">
                    <div class="col-md-8">
                        <label class="control-label" for="messageform-content">Прикрепить файл</label>
                        <?= \ms\files\attachments\common\widgets\FileAttachmentsEdit::widget([
                            'model' => $model,
                            'options' => [
                                'itemClass' => 'col-md-12',
                            ],
                            'maximumFiles' => 3,
                            'type' => 'userFile',
                        ]) ?>
                    </div>
                </div>

                <?php if ($model->scenario === \ms\loyalty\feedback\frontend\forms\MessageForm::SCENARIO_MESSAGE_FROM_GUEST): ?>
                    <?php if(getenv('RECAPTCHA_SITEKEY') && getenv('RECAPTCHA_SECRETKEY')): ?>
                        <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className(), [
                            'siteKey' => getenv('RECAPTCHA_SITEKEY'),
                            'size' => 'compact',
                        ])->label(false) ?>
                    <?php else: ?>
                        <?= $form->field($model, 'verificationCode')->widget(\yii\captcha\Captcha::className(), [
                            'captchaAction' => '/site/captcha',
                            'imageOptions' => ['class' => 'pull-left'],
                            'options' => ['class' => 'form-control'],
                        ]) ?>
                    <?php endif; ?>
                <?php endif ?>

                <div class="row">
                    <div class="col-md-8">
                        <?= Html::submitButton('Отправить сообщение', ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>

                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

