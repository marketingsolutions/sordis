<?php

/**
 * @var \yii\web\View $this
 * @var \ms\loyalty\feedback\frontend\forms\MessageForm $model
 * @var \yii\data\ActiveDataProvider $faqsDataProvider
 */
?>
<div class="row">

    <?= $this->render('partials/_faq', compact('faqsDataProvider')) ?>
    <?= $this->render('partials/_message', compact('model')) ?>

</div>