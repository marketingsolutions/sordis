<?php

/**
 * @var \yii\web\View $this
 * @var \ms\loyalty\feedback\common\models\Comment $model
 * @var array $history
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Добавить комментарий';
$this->params['header'] = 'Добавить комментарий';
$this->params['breadcrumbs'][] = 'Добавить комментарий';

?>

<div class="row container">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <h3>Ваш комментарий к обращению № <?= $model->message_id ?></h3>

        <?php $form = ActiveForm::begin(['method' => 'post']) ?>
        <?= $form->field($model, 'comment')->textarea(['rows' => 6])->label(false) ?>

        <div class="row">
          <div class="col-md-8">
              <?= Html::submitButton('Добавить', ['class' => 'btn btn_all']) ?>
          </div>
        </div>

        <?php ActiveForm::end() ?>

        <?php if (!empty($history)): ?>
          <hr/>
          <h3>История</h3>
          <?php foreach ($history as $h): ?>
          <div class="feedback-history">
            <div>
              <small><?= (new \DateTime($h->created_at))->format('d.m.Y H:i') ?></small>
            </div>
            <?php if ($h instanceof \ms\loyalty\feedback\common\models\Message): ?>
              <div><b>Ваше обращение</b>: <?= $h->content ?></div>
            <?php elseif ($h instanceof \ms\loyalty\feedback\common\models\Comment): ?>
              <div><b>Ваш комментарий</b>: <?= $h->comment ?></div>
            <?php elseif ($h instanceof \ms\loyalty\feedback\common\models\Reply): ?>
              <div><b>Модератор</b>: <?= $h->reply ?></div>
            <?php endif; ?>
          </div>
          <?php endforeach; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
