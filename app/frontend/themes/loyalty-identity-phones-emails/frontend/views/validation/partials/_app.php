<?php
use ms\loyalty\identity\phonesEmails\frontend\assets\PhoneEmailValidationAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var string $validateCodeText
 */

PhoneEmailValidationAsset::register($this);

?>

<div class="col-md-6 col-md-offset-3" ng-app="PhoneEmailValidation" ng-controller="Validation">
    <div class="panel panel-default">
        <div class="panel-heading">
            Подтвердите свой номера телефона в системе
        </div>
        <div class="panel-body">

            <div class="alert alert-danger" ng-show="errors.length > 0" ng-cloak>
                <strong>Внимание!</strong> Обнаружены следующие ошибки:
                <ul>
                    <li ng-repeat="error in errors">
                        {{ error.message }}
                    </li>
                </ul>
            </div>

            <div ng-show="! tokenRequested">

                <form class="form-horizontal">

                    <?php if(isset($this->blocks['app-prefix-text'])): ?>
                        <?= $this->blocks['app-prefix-text'] ?>
                    <?php else: ?>
                        <p><!--Выберите, что Вы хотите использовать для авторизации на сайте:--></p>
                    <?php endif ?>

<!--                    <p>-->
<!--                        <a href="#" class=""-->
<!--                           ng-click="type = 'phone'"-->
<!--                           ng-class="{'link-primary': type == 'phone'}">Номер телефона</a>-->
<!--                        или-->
<!--                        <a href="#" class=""-->
<!--                           ng-click="type = 'email'"-->
<!--                           ng-class="{'link-primary': type == 'email'}">адрес электронной почты</a>-->
<!--                    </p>-->

                    <div class="form-group" ng-show="type == 'email'">
                        <?= Html::label('Электронная почта', null, ['class' => 'col-md-3 control-label']) ?>
                        <div class="col-md-5">

                            <input type="text" class="form-control"
                                   ng-model="email"
                                />

                        </div>
                    </div>

                    <div class="form-group" ng-show="type == 'phone'">
                        <?= Html::label('Номер телефона', null, ['class' => 'col-md-3 control-label']) ?>
                        <div class="col-md-5">

                            <input type="text" class="form-control"
                                   ng-model="phone"
                                   input-mask="{mask: '+7 999 999-99-99'}"/>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-3">
                            <button class="btn btn-primary" ng-click="generateToken()" ng-disabled="disabled">
                                <i class="fa fa-btn fa-key" ng-if=" ! disabled"></i>
                                <i class="fa fa-btn fa-spinner fa-spin" ng-if="disabled"></i>
                                Получить код подтверждения
                            </button>
                        </div>
                    </div>

                </form>
            </div>

            <div ng-show="tokenRequested" ng-cloak>

                <form class="form-horizontal">

                    <div ng-if="type == 'phone'">
                        <strong>Код подтверждения отправлен на номер {{ phone }}</strong>

                        <p>Введите полученный код в поле ниже</p>
                    </div>

                    <div ng-if="type == 'email'">
                        <strong>Код подтверждения отправлен на адрес электронной почты {{ email }}</strong>

                        <p>Введите полученный код в поле ниже или перейдите по ссылке, содержащейся в письме</p>
                    </div>


                    <div class="form-group">
                        <?= Html::label('Код подтверждения', null, ['class' => 'col-md-3 control-label']) ?>
                        <div class="col-md-5">
                            <input type="text" class="form-control" ng-model="token"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-3">
                            <button class="btn btn-primary" ng-click="validateToken()" ng-disabled="disabled">
                                <i class="fa fa-btn fa-sign-in" ng-if=" ! disabled"></i>
                                <i class="fa fa-btn fa-spinner fa-spin" ng-if="disabled"></i>
                                <?= $validateCodeText ?>
                            </button>

                            <button class="btn btn-default" ng-click="reenterPhoneEmail()" ng-disabled="disabled">
                                <i class="fa fa-btn fa-repeat" ng-if=" ! disabled"></i>
                                <i class="fa fa-btn fa-spinner fa-spin" ng-if="disabled"></i>
                                Запросить код еще раз
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>