<?php
use ms\loyalty\identity\phonesEmails\frontend\assets\PhoneEmailValidationAsset;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 */

$settings = [
    'generateTokenUrl' => Url::to(['/identity/api/registration-validation/generate-token']),
    'validateTokenUrl' => Url::to(['/identity/api/registration-validation/validate-token']),
];

$js =<<<JS
phoneEmailValidation.settings.generateTokenUrl = '{$settings['generateTokenUrl']}';
phoneEmailValidation.settings.validateTokenUrl = '{$settings['validateTokenUrl']}';
JS;
$this->registerJs($js);

$this->params['header'] = 'Регистрация';

?>

<?= $this->render('partials/_app', ['validateCodeText' => 'Проверить код и перейти к регистрации']) ?>