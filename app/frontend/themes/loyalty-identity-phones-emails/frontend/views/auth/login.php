<?php
use ms\loyalty\identity\phonesEmails\common\models\IdentityType;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \ms\loyalty\identity\phonesEmails\frontend\forms\LoginForm $model
 */


$this->params['header'] = 'Вход в личный кабинет';
?>

<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <div class="panel-body">
            <?php $form = \yii\bootstrap\ActiveForm::begin(['layout' => 'horizontal']) ?>

            <p align="center">
                Введите для входа в систему:
            </p>

            <?php if ($model->type == IdentityType::PHONE): ?>
                <?= $form->field($model, 'phone_mobile_local')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '+7 999 999-99-99',
                ]) ?>
            <?php endif ?>


            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                    <?= Html::submitButton('Войти', ['class' => 'btn btn-primary']) ?>
                    <a href="<?= Url::to(['remind']) ?>">Забыли свой пароль?</a>
                </div>
            </div>

            <?php \yii\bootstrap\ActiveForm::end() ?>
        </div>
    </div>
</div>
