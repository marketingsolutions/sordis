<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \ms\loyalty\identity\phonesEmails\frontend\forms\ChangePasswordForm $model
 */
$this->params['header'] = 'Восстановление пароля';

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Вход
                </div>
                <div class="panel-body">
                    <?php $form = \yii\bootstrap\ActiveForm::begin(['layout' => 'horizontal']) ?>

                    Введите новый пароль:

                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <?= $form->field($model, 'passwordCompare')->passwordInput() ?>

                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-3">
                            <?= Html::submitButton('Сменить пароль', ['class' => 'btn btn-primary']) ?>

                            <a href="<?= Url::home() ?>">Отмена</a>
                        </div>
                    </div>

                    <?php \yii\bootstrap\ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>