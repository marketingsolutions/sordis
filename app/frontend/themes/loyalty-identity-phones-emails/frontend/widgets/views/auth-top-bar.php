<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 */
?>

<?= \yii\widgets\Menu::widget([
    'options' => [
        'tag' => false,

    ],
    'items' => [
        [
            'label' => 'Выход',
            'url' => ['/identity/auth/logout'],
            'visible' => ! Yii::$app->user->isGuest,
            'options' => [
                'class' => 'logout-link',
                'style' =>'font-size:14px;'
            ]
        ],
    ]
]) ?>
