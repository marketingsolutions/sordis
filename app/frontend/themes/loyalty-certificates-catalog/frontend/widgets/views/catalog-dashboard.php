<?php

use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 */

\yz\icons\FontAwesomeAsset::register($this);
?>
<div class="col-md-12">
  <div class="catalog-title">
    <h3 class="header_top"><?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Catalog' : 'Каталог электронных сертификатов' ?></h3>
    <a class="btn btn-primary btn_all" href="<?= Url::to(['/catalog/orders/index']) ?>">
        <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Orders' : 'Совершенные заказы' ?>
    </a>
  </div>
</div>
<div class="col-md-12 catalog-cards-widget">

  <div class="row">
    <div class="col-md-12">
        <?= \ms\loyalty\catalog\frontend\widgets\Cards::widget([
            'showDescription' => true,
            'maximum' => 2
        ]) ?>
    </div>
  </div>

</div>
<div class="row" style="margin-bottom: 15px; margin-left: 0px;">
  <div class="col-md-12 ">
    <a class="btn btn-primary btn_all" href="<?= Url::to(['/catalog/catalog/index']) ?>">
      <span class="fa fa-credit-card"></span>
      <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Complete catalog' : 'Перейти в полный каталог' ?>
    </a>
  </div>
</div>
