<?php
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \ms\loyalty\catalog\frontend\components\CatalogShoppingCart $cart
 */
$css=<<<CSS
.my_center{
    font-family: Calibri;
}
CSS;

$this->registerCss($css);
?>

<div class="shopping-cart">
	<h3 class="text-center my_center">
		<?//= \yz\icons\Icons::i('shopping-cart') ?>
		<?php if ($cart->isEmpty): ?>
			<?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Cart is empty' : 'Корзина товаров пуста' ?>
		<?php else: ?>
            <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Cart:' : 'В Вашей корзине' ?>
            <?= $cart->count ?>
            <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'certificates for' : 'сертификата(ов) на' ?>
            <?= $cart->cost ?> <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? '' : 'баллов' ?>
			<a href="<?= Url::to(['/catalog/orders/create']) ?>" class="btn btn-primary btn_all">
				<?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Order cards' :'Перейти к оформлению заказа' ?>
			</a>
		<?php endif ?>
	</h3>
</div>
