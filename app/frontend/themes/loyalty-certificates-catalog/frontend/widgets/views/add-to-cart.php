<?php
use ms\loyalty\catalog\frontend\assets\CatalogAsset;
use yii\helpers\Html;
use yz\icons\Icons;

/**
 * @var \yii\web\View $this
 * @var \ms\loyalty\catalog\frontend\widgets\AddToCart $widget
 * @var \ms\loyalty\catalog\common\cards\Card $card
 */
$orderedCard = $card->getCartPosition();
CatalogAsset::register($this);
$this->registerJs("electronicCatalog.watchAddToCart('#addToCart_{$orderedCard->card_id}')", \yii\web\View::POS_READY, 'cart_watch_' . $orderedCard->card_id);
?>
<?php echo Html::beginTag('div', [
	'class' => 'add-to-cart',
	'id' => 'addToCart_' . $orderedCard->card_id,
	'data' => [
		'card-id' => $orderedCard->card_id,
		'allow-manual-nominals' => $widget->allowManualNominals(),
	]
]) ?>
<?php echo Html::beginForm(['/catalog/cart/add'], 'post', ['class' => 'catalog-cards__add-to-cart-form',]) ?>
<?= Html::hiddenInput('id', $orderedCard->card_id) ?>

	<div class="catalog-cards__selected">

    <?= Html::input('number','quantity', $orderedCard->quantity, ['class' => 'form-control catalog-cards__quantity', 'min' => 1]) ?>

    <?php if ($widget->allowManualNominals()): ?>
      <?= Html::textInput('nominal', $orderedCard->nominal, ['class' => 'form-control catalog-cards__free-nominal', 'style' => 'display: none']) ?>
    <?php endif ?>
    <?= Html::dropDownList('nominal', $orderedCard->nominal, $widget->getSelectableNominals(), [
      'class' => 'form-control catalog-cards__price-select',
    ]) ?>
	</div>

  <button type="submit" class="btn btn-primary btn_pay">
    <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Add to cart' : 'В корзину' ?>
  </button>

<?php echo Html::endForm(); ?>
<?= Html::endTag('div') ?>
