<?php

use ms\loyalty\taxes\common\calculators\Calculator;
use ms\loyalty\taxes\common\calculators\CalculatorDiscount;
use ms\loyalty\taxes\common\Module;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \ms\loyalty\catalog\frontend\components\CatalogShoppingCart $cart
 */
?>

<ul class="pull-right nav nav-pills header__nav-second-main<?= $cart->isEmpty ? ' hidden' : '' ?>">
  <li class="quick-cart">
    <a class="quick-cart__cart" href="#">
			<span
        class="quick-cart__badge quick-cart__badge--corner quick-cart__badge--aqua badge btn-xs"><?= $cart->getCount() ?></span>
      <i class="quick-cart__icon fa fa-shopping-cart"></i>
    </a>
    <div class="quick-cart__box">
      <h4>
          <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Ordered cards' : 'Электронные подарочные сертификаты' ?>
      </h4>

      <div class="quick-cart__wrapper">

          <?php foreach ($cart->positions as $position): /** @var \ms\loyalty\catalog\common\models\OrderedCard $position */ ?>
            <div class="quick-cart__item">
              <h6><?= $position->card->title ?> на <?= $position->nominal ?>p. x <?= $position->quantity ?></h6>
                <?php
                /** @var \ms\loyalty\catalog\common\cards\Card $card */
                $card = $position->card;
                $price = $position->nominal;
                $price_comment = null;

                if (!empty($card->profile_discount)) {
                    $calculator = new CalculatorDiscount($price, $card->profile_discount_real, 0);
                    $price = $calculator->profile_amount;
                    $price_comment = \Yii::$app->getModule('taxes')->incomeTaxPaymentMethod == Module::INCOME_TAX_PAYMENT_METHOD_PROFILE
                        ? "скидка {$card->profile_discount_real}%, НДФЛ 13%"
                        : "скидка {$card->profile_discount_real}%";
                }
                elseif (!empty($card->profile_commission)) {
                    $calculator = new Calculator($price, $card->profile_commission_real, 0);
                    $price = $calculator->profile_amount;
                    $price_comment = \Yii::$app->getModule('taxes')->incomeTaxPaymentMethod == Module::INCOME_TAX_PAYMENT_METHOD_PROFILE
                        ? "наценка {$card->profile_commission_real}%, НДФЛ 13%"
                        : "наценка {$card->profile_discount_real}%";
                }
                elseif (\Yii::$app->getModule('taxes')->incomeTaxPaymentMethod == Module::INCOME_TAX_PAYMENT_METHOD_PROFILE) {
                    $calculator = new Calculator($price, 0, 0);
                    $price = $calculator->profile_amount;
                    $price_comment = "НДФЛ 13%";
                }
                ?>

                <?php if ($price_comment && $price): ?>
                    <?= "<h6>Стоимость для Вас: $price баллов<br/>($price_comment)</h6>" ?><?php endif; ?>
            </div>
          <?php endforeach ?>

      </div>

      <div class="quick-cart__footer clearfix">
        <a href="<?= Url::to(['/catalog/orders/create']) ?>" class="btn btn-primary btn-xs pull-right btn_all">
            <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Cart' : 'Перейти в корзину' ?>
        </a>
      </div>

    </div>
  </li>
</ul>
