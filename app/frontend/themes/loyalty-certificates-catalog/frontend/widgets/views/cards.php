<?php

use Imagine\Image\ManipulatorInterface;
use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var \ms\loyalty\catalog\common\cards\Card[] $cards
 * @var bool $showDescription
 */
$thumb = new \marketingsolutions\thumbnails\Thumbnail();
?>

<div class="catalog-cards row">
  <?php foreach ($cards as $card): ?>
    <div class="col-sm-6 col-xs-12">
      <div class="catalog-cards__card">
        <h4 class="catalog-card-title header_top">
          <?= Html::encode($card->name) ?>
        </h4>

        <div class="catalog-cards__body">
          <div class="catalog-cards__image">
              <?php if ($card->image): ?>
                  <img class="card-image" src="<?php echo $thumb->url($card->getImageFilePath(), function ($image) {
                      return \yii\imagine\Image::thumbnail($image, 256, 128, ManipulatorInterface::THUMBNAIL_INSET);
                  }, '256x128ins') ?>" alt=""/>
              <?php endif ?>
            <?= \ms\loyalty\catalog\frontend\widgets\AddToCart::widget(['card' => $card]) ?>
          </div>
            <div class="catalog-cards__info">
            <?php if ($showDescription): ?>
              <div>
                <?= $card->description ?>
              </div>
            <?php endif ?>
          </div>

        </div>
      </div>
    </div>
  <?php endforeach ?>
</div>
