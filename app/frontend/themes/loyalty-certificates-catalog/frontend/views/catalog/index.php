<?php
use ms\loyalty\catalog\frontend\widgets\Cards;

/**
 * @var \yii\web\View $this
 */

$title = \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Catalog' : 'Каталог электронных сертификатов';
$this->params['header'] = $title;
$this->params['breadcrumbs'][] = $title;
?>

	<div class="shopping-cart-container">
		<div class="shopping-cart-container-inner">
			<?= \ms\loyalty\catalog\frontend\widgets\Cart::widget() ?>
		</div>
	</div>
<?php echo Cards::widget() ?>
