<?php

use ms\loyalty\catalog\common\finances\CalculatorDiscount;
use ms\loyalty\taxes\common\calculators\Calculator;
use ms\loyalty\taxes\common\Module;
use yii\helpers\Html;
use yz\icons\Icons;

/**
 * @var \yii\web\View $this
 * @var \ms\loyalty\catalog\common\models\OrderedCard $position
 * @var \yii\bootstrap\ActiveForm $form
 */

$card = $position->card;
$price = $position->nominal * $position->quantity;
$price_comment = null;

if (!empty($card->profile_discount)) {
    $calculator = new CalculatorDiscount($price, $card->profile_discount_real, 0);
    $price = $calculator->profile_amount;
    $price_comment = \Yii::$app->getModule('taxes')->incomeTaxPaymentMethod == Module::INCOME_TAX_PAYMENT_METHOD_PROFILE
        ? "скидка {$card->profile_discount_real}%, НДФЛ 13%"
        : "скидка {$card->profile_discount_real}%";
}
elseif (!empty($card->profile_commission)) {
    $calculator = new Calculator($price, $card->profile_commission_real, 0);
    $price = $calculator->profile_amount;
    $price_comment = \Yii::$app->getModule('taxes')->incomeTaxPaymentMethod == Module::INCOME_TAX_PAYMENT_METHOD_PROFILE
        ? "наценка {$card->profile_commission_real}%, НДФЛ 13%"
        : "наценка {$card->profile_commission_real}%";
}
elseif (\Yii::$app->getModule('taxes')->incomeTaxPaymentMethod == Module::INCOME_TAX_PAYMENT_METHOD_PROFILE) {
    $calculator = new Calculator($price, 0, 0);
    $price = $calculator->profile_amount;
    $price_comment = "НДФЛ 13%";
}
?>

<tr>
	<td>
		<?= Html::encode($position->getName()) ?>
	</td>
	<td>
		<?= $position->nominal ?>
	</td>
	<td class="col-md-2">
		<div class="input-group">
			<?= Html::activeInput('number', $position, '[' . $position->getId() . ']quantity', ['class' => 'form-control position-quantity', 'min' => 1]) ?>
			<span class="input-group-addon">
				<?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'units' : 'шт.' ?>
			</span>
		</div>
	</td>
	<td>
		<?= $price ?> <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? '' : 'баллов' ?>
		<?php if (!empty($price_comment)): ?>
			<div><small>(<?= $price_comment ?>)</small></div>
		<?php endif; ?>
	</td>
	<td>
		<?= Html::a(Icons::i('trash'), ['/catalog/cart/remove', 'id' => $position->getId()], ['class' => 'btn btn-danger']) ?>
	</td>
</tr>
