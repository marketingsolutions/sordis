<?php

use ms\loyalty\catalog\common\models\OrderedCard;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \ms\loyalty\catalog\common\models\CatalogOrder[] $orders
 */
$statuses = [
    OrderedCard::STATUS_NEW => Html::tag('span', OrderedCard::getStatusValues()[OrderedCard::STATUS_NEW], ['class' => 'label label-default']),
    OrderedCard::STATUS_ORDERED => Html::tag('span', OrderedCard::getStatusValues()[OrderedCard::STATUS_ORDERED], ['class' => 'label label-info']),
    OrderedCard::STATUS_PARTIALLY_READY => Html::tag('span', OrderedCard::getStatusValues()[OrderedCard::STATUS_PARTIALLY_READY], ['class' => 'label label-warning']),
    OrderedCard::STATUS_READY => Html::tag('span', OrderedCard::getStatusValues()[OrderedCard::STATUS_READY], ['class' => 'label label-success']),
    OrderedCard::STATUS_REJECTED => Html::tag('span', OrderedCard::getStatusValues()[OrderedCard::STATUS_REJECTED], ['class' => 'label label-danger']),
    OrderedCard::STATUS_USER_CANCEL => Html::tag('span', OrderedCard::getStatusValues()[OrderedCard::STATUS_USER_CANCEL], ['class' => 'label label-warning']),
];

$this->title = \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Orders' : 'Ваши заказы';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'][] = $this->title;

\yz\icons\FontAwesomeAsset::register($this);
?>
<div class="panel panel_ferma orders-list">

    <?php if (count($orders) == 0): ?>
      <h4><?= \Yii::$app->session->get('lang', 'ru') == 'en'
              ? 'There is no orders'
              : 'Вы еще не совершили ни одного заказа'
          ?></h4>
    <?php else: ?>
        <?php foreach ($orders as $order): ?>

        <div data-toggle="collapse" data-target="#composeOrder" class="orders-list--head collapsed">
          <div class="olh-left">
              <?php if (\Yii::$app->session->get('lang', 'ru') == 'en'): ?>
                <div class="orders-list--number">
                  Order #<?= $order->id ?> at <?= Yii::$app->formatter->asDate($order->created_at) ?>
                </div>
              <?php else: ?>
                <div class="orders-list--number">Заказ №<?= $order->id ?></div>
                <div class="pink orders-list--date"><?= Yii::$app->formatter->asDate($order->created_at) ?></div>
              <?php endif; ?>
          </div>

          <div class="olh-right">
              <?php if (\Yii::$app->session->get('lang', 'ru') == 'en'): ?>
                Order amount: <?= $order->profile_amount ?>
              <?php else: ?>
                <span>Сумма заказа:</span>
                <div class="balls-block">
                  <span class="pink"><?= $order->profile_amount ?></span>
                  <span><?= Yii::t('loyalty', 'баллов') ?></span>
                </div>

              <?php endif; ?>
          </div>
        </div>

            <?php if ($order->canBeCanceled()): ?>
          <a href="<?= Url::to(['delete', 'id' => $order->id]) ?>" class="btn btn-danger btn-sm">
            <i class="fa fa-trash"></i>
              <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Cancel order' : 'Отменить заказ' ?>
          </a>
            <?php endif ?>

            <?php if ($order->is_canceled): ?>
          <span class="label label-warning">
              <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Canceled' : 'Отменен участником' ?>
          </span>
            <?php endif ?>

        <div id="composeOrder" class="collapse">
          <div class="composeOrder--head">
            <div class="composeOrder--th cth1">
                <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Ordered cards' : 'Состав заказа' ?>
            </div>
            <div class="composeOrder--th cth2">
              Статус
            </div>
          </div>
            <?php foreach ($order->orderedCards as $orderedCard): ?>
              <div class="composeOrder-wrap">
                <div class="composeOrder--name">
                    <?php if (\Yii::$app->session->get('lang', 'ru') == 'en'): ?>
                        <?= $orderedCard->card->name ?>, <?= $orderedCard->quantity ?> x <?= $orderedCard->nominal ?>
                    <?php else: ?>
                        <?= $orderedCard->card->name ?>,
                        <?= $orderedCard->nominal ?> р.,
                        <?= $orderedCard->quantity ?> шт.
                    <?php endif; ?>
                </div>
                <div class="composeOrder--status">
                    <?= $statuses[$orderedCard->status] ?>
                </div>

                  <?php foreach ($orderedCard->items as $cardItem): ?>
                    <div class="composeOrder--download">
                      <a href="<?= Url::to(['/catalog/certificates/download', 'id' => $cardItem->id]) ?>">
                          <?= \yz\icons\Icons::i('download') ?> <?= \yz\icons\Icons::i('file-pdf-o') ?>
                        скачать сертификат
                      </a>
                      <small>(<?= $cardItem->getTextualRepresentation() ?>)</small>
                      <div>Дата начала действия: <?= $cardItem->getStartFromText() ?></div>
                    </div>
                  <?php endforeach ?>

              </div>
            <?php endforeach ?>
            <?php if ($order->taxes_profile): ?>
              <div class="composeOrder--ndfl">Списание налога НДФЛ: <?= $order->taxes_profile ?></div>
            <?php endif; ?>

        </div>
        <?php endforeach ?>

    <?php endif ?>

</div>
