<?php

use ms\loyalty\catalog\common\finances\CalculatorDiscount;
use ms\loyalty\catalog\frontend\forms\CatalogOrderForm;
use ms\loyalty\taxes\common\calculators\Calculator;
use ms\loyalty\taxes\common\Module;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var CatalogOrderForm $orderForm
 * @var \ms\loyalty\contracts\prizes\PrizeRecipientInterface $catalogUser
 * @var bool $usePhoneMobile
 */

\yz\icons\FontAwesomeAsset::register($this);
$this->title = \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Complete order' : 'Оформление заказа';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'][] = $this->title;
$profile_amount = 0;
$taxes_profile = 0;
$amount = 0;
?>
<?php $form = ActiveForm::begin() ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="order-errors">
                <?= $form->errorSummary($orderForm, [
                    'class' => 'error-summary order-error-summary alert alert-danger',
                    'header' => 'Заказ не может быть совершен из-за следующих ошибок:'
                ]) ?>
            </div>

            <table class="table">
                <tr>
                    <th><?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Card' : 'Название' ?></th>
                    <th><?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Nominal' : 'Номинал' ?></th>
                    <th><?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Quantity' : 'Количество' ?></th>
                    <th><?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Total' : 'Стоимость для Вас' ?></th>
                    <th></th>
                </tr>
                <?php foreach ($orderForm->positions as $position): ?>
                    <?= $this->render('partials/_position', [
                        'position' => $position,
                    ]) ?>

                    <?php
                    $card = $position->card;
                    $price = $position->nominal * $position->quantity;
                    $amount += $price;

                    if (!empty($card->profile_discount)) {
                        $calculator = new CalculatorDiscount($price, $card->profile_discount_real, 0);
                        $profile_amount += $calculator->profile_amount;
                        $taxes_profile += $calculator->taxes_profile;
                    }
                    elseif (!empty($card->profile_commission)) {
                        $calculator = new Calculator($price, $card->profile_commission_real, 0);
                        $profile_amount += $calculator->profile_amount;
                        $taxes_profile += $calculator->taxes_profile;
                    }
                    else {
                        $calculator = new Calculator($price, 0, 0);
                        $profile_amount += $calculator->profile_amount;
                        $taxes_profile += $calculator->taxes_profile;
                    }
                    ?>
                <?php endforeach ?>
                <tr>
                    <td></td>
                    <td style="vertical-align: bottom">
                        <h3>
                            <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Summary' : 'Итого' ?>:
                        </h3>
                    </td>
                    <td colspan="2" style="vertical-align: bottom">
                        <?php if (\Yii::$app->session->get('lang', 'ru') == 'en'): ?>
                            <div>
                                <small>Summary cards: <?= $amount ?></small>
                            </div>
                            <?php if ($orderForm->taxes_profile): ?>
                                <div>
                                    <small>Summary taxes: <?= $profile_amount ?></small>
                                </div>
                            <?php endif ?>
                        <?php else: ?>
                            <div>
                                <small>Сумма товаров: <?= $profile_amount ?> баллов</small>
                            </div>
                            <?php if ($orderForm->taxes_profile): ?>
                                <div>
                                    <small>Сумма налога НДФЛ: <?= $taxes_profile ?></small>
                                </div>
                            <?php endif ?>
                        <?php endif; ?>
                        <h3><?= $profile_amount ?>
                            <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? '' : 'баллов' ?>
                        </h3>
                    </td>
                    <td>
                        <h3>
                            <?= Html::submitButton(\Yii::$app->session->get('lang', 'ru') == 'en' ? 'Recalculate' : 'Пересчитать',
                                ['class' => 'btn btn_all', 'id' => 'btn-recalculate',
                                    'name' => Html::getInputName($orderForm, 'action'), 'value' => CatalogOrderForm::ACTION_RECALCULATE]) ?>
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <?= $form->field($orderForm, 'delivery_email')->input('email')
                            ->label(\Yii::$app->session->get('lang', 'ru') == 'en'
                                ? 'E-mail for certificate delivery'
                                : 'Электронная почта для получения сертификата'
                            ) ?>
                        <?php if ($usePhoneMobile): ?>
                            <?= $form->field($orderForm, 'delivery_phone_mobile_local')->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '+7 999 999-99-99',
                            ]) ?>
                        <?php endif ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <?php echo $form->field($orderForm, 'is_allow_cancel')
                            ->label(\Yii::$app->session->get('lang', 'ru') == 'en'
                                ? 'Order processing plan'
                                : 'Как нам поступить с Вашим заказом?'
                            )
                            ->radioList(CatalogOrderForm::getIsAllowDeleteValues())
                        ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a class="btn btn-default" href="<?= \yii\helpers\Url::home() ?>">
                            <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Back to catalog' : 'Продолжить выбор' ?>
                        </a>
                        <?= Html::submitButton(\Yii::$app->session->get('lang', 'ru') == 'en' ? 'Order cards' : 'Сделать заказ',
                            ['class' => 'btn btn-success btn-lg',
                                'name' => Html::getInputName($orderForm, 'action'), 'value' => CatalogOrderForm::ACTION_CREATE]) ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

            </table>

        <?php ActiveForm::end() ?>
    </div>
</div>
<?php

$js = <<<JS
(function() {
    $('.position-quantity').on('blur', function() {
        $('#btn-recalculate').click();
    });
})();
JS;
$this->registerJs($js);
