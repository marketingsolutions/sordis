<?php

/**
 * @var \yii\web\View $this
 */
?>

<div class="form-group">
    <label class="col-md-3 control-label">Номер банковской карты (без пробелов)</label>

    <div class="col-md-3">
        <input type="text" class="form-control" ng-model="extra.cardNumber"/>
    </div>
</div>

<h4>Данные плательщика</h4>

<div class="form-group">
    <label class="col-md-3 control-label">Фамилия плательщика</label>

    <div class="col-md-3">
        <input type="text" class="form-control" ng-model="payment.parameters.pdrLastName"/>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Имя плательщика</label>

    <div class="col-md-3">
        <input type="text" class="form-control" ng-model="payment.parameters.pdrFirstName"/>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Отчество плательщика</label>

    <div class="col-md-3">
        <input type="text" class="form-control" ng-model="payment.parameters.pdrMiddleName"/>
    </div>
</div>

<h4>Документ, удостоверяющий личность</h4>

<div class="form-group">
    <label class="col-md-3 control-label">Серия и номер паспорта</label>

    <div class="col-md-3">
        <input type="text" class="form-control" ng-model="payment.parameters.pdrDocNumber" />
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Дата выдачи паспорта</label>

    <div class="col-md-3">
        <div class="input-group">
            <input type="text" class="form-control" ng-model="payment.parameters.pdrDocIssueDay" input-mask="{mask: '99'}"/>
            <span class="input-group-addon">.</span>
            <input type="text" class="form-control" ng-model="payment.parameters.pdrDocIssueMonth" input-mask="{mask: '99'}"/>
            <span class="input-group-addon">.</span>
            <input type="text" class="form-control" ng-model="payment.parameters.pdrDocIssueYear" input-mask="{mask: '9999'}"/>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Кем выдан паспорт</label>

    <div class="col-md-3">
        <input type="text" class="form-control" ng-model="payment.parameters.pdrDocIssuedBy"/>
    </div>
</div>

<h4>Персональные данные</h4>

<div class="form-group">
    <label class="col-md-3 control-label">Номер телефона плательщика</label>

    <div class="col-md-3">
        <input type="text" class="form-control" ng-model="payment.parameters.smsPhoneNumber_local"
               input-mask="{mask: '+7 999 999-99-99'}"/>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Дата рождения</label>

    <div class="col-md-3">
        <input type="text" class="form-control" ng-model="payment.parameters.pdrBirthDate" input-mask="{mask: '99.99.9999'}"/>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Место рождения</label>

    <div class="col-md-3">
        <input type="text" class="form-control" ng-model="payment.parameters.pdrBirthPlace"/>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Город регистрации плательщика</label>

    <div class="col-md-3">
        <input type="text" class="form-control" ng-model="payment.parameters.pdrCity"/>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Адрес регистрации</label>

    <div class="col-md-3">
        <input type="text" class="form-control" ng-model="payment.parameters.pdrAddress"/>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Почтовый индекс</label>

    <div class="col-md-3">
        <input type="text" class="form-control" ng-model="payment.parameters.pdrPostcode"/>
    </div>
</div>

<h4>Сумма платежа</h4>
