<?php

/**
 * @var \yii\web\View $this
 */

use ms\loyalty\prizes\payments\common\models\Payment;
use ms\loyalty\prizes\payments\common\models\Settings;

\ms\loyalty\prizes\payments\frontend\widgets\assets\PaymentsDashboardAsset::register($this);
?>

<div ng-controller="PaymentsController" class="payments-application" id="payments-application">

  <div class="alert alert-danger" ng-show="errors.length > 0" ng-cloak>
    <strong><i class="fa fa-exclamation-circle fa-lg"></i></strong>
      <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Errors' : 'Обнаружены следующие ошибки' ?>:<br><br>
    <ul>
      <li ng-repeat="error in errors">{{ error.message }}</li>
    </ul>
  </div>


  <div class="row">
    <div class="col-md-8">
      <h4><?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Details' : 'Реквизиты' ?></h4>

      <form role="form" class="form-horizontal" ng-cloak>

          <?php foreach (Payment::getTypesOrdered() as $type): ?>
              <?= $this->render("$type.php") ?>
          <?php endforeach ?>

        <div class="requisites--summ">
          <label class="control-label">
              <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Amount' : 'Сумма оплаты' ?>
          </label>

          <div class="input-group">
            <input type="number" min="1" class="form-control" required=""
                   ng-model="amount"
                   ng-change="calculateCommission()"/>
            <span class="input-group-addon"><i class="fa fa-rub"></i></span>
          </div>
        </div>

        <div class="form-group" ng-if="commission > 0 && bonuses > 0">
          <label class="col-md-3 control-label">
              <?php if (Settings::isCommissionRevert()): ?>
                  <?= Yii::t('loyalty', 'Get amount') ?>
              <?php else: ?>
                  <?= Yii::t('loyalty', 'Summary amount') ?>
              <?php endif ?>
          </label>

          <div class="col-md-3">
            <div class="input-group">
              <p class="form-control-static">
                {{ bonuses }}
                <small ng-if="commissionAsString">
                  ({{ commissionAsString }}
                    <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'taxes' : 'комиссии' ?>)
                </small>
              </p>
            </div>
          </div>
        </div>


        <button style="margin-top: 15px;" type="button" class="btn btn-requisites btn_blue" ng-click="makePayment()">
          <i class="fa fa-rub" ng-if="! disabled"></i>
          <i class="fa fa-spin fa-spinner" ng-if="disabled"></i>
            <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Pay' : 'Оплатить' ?>
        </button>

      </form>
    </div>


    <div class="col-md-4">
      <h4>
          <?php
          /*echo \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Payment type' : 'Метод перевода';*/
          ?>
      </h4>
      <!--закомментил, по желанию заказчика-->
        <?php if (false): ?>
          <ul class="nav nav-tabs nav-stacked nav-alternate" role="tablist" ng-cloak>
            <li role="presentation"
                ng-repeat="type in paymentTypes"
                ng-class="{ active: current == type.type }"
            >
              <a role="tab" data-toggle="tab" ng-click="select(type.type)">
                {{ type.title }}
              </a>
            </li>
          </ul>
        <?php endif; ?>
    </div>


  </div>


  <div class="modal fade" id="modal-payment-success">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title">
              <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Payment processed' : 'Платеж успешно принят' ?>
          </h4>
        </div>

        <div class="modal-body">
          <strong><?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Congratulations!' : 'Поздравляем!' ?></strong>

            <?php if (\Yii::$app->session->get('lang', 'ru') == 'en'): ?>
              <p>Your payment has been processed and in short time amount
                <nobr>{{ payment.amount }} <i class="fa fa-rub"></i></nobr>
                will be added to your account
              </p>
              <p>Your balance has been lowered for {{ payment.amount + commission }} <i class="fa fa-rub"></i>.</p>
            <?php else: ?>
              <p>Ваш платеж успешно принят и в скором времени денежные средства в размере
                <nobr>{{ payment.amount }} <i class="fa fa-rub"></i></nobr>
                поступят на Ваш счет
              </p>
              <p>С Вашего бонусного счета были списаны {{ payment.amount + commission
                }} <?= Yii::t('loyalty', 'баллов') ?>.</p>
            <?php endif; ?>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">
              <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Close' : 'Закрыть' ?>
          </button>
        </div>
      </div>
    </div>
  </div>

    <?php if (YII_ENV_DEV): ?>
      {{ payment }} {{ commission }} {{ payment.amount + commission }}
    <?php endif ?>
</div>
