<div ng-show="current == 'webmoney'" ng-if="enabled('webmoney')">
	<div class="form-group">
		<label class="col-md-3 control-label">Номер счета</label>
		<div class="col-md-3">
			<div class="input-group">
				<input type="text" class="form-control"
					   style="width:194px; border-radius:4px;"
					   ng-model="payment.parameters.phone_mobile"
					   input-mask="{mask: '999999999999'}"/>
			</div>
		</div>
	</div>
</div>