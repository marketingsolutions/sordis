<div ng-show="current == 'qiwi'" ng-if="enabled('qiwi')">
	<div class="form-group">
		<label class="col-md-3 control-label">Номер счета</label>

		<div class="col-md-3">
			<input type="text" class="form-control"
				   style="width:194px; border-radius:4px;"
				   ng-model="payment.parameters.phone_mobile_local"
				   input-mask="{mask: '+7 999 999-99-99'}"/>
		</div>
	</div>
</div>