<div ng-show="current == 'phone'" ng-if="enabled('phone')" class="requisites--phone">
  <label class=" control-label">
    <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Mobile phone number' : 'Номер мобильного телефона' ?>
  </label>
  <input type="text" class="form-control"  ng-model="payment.parameters.phone_mobile_local"/>
</div>
