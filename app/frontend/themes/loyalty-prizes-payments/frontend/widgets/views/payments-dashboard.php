<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var bool $isAllowPayments
 * @var string $title
 */
?>
<div class="col-md-12">
    <h3 class="header_top">
        <?= \Yii::$app->session->get('lang', 'ru') == 'en'
            ? 'Payments to mobile and wallets'
            : 'Перевод на мобильные телефоны' ?>
    </h3>
</div>
<div class="col-md-6">
    <div class="panel panel_ferma">
			<?php if ($isAllowPayments): ?>
				<?= $this->render('partials/_app') ?>
			<?php else: ?>
				<i>
					<?= \Yii::$app->session->get('lang', 'ru') == 'en'
						? 'In order to make a payment you should add NDFL profile'
						: 'Совершение платежей невозможно, так как Вы не заполнили анкету НДФЛ' ?>
				</i>
			<?php endif ?>

	</div>
</div>
<div class="col-md-6">
    <!--Список платежей-->
    <div class="pull-left">
        <a class="btn btn-primary btn_all" href="<?= Url::to(['/payments/payments/index']) ?>">
            <?= \Yii::$app->session->get('lang', 'ru') == 'en' ? 'My payments' : 'Список платежей' ?>
        </a>
    </div>
    <!--Список платежей КОНЕЦ-->
</div>