<?php

use ms\loyalty\prizes\payments\common\models\Payment;
use yii\grid\GridView;
use yii\helpers\Html;
use yz\icons\Icons;

/* @var $this yii\web\View */
/* @var $searchModel ms\loyalty\prizes\payments\frontend\models\PaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->params['header'] = 'Список платежей';
//$this->title = \Yii::$app->session->get('lang', 'ru') == 'en' ? 'My payments' : 'Список платежей';
$this->params['breadcrumbs'][] = 'Список платежей';

\yz\icons\FontAwesomeAsset::register($this);
?>
<div class="row container">
  <h2>Платежи</h2>
  <div class="panel panel_ferma payments-list">

      <?= GridView::widget([
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'columns' => [
              'id',
//				[
//					'attribute' => 'type',
//					'value' => function (Payment $data) {
//						return Payment::getTypeValues()[$data->type];
//					}
//				],
              [
                  'attribute' => 'payment_parameters_raw',
                  'label' => 'Номер телефона',
                  'value' => function (Payment $data) {
                      $phoneNumber = json_decode($data->payment_parameters_raw);
                      if (isset($phoneNumber->phone_mobile)) {
                          return $phoneNumber->phone_mobile;
                      }
                      return null;
                  }
              ],
              [
                  'attribute' => 'status',
                  'value' => function (Payment $data) {
                      return Payment::getStatusValues()[$data->status];
                  }
              ],
              [
                  'attribute' => 'payment_amount',
                  'format' => 'raw',
                  'value' => function (Payment $data) {
                      return $data->payment_amount . ' ' . Icons::i('rub');
                  }
              ],
              [
                  'attribute' => 'commission_amount',
                  'format' => 'raw',
                  'value' => function (Payment $data) {
                      return $data->commission_amount . ' ' . Icons::i('rub');
                  }
              ],
              [
                  'attribute' => 'amount',
                  'format' => 'raw',
                  'value' => function (Payment $data) {
                      return $data->amount . ' ' . Icons::i('rub');
                  }
              ],
              // 'payment_parameters_raw:ntext',
              'created_at',
              // 'updated_at',

              [
                  'class' => 'yii\grid\ActionColumn',
                  'template' => '{view}',
                  'buttons' => [
                      'view' => function ($url, $model, $key) {
                          $options = [
                              'title' => Yii::t('yii', 'View'),
                              'aria-label' => Yii::t('yii', 'View'),
                              'data-pjax' => '0',
                              'class' => 'btn btn-default',
                          ];
                          return Html::a(Icons::i('eye'), $url, $options);
                      }
                  ]
              ],
          ],
      ]); ?>

  </div>


