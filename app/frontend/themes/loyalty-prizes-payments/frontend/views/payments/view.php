<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use ms\loyalty\prizes\payments\common\models\Payment;

/* @var $this yii\web\View */
/* @var $model ms\loyalty\prizes\payments\common\models\Payment */

$this->title = \Yii::$app->session->get('lang', 'ru') == 'en' ? 'Payment #'.$model->id : 'Платеж №'.$model->id;
$this->params['breadcrumbs'][] = ['label' => \Yii::$app->session->get('lang', 'ru') == 'en' ? 'My payments' : 'Список платежей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="payment-view panel">

				<h1><?= Html::encode($this->title) ?></h1>

				<?= DetailView::widget([
					'model' => $model,
					'attributes' => [
						'id',
						[
							'attribute' => 'type',
							'value' => \ms\loyalty\prizes\payments\common\models\Payment::getTypeValues()[$model->type]
						],
						[
							'attribute' => 'status',
							'value' => \ms\loyalty\prizes\payments\common\models\Payment::getStatusValues()[$model->status]
						],
                        [
                            'attribute' => 'payment_parameters_raw',
                            'label' => 'Номер телефона',
                            'value' => function(Payment $data){
                                $phoneNumber = json_decode($data->payment_parameters_raw);
                                if(isset($phoneNumber->phone_mobile)){
                                    return $phoneNumber->phone_mobile;
                                }
                                return null;
                            }
                        ],
						'payment_amount',
						'commission_amount',
						'amount',
						'created_at:datetime',
					],
				]) ?>

			</div>
		</div>
	</div>
</div>
