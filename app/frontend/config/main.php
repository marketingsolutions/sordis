<?php

return [
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'cookieValidationKey' => getenv('FRONTEND_COOKIE_VALIDATION_KEY'),
        ],
        'session' => [
            'name' => 'MSSESSIDFRONT',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [

            ],
        ],
        'user' => [
            'identityClass' => \ms\loyalty\identity\phonesEmails\common\models\Identity::class,
            'loginUrl' => ['/identity/auth/login'],
        ],
        'assetManager' => [
            'appendTimestamp' => true,
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@vendor/marketingsolutions/loyalty-identity-phones-emails' => '@app/themes/loyalty-identity-phones-emails',
                    '@vendor/marketingsolutions/loyalty-feedback' => '@app/themes/loyalty-feedback',
                    '@vendor/marketingsolutions/loyalty-prizes-payments' => '@app/themes/loyalty-prizes-payments',
                    '@vendor/marketingsolutions/loyalty-certificates-catalog' => '@app/themes/loyalty-certificates-catalog'
                ]
            ]
        ]
    ],
    'modules' => [
        'profiles' => [
            'class' => modules\profiles\frontend\Module::class,
        ],
        'identity' => [
            'class' => modules\identity\frontend\Module::class,
        ],
        'sales' => [
            'class' => modules\sales\frontend\Module::class,
        ],
        'codes' => [
            'class' => \ms\loyalty\bonuses\codes\frontend\Module::class,
        ],
        'bonuses' => [
            'class' => \modules\bonuses\frontend\Module::class,
        ],
        'catalog' => [
            'class' => ms\loyalty\catalog\frontend\Module::class,
        ],
        'rsbcards' => [
            'class' => ms\loyalty\rsb\cards\frontend\Module::class,
        ],
        'feedback' => [
            'class' => modules\feedback\frontend\Module::class,
        ],
        'files-attachments' => [
            'class' => \ms\files\attachments\frontend\Module::class,
        ],
        'courses' => [
            'class' => \modules\courses\frontend\Module::class,
        ],
        'taxes' => [
            'class' => \ms\loyalty\taxes\frontend\Module::class,
        ],
        'payments' => [
            'class' => ms\loyalty\prizes\payments\frontend\Module::class,
        ],
        'like' => [
            'class' => halumein\like\Module::class,
        ],
        'news' => [
            'class' => \modules\news\frontend\Module::class,
        ],
        'social' => [
            'class' => modules\social\frontend\Module::class,
        ],
        'calc' => [
            'class' => modules\calc\frontend\Module::class,
        ],
        'files' =>[

        ],
        'polls' => [
            'class' => \modules\polls\frontend\Module::class,
        ],
        'pictures' => [
            'class' => \modules\pictures\frontend\Module::class,
        ],
    ],
    'params' => [
        'defaultTitle' => 'Мотивационная программа',
    ],
];
