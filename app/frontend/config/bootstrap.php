<?php

use ms\loyalty\bonuses\codes\frontend\api\v3\controllers\CodesController;

$listener = new \marketingsolutions\events\Listener(
    new \marketingsolutions\events\PatternEventsProvider(),
    new \marketingsolutions\events\PrefixMethodFinder()
);

\Yii::$container->set(
    \ms\loyalty\contracts\prizes\PrizeRecipientInterface::class,
    function() {
        if (Yii::$app->user->isGuest) {
            header('Location: /');
            exit;
        }

        return Yii::$app->user->identity->profile;
    }
);

/**
 * Profile
 */
$listener->bind(
    \ms\loyalty\theme\frontend\widgets\InterfaceWidgets::class,
    \modules\profiles\frontend\listeners\InterfaceWidgets::class
);

/**
 * Identity
 */
$listener->bind(
    \ms\loyalty\theme\frontend\widgets\InterfaceWidgets::class,
    \ms\loyalty\identity\phonesEmails\frontend\listeners\InterfaceWidgets::class
);

/**
 * Certificates catalog
 */
$listener->bind(
    \ms\loyalty\theme\frontend\widgets\InterfaceWidgets::class,
    \ms\loyalty\catalog\frontend\listeners\InterfaceWidgets::class
);

/**
 * Payments
 */
$listener->bind(
    \ms\loyalty\theme\frontend\widgets\InterfaceWidgets::class,
    \ms\loyalty\prizes\payments\frontend\listeners\InterfaceWidgets::class
);

/**
 * Feedback
 */
$listener->bind(
    \ms\loyalty\theme\frontend\widgets\InterfaceWidgets::class,
    \ms\loyalty\feedback\frontend\listeners\InterfaceWidgets::class
);

/**
 * Sales
 */
$listener->bind(
    \ms\loyalty\theme\frontend\widgets\InterfaceWidgets::class,
    \modules\sales\frontend\listeners\InterfaceWidgets::class
);

/**
 * Codes
 */
$listener->bind(
    \ms\loyalty\theme\frontend\widgets\InterfaceWidgets::class,
    \ms\loyalty\bonuses\codes\frontend\listeners\InterfaceWidgets::class
);

\yii\base\Event::on(
    CodesController::class,
    CodesController::EVENT_BEFORE_ACTION,
    [\frontend\listeners\CodesControllerListener::class, 'beforeCreate']
);

/**
 * Login form
 */
$listener->bind(
    \ms\loyalty\identity\phonesEmails\frontend\forms\LoginForm::class,
    \frontend\listeners\LoginFormListener::class
);

// check company balance for Order
\yii\base\Event::on(
	\ms\loyalty\catalog\frontend\forms\AddToCartForm::class,
	\ms\loyalty\catalog\frontend\forms\AddToCartForm::EVENT_BEFORE_VALIDATE,
	[\frontend\listeners\CartControllerListener::class, 'cartAdd']
);

// check company balance for Payment
\yii\base\Event::on(
	\ms\loyalty\prizes\payments\frontend\controllers\api\PaymentsController::class,
	\ms\loyalty\prizes\payments\frontend\controllers\api\PaymentsController::EVENT_BEFORE_ACTION,
	[\frontend\listeners\PaymentsControllerListener::class, 'beforeCreate']
);

\yii\base\Event::on(
	\ms\loyalty\prizes\payments\common\models\Payment::class,
	\ms\loyalty\prizes\payments\common\models\Payment::EVENT_BEFORE_INSERT,
	[\frontend\listeners\PaymentsControllerListener::class, 'beforeInsert']
);



