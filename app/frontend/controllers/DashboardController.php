<?php

namespace frontend\controllers;
use Imagine\Image\Profile;
use modules\courses\common\models\Course;
use modules\courses\common\models\CourseTest;
use modules\news\common\models\News;
use modules\profiles\common\models\Dealer;
use modules\profiles\common\models\PremiumHistory;
use modules\profiles\frontend\forms\DiscountForm;
use modules\profiles\frontend\forms\PremiumForm;
use modules\profiles\frontend\models\DiscountHistorySearch;
use modules\profiles\frontend\models\DiscountSearch;
use modules\profiles\frontend\models\PremiumHistorySearch;
use modules\profiles\frontend\models\PremiumSearch;
use modules\sales\common\models\Competition;
use modules\sales\common\models\Product;
use ms\loyalty\taxes\backend\grid\TaxesAccountColumn;
use ms\loyalty\taxes\common\models\AccountProfile;
use yii\web\Controller;
use yii\filters\AccessControl;
use Yii;



/**
 * Class DashboardController
 */
class DashboardController extends Controller
{
    /**
     * @var
     */
    private $recipient;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        /** @var \modules\profiles\common\models\Profile $profile */
        $profile = \Yii::$app->user->identity->profile;
        $sliderNews = News::getSliderNews();

        return $this->render('index', [
            'profile' => $profile,
            'sliderNews' => $sliderNews,
        ]);
    }

    public function actionPrizes()
    {
        $profile = \Yii::$app->user->identity->profile;
        return $this->render('prizes', [
            'profile' => $profile,
        ]);
    }


    /**
     * Добавление скидки
     * @return \yii\web\Response
     */
    public function actionAddDiscount()
    {

        $model = new DiscountForm();

        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->process()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Скидка списана успешно!'));
        }else{
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_ERROR, $model->getErrors('discountSum')[0]);
        }
        return $this->redirect(['prizes']);
    }


    /**
     * Добавление премии
     * @return \yii\web\Response
     */
    public function actionAddPremium()
    {
        $model = new PremiumForm();

        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->process()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Премия списана успешно!'));
        }else{
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_ERROR, $model->getErrors('premiumSum')[0]);
        }
        return $this->redirect(['prizes']);
    }

    public function actionPremium()
    {
        $searchModel = new PremiumSearch($this->recipient);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('premium', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDiscount()
    {
        $searchModel = new DiscountSearch($this->recipient);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('discount', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDiscountHistory()
    {
        $searchModel = new DiscountHistorySearch($this->recipient);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('discount_history', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPremiumHistory()
    {
        $searchModel = new PremiumHistorySearch($this->recipient);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('premium_history', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}