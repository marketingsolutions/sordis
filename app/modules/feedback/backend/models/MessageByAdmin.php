<?php

namespace modules\feedback\backend\models;
use modules\feedback\common\models\Message;


/**
 * Class MessageByAdmin
 */
class MessageByAdmin extends Message
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['from_admin_user_id', 'in', 'range' => array_keys(self::getFromAdminUserIdValues())],
        ]);
    }

}