<?php

namespace modules\feedback\backend\models;

use modules\dealers\common\models\Dealer;
use modules\feedback\common\models\Message;
use marketingsolutions\datetime\DateTimeBehavior;
use marketingsolutions\datetime\DateTimeRangeBehavior;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\models\User;
use yz\admin\search\SearchModelInterface;

/**
 * MessageSearch represents the model behind the search form about `ms\loyalty\feedback\common\models\Message`.
 */
class MessageSearch extends Message implements SearchModelInterface
{
    public $created_at_start;
    public $created_at_end;
    public $categoryName;

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'datetime' => [
                'class' => DateTimeBehavior::className(),
                'originalFormat' => ['date', 'yyyy-MM-dd'],
                'targetFormat' => ['date', 'dd.MM.yyyy'],
                'attributes' => [
                    'created_at_start',
                    'created_at_end',
                ]
            ],
            'created_at_range' => [
                'class' => DateTimeRangeBehavior::className(),
                'startAttribute' => 'created_at_start_local',
                'endAttribute' => 'created_at_end_local',
                'targetAttribute' => 'created_at_range',
            ]
        ]);
    }

    public function init()
    {
        parent::init();

        if ($this->created_at_start === null) {
            $this->created_at_start = self::find()->min('DATE(created_at)');
        }
        if ($this->created_at_end === null) {
            $this->created_at_end = self::find()->max('DATE(created_at)');
        }
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['name', 'email', 'phone_mobile', 'content', 'created_at', 'updated_at', 'categoryName'], 'safe'],
            [['is_processed', 'from_admin_user_id'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'user.dealer.name' => 'Дилер',
            'user.phone_mobile' => 'Номер телефона',
            'user.fullName' => 'Участник',
        ]);
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();

        $dataProvider = $this->prepareDataProvider($query);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $this->prepareFilters($query);

        return $dataProvider;
    }

    protected function getQuery()
    {
        return Message::find()
            ->joinWith('category');
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();

        $query->from(['message' => Message::tableName()]);
        $query
            ->joinWith([
                'fromAdminUser' => function (ActiveQuery $query) {
                    $query->from(['from_admin_user' => User::tableName()]);
                }
            ]);
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['created_at' => 'DESC']
            ]
        ]);
        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'categoryName' => [
                'asc' => ['{{%feedback_categories}}.name' => SORT_ASC],
                'desc' => ['{{%feedback_categories}}.name' => SORT_DESC],
            ],
        ]);
        $dataProvider->setSort($sort);

        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'message.id' => $this->id,
            'message.user_id' => $this->user_id,
            'updated_at' => $this->updated_at,
            'is_processed' => $this->is_processed,
            'from_admin_user_id' => $this->from_admin_user_id,
        ]);

        $query->andFilterWhere(['like', 'message.name', $this->name])
            ->andFilterWhere(['like', 'message.email', $this->email])
            ->andFilterWhere(['like', 'message.phone_mobile', $this->phone_mobile])
            ->andFilterWhere(['like', '{{%feedback_categories}}.name', $this->categoryName])
            ->andFilterWhere(['like', 'message.content', $this->content]);

        $query->andFilterWhere(['between', 'DATE(message.created_at)', $this->created_at_start, $this->created_at_end]);
    }
}
