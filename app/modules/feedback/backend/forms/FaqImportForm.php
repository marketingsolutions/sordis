<?php

namespace modules\feedback\backend\forms;

use modules\feedback\common\models\Faq;
use yii\helpers\ArrayHelper;
use yz\admin\import\ImportForm;
use yz\admin\import\InterruptImportException;


/**
 * Class FaqImportForm
 */
class FaqImportForm extends ImportForm
{
    const FIELD_ID = 'id';
    const FIELD_QUESTION = 'вопрос';
    const FIELD_ANSWER = 'ответ';
    const FIELD_PUBLISHED = 'опубликован';
    const FIELD_ORDER = 'порядок_сортировки';

    public function init()
    {
        $this->availableFields = [
            self::FIELD_ID => 'ID вопроса (не импортируется)',
            self::FIELD_QUESTION => 'Вопрос',
            self::FIELD_ANSWER => 'Ответ',
            self::FIELD_PUBLISHED => 'Опубликовать данный вопрос на сайте',
            self::FIELD_ORDER => 'Порядок отображения на сайте',
        ];

        parent::init();
    }

    protected function rowImport($row)
    {
        $model = new Faq();

        $isPublished = ArrayHelper::getValue($row, self::FIELD_PUBLISHED);

        $model->question = base64_decode(ArrayHelper::getValue($row, self::FIELD_QUESTION));
        $model->answer = base64_decode(ArrayHelper::getValue($row, self::FIELD_ANSWER));
        $model->is_published =(mb_strtolower($isPublished, 'utf-8') == 'да' || $isPublished == '1');
        $model->order = ArrayHelper::getValue($row, self::FIELD_ORDER);

        if ($model->save() == false) {
            throw new InterruptImportException('Ошибка при импорте вопроса: '.implode('; ', $model->getFirstErrors()), $row);
        }
    }
}