<?php

namespace modules\feedback\backend\controllers;

use yii\web\Controller;
use modules\feedback\backend\models\MessageSearch;
use modules\feedback\common\models\Comment;
use modules\feedback\common\models\Message;
use modules\feedback\common\models\Reply;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\web\NotFoundHttpException;
use yz\admin\actions\ExportAction;
use yz\admin\contracts\AccessControlInterface;
use yz\admin\grid\columns\DataColumn;
use yz\admin\grid\filters\DateRangeFilter;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\Yz;
use ms\files\attachments\common\traits\AttachedFilesProcessingTrait;

/**
 * MessagesController implements the CRUD actions for Message model.
 */
class MessagesController extends Controller implements AccessControlInterface
{
    use AttachedFilesProcessingTrait, CrudTrait, CheckAccessTrait;

    const ACTION_SEND_REPLY = 'sendReply';
    const ACTION_ADD_COMMENT = 'addComment';

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'dataProvider' => function ($params) {
                    $searchModel = Yii::createObject(MessageSearch::class);
                    $dataProvider = $searchModel->search($params);
                    return $dataProvider;
                },
            ]
        ]);
    }

    /**
     * Lists all Message models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = Yii::createObject(MessageSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns(),
        ]);
    }

    public function getGridColumns()
    {
        return [
            'id',
            [
                'attribute' => 'is_processed',
                'filter' => Message::getIsProcessedValues(),
                'value' => function (Message $data) {
                    return $data->is_processed == 1 ? 'processed' : 'notProcessed';
                },
                'titles' => [
                    'processed' => 'Обработан',
                    'notProcessed' => 'Необработан',
                ],
                'labels' => [
                    'processed' => DataColumn::LABEL_SUCCESS,
                    'notProcessed' => DataColumn::LABEL_DANGER,
                ]
            ],
            [
                'attribute' => 'from_admin_user_id',
                'filter' => Message::getFromAdminUserIdValues(),
                'value' => function (Message $data) {
                    if ($data->from_admin_user_id == null) {
                        return 'Сайт';
                    } else {
                        return $data->fromAdminUser->name;
                    }
                }
            ],
            'name',
            'email:email',
            [
                'attribute' => 'phone_mobile',
                'value' => 'phone_mobile_local',
            ],
            [
                'attribute' => 'categoryName',
                'label' => 'Категория сообщения',

            ],
            [
                'attribute' => 'content',
                'format' => 'raw',
                'value' => function(Message $message) {
                    return StringHelper::truncate(Yii::$app->formatter->asNtext($message->content), 200);
                }
            ],
            [
                'attribute' => 'created_at',
                'filter' => DateRangeFilter::instance(),
                'label' => 'Получено',
            ],
            'processed_at:datetime',
            'last_reply_sent_at:datetime',
            [
                'attribute' => 'adminUser.name',
                'header' => 'Администратор',
            ],
            // 'updated_at:datetime',
        ];
    }

    /**
     * Lists all Message models.
     * @return mixed
     */
    public function actionIndexView()
    {
        $searchModel = Yii::createObject(MessageSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index-view', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns(),
        ]);
    }

    /**
     * Updates an existing Message model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = Message::SCENARIO_ADMIN_EDIT;
        $model->currentAdminUser = Yii::$app->user->identity;

        if ($model->email) {
            $reply = new Reply();
            $reply->message_id = $model->id;
        } else {
            $reply = null;
        }

        $comment = new Comment([
            'message_id' => $model->id,
        ]);

        $renderUpdate = function() use ($model, $reply, $comment) {
            return $this->render('update', [
                'model' => $model,
                'reply' => $reply,
                'comment' => $comment,
            ]);
        };

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
            return $this->getCreateUpdateResponse($model, [
                self::ACTION_SEND_REPLY => function () use ($model, $reply, $renderUpdate) {
                    if ($reply && $reply->load(Yii::$app->request->post()) && $reply->saveReply(Yii::$app->user->identity)) {
                        $this->attachFilesToModel($reply, \Yii::$app->request->post());
                        if ($reply->send()) {
                            Yii::$app->session->setFlash(Yz::FLASH_INFO, 'Сообщение успешно отправлено');
                        } else {
                            Yii::$app->session->setFlash(Yz::FLASH_WARNING, 'Сообщение не отправлено');
                        }
                        return $this->redirect(['update', 'id' => $model->getPrimaryKey()]);
                    }

                    return $renderUpdate();
                },
                self::ACTION_ADD_COMMENT => function () use ($model, $comment, $renderUpdate) {
                    if ($comment->load(Yii::$app->request->post()) && $comment->add(Yii::$app->user->identity)) {
                        Yii::$app->session->setFlash(Yz::FLASH_INFO, 'Комментарий добавлен');
                        return $this->redirect(['update', 'id' => $model->getPrimaryKey()]);
                    }

                    return $renderUpdate();
                }
            ]);
        } else {
            return $renderUpdate();
        }
    }

    public function actionAnswer($id)
    {
        $model = $this->findModel($id);
        $model->scenario = Message::SCENARIO_ADMIN_EDIT;
        $model->currentAdminUser = Yii::$app->user->identity;

        if ($model->email) {
            $reply = new Reply();
            $reply->message_id = $model->id;
        } else {
            $reply = null;
        }

        $comment = new Comment([
            'message_id' => $model->id,
        ]);

        $renderUpdate = function() use ($model, $reply, $comment) {
            return $this->render('update', [
                'model' => $model,
                'reply' => $reply,
                'comment' => $comment,
            ]);
        };

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
            return $this->getCreateUpdateResponse($model, [
                self::ACTION_SEND_REPLY => function () use ($model, $reply, $renderUpdate) {
                    if ($reply && $reply->load(Yii::$app->request->post()) && $reply->saveReply(Yii::$app->user->identity)) {
                        $this->attachFilesToModel($reply, \Yii::$app->request->post());
                        if ($reply->send()) {
                            Yii::$app->session->setFlash(Yz::FLASH_INFO, 'Сообщение успешно отправлено');
                        } else {
                            Yii::$app->session->setFlash(Yz::FLASH_WARNING, 'Сообщение не отправлено');
                        }
                        return $this->redirect(['update', 'id' => $model->getPrimaryKey()]);
                    }

                    return $renderUpdate();
                },
                self::ACTION_ADD_COMMENT => function () use ($model, $comment, $renderUpdate) {
                    if ($comment->load(Yii::$app->request->post()) && $comment->add(Yii::$app->user->identity)) {
                        Yii::$app->session->setFlash(Yz::FLASH_INFO, 'Комментарий добавлен');
                        return $this->redirect(['update', 'id' => $model->getPrimaryKey()]);
                    }

                    return $renderUpdate();
                }
            ]);
        } else {
            return $renderUpdate();
        }
    }

    /**
     * Finds the Message model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Message the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Message::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Updates an existing Message model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model->scenario = Message::SCENARIO_ADMIN_EDIT;

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Message model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }
}
