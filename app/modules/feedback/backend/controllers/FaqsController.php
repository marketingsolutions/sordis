<?php

namespace modules\feedback\backend\controllers;

use modules\feedback\backend\forms\FaqImportForm;
use modules\feedback\backend\models\FaqSearch;
use modules\feedback\common\models\Faq;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yz\admin\actions\ExportAction;
use yz\admin\contracts\AccessControlInterface;
use yz\admin\import\BatchImportAction;
use yz\admin\import\ImportForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;

/**
 * FaqsController implements the CRUD actions for Faq model.
 */
class FaqsController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function ($params) {
                    /** @var FaqSearch $searchModel */
                    return Yii::createObject(FaqSearch::className());
                },
                'dataProvider' => function ($params, FaqSearch $searchModel) {
                    $dataProvider = $searchModel->search($params);
                    return $dataProvider;
                },
            ],
            'export-transfer' => [
                'class' => ExportAction::className(),
                'reportName' => 'transfer',
                'searchModel' => function ($params) {
                    /** @var FaqSearch $searchModel */
                    return Yii::createObject(FaqSearch::className());
                },
                'dataProvider' => function ($params, FaqSearch $searchModel) {
                    $dataProvider = $searchModel->search($params);
                    return $dataProvider;
                },
                'gridColumns' => function (FaqSearch $searchModel) {
                    return [
                        'id',
                        [
                            'attribute' => 'question',
                            'value' => function (Faq $faq) {
                                return base64_encode($faq->question);
                            }
                        ],
                        [
                            'attribute' => 'answer',
                            'value' => function (Faq $faq) {
                                return base64_encode($faq->answer);
                            }
                        ],
                        'is_published:boolean',
                        'order',
                        // 'created_at:datetime',
                        // 'updated_at:datetime',
                    ];
                },
            ],
            'import' => [
                'class' => BatchImportAction::class,
                'importConfig' => [
                    'class' => FaqImportForm::class,
                ]
            ]
        ]);
    }

    /**
     * Lists all Faq models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var FaqSearch $searchModel */
        $searchModel = Yii::createObject(FaqSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(FaqSearch $searchModel)
    {
        return [
            'id',
            'question',
            'answer:raw',
            'is_published:boolean',
            'order',
            // 'created_at:datetime',
            // 'updated_at:datetime',
        ];
    }

    /**
     * Creates a new Faq model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Faq;
        $model->loadDefaultValues();

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
            return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Faq model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
            return $this->getCreateUpdateResponse($model);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionPrev($id)
    {
        $model = $this->findModel($id);
        $model->movePrev();
        return $this->goPreviousUrl();
    }

    public function actionNext($id)
    {
        $model = $this->findModel($id);
        $model->moveNext();
        return $this->goPreviousUrl();
    }

    /**
     * Finds the Faq model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Faq the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Faq::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing Faq model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }
}
