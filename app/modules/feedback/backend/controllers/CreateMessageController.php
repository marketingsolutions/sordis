<?php

namespace modules\feedback\backend\controllers;

use yii\web\Controller;
use modules\feedback\backend\models\MessageByAdmin;
use modules\feedback\common\models\Message;
use yii\helpers\Url;
use yz\admin\contracts\AccessControlInterface;
use yz\Yz;

/**
 * Class CreateMessageController
 */
class CreateMessageController extends Controller implements AccessControlInterface
{
    const PERMISSION_CHANGE_FROM_ADMIN_USER_ID = 'Feedback.CreateMessage.changeFromAdminUserId';

    public function actionIndex()
    {
        $model = new MessageByAdmin();
        $model->scenario = MessageByAdmin::SCENARIO_ADMIN_EDIT;

        $model->from_admin_user_id = \Yii::$app->user->id;

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            $model->sendToAdmin();
            \Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Сообщение успешно добавлено');
            return $this->redirect(Url::home());
        }

        return $this->render('index', [
            'model' => $model,
            'canChangeFromUserId' => \Yii::$app->user->can(self::PERMISSION_CHANGE_FROM_ADMIN_USER_ID),
        ]);
    }
}