<?php

use modules\feedback\common\models\Category;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\ActiveForm;
use yz\admin\widgets\FormBox;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var ms\loyalty\feedback\common\models\Message $model
 * @var ms\loyalty\feedback\common\models\Reply $reply
 * @var \ms\loyalty\feedback\common\models\Comment $comment
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php $box = FormBox::begin(['cssClass' => 'message-form box-primary', 'title' => '']) ?>
<?php $form = ActiveForm::begin(); ?>

<?php $box->beginBody() ?>
<div class="panel panel-default">
	<div class="panel-heading">Вопрос участника</div>
	<div class="panel-body">
        <?php if (Category::find()->exists()): ?>
            <?= $form->field($model, 'category_id')->dropDownList($model->getCategoryIdValues()) ?><?php endif ?>
        <?= $form->field($model, 'name')->staticInput() ?>
        <?= $form->field($model, 'phone_mobile_local')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '+7 999 999-99-99',
        ]) ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'content')->staticInput(['format' => 'nText']) ?>

		<div class="row">
			<label class="control-label col-sm-2" for="message-content">Прикрепленные документы</label>
			<div class="col-sm-8">
                <?= \ms\files\attachments\common\widgets\FileAttachmentsView::widget([
                    'attachmentsQuery' => $model->getAttachedFiles(),
                    'type' => 'userFile',
                ]) ?>
			</div>
		</div>

        <?php if (!empty($model->file)): ?>
			<div class="row">
				<label class="control-label col-sm-2" for="message-content">Прикрепленный файл</label>
				<div class="col-sm-8">
					<a href="<?= $model->file_url ?>" target="_blank" class="btn btn-default">
                        <?php if (strpos($model->file, '.pdf') !== false): ?>
							<i class="fa fa-file-pdf-o" style="font-size:80px; color: darkred"></i>
                        <?php else: ?>
							<img src="<?= $model->file_url ?>" style="max-width: 400px; max-height: 300px;"/>
                        <?php endif; ?>
					</a>
				</div>
			</div>
        <?php endif; ?>

        <?= $form->field($model, 'is_processed')->checkbox() ?>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">Комментарии модератора</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-offset-2 col-sm-8">
				<table class="table">
					<tr>
						<th class="col-sm-2">Дата ответа</th>
						<th class="col-sm-2">Администратор</th>
						<th class="col-sm-6">Ответ</th>
						<th class="col-sm-2">Файл</th>
					</tr>
                    <?php foreach ($model->replies as $previousReply): ?>
						<tr>
							<td><?= Yii::$app->formatter->asDatetime($previousReply->created_at) ?></td>
							<td><?= Html::encode($previousReply->adminUser->name) ?></td>
							<td style="background: beige; padding: 5px 10px; font-size: 16px;">
								<?= Yii::$app->formatter->asHtml($previousReply->reply) ?>
							</td>
							<td>
								<table>
                                    <?php foreach ($previousReply->getAttachedFiles()->all() as $attach): ?>
										<tr>
											<td>
												<a href="<?= Url::to(['/files-attachments/files/download', 'id' => $attach->id]) ?>">
                                                    <?= Html::encode($attach->original_name) ?>
												</a>
											</td>
										</tr>
                                    <?php endforeach ?>
								</table>
							</td>
						</tr>
                    <?php endforeach ?>
				</table>
			</div>
		</div>

        <?php if ($reply): ?>

            <?= $form->field($reply, 'reply')->tinyMce(['rows' => 5])->label('Новый ответ') ?>

			<div class="row form-group">
				<div class="col-md-8 col-md-offset-2">
					<label class="control-label" for="messageform-content">Прикрепить файл</label>
                    <?= \ms\files\attachments\common\widgets\FileAttachmentsEdit::widget([
                        'model' => $reply,
                        'options' => [
                            'itemClass' => 'col-md-4',
                        ],
                        'maximumFiles' => 3,
                    ]) ?>
				</div>
			</div>

			<div class="col-sm-offset-2">
                <?php echo Html::submitButton('Сохранить и отправить ответ на Email', [
                    'class' => 'btn btn-primary',
                    'name' => AdminHtml::ACTION_BUTTON_NAME,
                    'value' => \ms\loyalty\feedback\backend\controllers\MessagesController::ACTION_SEND_REPLY,
                ]) ?>
			</div>

        <?php else: ?>

			<p class="lead">Для отправки ответа участнику на электронную почту, укажите его email</p>

        <?php endif ?>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">Комментарии участника</div>
	<div class="panel-body">
		<table class="table">
			<tr>
				<th class="col-sm-2">Дата комментария</th>
				<th class="col-sm-10">Ответ</th>
			</tr>
            <?php foreach ($model->comments as $previousComment): ?>
				<tr>
					<td><?= Yii::$app->formatter->asDatetime($previousComment->created_at) ?></td>
					<td style="background: beige; padding: 5px 10px; font-size: 16px;">
                        <?= Yii::$app->formatter->asHtml($previousComment->comment) ?>
					</td>
				</tr>
            <?php endforeach ?>
		</table>
	</div>
</div>

<?php $box->endBody() ?>

<?php $box->actions([
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
]) ?>
<?php ActiveForm::end(); ?>

<?php FormBox::end() ?>
