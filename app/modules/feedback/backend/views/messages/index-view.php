<?php

use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var ms\loyalty\feedback\backend\models\MessageSearch $searchModel
 * @var array $columns
 */

$this->title = ms\loyalty\feedback\common\models\Message::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'message-index box-primary']) ?>
<div class="text-right">
    <?php echo ActionButtons::widget([
        'order' => [['export', 'return']],
        'gridId' => 'message-grid',
        'searchModel' => $searchModel,
        'modelClass' => 'ms\loyalty\feedback\common\models\Message',
    ]) ?>
</div>

<?= GridView::widget([
    'id' => 'message-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => array_merge([
        ['class' => 'yii\grid\CheckboxColumn'],
    ], $columns, [
        [
            'class' => 'yz\admin\widgets\ActionColumn',
            'template' => '{view}',
        ],
    ]),
]); ?>
<?php Box::end() ?>
