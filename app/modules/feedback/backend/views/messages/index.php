<?php

use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use modules\profiles\common\models\Profile;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var ms\loyalty\feedback\backend\models\MessageSearch $searchModel
 * @var array $columns
 */

$this->title = ms\loyalty\feedback\common\models\Message::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

$js=<<<JS
    $( document ).ready(function() {
    var stringMessageAllowId = $('#is_read_admin_this_message').val();
    var arrId = JSON.parse("[" + stringMessageAllowId + "]");
    var countNoRead = 0;
    $("tr").each(function (index, el){
        var trId = $(this).data('key');
        if($.inArray(trId, arrId) != -1){
            $(this).css('font-weight', 'bold');
            countNoRead++;
        }    
    });
    $('.countNoRead').text(countNoRead);
});
JS;
$this->registerJs($js);
?>
<input type="hidden" id="is_read_admin_this_message" value="<?=Profile::adminReadMessages(\Yii::$app->user->id)?>">
<?php $box = Box::begin(['cssClass' => 'message-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
			'order' => [['export', 'delete', 'return']],
            'gridId' => 'message-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'ms\loyalty\feedback\common\models\Message',
        ]) ?>
    </div>
    <div class="text-left">
        Всего непрочитанных сообщений: <b><span><?=Profile::getCountUnread(\Yii::$app->user->id)?></span></b>
    </div>

    <?= GridView::widget([
        'id' => 'message-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '{answer} {delete}',
                'buttons'  => [
                    'answer'   => function ($url, $model) {
                        return Html::a(\yz\icons\Icons::i('arrow-alt-circle-left fa-lg'), ['messages/answer', 'id' => $model->id], [
                            'title'     => 'Ответить',
                            'class'     => 'btn btn-info btn-sm',
                            'data-pjax' => '0',
                        ]);
                    },
                ],
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
