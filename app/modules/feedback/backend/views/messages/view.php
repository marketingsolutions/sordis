<?php

use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;

/**
 * @var yii\web\View $this
 * @var ms\loyalty\feedback\common\models\Message $model
 */
$this->title = \Yii::t('admin/t', 'Update {item}', ['item' => ms\loyalty\feedback\common\models\Message::modelTitle()]);
$this->params['breadcrumbs'][] = ['label' => ms\loyalty\feedback\common\models\Message::modelTitlePlural(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<div class="message-update">

    <div class="text-right">
        <?php  Box::begin() ?>
        <?=  ActionButtons::widget([
            'order' => [['return']],
            'addReturnUrl' => false,
        ]) ?>
        <?php  Box::end() ?>
    </div>

    <?php echo $this->render('_form_view', [
        'model' => $model,
    ]); ?>

</div>
