<?php

use modules\feedback\common\models\Category;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\ActiveForm;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;

/**
 * @var yii\web\View $this
 * @var ms\loyalty\feedback\common\models\Message $model
 * @var ms\loyalty\feedback\common\models\Reply $reply
 * @var bool $canChangeFromUserId
 */
$this->title = 'Добавление вопроса';
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<div class="message-update">

    <div class="text-right">
        <?php  Box::begin() ?>
        <?=  ActionButtons::widget([
            'order' => [['index', 'update', 'return']],
            'addReturnUrl' => false,
        ]) ?>
        <?php  Box::end() ?>
    </div>

    <?php $box = FormBox::begin(['cssClass' => 'message-form box-primary', 'title' => '']) ?>
    <?php $form = ActiveForm::begin(); ?>

    <?php $box->beginBody() ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?php if ($canChangeFromUserId): ?>
                <?= $form->field($model, 'from_admin_user_id')->dropDownList($model->getFromAdminUserIdValues()) ?>
            <?php endif ?>
            <?php if (Category::find()->exists()): ?>
                <?= $form->field($model, 'category_id')->dropDownList($model->getCategoryIdValues()) ?>
            <?php endif ?>
            <?= $form->field($model, 'name')->textInput()->label('Имя, фамилия участника') ?>
            <?= $form->field($model, 'phone_mobile_local')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '+7 999 999-99-99',
            ]) ?>
            <?= $form->field($model, 'content')->textarea(['rows' => '5']) ?>
        </div>
    </div>

    <?php $box->endBody() ?>

    <?php $box->actions([
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
    ]) ?>
    <?php ActiveForm::end(); ?>

    <?php FormBox::end() ?>

</div>
