<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\widgets\ActionButtons;

/**
 * @var yii\web\View $this
 * @var ms\loyalty\feedback\common\models\Faq $model
 */
$this->title = \Yii::t('admin/t', 'Update {item}', ['item' => ms\loyalty\feedback\common\models\Faq::modelTitle()]);
$this->params['breadcrumbs'][] = ['label' => ms\loyalty\feedback\common\models\Faq::modelTitlePlural(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<div class="faq-update">

    <div class="text-right">
        <?php  Box::begin() ?>
        <?=  ActionButtons::widget([
            'order' => [['index', 'update', 'return']],
            'addReturnUrl' => false,
        ]) ?>
        <?php  Box::end() ?>
    </div>

    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
