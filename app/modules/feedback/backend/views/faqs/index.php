<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use yz\icons\Icons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var ms\loyalty\feedback\backend\models\FaqSearch $searchModel
 * @var array $columns
 */

$this->title = ms\loyalty\feedback\common\models\Faq::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'faq-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [/*['search'],*/ ['export', 'export-transfer', 'import', 'create', 'delete', 'return']],
            'gridId' => 'faq-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'ms\loyalty\feedback\common\models\Faq',
            'buttons' => [
                'export-transfer' => function () {
                    return Html::a(Icons::p('exchange').'Экспорт для переноса', \yii\helpers\ArrayHelper::merge(['export-transfer'], Yii::$app->request->get()), [
                        'class' => 'btn btn-default',
                        'title' => 'Экспорт вопросов для переноса в другое хранилище'
                    ]);
                }
            ]
        ]) ?>
    </div>

    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'id' => 'faq-grid',
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => yz\admin\grid\columns\ActionColumn::class,
                'template' => '{prev} {next} {update} {delete}',
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
