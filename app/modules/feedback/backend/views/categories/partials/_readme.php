<?php

use Carbon\Carbon;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 */
?>

<hr/>
<div class="row">
	<div class="col-md-2"></div>
	<div class="col-md-4">
		<h4>Описание параметров шаблона</h4>

		<table class="table">

			<tr>
				<th>{message}</th>
				<td>Текст сообщения</td>
			</tr>
			<tr>
				<th>{id}</th>
				<td>Идентификатор сообщения</td>
			</tr>
			<tr>
				<th>{name}</th>
				<td>Имя отправителя</td>
			</tr>
			<tr>
				<th>{phone}</th>
				<td>Телефон отправителя</td>
			</tr>
			<tr>
				<th>{email}</th>
				<td>E-mail отправителя</td>
			</tr>
			<tr>
				<th>{city}</th>
				<td>Город отправителя</td>
			</tr>
			<tr>
				<th>{info}</th>
				<td>Вся информация по сообщению, построчно</td>
			</tr>
		</table>
	</div>

	<div class="col-md-4">
		<h4>Пример шаблона</h4>
		<p>
			Участник {name}<br/>
			Оставил сообщение "{message}"<br/>
			Время: {created_at}<br/>
			E-mail: {email}<br/>
			Телефон: {phone}<br/>
			Город: {city}<br/>
		</p>
	</div>
	<div class="col-md-2"></div>
</div>
