<?php

use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var ms\loyalty\feedback\backend\models\CategorySearch $searchModel
 * @var array $columns
 */

$this->title = ms\loyalty\feedback\common\models\Category::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'category-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [['export', 'create', 'delete', 'return']],
            'gridId' => 'category-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'ms\loyalty\feedback\common\models\Category',
        ]) ?>
    </div>

    <?= GridView::widget([
        'id' => 'category-grid',
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
