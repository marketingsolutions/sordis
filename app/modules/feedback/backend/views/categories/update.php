<?php

use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;

/**
 * @var yii\web\View $this
 * @var ms\loyalty\feedback\common\models\Category $model
 */
$this->title = \Yii::t('admin/t', 'Update {item}', ['item' => ms\loyalty\feedback\common\models\Category::modelTitle()]);
$this->params['breadcrumbs'][] = ['label' => ms\loyalty\feedback\common\models\Category::modelTitlePlural(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<div class="category-update">

    <div class="text-right">
        <?php  Box::begin() ?>
        <?=  ActionButtons::widget([
            'order' => [['index', 'update', 'return']],
            'addReturnUrl' => false,
        ]) ?>
        <?php  Box::end() ?>
    </div>

    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
