<?php

namespace modules\feedback\backend\reports;

use modules\feedback\common\models\Message;
use ms\loyalty\reports\contracts\base\ReportInterface;
use ms\loyalty\reports\contracts\types\WidgetReportInterface;
use ms\loyalty\reports\support\WidgetConfig;
use yz\icons\Icons;


/**
 * Class FeedbackStat
 */
class FeedbackStat implements ReportInterface, WidgetReportInterface
{

    /**
     * Returns title of the report
     * @return string
     */
    public function title()
    {
        return 'Обратная связь';
    }

    /**
     * @return WidgetConfig[]
     */
    public function widgets()
    {
        return [
            (new WidgetConfig())
                ->title('Всего вопросов')
                ->icon(Icons::i('question-circle'))
                ->style(WidgetConfig::STYLE_AQUA)
                ->value(
                    Message::find()
                        ->count()
                ),

            (new WidgetConfig())
                ->title('Обработанных вопросов')
                ->icon(Icons::i('check'))
                ->style(WidgetConfig::STYLE_GREEN)
                ->value(
                    Message::find()
                        ->where(['is_processed' => 1])
                        ->count()
                ),

            (new WidgetConfig())
                ->title('Необработанных вопросов')
                ->icon(Icons::i('remove'))
                ->style(WidgetConfig::STYLE_YELLOW)
                ->value(
                    Message::find()
                        ->where(['is_processed' => 0])
                        ->count()
                ),
        ];
    }
}