<?php

/**
 * @var \ms\loyalty\feedback\common\models\Message $question
 * @var \ms\loyalty\feedback\common\models\Reply $reply
 * @var \yii\web\View $this
 * @var \yii\mail\BaseMessage $message instance of newly created mail message
 */

use yii\helpers\Url;

$message->setSubject('Ответ на Ваш вопрос');
$userReplyText = Yii::$app->getModule('feedback')->userReplyText;

$url = getenv('FRONTEND_WEB') . '/feedback/feedback/comment?message_id=' . $question->id . '&created_at=' . $question->created_at
?>

<?= $reply->reply ?>

<hr/>

<p>
	<small>Данное сообщение прислано Вам в ответ на вопрос,
		заданный <?= Yii::$app->formatter->asDatetime($question->created_at) ?></small>
</p>
<p>
	<small>Содержание Вашего вопроса:</small>
</p>
<pre><?= Yii::$app->formatter->asNtext($question->content) ?></pre>

<p>
	<b>Пожалуйста, не отвечайте на это письмо!</b><br/>
	Если Вы хотите оставить комментарий к своему обращению, перейдите по ссылке:<br/>
	<a href="<?= $url ?>"><?= $url ?></a>
</p>
