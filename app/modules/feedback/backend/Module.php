<?php

namespace modules\feedback\backend;

use modules\profiles\common\models\Profile;
use ms\loyalty\feedback\backend\controllers\CreateMessageController;
use yii\rbac\Item;
use yz\icons\Icons;


/**
 * Class Modules
 */
class Module extends \modules\feedback\common\Module
{
    public function getName()
    {
        return 'Обратная связь';
    }

    public function getCountNoread(){
        return '<span style="background-color: red;" class="badge badge-warning">'.Profile::getNotProcessingMessage().'</span>';
    }

    public function getAdminMenu()
    {
        return [
            [
                'label' => 'Помощь',
                'icon' => Icons::o('life-ring'),
                'items' => [
                    [
                        'route' => ['/feedback/create-message/index'],
                        'label' => 'Добавить вопрос',
                        'icon' => Icons::o('plus'),
                    ],
                    [
                        'route' => ['/feedback/messages/index'],
                        'label' => 'Вопросы участников&nbsp;'.$this->getCountNoread(),
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/feedback/messages/index-view'],
                        'label' => 'Просмотр вопросов участников',
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/feedback/categories/index'],
                        'label' => 'Категории вопросов',
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/feedback/faqs/index'],
                        'label' => 'Частые вопросы (FAQ)',
                        'icon' => Icons::o('question-circle'),
                    ],
                ]
            ],
        ];
    }

    public function getAuthItems()
    {
        return array_merge(parent::getAuthItems(), [
            CreateMessageController::PERMISSION_CHANGE_FROM_ADMIN_USER_ID => ['Изменение имени администратора при добавлении вопроса', Item::TYPE_PERMISSION, []]
        ]);
    }


}