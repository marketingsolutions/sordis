<?php

use yii\db\Schema;
use yii\db\Migration;

class m150305_072121_feedback_add_foreign_key extends Migration
{
    public function up()
    {
        $this->createIndex('message_id', '{{%feedback_messages_comments}}', 'message_id');
        $this->addForeignKey('{{%fk_comments_messages}}', '{{%feedback_messages_comments}}', 'message_id', '{{%feedback_messages}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('{{%fk_comments_messages}}', '{{%feedback_messages_comments}}');
        $this->dropIndex('message_id', '{{%feedback_messages_comments}}');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
