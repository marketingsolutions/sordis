<?php

use yii\db\Schema;
use yii\db\Migration;

class m150406_190744_feedback_switch_to_categories extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%feedback_messages}}', 'category');
    }

    public function down()
    {
        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
