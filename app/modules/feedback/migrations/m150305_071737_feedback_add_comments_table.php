<?php

use yii\db\Schema;
use yii\db\Migration;

class m150305_071737_feedback_add_comments_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%feedback_messages_comments}}', [
            'id' => Schema::TYPE_PK,
            'message_id' => Schema::TYPE_INTEGER,
            'comment' => Schema::TYPE_TEXT,
            'admin_user_id' => Schema::TYPE_INTEGER,
            'created_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%feedback_messages_comments}}');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
