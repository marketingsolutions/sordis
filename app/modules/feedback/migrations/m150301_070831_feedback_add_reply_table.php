<?php

use yii\db\Schema;
use yii\db\Migration;

class m150301_070831_feedback_add_reply_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->renameColumn('{{%feedback_messages}}', 'reply_sent_at', 'last_reply_sent_at');

        $this->createTable('{{%feedback_messages_replies}}', [
            'id' => Schema::TYPE_PK,
            'message_id' => Schema::TYPE_INTEGER,
            'reply' => Schema::TYPE_TEXT,
            'admin_user_id' => Schema::TYPE_INTEGER,
            'created_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%feedback_messages_replies}}');
        $this->renameColumn('{{%feedback_messages}}', 'last_reply_sent_at', 'reply_sent_at');
    }
}
