<?php

use yii\db\Schema;
use yii\db\Migration;

class m150301_142008_feedback_update_processed_time extends Migration
{
    public function up()
    {
        $this->update('{{%feedback_messages}}', [
            'processed_at' => new \yii\db\Expression('last_reply_sent_at')
        ], 'last_reply_sent_at IS NOT NULL');
    }

    public function down()
    {

    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
