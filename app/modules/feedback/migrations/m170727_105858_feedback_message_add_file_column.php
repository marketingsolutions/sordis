<?php

use yii\db\Migration;

class m170727_105858_feedback_message_add_file_column extends Migration
{
    public function up()
    {
        $this->addColumn('{{%feedback_messages}}', 'file', $this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%feedback_messages}}', 'file');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
