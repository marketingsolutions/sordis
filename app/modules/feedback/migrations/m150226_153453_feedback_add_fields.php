<?php

use yii\db\Schema;
use yii\db\Migration;

class m150226_153453_feedback_add_fields extends Migration
{
    public function up()
    {
        $this->addColumn('{{%feedback_messages}}', 'comment', Schema::TYPE_TEXT);
        $this->addColumn('{{%feedback_messages}}', 'reply', Schema::TYPE_TEXT);
        $this->addColumn('{{%feedback_messages}}', 'admin_user_id', Schema::TYPE_INTEGER);
        $this->addColumn('{{%feedback_messages}}', 'reply_sent_at', Schema::TYPE_DATETIME);

        $this->createIndex('admin_user_id', '{{%feedback_messages}}', 'admin_user_id');
        $this->addForeignKey('{{%fk_feedback_admin_users}}', '{{%feedback_messages}}', 'admin_user_id', '{{%admin_users}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        $this->dropColumn('{{%feedback_messages}}', 'reply_sent_at');
        $this->dropColumn('{{%feedback_messages}}', 'admin_user_id');
        $this->dropColumn('{{%feedback_messages}}', 'reply');
        $this->dropColumn('{{%feedback_messages}}', 'comment');
    }
}
