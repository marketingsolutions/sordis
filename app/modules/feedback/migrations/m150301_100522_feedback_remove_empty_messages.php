<?php

use yii\db\Schema;
use yii\db\Migration;

class m150301_100522_feedback_remove_empty_messages extends Migration
{
    public function up()
    {
        $this->delete('{{%feedback_messages_replies}}', [
            'admin_user_id' => null
        ]);
    }

    public function down()
    {

    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
