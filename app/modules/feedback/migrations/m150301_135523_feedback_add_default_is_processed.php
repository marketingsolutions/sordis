<?php

use yii\db\Schema;
use yii\db\Migration;

class m150301_135523_feedback_add_default_is_processed extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%feedback_messages}}', 'is_processed', Schema::TYPE_BOOLEAN.' DEFAULT 0');
        $this->update('{{%feedback_messages}}', [
            'is_processed' => 0
        ], 'is_processed IS NULL');
    }

    public function down()
    {
        $this->alterColumn('{{%feedback_messages}}', 'is_processed', Schema::TYPE_BOOLEAN);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
