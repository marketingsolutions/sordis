<?php

use yii\db\Schema;
use yii\db\Migration;

class m150301_092406_feedback_add_foreign_key extends Migration
{
    public function up()
    {
        $this->createIndex('message_id', '{{%feedback_messages_replies}}', 'message_id');
        $this->addForeignKey('{{%fk_replies_messages}}', '{{%feedback_messages_replies}}', 'message_id', '{{%feedback_messages}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('{{%fk_replies_messages}}', '{{%feedback_messages_replies}}');
        $this->dropIndex('message_id', '{{%feedback_messages_replies}}');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
