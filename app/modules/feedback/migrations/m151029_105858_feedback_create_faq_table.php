<?php

use yii\db\Migration;

class m151029_105858_feedback_create_faq_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%faqs}}', [
            'id' => $this->primaryKey(),
            'question' => $this->string(),
            'answer' => $this->text(),
            'is_published' => $this->boolean()->defaultValue(1),
            'order' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%faqs}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
