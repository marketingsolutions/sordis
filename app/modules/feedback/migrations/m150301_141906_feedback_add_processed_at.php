<?php

use yii\db\Schema;
use yii\db\Migration;

class m150301_141906_feedback_add_processed_at extends Migration
{
    public function up()
    {
        $this->addColumn('{{%feedback_messages}}', 'processed_at', Schema::TYPE_DATETIME);
    }

    public function down()
    {
        $this->dropColumn('{{%feedback_messages}}', 'processed_at');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
