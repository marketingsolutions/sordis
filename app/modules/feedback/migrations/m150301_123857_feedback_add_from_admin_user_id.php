<?php

use yii\db\Schema;
use yii\db\Migration;

class m150301_123857_feedback_add_from_admin_user_id extends Migration
{
    public function up()
    {
        $this->addColumn('{{%feedback_messages}}', 'from_admin_user_id', Schema::TYPE_INTEGER);
        $this->createIndex('from_admin_user_id', '{{%feedback_messages}}', 'from_admin_user_id');
        $this->addForeignKey('{{%fk_feedback_from_admin_users}}', '{{%feedback_messages}}', 'from_admin_user_id', '{{%admin_users}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        $this->dropColumn('{{%feedback_messages}}', 'from_admin_user_id');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
