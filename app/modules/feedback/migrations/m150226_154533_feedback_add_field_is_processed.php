<?php

use yii\db\Schema;
use yii\db\Migration;

class m150226_154533_feedback_add_field_is_processed extends Migration
{
    public function up()
    {
        $this->addColumn('{{%feedback_messages}}', 'is_processed', Schema::TYPE_BOOLEAN);
    }

    public function down()
    {
        $this->dropColumn('{{%feedback_messages}}', 'is_processed');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
