<?php

use yii\db\Schema;
use yii\db\Migration;

class m150301_083916_feedback_migrate_messages_and_drop_column extends Migration
{
    public function up()
    {
        $sql =<<<SQL
INSERT INTO {{%feedback_messages_replies}} (message_id, reply, admin_user_id, created_at)
SELECT id, reply, admin_user_id, last_reply_sent_at FROM {{%feedback_messages}}
SQL;
        $this->execute($sql);

        $this->dropColumn('{{%feedback_messages}}', 'reply');
    }

    public function down()
    {
        $this->addColumn('{{%feedback_messages}}', 'reply', Schema::TYPE_TEXT);
        $sql =<<<SQL
UPDATE {{%feedback_messages}} as mess, {{%feedback_messages_replies}} as repl
SET mess.reply = repl.reply WHERE mess.id = repl.message_id
SQL;
        $this->execute($sql);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
