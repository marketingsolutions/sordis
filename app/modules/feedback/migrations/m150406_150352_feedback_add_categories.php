<?php

use yii\db\Schema;
use yii\db\Migration;

class m150406_150352_feedback_add_categories extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%feedback_categories}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'is_send_to_email' => Schema::TYPE_BOOLEAN,
            'email' => Schema::TYPE_STRING,
            'template' => Schema::TYPE_TEXT,
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%feedback_categories}}');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
