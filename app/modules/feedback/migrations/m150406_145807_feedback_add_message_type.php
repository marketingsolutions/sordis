<?php

use yii\db\Schema;
use yii\db\Migration;

class m150406_145807_feedback_add_message_type extends Migration
{
    public function up()
    {
        $this->addColumn('{{%feedback_messages}}', 'category', Schema::TYPE_STRING.'(32) DEFAULT "default"');
    }

    public function down()
    {
        $this->dropColumn('{{%feedback_messages}}', 'category');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
