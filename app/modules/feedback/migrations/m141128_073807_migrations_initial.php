<?php

use yii\db\Migration;
use yii\db\Schema;

class m141128_073807_migrations_initial extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%feedback_messages}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING,
            'email' => Schema::TYPE_STRING,
            'phone_mobile' => Schema::TYPE_STRING.'(16)',
            'content' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
        $this->createIndex('user_id', '{{%feedback_messages}}', 'user_id');
    }

    public function down()
    {
        $this->dropTable('{{%feedback_messages}}');
    }
}
