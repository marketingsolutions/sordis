<?php

use yii\db\Schema;
use yii\db\Migration;

class m150305_084343_feedback_delete_empty_comments extends Migration
{
    public function up()
    {
        $this->delete('{{%feedback_messages_comments}}', 'comment = ""');
    }

    public function down()
    {

    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
