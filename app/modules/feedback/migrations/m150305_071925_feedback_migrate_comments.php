<?php

use yii\db\Schema;
use yii\db\Migration;

class m150305_071925_feedback_migrate_comments extends Migration
{
    public function safeUp()
    {
        $sql =<<<SQL
INSERT INTO {{%feedback_messages_comments}} (message_id, comment, admin_user_id, created_at)
SELECT id, comment, admin_user_id, processed_at FROM {{%feedback_messages}}
WHERE comment != '' AND comment IS NOT NULL
SQL;
        $this->execute($sql);

        $this->dropColumn('{{%feedback_messages}}', 'comment');
    }

    public function safeDown()
    {
        $this->addColumn('{{%feedback_messages}}', 'comment', Schema::TYPE_TEXT);
        $sql =<<<SQL
UPDATE {{%feedback_messages}} as mess, {{%feedback_messages_comments}} as comm
SET mess.comment = comm.comment WHERE mess.id = comm.message_id
SQL;
        $this->execute($sql);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
