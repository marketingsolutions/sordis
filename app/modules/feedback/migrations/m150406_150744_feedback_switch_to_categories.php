<?php

use yii\db\Schema;
use yii\db\Migration;

class m150406_150744_feedback_switch_to_categories extends Migration
{
    public function up()
    {
        $this->addColumn('{{%feedback_messages}}', 'category_id', Schema::TYPE_INTEGER);
        $this->createIndex('category_id', '{{%feedback_messages}}', 'category_id');
        $this->addForeignKey('{{%fk_feedback_massages_categories}}', '{{%feedback_messages}}', 'category_id', '{{%feedback_categories}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('{{%fk_feedback_massages_categories}}', '{{%feedback_messages}}');
        $this->dropColumn('{{%feedback_messages}}', 'category_id');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
