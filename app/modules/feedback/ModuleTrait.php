<?php
namespace modules\feedback;
use ms\loyalty\feedback\common\Module as CommonModule;
use ms\loyalty\feedback\frontend\Module as FrontendModule;
use ms\loyalty\feedback\backend\Module as BackendModule;


/**
 * Trait ModuleTrait
 */
trait ModuleTrait
{
    /**
     * @return CommonModule | FrontendModule | BackendModule
     */
    public static function getFeedbackModule()
    {
        return \Yii::$app->getModule('feedback');
    }
}