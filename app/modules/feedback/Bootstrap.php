<?php

namespace ms\loyalty\feedback;

use yii\base\Application;
use yii\base\BootstrapInterface;


/**
 * Class Bootstrap
 */
class Bootstrap implements BootstrapInterface
{

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        \Yii::$app->params['yii.migrations'][] = '@ms/loyalty/feedback/migrations';

        if (!isset($app->i18n->translations['loyalty'])) {
            $app->i18n->translations['loyalty'] = [
                'class' => \yii\i18n\PhpMessageSource::class,
                'basePath' => '@common/messages',
                'sourceLanguage' => 'ru-RU',
            ];
        }
    }
}