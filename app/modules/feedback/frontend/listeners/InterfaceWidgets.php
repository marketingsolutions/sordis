<?php

namespace modules\feedback\frontend\listeners;
use ms\loyalty\contracts\widgets\WidgetsCollectionInterface;
use yii\base\Event;
use modules\feedback\frontend\widgets\FeedbackTopMenu;


/**
 * Class InterfaceWidgets
 */
class InterfaceWidgets
{
    public static function whenInitCollection(Event $event)
    {
        /** @var \ms\loyalty\contracts\widgets\WidgetsCollectionInterface $dashboard */
        $dashboard = $event->sender;

        $dashboard->addWidget(WidgetsCollectionInterface::GROUP_MAIN_MENU_ITEMS, FeedbackTopMenu::class);
    }
}