<?php

namespace modules\feedback\frontend\controllers;

use frontend\base\Controller;
use modules\users\common\models\User;
use ms\loyalty\contracts\identities\IdentityInterface;
use ms\loyalty\contracts\profiles\HasEmailInterface;
use ms\loyalty\contracts\profiles\HasPhoneMobileInterface;
use ms\loyalty\contracts\profiles\ProfileInterface;
use ms\loyalty\contracts\profiles\UserNameInterface;
use modules\feedback\common\models\Faq;
use modules\feedback\common\models\Message;
use modules\feedback\frontend\forms\MessageForm;
use modules\feedback\frontend\forms\MessageFormExtends;
use modules\feedback\ModuleTrait;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yz\Yz;
use ms\files\attachments\common\traits\AttachedFilesProcessingTrait;

/**
 * Class MessagesController
 */
class MessagesController extends Controller
{
    use ModuleTrait, AttachedFilesProcessingTrait;

    public function getAccessRules()
    {
        return [
            [
                'allow' => true,
            ],
        ];
    }

    public function actionAdd()
    {
        if (getenv('RECAPTCHA_SITEKEY') && getenv('RECAPTCHA_SECRETKEY')) {
            $model = new MessageFormExtends();
        }
        else {
            $model = new MessageForm();
        }

        if (\Yii::$app->user->isGuest == false) {
            $model->scenario = MessageForm::SCENARIO_MESSAGE_FROM_REGISTERED;
            $this->fillMessageFromIdentity($model);
        }
        else {
            $model->scenario = MessageForm::SCENARIO_MESSAGE_FROM_GUEST;
        }

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            $model->refresh();
            $this->attachFilesToModel($model, \Yii::$app->request->post());
            $model->sendToAdmin();
            \Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, $this->module->messageAfterAdd);
            return $this->redirect(self::getFeedbackModule()->redirectAfterMessageCreate ?: Url::current());
        }

        return $this->render('add', [
            'model' => $model,
            'faqsDataProvider' => $this->getFaqDataProvider(),
        ]);
    }

    protected function fillMessageFromIdentity(Message $message)
    {
        /** @var IdentityInterface $identity */
        $identity = \Yii::$app->user->identity;
        $profile = $identity->getProfile();

        if ($profile instanceof ProfileInterface) {
            $message->user_id = $profile->getProfileId();
        }
        if ($profile instanceof HasEmailInterface) {
            $message->email = $profile->getEmail();
        }
        if ($profile instanceof HasPhoneMobileInterface) {
            $message->phone_mobile = $profile->getPhoneMobile();
        }
        if ($profile instanceof UserNameInterface) {
            $message->name = $profile->getFullName();
        }

    }

    /**
     * @return ActiveDataProvider
     */
    private function getFaqDataProvider()
    {
        return new ActiveDataProvider([
            'query' => Faq::find(),
            'sort' => [
                'defaultOrder' => [
                    'order' => SORT_ASC,
                ]
            ]
        ]);
    }
}