<?php

namespace modules\feedback\frontend\controllers\api;

use modules\profiles\frontend\models\ApiProfile;
use ms\loyalty\api\common\models\Log;
use ms\loyalty\api\frontend\base\ApiController;
use modules\feedback\common\models\Faq;
use modules\feedback\frontend\forms\ApiMessageForm;
use modules\feedback\common\models\Category;
use Yii;

/**
 * Class FeedbackController
 */
class FeedbackController extends ApiController
{
    public function actionFeedbackCategories()
    {
        $cats = Category::find()
            ->all();
        $return = ['result' => 'OK', 'categories' => $cats];

        return $return;
    }

    public function actionFaq()
    {
        $this->logResponse = false;

        return ['result' => 'OK', 'faq' => Faq::find()->orderBy('order')->all()];
    }

    public function actionFeedback()
    {
        $message = new ApiMessageForm();
        $message->scenario = 'messageFromRegistered';
        $message->user_id = null;

        if ($profile_id = Yii::$app->request->post('profile_id', null)) {
            if ($profile = ApiProfile::findOne(['id' => $profile_id])) {
                $message->user_id = $profile->id;
            }
        }

        if ($message->load(Yii::$app->request->post(), '') && $message->save()) {
            $message->sendToAdmin();

            return $this->ok(['message' => 'Сообщение отправлено!'], 'Получен фидбек от участника');
        }

        return $this->error($message->getFirstErrors(), 'Ошибка при отправке фидбека');
    }
}