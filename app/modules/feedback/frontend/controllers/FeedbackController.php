<?php

namespace modules\feedback\frontend\controllers;

use modules\feedback\common\models\Comment;
use modules\feedback\common\models\Message;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class FeedbackController extends Controller
{
    /**
     * @return array
     */
    public function actionAdd()
    {
        /** @var \modules\main\common\models\Profile $profile */


        $model = new Message();
        if ($profile = Yii::$app->user->identity) {
            $model->user_id = $profile->getId();
        }
        $model->name = $profile->full_name;
        $model->email = $profile->email;
        $model->phone_mobile = $profile->phone_mobile;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->refresh();
            $model->sendToAdmin();
            $model->sendToUser();

            Yii::$app->session->setFlash('success', 'Ваше обращение добавлено!');
        }
        else {
            $errors = $model->getFirstErrors();
            $errors = array_values($errors);
            Yii::$app->session->setFlash('warning', 'Ошибка! ' . implode('. ', $errors));
        }

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionComment($message_id, $created_at)
    {
        $message = Message::findOne(['id' => $message_id, 'created_at' => $created_at]);

        if ($message == null) {
            throw new NotFoundHttpException();
        }

        $model = new Comment();
        $model->message_id = $message->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $message->sendToAdmin($model);
            Yii::$app->session->setFlash('success', 'Ваше обращение добавлено!');

            return $this->redirect(['comment', 'message_id' => $message_id, 'created_at' => $created_at]);
        }

        $key = $message->created_at;
        $history = [$message];

        $comments = $message->comments;
        if (!empty($comments)) {
            foreach ($comments as $comment) {
                $history[] = $comment;
            }
        }

        $replies = $message->replies;
        if (!empty($replies)) {
            foreach ($replies as $reply) {
                $history[] = $reply;
            }
        }


        usort($history, function ($a, $b) {
            $dateA = (new \DateTime($a->created_at));
            $dateB = (new \DateTime($b->created_at));

            return $dateA < $dateB;
        });


        return $this->render('comment', compact('model', 'history'));
    }
}