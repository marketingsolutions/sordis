<?php

namespace modules\feedback\frontend\widgets;

use yii\base\Widget;

/**
 * Class FeedbackTopMenu
 */
class FeedbackTopMenu extends Widget
{
    public function run()
    {
        return $this->render('feedback-top-menu');
    }
}