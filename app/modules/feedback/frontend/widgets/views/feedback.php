<?php

/**
 * @var \yii\web\View $this
 * @var string $title
 * @var string $buttonText
 * @var string $buttonClass
 */

use ms\loyalty\feedback\common\models\Category;
use ms\loyalty\feedback\common\models\Message;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$model = new Message();
$categories = Category::find()->indexBy('id')->select('name, id')->orderBy(['name' => SORT_ASC])->column();
?>

	<h2><?= $title ?></h2>

<?php $form = ActiveForm::begin(['id' => 'feedbackForm', 'action' => '/feedback/feedback/add', 'options' => [
    'enctype' => 'multipart/form-data',
]]); ?>

<?= $form->field($model, 'category_id')->dropDownList($categories)->label(false) ?>
<?= $form->field($model, 'content')->textarea(['rows' => 5, 'class' => 'form-control full'])->label(false) ?>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group field-message-file_local">
				<label for="message-file_local" class="full <?= $buttonClass ?>">
					<input type="file" id="message-file_local" name="Message[file_local]"
						   style="position:fixed; top:-10000px;">
					<i class="fa fa-paperclip"></i> Прикрепить
				</label>
			</div>
		</div>
		<div class="col-md-6">
			<button type="submit" class="full <?= $buttonClass ?>" style="margin:0;">
				<i class="fa fa-paper-plane"></i> Отправить
			</button>
		</div>
	</div>

	<ul class="errors" id="feedback-errors"></ul>

<?php ActiveForm::end(); ?>