<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 */
?>

<?= \yii\widgets\Menu::widget([
    'options' => [
        'tag' => false
    ],
    'items' => [
        ['label' => 'Помощь', 'url' => ['/feedback/messages/add']],
    ]
]) ?>