<?php

namespace modules\feedback\frontend\widgets\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class FeedbackAsset extends AssetBundle
{
    public $sourcePath = '@ms/loyalty/feedback/frontend/widgets/assets/feedback';

    public $js = [
        'js/feedback.js',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}