$(document).ready(function() {

    $('#feedbackForm button[type="submit"]').click(function(e) {
        e.preventDefault();
        var data = $('#feedbackForm').serialize();

        $.ajax({
            type:    'POST',
            url:     '/feedback/feedback/add',
            data:    data,
            success: function(data) {
				$.notify(data.msg, {className: 'info', autoHideDelay: 7000, globalPosition: 'bottom left'});
				$('#feedbackForm textarea').val('');
				$("#feedback-errors li").remove();
            },
            error:   function(xhr) {
                var data = xhr.responseJSON;
                if ("errors" in data) {
                    $("#feedback-errors li").remove();
                    for (var i = 0; i < data.errors.length; i++) {
                        $("#feedback-errors").append("<li>" + data.errors[i] + "</li>");
                    }
                }
                $('#feedbackForm textarea').focus();
            }
        });

        return false;
    });

});