<?php

namespace modules\feedback\frontend\widgets;

use yii\base\Widget;

class FeedbackWidget extends Widget
{
    public $title = 'Связаться с нами';
    public $buttonText = 'Отправить';
    public $buttonClass= 'btn btn-primary';

    public function run()
    {
        return $this->render('feedback', [
            'title' => $this->title,
            'buttonText' => $this->buttonText,
            'buttonClass' => $this->buttonClass,
        ]);
    }
}