<?php

use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var \yii\mail\BaseMessage $message instance of newly created mail message
 * @var \ms\loyalty\feedback\frontend\forms\MessageForm $model
 * @var \ms\loyalty\feedback\common\models\Comment $comment
 */
$message->setSubject('Обращение на сайте');
$userAddText = Yii::$app->getModule('feedback')->userAddText;

$url = getenv('FRONTEND_WEB') . '/feedback/feedback/comment?message_id=' . $model->id . '&created_at=' . $model->created_at
?>
<p><strong>Здравствуйте, <?= Html::encode($model->name) ?>,</strong></p>

<p>Вами был задан вопрос:</p>

<pre>
    <?= Html::encode($comment ? $comment->comment : $model->content) ?>
</pre>

<?php if (!empty($userAddText)): ?>
	<p><?= $userAddText ?></p>
<?php endif; ?>

<p>
	<b>Пожалуйста, не отвечайте на это письмо!</b><br/>
	Если Вы хотите оставить комментарий к своему обращению, перейдите по ссылке:<br/>
	<a href="<?= $url ?>"><?= $url ?></a>
</p>