<?php
use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var \yii\mail\BaseMessage $message instance of newly created mail message
 * @var \ms\loyalty\feedback\frontend\forms\MessageForm $model
 */
$message->setSubject('Новый вопрос');
?>
<p><strong>Здравствуйте,</strong></p>

<p>Участник промо-акции <strong><?= Html::encode($model->name) ?></strong> задал вопрос:</p>

<pre>
    <?= Html::encode($model->content) ?>
</pre>
