<?php

namespace modules\feedback\frontend\forms;


/**
 * Class ApiMessageForm
 */
class ApiMessageForm extends MessageForm
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['name', 'email'], 'string', 'max' => 255],
            [['phone_mobile'], 'string', 'max' => 16],
            [['name', 'phone_mobile_local', 'content'], 'required'],
            ['email', 'email'],
            ['email', 'required', 'on' => [self::SCENARIO_DEFAULT]],
            ['is_processed', 'boolean', 'on' => [self::SCENARIO_ADMIN_EDIT]],
            ['user_id', 'integer'],
        ];
    }
}