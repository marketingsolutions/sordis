<?php

namespace modules\feedback\frontend\forms;

use modules\feedback\common\models\Message;


/**
 * Class MessageForm
 */
class MessageForm extends Message
{
    const SCENARIO_MESSAGE_FROM_GUEST = 'messageFromGuest';
    const SCENARIO_MESSAGE_FROM_REGISTERED = 'messageFromRegistered';

    /**
     * @var string
     */
    public $verificationCode;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['verificationCode'], 'required', 'on' => self::SCENARIO_MESSAGE_FROM_GUEST],
            [['verificationCode'], 'captcha', 'captchaAction' => '/site/captcha', 'on' => self::SCENARIO_MESSAGE_FROM_GUEST],
        ]);
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_MESSAGE_FROM_REGISTERED] = $scenarios[self::SCENARIO_DEFAULT];
        return $scenarios;
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'verificationCode' => 'Код подтверждения',
        ]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        /**
         * @todo Refactor the following code to use commands
         */
        if (empty($this->email)) {
            return;
        }

        $this->sendToUser();
    }
}