<?php

namespace modules\feedback\frontend\forms;

use modules\feedback\common\models\Message;

/**
 * Class MessageFormExtends
 */
class MessageFormExtends extends Message
{
    const SCENARIO_MESSAGE_FROM_GUEST = 'messageFromGuest';
    const SCENARIO_MESSAGE_FROM_REGISTERED = 'messageFromRegistered';

    /**
     * @var string
     */
    public $verificationCode;
    public $reCaptcha;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                'reCaptcha',
                \himiklab\yii2\recaptcha\ReCaptchaValidator::class,
                'secret' => getenv('RECAPTCHA_SECRETKEY'),
                'uncheckedMessage' => 'Пожалуйста, подтвердите,что вы не робот',
                'on' => self::SCENARIO_MESSAGE_FROM_GUEST
            ],
        ]);
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_MESSAGE_FROM_REGISTERED] = $scenarios[self::SCENARIO_DEFAULT];
        return $scenarios;
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'reCaptcha' => 'Код подтверждения',
        ]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        /**
         * @todo Refactor the following code to use commands
         */
        if (empty($this->email)) {
            return;
        }

        $this->sendToUser();
    }
}