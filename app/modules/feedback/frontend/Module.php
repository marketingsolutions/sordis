<?php

namespace modules\feedback\frontend;


/**
 * Class Module
 */
class Module extends \modules\feedback\common\Module
{
    /**
     * Url where to redirect after creation of the message
     * @var string | array
     */
    public $redirectAfterMessageCreate;

    /**
     * Text message after adding new question
     * @var string
     */
    public $messageAfterAdd;


    public function init()
    {
        parent::init();

        if(!$this->messageAfterAdd) {
            $this->messageAfterAdd = \Yii::t('loyalty', 'Ваше сообщение успешно отправлено администраторам акции, срок ответа на Ваш вопрос - 5 рабочих дней');
        }
    }

}