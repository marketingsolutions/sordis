<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \ms\loyalty\feedback\common\models\Faq $model
 * @var integer $index
 * @var \yii\widgets\ListView $widget
 */
?>
<div class="panel panel-default">
  <div class="panel-heading" role="tab" id="heading-<?= $index ?>">
    <h4 class="panel-title">
      <a class="collapsed faq-link" role="button" data-toggle="collapse" data-parent="#accordion-faq"
         href="#collapse-<?= $index ?>" aria-expanded="false" aria-controls="collapseOne">
        <?= Html::encode($model->question) ?>
      </a>
    </h4>
  </div>
  <div id="collapse-<?= $index ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
    <div class="panel-body">
      <?= $model->answer ?>
    </div>
  </div>
</div>
