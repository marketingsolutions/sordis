<?php

namespace modules\feedback\common\models;

use modules\profiles\common\models\City;
use modules\profiles\common\models\Profile;
use modules\users\common\models\User;
use marketingsolutions\phonenumbers\PhoneNumberBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yz\interfaces\ModelInfoInterface;
use ms\files\attachments\common\contracts\FileAttachmentsOwnerInterface;
use ms\files\attachments\common\models\AttachedFile;
use ms\files\attachments\common\traits\FileAttachmentsTrait;

/**
 * This is the model class for table "yz_feedback_messages".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $email
 * @property string $phone_mobile
 * @property string $content
 * @property string $created_at
 * @property string $updated_at
 * @property integer $admin_user_id
 * @property string $last_reply_sent_at
 * @property integer $is_processed
 * @property integer $from_admin_user_id
 * @property string $processed_at
 * @property integer $category_id
 * @property string $file
 *
 * @property User $user
 * @property \yz\admin\models\User $adminUser
 * @property \yz\admin\models\User $fromAdminUser
 * @property Reply[] $replies
 * @property Comment[] $comments
 * @property Category $category
 *
 * @property string $phone_mobile_local
 * @property string $file_url
 * @property string $file_path
 */
class Message extends \yz\db\ActiveRecord implements ModelInfoInterface, FileAttachmentsOwnerInterface
{
    use FileAttachmentsTrait;

    const SCENARIO_ADMIN_EDIT = 'adminEdit';
    const SCENARIO_BACKEND_ADD_MESSAGE = 'backendAddMessage';

    public $file_local;

    /**
     * @var \yz\admin\models\User
     */
    public $currentAdminUser;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%feedback_messages}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Сообщение';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Сообщения';
    }

    public static function getIsProcessedValues()
    {
        return [
            0 => 'Не обработан',
            1 => 'Обработан',
        ];
    }

    public static function getAdminUserIdValues()
    {
        return ArrayHelper::map(\yz\admin\models\User::find()->all(), 'id', 'name');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $categoryExists = Category::find()->exists();

        return [
            ['content', 'string'],
            ['content', 'required'],
            [['name', 'email'], 'string', 'max' => 255],
            [['phone_mobile'], 'string', 'max' => 16],
            [['name', 'phone_mobile_local', 'content'], 'required'],
            ['email', 'email'],
            ['email', 'required', 'on' => [self::SCENARIO_DEFAULT]],
            ['is_processed', 'boolean', 'on' => [self::SCENARIO_ADMIN_EDIT]],
            ['category_id', 'required', 'when' => function () use ($categoryExists) {
                return $categoryExists;
            }],
            ['category_id', 'in', 'range' => array_keys(self::getCategoryIdValues()), 'when' => function () use ($categoryExists) {
                return $categoryExists;
            }],
            ['file', 'string', 'max' => 255],
            ['file_local', 'file', 'extensions' => ['png', 'jpg', 'jpeg', 'gif', 'pdf'], 'maxSize' => 1024 * 1024 * 10, 'tooBig' => 'Максимальный размер загружаемого файла - 10 МБ'],
        ];
    }

    public static function getFromAdminUserIdValues()
    {
        return ArrayHelper::map(\yz\admin\models\User::find()->all(), 'id', 'name');
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            'phonenumber' => [
                'class' => PhoneNumberBehavior::className(),
                'attributes' => [
                    'phone_mobile_local' => 'phone_mobile',
                ],
                'defaultRegion' => 'RU',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Участник',
            'name' => 'Имя',
            'email' => 'Электронный адрес',
            'phone_mobile' => 'Мобильный телефон',
            'phone_mobile_local' => 'Мобильный телефон',
            'content' => 'Сообщение',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'is_processed' => 'Вопрос обработан',
            'last_reply_sent_at' => 'Дата отправки последнего ответа',
            'from_admin_user_id' => 'Создал вопрос',
            'processed_at' => 'Дата обработки',
            'category_id' => 'Категория вопроса',
            'file' => 'Файл',
            'file_local' => 'Файл',
        ];
    }

    /**
     * @param null | string $type
     * @return AttachedFile
     */
    public function instantiateAttachedFile($type = null)
    {
        $attachedFile = AttachedFile::getInstance(self::class, $this->id, '@data/feedback-messages');
        $attachedFile->type = $type;
        return $attachedFile;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminUser()
    {
        return $this->hasOne(\yz\admin\models\User::className(), ['id' => 'admin_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReplies()
    {
        return $this->hasMany(Reply::className(), ['message_id' => 'id'])->orderBy(['created_at' => 'ASC']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['message_id' => 'id'])->orderBy(['created_at' => 'ASC']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromAdminUser()
    {
        return $this->hasOne(\yz\admin\models\User::className(), ['id' => 'from_admin_user_id']);
    }

    public function beforeSave($insert)
    {
        if ($this->is_processed && $this->isAttributeChanged('is_processed')) {
            if ($this->currentAdminUser) {
                $this->admin_user_id = $this->currentAdminUser->id;
            }
            $this->processed_at = new Expression('NOW()');
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getCategoryName()
    {
        return isset($this->category->name) ? $this->category->name : null;
    }

    public static function getCategoryIdValues()
    {
        return ArrayHelper::map(Category::find()->all(), 'id', 'name');
    }

    /**
     * @return string
     */
    public function findCity()
    {
        /** @var Profile $profile */
        if ($profile = Profile::findOne($this->user_id)) {
            if ($profile->hasAttribute('city_id')) {
                if ($city = City::findOne($profile->city_id)) {
                    if (!empty($city->title)) {
                        return $city->title;
                    }
                }
            }
        }
        return '';
    }

    /**
     * @param Comment|null $comment
     * @return bool
     */
    public function sendToAdmin(Comment $comment = null)
    {
        $this->refresh();
        $category = $this->category;

        if ($category === null) {
            return false;
        }

        if ($category->is_send_to_email == 0) {
            return true;
        }

        $template = $category->template;
        $body = strtr($template, [
            '{message}' => $comment ? $comment->comment : $this->content,
            '{id}' => $this->id,
            '{name}' => $this->name,
            '{email}' => $this->email,
            '{created_at}' => Yii::$app->formatter->asDatetime($this->created_at),
            '{phone_mobile}' => $this->phone_mobile,
            '{phone_mobile_local}' => $this->phone_mobile_local,
            '{city}' => $this->findCity(),
            '{info}' => $this->getTextRepresentation()
        ]);

        try {
            Yii::$app->mailer->compose()
                ->setHtmlBody($body)
                ->setSubject($comment ? 'Участник добавил комментарий к вопросу' : 'Новый вопрос на сайте')
                ->setTo($category->getEmailAsArray())
                ->send();
        }
        catch (\Exception $e) {
            Yii::error("Feedback message #{$this->id} send error: {$e->getMessage()}");
            return false;
        }
        return true;
    }

    /**
     * @param Comment|null $comment
     * @return bool
     */
    public function sendToUser(Comment $comment = null)
    {
        $this->refresh();
        try {
            Yii::$app->mailer->compose('@ms/loyalty/feedback/frontend/mails/userAddsMessage.php', [
                'model' => $this,
                'comment' => $comment
            ])
                ->setTo($this->email)
                ->send();
        }
        catch (\Exception $e) {
            Yii::error("Feedback message #{$this->id} send error: {$e->getMessage()}");
            return false;
        }

        return true;
    }

    protected function getTextRepresentation()
    {
        $content = Yii::$app->formatter->asNtext($this->content);
        $date = Yii::$app->formatter->asDatetime($this->created_at);
        $url = Url::to('@backendWeb/feedback/messages/update?id=' . $this->id);
        return <<<HTML
<p>ID вопроса: <a href="{$url}">#{$this->id}</a></p>
<p>Дата: {$date}</p>
<p>Имя: {$this->name}</p>
<p>email: {$this->email}</p>
<p>Телефон: {$this->phone_mobile_local}</p>
<p>Сообщение:</p>
<code>{$content}</code>
HTML;

    }

    public function getFile_url()
    {
        return empty($this->file) ? null : getenv('FRONTEND_WEB') . '/data/feedback-messages/' . $this->file;
    }

    public function getFile_path()
    {
        return empty($this->file) ? null : Yii::getAlias('@data/feedback-messages/') . $this->file;
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->upload(['file']);

        parent::afterSave($insert, $changedAttributes);
    }

    public function upload($fields, $dataDir = 'feedback-messages')
    {
        $dir = getenv('YII_ENV') == 'dev'
            ? Yii::getAlias("@frontendWebroot/data/{$dataDir}")
            : Yii::getAlias("@data/{$dataDir}");

        FileHelper::createDirectory($dir, $mode = 0775, $recursive = true);

        foreach ($fields as $field) {
            $file = UploadedFile::getInstance($this, $field . '_local');
            $unique = uniqid();

            if ($file instanceof UploadedFile) {
                $name = "{$this->id}_{$field}_{$unique}.{$file->extension}";
                $path = Yii::getAlias($dir . "/$name");
                $file->saveAs($path, false);

                $this->$field = $name;
                $this->updateAttributes([$field]);
            }
        }
    }
}
