<?php

namespace modules\feedback\common\models;

use Yii;
use yii\helpers\Html;
use yii\validators\EmailValidator;
use yii\validators\Validator;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_feedback_categories".
 *
 * @property integer $id
 * @property string $name
 * @property integer $is_send_to_email
 * @property string $email
 * @property string $template
 *
 * @property Message[] $feedbackMessages
 */
class Category extends \yz\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%feedback_categories}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Категория';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Категории';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $isSendToEmail = Html::getInputId($this, 'is_send_to_email');
        return [
            [['is_send_to_email'], 'integer'],
            [['template'], 'string'],
            [['name', 'email'], 'string', 'max' => 255],

            ['name', 'required'],
            ['email', 'validateEmails'],
            ['email', 'required', 'when' => function() {
                return $this->is_send_to_email;
            }, 'whenClient' => "function() {
                return $('#{$isSendToEmail}:checked').length > 0;
            }"],
        ];
    }

    public function validateEmails()
    {
        $emails = $this->getEmailAsArray();
        $validator = new EmailValidator();

        foreach ($emails as $email) {
            if ($validator->validate($email, $error) == false) {
                $this->addError('email', $error);
                return;
            }
        }
    }

    /**
     * Returns email as array of emails (if there are many in the field)
     * @return array
     */
    public function getEmailAsArray()
    {
        return preg_split('/\s*;\s*/', $this->email, -1, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'is_send_to_email' => 'Отправлять на почту администратору',
            'email' => 'Электронный адрес администратора',
            'template' => 'Шаблон письма',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbackMessages()
    {
        return $this->hasMany(Message::className(), ['category_id' => 'id']);
    }
}
