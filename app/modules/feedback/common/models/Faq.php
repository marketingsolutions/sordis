<?php

namespace modules\feedback\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii2tech\ar\position\PositionBehavior;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_faqs".
 *
 * @property integer $id
 * @property string $question
 * @property string $answer
 * @property integer $is_published
 * @property integer $order
 * @property string $created_at
 * @property string $updated_at
 *
 * @method boolean movePrev() Moves owner record by one position towards the start of the list.
 * @method boolean moveNext() Moves owner record by one position towards the end of the list.
 * @method boolean moveFirst() Moves owner record to the start of the list.
 * @method boolean moveLast() Moves owner record to the end of the list.
 * @method boolean moveToPosition(integer $position) Moves owner record to the specific position.
 */
class Faq extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%faqs}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Вопрос';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Частые вопросы';
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            'position' => [
                'class' => PositionBehavior::class,
                'positionAttribute' => 'order',
            ]
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['question', 'string', 'max' => 255],
            ['answer', 'string'],
            ['is_published', 'integer'],
            ['order', 'integer'],
            ['created_at', 'safe'],
            ['updated_at', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Вопрос',
            'answer' => 'Ответ',
            'is_published' => 'Опубликован',
            'order' => 'Порядок',
            'created_at' => 'Дата добавления',
            'updated_at' => 'Дата изменения',
        ];
    }
}
