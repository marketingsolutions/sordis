<?php

namespace modules\feedback\common\models;

use Yii;
use yii\base\InvalidCallException;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\admin\models\User;
use yz\interfaces\ModelInfoInterface;
use ms\files\attachments\common\contracts\FileAttachmentsOwnerInterface;
use ms\files\attachments\common\models\AttachedFile;
use ms\files\attachments\common\traits\FileAttachmentsTrait;

/**
 * This is the model class for table "yz_feedback_messages_replies".
 *
 * @property integer $id
 * @property integer $message_id
 * @property string $reply
 * @property integer $admin_user_id
 * @property string $created_at
 *
 * @property Message $message
 * @property User adminUser
 */
class Reply extends \yz\db\ActiveRecord implements ModelInfoInterface, FileAttachmentsOwnerInterface
{
    use FileAttachmentsTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%feedback_messages_replies}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Reply';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Replies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_id', 'admin_user_id'], 'integer'],
            [['reply'], 'string'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => null,
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message_id' => 'Message ID',
            'reply' => 'Reply',
            'admin_user_id' => 'Admin User ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessage()
    {
        return $this->hasOne(Message::className(), ['id' => 'message_id']);
    }

    public function saveReply($adminUser = null)
    {
        if ($this->isNewRecord == false) {
            throw new InvalidCallException('You can send only new replies');
        }

        if ($adminUser) {
            $this->admin_user_id = $adminUser->id;
        }

        if ($this->save()) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * @param \yz\admin\models\User $adminUser
     * @return bool
     */
    public function send()
    {
        $this->message->last_reply_sent_at = new Expression('NOW()');
        $this->message->updateAttributes(['last_reply_sent_at']);

        try {
            $message = Yii::$app->mailer
                ->compose('@ms/loyalty/feedback/backend/mails/replyToUser.php', [
                    'question' => $this->message,
                    'reply' => $this,
                ])
                ->setTo($this->message->email);

            $attaches = $this->getAttachedFiles()->all();

            foreach ($attaches as $attach) {
                $pathToFile = Yii::getAlias($attach->file_path . '/' . $attach->file_name);
                $message->attach($pathToFile, ['fileName' => $attach->original_name]);
            }

            $message->send();

            return true;
        }
        catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminUser()
    {
        return $this->hasOne(User::className(), ['id' => 'admin_user_id']);
    }

    /**
     * @param null | string $type
     * @return AttachedFile
     */
    public function instantiateAttachedFile($type = null)
    {
        $attachedFile = AttachedFile::getInstance(self::class, $this->id, '@data/feedback-replies');
        $attachedFile->type = $type;
        return $attachedFile;
    }
}
