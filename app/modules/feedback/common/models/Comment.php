<?php

namespace modules\feedback\common\models;

use Yii;
use yii\base\InvalidCallException;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\admin\models\User;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_feedback_messages_comments".
 *
 * @property integer $id
 * @property integer $message_id
 * @property string $comment
 * @property integer $admin_user_id
 * @property string $created_at
 *
 * @property Message $message
 * @property \yz\admin\models\User $adminUser
 */
class Comment extends \yz\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%feedback_messages_comments}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Comment';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_id', 'admin_user_id'], 'integer'],
            ['comment', 'string'],
            ['comment', 'required'],
            ['created_at', 'safe']
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => null,
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message_id' => 'Message ID',
            'comment' => 'Комментарий',
            'admin_user_id' => 'Admin User ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessage()
    {
        return $this->hasOne(Message::className(), ['id' => 'message_id']);
    }

    /**
     * @param User $adminUser
     * @return bool
     */
    public function add($adminUser = null)
    {
        if ($this->isNewRecord == false) {
            throw new InvalidCallException('You can only new replies');
        }

        if ($adminUser) {
            $this->admin_user_id = $adminUser->id;
        }

        if ($this->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminUser()
    {
        return $this->hasOne(\yz\admin\models\User::className(), ['id' => 'admin_user_id']);
    }
}
