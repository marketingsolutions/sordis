<?php

namespace modules\feedback\common;

/**
 * Class Module
 */
class Module extends \yz\Module
{
    /** @var string */
    public $userAddText = 'Ваше сообщение передано администраторам акции, срок ответа на Ваш вопрос &mdash; 5 рабочих дней';

    public $userReplyText = null;
}