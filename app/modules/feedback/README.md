## Readme:

Add to your config/main.php:

```
'feedback' => [
            'class' => ms\loyalty\feedback\common\Module::class,
        ],
'files-attachments' => [
            'class' => ms\files\attachments\common\Module::class,
        ],
```