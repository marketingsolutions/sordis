<?php

namespace modules\identity\frontend\listeners;
use ms\loyalty\identity\phonesEmails\frontend\widgets\AuthTopBar;
use yii\base\Event;
use ms\loyalty\identity\phonesEmails\frontend\widgets\AuthTopMenu;


/**
 * Class InterfaceWidgets
 */
class InterfaceWidgets 
{
    public static function whenInitCollection(Event $event)
    {
        /** @var \ms\loyalty\contracts\widgets\WidgetsCollectionInterface $dashboard */
        $dashboard = $event->sender;

        $dashboard->addWidget('top-bar-items', AuthTopBar::class);
        $dashboard->addWidget('main-menu-items', AuthTopMenu::class);
    }
}