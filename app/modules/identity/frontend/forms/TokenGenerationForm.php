<?php

namespace modules\identity\frontend\forms;


/**
 * Class TokenGenerationForm
 */
class TokenGenerationForm extends \ms\loyalty\identity\phonesEmails\common\forms\TokenGenerationForm
{
    /**
     * @return bool
     */
    public function generate()
    {
        if ($this->validate() === false) {
            return false;
        }

        $this->generateToken();

        return true;
    }
}