<?php

namespace modules\identity\frontend\forms;

use ms\loyalty\identity\phonesEmails\common\models\EmailRegisterValidationToken;
use ms\loyalty\identity\phonesEmails\common\models\IdentityType;
use ms\loyalty\identity\phonesEmails\common\models\PhoneRegisterValidationToken;
use ms\loyalty\identity\phonesEmails\common\registration\RegistrationByEmailTokenManager;
use ms\loyalty\identity\phonesEmails\common\registration\RegistrationByPhoneTokenManager;
use marketingsolutions\phonenumbers\PhoneNumberBehavior;
use yii\base\Model;


/**
 * Class TokenValidatorForm
 * @property RegistrationByPhoneTokenManager | RegistrationByEmailTokenManager $tokenManager
 */
class TokenValidatorForm extends Model
{
    /**
     * @var string
     */
    public $phoneMobile;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $type;
    /**
     * @var string
     */
    public $token;
    /**
     * @var RegistrationByPhoneTokenManager | RegistrationByEmailTokenManager
     */
    private $tokenManager;

    public function behaviors()
    {
        return [
            'phonenumber' => [
                'class' => PhoneNumberBehavior::className(),
                'attributes' => [
                    'phoneMobileLocal' => 'phoneMobile',
                ],
                'defaultRegion' => 'RU',
            ],
        ];
    }

    public function rules()
    {
        $usePhoneMobile = function () {
            return $this->type == 'phone';
        };
        $useEmail = function () {
            return $this->type == 'email';
        };

        return [
            ['phoneMobileLocal', 'required', 'message' => 'Необходимо указать номер телефона', 'when' => $usePhoneMobile],
            ['email', 'required', 'message' => 'Необходимо указать адрес электронной почты', 'when' => $useEmail],
            ['email', 'email', 'message' => 'Неверный адрес электронной почты', 'when' => $useEmail],
            ['token', 'required', 'message' => 'Необходимо указать код подтверждения'],
            ['token', 'validateToken'],
            ['type', 'in', 'range' => array_keys(IdentityType::getTypeValues())],
        ];
    }

    public function validateToken()
    {
        switch ($this->type) {
            default:
            case IdentityType::PHONE:
                $token = PhoneRegisterValidationToken::compareToken($this->phoneMobile, $this->token);
                if ($token === null) {
                    if (YII_ENV_DEV) {
                        return;
                    }
                    $this->addError('token', 'Неверный код подтверждения');
                    return;
                }

                break;
            case IdentityType::EMAIL:
                $token = EmailRegisterValidationToken::compareToken($this->email, $this->token);
                if ($token === null) {
                    if (YII_ENV_DEV) {
                        return;
                    }
                    $this->addError('token', 'Неверный код подтверждения');
                    // @TODO Add email notification
                    return;
                }

                break;
        }

        $token->delete();
    }

    public function exchange()
    {
        if ($this->validate() === false) {
            return false;
        }

        if ($this->type == IdentityType::PHONE) {
            $this->tokenManager = RegistrationByPhoneTokenManager::create($this->phoneMobile);
        } else {
            $this->tokenManager = RegistrationByEmailTokenManager::create($this->email);
        }

        return true;
    }

    /**
     * @return RegistrationByPhoneTokenManager | RegistrationByEmailTokenManager
     */
    public function getTokenManager()
    {
        return $this->tokenManager;
    }
}