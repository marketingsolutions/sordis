<?php

namespace modules\identity\frontend\forms;

use ms\loyalty\identity\phonesEmails\common\models\Identity;
use ms\loyalty\identity\phonesEmails\common\models\IdentityType;
use ms\loyalty\identity\phonesEmails\common\remind\RemindByEmailTokenManager;
use ms\loyalty\identity\phonesEmails\common\remind\RemindByPhoneTokenManager;
use yii\base\InvalidConfigException;
use yii\base\Model;


/**
 * Class ChangePasswordForm
 */
class ChangePasswordForm extends Model
{
    /**
     * @var string
     */
    public $password;
    /**
     * @var string
     */
    public $passwordCompare;
    /**
     * @var RemindByEmailTokenManager | RemindByPhoneTokenManager
     */
    private $tokenManager;
    /**
     * @var Identity
     */
    private $identity;

    /**
     * @param RemindByEmailTokenManager | RemindByPhoneTokenManager $tokenManager
     * @param array $config
     */
    public function __construct($tokenManager, $config = [])
    {
        $this->tokenManager = $tokenManager;
        parent::__construct($config);
    }

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        if ($this->tokenManager instanceof RemindByEmailTokenManager) {
            $this->identity = Identity::findOne(['login' => $this->tokenManager->getEmail(), 'type' => IdentityType::EMAIL]);
        } else /*($this->tokenManager instanceof RemindByPhoneTokenManager) */{
            $this->identity = Identity::findOne(['login' => $this->tokenManager->getPhoneMobile(), 'type' => IdentityType::PHONE]);
        }
//        else {
//            throw new InvalidConfigException('$tokenManager should be RemindByEmailTokenManager or RemindByPhoneTokenManager');
//        }

        parent::init();
    }

    public function rules()
    {
        return [
            ['password', 'required'],
            ['passwordCompare', 'required'],
            ['passwordCompare', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Новый пароль',
            'passwordCompare' => 'Подтверждение пароля',
        ];
    }

    public function process()
    {
        if ($this->validate() === false) {
            return false;
        }

        $this->identity->passhash = Identity::hashPassword($this->password);
        $this->identity->save(false);

        $this->tokenManager->remove();

        return true;
    }


}