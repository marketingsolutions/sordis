<?php

namespace modules\identity\frontend\forms;
use modules\identity\common\models\IdentityType;
use modules\profiles\common\models\Profile;
use yii\base\Model;
use modules\identity\common\models\Identity;
use marketingsolutions\phonenumbers\PhoneNumberBehavior;


/**
 * Class LoginForm
 * @property string $phone_mobile_local
 * @property string $email
 */
class LoginForm extends Model
{
    /** Period of login via cookie (1 month) */
    const LOGIN_DURATION = 2592000;
    /**
     * @var string
     */
    public $type;
    /**
     * @var string
     */
    public $login;
    /**
     * @var string
     */
    public $password;
    /**
     * @var bool
     */
    public $rememberMe;
    /**
     * @var Identity
     */
    private $identity;

    public function behaviors()
    {
        return [
            'phonenumber' => [
                'class' => PhoneNumberBehavior::className(),
                'attributes' => [
                    'phone_mobile_local' => 'login',
                ],
                'defaultRegion' => 'RU',
            ]
        ];
    }

    public function rules()
    {
        $whenPhone = function () {
            return $this->type == IdentityType::PHONE;
        };
        $whenEmail = function () {
            return $this->type == IdentityType::EMAIL;
        };

        return [
            ['phone_mobile_local', 'required', 'when' => $whenPhone],
            ['email', 'required', 'when' => $whenEmail],
            ['email', 'email', 'when' => $whenEmail],

            ['password', 'required'],
            ['password', 'validatePassword'],

            ['rememberMe', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'login' => 'Номер телефона',
            'phone_mobile_local' => 'Номер телефона',
            'email' => 'Адрес электронной почты',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить меня на этом компьютере',
        ];
    }


    public function validatePassword()
    {
        $identity = $this->getIdentity();

        if ($identity === null && $this->type == IdentityType::PHONE) {
            $this->addError('phone_mobile_local', 'Участник с таким номером телефона не найден');
            return;
        }

        if ($identity === null && $this->type == IdentityType::EMAIL) {
            $this->addError('email', 'Участник с таким адресом электронной почты не найден');
            return;
        }

        if ($identity->validatePassword($this->password) === false) {
            $this->addError('password', 'Неверный пароль');
            return;
        }

        if ($identity->validatePassword($this->password) === true && !$this->isBlocked()){
            $this->addError('password', 'Ваш аккаунт временно заблокирован. Обратитесь к Администратору через раздел Помощь');
        }


    }

    /**
     * @return Identity
     */
    public function getIdentity()
    {
        if ($this->identity == null) {
            $this->identity = Identity::findOne([
                'login' => $this->login,
                'type' => $this->type,
            ]);
        }

        return $this->identity;
    }

    public function isBlocked()
    {
        $profile = Profile::findOne(['phone_mobile' => $this->login]);
        if(!$profile){
            return false;
        }
        if($profile->blocked_at){
            return false;
        }
        return true;
    }

    public function login()
    {
        if ($this->validate() == false) {
            return false;
        }

        $duration = $this->rememberMe ? self::LOGIN_DURATION : 0;

        return \Yii::$app->user->login($this->getIdentity(), $duration);
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->login;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->login = $email;
    }

}