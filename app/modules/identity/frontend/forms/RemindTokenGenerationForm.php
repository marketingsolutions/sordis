<?php

namespace modules\identity\frontend\forms;

use ms\loyalty\identity\phonesEmails\common\models\EmailRemindValidationToken;
use ms\loyalty\identity\phonesEmails\common\models\Identity;
use ms\loyalty\identity\phonesEmails\common\models\IdentityType;
use ms\loyalty\identity\phonesEmails\common\models\PhoneRemindValidationToken;
use marketingsolutions\phonenumbers\PhoneNumberBehavior;
use marketingsolutions\sms\Sms;
use yii\base\Model;
use yii\helpers\Url;


/**
 * Class RemindTokenGenerationForm
 */
class RemindTokenGenerationForm extends Model
{
    /**
     * @var string;
     */
    public $phoneMobile;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $type;

    public function behaviors()
    {
        return [
            'phonenumber' => [
                'class' => PhoneNumberBehavior::className(),
                'attributes' => [
                    'phoneMobileLocal' => 'phoneMobile',
                ],
                'defaultRegion' => 'RU',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'phoneMobileLocal' => 'Номер телефона',
            'email' => 'Адрес электронной почты',
            'type' => 'Способ авторизации',
        ];
    }


    public function rules()
    {
        $usePhoneMobile = function () {
            return $this->type == IdentityType::PHONE;
        };
        $useEmail = function () {
            return $this->type == IdentityType::EMAIL;
        };

        return [
            ['phoneMobileLocal', 'required', 'message' => 'Необходимо указать номер телефона', 'when' => $usePhoneMobile],
            ['phoneMobileLocal', 'exist', 'targetClass' => Identity::className(), 'targetAttribute' => ['phoneMobile' => 'login'],
                'message' => 'Данный номер телефона не зарегистрирован', 'when' => $usePhoneMobile],

            ['email', 'required', 'message' => 'Необходимо указать адрес электронной почты', 'when' => $useEmail],
            ['email', 'email', 'message' => 'Неверный адрес электронной почты', 'when' => $useEmail],
            ['email', 'exist', 'targetClass' => Identity::className(), 'targetAttribute' => ['email' => 'login'],
                'message' => 'Данный адрес электронной почты уже занят', 'when' => $useEmail
            ],
            ['type', 'in', 'range' => array_keys(IdentityType::getTypeValues())],
        ];
    }

    /**
     * @return bool
     */
    public function generate()
    {
        if ($this->validate() === false) {
            return false;
        }

        $this->generateToken();

        return true;
    }

    protected function generateToken()
    {
        switch ($this->type) {
            default:
            case IdentityType::PHONE:
                $token = PhoneRemindValidationToken::createToken($this->phoneMobile);

                /** @var Sms $sms */
                $sms = \Yii::$app->sms;
                $sms->send($this->phoneMobile, 'Код подтверждения: ' . $token->token);
                break;
            case IdentityType::EMAIL:
                $token = EmailRemindValidationToken::createToken($this->email);

                \Yii::$app->mailer
                    ->compose('@ms/loyalty/identity/phonesEmails/common/mail/remindEmail', [
                        'token' => $token->token,
                        'url' => Url::to('@frontendWeb/identity/remind-validation/validate-token?' . http_build_query([
                                'type' => 'email',
                                'email' => $this->email,
                                'token' => $token->token,
                            ]))
                    ])
                    ->setTo($this->email)
                    ->send();
                break;
        }


        return $token;
    }
}