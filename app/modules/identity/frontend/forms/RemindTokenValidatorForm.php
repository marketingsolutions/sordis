<?php

namespace modules\identity\frontend\forms;
use ms\loyalty\identity\phonesEmails\common\models\EmailRemindValidationToken;
use ms\loyalty\identity\phonesEmails\common\models\IdentityType;
use ms\loyalty\identity\phonesEmails\common\models\PhoneRegisterValidationToken;
use ms\loyalty\identity\phonesEmails\common\models\PhoneRemindValidationToken;
use ms\loyalty\identity\phonesEmails\common\registration\RegistrationByPhoneTokenManager;
use ms\loyalty\identity\phonesEmails\common\remind\RemindByEmailTokenManager;
use ms\loyalty\identity\phonesEmails\common\remind\RemindByPhoneTokenManager;
use marketingsolutions\phonenumbers\PhoneNumberBehavior;
use yii\base\Model;


/**
 * Class TokenValidatorForm
 * @property RemindByPhoneTokenManager $tokenManager
 */
class RemindTokenValidatorForm extends Model
{
    /**
     * @var string;
     */
    public $phoneMobile;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $type;
    /**
     * @var string
     */
    public $token;
    /**
     * @var RemindByPhoneTokenManager
     */
    private $tokenManager;

    public function behaviors()
    {
        return [
            'phonenumber' => [
                'class' => PhoneNumberBehavior::className(),
                'attributes' => [
                    'phoneMobileLocal' => 'phoneMobile',
                ],
                'defaultRegion' => 'RU',
            ],
        ];
    }

    public function rules()
    {
        $usePhoneMobile = function () {
            return $this->type == IdentityType::PHONE;
        };
        $useEmail = function () {
            return $this->type == IdentityType::EMAIL;
        };

        return [
            ['phoneMobileLocal', 'required', 'message' => 'Необходимо указать номер телефона', 'when' => $usePhoneMobile],
            ['email', 'required', 'message' => 'Необходимо указать адрес электронной почты', 'when' => $useEmail],
            ['email', 'email', 'when' => $useEmail],

            ['token', 'required', 'message' => 'Необходимо указать код подтверждения'],
            ['token', 'validateToken'],

            ['type', 'required'],
            ['type', 'in', 'range' => array_keys(IdentityType::getTypeValues())],
        ];
    }

    public function validateToken()
    {
        switch ($this->type) {
            default:
            case IdentityType::PHONE:
                $token = PhoneRemindValidationToken::compareToken($this->phoneMobile, $this->token);
                if ($token === null) {
                    if (YII_ENV_DEV) {
                        return;
                    }
                    $this->addError('token', 'Неверный код подтверждения');
                    return;
                }

                break;
            case IdentityType::EMAIL:
                $token = EmailRemindValidationToken::compareToken($this->email, $this->token);
                if ($token === null) {
                    if (YII_ENV_DEV) {
                        return;
                    }
                    $this->addError('token', 'Неверный код подтверждения');
                    return;
                }

                break;
        }

        $token->delete();
    }

    public function exchange()
    {
        if ($this->validate() === false) {
            return false;
        }

        if ($this->type == IdentityType::PHONE) {
            $this->tokenManager = RemindByPhoneTokenManager::create($this->phoneMobile);
        } else {
            $this->tokenManager = RemindByEmailTokenManager::create($this->email);
        }

        return true;
    }

    /**
     * @return RemindByPhoneTokenManager
     */
    public function getTokenManager()
    {
        return $this->tokenManager;
    }
}