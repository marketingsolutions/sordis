<?php

namespace modules\identity\frontend\controllers;
use modules\identity\frontend\forms\TokenGenerationForm;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;


/**
 * Class RegistrationValidationController
 */
class ValidationController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => false,
                    ]
                ]
            ]
        ];
    }

    public function actionRegistration()
    {
        return $this->render('registration');
    }

    public function actionRemind()
    {
        return $this->render('remind');
    }


}