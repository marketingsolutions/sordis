<?php

namespace modules\identity\frontend\controllers\api;
use ms\loyalty\identity\phonesEmails\frontend\forms\TokenValidatorForm;
use yii\base\InvalidParamException;
use yii\rest\Controller;
use ms\loyalty\identity\phonesEmails\frontend\forms\TokenGenerationForm;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;


/**
 * Class RegistrationValidationController
 */
class RegistrationValidationController extends Controller
{
    public function behaviors()
    {
        $behaviors = ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?']
                    ],
                ]
            ]
        ]);
        unset($behaviors['authenticator']);
        return $behaviors;
    }

    public function beforeAction($action)
    {
        \Yii::$app->request->parsers['application/json'] = 'yii\web\JsonParser';
        return parent::beforeAction($action);
    }

    public function actionGenerateToken()
    {
        $model = new TokenGenerationForm();

        $model->load(\Yii::$app->request->getBodyParams(), '');

        if ($model->generate()) {
            return ['success' => true];
        }

        return $model;
    }

    public function actionValidateToken()
    {
        $model = new TokenValidatorForm();

        $model->load(\Yii::$app->request->bodyParams, '');

        if ($model->exchange()) {
            return ['url' => $model->tokenManager->getUrl()];
        }

        return $model;
    }
}