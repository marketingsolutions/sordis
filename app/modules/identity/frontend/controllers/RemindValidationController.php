<?php

namespace modules\identity\frontend\controllers;
use modules\identity\frontend\forms\RemindTokenValidatorForm;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


/**
 * Class RemindValidationController
 */
class RemindValidationController extends Controller
{
    public function behaviors()
    {
        $behaviors = ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?']
                    ],
                ]
            ]
        ]);
        return $behaviors;
    }

    public function actionValidateToken()
    {
        $model = new RemindTokenValidatorForm();

        $model->load(\Yii::$app->request->get(), '');

        if ($model->exchange()) {
            return $this->redirect($model->tokenManager->getUrl());
        }

        throw new NotFoundHttpException('Неверный код подтверждения');
    }
}