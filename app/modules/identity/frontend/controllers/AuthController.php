<?php

namespace modules\identity\frontend\controllers;
use modules\identity\common\models\IdentityType;
use modules\identity\common\remind\RemindTokenProvider;
use modules\identity\frontend\forms\ChangePasswordForm;
use modules\identity\frontend\forms\LoginForm;
use yii\base\DynamicModel;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yz\Yz;


/**
 * Class AuthController
 */
class AuthController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'register', 'remind'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                        'actions' => ['change-password'],
                        'matchCallback' => function ($rule, $action) {
                            $tokenProvider = new RemindTokenProvider();
                            $tokenManager = $tokenProvider->findFromRequest(\Yii::$app->request);
                            return $tokenManager === null;
                        },
                        'denyCallback' => function ($rule, $action) {
                            \Yii::$app->session->setFlash(Yz::FLASH_ERROR, 'Извините, Вы должны подтвердить свои данные');
                            \Yii::$app->response->redirect(Url::home());
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['change-password'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => false,
                    ]
                ]
            ]
        ];
    }


    public function actionLogin($type = null)
    {
        $validation = DynamicModel::validateData(compact('type'), [
            ['type', 'required'],
            ['type', 'in', 'range' => array_keys(IdentityType::getTypeValues())],
        ]);

        if ($validation->hasErrors()) {
            $type = IdentityType::PHONE;
        }

        $model = new LoginForm([
            'type' => $type,
        ]);

        if ($model->load(\Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/dashboard/index']);
        }

        return $this->render('login', compact('model'));
    }

    public function actionLogout()
    {
        \Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionRemind()
    {
        return $this->redirect(array_merge(
            ['/identity/validation/remind'],
            \Yii::$app->request->queryParams)
        );
    }

    public function actionChangePassword()
    {
        $tokenManager = (new RemindTokenProvider())->findFromRequest(\Yii::$app->request);
        if ($tokenManager === null) {
            throw new NotFoundHttpException();
        }

        $model = new ChangePasswordForm($tokenManager);

        if ($model->load(\Yii::$app->request->post()) && $model->process()) {
            return $this->redirect(['login']);
        }

        return $this->render('change-password', compact('model'));
    }

    public function actionRegister()
    {
        return $this->redirect(array_merge(
            ['/identity/validation/registration'],
            \Yii::$app->request->queryParams
        ));
    }
}