<?php

namespace modules\identity\frontend\assets;

use marketingsolutions\assets\AngularAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;
use yii\widgets\MaskedInputAsset;
use yz\icons\FontAwesomeAsset;


/**
 * Class PhoneEmailValidationAsset
 */
class PhoneEmailValidationAsset extends AssetBundle
{
    public $sourcePath = '@ms/loyalty/identity/phonesEmails/frontend/assets/phone-email-validation';

    public $js = [
        'app.js',
    ];

    public $depends = [
        YiiAsset::class,
        AngularAsset::class,
        FontAwesomeAsset::class,
        MaskedInputAsset::class
    ];
}