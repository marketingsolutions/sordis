var phoneEmailValidation = (function () {
    var settings = {
        generateTokenUrl: '',
        validateTokenUrl: ''
    };

    var app = angular

            .module('PhoneEmailValidation', [])

            .controller('Validation', ['$scope', '$http',
                function ($scope, $http) {
                    $scope.type = 'phone';
                    $scope.email = '';
                    $scope.phone = '';
                    $scope.token = '';
                    $scope.tokenRequested = false;
                    $scope.errors = {};
                    $scope.disabled = false;

                    $scope.generateToken = function () {
                        $scope.disabled = true;
                        $scope.errors = {};
                        $http.post(settings.generateTokenUrl, {
                            phoneMobileLocal: $scope.phone,
                            email: $scope.email,
                            type: $scope.type
                        })
                            .success(function () {
                                $scope.token = '';
                                $scope.tokenRequested = true;
                                $scope.disabled = false;
                            })
                            .error(function (errors) {
                                $scope.disabled = false;
                                $scope.tokenRequested = false;
                                $scope.errors = errors;
                            });
                    };

                    $scope.validateToken = function () {
                        $scope.disabled = true;
                        $scope.errors = {};
                        $http.post(settings.validateTokenUrl, {
                            phoneMobileLocal: $scope.phone,
                            email: $scope.email,
                            type: $scope.type,
                            token: $scope.token
                        })
                            .success(function (result) {
                                window.location = result.url;
                            })
                            .error(function (errors) {
                                $scope.disabled = false;
                                $scope.tokenRequested = true;
                                $scope.errors = errors;
                            });
                    };

                    $scope.reenterPhoneEmail = function () {
                        $scope.tokenRequested = false;
                        $scope.phone = '';
                        $scope.email = '';
                        $scope.errors = {};
                        $scope.token = '';
                    }
                }
            ])

            .directive('inputMask', function () {
                return {
                    restrict: 'A',
                    link: function (scope, el, attrs) {
                        function update(val) {
                            scope.$eval(attrs.ngModel + "='" + val + "'");
                        }
                        var options = scope.$eval(attrs.inputMask);
                        options['onKeyPress'] = function() {
                            update($(this).val());
                        };
                        $(el).inputmask(options);
                        $(el).on('change', function () {
                            update($(this).val());
                        });
                    }
                };
            })
        ;

    return {
        settings: settings
    };
})();