<?php
use modules\identity\frontend\assets\PhoneEmailValidationAsset;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 */

$settings = [
    'generateTokenUrl' => Url::to(['/identity/api/remind-validation/generate-token']),
    'validateTokenUrl' => Url::to(['/identity/api/remind-validation/validate-token']),
];

$js =<<<JS
phoneEmailValidation.settings.generateTokenUrl = '{$settings['generateTokenUrl']}';
phoneEmailValidation.settings.validateTokenUrl = '{$settings['validateTokenUrl']}';
JS;
$this->registerJs($js);

$this->params['header'] = 'Восстановление пароля';

?>

<?php $this->beginBlock('app-prefix-text') ?>

<p>Для восстановления пароля выберите тип логина, который Вы использовали для регистрации и запросите код подтверждения</p>

<?php $this->endBlock() ?>

<?= $this->render('partials/_app', ['validateCodeText' => 'Проверить код и сменить пароль']) ?>