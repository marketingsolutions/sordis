<?php

use modules\identity\common\models\IdentityType;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \ms\loyalty\identity\phonesEmails\frontend\forms\LoginForm $model
 */

//$this->params['header'] = 'Вход в личный кабинет';

?>

<div class="col-md-8 col-md-offset-2">
  <div class="panel panel_ferma">
    <h1 class="center">Вход в личный кабинет</h1>
    <div class="panel-body">
      <?php $form = \yii\bootstrap\ActiveForm::begin(['layout' => 'horizontal']) ?>
      <?php if ($model->type == IdentityType::PHONE): ?>
          <?= $form->field($model, 'phone_mobile_local')->widget(\yii\widgets\MaskedInput::className(), [
              'mask' => '+7 999 999-99-99',
          ]) ?>
      <?php endif ?>

      <?php if ($model->type == IdentityType::EMAIL): ?>
          <?= $form->field($model, 'email') ?>
      <?php endif ?>

      <?= $form->field($model, 'password')->passwordInput() ?>
      <?= $form->field($model, 'rememberMe')->checkbox() ?>

      <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3 remind-pass">
          <?= Html::submitButton('Войти', ['class' => 'btn btn_all']) ?>
          <a href="<?= Url::to(['remind']) ?>">Забыли свой пароль?</a>
        </div>
      </div>

      <?php \yii\bootstrap\ActiveForm::end() ?>
    </div>
  </div>
</div>
