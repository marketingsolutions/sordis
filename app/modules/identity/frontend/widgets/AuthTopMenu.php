<?php

namespace modules\identity\frontend\widgets;
use yii\base\Widget;


/**
 * Class AuthTopMenu
 */
class AuthTopMenu extends Widget
{
    public function run()
    {
        return $this->render('auth-top-menu');
    }

}