<?php

namespace modules\identity\frontend\widgets;
use yii\base\Widget;


/**
 * Class AuthTopBar
 */
class AuthTopBar extends Widget
{
    public function run()
    {
        return $this->render('auth-top-bar');
    }
}