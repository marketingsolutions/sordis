<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 */
?>

<?= \yii\widgets\Menu::widget([
    'options' => [
        'tag' => false
    ],
    'items' => [
//        ['label' => 'Войти', 'url' => ['/identity/auth/login'], 'visible' => Yii::$app->user->isGuest],
//        ['label' => 'Регистрация', 'url' => ['/identity/auth/register'], 'visible' => Yii::$app->user->isGuest],
        [
            'label' => 'ВЫХОД',
            'url' => ['/identity/auth/logout'],
            'class' => ['logout-link'],
            'visible' => ! Yii::$app->user->isGuest
        ],
    ]
]) ?>
