<?php

namespace modules\identity\common\registration;

use ms\loyalty\contracts\identities\TokenProvidesPhoneMobileInterface;
use ms\loyalty\contracts\identities\RegistrationTokenInterface;
use ms\loyalty\contracts\identities\RegistrationTokenManagerInterface;
use marketingsolutions\tokens\algorithms\RandomString;
use marketingsolutions\tokens\Token;
use yii\helpers\Url;


/**
 * Class RemindByPhoneTokenManager
 */
class RegistrationByPhoneTokenManager implements RegistrationTokenManagerInterface, TokenProvidesPhoneMobileInterface
{
    const TOKEN_TYPE = 'identity.phone.registration';

    /**
     * @var Token
     */
    private $token;

    function __construct(Token $token)
    {
        $this->token = $token;
    }


    public static function create($phoneMobile)
    {
        $token = Token::create(self::TOKEN_TYPE, $phoneMobile, new RandomString());
        return new static($token);
    }

    public static function compare($phoneMobile, $token)
    {
        $token = Token::compare(self::TOKEN_TYPE, $phoneMobile, $token);
        if ($token === null) {
            return null;
        }

        return new static($token);
    }

    public function remove()
    {
        Token::remove(self::TOKEN_TYPE, $this->token->name);
    }

    /**
     * @return Token
     */
    public function getToken()
    {
        return $this->token;
    }

    public function getUrl()
    {
        return Url::to(['/profiles/registration/index', 'type' => 'phone', 'phone' => $this->getPhoneMobile(), 'token' => $this->token->token]);
    }

    /**
     * @return string
     */
    public function getPhoneMobile()
    {
        return $this->token->name;
    }
}