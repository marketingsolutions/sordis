<?php

namespace modules\identity\common\registration;

use ms\loyalty\contracts\identities\RegistrationTokenManagerInterface;
use ms\loyalty\contracts\identities\RegistrationTokenProviderInterface;
use ms\loyalty\identity\phonesEmails\common\models\IdentityType;
use yii\web\Request;


/**
 * Class RegistrationTokenProvider
 */
class RegistrationTokenProvider implements RegistrationTokenProviderInterface
{
    /**
     * @param Request $request
     * @return RegistrationTokenManagerInterface
     */
    public function findFromRequest(Request $request)
    {
        $type = $request->get('type');
        $token = $request->get('token');

        switch ($type) {
            default:
            case IdentityType::PHONE:
                $phone = $request->get('phone');
                return RegistrationByPhoneTokenManager::compare($phone, $token);
            case IdentityType::EMAIL:
                $email = $request->get('email');
                return RegistrationByEmailTokenManager::compare($email, $token);
        }
    }
}