<?php

namespace modules\identity\common\registration;

use ms\loyalty\contracts\identities\TokenProvidesEmailInterface;
use ms\loyalty\contracts\identities\RegistrationTokenManagerInterface;
use marketingsolutions\tokens\algorithms\RandomString;
use marketingsolutions\tokens\Token;
use yii\helpers\Url;


/**
 * Class RegistrationByEmailTokenManager
 */
class RegistrationByEmailTokenManager implements RegistrationTokenManagerInterface, TokenProvidesEmailInterface
{
    const TOKEN_TYPE = 'identity.email.registration';

    /**
     * @var Token
     */
    private $token;

    function __construct(Token $token)
    {
        $this->token = $token;
    }


    public static function create($email)
    {
        $token = Token::create(self::TOKEN_TYPE, $email, new RandomString());
        return new static($token);
    }

    public static function compare($email, $token)
    {
        $token = Token::compare(self::TOKEN_TYPE, $email, $token);
        if ($token === null) {
            return null;
        }

        return new static($token);
    }

    public function remove()
    {
        Token::remove(self::TOKEN_TYPE, $this->token->name);
    }

    /**
     * @return Token
     */
    public function getToken()
    {
        return $this->token;
    }

    public function getUrl()
    {
        return Url::to(['/profiles/registration/index', 'type' => 'email', 'email' => $this->getEmail(), 'token' => $this->token->token]);
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->token->name;
    }
}