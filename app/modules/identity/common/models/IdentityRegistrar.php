<?php

namespace modules\identity\common\models;
use ms\loyalty\contracts\identities\IdentityRegistrarInterface;
use ms\loyalty\contracts\identities\IdentityRoleInterface;
use ms\loyalty\contracts\identities\RegistrationTokenManagerInterface;
use ms\loyalty\contracts\identities\TokenProvidesEmailInterface;
use ms\loyalty\contracts\identities\TokenProvidesPhoneMobileInterface;
use ms\loyalty\contracts\profiles\HasPhoneMobileInterface;
use yii\base\InvalidParamException;


/**
 * Class IdentityRegistrar
 */
class IdentityRegistrar implements IdentityRegistrarInterface
{
    /**
     * @param HasPhoneMobileInterface $profile
     * @param string $password
     * @param RegistrationTokenManagerInterface $tokenManager
     * @return \yii\web\IdentityInterface
     */
    public function createForProfile($profile, $password, $tokenManager = null)
    {
        $identity = new Identity();
        $identity->role = 'identity';
        $identity->passhash = Identity::hashPassword($password);

        if ($tokenManager instanceof TokenProvidesEmailInterface) {
            $identity->login = $tokenManager->getEmail();
            $identity->type = IdentityType::EMAIL;
        } else if ($tokenManager instanceof TokenProvidesPhoneMobileInterface) {
            $identity->login = $tokenManager->getPhoneMobile();
            $identity->type = IdentityType::PHONE;
        } else {
            throw new InvalidParamException('$tokenManager argument must implement TokenProvidesEmailInterface or TokenProvidesPhoneMobileInterface');
        }

        if ($profile instanceof IdentityRoleInterface) {
            $identity->role = $profile->getIdentityRole();
        }

        $identity->save();

        return $identity;
    }

    /**
     * Removes identity by the given id
     * @param $id
     */
    public function removeIdentity($id)
    {
        /** @var Identity $identity */
        $identity = Identity::findOne($id);
        if ($identity) {
            $identity->delete();
        }
    }
}