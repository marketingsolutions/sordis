<?php

namespace modules\identity\common\models;

use ms\loyalty\contracts\identities\IdentityInterface as loyaltyIdentityInterface;
use ms\loyalty\contracts\profiles\ProfileFinderInterface;
use ms\loyalty\contracts\profiles\ProfileInterface;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\web\IdentityInterface;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_identities".
 *
 * @property integer $id
 * @property string $type
 * @property string $role
 * @property string $login
 * @property string $passhash
 * @property string $auth_key
 * @property string $created_at
 * @property string $updated_at
 */
class Identity extends \yz\db\ActiveRecord implements ModelInfoInterface,
    IdentityInterface, loyaltyIdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%identities}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Identity';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Identities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role'], 'string', 'max' => 32],
            [['login', 'passhash', 'auth_key'], 'string', 'max' => 255]
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'role' => 'Роль',
            'login' => 'Логин',
            'passhash' => 'Passhash',
            'auth_key' => 'Auth Key',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->auth_key = Yii::$app->security->generateRandomString(32);
        }

        return parent::beforeSave($insert);
    }


    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @param $password
     *
     * @return string
     */
    public static function hashPassword($password)
    {
        return \Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @param $password
     *
     * @return bool
     */
    public function validatePassword($password)
    {
        if ($this->isNewRecord) {
            return false;
        }

        return \Yii::$app->security->validatePassword($password, $this->passhash);
    }

    /**
     * @return ProfileInterface
     */
    public function getProfile()
    {
        /** @var ProfileFinderInterface $finder */
        $finder = Yii::$container->get(ProfileFinderInterface::class);
        return $finder->findByIdentityId($this->id);
    }
}
