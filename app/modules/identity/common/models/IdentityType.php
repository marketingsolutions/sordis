<?php

namespace modules\identity\common\models;


/**
 * Class IdentityType
 */
class IdentityType
{
    const EMAIL = 'email';
    const PHONE = 'phone';

    public static function getTypeValues()
    {
        return [
            self::EMAIL => 'Электронная почта',
            self::PHONE => 'Номер телефона',
        ];
    }
}