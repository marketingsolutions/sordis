<?php

namespace modules\identity\common\models;


/**
 * Class PhoneRegisterValidationToken
 */
class PhoneRegisterValidationToken extends PhoneValidationToken
{
    public static function tokenType()
    {
        return 'identity.phoneRegisterValidation';
    }
}