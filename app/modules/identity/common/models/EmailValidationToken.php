<?php

namespace modules\identity\common\models;
use marketingsolutions\tokens\algorithms\Numbers;
use marketingsolutions\tokens\Token;
use yii\base\InvalidCallException;


/**
 * Class EmailValidationToken
 */
abstract class EmailValidationToken extends Token
{
    /**
     * @param $email
     * @return static
     */
    public static function createToken($email)
    {
        return static::create(static::tokenType(), $email, new Numbers(['length' => 6]));
    }

    /**
     * @param $email
     * @param $token
     * @return null|Token
     */
    public static function compareToken($email, $token)
    {
        return static::compare(static::tokenType(), $email, $token);
    }

    /**
     * @return string
     */
    public static function tokenType()
    {
        throw new InvalidCallException();
    }
}