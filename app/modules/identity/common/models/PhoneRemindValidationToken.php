<?php

namespace modules\identity\common\models;


/**
 * Class PhoneRemindValidationToken
 */
class PhoneRemindValidationToken extends PhoneValidationToken
{
    public static function tokenType()
    {
        return 'identity.phoneRemindValidation';
    }
}