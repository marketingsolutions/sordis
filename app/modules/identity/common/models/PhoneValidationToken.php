<?php

namespace modules\identity\common\models;
use marketingsolutions\tokens\algorithms\Numbers;
use marketingsolutions\tokens\Token;
use yii\base\InvalidCallException;


/**
 * Class PhoneValidationToken
 */
abstract class PhoneValidationToken extends Token
{
    /**
     * @param $phone
     * @return static
     */
    public static function createToken($phone)
    {
        return static::create(static::tokenType(), $phone, new Numbers(['length' => 6]));
    }

    /**
     * @param $phone
     * @param $token
     * @return null|Token
     */
    public static function compareToken($phone, $token)
    {
        return static::compare(static::tokenType(), $phone, $token);
    }

    /**
     * @return string
     */
    public static function tokenType()
    {
        throw new InvalidCallException();
    }
}