<?php

namespace modules\identity\common\models;


/**
 * Class EmailRemindValidationToken
 */
class EmailRemindValidationToken extends EmailValidationToken
{
    public static function tokenType()
    {
        return 'identity.emailRemindValidation';
    }
}