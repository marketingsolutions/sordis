<?php

namespace modules\identity\common\models;


/**
 * Class EmailRegisterValidationToken
 */
class EmailRegisterValidationToken extends EmailValidationToken
{
    public static function tokenType()
    {
        return 'identity.emailRegisterValidation';
    }
}