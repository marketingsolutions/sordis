<?php

namespace modules\identity\common\remind;

use ms\loyalty\contracts\identities\RegistrationTokenManagerInterface;
use ms\loyalty\contracts\identities\RegistrationTokenProviderInterface;
use ms\loyalty\identity\phonesEmails\common\models\IdentityType;
use yii\web\Request;


/**
 * Class RegistrationTokenProvider
 */
class RemindTokenProvider implements RegistrationTokenProviderInterface
{
    /**
     * @param Request $request
     * @return RemindByEmailTokenManager | RemindByPhoneTokenManager
     */
    public function findFromRequest(Request $request)
    {
        $type = $request->get('type');

        $token = $request->get('token');

        if ($type == IdentityType::EMAIL) {
            $email = $request->get('email');
            return RemindByEmailTokenManager::compare($email, $token);
        } else {
            $phone = $request->get('phone');
            return RemindByPhoneTokenManager::compare($phone, $token);
        }
    }
}