<?php

namespace modules\identity\common\crossauth;
use ms\loyalty\contracts\identities\IdentityInterface;
use ms\loyalty\crossauth\common\contracts\CrossauthIdentitiesManagerInterface;
use modules\identity\common\models\Identity;
use yii\web\Application;


/**
 * Class CrossauthIdentitiesManager is used when marketingsolutions/loyalty-crossauth module is installed
 */
class CrossauthIdentitiesManager implements CrossauthIdentitiesManagerInterface
{

    /**
     * Should return id of the identity that can be used cross sites
     * @param IdentityInterface $identity
     * @return string
     */
    public function getIdentityId(IdentityInterface $identity)
    {
        /** @var Identity $identity  */
        return $identity->login;
    }

    /**
     * Should be able to login by cross site identity id
     * @param $identityId
     * @param Application $app
     * @return bool
     */
    public function login($identityId, Application $app)
    {
        /** @var Identity $identity */
        $identity = Identity::findOne(['login' => $identityId]);
        if ($identity === null) {
            return false;
        }

        return $app->user->login($identity);
    }
}