<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \yii\mail\BaseMessage $message instance of newly created mail message
 * @var string $token
 * @var string $url
 */
$message->setSubject('Подтверждение адреса электронной почты');
?>

<p>Уважаемый участник,</p>

<p>
    Для подтверждения Вашего адреса электронной почты, пожалуйста перейдите по ссылке:
    <a href="<?= $url ?>"><?= $url ?></a>
</p>

<p>
    или введите код подтверждения: <strong><?= $token ?></strong>
</p>



