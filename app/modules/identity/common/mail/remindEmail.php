<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \yii\mail\BaseMessage $message instance of newly created mail message
 * @var string $token
 * @var string $url
 */
$message->setSubject('Запрос восстановления пароля');
?>

<p>Уважаемый участник,</p>

<p>
    Для восстановления Вашего пароля, пожалуйста перейдите по ссылке
    <a href="<?= $url ?>"><?= $url ?></a>
</p>

<p>
    или введите код подтверждения: <strong><?= $token ?></strong>
</p>

<p><strong>Если Вы не понимаете о чем идет речь, просто проигнорируйте данное письмо</strong></p>



