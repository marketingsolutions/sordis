<?php

namespace modules\courses\backend\models;

use modules\courses\common\models\CourseTest;
use modules\courses\common\models\CourseTry;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\Region;
use yii\db\ActiveQuery;
use yz\admin\search\WithExtraColumns;


class CourseTryWithData extends CourseTry
{
    use WithExtraColumns;

    protected static function extraColumns()
    {
        return [
			'profile__full_name',
			'profile__phone_mobile',
			'test__title',
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), static::extraColumns());
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
			'profile__full_name' => 'Участник',
			'profile__phone_mobile' => 'Телефон',
			'test__title' => 'Тест',
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()
            ->select(static::selectWithExtraColumns(['try.*']))
            ->from(['try' => self::tableName()])
			->leftJoin(['profile' => Profile::tableName()], 'profile.id = try.profile_id')
			->leftJoin(['test' => CourseTest::tableName()], 'test.id = try.test_id');
            //->leftJoin(['region' => Region::tableName()], 'region.id = profile.region_id');
    }
}