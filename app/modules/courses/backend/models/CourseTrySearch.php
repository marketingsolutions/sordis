<?php

namespace modules\courses\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;

/**
 * CourseTrySearch represents the model behind the search form about `modules\courses\common\models\CourseTry`.
 */
class CourseTrySearch extends CourseTryWithData implements SearchModelInterface

{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'profile_id', 'test_id', 'position', 'step', 'correct_answers', 'wrong_answers', 'test_bonuses', 'test_questions', 'bonuses', 'finished', 'best_try', 'paid', 'extra_bonuses'], 'integer'],
            [['paid_at', 'created_at', 'updated_at'], 'safe'],
			[static::extraColumns(), 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        return CourseTryWithData::find()->orderBy(['created_at' => SORT_DESC]);
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'try.id' => $this->id,
            'try.profile_id' => $this->profile_id,
            'try.test_id' => $this->test_id,
            'try.position' => $this->position,
            'try.step' => $this->step,
            'try.correct_answers' => $this->correct_answers,
            'try.wrong_answers' => $this->wrong_answers,
            'try.test_bonuses' => $this->test_bonuses,
            'try.test_questions' => $this->test_questions,
            'try.bonuses' => $this->bonuses,
            'try.finished' => $this->finished,
            'try.best_try' => $this->best_try,
            'try.paid' => $this->paid,
            'try.extra_bonuses' => $this->extra_bonuses,
        ]);

		static::filtersForExtraColumns($query);
    }
}
