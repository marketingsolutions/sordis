<?php

namespace modules\courses\backend;

use yz\icons\Icons;

/**
 * Class Module
 */
class Module extends \modules\courses\common\Module
{
    public function getAdminMenu()
    {
		return [
			[
				'label' => 'Обучение',
				'icon' => Icons::o('user'),
				'items' => [
					[
						'route' => ['/courses/courses/index'],
						'label' => 'Образовательные материалы',
						'icon' => Icons::o('list'),
					],
//					[
//						'route' => ['/courses/course-lessons/index'],
//						'label' => 'Уроки курсов',
//						'icon' => Icons::o('list'),
//					],
					[
						'route' => ['/courses/course-tests/index'],
						'label' => 'Тесты',
						'icon' => Icons::o('list'),
					],
					[
						'route' => ['/courses/course-questions/index'],
						'label' => 'Вопросы к тестам',
						'icon' => Icons::o('list'),
					],
					[
						'route' => ['/courses/course-answers/index'],
						'label' => 'Ответы к вопросам',
						'icon' => Icons::o('list'),
					],
					[
						'route' => ['/courses/course-tries/index'],
						'label' => 'Прохождение тестов',
						'icon' => Icons::o('list'),
					],
				]
			],
		];
    }

}