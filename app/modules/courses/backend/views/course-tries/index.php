<?php

use modules\courses\common\models\CourseTry;
use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use yz\icons\Icons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\courses\backend\models\CourseTrySearch $searchModel
 * @var array $columns
 */

$this->title = modules\courses\common\models\CourseTry::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'course-try-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [/*['search'],*/ ['export', 'delete', 'return']],
            'gridId' => 'course-try-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\courses\common\models\CourseTry',
        ]) ?>
    </div>

    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'id' => 'course-try-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge([
            [
				'class' => 'yz\admin\widgets\ActionColumn',
				'template' => '{pay} {delete}',
				'buttons' => [
					'pay' => function ($url, CourseTry $model) {
//        				if(CourseTry::isTestComplete($model->id)) {
//							return Html::a(Icons::i('rub'), $url, [
//								'title' => 'Оплатить эту попылку отдельно',
//								'data-confirm' => 'Вы действительно хотите Оплатить эту попылку отдельно?',
//								'data-method' => 'post',
//								'class' => 'btn btn-warning btn-sm',
//								'data-pjax' => '0',
//							]);
						//}else{
							return "";
						//}
					},
				],
			],
        ], $columns),
    ]); ?>
<?php Box::end() ?>
