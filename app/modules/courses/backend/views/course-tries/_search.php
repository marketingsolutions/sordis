<?php

use yii\helpers\Html;
use yz\admin\widgets\ActiveForm;
use yz\admin\widgets\FormBox;

/**
 * @var yii\web\View $this
 * @var modules\courses\backend\models\CourseTrySearch $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<div class="course-try-search hidden" id="filter-search">
    <?php $box = FormBox::begin() ?>
    <?php $box->beginBody() ?>
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'fieldConfig' => [
            'horizontalCssClasses' => ['label' => 'col-sm-3', 'input' => 'col-sm-5', 'offset' => 'col-sm-offset-3 col-sm-5'],
        ],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'profile_id') ?>

    <?= $form->field($model, 'test_id') ?>

    <?= $form->field($model, 'position') ?>

    <?= $form->field($model, 'step') ?>

    <?php // echo $form->field($model, 'correct_answers') ?>

    <?php // echo $form->field($model, 'wrong_answers') ?>

    <?php // echo $form->field($model, 'test_bonuses') ?>

    <?php // echo $form->field($model, 'test_questions') ?>

    <?php // echo $form->field($model, 'bonuses') ?>

    <?php // echo $form->field($model, 'finished') ?>

    <?php // echo $form->field($model, 'best_try') ?>

    <?php // echo $form->field($model, 'paid') ?>

    <?php // echo $form->field($model, 'paid_at') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'extra_bonuses') ?>

        <?php  $box->endBody() ?>
        <?php  $box->beginFooter() ?>
            <?= Html::submitButton(\Yii::t('admin/t','Search'), ['class' => 'btn btn-primary']) ?>
        <?php  $box->endFooter() ?>

    <?php ActiveForm::end(); ?>
    <?php  FormBox::end() ?>
</div>
