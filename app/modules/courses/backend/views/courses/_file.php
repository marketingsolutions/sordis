<?php if (!empty($model->$field)): ?>
    <div class="row" style="margin-bottom:20px;">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <a href="<?= Yii::getAlias('@backendWeb/data/courses/' . $model->$field) ?>"
			   target="_blank" class="btn btn-default">
                <?php if (strpos($model->$field, '.pdf') !== false): ?>
                    PDF-документ
				<?php elseif (strpos($model->$field, '.ppt') !== false || strpos($model->$field, '.pptx') !== false): ?>
					Презентация PowerPoint
                <?php else: ?>
                    Файл: <?= $model->$field ?>
                <?php endif; ?>
            </a>
        </div>
    </div>
<?php endif; ?>