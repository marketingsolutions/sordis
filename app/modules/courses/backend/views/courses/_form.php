<?php

use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var modules\courses\common\models\Course $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php  $box = FormBox::begin(['cssClass' => 'course-form box-primary', 'title' => '']) ?>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php $box->beginBody() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'enabled')->checkbox() ?>
	<?= $form->field($model, 'position')->input('number')->staticControl() ?>

	<?= $form->field($model, 'frame_url')->textInput() ?>
	<?= $form->field($model, 'frame_width')->input('number') ?>
	<?= $form->field($model, 'frame_height')->input('number') ?>

	<?= $this->render('_file', ['model' => $model, 'field' => 'file']); ?>
	<?= $form->field($model, 'file_local')->fileInput()
		->label('Файл PDF. Максимальный размер загружаемого файла - 100 МБ') ?>

	<?php for ($i = 1; $i <= 5; $i++): ?>
		<?= $form->field($model, "video_{$i}_title") ?>
		<?= $form->field($model, "video_{$i}_frame")->textarea(['rows' => 3])
			->hint('Открываете страницу с видео YouTube "https://www.youtube.com/watch?v=yFp_UA3gUsE", там кнопка "Поделиться", вкладка "HTML-код"') ?>
	<?php endfor; ?>

    <?php $box->endBody() ?>

    <?php $box->actions([
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
    ]) ?>
    <?php ActiveForm::end(); ?>

<?php  FormBox::end() ?>
