<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use yz\icons\Icons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\courses\backend\models\CourseSearch $searchModel
 * @var array $columns
 */

$this->title = modules\courses\common\models\Course::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'course-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [/*['search'],*/ ['export', 'create', 'delete', 'return']],
            'gridId' => 'course-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\courses\common\models\Course',
        ]) ?>
    </div>

    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'id' => 'course-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
				'class' => 'yz\admin\widgets\ActionColumn',
				'template' => '{up} {down} {update} {delete}',
				'buttons' => [
					'up' => function ($url, $model) {
						return Html::a(Icons::i('angle-up'), $url, [
							'title' => 'Переместить вверх',
							'class' => 'btn btn-info btn-sm',
							'data-pjax' => '0',
						]);
					},
					'down' => function ($url, $model) {
						return Html::a(Icons::i('angle-down'), $url, [
							'title' => 'Переместить вниз',
							'class' => 'btn btn-info btn-sm',
							'data-pjax' => '0',
						]);
					}
				]
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
