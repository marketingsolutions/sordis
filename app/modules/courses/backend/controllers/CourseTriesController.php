<?php

namespace modules\courses\backend\controllers;

use modules\courses\backend\models\CourseTryWithData;
use Yii;
use modules\courses\common\models\CourseTry;
use modules\courses\backend\models\CourseTrySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;
use modules\profiles\common\models\Profile;
use ms\loyalty\finances\common\models\Transaction;
use modules\courses\common\models\CourseResult;
use modules\courses\common\finances\CourseTryPartner;

/**
 * CourseTriesController implements the CRUD actions for CourseTry model.
 */
class CourseTriesController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var CourseTrySearch $searchModel */
                    return Yii::createObject(CourseTrySearch::className());
                },
                'dataProvider' => function($params, CourseTrySearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all CourseTry models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var CourseTrySearch $searchModel */
        $searchModel = Yii::createObject(CourseTrySearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(CourseTrySearch $searchModel)
    {
        return [
			'profile__full_name',
			'test__title',
			'position',
			'step',
			'correct_answers',
			'wrong_answers',
			'test_bonuses',
			'test_questions',
			'bonuses',
			//'extra_bonuses',
			'finished:boolean',
			'best_try:boolean',
			'paid:boolean',
			'created_at:datetime',
        ];
    }

    /**
     * Creates a new CourseTry model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CourseTry;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CourseTry model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
        ]);
	}


    /**
     * Deletes an existing CourseTry model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_) {
            $profile=CourseTry::getProfileData($id_);
            //$purse = $profile->purse;
            $profile->purse->addTransaction(Transaction::factory(
                Transaction::OUTBOUND,
                CourseTry::getTestId($id_)['bonuses'],
                new CourseTryPartner($id_),
                "Списание бонусов участнику ({$profile->full_name}) за удаленный тест '{".CourseTry::getTestId($id_)['test_id']."}' "
            ));
            CourseResult::deleteAll(['profile_id' => $profile->id, 'test_id'=> CourseTry::getTestId($id_)['test_id']]);
            CourseTry::deleteAll(['id' => $id_]);

        }

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the CourseTry model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CourseTry the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CourseTry::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	public function actionPay($id)
	{
		$model = $this->findModel($id);

		if ($model->pay()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, 'Успешно выплачен');
		}
		else {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, 'Не удалось выплатить');
		}

		return $this->getCreateUpdateResponse($model);
	}
}
