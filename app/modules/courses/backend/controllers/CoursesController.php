<?php

namespace modules\courses\backend\controllers;

use Yii;
use modules\courses\common\models\Course;
use modules\courses\backend\models\CourseSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * CoursesController implements the CRUD actions for Course model.
 */
class CoursesController extends Controller implements AccessControlInterface
{
	use CrudTrait, CheckAccessTrait;

	public function beforeAction($event)
	{
		\Yii::$app->request->enableCsrfValidation = false;

		return parent::beforeAction($event);
	}

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'accessControl' => $this->accessControlBehavior(),
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		]);
	}

	public function actions()
	{
		return array_merge(parent::actions(), [
			'export' => [
				'class' => ExportAction::className(),
				'searchModel' => function ($params) {
					/** @var CourseSearch $searchModel */
					return Yii::createObject(CourseSearch::className());
				},
				'dataProvider' => function ($params, CourseSearch $searchModel) {
					$dataProvider = $searchModel->search($params);
					return $dataProvider;
				},
			]
		]);
	}

	/**
	 * Lists all Course models.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
		/** @var CourseSearch $searchModel */
		$searchModel = Yii::createObject(CourseSearch::className());
		$dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
		$dataProvider->setSort([
			'defaultOrder' => ['position' => SORT_ASC, 'id' => 'SORT_ASC'],
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
			'columns' => $this->getGridColumns($searchModel),
		]);
	}

	public function getGridColumns(CourseSearch $searchModel)
	{
		return [
			'id',
			'title',
			'description:raw',
			'enabled:boolean',
			'position',
		];
	}

	/**
	 * Creates a new Course model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionCreate()
	{
		$this->enableCsrfValidation = false;
		$model = new Course;
		$model->frame_height = 600;
		$model->frame_width = 1200;
		$model->enabled = 1;

		/** @var Course $maxPosition */
		$maxPosition = Course::find()->orderBy(['position' => SORT_DESC])->one();
		$model->position = $maxPosition ? $maxPosition->position + 1 : 1;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing Course model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$this->enableCsrfValidation = false;
		$model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * Deletes an existing Course model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete(array $id)
	{
		$message = is_array($id) ?
			\Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
		$id = (array) $id;

		foreach ($id as $id_) {
			$this->findModel($id_)->delete();
		}

		\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Course model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 * @return Course the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Course::findOne($id)) !== null) {
			return $model;
		}
		else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	public function actionDown($id)
	{
		$model = $this->findModel($id);

		/** @var Course $lowerOne */
		$lowerOne = Course::find()
			->where(['!=', 'id', $id])
			->andWhere(['>=', 'position', $model->position])
			->orderBy(['position' => SORT_ASC, 'id' => SORT_ASC])
			->one();

		if ($lowerOne) {
			$lowerPosition = $lowerOne->position;
			$lowerOne->updateAttributes(['position' => $model->position]);
			$model->updateAttributes(['position' => $lowerPosition]);
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, "Курс $id был смещен ниже");
		}

		return $this->redirect(['index']);
	}

	public function actionUp($id)
	{
		$model = $this->findModel($id);

		/** @var Course $upperOne */
		$upperOne = Course::find()
			->where(['!=', 'id', $id])
			->andWhere(['<=', 'position', $model->position])
			->orderBy(['position' => SORT_DESC, 'id' => SORT_DESC])
			->one();

		if ($upperOne) {
			$upperPosition = $upperOne->position;
			$upperOne->updateAttributes(['position' => $model->position]);
			$model->updateAttributes(['position' => $upperPosition]);
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, "Курс $id был смещен ниже");
		}

		return $this->redirect(['index']);
	}
}
