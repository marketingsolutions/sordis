<?php

namespace modules\courses\common;


/**
 * Class Module
 */
class Module extends \yz\Module
{
    public function getName()
    {
        return 'Обучающие материалы и тесты';
    }
}