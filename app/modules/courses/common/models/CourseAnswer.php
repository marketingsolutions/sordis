<?php

namespace modules\courses\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_course_answers".
 *
 * @property integer $id
 * @property string $title
 * @property integer $position
 * @property boolean $is_correct
 * @property integer $question_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CourseQuestion $question
 */
class CourseAnswer extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_answers}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Вариант ответ к вопросу теста';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Варианты ответов к вопросам теста';
    }

	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'value' => new Expression('NOW()'),
			],
		];
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'string', 'max' => 255],
			['title', 'required'],
            ['position', 'integer'],
            ['is_correct', 'safe'],
            ['question_id', 'integer'],
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Вариант ответа',
            'position' => 'Номер ответа',
            'is_correct' => 'Правильный ответ',
            'question_id' => 'Вопрос',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(CourseQuestion::className(), ['id' => 'question_id']);
    }
}
