<?php

namespace modules\courses\common\models;

use modules\profiles\common\models\ProfileCourseVisit;
use modules\profiles\common\models\ProfileTestPosition;
use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_course_tests".
 *
 * @property integer $id
 * @property string $title
 * @property integer $course_id
 * @property integer $bonuses
 * @property boolean $enabled
 *
 * @property CourseQuestion[] $questions
 * @property Course $course
 */
class CourseTest extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_tests}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Тест';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Тесты';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'string', 'max' => 255],
            ['course_id', 'integer'],
			['bonuses', 'integer'],
			['bonuses', 'required'],
			['time_min', 'required'],
            ['enabled', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'course_id' => 'Курс',
			'bonuses' => 'Бонусы за 100%',
			'time_min' => 'Максимальное время прохождения теста в минутах',
            'enabled' => 'Активен',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(CourseQuestion::className(), ['test_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

	public function beforeDelete()
	{
		if (parent::beforeDelete()) {
			foreach (CourseQuestion::findAll(['test_id' => $this->id]) as $q) {
				/** @var CourseQuestion $q */
				$q->delete();
			}
			ProfileTestPosition::deleteAll(['test_id' => $this->id]);
			CourseResult::deleteAll(['test_id' => $this->id]);

			return true;
		}
		else {
			return false;
		}
	}

    public static function getOptions()
    {
        return self::find()->indexBy('id')->select('title, id')->orderBy(['title' => SORT_ASC])->column();
    }
}
