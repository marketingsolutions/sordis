<?php

namespace modules\courses\common\models;

use modules\courses\common\finances\CourseTryPartner;
use modules\profiles\common\models\Profile;
use ms\loyalty\finances\common\models\Transaction;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_course_tries".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property integer $test_id
 * @property integer $position
 * @property integer $step
 * @property integer $correct_answers
 * @property integer $wrong_answers
 * @property integer $test_bonuses
 * @property integer $test_questions
 * @property integer $bonuses
 * @property integer $extra_bonuses
 * @property string $paid_at
 * @property string $created_at
 * @property string $updated_at
 *
 * @property boolean $paid
 * @property boolean $finished
 * @property boolean $best_try
 *
 * @property CourseResult[] $results
 * @property Profile $profile
 * @property CourseTest $test
 */
class CourseTry extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    const MAX_TRIES_PER_MONTH = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_tries}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Course Try';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Статисктика прохождения тестов';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['profile_id', 'integer'],
            ['test_id', 'integer'],
            ['position', 'integer'],
            ['step', 'integer'],
            ['correct_answers', 'integer'],
            ['time_start', 'integer'],
            ['time_finish', 'integer'],
            ['correct_answers', 'integer'],
            ['wrong_answers', 'integer'],
            ['test_bonuses', 'integer'],
            ['test_questions', 'integer'],
            ['bonuses', 'integer'],
            ['extra_bonuses', 'integer'],
            [['paid', 'finished', 'best_try'], 'safe'],
            ['paid_at', 'safe'],
            ['created_at', 'safe'],
            ['updated_at', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Profile ID',
            'test_id' => 'Test ID',
            'position' => 'Номер попытки',
            'step' => 'Текущая позиция в тесте',
            'correct_answers' => 'Правильных ответов',
            'wrong_answers' => 'Неправильных ответов',
            'test_bonuses' => 'Бонусов за тест',
            'test_questions' => 'Вопросов всего',
            'bonuses' => 'Бонусы',
            'extra_bonuses' => 'Доначисленные бонусы',
            'paid' => 'Бонусы зачислены',
            'paid_at' => 'Дата зачисления',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'finished' => 'Тест окончен',
            'best_try' => 'Это лучший результат'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResults()
    {
        return $this->hasMany(CourseResult::className(), ['try_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(CourseTest::className(), ['id' => 'test_id']);
    }

    public function calculateBonuses()
    {
        if ($this->test_questions <= $this->correct_answers) {
            $this->bonuses = round($this->test_bonuses / $this->test_questions * $this->correct_answers);
        }
        else {
            $this->bonuses = 0;
        }
    }

    public function pay()
    {
        $this->calculateBonuses();
        $this->updateAttributes(['bonuses']);

        if ($this->bonuses == 0) {
            return false; # нечего выплачивать
        }

        /** @var CourseTest $test */
        $test = CourseTest::findOne($this->test_id);
        /** @var Profile $profile */
        $profile = Profile::findOne($this->profile_id);

        $transaction = \Yii::$app->db->beginTransaction();

        try {
            $month = date('m');
            $year = date('Y');

            $date = new \DateTime('first day of this month');
            $from = $date->format('Y-m-d 00:00:00');
            $date = new \DateTime('last day of this month');
            $to = $date->format('Y-m-d 23:59:59');

            /** @var CourseTry[] $tries */
            $tries = CourseTry::find()
                ->where("created_at > '$from' AND created_at < '$to'")
                ->andWhere(['profile_id' => $this->profile_id, 'test_id' => $this->test_id, 'finished' => true])
                ->andWhere(['!=', 'id', $this->id])
                ->orderBy(['bonuses' => SORT_DESC])
                ->all();

            if (count($tries) > (self::MAX_TRIES_PER_MONTH - 1)) {
                # за этот месяц уже было 3 попытки - не пускаем дальше
                \Yii::$app->session->setFlash(
                    \yz\Yz::FLASH_WARNING,
                    'Превышено максимальное кол-во попыток (' . self::MAX_TRIES_PER_MONTH . ')'
                );
                return false;
            }

            if (empty($tries)) {
                # других попыток не было - это лучшая, полностью выплачиваем
                $profile->purse->addTransaction(Transaction::factory(
                    Transaction::INCOMING,
                    $this->bonuses,
                    new CourseTryPartner($this->id),
                    "Бонусы участнику ({$profile->full_name}) за тест '{$test->title}' за $month месяц $year года"
                ));
                $this->updateAttributes([
                    'paid' => true,
                    'paid_at' => (new \DateTime('now'))->format('Y-m-d H:i:s'),
                    'best_try' => true,
                ]);
            }
            else {
                # вычисляем, лучше ли эта попытка предыдущих
                $bestTry = $tries[0];

                if ($bestTry->bonuses < $this->bonuses) {
                    # эта попытка лучше предыдущей - выплачиваем разницу
                    $extra_bonuses = $this->bonuses - $bestTry->bonuses;
                    $profile->purse->addTransaction(Transaction::factory(
                        Transaction::INCOMING,
                        $extra_bonuses,
                        new CourseTryPartner($this->id),
                        "Бонусы участнику ({$profile->full_name}) ДОНАЧИСЛЕНИЕ за тест '{$test->title}' за $month месяц $year года"
                    ));
                    $this->updateAttributes([
                        'extra_bonuses' => $extra_bonuses,
                        'paid' => true,
                        'paid_at' => (new \DateTime('now'))->format('Y-m-d H:i:s'),
                    ]);

                    foreach ($tries as $try) {
                        $try->updateAttributes(['best_try' => false]);
                    }
                    $this->updateAttributes(['best_try' => true]);
                }
                else {
                    $this->updateAttributes(['best_try' => false]);
                }
            }

            $transaction->commit();

            return true;
        }
        catch (\Exception $e) {
            $transaction->rollBack();

            return false;
        }
    }

    /**
     * Для вывода/скрытия кнопки прохождения определенного теста
     *
     * @param $testId
     * @param $profileId
     * @return bool
     */
    public static function isBlockTest($testId, $profileId)
    {
        $model = self::find()->where(['profile_id' => $profileId, 'test_id' => $testId, 'finished' => 1])->one();
        if ($model) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * Ограничение по времени прохождения теста
     *
     * @param $testId
     * @return bool
     */
    public static function isTimeOut($testId)
    {
        $now = time();
        //$now = 14975982830;
        $profileId = \modules\profiles\common\models\Profile::findOne(['identity_id' => Yii::$app->user->identity->id])->id;
        $timeStart = self::find()->select('time_start')->where(['profile_id' => $profileId, 'test_id' => $testId])->asArray()->one()['time_start'];
        //        if($timeStart == null)
        //            self::setNewTimeStart(\Yii::$app->request->get('id') ,\Yii::$app->user->identity->id);
        $timeFinishMin = CourseTest::find()
            ->select('time_min')
            ->where(['id' => $testId])
            ->asArray()
            ->one()['time_min'];
        $timeFinish = $timeStart + $timeFinishMin * 60;
        if (($timeFinish - $now) < 0) {
            $blockTest = self::findOne(['profile_id' => $profileId, 'test_id' => $testId]);
            $blockTest->finished = 1;
            $blockTest->update(false);
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * Получаем метку времени начала прохождения теста
     *
     * @param $testId
     * @return mixed
     */
    public static function getTestTrieMin($testId)
    {
        return CourseTest::find()->select('time_min')->where(['id' => $testId])->asArray()->one()['time_min'];
    }

    /**
     * Проверяем пройден ли тест или он заблокирован из-за истечения лимита времени
     *
     * @param $trieId
     * @return bool
     */
    public static function isTestComplete($trieId)
    {
        $profileId = \modules\profiles\common\models\Profile::findOne(['identity_id' => Yii::$app->user->identity->id])->id;
        $trie = self::find()->where(['id' => $trieId])->asArray()->one();
        if ($trie['test_questions'] > $trie['step']) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * @param $coursTrieId
     * @return static
     */
    public static function getProfileData($coursTrieId)
    {
        $profileId = self::find()->where(['id' => $coursTrieId])->asArray()->one()['profile_id'];
        return Profile::findOne(['id' => $profileId]);
    }

    /**
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getTestId($id)
    {
        return self::find()->where(['id' => $id])->asArray()->one();
    }

    /**
     * @param $testId
     * @param $profileId
     * @return mixed
     */
    public static function getTimeStart($testId, $profileId)
    {
        return self::find()->select('time_start')->where(['test_id' => $testId, 'profile_id' => $profileId])->asArray()->one()['time_start'];
    }

    /**
     * @param $testId
     * @param $profileId
     * @return bool
     */
    public static function setNewTimeStart($testId, $profileId)
    {
        $model = self::findOne(['test_id' => $testId, 'profile_id' => $profileId]);
        $model->time_start = time();
        if ($model->update()) {
            return true;
        }
        else {
            return false;
        }
    }

    /*Для каждого теста*/
    public static function getFirstId($testId)
    {
        return CourseQuestion::find()->select('id')->where(['test_id' => $testId, 'position' => 1])->asArray()->one()['id'];
    }

}
