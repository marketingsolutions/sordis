<?php

namespace modules\courses\common\models;

use modules\courses\common\finances\CourseResultPartner;
use modules\profiles\common\models\Profile;
use marketingsolutions\finance\models\Transaction;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_course_results".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property integer $test_id
 * @property integer $question_id
 * @property integer $answer_id
 * @property integer $try_id
 * @property integer $is_correct
 * @property integer $test_bonuses
 * @property integer $test_questions
 * @property integer $bonuses
 * @property integer $time_start
 * @property integer $time_finish
 * @property boolean $paid
 * @property string $paid_at
 * @property string $created_at
 * @property string $updated_at
 * @property string $answers
 *
 * @property CourseAnswer $answer
 * @property Profile $profile
 * @property CourseQuestion $question
 * @property CourseTest $test
 * @property CourseTry $try
 */
class CourseResult extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_results}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Результат тестирования';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Результаты тестирования';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['profile_id', 'integer'],
            ['test_id', 'integer'],
            ['question_id', 'integer'],
            ['answer_id', 'integer'],
			['try_id', 'integer'],
            ['is_correct', 'safe'],
            ['test_bonuses', 'integer'],
            ['test_questions', 'integer'],
            ['bonuses', 'integer'],
            ['paid', 'safe'],
            ['paid_at', 'safe'],
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
			['answers', 'string'],

			[['test_id', 'question_id', 'answer_id', 'profile_id'], 'required'],
        ];
    }

	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'value' => new Expression('NOW()'),
			],
		];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Profile ID',
			'try_id' => 'Try ID',
            'test_id' => 'Test ID',
            'question_id' => 'Question ID',
            'answer_id' => 'Answer ID',
            'is_correct' => 'Is Correct',
            'test_bonuses' => 'Test Bonuses',
            'test_questions' => 'Test Questions',
            'bonuses' => 'Bonuses',
            'paid' => 'Paid',
            'paid_at' => 'Paid At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
			'answers' => 'Выбранные ответы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswer()
    {
        return $this->hasOne(CourseAnswer::className(), ['id' => 'answer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(CourseQuestion::className(), ['id' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(CourseTest::className(), ['id' => 'test_id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTry()
	{
		return $this->hasOne(CourseTry::className(), ['id' => 'try_id']);
	}

	public function calculateBonuses()
	{
		if ($this->test_bonuses && $this->test_questions) {
			$this->bonuses = ceil($this->test_bonuses / $this->test_questions);
		}
	}

	public function pay()
	{
		/** @var $result CourseResult */
		$transaction = \Yii::$app->db->beginTransaction();

		$this->updateAttributes([
			'paid' => true,
			'paid_at' => (new \DateTime('now'))->format('Y-m-d H:i:s'),
		]);

		/** @var Profile $profile */
		$profile = Profile::findOne($this->profile_id);
		$question = CourseQuestion::findOne($this->question_id);

		$profile->purse->addTransaction(Transaction::factory(
			Transaction::INCOMING,
			$this->bonuses,
			new CourseResultPartner($this->id),
			"Бонусы участнику ({$profile->full_name}) за вопрос #{$question->position} по тесту #{$this->test_id}"
		));

		$transaction->commit();
	}

	public function payDifference($amount)
	{
		/** @var Profile $profile */
		$profile = Profile::findOne($this->profile_id);

		$profile->purse->addTransaction(Transaction::factory(
			Transaction::INCOMING,
			$amount,
			new CourseResultPartner($this->id),
			"Бонусы участнику ({$profile->full_name}) за тест #{$this->test_id} при окончании теста (разница)"
		));
	}
}
