<?php

namespace modules\courses\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_course_lessons".
 *
 * @property integer $id
 * @property integer $position
 * @property string $type
 * @property string $file
 * @property string $text
 * @property integer $course_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Course $course
 */
class CourseLesson extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_lessons}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Урок курса';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Уроки курсов';
    }

	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'value' => new Expression('NOW()'),
			],
		];
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['position', 'integer'],
            ['type', 'string', 'max' => 255],
            ['file', 'string', 'max' => 255],
            ['text', 'string'],
            ['course_id', 'integer'],
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position' => 'Позиция от 1 до 9',
            'type' => 'Тип урока',
            'file' => 'Файл',
            'text' => 'Текст',
            'course_id' => 'Курс',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }
}
