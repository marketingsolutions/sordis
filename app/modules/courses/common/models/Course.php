<?php

namespace modules\courses\common\models;

use modules\profiles\common\models\ProfileCourseVisit;
use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_courses".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $file
 * @property string $html
 * @property string $frame_url
 * @property string $frame_width
 * @property string $frame_height
 * @property boolean $enabled
 *
 * @property string $video_1_title
 * @property string $video_1_frame
 * @property string $video_2_title
 * @property string $video_2_frame
 * @property string $video_3_title
 * @property string $video_3_frame
 * @property string $video_4_title
 * @property string $video_4_frame
 * @property string $video_5_title
 * @property string $video_5_frame
 *
 * @property integer $position
 *
 * @property CourseLesson[] $lessons
 */
class Course extends \yii\db\ActiveRecord implements ModelInfoInterface
{
	public $file_local;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%courses}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Курс';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Курсы';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'string', 'max' => 255],
            ['description', 'string'],
			['file', 'string'],
			['html', 'string'],
			['frame_url', 'string'],
			[['frame_width', 'frame_height'], 'integer'],
			['file_local', 'file', 'extensions' => 'pdf', 'maxSize' => 1024 * 1024 * 100, 'tooBig' => 'Максимальный размер загружаемого файла - 100 МБ'],
        	[['video_1_title', 'video_1_frame', 'video_2_title', 'video_2_frame', 'video_3_title', 'video_3_frame', 'video_4_title', 'video_4_frame', 'video_5_title', 'video_5_frame', ], 'string'],
			['enabled', 'safe'],
			['position', 'integer'],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Курс',
            'description' => 'Описание',
			'file' => 'Загрузка файла',
			'file_local' => 'Загрузка файла',
			'html' => 'Вставленный код',
			'frame_url' => 'URL-адрес фрейма',
			'frame_width' => 'Ширина фрейма',
			'frame_height' => 'Высота фрейма',
			'video_1_title' => 'Видео 1 заголовок',
			'video_1_frame' => 'Видео 1 код youtube-фрейма',
			'video_2_title' => 'Видео 2 заголовок',
			'video_2_frame' => 'Видео 2 код youtube-фрейма',
			'video_3_title' => 'Видео 3 заголовок',
			'video_3_frame' => 'Видео 3 код youtube-фрейма',
			'video_4_title' => 'Видео 4 заголовок',
			'video_4_frame' => 'Видео 4 код youtube-фрейма',
			'video_5_title' => 'Видео 5 заголовок',
			'video_5_frame' => 'Видео 5 код youtube-фрейма',
			'enabled' => 'Активен',
			'position' => 'Позиция в списке',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessons()
    {
        return $this->hasMany(CourseLesson::className(), ['course_id' => 'id']);
    }

	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);

		$this->upload(['file']);
	}

	private function upload(array $fields)
	{
		$dir = Yii::getAlias('@data/courses');
		FileHelper::createDirectory($dir, $mode = 0775, $recursive = true);

		foreach ($fields as $field) {
			$file = UploadedFile::getInstance($this, $field . '_local');
			$unique = uniqid();

			if ($file instanceof UploadedFile) {
				$name = "{$this->id}_{$field}_{$unique}.{$file->extension}";
				$path = Yii::getAlias($dir . "/$name");
				$file->saveAs($path, false);

				$this->$field = $name;
				$this->updateAttributes([$field]);
			}
		}
	}

	public function isPdf()
	{
		if (empty($this->file)) {
			return false;
		}

		if (strpos($this->file, '.pdf') !== false) {
			return true;
		}
		else {
			return false;
		}
	}

	public function hasFile()
	{
		return !empty($this->file);
	}

	public function beforeDelete()
	{
		if (parent::beforeDelete()) {
			ProfileCourseVisit::deleteAll(['course_id' => $this->id]);

			return true;
		}
		else {
			return false;
		}
	}
}
