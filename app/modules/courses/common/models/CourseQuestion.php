<?php

namespace modules\courses\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Query;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_course_questions".
 *
 * @property integer $id
 * @property string $title
 * @property integer $position
 * @property integer $test_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CourseAnswer[] $answers
 * @property CourseTest $test
 */
class CourseQuestion extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_questions}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Вопрос к тесту';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Вопросы к тестам';
    }

	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'value' => new Expression('NOW()'),
			],
		];
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'string', 'max' => 255],
			['title', 'required'],
            ['position', 'integer'],
            ['test_id', 'integer'],
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Вопрос',
            'position' => 'Номер вопроса',
            'test_id' => 'Тест',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(CourseAnswer::className(), ['question_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(CourseTest::className(), ['id' => 'test_id']);
    }

	public static function findOrdered($test_id)
	{
		/** @var CourseQuestion[] $questions */
		$questions = CourseQuestion::find()
			->where(['test_id' => $test_id])
			->orderBy(['position' => SORT_ASC, 'id' => SORT_ASC])
			->all();

		foreach ($questions as $i => $question) {
			$question->updateAttributes(['position' => $i + 1]);
		}

		return $questions;
	}

	public function beforeDelete()
	{
		if (parent::beforeDelete()) {
			CourseResult::deleteAll(['question_id' => $this->id]);
			CourseAnswer::deleteAll(['question_id' => $this->id]);

			return true;
		}
		else {
			return false;
		}
	}

	public function findCorrectAnswers()
	{
		return CourseAnswer::find()
			->where(['question_id' => $this->id, 'is_correct' => true])
			->all();
	}

	public function findCorrectAnswerIds()
	{
		$ids = [];
		foreach ($this->findCorrectAnswers() as $answer) {
			/** @var CourseAnswer $answer */
			$ids[] = $answer->id;
		}

		return $ids;
	}

	public function getOrderedAnswers()
	{
		return CourseAnswer::find()
			->where(['question_id' => $this->id])
			->orderBy(['position' => SORT_ASC, 'id' => SORT_ASC])
			->all();
	}

    public static function getOptions()
    {
        $questions = (new Query)
            ->select('q.title AS qTitle, t.title AS tTitle, q.id as qId')
            ->from(['q' => CourseQuestion::tableName()])
            ->innerJoin(['t' => CourseTest::tableName()], 'q.test_id = t.id')
            ->orderBy(['t.title' => SORT_ASC, 'q.title' => SORT_ASC])
            ->all();

        $result = [];
        foreach ($questions as $question) {
            $key = $question['qId'];
            $result[$key] = $question['tTitle'] . ' _____ ' . $question['qTitle'];
        }

        return $result;
    }
}
