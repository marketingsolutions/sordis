<?php

namespace modules\courses\console\controllers;

use console\base\Controller;
use modules\courses\common\finances\CourseTryPartner;
use modules\courses\common\models\CourseTry;
use ms\loyalty\finances\common\models\Transaction;

class PayTriesController extends Controller
{
	public function actionFix()
	{
		$tries = CourseTry::find()->where(['id' => [66]]);

		foreach ($tries->each() as $try) {
			/** @var CourseTry $try */
			$transaction = \Yii::$app->db->beginTransaction();
			$test = $try->test;

			$try->calculateBonuses();
			$try->updateAttributes([
				'paid' => true,
				'paid_at' => (new \DateTime('now'))->format('Y-m-d H:i:s'),
			]);

			$month = (new \DateTime($try->created_at))->format('m');
			$month = intval($month);
			$year = (new \DateTime($try->created_at))->format('Y');

			$profile = $try->profile;
			$profile->purse->addTransaction(Transaction::factory(
				Transaction::INCOMING,
				$try->bonuses,
				new CourseTryPartner($try->id),
				"Бонусы участнику ({$profile->full_name}) за тест '{$test->title}' за $month месяц $year года"
			));

			$transaction->commit();

			echo "=> paid CourseTry #{$try->id}" . PHP_EOL;
		}
	}
}