<?php

namespace modules\courses\frontend\models;

use modules\courses\common\models\CourseTest;

class ApiCourseTest extends CourseTest
{
    public function fields()
    {
        $fields = parent::fields() + [
            'questions'
        ];

        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(ApiCourseQuestion::className(), ['test_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

}