<?php

namespace modules\courses\frontend\models;

use modules\courses\common\models\CourseQuestion;

class ApiCourseQuestion extends CourseQuestion
{

    public function fields()
    {
        return [
            'id',
            'title',
            'position',
            'created_at' => function(ApiCourseQuestion $model) {
                /* @var $model ApiCourseQuestion */
                return $model->created_at ? (new \DateTime($model->created_at))->format('d.m.Y') : null;
            },
            'updated_at' => function(ApiCourseQuestion $model) {
                /* @var $model ApiCourseQuestion */
                return $model->updated_at ? (new \DateTime($model->updated_at))->format('d.m.Y') : null;
            },
            'answers'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(ApiCourseAnswer::className(), ['question_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

}