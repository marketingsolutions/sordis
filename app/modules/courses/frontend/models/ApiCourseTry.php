<?php

namespace modules\courses\frontend\models;

use modules\courses\common\models\CourseTry;

class ApiCourseTry extends CourseTry
{
    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'test_title' => function(ApiCourseTry $model) {
                /* @var $model ApiCourseTry */
                return $model->test->title;
            },
            'paid_at' => function(ApiCourseTry $model) {
                /* @var $model ApiCourseTry */
                return $model->paid_at ? (new \DateTime($model->paid_at))->format('d.m.Y') : null;
            },
            'created_at' => function(ApiCourseTry $model) {
                /* @var $model ApiCourseTry */
                return $model->created_at ? (new \DateTime($model->created_at))->format('d.m.Y') : null;
            },
            'updated_at' => function(ApiCourseTry $model) {
                /* @var $model ApiCourseTry */
                return $model->updated_at ? (new \DateTime($model->updated_at))->format('d.m.Y') : null;
            },
            'results',
        ]);

        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResults()
    {
        return $this->hasMany(ApiCourseResult::className(), ['try_id' => 'id']);
    }

}