<?php

namespace modules\courses\frontend\models;

use modules\courses\common\models\CourseAnswer;

class ApiCourseAnswer extends CourseAnswer
{

    public function fields()
    {
        return [
            'id',
            'title',
            'is_correct',
            'position',
            'created_at' => function(ApiCourseAnswer $model) {
                /* @var $model ApiCourseAnswer */
                return $model->created_at ? (new \DateTime($model->created_at))->format('d.m.Y') : null;
            },
            'updated_at' => function(ApiCourseAnswer $model) {
                /* @var $model ApiCourseAnswer */
                return $model->updated_at ? (new \DateTime($model->updated_at))->format('d.m.Y') : null;
            },
        ];
    }

}