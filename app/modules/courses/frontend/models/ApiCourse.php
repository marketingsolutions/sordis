<?php

namespace modules\courses\frontend\models;

use modules\courses\common\models\Course;

class ApiCourse extends Course
{

    public function fields()
    {
        return parent::fields();
    }
}