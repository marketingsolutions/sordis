<?php

namespace modules\courses\frontend\models;

use modules\courses\common\models\CourseResult;

class ApiCourseResult extends CourseResult
{

    public function fields()
    {
        $fields = [
            'id',
            'question_id',
            'answer_id',
            'is_correct',
            'test_bonuses',
            'test_questions',
            'bonuses',
            'paid',
            'answers',
            'paid_at' => function(ApiCourseResult $model) {
                /* @var $model ApiCourseResult */
                return $model->paid_at ? (new \DateTime($model->paid_at))->format('d.m.Y') : null;
            },
            'created_at' => function(ApiCourseResult $model) {
                /* @var $model ApiCourseResult */
                return $model->created_at ? (new \DateTime($model->created_at))->format('d.m.Y') : null;
            },
            'updated_at' => function(ApiCourseResult $model) {
                /* @var $model ApiCourseResult */
                return $model->updated_at ? (new \DateTime($model->updated_at))->format('d.m.Y') : null;
            },
        ];

        return $fields;
    }
}