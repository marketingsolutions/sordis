<?php

namespace modules\courses\frontend\controllers\api;

use Yii;
use ms\loyalty\api\frontend\base\ApiController;
use modules\courses\frontend\models\ApiCourseTry;
use modules\courses\frontend\models\ApiCourseTest;
use modules\profiles\frontend\models\ApiProfile;
use modules\courses\common\models\CourseQuestion;
use modules\courses\common\models\CourseResult;

/**
 * Class TestTryController
 */
class TestTryController extends ApiController
{

    const MAX_TRIES = 10;

    public function actionList()
    {
        $profile_id = Yii::$app->request->post('profile_id', null);

        $profile = ApiProfile::findOne($profile_id);

        if(null === $profile) {
            return $this->error("Не найден участник с ID $profile_id", "Не найден участник с profile_id=$profile_id");
        }

        $tries = ApiCourseTry::find()
            ->where(['profile_id' => $profile_id])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();

        return $this->ok(['tries' => $tries], 'Получение результатов тестов');
    }

    public function actionView()
    {
        $profile_id = Yii::$app->request->post('profile_id', null);
        $try_id = Yii::$app->request->post('try_id', null);

        $try = ApiCourseTry::find()
            ->where(['id' => $try_id, 'profile_id' => $profile_id])
            ->one();

        if(null === $try) {
            return $this->error("Не найден результат теста с ID участника $profile_id и ID попытки $try_id");
        }

        return $this->ok(['try' => $try], 'Получение результатов теста по ID');
    }

    public function actionViewSuccess()
    {
        $profile_id = Yii::$app->request->post('profile_id', null);

        $profile = ApiProfile::findOne($profile_id);

        if(null === $profile) {
            return $this->error("Не найден участник с ID $profile_id", "Не найден участник с profile_id=$profile_id");
        }

        $try = ApiCourseTry::find()
            ->where([
                'and',
                ['profile_id' => $profile_id],
                ['finished' => 1],
                ['wrong_answers' => 0],
                'correct_answers = test_questions',
            ])
            ->one();

        return $this->ok(['try' => $try], 'Получение результатов успешного теста');
    }

    public function actionIndex()
    {
        $profile_id = Yii::$app->request->post('profile_id', null);
        $test_id = Yii::$app->request->post('test_id', null);

        $profile = ApiProfile::findOne($profile_id);
        $test = ApiCourseTest::findOne($test_id);

        if(null === $profile) {
            return $this->error("Не найден участник с ID $profile_id", "Не найден участник с profile_id=$profile_id");
        }

        if(null === $test) {
            return $this->error("Не найден тест с ID $test_id", "Не найден тест с test_id=$test_id");
        }

        $questions = CourseQuestion::findOrdered($test->id);

        $try = ApiCourseTry::find()
            ->andWhere(['profile_id' => $profile->id, 'test_id' => $test->id, 'finished' => false])
            ->orderBy(['position' => SORT_DESC])
            ->one();

        if(null === $try) {
            $countTries = ApiCourseTry::find()
                ->andWhere(['profile_id' => $profile->id, 'test_id' => $test->id, 'finished' => true])
                ->count('id');

            if($countTries >= self::MAX_TRIES) {
                return $this->error('Превышено допустимое количество попыток прохождения теста');
            }

            $try = new ApiCourseTry();
            $try->profile_id = $profile->id;
            $try->test_id = $test->id;
            $try->position = $countTries + 1;
            $try->step = 1;
            $try->test_questions = count($questions);
            $try->test_bonuses = $test->bonuses;

            $try->save();
            $try->refresh();
        }

        $step = $try->step;

        /** @var CourseResult[] $results */
        $results = CourseResult::find()
            ->where(['profile_id' => $profile->id, 'test_id' => $test->id, 'try_id' => $try->id])
            ->all();

        $resultValues = [];

        foreach($results as $result) {
            $key = $result->question_id;
            $resultValues[$key] = $result->is_correct;
        }

        return $this->ok([
            'try' => $try,
            'step' => $step,
            'resultValues' => $resultValues,
        ], 'Прохождение теста');
    }

    public function actionAnswer()
    {
        $profile_id = Yii::$app->request->post('profile_id', null);
        $try_id = Yii::$app->request->post('try_id', null);
        $question_id = Yii::$app->request->post('question_id', null);
        $answer_ids = array_map('intval', (array)Yii::$app->request->post('answer_ids', []));

        $profile = ApiProfile::findOne($profile_id);
        $try = ApiCourseTry::find()->where(['id' => $try_id, 'profile_id' => $profile_id])->one();
        $question = CourseQuestion::findOne($question_id);

        if(null === $profile) {
            return $this->error("Не найден участник с ID $profile_id", "Не найден участник с profile_id=$profile_id");
        }

        if(null === $try) {
            return $this->error("Не найден результат теста с ID участника $profile_id и ID попытки $try_id");
        }

        if(null === $question) {
            return $this->error("Не найден вопрос с ID $question_id");
        }

        $test = $try->test;

        if(null === $test) {
            return $this->error("Не найден тест для результат с ID $try_id");
        }

        if(empty($answer_ids)) {
            return $this->error('Отсутствуют ответы answer_ids на вопросы теста');
        }

        # проверка, что еще не отвечал на этот вопрос
        $result = CourseResult::find()
            ->where(['try_id' => $try->id])
            ->andWhere(['question_id' => $question->id])
            ->one();

        if($result) {
            return $this->error('В этой попытке уже дан ответ на этот вопрос');
        }

        $countQuestions = CourseQuestion::find()->where(['test_id' => $test->id])->count();
        $correctAnswerIds = $question->findCorrectAnswerIds();
        sort($correctAnswerIds);
        sort($answer_ids);
        $isCorrect = ($correctAnswerIds == $answer_ids);
        $hasFinished = false;

        $transaction = Yii::$app->db->beginTransaction();

        try {
            # new test result
            $result = new CourseResult();
            $result->try_id = $try->id;
            $result->profile_id = $profile->id;
            $result->question_id = $question_id;
            $result->test_id = $test->id;
            $result->answer_id = null;
            $result->is_correct = $isCorrect;
            $result->test_bonuses = $test->bonuses;
            $result->test_questions = $countQuestions;
            $result->calculateBonuses();
            $result->answers = 'Правильные ответы на вопрос: '
                . implode(',', $correctAnswerIds) . '. Участник ответил так: ' . implode(',', $answer_ids);
            $result->paid_at = null;
            $result->paid = false;
            $result->save(false);

            if ($isCorrect) {
                $try->correct_answers = $try->correct_answers + 1;
            } else {
                $try->wrong_answers = $try->wrong_answers + 1;
            }

            $try->step = $try->step + 1;

            # Если прошли весь тест - обновляем статусы
            if ($try->step > $countQuestions) {
                $try->finished = true;
                $try->calculateBonuses();
                $hasFinished = true;
            }

            $try->updateAttributes(['step', 'finished', 'bonuses', 'best_try', 'correct_answers', 'wrong_answers']);
            $try->save(false);
            $transaction->commit();

            if ($try->finished) {
                $try->pay();
            }
        }
        catch (\Exception $e) {
            $transaction->rollBack();

            throw $e;
        }

        return $this->ok(['try' => $try], 'Ответ на вопрос теста');
    }

}