<?php

namespace modules\courses\frontend\controllers\api;

use Yii;
use ms\loyalty\api\frontend\base\ApiController;
use modules\courses\frontend\models\ApiCourseTest;

/**
 * Class TestController
 */
class TestController extends ApiController
{

    public function actionList()
    {
        $tests = ApiCourseTest::find()->orderBy(['id' => SORT_ASC])->all();

        return $this->ok(['tests' => $tests], 'Получение списка тестов');
    }

    public function actionView()
    {
        $test_id = Yii::$app->request->post('test_id', null);

        $test = ApiCourseTest::findOne($test_id);

        if(null === $test) {
            return $this->error("Не найден тест с ID $test_id", "Ошибка при получении теста test_id=$test_id");
        }

        return $this->ok(['test' => $test], "Получание теста по ID");
    }

}