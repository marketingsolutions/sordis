<?php

namespace modules\courses\frontend\controllers\api;

use Yii;
use ms\loyalty\api\frontend\base\ApiController;
use modules\courses\frontend\models\ApiCourse;

/**
 * Class CourseController
 */
class CourseController extends ApiController
{

    public function actionList()
    {
        $courses = ApiCourse::find()->orderBy(['position' => SORT_ASC])->all();

        return $this->ok(['courses' => $courses], 'Получение списка обучающих курсов');
    }

    public function actionView()
    {
        $course_id = Yii::$app->request->post('course_id', null);

        $course = ApiCourse::findOne($course_id);
        
        if(null === $course) {
            return $this->error("Не найден обучающий курс с ID $course_id", "Ошибка при получении обучающего курса course_id=$course_id");
        }

        return $this->ok(['course' => $course], "Получание обучающего курса по ID");
    }


}