<?php

namespace modules\courses\frontend\controllers;

use modules\courses\common\models\CourseAnswer;
use modules\courses\common\models\CourseQuestion;
use modules\courses\common\models\CourseResult;
use modules\courses\common\models\CourseTest;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\ProfileTestPosition;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class TestController extends Controller
{
	public function beforeAction($event)
	{
		\Yii::$app->request->enableCsrfValidation = false;

		return parent::beforeAction($event);
	}

	public function actionAnswer($test_id, $question_id, $step)
	{
		if (!\Yii::$app->user->identity) {
			return \Yii::$app->response->redirect('/identity/auth/login');
		}


		$answer_ids = \Yii::$app->request->post('answer_ids', null);

		if (empty($answer_ids)) {
			return $this->redirect(['/courses/test/index', 'id' => $test_id, 'step' => $step]);
		}

		$test = CourseTest::findOne($test_id);
		$question = CourseQuestion::findOne($question_id);

		if (!$test || !$question) {
			throw new NotFoundHttpException();
		}

		/** @var Profile $profile */
		$profile = \Yii::$app->user->identity->profile;

		# check if has succeed test result for this month
		$date = new \DateTime('first day of this month');
		$from = $date->format('Y-m-d 00:00:00');
		$date = new \DateTime('last day of this month');
		$to = $date->format('Y-m-d 23:59:59');

		$succeedResult = CourseResult::find()
			->where("created_at > '$from' AND created_at < '$to'")
			->andWhere(['is_correct' => true])
			->andWhere(['profile_id' => $profile->id])
			->andWhere(['question_id' => $question->id])
			->one();

		if ($succeedResult) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, 'В этом месяце Вы уже ответили на этот вопрос');
			return $this->redirect(['/courses/test/index', 'id' => $test_id, 'step' => $step]);
		}

		$countQuestions = CourseQuestion::find()->where(['test_id' => $test_id])->count();
		$correctAnswerIds = $question->findCorrectAnswerIds();
		$answer_ids = array_map('intval', $answer_ids);
		sort($correctAnswerIds);
		sort($answer_ids);
		$isCorrect = ($correctAnswerIds == $answer_ids);

		# new test result
		$result = new CourseResult();
		$result->profile_id = $profile->id;
		$result->question_id = $question_id;
		$result->test_id = $test_id;
		$result->answer_id = null;
		$result->is_correct = $isCorrect;
		$result->test_bonuses = $test->bonuses;
		$result->test_questions = $countQuestions;
		$result->calculateBonuses();
		$result->answers = 'Правильные ответы на вопрос: '
			. implode(',', $correctAnswerIds) . '. Участник ответил так: ' . implode(',', $answer_ids);
		$result->paid_at = null;
		$result->paid = false;
		$result->save(false);

		if ($isCorrect) {
			$result->refresh();
			$result->pay();

			# если ответил на каждый вопрос - доплачиваем разницу общую разницу теста
			/** @var CourseResult[] $answeredResults */
			$answeredResults = CourseResult::find()
				->where(['is_correct' => true, 'test_id' => $test_id, 'profile_id' => $profile->id])
				->andWhere("created_at > '$from' AND created_at < '$to'")
				->all();

			if (count($answeredResults) >= $countQuestions) {
				\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, 'Поздравляем! Вы успешно ответили на все вопросы теста');
				$sum = 0;
				foreach ($answeredResults as $result) {
					$sum += $result->bonuses;
				}
				if ($sum < $test->bonuses) {
					$diff = $test->bonuses - $sum;
					$result->payDifference($diff);
				}
			}
			else {
				\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, 'Поздравляем! Вы успешно ответили на вопрос');
			}
		}
		else {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_WARNING, 'Вы предоставили неверный ответ');
		}

		return $this->redirect(['/courses/test/index', 'id' => $test_id, 'step' => $step]);
	}

	public function actionIndex($id, $step=0)
	{
		if (!\Yii::$app->user->identity) {
			return \Yii::$app->response->redirect('/identity/auth/login');
		}

		/** @var Profile $profile */
		$profile = \Yii::$app->user->identity->profile;

		$test = CourseTest::findOne($id);

		if (!$test) {
			throw new NotFoundHttpException();
		}

		# save current step
		$step = ProfileTestPosition::updatePosition($profile->id, $test->id, $step);

		# find questions
		$questions = CourseQuestion::findOrdered($test->id);
		$currentQuestion = $questions[$step - 1];

		# results for current month only
		$date = new \DateTime('first day of this month');
		$from = $date->format('Y-m-d 00:00:00');
		$date = new \DateTime('last day of this month');
		$to = $date->format('Y-m-d 23:59:59');

		/** @var CourseResult[] $results */
		$results = CourseResult::find()
			->where("created_at > '$from' AND created_at < '$to'")
			->andWhere(['profile_id' => $profile->id, 'test_id' => $test->id])
			->all();

		# status of question. -1 = not answered. 0 = correct answered. 1+ = wrong answered
		$questionStatuses = [];

		foreach ($questions as $question) {
			$key = $question->id;

			foreach ($results as $result) {
				if ($result->question_id == $question->id && $result->is_correct) {
					$questionStatuses[$key] = 0;
					break;
				}
			}

			if (!isset($questionStatuses[$key])) {
				foreach ($results as $result) {
					if ($result->question_id == $question->id && $result->is_correct == false) {
						if (isset($questionStatuses[$key])) {
							$questionStatuses[$key] = $questionStatuses[$key] + 1;
						}
						else {
							$questionStatuses[$key] = 1;
						}
					}
				}
			}

			if (!isset($questionStatuses[$key])) {
				$questionStatuses[$key] = -1;
			}
		}

		# correct result for current question if exists
		$correctResult = null;
		foreach ($results as $result) {
			if ($result->question_id == $currentQuestion->id && $result->is_correct) {
				$correctResult = $result;
				break;
			}
		}

		$answers = CourseAnswer::find()
			->where(['question_id' => $currentQuestion->id])
			->orderBy(['position' => SORT_ASC, 'id' => SORT_ASC])
			->all();

		return $this->render('index', compact('test', 'results', 'step', 'profile', 'questions', 'correctResult', 'questionStatuses', 'currentQuestion', 'answers'));
	}
}
