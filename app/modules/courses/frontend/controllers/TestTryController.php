<?php

namespace modules\courses\frontend\controllers;

use modules\courses\common\models\CourseAnswer;
use modules\courses\common\models\CourseQuestion;
use modules\courses\common\models\CourseResult;
use modules\courses\common\models\CourseTest;
use modules\courses\common\models\CourseTry;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\ProfileTestPosition;
use Yii;
use yii\db\Transaction;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class TestTryController extends Controller
{
    const MAX_TRIES = 10;

    public function beforeAction($event)
    {
        \Yii::$app->request->enableCsrfValidation = false;

        return parent::beforeAction($event);
    }

    public function actionAnswer($try_id, $question_id)
    {
        if (!\Yii::$app->user->identity) {
            return \Yii::$app->response->redirect('/identity/auth/login');
        }

        /** @var Profile $profile */
        $profile = \Yii::$app->user->identity->profile;
        /** @var CourseTry $try */
        $try = CourseTry::find()->where(['id' => $try_id, 'profile_id' => $profile->id])->one();

        if (null == $try) {
            throw new NotFoundHttpException();
        }

        $answer_ids = \Yii::$app->request->post('answer_ids', null);
        $test = $try->test;
        $question = CourseQuestion::findOne($question_id);

        if (!$test || !$question || empty($answer_ids)) {
            return $this->redirect(['/courses/test-try/index', 'id' => $test->id]);
        }

        # проверка, что не отвечал еще на этот вопрос
        $result = CourseResult::find()
            ->where(['try_id' => $try->id])
            ->andWhere(['question_id' => $question->id])
            ->one();

        if ($result) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, 'В этой попытке ответа на тест данный вопрос уже дан ответ');
            return $this->redirect(['/courses/test-try/index', 'id' => $test->id]);
        }

        $countQuestions = CourseQuestion::find()->where(['test_id' => $test->id])->count();
        $correctAnswerIds = $question->findCorrectAnswerIds();
        $answer_ids = array_map('intval', $answer_ids);
        sort($correctAnswerIds);
        sort($answer_ids);
        $isCorrect = ($correctAnswerIds == $answer_ids);
        $hasFinished = false;

        $transaction = \Yii::$app->db->beginTransaction();

        try {
            # new test result
            $result = new CourseResult();
            $result->try_id = $try->id;
            $result->profile_id = $profile->id;
            $result->question_id = $question_id;
            $result->test_id = $test->id;
            $result->answer_id = null;
            $result->is_correct = $isCorrect;
            $result->test_bonuses = $test->bonuses;
            $result->test_questions = $countQuestions;
            $result->calculateBonuses();
            $result->answers = 'Правильные ответы на вопрос: '
                . implode(',', $correctAnswerIds) . '. Участник ответил так: ' . implode(',', $answer_ids);
            $result->paid_at = null;
            $result->paid = false;
            $result->save(false);

            if ($isCorrect) {
                \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, 'Поздравляем! Вы успешно ответили на предыдущий вопрос');
                $try->correct_answers = $try->correct_answers + 1;
            }
            else {
                \Yii::$app->session->setFlash(\yz\Yz::FLASH_WARNING, 'Вы предоставили неверный ответ на предыдущий вопрос');
                $try->wrong_answers = $try->wrong_answers + 1;
            }

            $try->step = $try->step + 1;

            # Если прошли весь тест - обновляем статусы
            if ($try->step > $countQuestions) {
                $try->finished = true;
                $try->calculateBonuses();
                $hasFinished = true;
                \Yii::$app->session->setFlash(\yz\Yz::FLASH_INFO, 'Вы завершили тестирование');
            }

            $try->updateAttributes(['step', 'finished', 'bonuses', 'best_try', 'correct_answers', 'wrong_answers']);
            $try->save(false);
            $transaction->commit();

            if ($try->finished) {
                $try->pay();
            }
        }
        catch (\Exception $e) {
            $transaction->rollBack();

            throw $e;
        }

        return $hasFinished
            ? $this->redirect(['/courses/test-try/results', 'id' => $test->id])
            : $this->redirect(['/courses/test-try/index', 'id' => $test->id]);
    }

    public function actionIndex($id)
    {
        if (!\Yii::$app->user->identity) {
            return \Yii::$app->response->redirect('/identity/auth/login');
        }

        /** @var Profile $profile */
        $profile = \Yii::$app->user->identity->profile;
        $test = CourseTest::findOne($id);

        if (!$test) {
            throw new NotFoundHttpException();
        }

        $questions = CourseQuestion::findOrdered($test->id);

        $try = CourseTry::find()
            ->andWhere(['profile_id' => $profile->id, 'test_id' => $test->id, 'finished' => false])
            ->orderBy(['position' => SORT_DESC])
            ->one();

        if (null == $try) {
            $countTries = CourseTry::find()
                ->andWhere(['profile_id' => $profile->id, 'test_id' => $test->id, 'finished' => true])
                ->count('id');

            if ($countTries >= self::MAX_TRIES) {
                return $this->render('max_tries', compact('test', 'profile'));
            }

            $try = new CourseTry();
            $try->profile_id = $profile->id;
            $try->test_id = $test->id;
            $try->position = $countTries + 1;
            $try->step = 1;
            $try->test_questions = count($questions);
            $try->test_bonuses = $test->bonuses;

            $try->save();
            $try->refresh();
        }

        $step = $try->step;

        /** @var CourseResult[] $results */
        $results = CourseResult::find()
            ->where(['profile_id' => $profile->id, 'test_id' => $test->id, 'try_id' => $try->id])
            ->all();

        $resultValues = [];

        foreach ($results as $result) {
            $key = $result->question_id;
            $resultValues[$key] = $result->is_correct;
        }

        return $this->render('index', compact('test', 'profile', 'questions', 'try', 'step', 'resultValues'));
    }

    public function actionResults($id)
    {
        /** @var Profile $profile */
        $profile = \Yii::$app->user->identity->profile;

        $tries = CourseTry::find()
            ->where(['test_id' => $id, 'profile_id' => $profile->id])
            ->orderBy(['position' => SORT_ASC])
            ->all();

        $test = CourseTest::findOne($id);
        $questions = CourseQuestion::findOrdered($test->id);

        return $this->render('results', compact('test', 'tries', 'profile', 'questions'));
    }
}
