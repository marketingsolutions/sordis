<?php

namespace modules\courses\frontend\controllers;

use modules\courses\common\models\Course;
use modules\courses\common\models\CourseResult;
use modules\courses\common\models\CourseTest;
use modules\courses\common\models\CourseTry;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\ProfileCourseVisit;
use ms\loyalty\finances\common\models\Transaction;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CourseController extends Controller
{
    public function actionAll()
    {
        $tests = CourseTest::find()->where(['enabled' => true])->orderBy(['id' => SORT_DESC])->all();

        return $this->render('all', compact(/*'courses',*/ 'tests'));
    }

    public function actionAllHint()
    {
        $courses = Course::find()->where(['enabled' => true])->orderBy(['id' => SORT_DESC])->all();

        return $this->render('all-hint', compact('courses'/*, 'tests'*/));
    }

    /**
     * @return \yii\web\Response
     */
    public function actionNullify()
    {
        /** @var Profile $profile */
        $profile = \Yii::$app->user->identity->profile;

        if (in_array($profile->id, Profile::getTestIds())) {
            $purse = $profile->purse;
            $purse->updateAttributes(['balance' => 0]);
            Transaction::deleteAll(['purse_id' => $purse->id]);
            CourseResult::deleteAll(['profile_id' => $profile->id]);
            CourseTry::deleteAll(['profile_id' => $profile->id]);

            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, 'Баланс обнулен, попытки сброшены');
        }

        return $this->redirect('/dashboard/index');
    }

    public function actionFrame($id)
    {
        if (!\Yii::$app->user->identity) {
            return \Yii::$app->response->redirect('/identity/auth/login');
        }

        $this->layout = '@app/views/layouts/blank';
        $course = Course::findOne($id);

        return $this->render('frame', compact('course'));
    }

    public function actionPdf($id)
    {
        if (!\Yii::$app->user->identity) {
            return \Yii::$app->response->redirect('/identity/auth/login');
        }

        $this->layout = '@app/views/layouts/blank';
        $course = Course::findOne($id);

        return $this->render('pdf', compact('course'));
    }

    public function actionIndex($id)
    {
        if (!\Yii::$app->user->identity) {
            return \Yii::$app->response->redirect('/identity/auth/login');
        }

        $course = Course::findOne($id);

        if (!$course) {
            throw new NotFoundHttpException();
        }

        /** @var Profile $profile */
        $profile = \Yii::$app->user->identity->profile;
        $visit = new ProfileCourseVisit();
        $visit->profile_id = $profile->id;
        $visit->course_id = $course->id;
        $visit->save();

        return $this->render('index', compact('course'));
    }

    public function actionDownload($id)
    {
        if (!\Yii::$app->user->identity) {
            return \Yii::$app->response->redirect('/identity/auth/login');
        }

        $course = Course::findOne($id);

        if (!$course) {
            throw new NotFoundHttpException();
        }

        $filePath = \Yii::getAlias('@data/courses/' . $course->file);
        Yii::$app->response->sendFile($filePath);
    }
}
