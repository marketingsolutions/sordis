<?php

use modules\courses\common\models\Course;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $course Course */

$this->title = $course->title;
$this->params['header'] = $this->title;
$this->params['breadcrumbs'][] = $this->title;

$is_video = !empty($course->video_1_frame)
	|| !empty($course->video_2_frame)
	|| !empty($course->video_3_frame)
	|| !empty($course->video_4_frame)
	|| !empty($course->video_5_frame);

$css = <<<CSS
    h1 {
    	margin-top: -10px !important;
    }
    .fullscreen {
    	position: fixed;
    	top: 0;
    	bottom: 0;
    	left: 0;
    	right: 0;
    	z-index: 1000;
    }
    #close {
    	position: fixed;
    	z-index: 1001;
    	top: 0;
    	right: 0;
    	display: none;
    	padding: 10px;
    }
    #close img {
    	max-width: 20px;
    	max-height: 20px;
    }
    iframe {
    	z-index: 1000;
    	background: white;
    }
    .youtube-title {
    	margin-bottom: 10px !important;
    }
    .youtube-frame {
    	margin-bottom: 25px;
    }
CSS;
$this->registerCss($css);

if ($is_video) {
	$js = <<<JS
	$(document).ready(function() {
		var window_width = $('.container').width();
		if (window_width > 800) {
			window_width = 800;
		}
		var window_height = 315 * window_width / 560;

		$('.youtube-frame iframe').attr('width', window_width).attr('height', window_height);
	});
JS;
}
else {
	$js = <<<JS
	$(document).ready(function() {
		var top_space = 242;
		var left_right_space = 60;
		var window_width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		var window_height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

		if (window_width < ($course->frame_width + left_right_space) || window_height < ($course->frame_height+top_space)) {
			$('iframe').attr('width', window_width).attr('height', window_height).addClass('fullscreen');
			$('#close').show();
		}
	});
JS;
}
$this->registerJs($js);

if (!empty($course->file)) {
	$jsFull = <<<JS
	$(document).ready(function() {
		var window_width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		var window_height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
		$('.youtube').attr('width', window_width).attr('height', window_height).addClass('fullscreen');
		$('#close').show();
	});
JS;
	$this->registerJs($jsFull);
}
?>
<div class="container col-md-12">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <a id="close" href="/">
                        <img src="/images/close.jpg"/>
                    </a>

                    <?php if (!empty($course->description)): ?>
                        <div>
                            <?= $course->description ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($is_video): ?>
                        <?php for ($i = 1; $i < 5; $i++): ?>
                            <?php $propTitle = "video_{$i}_title"; ?>
                            <?php $propFrame = "video_{$i}_frame"; ?>
                            <?php if (!empty($course->$propTitle)): ?>
                                <h3 class="youtube-title"><?= $course->$propTitle ?></h3>
                            <?php endif; ?>
                            <?php if (!empty($course->$propFrame)): ?>
                                <div class="youtube-frame">
                                    <?= $course->$propFrame ?>
                                </div>
                            <?php endif; ?>
                        <?php endfor; ?>

                    <?php elseif (!empty($course->file)): ?>
                        <div>
                            <iframe src="<?= Url::to(['/courses/course/pdf', 'id' => $course->id]) ?>"
                                    width="<?= $course->frame_width ?>" height="<?= $course->frame_height ?>"
                                    frameborder="0" scrolling="no"></iframe>
                        </div>
                    <?php elseif (!empty($course->frame_url)): ?>
                        <div>
                            <iframe src="<?= Url::to(['/courses/course/frame', 'id' => $course->id]) ?>"
                                    width="<?= $course->frame_width ?>" height="<?= $course->frame_height ?>"
                                    frameborder="0" scrolling="no"></iframe>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
