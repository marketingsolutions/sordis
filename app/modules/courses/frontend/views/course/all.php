<?php
use modules\courses\common\models\Course;
use modules\courses\common\models\CourseTest;
use modules\profiles\common\models\Profile;
use yii\helpers\Html;
use yii\helpers\Url;
use ms\loyalty\theme\frontend\widgets\InterfaceWidgets;

/**
 * @var \yii\web\View $this
 * @var Course[] $courses
 * @var CourseTest[] $tests
 */

$css = <<<CSS
    .container.block {
        margin-bottom: 35px;
    }
    .container.block h3 {
        margin-bottom: 15px;
    }
CSS;
$this->registerCss($css);

/** @var Profile $profile */
$profile = \Yii::$app->user->identity->profile;

$this->params['header']        = 'Получить дополнительные баллы';
$this->params['breadcrumbs'][] = 'Получить дополнительные баллы';
?>

<div class="container col-md-12">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Анкетирование
                </div>
                <div class="panel-body">
                    <?php foreach ($tests as $test): ?>
                        <div><b><?= $test->title ?></b></div>
                        <div style="margin-top: 15px;">
                            <a href="<?= Url::to(['/courses/test-try/index', 'id' => $test->id]) ?>"
                               class="btn btn-primary">Пройти</a>
                            <a href="<?= Url::to(['/courses/test-try/results', 'id' => $test->id]) ?>"
                               style="margin-left: 20px;" class="btn btn-default">Результаты</a>
                        </div>
                    <?php endforeach ?>
                    <?php if (in_array($profile->id, Profile::getTestIds())): ?>
                        <div style="padding: 13px 0">
                            <a href="/courses/course/nullify" class="btn btn-warning">
                                Обнулить все баллы и тестовые результаты
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>