<?php
/** @var Course $course */

use modules\courses\common\models\Course;
?>

<?= \yii2assets\pdfjs\PdfJs::widget([
	'url'=> '/data/courses/' . $course->file,
	'options' => [
		'scale' => 'page_scale_actual',
		'style' => 'border: 2px solid rgb(64, 64, 64); width: 100%; height: 100%; background-color: rgb(64, 64, 64);'
	],
	'buttons'=>[
		'presentationMode' => false,
		'openFile' => false,
		'print' => false,
		'download' => false,
		'viewBookmark' => false,
		'secondaryToolbarToggle' => false
	]
]);
?>

