<?php
/** @var Course $course */

use modules\courses\common\models\Course;

$context = stream_context_create(array(
	'http' => array(
		'method' => 'GET',
		'header' => "Host: onedrive.live.com\r\n" .
			"Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3\r\n"
	)
));

$domain = file_get_contents($course->frame_url, false, $context);
$domain = str_replace('</body>','<div style="position:absolute; width: 100px; height: 20px; bottom:2px; right:2px; background:#444444;"></div></body>', $domain);
?>

<?= $domain ?>

