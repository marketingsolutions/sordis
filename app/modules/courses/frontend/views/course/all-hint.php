<?php
use modules\courses\common\models\Course;
use modules\courses\common\models\CourseTest;
use modules\profiles\common\models\Profile;
use yii\helpers\Html;
use yii\helpers\Url;
use ms\loyalty\theme\frontend\widgets\InterfaceWidgets;

/**
 * @var \yii\web\View $this
 * @var Course[] $courses
 * @var CourseTest[] $tests
 */

$css = <<<CSS
    .container.block {
        margin-bottom: 35px;
    }
    .container.block h3 {
        margin-bottom: 15px;
    }
CSS;
$this->registerCss($css);

/** @var Profile $profile */
$profile = \Yii::$app->user->identity->profile;

$this->params['header']        = 'Подсказки';
$this->params['breadcrumbs'][] = 'Подсказки';
?>

<div class="container col-md-12">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Обучающие материалы
                </div>
                <div class="panel-body">
                    <?php foreach ($courses as $course): ?>
                        <?php if ($course->enabled): ?>
                            <div><b><?= $course->title ?></b></div>
                            <div style="margin: 10px 0 20px;">
                                <a href="<?= Url::to(['/courses/course/index', 'id' => $course->id]) ?>"
                                   class="btn btn-primary">Просмотреть</a>
                            </div>
                        <?php endif; ?>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</div>