<?php

use modules\courses\common\models\CourseAnswer;
use modules\courses\common\models\CourseQuestion;
use modules\courses\common\models\CourseResult;
use modules\courses\common\models\CourseTest;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $test CourseTest */
/* @var $questions CourseQuestion[] */
/* @var $currentQuestion CourseQuestion */
/* @var $results CourseResult[] */
/* @var $correctResult CourseResult */
/* @var $step integer */
/* @var $questionStatuses array */
/* @var $answers CourseAnswer[] */

$this->title = 'Тест: ' . $test->title;
$this->params['no-header'] = true;
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
	.question {
		width: 35px;
		height: 35px;
		display: inline-block;
		font-size: 14px;
		border-radius: 50px;
		margin-right: 10px;
		font-weight: bold;
		vertical-align: top;
		padding: 7px 3px 7px;
		border: 3px solid darkgreen;
		text-align: center;
		background-color: white;
	}
	span.question {
		border: 3px solid red;
		color: red !important;
	}
	.question.blank, .question.blank:hover, .question.blank:active {
		color: black;
	}
	label {
	    vertical-align: top;
		margin-left: 5px;
		margin-top: 2px;
	}
	#wrapper .page-header {
		padding: 40px 0 0 0;
	}
	h2 {
	    font-size: 17px;
		font-weight: bold;
		margin: 20px 0 0;
	}
	input[type="radio"], input[type="checkbox"] {
		margin: 2px 6px 0 0;
		width: 18px;
		height: 18px;
	}
CSS;
$this->registerCss($css);
?>
<div class="row container">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
        <div class="questions">
            <?php foreach ($questions as $i => $question): ?>
                <?php if ($step == $question->position): ?>
                    <span
                        title="<?= $questionStatuses[$question->id] < 0 ? '' : ($questionStatuses[$question->id] == 0 ? 'Отвечен' : 'Были ошибки') ?>"
                        class="question label <?= $questionStatuses[$question->id] < 0 ? 'blank' : ($questionStatuses[$question->id] == 0 ? 'label-success' : 'label-warning') ?>">
                        <?= $question->position ?>
                    </span>
                <?php else: ?>
                    <a title="<?= $questionStatuses[$question->id] < 0 ? '' : ($questionStatuses[$question->id] == 0 ? 'Отвечен' : 'Были ошибки') ?>"
                       href="<?= Url::to(['/courses/test/index', 'id' => $test->id, 'step' => $i + 1]) ?>"
                       class="question label <?= $questionStatuses[$question->id] < 0 ? 'blank' : ($questionStatuses[$question->id] == 0 ? 'label-success' : 'label-warning') ?>">
                        <?= $question->position ?>
                    </a>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>

        <h2>
            <?= $currentQuestion->title ?>
        </h2>

        <?php if ($correctResult): ?>
            <h4 style="margin-top: 20px;">Вы успешно ответили на данный вопрос. Бонусные баллы за вопрос: <?= $correctResult->bonuses ?></h4>
        <?php else: ?>
            <form method="POST" style="margin-top:25px;"
                  action="<?= Url::to(['/courses/test/answer', 'test_id' => $test->id, 'question_id' => $currentQuestion->id, 'step' => $step]) ?>">
                <?php foreach ($answers as $answer): ?>
                    <p>
                        <input type="checkbox" name="answer_ids[]" value="<?= $answer->id ?>"
                               id="a-<?= $answer->id ?>"/>
                        <label for="a-<?= $answer->id ?>"><?= $answer->title ?></label>
                    </p>
                <?php endforeach; ?>
                <div style="margin-top: 15px">
                    <button type="submit" class="btn btn-primary">Ответить</button>
                </div>
            </form>
        <?php endif; ?>
            </div>
        </div>
    </div>
</div>

