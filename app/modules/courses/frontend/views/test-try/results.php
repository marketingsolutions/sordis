<?php

use modules\courses\common\models\CourseAnswer;
use modules\courses\common\models\CourseQuestion;
use modules\courses\common\models\CourseResult;
use modules\courses\common\models\CourseTest;
use modules\courses\common\models\CourseTry;
use modules\profiles\common\models\Profile;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $test CourseTest */
/* @var $profile Profile */
/* @var $tries CourseTry[] */
/* @var $questions CourseQuestion[] */

$this->title = 'Тест: ' . $test->title;
$this->params['header'] = 'Результаты';
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
	.questions {
		cursor: default !important;
		margin-top: 15px;
	}
	.question {
		width: 35px;
		height: 35px;
		display: inline-block;
		font-size: 14px;
		border-radius: 50px;
		margin-right: 10px;
		font-weight: bold;
		vertical-align: top;
		padding: 7px 3px 7px;
		border: 3px solid darkgreen;
		text-align: center;
		cursor: default !important;
	}
	span.question {
		border: 3px solid #ccc;
		color: #333 !important;
	}
	.question.blank, .question.blank:hover, .question.blank:active {
		color: #333;
	}
	.question.current {
		background-color: #fff100;
		border-color: #867a00;
	}
	.question.label-success, .question.label-warning {
		border-color: #867074;
	}
	label {
	    vertical-align: top;
		margin-left: 5px;
		margin-top: 2px;
	}
	#wrapper .page-header {
		padding: 40px 0 0 0;
	}
	h2 {
	    font-size: 17px;
		font-weight: bold;
		margin: 20px 0 0;
	}
	input[type="radio"], input[type="checkbox"] {
		margin: 2px 6px 0 0;
		width: 18px;
		height: 18px;
	}
	.q {
		display: none;
	}
	.q.active {
		display: block;
	}
	.correct-answer {
		text-decoration: underline;
	}
CSS;
$this->registerCss($css);
?>
<div class="row container">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php if (empty($tries)): ?>
                    Пройденные тесты отсутствуют
                <?php else: ?>
                    <?php foreach ($tries as $try): ?>
                        <?php
                        /** @var CourseResult[] $results */
                        $results = CourseResult::find()
                            ->where(['profile_id' => $profile->id, 'test_id' => $test->id, 'try_id' => $try->id])
                            ->all();
                        $resultValues = [];

                        foreach ($results as $result) {
                            $key = $result->question_id;
                            $resultValues[$key] = $result->is_correct;
                        }
                        ?>


                        <h2>Тестирование завершено!</h2>
                        <h3>Результаты тестирования</h3>
                        <div class="info">
                            <?php if ($try->correct_answers): ?>
                                Верных ответов: <?= $try->correct_answers ?>.<br/>
                            <?php endif; ?>
                            <?php if ($try->finished): ?>
                                <?php if ($try->extra_bonuses): ?>
                                    Была доплата: <?= $try->extra_bonuses ?> баллов.<br/>
                                <?php elseif ($try->paid): ?>
                                    Бонусов за тест: <?= $try->bonuses ?> баллов.<br/>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>

                        <div class="questions">
                            <?php foreach ($questions as $i => $question): ?>
                                <span
                                    title="<?= !isset($resultValues[$question->id]) ? '' : ($resultValues[$question->id] ? 'Отвечен верно' : 'Невеный ответ') ?>"
                                    class="question label <?= !isset($resultValues[$question->id]) ? 'blank' : ($resultValues[$question->id] ? 'label-success' : 'label-warning') ?>">
                            <?= $i + 1 ?>
                        </span>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>

                <div style="margin-top: 30px;">
                    <a href="<?= Url::to(['/courses/course/all']) ?>" class="btn btn-primary">
                        Назад
                    </a>
                </div>
            </div>
     </div>
</div>



