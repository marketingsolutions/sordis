<?php

use modules\courses\common\models\CourseAnswer;
use modules\courses\common\models\CourseQuestion;
use modules\courses\common\models\CourseResult;
use modules\courses\common\models\CourseTest;
use modules\courses\common\models\CourseTry;
use modules\profiles\common\models\Profile;
use yii\helpers\Url;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $test CourseTest */
/* @var $questions CourseQuestion[] */
/* @var $profile Profile */
/* @var $try CourseTry */
/* @var $step integer */
/* @var $resultValues array */

$this->title = 'Тест: ' . $test->title;
//$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

$css = <<<CSS
	.questions {
		cursor: default !important;
	}
	.question {
		width: 35px;
		height: 35px;
		display: inline-block;
		font-size: 14px;
		border-radius: 50px;
		margin-right: 10px;
		font-weight: bold;
		vertical-align: top;
		padding: 7px 3px 7px;
		border: 3px solid darkgreen;
		text-align: center;
		cursor: default !important;
	}
	span.question {
		border: 3px solid #ccc;
		color: #333 !important;
	}
	.question.blank, .question.blank:hover, .question.blank:active {
		color: #333;
	}
	.question.current {
		background-color: #fff100;
		border-color: #867a00;
	}
	.question.label-success, .question.label-warning {
		border-color: #867074;
	}
	label {
	    vertical-align: top;
		margin-left: 5px;
		margin-top: 2px;
	}
	#wrapper .page-header {
		padding: 40px 0 0 0;
	}
	h2 {
	    font-size: 17px;
		font-weight: bold;
		margin: 20px 0 0;
	}
	input[type="radio"], input[type="checkbox"] {
		margin: 2px 6px 0 0;
		width: 18px;
		height: 18px;
	}
	.q {
		display: none;
	}
	.q.active {
		display: block;
	}
	.correct-answer {
		text-decoration: underline;
	}
CSS;
$this->registerCss($css);

/** @var Profile $profile */
$profile = \Yii::$app->user->identity->profile;
?>
<div class="row container">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="questions">
                    <?php foreach ($questions as $i => $question): ?>
                        <span
                            title="<?= $i+1 == $step ? 'Текущий вопрос' : (!isset($resultValues[$question->id]) ? '' : ($resultValues[$question->id] ? 'Отвечен верно' : 'Невеный ответ')) ?>"
                            class="question label <?= $i+1 == $step ? 'current' : (!isset($resultValues[$question->id]) ? 'blank' : ($resultValues[$question->id] ? 'label-success' : 'label-warning')) ?>">
                            <?= $i + 1 ?>
                        </span>
                    <?php endforeach; ?>
                </div>


                <?php foreach ($questions as $i => $question): ?>
                    <?php /** @var CourseQuestion $question */ ?>
                    <div class="q q-<?= $i + 1 ?> <?= $step == ($i+1) ? 'active' : '' ?>">
                        <h2><?= $question->title ?></h2>

                        <form method="POST" style="margin-top:25px;"
                              action="<?= Url::to(['/courses/test-try/answer', 'try_id' => $try->id, 'question_id' => $question->id]) ?>">
                            <?php foreach ($question->getOrderedAnswers() as $answer): ?>
                                <?php /** @var CourseAnswer $answer */ ?>
                                <p>
                                    <input type="checkbox" name="answer_ids[]" value="<?= $answer->id ?>"
                                           id="a-<?= $answer->id ?>"/>
                                    <label for="a-<?= $answer->id ?>"
                                           class="<?= in_array($profile->id, Profile::getTestIds()) && $answer->is_correct ? 'correct-answer' : '' ?>">
                                        <?= $answer->title ?>
                                    </label>
                                </p>
                            <?php endforeach; ?>
                            <div style="margin-top: 15px">
                                <button type="submit" class="btn btn-primary">Ответить</button>
                            </div>
                        </form>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>

