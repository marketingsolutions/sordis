<?php

use modules\courses\common\models\CourseAnswer;
use modules\courses\common\models\CourseQuestion;
use modules\courses\common\models\CourseResult;
use modules\courses\common\models\CourseTest;
use modules\courses\common\models\CourseTry;
use modules\profiles\common\models\Profile;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $test CourseTest */
/* @var $profile Profile */

$this->title = 'Тест: ' . $test->title;
$this->params['no-header'] = true;
$this->params['breadcrumbs'][] = $this->title;
?>

<p>Вы уже исчерпали максимальное количество попыток прохождения теста на сегодня
	(<?= \modules\courses\frontend\controllers\TestTryController::MAX_TRIES ?>)</p>