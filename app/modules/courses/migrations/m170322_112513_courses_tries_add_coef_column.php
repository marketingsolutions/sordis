<?php

use yii\db\Migration;

class m170322_112513_courses_tries_add_coef_column extends Migration
{
    public function up()
    {
		$this->addColumn('{{%course_tries}}', 'coef', $this->integer());
    }

    public function down()
    {
		$this->dropColumn('{{%course_tries}}', 'coef');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
