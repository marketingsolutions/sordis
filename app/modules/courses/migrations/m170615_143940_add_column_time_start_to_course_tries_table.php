<?php

use yii\db\Migration;

class m170615_143940_add_column_time_start_to_course_tries_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%course_tries}}', 'time_start', $this->integer());
        $this->addColumn('{{%course_tries}}', 'time_finish', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%course_tries}}', 'time_start');
        $this->dropColumn('{{%course_tries}}', 'time_finish');
    }
}
