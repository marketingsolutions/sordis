<?php

use yii\db\Migration;

class m170406_054942_courses_add_video_columns extends Migration
{
	const MAX = 5;

    public function up()
    {
		for ($i = 1; $i <= self::MAX; $i++) {
			$this->addColumn('{{%courses}}', "video_{$i}_title", $this->string());
			$this->addColumn('{{%courses}}', "video_{$i}_frame", $this->text());
		}
    }

    public function down()
    {
		for ($i = 1; $i <= self::MAX; $i++) {
			$this->dropColumn('{{%courses}}', "video_{$i}_title");
			$this->dropColumn('{{%courses}}', "video_{$i}_frame");
		}
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
