<?php

use yii\db\Migration;

class m170127_135645_courses_results_add_answers_column extends Migration
{
    public function up()
    {
		$this->addColumn('{{%course_results}}', 'answers', $this->text());
    }

    public function down()
    {
        $this->dropColumn('{{%course_results}}', 'answers');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
