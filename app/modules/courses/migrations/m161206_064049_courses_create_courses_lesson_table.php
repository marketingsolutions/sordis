<?php

use yii\db\Migration;

class m161206_064049_courses_create_courses_lesson_table extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
		}

		$this->createTable('{{%course_lessons}}', [
			'id' => $this->primaryKey(),
			'position' => $this->integer(),
			'type' => $this->string(),
			'file' => $this->string(),
			'text' => $this->text(),
			'course_id' => $this->integer(),
			'created_at' => $this->dateTime(),
			'updated_at' => $this->dateTime(),
		], $tableOptions);
	}

	public function down()
	{
		$this->dropTable('{{%course_lessons}}');
	}
}
