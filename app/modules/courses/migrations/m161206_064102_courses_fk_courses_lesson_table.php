<?php

use yii\db\Migration;

class m161206_064102_courses_fk_courses_lesson_table extends Migration
{
	public function up()
	{
		$this->addForeignKey('{{%fk_course_id}}',
			'{{%course_lessons}}', 'course_id',
			'{{%courses}}', 'id',
			'RESTRICT', 'CASCADE'
		);
	}

	public function down()
	{
		$this->dropForeignKey('{{%fk_course_id}}', '{{%courses}}');
	}
}
