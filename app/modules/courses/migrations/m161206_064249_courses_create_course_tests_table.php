<?php

use yii\db\Migration;

class m161206_064249_courses_create_course_tests_table extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
		}

		$this->createTable('{{%course_tests}}', [
			'id' => $this->primaryKey(),
			'title' => $this->string(),
			'course_id' => $this->integer(),
		], $tableOptions);
	}

	public function down()
	{
		$this->dropTable('{{%course_tests}}');
	}
}
