<?php

use yii\db\Migration;

class m161206_083137_courses_create_course_questions_table extends Migration
{
    public function up()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
		}

		$this->createTable('{{%course_questions}}', [
			'id' => $this->primaryKey(),
			'title' => $this->string(),
			'position' => $this->integer(),
			'test_id' => $this->integer(),
			'created_at' => $this->dateTime(),
			'updated_at' => $this->dateTime(),
		], $tableOptions);
    }

    public function down()
    {
		$this->dropTable('{{%course_questions}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
