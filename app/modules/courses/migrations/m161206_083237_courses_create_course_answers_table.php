<?php

use yii\db\Migration;

class m161206_083237_courses_create_course_answers_table extends Migration
{
    public function up()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
		}

		$this->createTable('{{%course_answers}}', [
			'id' => $this->primaryKey(),
			'title' => $this->string(),
			'position' => $this->integer(),
			'is_correct' => $this->boolean()->defaultValue(false),
			'question_id' => $this->integer(),
			'created_at' => $this->dateTime(),
			'updated_at' => $this->dateTime(),
		], $tableOptions);
    }

    public function down()
    {
		$this->dropTable('{{%course_answers}}');
    }
}
