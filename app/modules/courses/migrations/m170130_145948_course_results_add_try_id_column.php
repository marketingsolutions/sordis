<?php

use yii\db\Migration;

class m170130_145948_course_results_add_try_id_column extends Migration
{
    public function up()
    {
		$this->addColumn('{{%course_results}}', 'try_id', $this->integer());
    }

    public function down()
    {
		$this->dropColumn('{{%course_results}}', 'try_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
