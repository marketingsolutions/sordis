<?php

use yii\db\Migration;

/**
 * Class m171005_070140_course_test_add_enabled_column
 */
class m171005_070140_courses_tests_add_enabled_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%course_tests}}', 'enabled', $this->boolean()->defaultValue(true));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%course_tests}}', 'enabled');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171005_070140_course_test_add_enabled_column cannot be reverted.\n";

        return false;
    }
    */
}
