<?php

use yii\db\Migration;

class m170410_063516_courses_add_position_column extends Migration
{
    public function up()
    {
		$this->addColumn('{{%courses}}', 'position', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%courses}}', 'position');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
