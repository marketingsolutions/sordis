<?php

use yii\db\Migration;

class m170615_142703_add_column_time_min_for_course_tests_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%course_tests}}', 'time_min', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%course_tests}}', 'time_min');
    }

}
