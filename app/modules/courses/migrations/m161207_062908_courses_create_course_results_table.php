<?php

use yii\db\Migration;

class m161207_062908_courses_create_course_results_table extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
		}

		$this->createTable('{{%course_results}}', [
			'id' => $this->primaryKey(),
			'profile_id' => $this->integer(),
			'test_id' => $this->integer(),
			'question_id' => $this->integer(),
			'answer_id' => $this->integer(),
			'is_correct' => $this->boolean(),
			'test_bonuses' => $this->integer(),
			'test_questions' => $this->integer(),
			'bonuses' => $this->integer(),
			'paid' => $this->boolean(),
			'paid_at' => $this->dateTime(),
			'created_at' => $this->dateTime(),
			'updated_at' => $this->dateTime(),
		], $tableOptions);
	}

	public function down()
	{
		$this->dropTable('{{%course_results}}');
	}
}
