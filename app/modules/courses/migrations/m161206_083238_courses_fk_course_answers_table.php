<?php

use yii\db\Migration;

class m161206_083238_courses_fk_course_answers_table extends Migration
{
	public function up()
	{
		$this->addForeignKey('{{%fk-question-id}}',
			'{{%course_answers}}', 'question_id',
			'{{%course_questions}}', 'id',
			'RESTRICT', 'CASCADE'
		);
	}

	public function down()
	{
		$this->dropForeignKey('{{%fk-question-id}}', '{{%course_answers}}');
	}
}
