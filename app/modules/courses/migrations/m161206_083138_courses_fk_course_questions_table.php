<?php

use yii\db\Migration;

class m161206_083138_courses_fk_course_questions_table extends Migration
{
	public function up()
	{
		$this->addForeignKey('{{%fk-test-id}}',
			'{{%course_questions}}', 'test_id',
			'{{%course_tests}}', 'id',
			'RESTRICT', 'CASCADE'
		);
	}

	public function down()
	{
		$this->dropForeignKey('{{%fk-test-id}}', '{{%course_questions}}');
	}
}
