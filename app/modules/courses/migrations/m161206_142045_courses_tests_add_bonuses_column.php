<?php

use yii\db\Migration;

class m161206_142045_courses_tests_add_bonuses_column extends Migration
{
    public function up()
    {
		$this->addColumn('{{%course_tests}}', 'bonuses', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%course_tests}}', 'bonuses');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
