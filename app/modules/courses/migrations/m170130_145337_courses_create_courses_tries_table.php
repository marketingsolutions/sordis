<?php

use yii\db\Migration;

class m170130_145337_courses_create_courses_tries_table extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
		}

		$this->createTable('{{%course_tries}}', [
			'id' => $this->primaryKey(),
			'profile_id' => $this->integer(),
			'test_id' => $this->integer(),
			'position' => $this->integer(),
			'step' => $this->integer()->defaultValue(1),
			'correct_answers' => $this->integer()->defaultValue(0),
			'wrong_answers' => $this->integer()->defaultValue(0),
			'test_bonuses' => $this->integer(),
			'test_questions' => $this->integer(),
			'bonuses' => $this->integer(),
			'finished' => $this->boolean()->defaultValue(false),
			'best_try' => $this->boolean()->defaultValue(false),
			'paid' => $this->boolean()->defaultValue(false),
			'paid_at' => $this->dateTime(),
			'created_at' => $this->dateTime(),
			'updated_at' => $this->dateTime(),
		], $tableOptions);
	}

	public function down()
	{
		$this->dropTable('{{%course_tries}}');
	}
}
