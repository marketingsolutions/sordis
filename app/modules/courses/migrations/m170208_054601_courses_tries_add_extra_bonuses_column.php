<?php

use yii\db\Migration;

class m170208_054601_courses_tries_add_extra_bonuses_column extends Migration
{
    public function up()
    {
		$this->addColumn('{{%course_tries}}', 'extra_bonuses', $this->integer());
    }

    public function down()
    {
		$this->dropColumn('{{%course_tries}}', 'extra_bonuses');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
