<?php

use yii\db\Migration;

class m170109_130202_courses_add_frame_columns extends Migration
{
    public function up()
    {
		$this->addColumn('{{%courses}}', 'frame_url', $this->text());
		$this->addColumn('{{%courses}}', 'frame_width', $this->integer());
		$this->addColumn('{{%courses}}', 'frame_height', $this->integer());
    }

    public function down()
    {
		$this->dropColumn('{{%courses}}', 'frame_url');
		$this->dropColumn('{{%courses}}', 'frame_width');
		$this->dropColumn('{{%courses}}', 'frame_height');
    }
}
