<?php

use yii\db\Migration;

class m170130_150130_course_results_fk_try_id_column extends Migration
{
    public function up()
    {
		$this->addForeignKey('fktryid',
			'{{%course_results}}', 'try_id',
			'{{%course_tries}}', 'id',
			'RESTRICT', 'CASCADE'
		);
    }

    public function down()
    {
        $this->dropForeignKey('fktryid', '{{%course_results}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
