<?php

use yii\db\Migration;

class m170109_095814_courses_courses_add_html_column extends Migration
{
    public function up()
    {
		$this->addColumn('{{%courses}}', 'html', $this->text());
    }

    public function down()
    {
        $this->dropColumn('{{%courses}}', 'html');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
