<?php

use yii\db\Migration;

class m161206_064250_courses_fk_course_tests_table extends Migration
{
	public function up()
	{
		$this->addForeignKey('{{%fk-course-id}}',
			'{{%course_tests}}', 'course_id',
			'{{%courses}}', 'id',
			'RESTRICT', 'CASCADE'
		);
	}

	public function down()
	{
		$this->dropForeignKey('{{%fk-course-id}}', '{{%course_tests}}');
	}
}
