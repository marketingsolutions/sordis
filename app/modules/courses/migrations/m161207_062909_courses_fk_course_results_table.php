<?php

use yii\db\Migration;

class m161207_062909_courses_fk_course_results_table extends Migration
{
	public function up()
	{
		$this->addForeignKey('profile_id',
			'{{%course_results}}', 'profile_id',
			'{{%profiles}}', 'id',
			'RESTRICT', 'CASCADE'
		);

		$this->addForeignKey('test_id',
			'{{%course_results}}', 'test_id',
			'{{%course_tests}}', 'id',
			'RESTRICT', 'CASCADE'
		);

		$this->addForeignKey('question_id',
			'{{%course_results}}', 'question_id',
			'{{%course_questions}}', 'id',
			'RESTRICT', 'CASCADE'
		);

		$this->addForeignKey('answer_id',
			'{{%course_results}}', 'answer_id',
			'{{%course_answers}}', 'id',
			'RESTRICT', 'CASCADE'
		);
	}

	public function down()
	{

	}
}
