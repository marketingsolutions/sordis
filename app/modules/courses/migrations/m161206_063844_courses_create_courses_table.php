<?php

use yii\db\Migration;

class m161206_063844_courses_create_courses_table extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
		}

		$this->createTable('{{%courses}}', [
			'id' => $this->primaryKey(),
			'title' => $this->string(),
			'description' => $this->text(),
			'file' => $this->string(),
		], $tableOptions);
	}

	public function down()
	{
		$this->dropTable('{{%courses}}');
	}
}
