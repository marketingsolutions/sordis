<?php

use yii\db\Migration;

class m170406_071744_courses_add_enabled_column extends Migration
{
    public function up()
    {
		$this->addColumn('{{%courses}}', 'enabled', $this->boolean()->defaultValue(true));
    }

    public function down()
    {
		$this->dropColumn('{{%courses}}', 'enabled');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
