<?php
namespace modules\bonuses\frontend\controllers;

use modules\bonuses\common\models\Bonus;
use modules\bonuses\common\models\TempBonuses;
use modules\bonuses\common\models\TempConfirmedBonuses;
use modules\social\common\models\TargetUsers;
use yii\web\Controller;
use yii\filters\AccessControl;

class TargetBonusController extends Controller
{


    /**
     * Раздел целевые акции
     * @return string
     */
    public function actionIndex()
    {
        $profile = \Yii::$app->user->identity->profile;
        $targets = TargetUsers::getUserTargets($profile->id);
        return $this->render('index', [
            'profile' => $profile,
            'targets' => $targets,
        ]);
    }
}
