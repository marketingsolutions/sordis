<?php

use modules\bonuses\common\models\Bonus;

$this->title = 'Целевые акции';
$this->params['header'] = $this->title;;
$this->params['breadcrumbs'][] = $this->title;

?>

<?php if (!empty($targets)): ?>
  <div class="row container">
    <!--    <h2>Целевые акции</h2>-->
    <?php foreach ($targets as $item): ?>
      <div class="panel_ferma targets-block">
        <div class="targets-block--head">
          <div class="targets-block--th1">Наименование акции</div>
          <div class="targets-block--th2">Условия акции</div>
          <div class="targets-block--th3">Дата проведения</div>
        </div>

        <div class="targets-block--body">
          <div class="targets-block--td1">
            <div class="title-pink sm-hide">Цель:</div>
              <?= $item['name_target'] ?>
          </div>
          <div class="targets-block--td2">
            <div class="title-pink sm-hide">Условие:</div>
              <?= $item['description_target'] ?>
          </div>
          <div class="targets-block--td3">
            <div class="title-pink sm-hide">Дата проведения:</div>
            <span>с <?= date("d.m.Y", strtotime($item['from'])) ?></span>
            <span>по <?= date("d.m.Y", strtotime($item['to'])) ?></span>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
<?php endif; ?>

<?php if (empty($targets)): ?>
  <div class="row container">
    <div class="col-md-12">

      <h4>У Вас нет поставленных целей.</h4>

    </div>
  </div>
<?php endif; ?>
