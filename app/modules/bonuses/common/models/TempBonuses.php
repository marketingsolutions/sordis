<?php

namespace modules\bonuses\common\models;

use modules\polls\common\models\Poll;
use modules\profiles\common\models\Profile;
use modules\sales\common\models\DistributorProfile;
use modules\social\common\models\SocialActionRules;
use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_temp_bonuses".
 *
 * @property integer $id
 * @property integer $poll_id
 * @property string $bonus_type
 * @property string $target_id
 * @property string $target_name
 * @property string $code_egais
 * @property string $inn
 * @property string $phone_mobile
 * @property string $license_number
 * @property string $period
 * @property integer $bonus_sum
 * @property integer $state
 * @property integer $action_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $paid_at
 */
class TempBonuses extends \yii\db\ActiveRecord implements ModelInfoInterface
{

    const BASIC_BONUS = 'basic';
    const TARGET_BONUS = 'target';
    const SOCIAL_BONUS = 'social';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%temp_bonuses}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Temp Bonuses';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Начисленные баллы';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['bonus_type', 'string', 'max' => 25],
            ['code_egais', 'string', 'max' => 25],
            ['phone_mobile', 'string', 'max' => 25],
            [['target_id', 'target_name'], 'string'],
            ['inn', 'string', 'max' => 25],
            ['license_number', 'string', 'max' => 25],
            ['period', 'safe'],
            ['bonus_sum', 'integer'],
            ['state', 'integer'],
            ['poll_id', 'integer'],
            ['action_id', 'integer'],
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
            ['paid_at', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bonus_type' => 'Типп баллов',
            'code_egais' => 'Код ЕГАИС',
            'inn' => 'ИНН',
            'license_number' => 'Номер лицензии',
            'period' => 'Отчетный период',
            'bonus_sum' => 'Сумма бонусов',
            'created_at' => 'Дата загрузки',
            'updated_at' => 'Дата обновления',
            'paid_at' => 'Дата подтверждения',
            'phone_mobile' => 'Мобильный телефон участника',
            'target_id' => 'ID целевой акции в системе Sordis',
            'target_name' => 'Название целевой акции',
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::BASIC_BONUS => 'Основные баллы',
            self::TARGET_BONUS => 'Целевые баллы',
            self::SOCIAL_BONUS => 'Призовые баллы',
        ];
    }

    /**
     * @return array
     */
    public static function periodOptions(){
        $arrMonth = [
            '01'=>'Январь',
            '02'=>'Февраль',
            '03'=>'Март',
            '04'=>'Апрель',
            '05'=>'Май',
            '06'=>'Июнь',
            '07'=>'Июль',
            '08'=>'Август',
            '09'=>'Сентябрь',
            '10'=>'Октябрь',
            '11'=>'Ноябрь',
            '12'=>'Декабрь',
        ];
        $arrReturn=[];
        $model = self::find()->groupBy('period')->asArray()->all();
        foreach ($model as $period){
            $value = $arrMonth[date("m", strtotime($period['period']))];
            $arrReturn[$period['period']] = $value." ".date("Y", strtotime($period['period']));
        }
        return $arrReturn;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile(){
        return $this->hasOne(Profile::className(), ['phone_mobile' => 'phone_mobile']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll(){
        return $this->hasOne(Poll::className(), ['id' => 'poll_id']);
    }

    /**
     * @return null
     */
    public function getPollTitle(){
        return isset($this->poll->title) ? $this->poll->title : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocial(){
        return $this->hasOne(SocialActionRules::className(), ['id' => 'action_id']);
    }

    public function getSocialName(){
        return isset($this->social->name) ? $this->social->name : null;
    }

    /**
     * @return mixed
     */
    public function getProfileName(){
        return isset($this->profile->full_name) ? $this->profile->full_name : null;
    }

    /**
     * @return mixed
     */
    public function getProfileRole(){
        return isset($this->profile->role) ? $this->profile->role : null;
    }

    /**
     * @return mixed
     */
    public static function getBonusSum(){

        if(empty(\Yii::$app->request->get('TempBonusesSearch')['period'])) {
            return self::find()->sum('bonus_sum');
        }else{
            return self::find()->where(['period' => \Yii::$app->request->get('TempBonusesSearch')['period'] ])->sum('bonus_sum');
        }
    }


    /**
     * Подсчет неподтвержденных начисленных бонусов для РТТ
     * @param null $bonusType
     * @param null $period
     * @param $phoneMobile
     * @return int|mixed
     */
    public static function calculateBonusSum($bonusType = null, $period = null, $phoneMobile, $codeEGAIS = null)
    {
        $query = TempBonuses::find();
        if(!$period && !$bonusType && $phoneMobile && !$codeEGAIS){
            $sum = $query->where(['phone_mobile' => $phoneMobile])
                ->sum('bonus_sum');
        }elseif (!$bonusType && $period && $phoneMobile  && !$codeEGAIS){
            $sum = $query->where(['phone_mobile' => $phoneMobile])
                ->andWhere(['period' => $period])
                ->sum('bonus_sum');
        }elseif(!$bonusType && !$period && $phoneMobile  && $codeEGAIS){
            $sum = $query->where(['phone_mobile' => $phoneMobile])
                ->andWhere(['code_egais' => $codeEGAIS])
                ->sum('bonus_sum');
        }elseif($bonusType && $period && $phoneMobile  && !$codeEGAIS){
                $sum = $query->where(['phone_mobile' => $phoneMobile])
                ->andWhere(['period' => $period])
                ->andWhere(['bonus_type' => $bonusType])
                ->sum('bonus_sum');
        }elseif (!$bonusType && $period && $phoneMobile  && $codeEGAIS){
            $sum = $query->where(['phone_mobile' => $phoneMobile])
                ->andWhere(['period' => $period])
                ->andWhere(['code_egais' => $codeEGAIS])
                ->sum('bonus_sum');
        }else{
            $sum =  $query->where(['phone_mobile' => $phoneMobile])
                    ->andWhere(['period' => $period])
                    ->andWhere(['code_egais' => $codeEGAIS])
                    ->andWhere(['bonus_type' => $bonusType])
                    ->sum('bonus_sum');
        }
        if(!$sum){
            return 0;
        }
        return $sum;
    }


    /**
     * Получаем рабочие периоды
     * @return array
     */
    public static function getCurrentPeriod()
    {
        return TempBonuses::find()
            ->select('period')
            ->groupBy('period')
            ->column();
    }



    /**
     * @param $monthNum
     * @return mixed
     */
    public static function getMonthName($monthNum)
    {
        $month = [
            "01"=>"Январь",
            "02"=>"Февраль",
            "03"=>"Март",
            "04"=>"Апрель",
            "05"=>"Май",
            "06"=>"Июнь",
            "07"=>"Июль",
            "08"=>"Август",
            "09"=>"Сентябрь",
            "10"=>"Октябрь",
            "11"=>"Ноябрь",
            "12"=>"Декабрь",
        ];
        return $month[$monthNum];
    }
}
