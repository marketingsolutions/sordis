<?php

namespace modules\bonuses\common\models;

use modules\profiles\common\models\Profile;
use ms\loyalty\catalog\common\components\CertificatesCatalogPartner;
use ms\loyalty\finances\common\models\Transaction;
use ms\loyalty\prizes\payments\common\models\Payment;
use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_temp_confirmed_bonuses".
 *
 * @property integer $id
 * @property string $bonus_type
 * @property string $code_egais
 * @property string $inn
 * @property string $license_number
 * @property string $period
 * @property integer $bonus_sum
 * @property integer $state
 * @property string $created_at
 * @property string $updated_at
 * @property string $confirm_points_id
 * @property string $paid_at
 * @property string $phone_mobile
 */
class TempConfirmedBonuses extends \yii\db\ActiveRecord implements ModelInfoInterface
{

    const BASIC_BONUS = 'basic';
    const TARGET_BONUS = 'target';
    const SOCIAL_BONUS = 'social';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%temp_confirmed_bonuses}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Temp Confirmed Bonuses';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Подтвержденные баллы';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['bonus_type', 'string', 'max' => 25],
            ['code_egais', 'string', 'max' => 25],
            ['inn', 'string', 'max' => 25],
            ['license_number', 'string', 'max' => 25],
            ['period', 'safe'],
            ['state', 'integer'],
            ['bonus_sum', 'integer'],
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
            ['paid_at', 'safe'],
            ['phone_mobile', 'string', 'max' => 25],
            ['confirm_points_id', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bonus_type' => 'Тип баллов',
            'code_egais' => 'Код ЕГАИС',
            'inn' => 'ИНН',
            'license_number' => 'Номер лицензии',
            'period' => 'Отчетный период',
            'bonus_sum' => 'Сумма бонусов',
            'created_at' => 'Дата загрузки',
            'updated_at' => 'Дата обновления',
            'paid_at' => 'Дата подтверждения',
            'phone_mobile' => 'Мобильный телефон участника',
            'target_id' => 'ID целевой акции в системе Sordis',
            'target_name' => 'Название целевой акции',
            'confirm_points_id' => 'ID начисления подтвержденных бонусов',
            'state' => 'Статус РТТ (для картинки в ЛК)',
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::BASIC_BONUS => 'Основные баллы',
            self::TARGET_BONUS => 'Целевые баллы',
            self::SOCIAL_BONUS => 'Призовые баллы',
        ];
    }

    /**
     * @return array
     */
    public static function periodOptions(){
        $arrMonth = [
            '01'=>'Январь',
            '02'=>'Февраль',
            '03'=>'Март',
            '04'=>'Апрель',
            '05'=>'Май',
            '06'=>'Июнь',
            '07'=>'Июль',
            '08'=>'Август',
            '09'=>'Сентябрь',
            '10'=>'Октябрь',
            '11'=>'Ноябрь',
            '12'=>'Декабрь',
        ];
        $arrReturn=[];
        $model = self::find()->groupBy('period')->asArray()->all();
        foreach ($model as $period){
            $value = $arrMonth[date("m", strtotime($period['period']))];
            $arrReturn[$period['period']] = $value." ".date("Y", strtotime($period['period']));
        }
        return $arrReturn;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile(){
        return $this->hasOne(Profile::className(), ['phone_mobile' => 'phone_mobile']);
    }

    /**
     * @return mixed
     */
    public function getProfileName(){
        return isset($this->profile->full_name) ? $this->profile->full_name : null;
    }

    /**
     * @return mixed
     */
    public function getProfileRole(){
        return isset($this->profile->role) ? $this->profile->role : null;
    }

    /**
     * @return mixed
     */
    public static function getBonusSum(){

        if(empty(\Yii::$app->request->get('TempConfirmedBonusesSearch')['period'])) {
            return self::find()->sum('bonus_sum');
        }else{
            return self::find()->where(['period' => \Yii::$app->request->get('TempConfirmedBonusesSearch')['period'] ])->sum('bonus_sum');
        }
    }


    /**
     * Получаем рабочие периоды поподтвержденным баллам
     * @return array
     */
    public static function getCurrentPeriod()
    {
        $query1 =  TempConfirmedBonuses::find()
            ->select('period')
            ->orderBy(['period' =>SORT_DESC])
            ->groupBy('period')
            //->limit(12)
            ->column();
        $query2 = \marketingsolutions\finance\models\Transaction::find()
            ->select(['DATE_FORMAT(created_at , "%Y-%m-01")'])
            ->groupBy(['DATE_FORMAT(created_at , "%Y-%m-01")'])
            ->column();
        $period = array_merge($query1, $query2);
        $period=array_unique($period);
        rsort($period);
        array_slice($period, 0, 12);
        return $period;
    }

    /*Считаем общуб сумму подтвержденных бонусов*/
    public static function getAllBonusSum($periodArr, $profileId)
    {
        $sum=0;
        if(empty ($periodArr)){
            return 0;
        }
        $phone_mobile = Profile::findOne($profileId)->phone_mobile;
        foreach ($periodArr as $period){
           $sum += TempConfirmedBonuses::find()
                ->where(['period' => $period])
                ->andWhere(['phone_mobile' => $phone_mobile])
                ->sum('bonus_sum');
        }
        return $sum;
    }

    /**
     * Подсчет подтвержденных бонусов
     * @param null $bonusType
     * @param null $period
     * @param $phoneMobile
     * @param null $codeEGAIS
     * @return int|mixed
     */
    public static function calculateBonusSum($bonusType = null, $period = null, $phoneMobile, $codeEGAIS = null)
    {
        $query = TempConfirmedBonuses::find();
        if(!$period && !$bonusType && $phoneMobile && !$codeEGAIS){
            $sum = $query->where(['phone_mobile' => $phoneMobile])
                ->sum('bonus_sum');
        }elseif (!$bonusType && $period && $phoneMobile  && !$codeEGAIS){
            $sum =  $query->where(['phone_mobile' => $phoneMobile])
                ->andWhere(['period' => $period])
                ->sum('bonus_sum');
        }elseif(!$bonusType && !$period && $phoneMobile  && $codeEGAIS){
            $sum =  $query->where(['phone_mobile' => $phoneMobile])
                ->andWhere(['code_egais' => $codeEGAIS])
                ->sum('bonus_sum');
        }elseif($bonusType && $period && $phoneMobile  && !$codeEGAIS){
            $sum =  $query->where(['phone_mobile' => $phoneMobile])
                ->andWhere(['period' => $period])
                ->andWhere(['bonus_type' => $bonusType])
                ->sum('bonus_sum');
        }
        else{
            $sum =  $query->where(['phone_mobile' => $phoneMobile])
                ->andWhere(['period' => $period])
                ->andWhere(['code_egais' => $codeEGAIS])
                ->andWhere(['bonus_type' => $bonusType])
                ->sum('bonus_sum');
        }
        if(!$sum){
            return 0;
        }
        return $sum;
    }


    /**
     * Рассчет потраченных баллов за период
     * @param $period
     * @param $purse_id
     * @return int|mixed
     */
    public static function calculateSpendBonusesSum($period=null, $purse_id)
    {

        if($period){
            $sum = Transaction::find()->where(['purse_id' => $purse_id ])
                ->andWhere(['type' => Transaction::OUTBOUND])
                ->andWhere([ '>=','{{%finance_transactions}}.created_at', date("Y-m-d 00:00:00", strtotime($period))])
                ->andWhere([ '<=','{{%finance_transactions}}.created_at', date("Y-m-t 23:59:59", strtotime($period))])
                ->sum('amount');
            $sumVozvratCertificat = Transaction::find()->where(['purse_id' => $purse_id ])
                ->andWhere(['type' => Transaction::INCOMING])
                ->andWhere(['partner_type' => CertificatesCatalogPartner::className()])
                ->andWhere([ '>=','{{%finance_transactions}}.created_at', date("Y-m-d 00:00:00", strtotime($period))])
                ->andWhere([ '<=','{{%finance_transactions}}.created_at', date("Y-m-t 23:59:59", strtotime($period))])
                ->sum('amount');
            if(!$sumVozvratCertificat){
                $sumVozvratCertificat=0;
            }

            if($sum){
                return $sum-$sumVozvratCertificat;
            }
        }else{
            $sum = Transaction::find()->where(['purse_id' => $purse_id ])
                ->andWhere(['type' => Transaction::OUTBOUND])
                //->andWhere([ '>=','{{%finance_transactions}}.created_at', date("Y-m-d 00:00:00", strtotime($period))])
                //->andWhere([ '<=','{{%finance_transactions}}.created_at', date("Y-m-t 23:59:59", strtotime($period))])
                ->sum('amount');
            $sumVozvratCertificat = Transaction::find()->where(['purse_id' => $purse_id ])
                ->andWhere(['type' => Transaction::INCOMING])
                ->andWhere(['partner_type' => CertificatesCatalogPartner::className()])
                //->andWhere([ '>=','{{%finance_transactions}}.created_at', date("Y-m-d 00:00:00", strtotime($period))])
                //->andWhere([ '<=','{{%finance_transactions}}.created_at', date("Y-m-t 23:59:59", strtotime($period))])
                ->sum('amount');
            if(!$sumVozvratCertificat){
                $sumVozvratCertificat=0;
            }

            if($sum){
                return $sum-$sumVozvratCertificat;
            }
        }
    }
}
