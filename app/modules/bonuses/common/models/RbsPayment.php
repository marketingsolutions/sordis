<?php

namespace modules\bonuses\common\models;

use modules\profiles\common\models\Profile;
use ms\loyalty\prizes\payments\common\models\Payment;

/**
 * Class RbsPayment
 *
 * @package modules\bonuses\common\models
 *
 * @var string $loyalty_1c_name
 * @var integer $dealer_id
 */
class RbsPayment extends Payment
{
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $uid = $this->loyalty_1c_name . '|' . $this->id;
            $this->updateAttributes(['uid' => $uid]);
        }
    }
}
