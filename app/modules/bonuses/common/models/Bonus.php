<?php

namespace modules\bonuses\common\models;

use modules\profiles\common\models\Action;
use modules\profiles\common\models\Profile;
use ms\loyalty\catalog\common\models\CatalogOrder;
use ms\loyalty\catalog\common\models\OrderedCard;
use ms\loyalty\prizes\payments\common\models\Payment;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_bonuses".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property integer $bonuses
 * @property integer $month
 * @property integer $action_id
 * @property integer $year
 * @property boolean $duple
 * @property boolean $paid
 * @property boolean $sms_sent
 * @property string $paid_at
 * @property string $target_id
 * @property string $target_name
 * @property string $created_at
 * @property string $updated_at
 * @property string $bonus_type
 * @property string $poll_id
 * @property string $code_egais
 * @property string $phone_mobile
 * @property string $inn
 * @property string $license_number
 * @property string $period
 *
 * @property Profile $profile
 */
class Bonus extends \yii\db\ActiveRecord implements ModelInfoInterface
{


    const TYPE_TEMP_TARGET_BONUS = 'temp';

    const TYPE_CONFIRM_TARGET_BONUS = 'confirm';

    public static function getTargetBonusType(){
        return [
            self::TYPE_TEMP_TARGET_BONUS => 'Неподтвержденные целевые баллы',
            self::TYPE_CONFIRM_TARGET_BONUS => 'Подтвержденные целевые баллы',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bonuses}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Bonus';
    }

	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'value' => new Expression('NOW()'),
			],
		];
	}

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Зачисление баллов на кошельки участников';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['profile_id', 'integer'],
            ['profile_id', 'required'],
            ['bonuses', 'integer'],
            ['bonuses', 'required'],
            ['month', 'integer'],
            ['year', 'integer'],
            ['paid', 'safe'],
			['paid_at', 'safe'],
			['duple', 'safe'],
			['sms_sent', 'safe'],
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
            ['bonus_type', 'safe'],
            ['action_id', 'integer'],
            ['target_id', 'safe'],
            ['target_name', 'safe'],
            ['period', 'safe'],
            ['phone_mobile', 'safe'],
            ['poll_id', 'integer'],
            ['code_egais', 'string'],
            ['inn', 'string'],
            ['license_number', 'string'],
            ['period', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Profile ID',
            'bonuses' => 'Бонусы',
            'month' => 'Месяц',
            'year' => 'Год',
            'paid' => 'Зачислены на кошелек',
            'paid_at' => 'Когда зачислены',
            'duple' => 'Дубликат (не начисляется)',
			'sms_sent' => 'Было отправлено СМС',
			'created_at' => 'Загружено',
            'updated_at' => 'Обновлено',
            'bonus_type' => 'Тип баллов',
            'target_id' => 'ID акции в системе Sordis',
            'target_name' => 'Название акции',
        ];
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProfile()
	{
		return empty($this->hasOne(Profile::className(), ['id' => 'profile_id'])) ? null : $this->hasOne(Profile::className(), ['id' => 'profile_id']);
	}

	public function getProfileFullname(){
	    return empty($this->profile->full_name) ? null : $this->profile->full_name;
    }

    public function getProfileRole(){
        return empty($this->profile->role) ? null : $this->profile->role;
    }

    public function getProfileEmail(){
        return empty($this->profile->email) ? null : $this->profile->email;
    }

    public function getPhoneMobile(){
        return empty($this->profile->phone_mobile) ? null : $this->profile->phone_mobile;
    }

    /**
     * @return array
     */
    public static function periodOptions(){
        $arrMonth = [
            '01'=>'Январь',
            '02'=>'Февраль',
            '03'=>'Март',
            '04'=>'Апрель',
            '05'=>'Май',
            '06'=>'Июнь',
            '07'=>'Июль',
            '08'=>'Август',
            '09'=>'Сентябрь',
            '10'=>'Октябрь',
            '11'=>'Ноябрь',
            '12'=>'Декабрь',
        ];
        $arrReturn=[];
        $model = self::find()->groupBy('period')->asArray()->all();
        foreach ($model as $period){
            $value = $arrMonth[date("m", strtotime($period['period']))];
            $arrReturn[$period['period']] = $value." ".date("Y", strtotime($period['period']));
        }
        return $arrReturn;
    }


    /**
     * Берем все бонусы наростающим итогом
     * @return string
     */
    public static function arrSpentBonus(){
        //Платежи без комисии
        $spentPay = Profile::find()
            ->select('{{%payments}}.id as idp, {{%profiles}}.phone_mobile as Participant, {{%payments}}.amount as PointSum, {{%payments}}.company_money_amount as MoneySum, {{%payments}}.created_at as SpentDate, {{%payments}}.type as PoinsType')
            ->InnerJoin('{{%payments}}', '{{%profiles}}.id = {{%payments}}.recipient_id')
            ->where(['{{%payments}}.status' => Payment::STATUS_SUCCESS])
            ->asArray()
            ->all();
        //ЭПС без комиссии
        $spentEps = Profile::find()
            ->select(['{{%catalog_orders}}.id as idc, {{%profiles}}.phone_mobile as Participant', '{{%ordered_cards}}.nominal as PointSum', '{{%ordered_cards}}.company_amount as MoneySum', '{{%ordered_cards}}.created_at as SpentDate', 'CONCAT("Сертификат-", {{%ordered_cards}}.type, "; номинал-", {{%ordered_cards}}.nominal, "; ",{{%ordered_cards}}.quantity, " шт.") as PoinsType'])
            ->innerJoin('{{%catalog_orders}}', '{{%catalog_orders}}.user_id={{%profiles}}.id')
            ->innerJoin('{{%ordered_cards}}', '{{%ordered_cards}}.catalog_order_id={{%catalog_orders}}.id')
            ->where(['{{%ordered_cards}}.status' => OrderedCard::STATUS_READY])
            ->asArray()
            ->all();
        $allSpent = array_merge($spentPay, $spentEps);
        $arrAllUsers = [];
        foreach ($allSpent as $user){
            $arrAllUsers[] = $user['Participant'];
        }

        $arrUsers = array_unique($arrAllUsers);

        $xml = new \DOMDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xmlObmenParticipantData = $xml->createElement('ObmenParticipantData');
        $xml->appendChild($xmlObmenParticipantData);

        foreach ($arrUsers as $item){
            $xmlParticipantSpentPoints = $xml->createElement('ParticipantSpentPoints');
            $xmlObmenParticipantData->appendChild($xmlParticipantSpentPoints);
            $xmlParticipant = $xml->createElement('Participant',  $item);
            $xmlParticipantSpentPoints->appendChild($xmlParticipant);
            $xmlSpentPoints = $xml->createElement('SpentPoints');
            $xmlParticipantSpentPoints->appendChild($xmlSpentPoints);
            $totalPontSum=0;
            $totalMoneySum=0;
            foreach ($allSpent as $spent){
                if ($item == $spent['Participant']){
                    $xmlSpentPoint = $xml->createElement('SpentPoint');
                    $xmlSpentPoints->appendChild($xmlSpentPoint);

                    $xmlSpentDate = $xml->createElement('SpentDate', $spent['SpentDate']);
                    $xmlSpentPoint->appendChild($xmlSpentDate);

                    $xmlPointSum = $xml->createElement('PointSum', $spent['PointSum']);
                    $xmlSpentPoint->appendChild($xmlPointSum);

                    $xmlMooneySum = $xml->createElement('MoneySum', $spent['MoneySum']/100);
                    $xmlSpentPoint->appendChild($xmlMooneySum);

                    $xmlMoneySumId = $xml->createElement('MoneySumId', $spent['PoinsType']=='phone' ?  'phone|'.$spent['idp'] : 'cert|'.$spent['idc']);
                    $xmlSpentPoint->appendChild($xmlMoneySumId);

                    $xmlPoinsType = $xml->createElement('PoinsType', $spent['PoinsType']);
                    $xmlSpentPoint->appendChild($xmlPoinsType);

                    $totalPontSum = $totalPontSum+$spent['PointSum'];
                    $totalMoneySum = $totalMoneySum+$spent['MoneySum'];
                }
            }

            $xmlSpentPointsTotal = $xml->createElement('SpentPointsTotal', $totalPontSum);
            $xmlParticipantSpentPoints->insertBefore($xmlSpentPointsTotal,$xmlSpentPoints);

            $xmlSpentMoneyTotal = $xml->createElement('SpentMoneyTotal', $totalMoneySum/100);
            $xmlParticipantSpentPoints->insertBefore($xmlSpentMoneyTotal,$xmlSpentPoints);
        }
        return $xml->saveXML();
    }


    /**
     * Бонусы наростающим итогом для выгрузки на ftp
     * @return \DOMDocument
     */
    public static function arrSpentBonusConsole(){
        //Платежи без комисии
        $spentPay = Profile::find()
            ->select('{{%payments}}.id as idp, {{%profiles}}.phone_mobile as Participant, {{%payments}}.amount as PointSum, {{%payments}}.company_money_amount as MoneySum, {{%payments}}.created_at as SpentDate, {{%payments}}.type as PoinsType')
            ->InnerJoin('{{%payments}}', '{{%profiles}}.id = {{%payments}}.recipient_id')
            ->where(['{{%payments}}.status' => Payment::STATUS_SUCCESS])
            ->asArray()
            ->all();
        //ЭПС без комиссии
        $spentEps = Profile::find()
            ->select(['{{%catalog_orders}}.id as idc, {{%profiles}}.phone_mobile as Participant', '{{%ordered_cards}}.nominal as PointSum', '{{%ordered_cards}}.company_amount as MoneySum', '{{%ordered_cards}}.created_at as SpentDate', 'CONCAT("Сертификат-", {{%ordered_cards}}.type, "; номинал-", {{%ordered_cards}}.nominal, "; ",{{%ordered_cards}}.quantity, " шт.") as PoinsType'])
            ->innerJoin('{{%catalog_orders}}', '{{%catalog_orders}}.user_id={{%profiles}}.id')
            ->innerJoin('{{%ordered_cards}}', '{{%ordered_cards}}.catalog_order_id={{%catalog_orders}}.id')
            ->where(['{{%ordered_cards}}.status' => OrderedCard::STATUS_READY])
            ->asArray()
            ->all();
        $allSpent = array_merge($spentPay, $spentEps);
        $arrAllUsers = [];
        foreach ($allSpent as $user){
            $arrAllUsers[] = $user['Participant'];
        }

        $arrUsers = array_unique($arrAllUsers);

        $xml = new \DOMDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xmlObmenParticipantData = $xml->createElement('ObmenParticipantData');
        $xml->appendChild($xmlObmenParticipantData);

        foreach ($arrUsers as $item){
            $xmlParticipantSpentPoints = $xml->createElement('ParticipantSpentPoints');
            $xmlObmenParticipantData->appendChild($xmlParticipantSpentPoints);
            $xmlParticipant = $xml->createElement('Participant',  $item);
            $xmlParticipantSpentPoints->appendChild($xmlParticipant);
            $xmlSpentPoints = $xml->createElement('SpentPoints');
            $xmlParticipantSpentPoints->appendChild($xmlSpentPoints);
            $totalPontSum=0;
            $totalMoneySum=0;
            foreach ($allSpent as $spent){
                if ($item == $spent['Participant']){
                    $xmlSpentPoint = $xml->createElement('SpentPoint');
                    $xmlSpentPoints->appendChild($xmlSpentPoint);

                    $xmlSpentDate = $xml->createElement('SpentDate', $spent['SpentDate']);
                    $xmlSpentPoint->appendChild($xmlSpentDate);

                    $xmlPointSum = $xml->createElement('PointSum', $spent['PointSum']);
                    $xmlSpentPoint->appendChild($xmlPointSum);

                    $xmlMooneySum = $xml->createElement('MoneySum', $spent['MoneySum']/100);
                    $xmlSpentPoint->appendChild($xmlMooneySum);

                    $xmlMoneySumId = $xml->createElement('MoneySumId', $spent['PoinsType']=='phone' ?  'phone|'.$spent['idp'] : 'cert|'.$spent['idc']);
                    $xmlSpentPoint->appendChild($xmlMoneySumId);

                    $xmlPoinsType = $xml->createElement('PoinsType', $spent['PoinsType']);
                    $xmlSpentPoint->appendChild($xmlPoinsType);

                    $totalPontSum = $totalPontSum+$spent['PointSum'];
                    $totalMoneySum = $totalMoneySum+$spent['MoneySum'];
                }
            }

            $xmlSpentPointsTotal = $xml->createElement('SpentPointsTotal', $totalPontSum);
            $xmlParticipantSpentPoints->insertBefore($xmlSpentPointsTotal,$xmlSpentPoints);

            $xmlSpentMoneyTotal = $xml->createElement('SpentMoneyTotal', $totalMoneySum/100);
            $xmlParticipantSpentPoints->insertBefore($xmlSpentMoneyTotal,$xmlSpentPoints);
        }
        return $xml;
    }

    /**
     * Вывод целей
     * @param $profileId
     * @param string $type
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getTargetBonus($profileId, $type = self::TYPE_TEMP_TARGET_BONUS){
        $profile = Profile::findOne(['id' => $profileId]);
        if($type == self::TYPE_TEMP_TARGET_BONUS){
            $query = TempBonuses::find()
                ->select('SUM(bonus_sum) as sum_bonus, target_id , target_name, period')
                ->where(['phone_mobile' => $profile->phone_mobile, 'bonus_type' => TempBonuses::TARGET_BONUS])
                ->groupBy('period, target_id')
                ->asArray()
                ->all();
        }else{
            $query = TempConfirmedBonuses::find()
                ->select('SUM(bonus_sum) as sum_bonus, target_id , target_name, period')
                ->where(['phone_mobile' => $profile->phone_mobile, 'bonus_type' => TempBonuses::TARGET_BONUS])
                ->groupBy('period, target_id')
                ->asArray()
                ->all();
        }

        return $query;
    }

    public static function getMonth(){
        return [
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь',
        ];
    }
}
