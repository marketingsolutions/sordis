<?php

/**
 * @var Profile $profile
 * @var Bonus $bonus
 * @var string $msg
 * @var \yii\web\View $this
 * @var \yii\mail\BaseMessage $message instance of newly created mail message
 */

use modules\profiles\common\models\Bonus;
use modules\profiles\common\models\Profile;
use yii\helpers\Html;
use yii\helpers\Url;

$message->setSubject('Уведомление о поступившем бонусе на Ваш баланс');
?>

<p>Уважаемый (ая) <?= $profile->full_name ?>!</p>
<p><?= $msg ?></p>