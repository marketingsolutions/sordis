<?php

use yii\db\Migration;

/**
 * Class m181025_065916_create_upload_bonus_table
 */
class m181025_065916_create_upload_bonus_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%upload_bonus_file}}', [
            'id' => $this->primaryKey(),
            'original_file_name' => $this->string(),
            'file_name' => $this->string(),
            'status_process' => $this->string(16),
            'created_at' => $this->dateTime(),
            'processed_at' => $this->dateTime(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%upload_bonus_file}}');
    }
}
