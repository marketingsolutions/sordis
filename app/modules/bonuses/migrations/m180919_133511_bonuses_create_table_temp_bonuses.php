<?php

use yii\db\Migration;

/**
 * Class m180919_133511_bonuses_create_table_temp_bonuses
 */
class m180919_133511_bonuses_create_table_temp_bonuses extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%temp_bonuses}}', [
            'id' => $this->primaryKey(),
            'bonus_type' => $this->string(25),
            'code_egais' => $this->string(25),
            'inn' => $this->string(25),
            'license_number' => $this->string(25),
            'period' => $this->date(),
            'bonus_sum' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),

        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%temp_bonuses}}');
    }
}
