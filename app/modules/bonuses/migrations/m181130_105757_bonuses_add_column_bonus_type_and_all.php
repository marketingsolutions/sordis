<?php

use yii\db\Migration;

/**
 * Class m181130_105757_bonuses_add_column_bonus_type_and_all
 */
class m181130_105757_bonuses_add_column_bonus_type_and_all extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%bonuses}}', 'bonus_type', $this->string(25));
        $this->addColumn('{{%bonuses}}', 'action_id', $this->integer());
        $this->addColumn('{{%bonuses}}', 'target_id', $this->string());
        $this->addColumn('{{%bonuses}}', 'target_name', $this->string());
        $this->addColumn('{{%bonuses}}', 'poll_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%bonuses}}', 'bonus_type');
        $this->dropColumn('{{%bonuses}}', 'action_id');
        $this->dropColumn('{{%bonuses}}', 'target_id');
        $this->dropColumn('{{%bonuses}}', 'target_name');
        $this->dropColumn('{{%bonuses}}', 'poll_id');
    }
}
