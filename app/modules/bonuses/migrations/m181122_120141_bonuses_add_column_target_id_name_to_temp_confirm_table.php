<?php

use yii\db\Migration;

/**
 * Class m181122_120141_bonuses_add_column_target_id_name_to_temp_confirm_table
 */
class m181122_120141_bonuses_add_column_target_id_name_to_temp_confirm_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%temp_confirmed_bonuses}}', 'action_id', $this->integer());
        $this->addColumn('{{%temp_confirmed_bonuses}}', 'target_id', $this->string());
        $this->addColumn('{{%temp_confirmed_bonuses}}', 'target_name', $this->string());
        $this->addColumn('{{%temp_confirmed_bonuses}}', 'poll_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%temp_confirmed_bonuses}}', 'action_id');
       $this->dropColumn('{{%temp_confirmed_bonuses}}', 'target_id');
       $this->dropColumn('{{%temp_confirmed_bonuses}}', 'target_name');
       $this->dropColumn('{{%temp_confirmed_bonuses}}', 'poll_id');
    }
}
