<?php

use yii\db\Migration;

/**
 * Class m180920_061756_bonuses_add_column_payed_at_to_temp_bonuses_table
 */
class m180920_061756_bonuses_add_column_payed_at_to_temp_bonuses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%temp_bonuses}}', 'paid_at', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%temp_bonuses}}', 'paid_at');
    }
}
