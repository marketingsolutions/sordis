<?php

use yii\db\Migration;

class m170125_060652_bonuses_fk_bonuses_table extends Migration
{
    public function up()
    {
		$this->addForeignKey('fkprofileid',
			'{{%bonuses}}', 'profile_id',
			'{{%profiles}}', 'id',
			'CASCADE', 'CASCADE'
		);
    }

    public function down()
    {

    }
}
