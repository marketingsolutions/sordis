<?php

use yii\db\Migration;

/**
 * Class m181109_110601_bonuses_create_table_temp_confirmed_bonuses
 */
class m181109_110601_bonuses_create_table_temp_confirmed_bonuses extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%temp_confirmed_bonuses}}', [
            'id' => $this->primaryKey(),
            'bonus_type' => $this->string(25),
            'code_egais' => $this->string(25),
            'inn' => $this->string(25),
            'license_number' => $this->string(25),
            'period' => $this->date(),
            'bonus_sum' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'paid_at' => $this->dateTime(),
            'phone_mobile' => $this->string(25)

        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%temp_confirmed_bonuses}}');
    }
}
