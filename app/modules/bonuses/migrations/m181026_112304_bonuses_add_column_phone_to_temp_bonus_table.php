<?php

use yii\db\Migration;

/**
 * Class m181026_112304_bonuses_add_column_phone_to_temp_bonus_table
 */
class m181026_112304_bonuses_add_column_phone_to_temp_bonus_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%temp_bonuses}}' , 'phone_mobile' , $this->string(25));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%temp_bonuses}}' , 'phone_mobile');
    }
}
