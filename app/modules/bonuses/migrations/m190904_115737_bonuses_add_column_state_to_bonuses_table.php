<?php

use yii\db\Migration;

/**
 * Class m190904_115737_bonuses_add_column_state_to_bonuses_table
 */
class m190904_115737_bonuses_add_column_state_to_bonuses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%temp_bonuses}}', 'state', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%temp_bonuses}}', 'state');
    }
}
