<?php

use yii\db\Migration;

/**
 * Class m190128_101914_bonuses_add_column_confirm_points_id_to_bonus_table
 */
class m190128_101914_bonuses_add_column_confirm_points_id_to_bonus_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%temp_confirmed_bonuses}}', 'confirm_points_id', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%temp_confirmed_bonuses}}', 'confirm_points_id');
    }
}
