<?php

use yii\db\Migration;

/**
 * Class m190605_123009_bonuses_add_column_state_to_confirmed_bonus_table
 */
class m190605_123009_bonuses_add_column_state_to_confirmed_bonus_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%temp_confirmed_bonuses}}', 'state' , $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%temp_confirmed_bonuses}}', 'state');
    }
}
