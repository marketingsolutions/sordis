<?php

use yii\db\Migration;

/**
 * Class m181112_075601_bonuses_add_column_target_id_to_temp_bonus_table
 */
class m181112_075601_bonuses_add_column_target_id_to_temp_bonus_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%temp_bonuses}}', 'target_id', $this->string());
        $this->addColumn('{{%temp_bonuses}}', 'target_name', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%temp_bonuses}}', 'target_id');
        $this->dropColumn('{{%temp_bonuses}}', 'target_name');
    }
}
