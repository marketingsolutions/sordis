<?php

use yii\db\Migration;

/**
 * Class m181130_134430_bonuses_add_column_code_egais_inn_license_number
 */
class m181130_134430_bonuses_add_column_code_egais_inn_license_number extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%bonuses}}', 'code_egais', $this->string(25));
        $this->addColumn('{{%bonuses}}', 'inn', $this->string(25));
        $this->addColumn('{{%bonuses}}', 'license_number', $this->string(25));
        $this->addColumn('{{%bonuses}}', 'period', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%bonuses}}', 'code_egais');
        $this->dropColumn('{{%bonuses}}', 'inn');
        $this->dropColumn('{{%bonuses}}', 'license_number');
        $this->dropColumn('{{%bonuses}}', 'period');
    }
}
