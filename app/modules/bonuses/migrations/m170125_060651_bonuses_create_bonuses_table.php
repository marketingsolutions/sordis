<?php

use yii\db\Migration;

class m170125_060651_bonuses_create_bonuses_table extends Migration
{
    public function up()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
		}

		$this->createTable('{{%bonuses}}', [
			'id' => $this->primaryKey(),
			'profile_id' => $this->integer(),
			'bonuses' => $this->integer(),
			'month' => $this->integer(),
			'year' => $this->integer(),
			'paid' => $this->boolean()->defaultValue(false),
			'paid_at' => $this->dateTime(),
			'duple' => $this->boolean()->defaultValue(false),
            'sms_sent' => $this->boolean()->defaultValue(false),
			'created_at' => $this->dateTime(),
			'updated_at' => $this->dateTime(),
		], $tableOptions);

        $this->createIndex('fkprofileid', '{{%bonuses}}', 'profile_id');
    }

    public function down()
    {
        $this->dropTable('{{%bonuses}}');
    }
}
