<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use yz\icons\Icons;
use ms\loyalty\finances\common\components\CompanyAccount;


/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\bonuses\backend\models\BonusesSearch $searchModel
 * @var array $columns
 */

$this->title = 'Информация о балансе проекта';
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>


<div class="row">

    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?= CompanyAccount::getPurse()->balance / 100 ?></h3>
                <p>Текущий баланс проекта</p>
            </div>
            <div class="icon"><i class="fa fa-ruble-sign"></i></div>
        </div>
    </div>


    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?= CompanyAccount::calculateTransactionsSum(\marketingsolutions\finance\models\Transaction::INCOMING) / 100 ?></h3>
                <p>Сумма входящих транзакций проекта</p>
            </div>
            <div class="icon"><i class="fa fa-ruble-sign"></i></div>

        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-red">
            <div class="inner">
                <h3><?= CompanyAccount::calculateTransactionsSum(\marketingsolutions\finance\models\Transaction::OUTBOUND) / 100 ?></h3>
                <p>Сумма исходящих транзакций проекта</p>
            </div>
            <div class="icon"><i class="fa fa-ruble-sign"></i></div>

        </div>
    </div>

</div>


