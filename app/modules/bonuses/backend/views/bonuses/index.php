<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use yz\icons\Icons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\bonuses\backend\models\BonusesSearch $searchModel
 * @var array $columns
 */

$this->title = modules\bonuses\common\models\Bonus::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'bonus-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [[/*'file', 'import',*/ 'export', 'return']],
            'gridId' => 'bonus-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\bonuses\common\models\Bonus',
            'buttons' => [
            ]
        ]) ?>
    </div>

    <?= GridView::widget([
        'id' => 'bonus-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '{update}',
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
