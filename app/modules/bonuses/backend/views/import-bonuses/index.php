<?php
/**
 * @var \yz\admin\widgets\ActiveForm $form
 * @var \modules\bonuses\backend\forms\BonusesImportForm $model
 */

use yii\helpers\Url;
use yz\icons\Icons;

?>

<?= $form->field($model, 'year')->input('number') ?>
<?= $form->field($model, 'month')->input('number') ?>

<div class="text-center">
    <a href="<?= Url::to(['/bonuses/bonuses/file']) ?>" class="btn btn-info">
        <?= Icons::o('upload') ?>
        Скачать файл для загрузки бонусов
    </a>
</div>
