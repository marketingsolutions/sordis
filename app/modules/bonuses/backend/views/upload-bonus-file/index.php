<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use \modules\bonuses\common\models\UploadBonusFile;
use yz\icons\Icons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\bonuses\backend\models\UploadBonusFileSearch $searchModel
 * @var array $columns
 */

$this->title = modules\bonuses\common\models\UploadBonusFile::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'upload-bonus-file-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [/*['search'],*/ ['export', 'create', 'delete', 'return']],
            'gridId' => 'upload-bonus-file-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\bonuses\common\models\UploadBonusFile',
        ]) ?>
    </div>

    <?= GridView::widget([
        'id' => 'upload-bonus-file-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '{rollback} {delete}',
                'buttons' => [
                    'rollback' => function ($url, UploadBonusFile $model) {
                        $url = '/bonuses/upload-bonus-file/rollback?id='.$model->id;
                        if ($model->status_process == UploadBonusFile::STATUS_NEW || $model->status_process == UploadBonusFile::STATUS_COMPLETE) {
                            return '';
                        }
                        return Html::a(Icons::i('refresh'), $url, [
                            'title' => 'Обработать заново',
                            'data-confirm' => 'Вы действительно хотите обработать данный файл заново?',
                            'data-method' => 'post',
                            'class' => 'btn btn-warning btn-sm',
                            //'data-pjax' => '0',
                        ]);
                    },
                ],
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
