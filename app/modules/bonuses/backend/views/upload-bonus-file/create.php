<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\widgets\ActionButtons;

/**
 * @var yii\web\View $this
 * @var modules\bonuses\common\models\UploadBonusFile $model
 */
$this->title = 'Загрузка баллов из XML файлов';
$this->params['breadcrumbs'][] = ['label' => modules\bonuses\common\models\UploadBonusFile::modelTitlePlural(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<div class="upload-bonus-file-create">

    <div class="text-right">
        <?php  Box::begin() ?>
        <?=  ActionButtons::widget([
            'order' => [['index', 'create', 'return']],
            'addReturnUrl' => false,
        ]) ?>
        <?php  Box::end() ?>
    </div>

    <?php echo $this->render('_form', [
        'model' => $model,
        'uploadForm' => $uploadForm,
    ]); ?>

</div>
