<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\widgets\ActionButtons;

/**
 * @var yii\web\View $this
 * @var modules\bonuses\common\models\UploadBonusFile $model
 */
$this->title = \Yii::t('admin/t', 'Update {item}', ['item' => modules\bonuses\common\models\UploadBonusFile::modelTitle()]);
$this->params['breadcrumbs'][] = ['label' => modules\bonuses\common\models\UploadBonusFile::modelTitlePlural(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<div class="upload-bonus-file-update">

    <div class="text-right">
        <?php  Box::begin() ?>
        <?=  ActionButtons::widget([
            'order' => [['index', 'return']],
            'addReturnUrl' => false,
        ]) ?>
        <?php  Box::end() ?>
    </div>

    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
