<?php

use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var modules\bonuses\common\models\TempBonuses $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php  $box = FormBox::begin(['cssClass' => 'temp-bonuses-form box-primary', 'title' => '']) ?>
    <?php $form = ActiveForm::begin(); ?>

    <?php $box->beginBody() ?>
    <?= $form->field($model, 'bonus_type')->dropDownList(\modules\bonuses\common\models\TempBonuses::getTypes(), ['prompt' => 'Выберите тип баллов...']) ?>

    <?= $form->field($model, 'phone_mobile')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '+7 999 999-99-99',
    ]) ?>

    <?= $form->field($model, 'code_egais')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'inn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'license_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'period')->textInput() ?>

    <?= $form->field($model, 'bonus_sum')->textInput() ?>

    <?= $form->field($model, 'created_at')->hiddenInput(['value' => date("Y-m-d H:i:s")])->label(false) ?>



    <?php $box->endBody() ?>

    <?php $box->actions([
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
    ]) ?>
    <?php ActiveForm::end(); ?>

<?php  FormBox::end() ?>
