<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;



/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\bonuses\backend\models\TempBonusesSearch $searchModel
 * @var array $columns
 */

$this->title = modules\bonuses\common\models\TempBonuses::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin();?>
<h4>Итоговая сумма начисленных баллов: <?=\modules\bonuses\common\models\TempBonuses::getBonusSum();?></h4>
<?php Box::end() ?>
<?php $box = Box::begin(['cssClass' => 'temp-bonuses-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [/*['search'],*/ ['export', /*'create', 'delete',*/ 'return']],
            'gridId' => 'temp-bonuses-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\bonuses\common\models\TempBonuses',
        ]) ?>
    </div>

    <?= GridView::widget([
        'id' => 'temp-bonuses-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '',
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
