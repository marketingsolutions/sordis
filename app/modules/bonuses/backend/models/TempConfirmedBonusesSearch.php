<?php

namespace modules\bonuses\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\bonuses\common\models\TempConfirmedBonuses;

/**
 * TempConfirmedBonusesSearch represents the model behind the search form about `modules\bonuses\common\models\TempConfirmedBonuses`.
 */
class TempConfirmedBonusesSearch extends TempConfirmedBonuses implements SearchModelInterface

{

    /**
     * @var
     */
    public $profileName;

    /**
     * @var
     */
    public $profileRole;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'bonus_sum'], 'integer'],
            [['bonus_type', 'state','code_egais', 'inn', 'license_number', 'period', 'created_at', 'updated_at', 'paid_at', 'phone_mobile', 'target_id', 'target_name', 'profileName', 'profileRole',  'confirm_points_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        return TempConfirmedBonuses::find()
            ->joinWith('profile');
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'profileName' => [
                'asc' => ['{{%profiles}}.full_name' => SORT_ASC],
                'desc' => ['{{%profiles}}.full_name' => SORT_DESC],
            ],
            'profileRole' => [
                'asc' => ['{{%profiles}}.role' => SORT_ASC],
                'desc' => ['{{%profiles}}.role' => SORT_DESC],
            ],
        ]);
        $dataProvider->setSort($sort);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'id' => $this->id,
            'bonus_sum' => $this->bonus_sum,
        ]);

        $query->andFilterWhere(['like', 'bonus_type', $this->bonus_type])
            ->andFilterWhere(['like', 'code_egais', $this->code_egais])
            ->andFilterWhere(['like', 'confirm_points_id', $this->confirm_points_id])
            ->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', '{{%profiles}}.role', $this->profileRole])
            ->andFilterWhere(['like', '{{%profiles}}.full_name', $this->profileName])
            ->andFilterWhere(['like', 'license_number', $this->license_number])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['period' => $this->period])
            ->andFilterWhere(['like', 'target_id', $this->target_id])
            ->andFilterWhere(['like', 'target_name', $this->target_name])
//            ->andFilterWhere(['between', 'created_at', Yii::$app->formatter->asDate($this->created_at, 'YYYY-MM-d 00:00:00'), Yii::$app->formatter->asDate($this->created_at, 'YYYY-MM-d 23:59:59')])
//            ->andFilterWhere(['between', 'updated_at', Yii::$app->formatter->asDate($this->updated_at, 'YYYY-MM-d 00:00:00'), Yii::$app->formatter->asDate($this->updated_at, 'YYYY-MM-d 23:59:59')])
            //->andFilterWhere(['between', 'paid_at', Yii::$app->formatter->asDate($this->paid_at, 'YYYY-MM-d 00:00:00'), Yii::$app->formatter->asDate($this->paid_at, 'YYYY-MM-d 23:59:59')])
            ->andFilterWhere(['like', '{{%profiles}}.phone_mobile', $this->phone_mobile]);

    }
}
