<?php

namespace modules\bonuses\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\bonuses\common\models\Bonus;

/**
 * BonusesSearch represents the model behind the search form about `modules\bonuses\common\models\Bonus`.
 */
class BonusesSearch extends Bonus implements SearchModelInterface
{
    /**
     * @var
     */
    public $profileFullname;

    /**
     * @var
     */
    public $profileEmail;

    /**
     * @var
     */
    public $phoneMobile;

    public $profileRole;

    /**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'profile_id', 'bonuses', 'month', 'year', 'sms_sent'], 'integer'],
			[['paid_at', 'created_at', 'updated_at', 'paid', 'duple', 'bonus_type', 'period', 'profileFullname', 'profileEmail', 'phoneMobile', 'target_id', 'target_name', 'profileRole'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = $this->prepareQuery();
		$this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
			'query' => $query,
		]));

		$dataProvider = $this->prepareDataProvider($query);
		$this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
			'query' => $query,
			'dataProvider' => $dataProvider,
		]));

		$this->load($params);

		$this->prepareFilters($query);
		$this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
			'query' => $query,
			'dataProvider' => $dataProvider,
		]));

		return $dataProvider;
	}

	/**
	 * @return ActiveQuery
	 */
	protected function getQuery()
	{
		return Bonus::find()
            ->joinWith('profile');
	}

	/**
	 * @return ActiveQuery
	 */
	protected function prepareQuery()
	{
		$query = $this->getQuery();
		return $query;
	}

	/**
	 * @param ActiveQuery $query
	 * @return ActiveDataProvider
	 */
	protected function prepareDataProvider($query)
	{
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'profileFullname' => [
                'asc' => ['{{%profiles}}.full_name' => SORT_ASC],
                'desc' => ['{{%profiles}}.full_name' => SORT_DESC],
            ],
            'phoneMobile' => [
                'asc' => ['{{%profiles}}.phone_mobile' => SORT_ASC],
                'desc' => ['{{%profiles}}.phone_mobile' => SORT_DESC],
            ],
            'profileEmail' => [
                'asc' => ['{{%profiles}}.email' => SORT_ASC],
                'desc' => ['{{%profiles}}.email' => SORT_DESC],
            ],
        ]);
        $dataProvider->setSort($sort);
		return $dataProvider;
	}

	/**
	 * @param ActiveQuery $query
	 */
	protected function prepareFilters($query)
	{
		$query->andFilterWhere([
			'id' => $this->id,
			'profile_id' => $this->profile_id,
			'bonuses' => $this->bonuses,
			'month' => $this->month,
			'year' => $this->year,
			'paid' => $this->paid,
			'duple' => $this->duple,
			'sms_sent' => $this->sms_sent,
			'bonus_type' => $this->bonus_type,
			'period' => $this->period,
			'{{%profiles}}.role' => $this->profileRole,
		]);
        $query->andFilterWhere(['like', '{{%profiles}}.phone_mobile', $this->phoneMobile]);
        $query->andFilterWhere(['like', '{{%profiles}}.full_name', $this->profileFullname]);
        $query->andFilterWhere(['like', '{{%profiles}}.email', $this->profileEmail]);
        $query->andFilterWhere(['like', 'target_id', $this->target_id]);
        $query->andFilterWhere(['like', 'target_name', $this->target_name]);
	}
}
