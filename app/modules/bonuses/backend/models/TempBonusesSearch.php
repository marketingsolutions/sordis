<?php

namespace modules\bonuses\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\bonuses\common\models\TempBonuses;

/**
 * TempBonusesSearch represents the model behind the search form about `modules\bonuses\common\models\TempBonuses`.
 */
class TempBonusesSearch extends TempBonuses implements SearchModelInterface

{

    public $profileName;

    public $profileRole;

    public $pollTitle;

    public $socialName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'bonus_sum'], 'integer'],
            [['bonus_type', 'code_egais', 'inn', 'license_number', 'period', 'created_at', 'updated_at', 'paid_at', 'phone_mobile', 'target_id', 'target_name', 'profileName', 'profileRole', 'pollTitle', 'socialName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        return TempBonuses::find()
            ->joinWith('profile')
            ->joinWith('social')
            ->joinWith('poll');
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'profileName' => [
                'asc' => ['{{%profiles}}.full_name' => SORT_ASC],
                'desc' => ['{{%profiles}}.full_name' => SORT_DESC],
            ],
            'profileRole' => [
                'asc' => ['{{%profiles}}.role' => SORT_ASC],
                'desc' => ['{{%profiles}}.role' => SORT_DESC],
            ],
            'pollTitle' => [
            'asc' => ['{{%ms_polls_main}}.title' => SORT_ASC],
            'desc' => ['{{%ms_polls_main}}.title' => SORT_DESC],
            ],
            'socialName' => [
                'asc' => ['{{%social_action_rules}}.name' => SORT_ASC],
                'desc' => ['{{%social_action_rules}}.name' => SORT_DESC],
            ],
        ]);
        $dataProvider->setSort($sort);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'id' => $this->id,
            'bonus_sum' => $this->bonus_sum,
        ]);

        $query->andFilterWhere(['like', 'bonus_type', $this->bonus_type])
            ->andFilterWhere(['like', 'code_egais', $this->code_egais])
            ->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'target_id', $this->target_id])
            ->andFilterWhere(['like', 'target_name', $this->target_name])
            ->andFilterWhere(['like', '{{%profiles}}.phone_mobile', $this->phone_mobile])
            ->andFilterWhere(['like', '{{%profiles}}.full_name', $this->profileName])
            ->andFilterWhere(['like', '{{%profiles}}.role', $this->profileRole])
            ->andFilterWhere(['like', '{{%ms_polls_main}}.title', $this->pollTitle])
            ->andFilterWhere(['like', '{{%social_action_rules}}.name', $this->socialName])
            ->andFilterWhere(['like', 'license_number', $this->license_number]);
        if($this->period){
            $query->andFilterWhere(['between', 'period', Yii::$app->formatter->asDate($this->period, 'YYYY-MM-d'), Yii::$app->formatter->asDate($this->period, 'YYYY-MM-d')]);
        }
        if($this->created_at){
            $query->andFilterWhere(['between', 'created_at', Yii::$app->formatter->asDate($this->created_at, 'YYYY-MM-d 00:00:00'), Yii::$app->formatter->asDate($this->created_at, 'YYYY-MM-d 23:59:59')]);
        }
        if($this->paid_at){
            $query->andFilterWhere(['between', 'paid_at', Yii::$app->formatter->asDate($this->paid_at, 'YYYY-MM-d 00:00:00'), Yii::$app->formatter->asDate($this->paid_at, 'YYYY-MM-d 23:59:59')]);
        }
        if($this->updated_at){
            $query->andFilterWhere(['between', 'updated_at', Yii::$app->formatter->asDate($this->updated_at, 'YYYY-MM-d 00:00:00'), Yii::$app->formatter->asDate($this->updated_at, 'YYYY-MM-d 23:59:59')]);
        }


    }
}
