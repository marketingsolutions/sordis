<?php

namespace modules\bonuses\backend\controllers;

use modules\profiles\common\models\Profile;
use Yii;
use modules\bonuses\common\models\TempConfirmedBonuses;
use modules\bonuses\backend\models\TempConfirmedBonusesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * TempConfirmedBonusesController implements the CRUD actions for TempConfirmedBonuses model.
 */
class TempConfirmedBonusesController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var TempConfirmedBonusesSearch $searchModel */
                    return Yii::createObject(TempConfirmedBonusesSearch::className());
                },
                'dataProvider' => function($params, TempConfirmedBonusesSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all TempConfirmedBonuses models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var TempConfirmedBonusesSearch $searchModel */
        $searchModel = Yii::createObject(TempConfirmedBonusesSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(TempConfirmedBonusesSearch $searchModel)
    {
        return [
			'id',
            'confirm_points_id',
			[
                'attribute' => 'bonus_type',
                'filter' => TempConfirmedBonuses::getTypes(),
                'format' => 'html',
                'contentOptions' => ['style' => 'min-width:160px; font-size:17px;'],
                'value' => function($data){
                    if($data->bonus_type == TempConfirmedBonuses::BASIC_BONUS){
                        return "<span class='label label-info'>".TempConfirmedBonuses::getTypes()[$data->bonus_type]."</span>";
                    }elseif($data->bonus_type == TempConfirmedBonuses::TARGET_BONUS){
                        return "<span class='label label-success'>".TempConfirmedBonuses::getTypes()[$data->bonus_type]."</span>";
                    }elseif($data->bonus_type == TempConfirmedBonuses::SOCIAL_BONUS){
                        return "<span style='margin: 5px; font-size: 13px; background-color: #4c3000;' class='label label-warning'>".TempConfirmedBonuses::getTypes()[$data->bonus_type]."</span>";
                    }

                }
            ],
            'phone_mobile',
            [
                'attribute' => 'profileName',
                'label' => 'ФИО участника',
            ],
            [
                'attribute' => 'profileRole',
                'contentOptions' => ['style' => 'min-width:170px;'],
                'label' => 'Роль участника',
                'filter' => Profile::getDashboardRealRole(),
                'value' => function($data){
                    return isset(Profile::getDashboardRealRole()[$data->profileRole]) ? Profile::getDashboardRealRole()[$data->profileRole] : null;
                }
            ],

			'code_egais',
			'inn',
			'license_number',
            [
                'attribute' => 'period',
                'value' => function($data) {
                              return TempConfirmedBonuses::periodOptions()[$data->period];
                },
                'filter' => TempConfirmedBonuses::periodOptions(),
            ],
            'bonus_sum',
            //'state',
            'target_id',
            'target_name',
			'created_at:datetime',
			'updated_at:datetime',
			'paid_at:datetime',
        ];
    }

    /**
     * Creates a new TempConfirmedBonuses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TempConfirmedBonuses;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TempConfirmedBonuses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
        ]);
	}


    /**
     * Deletes an existing TempConfirmedBonuses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the TempConfirmedBonuses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TempConfirmedBonuses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TempConfirmedBonuses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
