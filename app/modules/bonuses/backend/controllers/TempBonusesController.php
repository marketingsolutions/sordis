<?php

namespace modules\bonuses\backend\controllers;

use modules\profiles\common\models\Profile;
use Yii;
use modules\bonuses\common\models\TempBonuses;
use modules\bonuses\backend\models\TempBonusesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * TempBonusesController implements the CRUD actions for TempBonuses model.
 */
class TempBonusesController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var TempBonusesSearch $searchModel */
                    return Yii::createObject(TempBonusesSearch::className());
                },
                'dataProvider' => function($params, TempBonusesSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all TempBonuses models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var TempBonusesSearch $searchModel */
        $searchModel = Yii::createObject(TempBonusesSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(TempBonusesSearch $searchModel)
    {
        return [
			'id',
            [
                'attribute' => 'bonus_type',
                'filter' => TempBonuses::getTypes(),
                'format' => 'html',
                'contentOptions' => ['style' => 'min-width:160px; font-size:17px;'],
                'value' => function($data){
                    if($data->bonus_type == TempBonuses::BASIC_BONUS){
                        return "<span class='label label-info'>".TempBonuses::getTypes()[$data->bonus_type]."</span>";
                    }elseif($data->bonus_type == TempBonuses::TARGET_BONUS){
                        return "<span class='label label-success'>".TempBonuses::getTypes()[$data->bonus_type]."</span>";
                    }elseif($data->bonus_type == TempBonuses::SOCIAL_BONUS){
                        return "<span style='margin: 5px; font-size: 13px; background-color: #4c3000;' class='label label-warning'>".TempBonuses::getTypes()[$data->bonus_type]."</span>";
                    }

                }
            ],
            'phone_mobile',
            [
                'attribute' => 'profileName',
                'label' => 'ФИО участника'
            ],
            [
                'attribute' => 'profileRole',
                'label' => 'Роль участника',
                'filter' => Profile::getDashboardRealRole(),
                'contentOptions' => ['style' => 'min-width:170px;'],
                'value' => function($data){
                    return isset(Profile::getDashboardRealRole()[$data->profileRole]) ? Profile::getDashboardRealRole()[$data->profileRole] : null;
                }
            ],

			'code_egais',
			'inn',
			'license_number',
            [
                'attribute' => 'period',
                'value' => function($data){
                    return TempBonuses::periodOptions()[$data->period];
                },
                'filter' => TempBonuses::periodOptions(),
            ],
			'bonus_sum',
            'target_id',
            'target_name',
            [
                'attribute' =>'pollTitle',
                'label' => 'Название опроса',
            ],
            [
                'attribute' => 'socialName',
                'label' => 'Название призовой акции',
            ],
            'created_at:datetime',
            'updated_at:datetime',
            'paid_at:datetime',
        ];
    }

    /**
     * Creates a new TempBonuses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TempBonuses;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TempBonuses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
        ]);
	}


    /**
     * Deletes an existing TempBonuses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the TempBonuses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TempBonuses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TempBonuses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
