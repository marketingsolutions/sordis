<?php

namespace modules\bonuses\backend\controllers;

use modules\bonuses\backend\forms\UploadBonusForm;
use Yii;
use modules\bonuses\common\models\UploadBonusFile;
use modules\bonuses\backend\models\UploadBonusFileSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;
use yii\web\UploadedFile;

/**
 * UploadBonusFileController implements the CRUD actions for UploadBonusFile model.
 */
class UploadBonusFileController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var UploadBonusFileSearch $searchModel */
                    return Yii::createObject(UploadBonusFileSearch::className());
                },
                'dataProvider' => function($params, UploadBonusFileSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all UploadBonusFile models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var UploadBonusFileSearch $searchModel */
        $searchModel = Yii::createObject(UploadBonusFileSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(UploadBonusFileSearch $searchModel)
    {
        return [
			'id',
			'original_file_name',
			'file_name',
            [
                'attribute' => 'status_process',
                'filter' => UploadBonusFile::getStatus(),
                'format'=>'html',
                'value' => function($data){
                    if($data->status_process == UploadBonusFile::STATUS_NEW) {
                        return "<span  style='font-size: 12px;' class='label label-info'>".UploadBonusFile::getStatus()[$data->status_process]."</span>";
                    }elseif($data->status_process == UploadBonusFile::STATUS_PROCESS){
                        return  "<span style='font-size: 12px;' class='label label-warning'>".UploadBonusFile::getStatus()[$data->status_process]."</span>";
                    }elseif($data->status_process == UploadBonusFile::STATUS_COMPLETE){
                        return  "<span style='font-size: 12px;' class='label label-success'>".UploadBonusFile::getStatus()[$data->status_process]."</span>";
                    }elseif ($data->status_process == UploadBonusFile::STATUS_ERROR){
                        return  "<span style='font-size: 12px;' class='label label-danger'>".UploadBonusFile::getStatus()[$data->status_process]."</span>";
                    }
                }

            ],
			'created_at:datetime',
			// 'processed_at:datetime',
        ];
    }

    /**
     * Creates a new UploadBonusFile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UploadBonusFile;
        $uploadForm = new UploadBonusForm();
        if (Yii::$app->request->isPost) {
            $uploadForm->xmlFile = UploadedFile::getInstance($uploadForm, 'xmlFile');
            if ($uploadForm->upload()) {
                \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Файл загружен и передан в обработку.'));
                $this->redirect(['index']);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'uploadForm' => $uploadForm,
        ]);
    }

    public function actionRollback($id){
        if(!$id){
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_ERROR, \Yii::t('admin/t', 'Отсутствует ID для обработки данной операции'));
            return false;
        }
        $model = UploadBonusFile::findOne($id);
        $model->status_process = UploadBonusFile::STATUS_NEW;
        $model->update(false);
        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Файл возвращен на обработку'));
        $this->redirect(['index']);
    }

    /**
     * Updates an existing UploadBonusFile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
        ]);
	}


    /**
     * Deletes an existing UploadBonusFile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the UploadBonusFile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UploadBonusFile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UploadBonusFile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
