<?php

namespace modules\bonuses\backend\controllers;

use modules\bonuses\common\models\TempConfirmedBonuses;
use modules\profiles\common\models\Action;
use modules\profiles\common\models\Profile;
use Yii;
use modules\bonuses\common\models\Bonus;
use modules\bonuses\backend\models\BonusesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;
use ms\loyalty\reports\support\WidgetConfig;
use ms\loyalty\finances\common\components\CompanyAccount;
use marketingsolutions\finance\models\Transaction;
use yz\icons\Icons;

/**
 * BonusesController implements the CRUD actions for Bonus model.
 */
class BonusesController extends Controller implements AccessControlInterface
{
	use CrudTrait, CheckAccessTrait;

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'accessControl' => $this->accessControlBehavior(),
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		]);
	}

	public function actions()
	{
		return array_merge(parent::actions(), [
			'export' => [
				'class' => ExportAction::className(),
				'searchModel' => function ($params) {
					/** @var BonusesSearch $searchModel */
					return Yii::createObject(BonusesSearch::className());
				},
				'dataProvider' => function ($params, BonusesSearch $searchModel) {
					$dataProvider = $searchModel->search($params);
					return $dataProvider;
				},
			]
		]);
	}

	/**
	 * Lists all Bonus models.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
		/** @var BonusesSearch $searchModel */
		$searchModel = Yii::createObject(BonusesSearch::className());
		$dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
		$dataProvider->setSort([
			'defaultOrder' => ['id' => SORT_DESC],
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
			'columns' => $this->getGridColumns($searchModel),
		]);
	}

    /**
     * Баланс проекта
     * @return string
     */
	public function actionProjectInfo()
    {
        $companyBalance = CompanyAccount::getPurse()->balance / 100;
        $companyIncomingSum = CompanyAccount::calculateTransactionsSum(Transaction::INCOMING) / 100;
        $companyOutboundSum = CompanyAccount::calculateTransactionsSum(Transaction::INCOMING) / 100;


        return $this->render('project-info', [
            'companyBalance' => $companyBalance,
            'companyIncomingSum' => $companyIncomingSum,
            'companyOutboundSum' => $companyOutboundSum,
        ]);
    }

	public function getGridColumns(BonusesSearch $searchModel)
	{
		return [
			'id',
			[
			    'attribute' => 'profileFullname',
                'label' => 'ФИО'
            ],
			[
			    'attribute' => 'phoneMobile',
                'label' => 'Телефон'
            ],
			[
			    'attribute' => 'profileEmail',
                'label' => 'E-mail'
            ],
            [
                'attribute' =>  'profileRole',
                'label' => 'Роль участника',
                'value' => function($model){
		            return Profile::getDashboardRealRole()[$model->profileRole];
                },
                'filter' => Profile::getDashboardRealRole(),

            ],

			'bonuses',
            [
                'attribute' => 'bonus_type',
                'label' => 'Тип бонусов',
                'filter' => TempConfirmedBonuses::getTypes(),
                'format' => 'html',
                'contentOptions' => ['style' => 'min-width:160px; font-size:17px;'],
                'value' => function($data){
                    if($data->bonus_type == TempConfirmedBonuses::BASIC_BONUS){
                        return "<span class='label label-info'>".TempConfirmedBonuses::getTypes()[$data->bonus_type]."</span>";
                    }elseif($data->bonus_type == TempConfirmedBonuses::TARGET_BONUS){
                        return "<span class='label label-success'>".TempConfirmedBonuses::getTypes()[$data->bonus_type]."</span>";
                    }elseif($data->bonus_type == TempConfirmedBonuses::SOCIAL_BONUS){
                        return "<span style='margin: 5px; font-size: 13px; background-color: #4c3000;' class='label label-warning'>".TempConfirmedBonuses::getTypes()[$data->bonus_type]."</span>";
                    }

                }
            ],
            [
                'attribute' => 'period',
                'label' => 'Отчетный период',
                'value' => function($data) {
                    return Bonus::periodOptions()[$data->period];
                },
                'filter' => Bonus::periodOptions(),
            ],
            'target_id',
            'target_name',
			'paid:boolean',
			'paid_at:date',
			'sms_sent:boolean',
		];
	}

	/**
	 * Creates a new Bonus model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Bonus;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing Bonus model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * Deletes an existing Bonus model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete(array $id)
	{
		$message = is_array($id) ?
			\Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
		$id = (array) $id;

		foreach ($id as $id_) {
			$this->findModel($id_)->delete();
		}

		\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Bonus model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 * @return Bonus the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Bonus::findOne($id)) !== null) {
			return $model;
		}
		else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

    public function actionFile()
    {
        $path = Yii::getAlias('@modules/bonuses/common/files/bonuses.xlsx');

        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        }

        throw new NotFoundHttpException();
    }


    /**
     * Выгрузка потраченных баллов
     * @return \yii\console\Response|Response
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadSpentBonus(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
        $arrSpentBonus = Bonus::arrSpentBonus();
        return \Yii::$app->response->sendContentAsFile($arrSpentBonus, 'spent.xml');
    }
}
