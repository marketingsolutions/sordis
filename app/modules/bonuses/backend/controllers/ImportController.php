<?php

namespace modules\bonuses\backend\controllers;

use backend\base\Controller;
use libphonenumber\PhoneNumberFormat;
use modules\bonuses\common\models\Bonus;
use modules\profiles\common\models\Action;
use modules\profiles\common\models\Profile;
use modules\bonuses\backend\forms\BonusesImportForm;
use marketingsolutions\phonenumbers\PhoneNumber;
use modules\profiles\common\models\Region;
use ms\loyalty\identity\phonesEmails\common\models\Identity;
use ms\loyalty\identity\phonesEmails\common\models\IdentityType;
use yz\admin\import\BatchImportAction;
use yz\admin\import\ImportForm;
use yz\admin\import\InterruptImportException;
use modules\profiles\common\models\Dealer;

class ImportController extends Controller
{
    const FIO = 'фио';
    const PHONE = 'телефон';
    const EMAIL = 'e-mail';
    const REGION = 'регион';
    const DEALER = 'дистрибьютор';
    const BONUSES = 'баллы';

    public function actions()
    {
        return [
            'index' => [
                'class' => BatchImportAction::className(),
                'extraView' => '@modules/bonuses/backend/views/import-bonuses/index.php',
                'importConfig' => [
                    'class' => BonusesImportForm::class,
                    'availableFields' => [
                        self::FIO => 'ФИО',
                        self::PHONE => 'Телефон',
                        self::EMAIL => 'E-mail',
                        self::REGION => 'Регион',
                        self::DEALER => 'Дистрибьютор',
                        self::BONUSES => 'Баллы',
                    ],
                    'rowImport' => [$this, 'rowImport'],
                    'skipFirstLine' => true,
                ]
            ]
        ];
    }

    public function rowImport(BonusesImportForm $form, array $row)
    {
        if (empty($row[self::PHONE])) {
            return;
        }

        if (PhoneNumber::validate($row[self::PHONE], 'RU') == false) {
            throw new InterruptImportException('Неверный номер телефона: ' . $row[self::PHONE], $row);
        }

        $region = $this->importRegion($row);
        $dealer = $this->importDealer($region, $row);
        $profile = $this->importProfile($dealer, $row);
        $this->importBonusesReport($row, $profile, $form);
    }

    /**
     * @param $row
     * @return Region
     * @throws InterruptImportException
     */
    private function importRegion($row)
    {
        $region = Region::findOne(['name' => trim($row[self::REGION])]);

        if ($region == null) {
            $region = new Region();
            if ($region->save() == false) {
                throw new InterruptImportException('Ошибка при импорте региона: ' . implode(', ', $region->getFirstErrors()), $row);
            }
            $region->refresh();
        }

        return $region;
    }

    /**
     * @param Region $region
     * @param $row
     * @return Dealer
     * @throws InterruptImportException
     */
    private function importDealer(Region $region, $row)
    {
        # Дистрибьютор
        $dealer = Dealer::findOne(['name' => trim($row[self::DEALER])]);

        if ($dealer == null) {
            $dealer = new Dealer();
            $dealer->region_id = $region->id;
            $dealer->name = trim($row[self::DEALER]);
            if ($dealer->save() == false) {
                throw new InterruptImportException('Ошибка при импорте диcтрибьютора: ' . implode(', ', $dealer->getFirstErrors()), $row);
            }
        }

        return $dealer;
    }

    private function importProfile(Dealer $dealer, $row)
    {
        $phone = trim($row[self::PHONE]);
        if (PhoneNumber::validate($phone, 'RU') == false) {
            throw new InterruptImportException('Неверный номер телефона: ' . $phone, $row);
        }
        $phoneNumber = PhoneNumber::format($phone, PhoneNumberFormat::E164, 'RU');

        /** @var Profile $profile */
        $profile = Profile::findOne(['phone_mobile' => $phoneNumber]);

        if ($profile === null) {
            $profile = new Profile();
            $profile->phone_mobile_local = $phone;
            $profile->dealer_id = $dealer->id;

            $fio = trim($row[self::FIO]);
            $fioParts = explode(' ', $fio);

            if (!empty($fioParts[2])) {
                $profile->last_name = $fioParts[0];
                $profile->first_name = $fioParts[1];
                $profile->middle_name = $fioParts[2];
            }
            elseif (!empty($fioParts[1])) {
                $profile->last_name = $fioParts[0];
                $profile->first_name = $fioParts[1];
            }
            elseif (!empty($fioParts[0])) {
                $profile->first_name = $fioParts[0];
            }

            $profile->email = trim($row[self::EMAIL]);

            if ($profile->save() == false) {
                throw new InterruptImportException('Ошибка при импорте участника: ' . implode(', ', $profile->getFirstErrors()), $row);
            }
        }

        return $profile;
    }

    private function importBonusesReport($row, Profile $profile, BonusesImportForm $form)
    {
        $month = intval($form->month);
        $year = intval($form->year);

        $bonus = new Bonus;
        $bonus->profile_id = $profile->id;
        $bonus->month = $month;
        $bonus->year = $year;

        $fileBonus = intval(floatval(str_replace(',', '.', str_replace(' ', '', $row[self::BONUSES]))));

        if ($fileBonus == 0) {
            return;
            // throw new InterruptImportException("Ошибка при импорте бонуса: бонусы не могут быть 0 в последней колонке", $row);
        }

        $bonus->bonuses = $fileBonus;
        $bonus->duple = false;

        if ($bonus->save() == false) {
            throw new InterruptImportException('Ошибка при импорте бонуса: ' . implode(', ', $bonus->getFirstErrors()), $row);
        }
    }
}