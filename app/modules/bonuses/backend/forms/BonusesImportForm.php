<?php

namespace modules\bonuses\backend\forms;

use yz\admin\import\ImportForm;


class BonusesImportForm extends ImportForm
{
	/** @var integer */
	public $month;

	/** @var integer */
	public $year;

	public function rules()
	{
		return array_merge(parent::rules(), [
			[['month', 'year'], 'required'],
			[['month', 'year'], 'integer'],
		]);
	}

	public function attributeLabels()
	{
		return array_merge(parent::attributeLabels(), [
			'month' => 'Месяц, номер',
			'year' => 'Год',
            'action' => 'Акция',
		]);
	}
}