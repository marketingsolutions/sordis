<?php

namespace modules\bonuses\backend;

use modules\bonuses\backend\rbac\Rbac;
use yz\icons\Icons;


/**
 * Class Module
 */
class Module extends \modules\bonuses\common\Module
{
    public function getName()
    {
        return 'Бонусные баллы';
    }

    public function getAdminMenu()
    {
        return [
            [
                'label' => 'Бонусные баллы',
                'icon' => Icons::o('shopping-cart'),
                'items' => [
                    [
                        'route' => ['/bonuses/temp-bonuses/index'],
                        'label' => 'Начисленные баллы',
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/bonuses/temp-confirmed-bonuses/index'],
                        'label' => 'Подтвержденные баллы',
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/bonuses/bonuses/index'],
                        'label' => 'Зачисление баллов  на кошельки участников',
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/bonuses/upload-bonus-file/index'],
                        'label' => 'Загрузка бонусов из xml',
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/bonuses/bonuses/download-spent-bonus'],
                        'label' => 'Выгрузка потраченных бонусов в xml',
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/bonuses/bonuses/project-info'],
                        'label' => 'Информация о балансе проекта',
                        'icon' => Icons::o('list'),
                    ],
                ]
            ],
        ];
    }

    public function getAuthItems()
    {
        return array_merge(parent::getAuthItems(), Rbac::dependencies());
    }
}