<?php

namespace modules\bonuses\console\controllers;

use modules\bonuses\common\models\Bonus;
use modules\profiles\common\models\Profile;
use modules\sms\common\models\SmsLog;
use Yii;
use console\base\Controller;

class SmsBonusesController extends Controller
{
    public function actionSend()
    {
        $frontend = Yii::getAlias('@frontendWeb');

        $bonuses = Bonus::find()
            ->joinWith('profile')
            ->where(['sms_sent' => false, 'paid' => true]);

        foreach ($bonuses->each() as $bonus) {
            /** @var Bonus $bonus */
            /** @var Profile $profile */
            $profile = $bonus->profile;
            echo '... ' . $bonus->profile->phone_mobile . PHP_EOL;

            $message = "Вам начислено $bonus->bonuses баллов, можете использовать их на $frontend";
            $result = Yii::$app->sms->send($profile->phone_mobile, $message);

            SmsLog::add([
                'type' => SmsLog::TYPE_BONUS_ADD,
                'phone_mobile' => $profile->phone_mobile,
                'message' => $message,
                'status' => $result,
            ]);

            try {
                if (!empty($profile->email)) {
                    Yii::$app->mailer->compose('@modules/bonuses/common/emails/bonus_paid.php', [
                        'bonus' => $bonus,
                        'profile' => $profile,
                        'msg' => $message,
                    ])->setTo($profile->email)->send();
                }
            }
            catch (\Exception $e) {
            }

            $bonus->updateAttributes(['sms_sent' => true]);
        }
    }
}