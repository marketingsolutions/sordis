<?php
namespace modules\bonuses\console\controllers;

use console\base\Controller;
use modules\bonuses\common\finance\BonusesPartner;
use modules\bonuses\common\models\Bonus;
use modules\profiles\common\models\Action;
use modules\profiles\common\models\Profile;
use ms\loyalty\finances\common\models\Transaction;
use yii\db\Expression;

class PayBonusesController extends Controller
{
	public function actionIndex()
	{
		$bonuses = Bonus::find()->where(['paid' => false, 'duple' => false]);

		foreach ($bonuses->each() as $bonus) {
            /** @var Bonus $bonus */

            /** @var Profile $profile */
            $profile = $bonus->profile;
            $purse = $profile->purse;

			$transaction = \Yii::$app->db->beginTransaction();

			try {
				$t = new Transaction();
				$t->type = Transaction::INCOMING;
				$t->amount = $bonus->bonuses;
				$t->partner_type = BonusesPartner::class;
				$t->partner_id = $bonus->id;
				$t->title = "Зачисление баллов по списку #{$bonus->id} за {$bonus->month} месяц {$bonus->year} года";
				$t->purse_id = $purse->id;
				$t->save(false);

				$purse->balance = $purse->balance + $bonus->bonuses;
				$purse->save(false);

				$bonus->updateAttributes([
					'paid' => true,
					'paid_at' => new Expression('NOW()'),
				]);

				$transaction->commit();
			}
			catch (\Exception $e) {
				$transaction->rollBack();
				\Yii::error('Can not pay for Bonus #' . $bonus->id);
				\Yii::error("Error: " . $e->getMessage());

				throw $e;
			}
		}
	}
}