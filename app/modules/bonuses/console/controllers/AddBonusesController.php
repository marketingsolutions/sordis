<?php
namespace modules\bonuses\console\controllers;

use console\base\Controller;
use modules\bonuses\common\models\Bonus;
use modules\bonuses\common\models\TempConfirmedBonuses;
use modules\profiles\common\models\Profile;

class AddBonusesController extends Controller
{
    public function actionIndex(){
        //Добавляем бонусы из таблицы временных подтвержденных бонусов
        $model = TempConfirmedBonuses::find()->where(['updated_at' => null, 'paid_at' => null])->asArray()->all();
        $fields = [];
        $rows = ['profile_id', 'bonus_type', 'code_egais', 'inn', 'license_number', 'period', 'bonuses', 'created_at', 'action_id', 'target_id', 'target_name', 'poll_id'];
        $now = date("Y-m-d H:i:s");
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            foreach ($model as $item) {
                $profile = Profile::findOne(['phone_mobile' => $item['phone_mobile']]);
                $profileId = $profile ? $profile->id : null;
                $fields[] = [
                    $profileId,
                    $item['bonus_type'],
                    $item['code_egais'],
                    $item['inn'],
                    $item['license_number'],
                    $item['period'],
                    $item['bonus_sum'],
                    $item['created_at'],
                    $item['action_id'],
                    $item['target_id'],
                    $item['target_name'],
                    $item['poll_id']
                ];

                //Обновляем признак зачисления баллов в таблицу временных подтвержденных бонусов
                $addBonus = TempConfirmedBonuses::findOne($item['id']);
                $addBonus->updated_at = $now;
                $addBonus->paid_at = $now;
                $addBonus->update(false);
            }
            $db = \Yii::$app->db;
            $sql = $db->queryBuilder->batchInsert(Bonus::tableName(), $rows, $fields);
            $db->createCommand($sql)->execute();
            $transaction->commit();
        }
        catch (\Exception $e) {
            $transaction->rollBack();
            \Yii::error('Ошибка при начислении бонусов');
            \Yii::error("Error: " . $e->getMessage());

            throw $e;
        }




    }
}