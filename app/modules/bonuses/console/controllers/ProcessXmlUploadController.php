<?php
namespace modules\bonuses\console\controllers;

use console\base\Controller;
use modules\bonuses\common\finance\BonusesPartner;
use modules\bonuses\common\models\Bonus;
use modules\bonuses\common\models\TempBonuses;
use modules\bonuses\common\models\TempConfirmedBonuses;
use modules\bonuses\common\models\UploadBonusFile;
use modules\profiles\common\models\Action;
use modules\profiles\common\models\Profile;
use ms\loyalty\finances\common\models\Transaction;
use yii\db\Exception;
use yii\db\Expression;
use modules\logs\common\models\XmlLogs;

class ProcessXmlUploadController extends Controller
{
	public function actionIndex()
	{
        $processFile = UploadBonusFile::findOne(['status_process'=>UploadBonusFile::STATUS_NEW, 'processed_at' => null]);
        if(!$processFile){
            echo "Нет данных для обработки!\n";
            return false;
        }
        if (getenv('YII_ENV') == 'dev') {
            $xmlFilePath = \Yii::getAlias('@backend/web/upload_bonus/' . $processFile->file_name);
        }else{
            $xmlFilePath = \Yii::getAlias('@data/upload_bonus/' . $processFile->file_name);
        }
        $textXmlFile = simplexml_load_file($xmlFilePath);
        //Разбираем основные баллы и добавляем их в таблицу TempBonuses
        $now = date("Y-m-d H:i:s");
        $processFile->status_process = UploadBonusFile::STATUS_PROCESS;
        $processFile->update(false);

        //Выгружаем все имеющиеся подтвержденные бонусы и формируем массив из Уникальных ID (Секция ConfirmPointsID в файле xml)
        $confirmPointsID = TempConfirmedBonuses::find()
            ->select('id, confirm_points_id')
            ->indexBy('confirm_points_id')
            ->asArray()
            ->all();
//        var_dump($this->idUniqBonus( $confirmPointsID, '12345|32154|dhfgdhfg'));
//
//        if($this->idUniqBonus( $confirmPointsID, '12345|32154|dhfgdhfg'))
//        print_r($confirmPointsID);
//        exit;



        $transaction = \Yii::$app->db->beginTransaction();
        try {
            //Удаляем основные и целевые баллы перед загрузкой нового файла
            TempBonuses::deleteAll("bonus_type='".TempBonuses::TARGET_BONUS."' OR bonus_type='".TempBonuses::BASIC_BONUS."'");
            foreach ($textXmlFile->ObmenData->ObmenParticipantData as $data) {
                $phoneMobile = '+'.$data->Participant; //Моб тел
                //Зачитываем Основные баллы
                if(!empty($data->ScorePoints->BasicScore)) {
                    $sumTotalFromTitle = $data->ScorePoints->BasicScore->BasicPointsTotal; //Сумма из xml- файла (итог по основноым баллам)
                    $sumUploadScore = 0; //Сумма загруженных
                    foreach ($data->ScorePoints->BasicScore->ParticipantBasicPoints->PartBasicPoints as $scorData) {
                        $bonusType = TempBonuses::BASIC_BONUS;
                        $codeEgais = $scorData->FSRARID;
                        $inn = $scorData->INN;
                        $period = $scorData->Period;
                        $bonusSum = $scorData->Points; //Сумма основные баллы
                        $newBonusAdd = new TempBonuses();
                        $newBonusAdd->bonus_type = $bonusType;
                        $newBonusAdd->phone_mobile = $phoneMobile;
                        $newBonusAdd->code_egais = $codeEgais;
                        $newBonusAdd->inn = $inn;
                        $newBonusAdd->state = $scorData->State;
                        //$newBonusAdd->period = substr($period, 0, 4) . '-' . substr($period, 4, 2) . '-01';
                        $newBonusAdd->period = $period . '-01';
                        $newBonusAdd->bonus_sum = $bonusSum;
                        $newBonusAdd->created_at = $now;
                        $newBonusAdd->save(false);
                        $sumUploadScore += $bonusSum;
                    }
                    if ($sumTotalFromTitle == $sumUploadScore) {
                        echo "По участнику " . $phoneMobile . " cуммы основных баллов загружены верно! \n";
                    } else {
                        throw new Exception('Ошибка в загруженных суммах у участника '.$phoneMobile.'! Суммы на совпадают!');
                    }
                }
                //Зачитываем целевые баллы
                if(!empty($data->ScorePoints->TargetScore)){
                    $sumTotalFromTitle = $data->ScorePoints->TargetScore->TargetPointsTotal; //Сумма из xml- файла (итог по целевым баллам)
                    $sumUploadScore = 0; //Сумма загруженных
                    foreach ($data->ScorePoints->TargetScore->ParticipantTargetPoints->PartTargetPoints as $scorData){
                        $bonusType = TempBonuses::TARGET_BONUS;
                        $codeEgais = $scorData->FSRARID;
                        $inn = $scorData->INN;
                        $period = $scorData->Period;
                        $bonusSum = $scorData->Points; //Сумма основные баллы
                        $newBonusAdd = new TempBonuses();
                        $newBonusAdd->bonus_type = $bonusType;
                        $newBonusAdd->phone_mobile = $phoneMobile;
                        $newBonusAdd->code_egais = $codeEgais;
                        $newBonusAdd->inn = $inn;
                        $newBonusAdd->state = $scorData->State;
                        $newBonusAdd->target_id = $scorData->TargetID;   //ID цели
                        $newBonusAdd->target_name = $scorData->TargetName; //Название цели
                        //$newBonusAdd->period = substr($period, 0, 4) . '-' . substr($period, 4, 2) . '-01';
                        $newBonusAdd->period = $period . '-01';
                        $newBonusAdd->bonus_sum = $bonusSum;
                        $newBonusAdd->created_at = $now;
                        $newBonusAdd->save(false);
                        $sumUploadScore += $bonusSum;
                    }
                    if ($sumTotalFromTitle == $sumUploadScore) {
                        echo "По участнику " . $phoneMobile . " cуммы целевых баллов загружены верно! \n";
                    } else {
                        throw new Exception('Ошибка в загруженных суммах у участника '.$phoneMobile.'! Суммы на совпадают!');
                    }
                }
                //Зачитываем подтвержденные баллы
                if(!empty($data->ScorePoints->ConfirmedScore)){
                    $sumTotalFromTitle = $data->ScorePoints->ConfirmedScore->ConfirmedPointsTotal; //Сумма из xml- файла (итог по подтвержденным баллам)
                    $sumUploadScore = 0; //Сумма загруженных
                    foreach ($data->ScorePoints->ConfirmedScore->ParticipantConfirmedPoints->PartConfirmedPoints as $scorData){
                        //---------
                        if($scorData->TypeInPastLife != 'Target' || $scorData->TypeInPastLife != 'Basic'){
                            print_r($scorData->TypeInPastLife);
                        }
                        //--------
                        if($scorData->TypeInPastLife == 'Target'){
                            $bonusType = TempConfirmedBonuses::TARGET_BONUS;
                        }elseif($scorData->TypeInPastLife == 'Basic') {
                            $bonusType = TempConfirmedBonuses::BASIC_BONUS;
                        }elseif($scorData->TypeInPastLife == 'Social') {
                            $bonusType = TempConfirmedBonuses::SOCIAL_BONUS;
                        }
                        $codeEgais = $scorData->FSRARID;
                        $inn = $scorData->INN;
                        $period = $scorData->Period;
                        $bonusSum = $scorData->Points; //Сумма подтвержденные баллы
                        if($this->idUniqBonus( $confirmPointsID, (string)$scorData->ConfirmPointsID)){
                            $newBonusAdd = new TempConfirmedBonuses();
                            $newBonusAdd->bonus_type = $bonusType;
                            $newBonusAdd->phone_mobile = $phoneMobile;
                            $newBonusAdd->state = $scorData->State;
                            $newBonusAdd->code_egais = $codeEgais;
                            $newBonusAdd->inn = $inn;
                            $newBonusAdd->target_id = $scorData->TargetID;   //ID цели
                            $newBonusAdd->target_name = $scorData->TargetName; //Название цели
                            //$newBonusAdd->period = substr($period, 0, 4) . '-' . substr($period, 4, 2) . '-01';
                            $newBonusAdd->period = $period . '-01';
                            $newBonusAdd->bonus_sum = $bonusSum;
                            $newBonusAdd->confirm_points_id = (string)$scorData->ConfirmPointsID;
                            $newBonusAdd->created_at = $now;
                            $newBonusAdd->save(false);
                           // $sumUploadScore += $bonusSum;
                            $arrMergeId=[];
                            $arrMergeId[(string)$scorData->ConfirmPointsID]=['confirm_points_id'=>(string)$scorData->ConfirmPointsID];
                            array_merge($confirmPointsID, $arrMergeId);
                        }else{
                            $this->updateState($scorData->ConfirmPointsID, $scorData->State);
                            $message = "Начисление с ID ".$scorData->ConfirmPointsID." не добавлено, т к уже было загружено ранее, либо параметр ID начисления подтвержденных бонусов в файле пустой!";;
                            XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Обработка xml-файла Бонусные баллы полученного с FTP', $message);
                            echo "Начисление с ID ".$scorData->ConfirmPointsID." не добавлено, т к уже было загружено ранее, либо параметр ID начисления подтвержденных бонусов пустой! \n";
                        }
                    }
                    /*========Проверка по суммам отключена, т к есть уникальный ID зачисления=====*/
//                    if ($sumTotalFromTitle == $sumUploadScore) {
//                        echo "По участнику " . $phoneMobile . " cуммы подтвержденных баллов загружены верно! \n";
//                    } else {
//                        throw new Exception('Ошибка в загруженных суммах у участника '.$phoneMobile.'! Суммы загруженных ='.$sumUploadScore.' | итоговая сумма из xml='.$sumTotalFromTitle.' на совпадают!');
//                    }
                }
            }
            $transaction->commit();
            $statusComplete = UploadBonusFile::findOne(['id' => $processFile->id]);
            $statusComplete->status_process = UploadBonusFile::STATUS_COMPLETE;
            $statusComplete->processed_at = date("Y-m-d H:i:s");
            $statusComplete->update(false);
        }
        catch (\Exception $e) {
            $transaction->rollBack();
            $statusError = UploadBonusFile::findOne($processFile->id);
            $statusError->status_process = UploadBonusFile::STATUS_ERROR;
            $statusError->update(false);
            \Yii::error('НЕ удалось добавить бонусы в таблицу #');
            \Yii::error("Error: " . $e->getMessage());
            throw $e;
        }
    }

    /**
     * Проверка, есть ли такое бонусное начисление в подтвержденных баллах или нет (если есто, то false, если нет то true)
     * @param $arrBonusId
     * @param $bonusId
     * @return bool
     */
    public function idUniqBonus($arrBonusId, $bonusId){
        if(!isset($bonusId) || !$bonusId){
            return false;
        }
	    if(array_key_exists($bonusId, $arrBonusId)){
	        return false;
        }else{
	        return true;
        }
    }

    public function updateState($confirmPointsID, $state){
        $model = TempConfirmedBonuses::findOne(['confirm_points_id' => $confirmPointsID]);
        if($model && $state){
            $model->state = (int)$state;
            $model->update(false);
        }
    }
}