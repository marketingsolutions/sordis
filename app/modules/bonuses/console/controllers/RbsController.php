<?php
namespace modules\bonuses\console\controllers;

use console\base\Controller;
use modules\bonuses\common\finance\BonusesPartner;
use modules\bonuses\common\models\Bonus;
use modules\bonuses\common\models\RbsPayment;
use modules\profiles\common\models\Action;
use modules\profiles\common\models\Profile;
use ms\loyalty\finances\common\models\Transaction;
use ms\loyalty\prizes\payments\common\models\Payment;
use ms\loyalty\prizes\payments\common\models\Settings;
use ms\loyalty\taxes\common\calculators\Calculator;
use yii\db\Expression;

class RbsController extends Controller
{
	public function actionIndex()
	{
        /** @var Profile $profile */
        $profile = Profile::findOne($this->recipient_id);

	    $paymentParameters = [
	        'phone_mobile' => $profile->phone_mobile,
            'BankAccount' => '40817810438253444252',
            'rsvr_bik' => '044525225',
            'rsvr_fio' => $profile->full_name,
            'payer_phone' => $profile->phone_mobile,
            'payer_fio' => $profile->full_name,
            'payer_address' => 'Москва, Рижский проезд, д.7 кв. 63',
            'pay_purpose' => 'Gift',
        ];

        $model = new RbsPayment();
        $model->payment_amount = 10;
        $model->type = Payment::TYPE_RBS;
        $model->status = Payment::STATUS_NEW;
        $model->recipient_id = 508;
        $model->loyalty_1c_name = '...';
        $model->dealer_id = '...';
        $model->setPaymentParametersArray($paymentParameters);
        $model->calculateAmounts();

        $model->save();
	}
}