<?php

namespace modules\pictures\frontend\controllers\api;

use modules\pictures\frontend\models\ApiImage;
use modules\pictures\frontend\models\ApiPicture;
use modules\profiles\frontend\models\ApiProfile;
use ms\loyalty\api\common\models\Log;
use ms\loyalty\api\frontend\base\ApiController;
use Yii;

class PicturesController extends ApiController
{
    public function actionList()
    {
        $profile_id = Yii::$app->request->post('profile_id');
        /** @var ApiProfile $profile */
        $profile = ApiProfile::findOne($profile_id);

        if ($profile === null) {
            return $this->error('Не найден участник по profile_id=' . $profile_id, 'Ошибка получения списка изображений');
        }

        $pictures = ApiPicture::find()
            ->where(['profile_id' => $profile_id])
            ->orderBy(['id' => SORT_DESC])
            ->all();

        $return = ['result' => 'OK', 'pictures' => $pictures];
        Log::setComment('Выдача списка изображений по участнику');

        return $return;
    }

    public function actionLoadImage()
    {
        $profile_id = Yii::$app->request->post('profile_id');
        $latitude = Yii::$app->request->post('latitude');
        $longitude = Yii::$app->request->post('longitude');
        /** @var ApiProfile $profile */
        $profile = ApiProfile::findOne($profile_id);

        if ($profile === null) {
            return $this->error('Не найден участник по profile_id=' . $profile_id, 'Ошибка получения списка изображений');
        }

        $data = ['ApiImage' => Yii::$app->request->post()];
        $model = new ApiImage();

        if ($model->load($data) && $model->save('pictures')) {
            $picture = new ApiPicture();
            $picture->profile_id = $profile_id;
            $picture->file = $model->getFilename();
            if (!empty($latitude)) {
                $picture->latitude = $latitude;
            }
            if (!empty($longitude)) {
                $picture->longitude = $longitude;
            }
            $picture->save(false);

            return $this->ok(compact('picture'), 'Успешная загрузка изображения');
        }
        else {
            return $this->error($model->getFirstErrors(), 'Ошибка при загрузке изображения');
        }
    }

    public function actionLoadImageTest()
    {
        $filePath = Yii::getAlias('@data' . DIRECTORY_SEPARATOR . 'pictures' . DIRECTORY_SEPARATOR . '1.jpg');
        $data = file_get_contents($filePath);
        return base64_encode($data);
    }
}