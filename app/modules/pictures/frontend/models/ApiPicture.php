<?php

namespace modules\pictures\frontend\models;

use modules\pictures\common\models\Picture;
use Yii;

class ApiPicture extends Picture
{
    public function fields()
    {
        return [
            'id',
            'profile_id',
            'file',
            'file_url' => function (ApiPicture $model) {
                return $model->getPictureUrl();
            },
            'latitude',
            'longitude',
            'created_at' => function (ApiPicture $model) {
                return empty($model->created_at)
                    ? null
                    : (new \DateTime($model->created_at))->format('d.m.Y H:i:s');
            },
        ];
    }
}