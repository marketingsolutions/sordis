<?php

namespace modules\pictures\frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;

class ApiImage extends Model
{
    public $type;
    public $image;
    protected $filename;
    protected $directory;

    public function rules()
    {
        return [
            ['type', 'required'],
            ['image', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'type' => 'Расширение картинки',
            'image' => 'Изображение в Base64'
        ];
    }

    public function save($directory)
    {
        $this->directory = $directory;
        $fileName = uniqid() . '.' . $this->type;
        $dir = Yii::getAlias('@data' . DIRECTORY_SEPARATOR . $directory);
        FileHelper::createDirectory($dir);
        $filePath = Yii::getAlias($dir . DIRECTORY_SEPARATOR . $fileName);
        $fileSize = (int) file_put_contents($filePath, base64_decode($this->image));

        if ($fileSize == 0) {
            $this->addError('image', 'Ошибка при загрузке файла, неверное значение');

            return false;
        }

        $this->filename = $fileName;

        return true;
    }

    public function getWebpath()
    {
        return  Yii::getAlias('@frontendWeb/data/' . $this->directory . '/' . $this->filename);
    }

    public function getFilename()
    {
        return $this->filename;
    }
}