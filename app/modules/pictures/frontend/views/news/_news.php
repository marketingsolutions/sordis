<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \modules\pictures\common\models\News $model
 */

?>

<div class="items">
  <div class="item sale-item--status">
    <div class="news-title">
      <?= Html::encode($model->title) ?>
    </div>
    <div class="news-content">
      <?= $model->content ?>
    </div>
    <div class="news-footer">
      <?= (new \DateTime($model->created_at))->format('d.m.Y') ?>
    </div>
  </div>
</div>
