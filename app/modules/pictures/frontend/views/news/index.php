<?php

use modules\warranty\common\models\Warranty;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\pictures\backend\models\NewsSearch $searchModel
 * @var array $columns
 */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

$css = <<<CSS
    .item {
        margin-top: 26px;
        padding: 12px 14px;
    }
    .news-title {
        font-weight: bold;
        margin-bottom: 10px;
    }
    .news-content {
        font-size: 13px;
    }
    .news-content p {
        line-height: 13px;
    }
    .news-footer {
        color: gray;
        font-size: 13px;
    }
CSS;

$this->registerCss($css);
?>

<?= \yii\widgets\ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_news',
]); ?>
