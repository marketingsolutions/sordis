<?php

namespace modules\pictures\backend;

use yz\icons\Icons;

/**
 * Class Module
 */
class Module extends \modules\pictures\common\Module
{
    public function getAdminMenu()
    {
        return [
            [
                'route' => ['/pictures/pictures/index'],
                'label' => 'Загруженные изображения',
                'icon' => Icons::o('picture-o'),
            ],
        ];
    }

}