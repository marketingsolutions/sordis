<?php

namespace modules\pictures\common\models;

use modules\profiles\common\models\Profile;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_pictures".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property string $file
 * @property string $latitude
 * @property string $longitude
 * @property string $created_at
 * @property string $updated_at
 *
 * @property string $picture_url
 * @property Profile $profile
 */
class Picture extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pictures}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Загруженное изображение';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Загруженные изображения';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['profile_id', 'integer'],
            ['file', 'string', 'max' => 255],
            ['file', 'file', 'extensions' => ['png', 'jpg', 'jpeg'], 'maxSize' => 1024 * 1024 * 15],
            ['latitude', 'string', 'max' => 255],
            ['longitude', 'string', 'max' => 255],
            ['created_at', 'safe'],
            ['updated_at', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'ID Участника',
            'file' => 'Файл',
            'latitude' => 'Координата широты',
            'longitude' => 'Координата долготы',
            'created_at' => 'Дата загрузки',
            'updated_at' => 'Дата обновления',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => (new \DateTime('now'))->format('Y-m-d H:i:s'),
            ],
        ];
    }

    public function getPictureUrl()
    {
        return empty($this->file)
            ? null
            : Yii::getAlias('@frontendWeb/data/pictures/' . $this->file);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }
}
