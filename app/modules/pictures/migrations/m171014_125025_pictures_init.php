<?php

use yii\db\Migration;
use yii\db\Schema;

class m171014_125025_pictures_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%pictures}}', [
            'id' => $this->primaryKey(),
            'profile_id' => $this->integer(),
            'file' => $this->string(),
            'latitude' => $this->string(),
            'longitude' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%pictures}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
