<?php
namespace modules\social\console\controllers;

use console\base\Controller;
use modules\profiles\common\models\Profile;
use modules\social\common\models\UploadsTargetsFile;
use modules\social\common\models\Targets;
use modules\social\common\models\TargetUsers;
use yii\db\Exception;
use modules\logs\common\models\XmlLogs;

class ProcessXmlUploadController extends Controller
{
    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionIndex()
    {
        $processFile = UploadsTargetsFile::findOne(['status_process'=>UploadsTargetsFile::STATUS_NEW, 'processed_at' => null]);
        if(!$processFile){
            //echo "Нет данных для обработки!\n";
            $message = "Нет данных для обработки!";
            XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Загрузка данных в систему по целям', $message);
            throw new Exception('Нет данных для обработки!');
            return false;
        }
        if (getenv('YII_ENV') == 'dev') {
            $xmlFilePath = \Yii::getAlias('@backend/web/upload_target/' . $processFile->file_name);
        }else{
            $xmlFilePath = \Yii::getAlias('@data/upload_target/' . $processFile->file_name);
        }

        $textXmlFile = simplexml_load_file($xmlFilePath);
        if (!$this->isTarget($textXmlFile->TargetID)){
            $statusError = UploadsTargetsFile::findOne($processFile->id);
            $statusError->status_process = UploadsTargetsFile::STATUS_ERROR;
            $statusError->update(false);
            $message = 'Ошибка! Цель с ID- '.$textXmlFile->TargetID.' уже существует в системе!';
            XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Загрузка данных в систему по целям', $message);
            throw new Exception('Ошибка! Цель с ID- '.$textXmlFile->TargetID.' уже существует в системе!');
        }
        $processFile->status_process = UploadsTargetsFile::STATUS_PROCESS;
        $processFile->update(false);
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            //Добавляем цель
            $newTargets = new Targets();
            $newTargets->sordis_id = $textXmlFile->TargetID;
            $newTargets->name = $textXmlFile->TargetName;
            $newTargets->description = $textXmlFile->TargetDescription;
            $newTargets->date_appearance = date("Y-m-d", strtotime($textXmlFile->TargetDateAppearance));
            $newTargets->date_begin = date("Y-m-d",strtotime($textXmlFile->TargetDateBegin));
            $newTargets->date_end = date("Y-m-d", strtotime($textXmlFile->TargetDateEnd));
            $newTargets->save(false);

            //Добавляем участников для показа данных по этой цели
            foreach ($textXmlFile->TargetParticipants->TargetParticipant as $user){
                $profilePhone = '+'.$user->Participant;
                $profile = Profile::findOne(['phone_mobile' => $profilePhone]);

                if(!$profile){
                    $message = 'Ошибка! Участника  с телефоном '.$profilePhone.' отсутствует в системе!';
                    XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Загрузка данных в систему по целям', $message);
                    throw new Exception('Ошибка! Участника  с телефоном '.$profilePhone.' отсутствует в системе!');
                }
                if($this->isUserInTargets($profile->id, $newTargets->id)){
                    $newUserTarget = new TargetUsers();
                    $newUserTarget->profile_id = $profile->id;
                    $newUserTarget->target_id = $newTargets->id;
                    $newUserTarget->save(false);
                    $message = "Участник ".$profile->full_name." - ".$profile->phone_mobile." - ".Profile::getRoles()[$profile->role]." >>> добавлен к цели";
                    XmlLogs::addMessage( XmlLogs::TYPE_MESSAGE_INFO,  'Загрузка данных в систему по целям', $message);
                    echo "Участник ".$profile->full_name." - ".$profile->phone_mobile." - ".Profile::getRoles()[$profile->role]." >>> добавлен к цели \n\n";
                }else{
                    $message = "Участник ".$profile->full_name." - ".$profile->phone_mobile." - ".Profile::getRoles()[$profile->role]." >>> <span style='color:red;'>ПРОПУЩЕН</span>, т. к. уже добавлен к цели \n";
                    XmlLogs::addMessage( XmlLogs::TYPE_MESSAGE_INFO,  'Загрузка данных в систему по целям', $message);
                    echo "Участник ".$profile->full_name." - ".$profile->phone_mobile." - ".Profile::getRoles()[$profile->role]." >>> <span style='color:red;'>ПРОПУЩЕН</span>, т. к. уже добавлен к цели \n";
                }
            }
            $statusComplete = UploadsTargetsFile::findOne(['id' => $processFile->id]);
            $statusComplete->status_process = UploadsTargetsFile::STATUS_COMPLETE;
            $statusComplete->processed_at = date("Y-m-d H:i:s");
            $statusComplete->update(false);
            $transaction->commit();
        }
        catch (\Exception $e) {
            $transaction->rollBack();
            $statusError = UploadsTargetsFile::findOne($processFile->id);
            $statusError->status_process = UploadsTargetsFile::STATUS_ERROR;
            $statusError->update(false);
            $message = 'Не удалось добавить Цель и участников!!!';
            XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Загрузка данных в систему по целям', $message);
            \Yii::error('Не удалось добавить Цель и участников!!!');
            \Yii::error("Error: " . $e->getMessage());
            throw $e;
        }
    }

    /**
     * Проверка на существование участника в данной цели
     * @param $profileId
     * @param $targetId
     * @return bool
     */
    public  function isUserInTargets($profileId, $targetId)
    {
        return TargetUsers::findOne(['profile_id' => $profileId, 'target_id' => $targetId]) ? false : true;
    }

    /**
     * Проверка на наличие цели (воизбежании задвоений целей)
     * @param $sordisTargetId
     * @return bool
     */
    public function isTarget($sordisTargetId){
        return Targets::findOne(['sordis_id' => $sordisTargetId]) ? false : true;
    }
}