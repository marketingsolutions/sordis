<?php

use yii\db\Migration;

/**
 * Class m181102_124730_social_add_column_comment_created_at_action_user_table
 */
class m181102_124730_social_add_column_comment_created_at_action_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%social_action_user}}', 'comment', $this->text());
        $this->addColumn('{{%social_action_user}}', 'created_at', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%social_action_user}}', 'comment');
        $this->dropColumn('{{%social_action_user}}', 'created_at');
    }
}
