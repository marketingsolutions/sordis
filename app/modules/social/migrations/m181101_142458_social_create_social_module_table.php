<?php

use yii\db\Migration;

/**
 * Class m181101_142458_social_create_social_module_table
 */
class m181101_142458_social_create_social_module_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%social_action_rules}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'rules' => $this->text(),
            'action_text' => $this->text(),
            'date_from' => $this->date(),
            'date_to' => $this->date(),
            'bonus_sum' => $this->integer(),
            'email_is_send' => $this->boolean()->defaultValue(false),
            'sms_is_send' => $this->boolean()->defaultValue(false),
            'is_active' => $this->boolean()->defaultValue(true),
        ], $tableOptions);

        $this->createTable('{{%social_action_user}}', [
            'id' => $this->primaryKey(),
            'action_id' => $this->integer(),
            'profile_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_to_user_profile', '{{%social_action_user}}', 'profile_id', '{{%profiles}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk_action_to_rules', '{{%social_action_user}}', 'action_id', '{{%social_action_rules}}', 'id', 'RESTRICT', 'CASCADE');

        $this->addColumn('{{%attached_files}}', 'action_id', $this->integer());
        $this->addColumn('{{%attached_files}}', 'profile_id', $this->integer());

        $this->addForeignKey('fk_attached_to_profile', '{{%attached_files}}', 'profile_id', '{{%profiles}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk_attached_to_action', '{{%attached_files}}', 'action_id', '{{%social_action_rules}}', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_attached_to_profile', '{{%attached_files}}');
        $this->dropForeignKey('fk_attached_to_action', '{{%attached_files}}');

        $this->dropColumn('{{%attached_files}}', 'action_id');
        $this->dropColumn('{{%attached_files}}', 'profile_id');

        $this->dropForeignKey('fk_to_user_profile', '{{%social_action_user}}');
        $this->dropForeignKey('fk_action_to_rules', '{{%social_action_user}}');

        $this->dropTable('{{%social_action_user}}');
        $this->dropTable('{{%social_action_rules}}');

    }
}
