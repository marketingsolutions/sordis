<?php

use yii\db\Migration;

/**
 * Class m190111_083450_social_create_targets_table
 */
class m190111_083450_social_create_targets_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%uploads_targets_file}}', [
            'id' => $this->primaryKey(),
            'original_file_name' => $this->string(),
            'file_name' => $this->string(),
            'status_process' => $this->string(16),
            'created_at' => $this->dateTime(),
            'processed_at' => $this->dateTime(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%uploads_targets_file}}');
    }
}
