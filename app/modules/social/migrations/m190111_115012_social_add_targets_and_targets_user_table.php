<?php

use yii\db\Migration;

/**
 * Class m190111_115012_social_add_targets_and_targets_user_table
 */
class m190111_115012_social_add_targets_and_targets_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%targets}}', [
            'id' => $this->primaryKey(),
            'sordis_id' => $this->string(),
            'name' => $this->string(),
            'description' => $this->text(),
            'date_appearance' => $this->date(),
            'date_begin' => $this->date(),
            'date_end' => $this->date(),

        ], $tableOptions);

        $this->createTable('{{%target_users}}', [
            'id' => $this->primaryKey(),
            'target_id' => $this->integer(),
            'profile_id' => $this->integer(),

        ], $tableOptions);

        $this->addForeignKey('fk_target_users_targets', '{{%target_users}}', 'target_id', '{{%targets}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_target_users_profile', '{{%target_users}}', 'profile_id', '{{%profiles}}', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_target_users_targets', '{{%target_users}}');
        $this->dropForeignKey('fk_target_users_profile', '{{%target_users}}');
        $this->dropTable('{{%target_users}}');
        $this->dropTable('{{%targets}}');
    }
}
