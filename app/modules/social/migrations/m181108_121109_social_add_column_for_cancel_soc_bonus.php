<?php

use yii\db\Migration;

/**
 * Class m181108_121109_social_add_column_for_cancel_soc_bonus
 */
class m181108_121109_social_add_column_for_cancel_soc_bonus extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%social_action_user}}', 'is_cancel', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%social_action_user}}', 'cancel_text' , $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%social_action_user}}', 'is_cancel');
        $this->dropColumn('{{%social_action_user}}', 'cancel_text');
    }
}
