<?php

use yii\db\Migration;

/**
 * Class m181108_103212_social_add_column_to_temp_bonus_table
 */
class m181108_103212_social_add_column_to_temp_bonus_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%temp_bonuses}}', 'action_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%temp_bonuses}}', 'action_id');
    }
}
