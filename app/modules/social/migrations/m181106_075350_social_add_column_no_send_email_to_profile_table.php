<?php

use yii\db\Migration;

/**
 * Class m181106_075350_social_add_column_no_send_email_to_profile_table
 */
class m181106_075350_social_add_column_no_send_email_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'no_send_email', $this->boolean()->defaultValue(true));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'no_send_email');
    }
}
