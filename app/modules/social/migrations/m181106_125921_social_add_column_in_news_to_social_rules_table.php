<?php

use yii\db\Migration;

/**
 * Class m181106_125921_social_add_column_in_news_to_social_rules_table
 */
class m181106_125921_social_add_column_in_news_to_social_rules_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%social_action_rules}}', 'in_news', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%social_action_rules}}', 'in_news');
    }
}
