<?php

use yii\db\Migration;

/**
 * Class m181108_061913_social_add_column_sum_to_social_module
 */
class m181108_061913_social_add_column_sum_to_social_module extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%social_action_user}}', 'bonus_sum', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%social_action_user}}', 'bonus_sum');
    }
}
