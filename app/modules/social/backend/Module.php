<?php

namespace modules\social\backend;

use modules\bonuses\backend\rbac\Rbac;
use yz\icons\Icons;


/**
 * Class Module
 */
class Module extends \modules\social\common\Module
{
    public function getName()
    {
        return 'Бонусные баллы';
    }

    public function getAdminMenu()
    {
        return [
            [
                'label' => 'Призовые акции и цели',
                'icon' => Icons::o('ruble-sign'),
                'items' => [
					[
						'route' => ['/social/social-action-rules/index'],
						'label' => 'Призовые акции',
						'icon' => Icons::o('list'),
					],
                    [
                        'route' => ['/social/uploads-targets-file/index'],
                        'label' => 'Загрузка целей из xml',
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/social/targets/index'],
                        'label' => 'Участники - цели',
                        'icon' => Icons::o('list'),
                    ],
                ]
            ],
        ];
    }

    public function getAuthItems()
    {
        return array_merge(parent::getAuthItems(), Rbac::dependencies());
    }
}