<?php
namespace modules\social\backend\forms;
/**
 * Created by PhpStorm.
 * User: MihailKri
 * Date: 08.11.2018
 * Time: 14:43
 */
use modules\bonuses\common\models\TempBonuses;
use modules\social\common\models\SocialActionUser;
use yii\base\Model;

class CancelBonusForm extends Model
{
    /**
     * @var
     */
    public $cancel_text;

    /**
     * @var
     */
    public $profile_id;

    /**
     * @var
     */
    public $action_id;

    /**
     * @var
     */
    public $bonus_sum;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['cancel_text', 'string'],
            ['cancel_text', 'required'],
            [['profile_id', 'action_id', 'bonus_sum'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'cancel_text' => 'Обоснование (комментарий) к откату баллов'
        ];
    }

    public function process()
    {
        $addCancel = SocialActionUser::findOne(['profile_id' => $this->profile_id, 'action_id' => $this->action_id]);
        if(!$addCancel){
            return false;
        }
        $transaction = \Yii::$app->db->beginTransaction();

        $addCancel->is_cancel = true;
        $addCancel->cancel_text = $this->cancel_text;
        $addCancel->save(false);

        TempBonuses::findOne(['bonus_type' => TempBonuses::SOCIAL_BONUS, 'action_id' => $this->action_id, 'bonus_sum' => $this->bonus_sum])->delete();

        $transaction->commit();

        return true;
    }
}

