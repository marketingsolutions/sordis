<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use modules\social\common\models\TargetUsers;
use yz\icons\Icons;

$this->title = modules\social\common\models\TargetUsers::modelTitlePlural();
if(\Yii::$app->request->get('id') && TargetUsers::getTitleName(\Yii::$app->request->get('id'))){
    $this->title = "Участники цели ".TargetUsers::getTitleName(\Yii::$app->request->get('id'));
}

$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'target-users-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [/*['search'],*/ ['targets', 'export', /*'create', 'delete', */'return']],
            'gridId' => 'target-users-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\social\common\models\TargetUsers',
            'buttons' => [
                'targets' => [
                    'label' => '&nbsp;Вернуться к списку целей',
                    'icon' => Icons::o('arrow-alt-circle-left'),
                    'route' => '/social/targets/index',
                    'class' => 'btn btn-info',
                ],
            ]
        ]) ?>
    </div>

    <?= GridView::widget([
        'id' => 'target-users-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '',
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
