<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use modules\social\common\models\UploadsTargetsFile;
use yz\icons\Icons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\social\backend\models\UploadsTargetsFileSearch $searchModel
 * @var array $columns
 */

$this->title = modules\social\common\models\UploadsTargetsFile::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'uploads-targets-file-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [/*['search'],*/ ['export', 'create', 'delete', 'return']],
            'gridId' => 'uploads-targets-file-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\social\common\models\UploadsTargetsFile',
        ]) ?>
    </div>
    <?= GridView::widget([
        'id' => 'uploads-targets-file-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '{rollback} {delete}',
                'buttons' => [
                    'rollback' => function ($url, UploadsTargetsFile $model) {
                        $url = '/social/uploads-targets-file/rollback?id='.$model->id;
                        if ($model->status_process == UploadsTargetsFile::STATUS_NEW || $model->status_process == UploadsTargetsFile::STATUS_COMPLETE) {
                            return '';
                        }
                        return Html::a(Icons::i('refresh'), $url, [
                            'title' => 'Обработать заново',
                            'data-confirm' => 'Вы действительно хотите обработать данный файл заново?',
                            'data-method' => 'post',
                            'class' => 'btn btn-warning btn-sm',
                        ]);
                    },
                ],
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
