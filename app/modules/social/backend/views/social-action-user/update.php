<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\widgets\ActionButtons;

/**
 * @var yii\web\View $this
 * @var modules\social\common\models\SocialActionUser $model
 */
$this->title = \Yii::t('admin/t', 'Проверка выполнения условий акции');
$this->params['breadcrumbs'][] = ['label' => modules\social\common\models\SocialActionUser::modelTitlePlural(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<div class="social-action-user-update">

    <div class="text-right">
        <?php  Box::begin() ?>
        <?=  ActionButtons::widget([
            'order' => [['index', 'return']],
            'addReturnUrl' => false,
        ]) ?>
        <?php  Box::end() ?>
    </div>

    <?php echo $this->render('_form', [
        'model' => $model,
        'profile' => $profile,
        'action' => $action,
        'cancelModel' => $cancelModel,
    ]); ?>

</div>
