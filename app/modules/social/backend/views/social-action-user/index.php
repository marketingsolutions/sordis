<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\social\backend\models\SocialActionUserSearch $searchModel
 * @var array $columns
 */

$this->title = modules\social\common\models\SocialActionUser::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'social-action-user-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [/*['search'],*/ ['export', /*'delete',*/ 'return']],
            'gridId' => 'social-action-user-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\social\common\models\SocialActionUser',
        ]) ?>
    </div>
    <?= GridView::widget([
        'id' => 'social-action-user-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '{update}',
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
