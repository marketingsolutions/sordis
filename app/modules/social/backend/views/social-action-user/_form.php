<?php

use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;
use modules\social\common\models\SocialActionUser;

/**
 * @var yii\web\View $this
 * @var modules\social\common\models\SocialActionUser $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php  $box = FormBox::begin(['cssClass' => 'social-action-user-form box-primary', 'title' => '']) ?>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-sm">
                <tbody>
                <tr>
                    <th scope="row">ФИО участника</th>
                    <td><?=$profile->full_name?></td>
                </tr>
                <tr>
                    <th scope="row">Наменование акции</th>
                    <td><?=$action->name?></td>
                </tr>
                <tr>
                    <th scope="row">Условия акции</th>
                    <td><?=$action->rules?></td>
                </tr> <tr>
                    <th scope="row">Описание акции</th>
                    <td><?=$action->action_text?></td>
                </tr>
                <tr>
                    <th scope="row">Колличество начисленных бонусов за акцию</th>
                    <td><?=$action->bonus_sum?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php  FormBox::end() ?>
<?php  $box = FormBox::begin();?>
    <div class="row">
        <div class="col-md-12"><h3 align="center">Загруженные документы</h3></div>
    </div>
<?php if($data = \modules\social\common\models\SocialActionUser::isParticipantUser($profile->id,$action->id )):?>
    <div class="row">
        <ul class="thumbnails" style="list-style:none;">
            <?php foreach ($data as $item):?>
                <div class="col-md-4">
                    <li class="span4">
                        <a href="#" class="thumbnail">
                            <img src="<?php \Yii::getAlias('data')?>/<?=trim($item['file_path'], '@').'/'.$item['file_name']?>" alt="">
                        </a>
                    </li>
                </div>
            <?php endforeach;?>
        </ul>
    </div>
<?php endif;?>
    <hr>
    <div class="row">
        <label class="control-label col-sm-2" for="message-content">Скачать загруженные файлы</label>
        <div class="col-sm-8">
            <?= \ms\files\attachments\common\widgets\FileAttachmentsView::widget([
                'attachmentsQuery' => \ms\files\attachments\common\models\AttachedFile::find()->where(['profile_id' =>$profile->id, 'action_id' => $action->id]),
                'type' => 'userFile',
            ]) ?>
        </div>
    </div>

<?php FormBox::end() ?>
<?php $box = FormBox::begin() ?>
<?php if(!$model->is_cancel):?>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($cancelModel, 'action_id')->hiddenInput(['value' => $action->id])->label(false) ?>
    <?= $form->field($cancelModel, 'bonus_sum')->hiddenInput(['value' => $action->bonus_sum])->label(false) ?>
    <?= $form->field($cancelModel, 'cancel_text')->textarea() ?>
    <?= $form->field($cancelModel, 'profile_id')->hiddenInput(['value' => $profile->id])->label(false) ?>


    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8"><button type="submit" class="btn btn-primary">Откатить социальные баллы за эту акцию</button></div>
    </div>
    <?php ActiveForm::end(); ?>
<?php else:?>
    <div class="row">
        <div class="col-sm-12"><h4 style="color: red">По этой социальной акции у участника отклюнены баллы</h4></div>
    </div>
    <div class="row">
        <div class="col-sm-2">Причина отмены баллов:</div>
        <div class="col-sm-8"><b><?=$model->cancel_text?></b></div>
    </div>
<?php endif;?>
<?php FormBox::end() ?>

