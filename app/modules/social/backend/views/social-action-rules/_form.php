<?php

use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var modules\social\common\models\SocialActionRules $model
 * @var yz\admin\widgets\ActiveForm $form
 */
$dir = Yii::getAlias(getenv('YII_ENV') == 'dev' ? '@frontendWebroot/data/filemanager/source/' : '@data/filemanager/source/');
FileHelper::createDirectory($dir);
$thumbsDir = $dir = Yii::getAlias(getenv('YII_ENV') == 'dev' ? '@frontendWebroot/data/filemanager/thumbs/' : '@data/filemanager/thumbs/');
FileHelper::createDirectory($thumbsDir);
$css=<<<CSS
a.disabled {
pointer-events: none; /* делаем элемент неактивным для взаимодействия */
cursor: default; /*  курсор в виде стрелки */
color: #888;/* цвет текста серый */
}
CSS;
$this->registerCss($css);

?>

<?php  $box = FormBox::begin(['cssClass' => 'social-action-rules-form box-primary', 'title' => '']) ?>
    <?php $form = ActiveForm::begin(); ?>

    <?php $box->beginBody() ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rules')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'action_text')->widget(\xvs32x\tinymce\Tinymce::className(), [
        'pluginOptions' => [
            'plugins' => [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
            ],
            'toolbar1' => "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
            'toolbar2' => "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor ",
            'image_advtab' => true,
            'filemanager_title' => "Filemanager",
            'language' => ArrayHelper::getValue(explode('_', Yii::$app->language), '0', Yii::$app->language),
        ],
        'fileManagerOptions' => [
            'configPath' => [
                'upload_dir' => '/data/filemanager/source/',
                'current_path' => '../../../../../frontend/web/data/filemanager/source/',
                'thumbs_base_path' => '../../../../../frontend/web/data/filemanager/thumbs/',
                'base_url' => Yii::getAlias('@frontendWeb'), // <-- uploads/filemanager path must be saved in frontend
            ]
        ]
    ]); ?>
        <?php $model->date_from = date('d-m-Y', strtotime($model->date_from))?>
        <?= $form->field($model, 'date_from')->widget(\yii\widgets\MaskedInput::class, [
            'mask' => '99-99-9999',
            'options' => [
                'class' => 'form-control',
                'placeholder' => 'гггг.мм.дд',
            ]
        ])->datePicker(['dateFormat' => 'dd-MM-yyyy']) ?>

        <?php $model->date_to = date('d-m-Y', strtotime($model->date_to))?>
        <?= $form->field($model, 'date_to')->widget(\yii\widgets\MaskedInput::class, [
        'mask' => '99-99-9999',
        'options' => [
            'class' => 'form-control',
            'placeholder' => 'гггг.мм.дд',
        ]
    ])->datePicker(['dateFormat' => 'dd-MM-yyyy']) ?>
    <?= $form->field($model, 'bonus_sum')->textInput() ?>

    <?php if(Yii::$app->controller->action->id == 'update'):?>
        <?= $form->field($model, 'email_is_send')->staticControl(['value' => $model->email_is_send ? 'Да' : 'Нет']) ?>
            <?php if(!$model->email_is_send):?>
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <a  class="btn btn-primary" href="/social/social-action-rules/email-send?action_id=<?=$model->id?>"><i class="glyphicon glyphicon-envelope"></i>&nbsp; Отправить приглашение по Email</a>
                    </div>
                </div>
            <?php endif;?>
        <br/><br/>
        <?= $form->field($model, 'sms_is_send')->staticControl(['value' => $model->sms_is_send ? 'Да' : 'Нет']) ?>

            <?php if(!$model->sms_is_send):?>
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <a  class="btn btn-primary disabled" href="/social/social-action-rules/sms-send?action_id=<?=$model->id?>"><i class="glyphicon glyphicon-earphone"></i>&nbsp; Отправить приглашение по SMS</a>
                        <p style="color:green">Внимание! Отправка смс пока не активна, дабы не отправлять платные смс 10000 фэйковым юзерам</p>
                    </div>
                </div>
            <?php endif;?>
        <br/><br/>
    <?php endif;?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <?php $box->endBody() ?>

    <?php $box->actions([
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
    ]) ?>
    <?php ActiveForm::end(); ?>

<?php  FormBox::end() ?>
