<?php

use yii\helpers\Html;
use yz\admin\widgets\ActiveForm;
use yz\admin\widgets\FormBox;

/**
 * @var yii\web\View $this
 * @var modules\social\backend\models\SocialActionRulesSearch $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<div class="social-action-rules-search hidden" id="filter-search">
    <?php $box = FormBox::begin() ?>
    <?php $box->beginBody() ?>
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'fieldConfig' => [
            'horizontalCssClasses' => ['label' => 'col-sm-3', 'input' => 'col-sm-5', 'offset' => 'col-sm-offset-3 col-sm-5'],
        ],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'rules') ?>

    <?= $form->field($model, 'action_text') ?>

    <?= $form->field($model, 'date_from') ?>

    <?php // echo $form->field($model, 'date_to') ?>

    <?php // echo $form->field($model, 'bonus_sum') ?>

    <?php // echo $form->field($model, 'email_is_send') ?>

    <?php // echo $form->field($model, 'sms_is_send') ?>

    <?php // echo $form->field($model, 'is_active')->radioList([
        '' => \Yii::t('admin/t','All records'),
        '1' => \Yii::t('admin/t','Yes'),
        '0' => \Yii::t('admin/t','No')
    ]) ?>

        <?php  $box->endBody() ?>
        <?php  $box->beginFooter() ?>
            <?= Html::submitButton(\Yii::t('admin/t','Search'), ['class' => 'btn btn-primary']) ?>
        <?php  $box->endFooter() ?>

    <?php ActiveForm::end(); ?>
    <?php  FormBox::end() ?>
</div>
