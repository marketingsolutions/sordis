<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use yz\icons\Icons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\social\backend\models\SocialActionRulesSearch $searchModel
 * @var array $columns
 */

$this->title = modules\social\common\models\SocialActionRules::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'social-action-rules-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [/*['search'],*/ ['export', 'create', /*'delete',*/ 'return']],
            'gridId' => 'social-action-rules-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\social\common\models\SocialActionRules',
        ]) ?>
    </div>

    <?= GridView::widget([
        'id' => 'social-action-rules-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '{update} {index} {xml-upload-action}',
                'buttons'  => [
                    'index' => function ($url, $model) {
                        return Html::a(\yz\icons\Icons::i('eye fa-lg'), ['/social/social-action-user/index', 'action' => $model->id], [
                            'title'     => 'Публикации участников по этой акции',
                            'class'     => 'btn btn-warning btn-sm',
                            'data-pjax' => '0',
                        ]);
                    },
                    'xml-upload-action' => function ($url, $model) {
                        return Html::a(\yz\icons\Icons::i('angle-double-down fa-lg').' XML', ['/social/social-action-rules/xml-upload-action', 'id' => $model->id], [
                            'title'     => 'Скачать результаты акции в XML',
                            'class'     => 'btn btn-warning btn-sm',
                            'data-pjax' => '0',
                        ]);
                    },
                ],
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
