<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use yz\icons\Icons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\social\backend\models\TargetsSearch $searchModel
 * @var array $columns
 */

$this->title = modules\social\common\models\Targets::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'targets-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [/*['search'],*/ ['export', 'delete', 'return']],
            'gridId' => 'targets-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\social\common\models\Targets',
        ]) ?>
    </div>
    <?= GridView::widget([
        'id' => 'targets-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '{index} {update} {delete}',
                'buttons'  => [
                    'index' => function ($url, $model) {
                        return Html::a(\yz\icons\Icons::i('eye fa-lg'), ['/social/target-users/index', 'id' => $model->id], [
                            'title'     => 'Посмотреть участников этой цели',
                            'class'     => 'btn btn-warning btn-sm',
                            'data-pjax' => '0',
                        ]);
                    },
                ],
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
