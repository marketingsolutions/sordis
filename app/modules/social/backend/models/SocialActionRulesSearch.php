<?php

namespace modules\social\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\social\common\models\SocialActionRules;

/**
 * SocialActionRulesSearch represents the model behind the search form about `modules\social\common\models\SocialActionRules`.
 */
class SocialActionRulesSearch extends SocialActionRules implements SearchModelInterface

{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'bonus_sum'], 'integer'],
            [['name', 'rules', 'action_text', 'date_from', 'date_to', 'email_is_send', 'sms_is_send', 'is_active'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        return SocialActionRules::find();
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ]
        ]);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'id' => $this->id,
            'bonus_sum' => $this->bonus_sum,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'rules', $this->rules])
            ->andFilterWhere(['like', 'action_text', $this->action_text])
           // ->andFilterWhere(['between', 'date_from', Yii::$app->formatter->asDate($this->date_from, 'YYYY-MM-d'), Yii::$app->formatter->asDate($this->date_from, 'YYYY-MM-d')])
           // ->andFilterWhere(['between', 'date_to', Yii::$app->formatter->asDate($this->date_to, 'YYYY-MM-d'), Yii::$app->formatter->asDate($this->date_to, 'YYYY-MM-d')])
            ->andFilterWhere(['like', 'email_is_send', $this->email_is_send])
            ->andFilterWhere(['like', 'sms_is_send', $this->sms_is_send])
            ->andFilterWhere(['like', 'is_active', $this->is_active]);

    }
}
