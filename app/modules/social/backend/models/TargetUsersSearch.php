<?php

namespace modules\social\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\social\common\models\TargetUsers;

/**
 * TargetUsersSearch represents the model behind the search form about `modules\social\common\models\TargetUsers`.
 */
class TargetUsersSearch extends TargetUsers implements SearchModelInterface

{

    /**
     * @var
     */
    public $profileName;

    /**
     * @var
     */
    public $profilePhone;

    /**
     * @var
     */
    public $profileRole;

    /**
     * @var
     */
    public $targetName;

    /**
     * @var
     */
    public $targetDescription;

    /**
     * @var
     */
    public $targetSordisId;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'target_id', 'profile_id'], 'integer'],
            [['profileName', 'profilePhone', 'profileRole', 'targetName', 'targetDescription', 'targetSordisId'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        if(\Yii::$app->request->get('id')){
            return TargetUsers::find()->joinWith('profile')->joinWith('target')->where(['{{%target_users}}.target_id' => \Yii::$app->request->get('id')]);
        }
        return TargetUsers::find()->joinWith('profile')->joinWith('target');
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'profileName' => [
                'asc' => ['{{%profiles}}.full_name' => SORT_ASC],
                'desc' => ['{{%profiles}}.full_name' => SORT_DESC]
            ],
            'profilePhone' => [
                'asc' => ['{{%profiles}}.phone_mobile' => SORT_ASC],
                'desc' => ['{{%profiles}}.phone_mobile' => SORT_DESC]
            ],
            'profileRole' => [
                'asc' => ['{{%profiles}}.role' => SORT_ASC],
                'desc' => ['{{%profiles}}.role' => SORT_DESC]
            ],
            'targetName' => [
                'asc' => ['{{%targets}}.name' => SORT_ASC],
                'desc' => ['{{%targets}}.name' => SORT_DESC]
            ],
            'targetDescription' => [
                'asc' => ['{{%targets}}.description' => SORT_ASC],
                'desc' => ['{{%targets}}.description' => SORT_DESC]
            ],
         'targetSordisId' => [
                'asc' => ['{{%targets}}.sordis_id' => SORT_ASC],
                'desc' => ['{{%targets}}.sordis_id' => SORT_DESC]
            ],
        ]);
        $dataProvider->setSort($sort);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'id' => $this->id,
            'target_id' => $this->target_id,
            'profile_id' => $this->profile_id,
        ]);

        $query->andFilterWhere(['like', '{{%profiles}}.full_name', $this->profileName]);
        $query->andFilterWhere(['like', '{{%profiles}}.phone_mobile', $this->profilePhone]);
        $query->andFilterWhere(['like', '{{%profiles}}.role', $this->profileRole]);
        $query->andFilterWhere(['like', '{{%targets}}.name', $this->targetName]);
        $query->andFilterWhere(['like', '{{%targets}}.description', $this->targetDescription]);
        $query->andFilterWhere(['like', '{{%targets}}.sordis_id', $this->targetSordisId]);
    }
}
