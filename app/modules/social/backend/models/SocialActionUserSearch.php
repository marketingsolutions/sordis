<?php

namespace modules\social\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\social\common\models\SocialActionUser;

/**
 * SocialActionUserSearch represents the model behind the search form about `modules\social\common\models\SocialActionUser`.
 */
class SocialActionUserSearch extends SocialActionUser implements SearchModelInterface

{
    /**
     * @var
     */
    public $profileFullname;

    /**
     * @var
     */
    public $profilePhone;

    /**
     * @var
     */
    public $profileRole;

    /**
     * @var
     */
    public $actionName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'action_id', 'profile_id'], 'integer'],
            [['profileFullname', 'profilePhone', 'profileRole', 'comment', 'bonus_sum', 'actionName', 'is_cancel', 'cancel_text'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        if(\Yii::$app->request->get('action')) {
            return SocialActionUser::find()
                ->joinWith('profile')
                ->joinWith('action')
                ->where(['{{%social_action_rules}}.id' => \Yii::$app->request->get('action')]);
        }else{
            return SocialActionUser::find()
                ->joinWith('profile')
                ->joinWith('action');
        }
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ]
        ]);

        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'profileFullname' => [
                'asc' => ['{{%profiles}}.full_name' => SORT_ASC],
                'desc' => ['{{%profiles}}.full_name' => SORT_DESC]
            ],
            'profilePhone' => [
                'asc' => ['{{%profiles}}.phone_mobile' => SORT_ASC],
                'desc' => ['{{%profiles}}.phone_mobile' => SORT_DESC]
            ],
            'profileRole' => [
                'asc' => ['{{%profiles}}.role' => SORT_ASC],
                'desc' => ['{{%profiles}}.role' => SORT_DESC]
            ],
            'actionName' => [
                'asc' => ['{{%social_action_rules}}.name' => SORT_ASC],
                'desc' => ['{{%social_action_rules}}.name' => SORT_DESC]
            ],
        ]);
        $dataProvider->setSort($sort);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'action_id' => $this->action_id,
            'profile_id' => $this->profile_id,
            'is_cancel' => $this->is_cancel,
        ]);
        $query->andFilterWhere(['like', '{{%profiles}}.full_name', $this->profileFullname])
            ->andFilterWhere(['like', '{{%profiles}}.phone_mobile', $this->profilePhone])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'cancel_text', $this->cancel_text])
            ->andFilterWhere(['like', '{{%social_action_rules}}.name', $this->actionName])
            ->andFilterWhere([ '{{%social_action_user}}.id' => $this->id])
            ->andFilterWhere([ '{{%social_action_user}}.bonus_sum' => $this->bonus_sum])
            ->andFilterWhere(['like', '{{%profiles}}.role', $this->profileRole]);

    }
}
