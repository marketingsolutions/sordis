<?php

namespace modules\social\backend\controllers;

use modules\social\backend\forms\UploadTargetsForm;
use Yii;
use modules\social\common\models\UploadsTargetsFile;
use modules\social\backend\models\UploadsTargetsFileSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;
use yii\web\UploadedFile;

/**
 * UploadsTargetsFileController implements the CRUD actions for UploadsTargetsFile model.
 */
class UploadsTargetsFileController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var UploadsTargetsFileSearch $searchModel */
                    return Yii::createObject(UploadsTargetsFileSearch::className());
                },
                'dataProvider' => function($params, UploadsTargetsFileSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all UploadsTargetsFile models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var UploadsTargetsFileSearch $searchModel */
        $searchModel = Yii::createObject(UploadsTargetsFileSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(UploadsTargetsFileSearch $searchModel)
    {
        return [
			'id',
			'original_file_name',
			'file_name',
            [
                'attribute' => 'status_process',
                'filter' => UploadsTargetsFile::getStatus(),
                'format'=>'html',
                'value' => function($data){
                    if($data->status_process == UploadsTargetsFile::STATUS_NEW) {
                        return "<span  style='font-size: 12px;' class='label label-info'>".UploadsTargetsFile::getStatus()[$data->status_process]."</span>";
                    }elseif($data->status_process == UploadsTargetsFile::STATUS_PROCESS){
                        return  "<span style='font-size: 12px;' class='label label-warning'>".UploadsTargetsFile::getStatus()[$data->status_process]."</span>";
                    }elseif($data->status_process == UploadsTargetsFile::STATUS_COMPLETE){
                        return  "<span style='font-size: 12px;' class='label label-success'>".UploadsTargetsFile::getStatus()[$data->status_process]."</span>";
                    }elseif ($data->status_process == UploadsTargetsFile::STATUS_ERROR){
                        return  "<span style='font-size: 12px;' class='label label-danger'>".UploadsTargetsFile::getStatus()[$data->status_process]."</span>";
                    }
                }
            ],
			'created_at:datetime',
        ];
    }

    /**
     * Creates a new UploadsTargetsFile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UploadsTargetsFile;
        $uploadForm = new UploadTargetsForm();
        if (Yii::$app->request->isPost) {
            $uploadForm->xmlFile = UploadedFile::getInstance($uploadForm, 'xmlFile');
            if ($uploadForm->upload()) {
                \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Файл загружен и передан в обработку.'));
                $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'uploadForm' => $uploadForm,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
        ]);
	}

    /**
     * @param $id
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionRollback($id){
        if(!$id){
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_ERROR, \Yii::t('admin/t', 'Отсутствует ID для обработки данной операции'));
            return false;
        }
        $model = UploadsTargetsFile::findOne($id);
        $model->status_process = UploadsTargetsFile::STATUS_NEW;
        $model->update(false);
        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Файл возвращен на обработку'));
        $this->redirect(['index']);
    }


    /**
     * @param array $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the UploadsTargetsFile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UploadsTargetsFile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UploadsTargetsFile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
