<?php

namespace modules\social\backend\controllers;

use modules\profiles\common\models\Profile;
use Yii;
use modules\social\common\models\TargetUsers;
use modules\social\backend\models\TargetUsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * TargetUsersController implements the CRUD actions for TargetUsers model.
 */
class TargetUsersController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var TargetUsersSearch $searchModel */
                    return Yii::createObject(TargetUsersSearch::className());
                },
                'dataProvider' => function($params, TargetUsersSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all TargetUsers models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var TargetUsersSearch $searchModel */
        $searchModel = Yii::createObject(TargetUsersSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(TargetUsersSearch $searchModel)
    {
        return [
			'id',
            [
                'attribute' => 'profileName',
                'label' => 'ФИО участника',
            ],
            [
                'attribute' =>  'profilePhone',
                'label' => 'Телефон участника',
            ],
            [
                'attribute' => 'profileRole',
                'label' => 'Роль участника',
                'filter' => Profile::getDashboardRealRole(),
                'value' => function($data){
                    return Profile::getDashboardRealRole()[$data->profileRole];
                },
            ],
            [
                'attribute' => 'targetSordisId',
                'label' => 'ID цели в системе Sordis'
            ],
            [
                'attribute' => 'targetName',
                'label' => 'Название цели'
            ],
            [
                'attribute' => 'targetDescription',
                'label' => 'Описание цели',
            ],
        ];
    }

    /**
     * Creates a new TargetUsers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TargetUsers;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TargetUsers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
        ]);
	}


    /**
     * Deletes an existing TargetUsers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the TargetUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TargetUsers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TargetUsers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
