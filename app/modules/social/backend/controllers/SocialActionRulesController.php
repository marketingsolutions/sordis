<?php

namespace modules\social\backend\controllers;

use Yii;
use modules\social\common\models\SocialActionRules;
use modules\social\backend\models\SocialActionRulesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;
use yii\helpers\Url;

/**
 * SocialActionRulesController implements the CRUD actions for SocialActionRules model.
 */
class SocialActionRulesController extends Controller /*implements AccessControlInterface*/
{
    use CrudTrait, CheckAccessTrait;



    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var SocialActionRulesSearch $searchModel */
                    return Yii::createObject(SocialActionRulesSearch::className());
                },
                'dataProvider' => function($params, SocialActionRulesSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all SocialActionRules models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var SocialActionRulesSearch $searchModel */
        $searchModel = Yii::createObject(SocialActionRulesSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(SocialActionRulesSearch $searchModel)
    {
        return [
			'id',
			'name',
			'rules:ntext',
//            [
//                'attribute' => 'action_text',
//                'format' => 'html',
//            ],
            [
                'attribute' => 'date_from',
                'format' => ['date', 'php:d.m.Y'],
            ],
			[
			    'attribute' => 'date_to',
                'format' => ['date', 'php:d.m.Y'],
            ],
			'bonus_sum',
			'email_is_send:boolean',
			'sms_is_send:boolean',
			'is_active:boolean',
        ];
    }

    /**
     * Creates a new SocialActionRules model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SocialActionRules;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SocialActionRules model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
        ]);
	}


    /**
     * Deletes an existing SocialActionRules model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param array $id
     * @return Response
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the SocialActionRules model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SocialActionRules the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SocialActionRules::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Отправка приглашений по E-mail
     * @param $action_id
     * @return Response
     */
    public function actionEmailSend($action_id)
    {
        if($action_id && SocialActionRules::sendEmail($action_id)){
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Уведомления по Email поставлены в очередь на отправку'));
            return $this->redirect(Yii::$app->request->referrer);
        }else{
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_WARNING, \Yii::t('admin/t', 'Уведомления не отправлены'));
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Отправка приглашений по SMS
     * @param $action_id
     * @return Response
     */
    public function actionSmsSend($action_id)
    {
        if($action_id && SocialActionRules::sendSms($action_id)){
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Уведомления по SMS поставлены в очередь на отправку'));
            return $this->redirect(Yii::$app->request->referrer);
        }else{
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_WARNING, \Yii::t('admin/t', 'Уведомления не отправлены'));
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * @param $id
     * @return \yii\console\Response|Response
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionXmlUploadAction($id){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
        $uploadArr = SocialActionRules::getArrXml($id);
        return \Yii::$app->response->sendContentAsFile($uploadArr, 'social.xml');
    }
}
