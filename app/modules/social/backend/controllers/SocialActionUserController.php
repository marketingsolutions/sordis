<?php

namespace modules\social\backend\controllers;

use modules\profiles\common\models\Profile;
use modules\social\backend\forms\CancelBonusForm;
use modules\social\common\models\SocialActionRules;
use Yii;
use modules\social\common\models\SocialActionUser;
use modules\social\backend\models\SocialActionUserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * SocialActionUserController implements the CRUD actions for SocialActionUser model.
 */
class SocialActionUserController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var SocialActionUserSearch $searchModel */
                    return Yii::createObject(SocialActionUserSearch::className());
                },
                'dataProvider' => function($params, SocialActionUserSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        /** @var SocialActionUserSearch $searchModel */
        $searchModel = Yii::createObject(SocialActionUserSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    /**
     * @param SocialActionUserSearch $searchModel
     * @return array
     */
    public function getGridColumns(SocialActionUserSearch $searchModel)
    {
        return [
			'id',
            [
                'attribute' => 'profileFullname',
                'label' => 'ФИО участника'
            ],
            [
                'attribute' => 'profilePhone',
                'label' => 'Телефон участника'
            ],
            [
                'attribute' => 'profileRole',
                'label' => 'Роль участника',
                'value' => function($data){
                        return Profile::getDashboardRealRole()[$data->profileRole];
                },
                'filter' => Profile::getDashboardRealRole(),
            ],
            'bonus_sum',
            'comment',
            [
                'attribute' => 'actionName',
                'label' => 'Название акции',
            ],
            [
                'attribute' =>  'is_cancel',
                'label' => 'Отмена баллов по данной акции',
                'format' => 'html',
                'filter' =>[
                    '0'=>'НЕТ',
                    '1' => 'ДА',
                ],
                'value' => function($data){
                    if($data->is_cancel){
                        return "<span class='label label-danger'>ДА</span>";
                    }else{
                        return "<span class='label label-success'>НЕТ</span>";
                    }
                }
            ],
            [
                'attribute' => 'cancel_text',
                'label' => 'Причина отмены баллов',
            ],


        ];
    }

    /**
     * @return string|Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionCreate()
    {
        $model = new SocialActionUser;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionUpdate($id)
    {
        $cancelModel = new CancelBonusForm();
        if ($cancelModel->load(\Yii::$app->request->post()) && $cancelModel->process()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Баллы успешно отменены'));
			//return $this->getCreateUpdateResponse($model);
		}
        $model = $this->findModel($id);
        $profile = Profile::findOne(['id' => $model->profile_id]);
        $action = SocialActionRules::findOne(['id' => $model->action_id ]);
        return $this->render('update', [
            'model' => $model,
            'profile' => $profile,
            'action' => $action,
            'cancelModel' => $cancelModel,

        ]);
	}


    /**
     * Deletes an existing SocialActionUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the SocialActionUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SocialActionUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SocialActionUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
