<?php
namespace modules\social\frontend\controllers;
/**
 * Created by PhpStorm.
 * User: MihailKri
 * Date: 06.11.2018
 * Time: 16:23
 */
use modules\social\common\models\SocialActionRules;
use modules\social\common\models\SocialActionUser;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use ms\loyalty\feedback\ModuleTrait;
use modules\social\common\traits\AttachedFilesProcessingTrait;
use yz\Yz;

class SocialController extends Controller
{

    use ModuleTrait, AttachedFilesProcessingTrait;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $profile = \Yii::$app->user->identity->profile;
        $now = date("Y-m-d");
        $actions = SocialActionRules::find()
            ->where(['is_active' => true])
            ->andWhere("date_from <= '".$now."'")
            ->andWhere("date_to >= '".$now."'")
            ->orderBy(['id' => SORT_DESC])
            ->asArray()
            ->all();
        return $this->render('index', [
            'actions' => $actions,
            'profile' => $profile,
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionAdd($id)
    {
        $profile = \Yii::$app->user->identity->profile;
        $model = new SocialActionUser();
        if ($model->load(\Yii::$app->request->post()) && $model->process()) {
            $this->attachFilesToModel($model, \Yii::$app->request->post());
            $model->refresh();
            \Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Файлы загружены, социальные бонусы успешно начислены');
        }
        $action = SocialActionRules::findOne(['id' => $id]);
        $userModel = SocialActionUser::findOne(['profile_id' => $profile->id, 'action_id' => $action->id ]);
        return $this->render('add',[
            'profile' => $profile,
            'action' => $action,
            'model' => $model,
            'userModel' => $userModel,
        ]);
    }
}