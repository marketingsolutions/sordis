<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use modules\social\common\models\SocialActionUser;

/**
 * Created by PhpStorm.
 * User: MihailKri
 * Date: 07.11.2018
 * Time: 9:22
 */
$this->params['header'] = 'Участие в акции';
$this->params['breadcrumbs'][] = 'Участие в акции';
?>
<div class="row container">
  <h2><?= $action->name ?></h2>

  <div class="panel_ferma oneStock-block">

    <div class="oneStock-block--head">
      <div class="oneStock-block--th1">Правила акции</div>
      <div class="oneStock-block--th2">Кол-во баллов за участие в Призовой акции</div>
      <div class="oneStock-block--th3">Период проведения</div>
    </div>

    <div class="oneStock-block--body">
      <div class="oneStock-block--td1">
        <div class="title-pink sm-hide">Правила акции:</div>
          <?= $action->rules ?>
      </div>
      <div class="oneStock-block--td2">
        <div class="title-pink sm-hide">Социальные бонусы за участие:</div>
          <?= $action->bonus_sum ?>
      </div>
      <div class="oneStock-block--td3">
        <div class="title-pink sm-hide">Период проведения:</div>
          <?php if ($action->date_from && $action->date_to): ?>
            <span>с <?= date("d.m.Y", strtotime($action->date_from)) ?></span>
            <span>по <?= date("d.m.Y", strtotime($action->date_to)) ?></span>
          <?php else: ?>
            <span>Бессрочно</span>
          <?php endif; ?>
      </div>
    </div>

    <div class="oneStock-block--footer">
        <?php if ($action->action_text): ?>
            <div class="title-pink">Подробнее по акции:</div>
            <?= $action->action_text ?>
        <?php endif; ?>

      <?php if (isset($userModel) && $userModel->is_cancel): ?>
        <div class="alert alert-warning">
          <h4><b>Внимание!</b></h4>
          <p><b>Социальные бонусы по этой акции отменены администратором портала</b></p>
          <p><b><span style="text-decoration: underline;">Причина отмены:</span> <?= $userModel->cancel_text ?></b>
          </p>
        </div>
        <hr>
      <?php endif; ?>
      <?php if ($data = SocialActionUser::isParticipantUser($profile->id, $action->id)): ?>
        <div class="row">
          <ul class="thumbnails" style="list-style:none;">
              <?php foreach ($data as $item): ?>
                <div class="col-md-4">
                  <li class="span4">
                    <a href="#" class="thumbnail">
                      <img src="<?php \Yii::getAlias('data') ?>/<?= trim($item['file_path'], '@') . '/' . $item['file_name'] ?>"
                           alt="">
                    </a>
                  </li>
                </div>
              <?php endforeach; ?>
          </ul>
        </div>
        <hr>

        <label class="file-attach--label" for="message-content">Скачать загруженные файлы</label>
        <div class="file-attach">
            <?= \ms\files\attachments\common\widgets\FileAttachmentsView::widget([
                'attachmentsQuery' => \ms\files\attachments\common\models\AttachedFile::find()->where(['profile_id' => $profile->id, 'action_id' => $action->id]),
                'type' => 'userFile',
            ]) ?>
        </div>


      <?php else: ?>
          <?php $form = ActiveForm::begin(['method' => 'post']) ?>
        <div class="file--upload">
          <label class="hide" for="messageform-content">Прикрепить файл</label>
          <?= \ms\files\attachments\common\widgets\FileAttachmentsEdit::widget([
              'model' => $model,
              'options' => [
                  'itemClass' => 'file--uploaded',
              ],
              'maximumFiles' => 5,
              'type' => 'userFile',
          ]) ?>
        </div>



          <?= $form->field($model, 'comment') ?>
          <?= $form->field($model, 'action_id')->hiddenInput(['value' => $action->id])->label(false) ?>
          <?= $form->field($model, 'profile_id')->hiddenInput(['value' => $profile->id])->label(false) ?>
          <?= $form->field($model, 'bonus_sum')->hiddenInput(['value' => $action->bonus_sum])->label(false) ?>

        <div class="oneStock-button">
            <?= Html::submitButton('Добавить фото и получить баллы', ['class' => 'btn btn_all']) ?>
        </div>
    </div>

          <?php ActiveForm::end() ?>

      <?php endif; ?>

  </div>

</div>

