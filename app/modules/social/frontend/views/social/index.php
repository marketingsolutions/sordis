<?php

use modules\social\common\models\SocialActionUser;

/**
 * Created by PhpStorm.
 * User: MihailKri
 * Date: 06.11.2018
 * Time: 16:38
 */
$this->params['header'] = 'Призовые акции';
$this->params['breadcrumbs'][] = 'Призовые акции';
?>

<div class="row container">

  <h2>Текущие акции</h2>
  <?php foreach ($actions as $action): ?>
    <div class="panel_ferma stocks-block">
      <div class="stocks-block--head">
        <div class="stocks-block--th1">Наименование акции</div>
        <div class="stocks-block--th2">Условия акции</div>
        <div class="stocks-block--th3">Дата проведения</div>
        <div class="stocks-block--th4">Детали акции</div>
      </div>

      <div class="stocks-block--body">
        <div class="stocks-block--td1">
          <div class="title-pink sm-hide">Наименование акции:</div>
            <?= $action['name'] ?>
        </div>
        <div class="stocks-block--td2">
          <div class="title-pink sm-hide">Условия акции:</div>
            <?= $action['rules'] ?>
        </div>
        <div class="stocks-block--td3">
          <div class="title-pink sm-hide">Дата проведения:</div>
          <span>c <?= date("d.m.Y", strtotime($action['date_from'])) ?></span>
          <span>по <?= date("d.m.Y", strtotime($action['date_to'])) ?></span>
        </div>
        <div class="stocks-block--td4">
          <div class="title-pink sm-hide">Детали акции:</div>
            <?php if (SocialActionUser::isParticipantUser($profile->id, $action['id'])): ?>
              <a href="/social/social/add?id=<?= $action['id'] ?>" class="btn btn_all">Подробнее</a>
            <?php else: ?>
              <a href="/social/social/add?id=<?= $action['id'] ?>" class="btn btn_all">Участвовать</a>
            <?php endif; ?>
        </div>
      </div>
    </div>
  <?php endforeach; ?>

</div>


