<?php

namespace modules\social\common\models;

use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_targets".
 *
 * @property integer $id
 * @property string $sordis_id
 * @property string $name
 * @property string $description
 * @property string $date_appearance
 * @property string $date_begin
 * @property string $date_end
 *
 * @property TargetUsers[] $targetUsers
 */
class Targets extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%targets}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Targets';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Цели';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['sordis_id', 'string', 'max' => 255],
            ['name', 'string', 'max' => 255],
            ['description', 'string'],
            ['date_appearance', 'safe'],
            ['date_begin', 'safe'],
            ['date_end', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sordis_id' => 'ID цели в системе Sordis',
            'name' => 'Название цели',
            'description' => 'Оисание',
            'date_appearance' => 'Дата начала показа цели в ЛК участника',
            'date_begin' => 'Дата начала',
            'date_end' => 'Дата окончания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTargetUsers()
    {
        return $this->hasMany(TargetUsers::className(), ['target_id' => 'id']);
    }
}
