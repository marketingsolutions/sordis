<?php

namespace modules\social\common\models;

use modules\profiles\common\models\Profile;
use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_target_users".
 *
 * @property integer $id
 * @property integer $target_id
 * @property integer $profile_id
 *
 * @property Profiles $profile
 * @property Targets $target
 */
class TargetUsers extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%target_users}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Target Users';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Цели и их участники';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['target_id', 'integer'],
            ['profile_id', 'integer'],
            [['profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['profile_id' => 'id']],
            [['target_id'], 'exist', 'skipOnError' => true, 'targetClass' => Targets::className(), 'targetAttribute' => ['target_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'target_id' => 'Цель',
            'profile_id' => 'Участник',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }

    /**
     * @return mixed
     */
    public function getProfileName(){
        return $this->profile->full_name;
    }

    /**
     * @return mixed
     */
    public function getProfilePhone(){
        return $this->profile->phone_mobile;
    }

    /**
     * @return mixed
     */
    public function getProfileRole(){
        return $this->profile->role;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarget()
    {
        return $this->hasOne(Targets::className(), ['id' => 'target_id']);
    }

    /**
     * @return string
     */
    public function getTargetName(){
        return $this->target->name;
    }

    /**
     * @return string
     */
    public function getTargetDescription(){
        return $this->target->description;
    }

    /**
     * @return string
     */
    public function getTargetSordisId(){
        return $this->target->sordis_id;
    }

    /**
     * @param $targetId
     * @return bool|string
     */
    public static function getTitleName($targetId){
        return Targets::findOne($targetId) ? Targets::findOne($targetId)->name : false;
    }

    /**
     * Вывод целей в ЛК
     * @param $profileId
     * @return array|bool|\yii\db\ActiveRecord[]
     */
    public static function getUserTargets($profileId){
        $now = date("Y-m-d");
        $actionIdForAll = Targets::find()
            ->select('{{%targets}}.id')
            ->leftJoin('{{%target_users}}', '{{%targets}}.id = {{%target_users}}.target_id')
            ->where('{{%target_users}}.profile_id IS NULL')
            ->column();

        $model = Targets::find()
            ->select('{{%targets}}.name as name_target, {{%targets}}.description as description_target, {{%targets}}.date_begin as from, {{%targets}}.date_end as to')
            ->leftJoin('{{%target_users}}', '{{%targets}}.id = {{%target_users}}.target_id')
            ->where(['{{%target_users}}.profile_id' => $profileId])
            ->orWhere(['{{%targets}}.id' => $actionIdForAll])
            ->andWhere("{{%targets}}.date_appearance <= '".$now."'")
            ->andWhere("{{%targets}}.date_end >= '".$now."'")
            ->asArray()
            ->all();
        if(!$model){
            return false;
        }
        return $model;
    }
}
