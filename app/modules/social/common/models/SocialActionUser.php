<?php

namespace modules\social\common\models;

use modules\bonuses\common\models\TempBonuses;
use modules\profiles\common\models\Profile;
use Yii;
use yii\helpers\FileHelper;
use yz\interfaces\ModelInfoInterface;
use ms\files\attachments\common\contracts\FileAttachmentsOwnerInterface;
use ms\files\attachments\common\models\AttachedFile;
use ms\files\attachments\common\traits\FileAttachmentsTrait;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "yz_social_action_user".
 *
 * @property integer $id
 * @property integer $action_id
 * @property integer $bonus_sum
 * @property integer $profile_id
 * @property integer $is_cancel
 * @property string $comment
 * @property string $created_at
 * @property string $cancel_text
 *
 * @property SocialActionRules $action
 * @property Profiles $profile
 */
class SocialActionUser extends \yii\db\ActiveRecord implements FileAttachmentsOwnerInterface
{
    use FileAttachmentsTrait;


    public $file_local;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%social_action_user}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Social Action User';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Публикации участников';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['action_id', 'integer'],
            ['profile_id', 'integer'],
            ['bonus_sum', 'integer'],
            ['comment', 'string'],
            ['cancel_text', 'string'],
            ['is_cancel', 'integer'],
            ['created_at', 'string'],
            [['action_id'], 'exist', 'skipOnError' => true, 'targetClass' => SocialActionRules::className(), 'targetAttribute' => ['action_id' => 'id']],
            [['profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['profile_id' => 'id']],
            ['file', 'string', 'max' => 255],
            ['file_local', 'file', 'extensions' => ['png', 'jpg', 'jpeg', 'gif', 'pdf'], 'maxSize' => 1024 * 1024 * 10, 'tooBig' => 'Максимальный размер загружаемого файла - 10 МБ'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID публикации',
            'action_id' => 'Action ID',
            'profile_id' => 'Profile ID',
            'comment' => 'Комментарий к публикации',
            'created_at' => 'Дата создания',
            'file' => 'Файл',
            'file_local' => 'Файл',
            'bonus_sum' => 'Сумма начисленных бонусов',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(SocialActionRules::className(), ['id' => 'action_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }

    /**
     * @return mixed
     */
    public function getProfileFullname(){
        return $this->profile->full_name;
    }

    /**
     * @return mixed
     */
    public function getProfilePhone(){
        return $this->profile->phone_mobile;
    }

    /**
     * @return mixed
     */
    public function getProfileRole(){
        return $this->profile->role;
    }

    public function getActionName(){
        return $this->action->name;
    }

    /**
     * @param null $type
     * @return AttachedFile
     */
    public function instantiateAttachedFile($type = null)
    {
        $attachedFile = AttachedFile::getInstance(static::class, $this->id, "@data/social");
        $attachedFile->type = $type;
        return $attachedFile;
    }

    public function process()
    {
        $profileId = \Yii::$app->request->post('SocialActionUser')['profile_id'];
        $actionId = \Yii::$app->request->post('SocialActionUser')['action_id'];
        $files = \Yii::$app->request->post('SocialActionUser')['attachedFiles'];
        $bonus_sum = \Yii::$app->request->post('SocialActionUser')['bonus_sum'];

        $transaction = \Yii::$app->db->beginTransaction();

        //Создаем новую запись о прохождении акции
        $newAction = new self();
        $newAction->profile_id = $profileId;
        $newAction->action_id = $actionId;
        $newAction->bonus_sum = $bonus_sum;
        $newAction->comment = \Yii::$app->request->post('SocialActionUser')['comment'];
        $newAction->created_at = date("Y-m-d H:i:s");
        $newAction->save(false);
        $model_id = $newAction->id;

        //Обновляем инфу по файлам
        foreach ($files as $file){
            $dataAttach = AttachedFile::findOne(['id' => $file]);
            $dataAttach->action_id = $actionId;
            $dataAttach->profile_id = $profileId;
            $dataAttach->model_id = $model_id;
            $dataAttach->update(false);
        }

        //начисляем баллы
        $profile = Profile::findOne(['id' => $profileId]);
        $addBonus = new TempBonuses();
        $addBonus->bonus_type = TempBonuses::SOCIAL_BONUS;
        $addBonus->period = date("Y-m")."-01";
        $addBonus->bonus_sum = $bonus_sum;
        $addBonus->created_at = date("Y-m-d H:i:s");
        $addBonus->phone_mobile = $profile->phone_mobile;
        $addBonus->action_id = $actionId;
        $addBonus->save(false);
        $transaction->commit();

        return true;
    }

    /**
     * Проверяем загрузил ли фото для акции участник
     * @param $profileId
     * @param $actionId
     * @return array|bool|\yii\db\ActiveRecord[]
     */
    public static function isParticipantUser($profileId, $actionId)
    {
        $model = SocialActionUser::findOne(['action_id' => $actionId, 'profile_id' => $profileId]);
        if($model){
            $uploadFile =  AttachedFile::find()->where(['action_id' => $actionId, 'profile_id' => $profileId])->asArray()->all();
            if(!empty($uploadFile)){
                return $uploadFile;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}
