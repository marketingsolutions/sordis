<?php

namespace modules\social\common\models;

use modules\profiles\common\models\Profile;
use modules\sms\common\models\Sms;
use ms\files\attachments\common\models\AttachedFile;
use Yii;
use yz\admin\mailer\common\lists\ManualMailList;
use yz\admin\mailer\common\models\Mail;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_social_action_rules".
 *
 * @property integer $id
 * @property string $name
 * @property string $rules
 * @property string $action_text
 * @property string $date_from
 * @property string $date_to
 * @property integer $bonus_sum
 * @property integer $email_is_send
 * @property integer $sms_is_send
 * @property integer $is_active
 * @property integer $in_news
 *
 * @property AttachedFiles[] $attachedFiles
 * @property SocialActionUser[] $socialActionUsers
 */
class SocialActionRules extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%social_action_rules}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Social Action Rules';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Условия призовых акций';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string', 'max' => 255],
            ['rules', 'string'],
            ['action_text', 'string'],
            [['date_from', 'date_to'], 'safe'],
            ['date_to', 'safe'],
            ['bonus_sum', 'integer'],
            ['in_news',  'integer', 'max' => 1],
            ['email_is_send', 'integer', 'max' => 1],
            ['sms_is_send', 'integer', 'max' => 1],
            ['is_active', 'integer', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID акции',
            'name' => 'Название акции',
            'rules' => 'Условия Акции',
            'action_text' => 'Текст для акции (необязательное поле)',
            'date_from' => 'Дата начала акции',
            'date_to' => 'Дата окончания акции',
            'bonus_sum' => 'Сумма бонусов за выполнение условий',
            'email_is_send' => 'Приглашение отправлено на Email',
            'sms_is_send' => 'Приглашение отправлено по SMS',
            'in_news' => 'Приглашение отобразилось в разделе Новости',
            'is_active' => 'Активная акция или нет',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachedFiles()
    {
        return $this->hasMany(AttachedFile::className(), ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialActionUsers()
    {
        return $this->hasMany(SocialActionUser::className(), ['action_id' => 'id']);
    }

    /**
     * Отправка E-mail приглашений по акции
     * @param $action_id
     * @return bool
     */
    public static function sendEmail($action_id){
        $model = self::findOne(['id' => $action_id]);
        if(!$model){
            return false;
        }
        $profile = Profile::find()->where(['no_send_email' => true])->asArray()->all();
        $arrUser = "";
        foreach ($profile as $user){
            $arrUser .= $user['email'].";";
        }
        $arrUserSend = trim($arrUser, ';');
        $theme = $model->name;
        $rules = $model->rules;
        $dateFrom = $model->date_from;
        $dateTo = $model->date_to;
        $action_text = $model->action_text;
        $transaction = \Yii::$app->db->beginTransaction();
        $send = new Mail();
        $send->status = Mail::STATUS_WAITING;
        $send->receivers_provider = ManualMailList::className();
        $send->receivers_provider_data = json_encode(['to' => $arrUserSend]);
        $send->subject = $model->name;
        $send->body_html = '<div class="row">
                <div class="col-md-12"><h3 align="center">Приглашаем Вас принять участие в социальной акции!</h3></div>
            </div>
            <div class="row">
                <div class="col-md-4">Тема акции:</div>
                <div class="col-md-8">'.$theme.'</div>
            </div>
            <div class="row">
                <div class="col-md-4">Правила акции:</div>
                <div class="col-md-8">'.$rules.'</div>
            </div>
                <div class="row">
                    <div class="col-md-4">Описание акции:</div>
                    <div class="col-md-8">'.$action_text.'</div>
                </div>
            <div class="row">
                <div class="col-md-4">Период проведения:</div>
                <div class="col-md-8"> C '.$dateFrom.' по '.$dateTo.'</div>
            </div>
            <div class="row">
                
                <div class="col-md-12">Для того, чтобы более не получать письма автоматической E-mail рассылки пройдите по <a href="'.getenv('FRONTEND_WEB').'profiles/profile/index">этой ссылке</a> и отключите её в настройках</div>
            </div>';
        $send->created_at = date("Y-m-d H:i:s");
        $send->save(false);
        $model->email_is_send = 1;
        $model->update(false);
        $send->updateAttributes(['status' => Mail::STATUS_WAITING]);
        $transaction->commit();
        return true;
    }

    /**
     * Отправка SMS - приглашений к акции
     * @param $action_id
     * @return bool
     */
    public static function sendSms($action_id){
        $model = self::findOne(['id' => $action_id]);
        if(!$model){
            return false;
        }
        $profile = Profile::find()->asArray()->all();
        $arrUser = "";
        foreach ($profile as $user){
            $arrUser .= $user['phone_mobile']."; ";
        }

        $transaction = \Yii::$app->db->beginTransaction();

        $arrUserSend = trim($arrUser, '; ');
        $newSms = new Sms();
        $newSms->status = Sms::STATUS_NEW;
        $newSms->type = Sms::TYPE_INDIVIDUAL;
        $newSms->to = $arrUserSend;
        $newSms->message = "Приглашаем принять участие в социальной акции. Подробности на портале ".getenv('FRONTEND_WEB');
        $newSms->created_at = date("Y-m-d H:i:s");
        $newSms->save(false);
        $model->updateAttributes(['sms_is_send' => true]);

        $transaction->commit();
        return true;
    }

    /**
     * Выгрузка в xml социальных акций
     * @param $socialActionId
     * @return string
     */
    public static function getArrXml($socialActionId){
        $actions = SocialActionUser::find()
            ->select('{{%social_action_rules}}.name as ActionName, {{%social_action_rules}}.rules as ActionRule, {{%social_action_rules}}.date_from ActionDateFrom, {{%social_action_rules}}.date_to as ActionDateTo, {{%profiles}}.phone_mobile as User, {{%social_action_user}}.action_id as ActionId, {{%social_action_user}}.bonus_sum as SocialActionPoints, {{%social_action_user}}.is_cancel as IsCancel, {{%social_action_user}}.cancel_text as IsCancelComment')
            ->leftJoin('{{%profiles}}', '{{%profiles}}.id={{%social_action_user}}.profile_id')
            ->leftJoin('{{%social_action_rules}}', '{{%social_action_rules}}.id = {{%social_action_user}}.action_id')
            ->where(['{{%social_action_user}}.action_id' => $socialActionId])
            ->asArray()
            ->all();

        $xml = new \DOMDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xmlSocialActions = $xml->createElement('SocialActions');
        $xml->appendChild($xmlSocialActions);

        foreach ($actions as $action){
            $xmlSocialActionUsers = $xml->createElement('SocialActionUsers');
            $xmlSocialActions->appendChild($xmlSocialActionUsers);

            $xmlUser = $xml->createElement('User' , trim($action['User'], "+"));
            $xmlSocialActionUsers->appendChild($xmlUser);

            $xmlActionId = $xml->createElement('ActionId' , $action['ActionId']);
            $xmlSocialActionUsers->appendChild($xmlActionId);

            $xmlActionName = $xml->createElement('ActionName' , $action['ActionName']);
            $xmlSocialActionUsers->appendChild($xmlActionName);

            $xmlActionRule = $xml->createElement('ActionRule' , $action['ActionRule']);
            $xmlSocialActionUsers->appendChild($xmlActionRule);


            $dateFrom = date("Y-m-d" , strtotime($action['ActionDateFrom']));
            $dateTo = date("Y-m-d", strtotime($action['ActionDateTo']));
            $xmlActionDateFrom = $xml->createElement('ActionDateFrom' , $dateFrom);
            $xmlSocialActionUsers->appendChild($xmlActionDateFrom);

            $xmlActionDateTo = $xml->createElement('ActionDateTo' , $dateTo);
            $xmlSocialActionUsers->appendChild($xmlActionDateTo);

            $xmlSocialActionPoints = $xml->createElement('SocialActionPoints' , $action['SocialActionPoints']);
            $xmlSocialActionUsers->appendChild($xmlSocialActionPoints);

            $xmlIsCancel = !empty($action['IsCancel']) ? $xml->createElement('IsCancel' , $action['SocialActionPoints']) : $xml->createElement('IsCancel');
            $xmlSocialActionUsers->appendChild($xmlIsCancel);

            $xmlIsCancelComment = !empty($action['IsCancelComment']) ? $xml->createElement('IsCancelComment' , $action['IsCancelComment']) : $xml->createElement('IsCancelComment');
            $xmlSocialActionUsers->appendChild($xmlIsCancelComment);
        }

        return $xml->saveXML();
    }

    /**
     * Выгрузка в xml социальных акций для отправки по ftp
     * @param $socialActionId
     * @return \DOMDocument
     */
    public static function getArrXmlInConsole($socialActionId){
        $actions = SocialActionUser::find()
            ->select('{{%social_action_rules}}.name as ActionName, {{%social_action_rules}}.rules as ActionRule, {{%social_action_rules}}.date_from ActionDateFrom, {{%social_action_rules}}.date_to as ActionDateTo, {{%profiles}}.phone_mobile as User, {{%social_action_user}}.action_id as ActionId, {{%social_action_user}}.bonus_sum as SocialActionPoints, {{%social_action_user}}.is_cancel as IsCancel, {{%social_action_user}}.cancel_text as IsCancelComment')
            ->leftJoin('{{%profiles}}', '{{%profiles}}.id={{%social_action_user}}.profile_id')
            ->leftJoin('{{%social_action_rules}}', '{{%social_action_rules}}.id = {{%social_action_user}}.action_id')
            ->where(['{{%social_action_user}}.action_id' => $socialActionId])
            ->asArray()
            ->all();

        $xml = new \DOMDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xmlSocialActions = $xml->createElement('SocialActions');
        $xml->appendChild($xmlSocialActions);

        foreach ($actions as $action){
            $xmlSocialActionUsers = $xml->createElement('SocialActionUsers');
            $xmlSocialActions->appendChild($xmlSocialActionUsers);

            $xmlUser = $xml->createElement('User' , trim($action['User'], "+"));
            $xmlSocialActionUsers->appendChild($xmlUser);

            $xmlActionId = $xml->createElement('ActionId' , $action['ActionId']);
            $xmlSocialActionUsers->appendChild($xmlActionId);

            $xmlActionName = $xml->createElement('ActionName' , $action['ActionName']);
            $xmlSocialActionUsers->appendChild($xmlActionName);

            $xmlActionRule = $xml->createElement('ActionRule' , $action['ActionRule']);
            $xmlSocialActionUsers->appendChild($xmlActionRule);


            $dateFrom = date("Y-m-d" , strtotime($action['ActionDateFrom']));
            $dateTo = date("Y-m-d", strtotime($action['ActionDateTo']));
            $xmlActionDateFrom = $xml->createElement('ActionDateFrom' , $dateFrom);
            $xmlSocialActionUsers->appendChild($xmlActionDateFrom);

            $xmlActionDateTo = $xml->createElement('ActionDateTo' , $dateTo);
            $xmlSocialActionUsers->appendChild($xmlActionDateTo);

            $xmlSocialActionPoints = $xml->createElement('SocialActionPoints' , $action['SocialActionPoints']);
            $xmlSocialActionUsers->appendChild($xmlSocialActionPoints);

            $xmlIsCancel = !empty($action['IsCancel']) ? $xml->createElement('IsCancel' , $action['SocialActionPoints']) : $xml->createElement('IsCancel');
            $xmlSocialActionUsers->appendChild($xmlIsCancel);

            $xmlIsCancelComment = !empty($action['IsCancelComment']) ? $xml->createElement('IsCancelComment' , $action['IsCancelComment']) : $xml->createElement('IsCancelComment');
            $xmlSocialActionUsers->appendChild($xmlIsCancelComment);
        }

        return $xml;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $this->date_from = date('Y-m-d', strtotime($this->date_from));
            $this->date_to = date('Y-m-d', strtotime($this->date_to));

            return true;
        }
        return false;
    }
}
