<?php
namespace modules\social\common\traits;
use ms\files\attachments\common\contracts\FileAttachmentsOwnerInterface;
use ms\files\attachments\common\models\AttachedFile;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


/**
 * Trait AttachedFilesProcessingTrait
 */
trait AttachedFilesProcessingTrait
{
    public function attachFiles(FileAttachmentsOwnerInterface $model, $files)
    {
        $baseAttachedFile = $model->instantiateAttachedFile(null);
        $query = AttachedFile::find()->where(['id' => $files]);
        foreach ($query->each() as $attachedFile) {
            /** @var AttachedFile $attachedFile */
            if (
                $attachedFile->model_class == $baseAttachedFile->model_class
                && $attachedFile->model_id == $baseAttachedFile->model_id
            ) {
                continue;
            }
            $attachedFile->model_class = $baseAttachedFile->model_class;
            $attachedFile->model_id = $baseAttachedFile->model_id;
            $attachedFile->changeFilePath($baseAttachedFile->file_path);
            $attachedFile->updateAttributes(['model_class', 'model_id', 'file_path']);
        }

        // Delete all other files
        $query = AttachedFile::find()
            ->where([
                'model_class' => $baseAttachedFile->model_class,
                'model_id' => $baseAttachedFile->model_id,
                'action_id' =>NULL,
                'profile_id' =>NULL,
            ])
                ->orWhere([
                'action_id' =>NULL,
                'profile_id' =>NULL,
    ]);
        foreach ($query->each() as $attachedFile) {
            /** @var AttachedFile $attachedFile */
            $attachedFile->delete();
        }
    }

    /**
     * @param FileAttachmentsOwnerInterface $model
     * @param $params
     * @deprecated Use attachFiles method
     */
    public function attachFilesToModel(FileAttachmentsOwnerInterface $model, $params)
    {
        if ($model instanceof Model) {
            $name = $model->formName() . '.attachedFiles';
        } else {
            $name = 'attachedFiles';
        }
        $files = ArrayHelper::getValue($params, $name, []);

        $this->attachFiles($model, $files);
    }
}