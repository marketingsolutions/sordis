<?php
namespace modules\ftp\console\controllers;


use console\base\Controller;
use common\utils\Ftp;
use modules\bonuses\common\models\Bonus;
use modules\bonuses\common\models\UploadBonusFile;
use modules\polls\common\models\Poll;
use modules\polls\common\models\UploadPollsFile;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\ProfileConfirmRtt;
use modules\sales\common\models\UploadSalePointDistrib;
use modules\social\common\models\SocialActionRules;
use modules\social\common\models\UploadsTargetsFile;
use yii\helpers\FileHelper;
use yii\web\Response;
use modules\logs\common\models\XmlLogs;

class FtpExchangeController extends Controller
{

   const FILE_TYPE_BONUS = 'bonus';
   const FILE_TYPE_POLLS = 'poll';
   const FILE_TYPE_TARGET = 'target';
   const FILE_TYPE_SOCIAL = 'social';
   const FILE_TYPE_RTT_DISTRIB = 'rtt_distrib';
   const FILE_TYPE_REG_RTT = 'rtt_registred';
   const FILE_TYPE_REG_DISTRIB = 'distrib_registred';

   //Папки для забора файлов для загрузки на портал
   const REMOTE_DIR_OUT_BONUS = 'Score';  //Баллы от Сордис
   const REMOTE_DIR_OUT_POLLS  = 'Quiz';  //Опросы от Сордис
   const REMOTE_DIR_OUT_TARGETS = 'Target'; //Цели от Сордис
   const REMOTE_DIR_OUT_SOCIAL = 'Social'; //Социальные (призовые) от Сордис
   const REMOTE_DIR_RTT_OUT_DISTRIB = 'Base'; //Список РТТ и Дистров от Сордис
   const REMOTE_DIR_REG_RTT = 'Base'; //Список РТТ и Дистров от Сордис
   const REMOTE_DIR_REG_DISTRIB = 'Base'; //Список РТТ и Дистров от Сордис

    public function GetFileTypeValue(){
        return [
            self::FILE_TYPE_BONUS => 'Бонусные баллы',
            self::FILE_TYPE_POLLS => 'Опросы',
            self::FILE_TYPE_TARGET => 'Цели',
            self::FILE_TYPE_SOCIAL => 'Социальные акции',
            self::FILE_TYPE_RTT_DISTRIB => 'РТТ-Дистрибьюторы',
            self::FILE_TYPE_REG_RTT => 'Зарегистрировавшиеся участники РТТ',
            self::FILE_TYPE_REG_DISTRIB => 'Зарегистрировавшиеся дисрибьюторы',
        ];
    }

    public $profilePhone = '';


    /**
     * Список файлов в таблице бонусов
     */
    public function actionGetBonusXmlFileSpisok(){
        $files = $this->scanRemoteDir(self::REMOTE_DIR_OUT_BONUS);
        var_dump($files);
    }

    /**
     * Получаем файлы с бонусами , обрабатываем и отправляем в архив
     * @throws \yii\base\Exception
     */
    public function actionGetBonusXmlFile(){
        if($this->uploadXmlFiles(self::FILE_TYPE_BONUS,"Out/".self::REMOTE_DIR_OUT_BONUS, "Out/Arc/".self::REMOTE_DIR_OUT_BONUS)){
            echo "Файлы перемещены в директорию !!!";
        }else{
            echo "Что то пошло не так!";
        }
    }



    /**
     * Получаем файлы с опросами, обрабатываем и отправляем в архив
     * @throws \yii\base\Exception
     */
    public function actionGetPollsXmlFile(){
        if($this->uploadXmlFiles(self::FILE_TYPE_POLLS,"Out/".self::REMOTE_DIR_OUT_POLLS, "Out/Arc/".self::REMOTE_DIR_OUT_POLLS)){
            echo "Файлы перемещены в директорию !!!";
        }else{
            echo "Что то пошло не так!";
        }
    }

    /**
     * Получаем файлы с целями, обрабатываем и отправляем в архив
     * @throws \yii\base\Exception
     */
    public function actionGetTargetXmlFile(){
        if($this->uploadXmlFiles(self::FILE_TYPE_TARGET,"Out/".self::REMOTE_DIR_OUT_TARGETS, "Out/Arc/".self::REMOTE_DIR_OUT_TARGETS)){
            echo "Файлы перемещены в директорию !!!";
        }else{
            echo "Что то пошло не так!";
        }
    }

    /**
     * Получаем файлы со списком РТТ, Дистров и Менеджеров ТО
     * @throws \yii\base\Exception
     */
    public function actionGetRttDistribXmlFile(){
        if($this->uploadXmlFiles(self::FILE_TYPE_RTT_DISTRIB,"Out/".self::REMOTE_DIR_RTT_OUT_DISTRIB, "Out/Arc/".self::REMOTE_DIR_RTT_OUT_DISTRIB)){
            echo "Файлы перемещены в директорию !!!";
        }else{
            echo "Что то пошло не так!";
        }
    }

    /**
     * Отправляем бонусы по ftp
     * @throws \yii\base\Exception
     */
    public function actionPutBonusFtpFile(){
        $path = $this->getLocalPutPath(self::FILE_TYPE_BONUS);
        $file = $this->getBonusFile();
        if($file && $this->putOneXmlFile("In/".self::REMOTE_DIR_OUT_BONUS.DIRECTORY_SEPARATOR.$file,  $this->getLocalPutPath(self::FILE_TYPE_BONUS).DIRECTORY_SEPARATOR.$file)){
            $message = 'Файл '.$file.' с потраченными бонусами успешно отправлен на удаленный FTP-сервер.';
            XmlLogs::addMessage( XmlLogs::TYPE_MESSAGE_INFO,  'Отправка потраченных бонусов на FTP', $message);
            echo "Файл успешно отправлен на ftp сервер!";
        }else{
            $message = 'Ошибка при отправке файла с потраченными бонусами! Файл '.$file.' с потраченными бонусами не отправлен на удаленный FTP-сервер.';
            XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Отправка потраченных бонусов на FTP', $message);
            echo "Что то пошло не так!";
        }
    }

    /**
     * Отправляем новых зарегенных участников-дистрибьюторов на ftp - обмен  в Сордис
     */
    public function actionPutNewUserDistrib()
    {
        $file = $this->getDistribUserFile();

        if($file && $this->getLocalPutPath(self::FILE_TYPE_REG_DISTRIB) && $this->putOneXmlFile("In/".self::REMOTE_DIR_REG_DISTRIB.DIRECTORY_SEPARATOR.$file, $this->getLocalPutPath(self::FILE_TYPE_REG_DISTRIB).DIRECTORY_SEPARATOR.$file)){
            $message = 'Файл '.$file.' с новыми зарегистрированными участниками-дистрибьюторами успешно отправлен на удаленный FTP-сервер.';
            XmlLogs::addMessage( XmlLogs::TYPE_MESSAGE_INFO,  'Отправка новых зарегистрированных участников-дистрибьюторов на FTP', $message);
            echo "Файл успешно отправлен на ftp сервер!";
        }else{
            $message = 'Ошибка при отправке файла с новыми зарегистрированными участниками-дистрибьюторами! Файл '.$file.' с с новыми зарегистрированными участниками-дистрибьюторами пуст и не отправлен на удаленный FTP-сервер.';
            XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Отправка новых зарегистрированных участников-дистрибьюторов на FTP', $message);
            echo "Что то пошло не так!";
        }

    }

    /**
     * Отправляем новых зарегенных участников-ТТ на ftp - обмен  в Сордис
     */
    public function actionPutNewUserTt()
    {
        $file = $this->getRttUserFile();

        if($file && $this->getLocalPutPath(self::FILE_TYPE_REG_RTT) && $this->putOneXmlFile("In/".self::REMOTE_DIR_REG_RTT.DIRECTORY_SEPARATOR.$file, $this->getLocalPutPath(self::FILE_TYPE_REG_RTT).DIRECTORY_SEPARATOR.$file)){
            $message = 'Файл '.$file.' с новыми зарегистрированными участниками-ртт успешно отправлен на удаленный FTP-сервер.';
            XmlLogs::addMessage( XmlLogs::TYPE_MESSAGE_INFO,  'Отправка новых зарегистрированных участников-ртт на FTP', $message);
            echo "Файл успешно отправлен на ftp сервер!";
        }else{
            $message = 'Ошибка при отправке файла с новыми зарегистрированными участниками-ртт! Файл '.$file.' с с новыми зарегистрированными участниками-ртт пуст и не отправлен на удаленный FTP-сервер.';
            XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Отправка новых зарегистрированных участников-ртт на FTP', $message);
            echo "Что то пошло не так!";
        }
    }

    public function getNewUsersTtFile()
    {
        //Забераем данные с новыми участниками ТТ
        $profiles = Profile::find()
            ->select('{{%profiles}}.id as ids,{{%profiles}}.phone_mobile as Participant, {{%profiles}}.full_name as ParticipantFullName, {{%profiles}}.created_at as ParticipantDateRegister, 
            {{%profiles}}.role as ParticipantRole, {{%profiles}}.email as ParticipantEmail, {{%sale_point}}.name as SalesPointName, {{%sale_point}}.address as SalesPointAddress, 
            {{%sale_point}}.code_egais as SalesPointEgais, {{%sale_point}}.inn as SalesPointInn')
            ->leftJoin('{{%sale_point_profile}}', '{{%profiles}}.id = {{%sale_point_profile}}.profile_id')
            ->leftJoin('{{%sale_point}}', '{{%sale_point}}.id = {{%sale_point_profile}}.sale_point_id')
            ->where(['{{%profiles}}.role' =>Profile::ROLE_MANAGER_TT])
            ->andWhere(['{{%profiles}}.is_xml_upload' => false])
            ->asArray()
            ->all();

        //Забираем все добавленные/удаленные ТТ по заявкам для добавления в xml
        $deleteConfirnRtt = ProfileConfirmRtt::find()
            ->select('{{%profile_confirm_rtt}}.id, {{%profiles}}.phone_mobile, {{%profile_confirm_rtt}}.type, {{%profile_confirm_rtt}}.code_egais, {{%profile_confirm_rtt}}.inn, {{%profile_confirm_rtt}}.name, {{%profile_confirm_rtt}}.address, {{%profile_confirm_rtt}}.add_delele_at')
            ->leftJoin('{{%profiles}}', '{{%profiles}}.id={{%profile_confirm_rtt}}.profile_id')
            ->where(['{{%profile_confirm_rtt}}.is_xml_upload' => false])
            ->andWhere("{{%profile_confirm_rtt}}.add_delele_at is not NULL")
            ->andWhere(["!=", "{{%profile_confirm_rtt}}.status", ProfileConfirmRtt::STATUS_NEW])
            ->andWhere(["!=", "{{%profile_confirm_rtt}}.status", ProfileConfirmRtt::STATUS_CANCEL])
            ->asArray()
            ->all();

        if(empty($profiles) && empty($deleteConfirnRtt)){
            return false;
        }
        // формируем xml
        $xml = new \DOMDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xmlObmenData = $xml->createElement('ObmenData');
        $xml->appendChild($xmlObmenData);

        $profilesIdArr = [];
        $addDeleteRttArr = [];

        foreach ($profiles as $new_user){
            $profilesIdArr[] = $new_user['ids'];
            if ($this->profilePhone != $new_user['Participant']) {
                $xmlObmenParticipantData = $xml->createElement('ObmenParticipantData');
                $xmlObmenData->appendChild($xmlObmenParticipantData);

                $xmlParticipant = $xml->createElement('Participant', $new_user['Participant']);
                $xmlObmenParticipantData->appendChild($xmlParticipant);

                $xmlParticipantFullName = $xml->createElement('ParticipantFullName', $new_user['ParticipantFullName']);
                $xmlObmenParticipantData->appendChild($xmlParticipantFullName);

                $xmlParticipantDateRegister = $xml->createElement('ParticipantDateRegister', $new_user['ParticipantDateRegister']);
                $xmlObmenParticipantData->appendChild($xmlParticipantDateRegister);

                $xmlParticipantEmail = $xml->createElement('ParticipantEmail', $new_user['ParticipantEmail']);
                $xmlObmenParticipantData->appendChild($xmlParticipantEmail);

                $xmlParticipantRole = $xml->createElement('ParticipantRole', $new_user['ParticipantRole']);
                $xmlObmenParticipantData->appendChild($xmlParticipantRole);

                $this->profilePhone = $new_user['Participant'];

                $xmlSalesPoints = $xml->createElement('SalesPoints');
                $xmlObmenParticipantData->appendChild($xmlSalesPoints);

                $xmlSalesPoint = $xml->createElement('SalesPoint');
                $xmlSalesPoints->appendChild($xmlSalesPoint);

                $xmlSalesPointName = $xml->createElement('SalesPointName', $new_user['SalesPointName']);
                $xmlSalesPoint->appendChild($xmlSalesPointName);

                $xmlSalesPointAddress = $xml->createElement('SalesPointAddress', htmlspecialchars ($new_user['SalesPointAddress']));
                $xmlSalesPoint->appendChild($xmlSalesPointAddress);

                $xmlSalesPointEgais = $xml->createElement('SalesPointEgais', $new_user['SalesPointEgais']);
                $xmlSalesPoint->appendChild($xmlSalesPointEgais);

                $xmlSalesPointInn = $xml->createElement('SalesPointInn', $new_user['SalesPointInn']);
                $xmlSalesPoint->appendChild($xmlSalesPointInn);
            } else {
                $xmlSalesPoint = $xml->createElement('SalesPoint');
                $xmlSalesPoints->appendChild($xmlSalesPoint);
                $xmlSalesPointName = $xml->createElement('SalesPointName', $new_user['SalesPointName']);
                $xmlSalesPoint->appendChild($xmlSalesPointName);
                $xmlSalesPointAddress = $xml->createElement('SalesPointAddress', htmlspecialchars ($new_user['SalesPointAddress']));
                $xmlSalesPoint->appendChild($xmlSalesPointAddress);
                $xmlSalesPointEgais = $xml->createElement('SalesPointEgais', $new_user['SalesPointEgais']);
                $xmlSalesPoint->appendChild($xmlSalesPointEgais);
                $xmlSalesPointInn = $xml->createElement('SalesPointInn', $new_user['SalesPointInn']);
                $xmlSalesPoint->appendChild($xmlSalesPointInn);
            }
        }
        $xmlChangePoints = $xml->createElement('ChangePoints');
        $xmlObmenData->appendChild($xmlChangePoints);

        foreach ($deleteConfirnRtt as $rttDelConf){
            $addDeleteRttArr[]=$rttDelConf['id'];

            $xmlChangePoint = $xml->createElement('ChangePoint');
            $xmlChangePoints->appendChild($xmlChangePoint);

            $xmlParticipant = $xml->createElement('Participant', $rttDelConf['phone_mobile']);
            $xmlChangePoint->appendChild($xmlParticipant);

            //В зависимости от того, добавление это или удаление
            if($rttDelConf['type'] == ProfileConfirmRtt::TYPE_ADD){
                $xmlAddDellPoint = $xml->createElement('AddPoint');

            }else{
                $xmlAddDellPoint = $xml->createElement('DelPoint');
            }
            $xmlChangePoint->appendChild($xmlAddDellPoint);

            $xmlFSRARID = $xml->createElement('FSRARID', $rttDelConf['code_egais']);
            $xmlAddDellPoint->appendChild($xmlFSRARID);

            $xmlINN = $xml->createElement('INN', $rttDelConf['inn']);
            $xmlAddDellPoint->appendChild($xmlINN);

            $xmlName = $xml->createElement('Name', $rttDelConf['name']);
            $xmlAddDellPoint->appendChild($xmlName);

            $xmlAddress = $xml->createElement('Address', $rttDelConf['address']);
            $xmlAddDellPoint->appendChild($xmlAddress);

            if($rttDelConf['type'] == ProfileConfirmRtt::TYPE_ADD){
                $xmlDateAddDell = $xml->createElement('DateAdd', $rttDelConf['add_delele_at']);
            }else{
                $xmlDateAddDell = $xml->createElement('DateDel', $rttDelConf['add_delele_at']);
            }
            $xmlAddDellPoint->appendChild($xmlDateAddDell);
        }
        //Обновляем признак отправки (поле is_xml_upload) для исключения повторной отправки участников
        $now = date("Y-m-d H:i:s");
        Profile::updateAll(['is_xml_upload'=>1, 'is_xml_upload_at' => $now], ['id'=>$profilesIdArr]);
        ProfileConfirmRtt::updateAll(['is_xml_upload'=>1, 'is_xml_upload_at' => $now], ['id'=>$addDeleteRttArr]);
        return $xml;
    }


    public function getNewUsersDistribFile()
    {
        //Забираем новых зарегенных юзеров
        $profiles = Profile::find()
            ->select('{{%profiles}}.id as ids,{{%profiles}}.phone_mobile as Participant, {{%profiles}}.full_name as ParticipantFullName, {{%profiles}}.created_at as ParticipantDateRegister, 
            {{%profiles}}.role as ParticipantRole, {{%profiles}}.email as ParticipantEmail, {{%distributor}}.distrib_name as DistributorName, {{%distributor}}.distrib_address as DistributorAddress, 
            {{%distributor}}.egais as DistributorEgais, {{%distributor}}.inn as DistributorInn')
            ->leftJoin('{{%distributor_profile}}', '{{%profiles}}.id = {{%distributor_profile}}.profile_id')
            ->leftJoin('{{%distributor}}', '{{%distributor}}.id = {{%distributor_profile}}.distrib_id')
            ->where(['{{%profiles}}.role' =>Profile::ROLE_DISTRIBUTOR])
            ->andWhere(['{{%profiles}}.is_xml_upload' => false])
            ->asArray()
            ->all();
        if(empty($profiles)){
            return false;
        }
        // формируем xml
        $xml = new \DOMDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xmlObmenData = $xml->createElement('ObmenData');
        $xml->appendChild($xmlObmenData);

        //Массив с ID профилей для добавления признака отправки новых юзеров
        $profilesIdArr = [];
        foreach ($profiles as $new_user) {

            $profilesIdArr[] = $new_user['ids'];

            if ($this->profilePhone != $new_user['Participant']) {
                $xmlObmenParticipantData = $xml->createElement('ObmenParticipantData');
                $xmlObmenData->appendChild($xmlObmenParticipantData);
                $xmlParticipant = $xml->createElement('Participant', $new_user['Participant']);
                $xmlObmenParticipantData->appendChild($xmlParticipant);
                $xmlParticipantFullName = $xml->createElement('ParticipantFullName', $new_user['ParticipantFullName']);
                $xmlObmenParticipantData->appendChild($xmlParticipantFullName);
                $xmlParticipantDateRegister = $xml->createElement('ParticipantDateRegister', $new_user['ParticipantDateRegister']);
                $xmlObmenParticipantData->appendChild($xmlParticipantDateRegister);
                $xmlParticipantEmail = $xml->createElement('ParticipantEmail', $new_user['ParticipantEmail']);
                $xmlObmenParticipantData->appendChild($xmlParticipantEmail);
                $xmlParticipantRole = $xml->createElement('ParticipantRole', $new_user['ParticipantRole']);
                $xmlObmenParticipantData->appendChild($xmlParticipantRole);

                $this->profilePhone = $new_user['Participant'];
                $xmlDistributors = $xml->createElement('Distributors');
                $xmlObmenParticipantData->appendChild($xmlDistributors);
                $xmlDistributor = $xml->createElement('Distributor');
                $xmlDistributors->appendChild($xmlDistributor);
                $xmlDistributorName = $xml->createElement('DistributorName', $new_user['DistributorName']);
                $xmlDistributor->appendChild($xmlDistributorName);
                $xmlDistributorAddress = $xml->createElement('DistributorAddress', $new_user['DistributorAddress']);
                $xmlDistributor->appendChild($xmlDistributorAddress);
                $xmlDistributorEgais = $xml->createElement('DistributorEgais', $new_user['DistributorEgais']);
                $xmlDistributor->appendChild($xmlDistributorEgais);
                $xmlDistributorInn = $xml->createElement('DistributorInn', $new_user['DistributorInn']);
                $xmlDistributor->appendChild($xmlDistributorInn);
            } else {
                $xmlDistributor = $xml->createElement('Distributor');
                $xmlDistributors->appendChild($xmlDistributor);
                $xmlDistributorName = $xml->createElement('DistributorName', $new_user['DistributorName']);
                $xmlDistributor->appendChild($xmlDistributorName);
                $xmlDistributorAddress = $xml->createElement('DistributorAddress', $new_user['DistributorAddress']);
                $xmlDistributor->appendChild($xmlDistributorAddress);
                $xmlDistributorEgais = $xml->createElement('DistributorEgais', $new_user['DistributorEgais']);
                $xmlDistributor->appendChild($xmlDistributorEgais);
                $xmlDistributorInn = $xml->createElement('DistributorInn', $new_user['DistributorInn']);
                $xmlDistributor->appendChild($xmlDistributorInn);
            }
        }

        //Обновляем признак отправки (поле is_xml_upload) для исключения повторной отправки участников
        Profile::updateAll(['is_xml_upload'=>1], ['id'=>$profilesIdArr]);
        return $xml;
    }

    /**
     * Отправляем опросы по ftp
     * @throws \yii\base\Exception
     */
    public function actionPutPollsFtpFile(){
        if($this->getPollInXml()){
            echo "Файлы удачно сформированы и  отправлены на сервер";
        }else{
            echo "Что то пошло не так!";
        }
    }

    /**
     * Отправляем социальные акции по ftp
     * @throws \yii\base\Exception
     */
    public function actionPutSocialFtpFile(){
        if($this->getSocialInXml()){
            echo "Файлы удачно сформированы и отправлены на сервер";
        }else{
            echo "Что то пошло не так!";
        }
    }



    /**
     * Отправить файлы по ФТП
     * @param $fileType
     * @param $localeDir
     * @return bool
     * @throws \yii\base\Exception
     */
    public function putXmlFiles($fileType, $localeDir, $remote_dir){
        $conn_id = $this->ftpConnect();
        $localFileName = scandir($localeDir);
        foreach ($localFileName as $file){
            if($file != '.' && $file != '..'){
                if(!ftp_put($conn_id, $remote_dir.DIRECTORY_SEPARATOR.$file, $this->getLocalPutPath($fileType).DIRECTORY_SEPARATOR.$file, FTP_BINARY)){
                    return false;
                }
            }
        }
        ftp_close($conn_id);
        return true;
    }

    /**
     * ftp - коннект
     * @return resource
     */
    public function ftpConnect()
    {
        $host =  getenv('FTP_HOST');
        $port = getenv('FTP_PORT');
        $login = getenv('FTP_NAME');
        $password = getenv('FTP_PASS');

        error_reporting(E_ALL);

        $conn_id = ftp_connect($host, $port);
        ftp_login($conn_id, $login, $password);
        ftp_pasv($conn_id, true);
        return $conn_id;
    }

    /**
     * Получаем локальный путь для загрузки
     * @param $fileType
     * @return bool|string
     * @throws \yii\base\Exception
     */
    public function getLocalPath($fileType)
    {
        if (getenv('YII_ENV') == 'dev') {
            if($fileType == self::FILE_TYPE_BONUS) {
                $localDir = \Yii::getAlias('@backend/web/upload_bonus');
            }elseif($fileType == self::FILE_TYPE_POLLS) {
                $localDir = \Yii::getAlias('@backend/web/upload_polls');
            }elseif ($fileType == self::FILE_TYPE_TARGET) {
                $localDir = \Yii::getAlias('@backend/web/upload_target');
            }else {
                $localDir = \Yii::getAlias('@backend/web/upload_rtt_distrib');
            }
        }else{
            if($fileType == self::FILE_TYPE_BONUS) {
                $localDir = \Yii::getAlias('@data/upload_bonus');
            }elseif($fileType == self::FILE_TYPE_POLLS) {
                $localDir = \Yii::getAlias('@data/upload_polls');
            }elseif($fileType == self::FILE_TYPE_TARGET) {
                $localDir = \Yii::getAlias('@data/upload_target');
            }else {
                $localDir = \Yii::getAlias('@data/upload_rtt_distrib');
            }
        }
        FileHelper::createDirectory($localDir);

        return $localDir;
    }


    /**
     * Получаем локальный путь для файлов на отправку
     * @param $fileType
     * @return bool|string
     * @throws \yii\base\Exception
     */
    public function getLocalPutPath($fileType){
        if (getenv('YII_ENV') == 'dev') {
            if($fileType == self::FILE_TYPE_BONUS) {
                $localDir = \Yii::getAlias('@backend/web/put_bonus');
            }elseif($fileType == self::FILE_TYPE_POLLS) {
                $localDir = \Yii::getAlias('@backend/web/put_polls');
            }elseif($fileType == self::FILE_TYPE_REG_DISTRIB) {
                $localDir = \Yii::getAlias('@backend/web/put_base');
            }elseif($fileType == self::FILE_TYPE_REG_RTT) {
                $localDir = \Yii::getAlias('@backend/web/put_base');
            }else{
                $localDir = \Yii::getAlias('@backend/web/put_social');
            }
        }else{
            if($fileType == self::FILE_TYPE_BONUS) {
                $localDir = \Yii::getAlias('@data/put_bonus');
            }elseif($fileType == self::FILE_TYPE_POLLS) {
                $localDir = \Yii::getAlias('@data/put_polls');
            }elseif($fileType == self::FILE_TYPE_REG_DISTRIB) {
                $localDir = \Yii::getAlias('@data/put_base');
            }elseif($fileType == self::FILE_TYPE_REG_RTT) {
                $localDir = \Yii::getAlias('@data/put_base');
            }else{
                $localDir = \Yii::getAlias('@data/put_social');
            }
        }
        FileHelper::createDirectory($localDir);

        return $localDir;
    }

    /**
     * Забрать файлы по ftp обработать и передать в архив папка Arc
     * @param $fileType
     * @param $remoteDir
     * @return bool
     * @throws \yii\base\Exception
     */
    public function uploadXmlFiles($fileType, $remoteDir, $remoteArcDir){
        $conn_id = $this->ftpConnect();
        $remoteFileName = $this->scanRemoteDir($remoteDir);
        if($remoteFileName) {
            foreach ($remoteFileName as $file) {

                $file = str_replace("/".$remoteDir."/", '', $file);
//                print_r($remoteDir);
//                echo DIRECTORY_SEPARATOR;
//                print_r($file);
//                exit;
                if (!ftp_get($conn_id, $this->getLocalPath($fileType) . DIRECTORY_SEPARATOR . $file, $remoteDir . DIRECTORY_SEPARATOR . $file, FTP_BINARY)) {
                    $message = 'Ошибка при получении файла '.$file.' с ftp-сервера!';
                    XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR, 'Выгрузка xml-файла '.$this->GetFileTypeValue()[$fileType].' с FTP', $message);
                    echo $message;
                    return false;
                }else{
                    if(!$this->addDownloadingFileToSystem($fileType, $file)){
                        $message = 'Ошибка при добавлении файла '.$file.' в таблицу для обработки и добавления в систему!';
                        XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Загрузка xml-файла '.$this->GetFileTypeValue()[$fileType].' на обработку', $message);
                        return false;
                    }

                    if(!$this->deleteXmlFile($conn_id, $remoteDir, $file)){
                        $message = 'Ошибка при удалении файла '.$file.' на FTP сервере! Не удалось удалить файл на FTP-сервере в папке Out (файлы для файлообмена от компании Сордис)';
                        XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Удаление xml-файла '.$this->GetFileTypeValue()[$fileType].' на FTP', $message);
                        return false;
                    }

                    if(!$this->putOneXmlFile($remoteArcDir . DIRECTORY_SEPARATOR . $file,  $this->getLocalPath($fileType) . DIRECTORY_SEPARATOR . $file)){
                        $message = 'Ошибка при отправке файла '.$file.' на FTP сервер в архивную папку Arc! Не удалось отправить файл на удаленный FTP-сервер.';
                        XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Удаление xml-файла '.$this->GetFileTypeValue()[$fileType].' на FTP', $message);
                        return false;
                    }
                }
                $message = 'Файл '.$file.' успешно загружен, добавлен в систему для обработки и отправлен в архивную папку Arc на FTP- сервер.';
                XmlLogs::addMessage( XmlLogs::TYPE_MESSAGE_INFO,  'Обработка xml-файла '.$this->GetFileTypeValue()[$fileType].' полученного с  FTP', $message);
            }
            return true;
        }else{
            $message = 'Папка '.$remoteDir.' пуста! Нет файлов для загрузки и обработки!';
            XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Сканирование папки для загрузки '.$remoteDir, $message);
            return false;
        }
        ftp_close($conn_id);
    }


    /**
     * Отправка одного xml- файла
     * @param $remote_dir_file
     * @param $local_dir_file
     * @return bool
     */
    public function putOneXmlFile($remote_dir_file, $local_dir_file){
        $conn_id = $this->ftpConnect();
        if(!ftp_put($conn_id, $remote_dir_file, $local_dir_file, FTP_BINARY)){
            return false;
        }
        ftp_close($conn_id);
        return true;
    }

    /**
     * Удаляем вайл через ftp
     * @param $conn_id
     * @param $remoteDir
     * @param $file
     * @return bool
     */
    public function deleteXmlFile($conn_id, $remoteDir, $file){
        if(ftp_delete($conn_id, $remoteDir.DIRECTORY_SEPARATOR.$file)){
            return true;
        }else{
            echo "Ошибка при удалении файла!";
            return false;
        }

    }

    /**
     * Получение списка файлов в директории
     * @param $remoteDir
     * @return array
     */
    public function scanRemoteDir($remoteDir){
        $conn_id = $this->ftpConnect();
        $contents = ftp_nlist($conn_id, "/".$remoteDir);
        ftp_close($conn_id);

        return $contents;
    }


    /**
     * Добавляем выгруженные с FTP xml-файлы на обработку
     * @param $fileType
     * @param $file
     * @return bool
     */
    public function addDownloadingFileToSystem($fileType, $file){
        $now = date("Y-m-d H:i:s");
        if(!$this->isRecord($fileType, $file)){
            $message = 'Файл '.$file.' на обработку не добавлен, т к файл с таким именем уже был загружен в систему!';
            XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Добавление файла '.$file.' на обработку', $message);
            return false;
        }
        if($fileType == self::FILE_TYPE_BONUS){
            $newFile = new UploadBonusFile();
        }elseif($fileType == self::FILE_TYPE_TARGET){
            $newFile = new UploadsTargetsFile();
        }elseif($fileType == self::FILE_TYPE_POLLS){
            $newFile = new UploadPollsFile();
        }else{
            $newFile = new UploadSalePointDistrib();
        }

        $newFile->original_file_name = $file;
        $newFile->file_name = $file;
        $newFile->status_process = 'new';
        $newFile->created_at = $now;
        if($newFile->save()){
            $message = 'Файл '.$file.' '.$this->GetFileTypeValue()[$fileType].'успешно добавлен на обработку в систему';
            XmlLogs::addMessage( XmlLogs::TYPE_MESSAGE_INFO,  'Добавление файла '.$file.' на обработку', $message);
            return true;
        }
        return false;
    }

    /**
     * Проверяем, есть ли запись в таблицах загрузки с такими именами файлов воизбежание задвоений
     * @param $fileType
     * @param $file
     * @return bool
     */
    public function isRecord($fileType, $file){
        if($fileType == self::FILE_TYPE_BONUS){
            $isNewFile = UploadBonusFile::findOne(['original_file_name' => $file, 'file_name' => $file]);
        }elseif($fileType == self::FILE_TYPE_TARGET){
            $isNewFile = UploadsTargetsFile::findOne(['original_file_name' => $file, 'file_name' => $file]);
        }elseif($fileType == self::FILE_TYPE_POLLS){
            $isNewFile = UploadPollsFile::findOne(['original_file_name' => $file, 'file_name' => $file]);
        }else{
            $isNewFile = UploadSalePointDistrib::findOne(['original_file_name' => $file, 'file_name' => $file]);
        }

        if($isNewFile){
            return false;
        }else{
            return true;
        }
    }

    /**
     * Получаем файл с бонусами для отправки на ftp
     * @return bool|string
     * @throws \yii\base\Exception
     */
    public function getBonusFile(){
        $arrSpentBonus =  Bonus::arrSpentBonusConsole();
        $path = $this->getLocalPutPath(self::FILE_TYPE_BONUS);
        $now = date("Y-m-d-H-i-s");
        $file_name = 'bonus_'.$now.'.xml';
        if($arrSpentBonus->save($path.DIRECTORY_SEPARATOR.$file_name)){
            $message = 'Файл с потраченными бонусами '.$file_name.' успешно сгенерирован!';
            XmlLogs::addMessage( XmlLogs::TYPE_MESSAGE_INFO,  'Генерация  xml файла с потраченными бонусами '.$file_name, $message);
            return $file_name;
        }else{
            $message = 'Ошибка при генерации файла с потраченными бонусами! Файл с потраченными бонусами '.$file_name.' не сгенерирован!';
            XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Генерация  xml файла с потраченными бонусами '.$file_name, $message);
            return false;
        }
    }

    public function getDistribUserFile()
    {
        $arrUserDistrib = $this->getNewUsersDistribFile();
        $path = $this->getLocalPutPath(self::FILE_TYPE_REG_DISTRIB);
        $now = date("Y-m-d-H-i-s");
        $file_name = 'new_user_distrib'.$now.'.xml';
        if($arrUserDistrib && $arrUserDistrib->save($path.DIRECTORY_SEPARATOR.$file_name)){
            $message = 'Файл с новыми зарегистрированными пользователями '.$file_name.' успешно сгенерирован!';
            XmlLogs::addMessage( XmlLogs::TYPE_MESSAGE_INFO,  'Генерация  xml файла с с новыми зарегистрированными пользователями '.$file_name, $message);
            return $file_name;
        }else{
            $message = 'Ошибка при генерации файла с потраченными бонусами! Файл с потраченными бонусами '.$file_name.' не сгенерирован!';
            XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Генерация  xml файла с потраченными бонусами '.$file_name, $message);
            return false;
        }
    }

    public function getRttUserFile()
    {
        $arrUserRtt = $this->getNewUsersTtFile();
        $path = $this->getLocalPutPath(self::FILE_TYPE_REG_RTT);
        $now = date("Y-m-d-H-i-s");
        $file_name = 'new_user_tt'.$now.'.xml';
        if($arrUserRtt && $arrUserRtt->save($path.DIRECTORY_SEPARATOR.$file_name)){
            $message = 'Файл с новыми зарегистрированными пользователями '.$file_name.' успешно сгенерирован!';
            XmlLogs::addMessage( XmlLogs::TYPE_MESSAGE_INFO,  'Генерация  xml файла с с новыми зарегистрированными пользователями '.$file_name, $message);
            return $file_name;
        }else{
            $message = 'Ошибка при генерации Файла с новыми зарегистрированными пользователями! Файл новыми зарегистрированными пользователями '.$file_name.' не сгенерирован!';
            XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Генерация  xml файла с потраченными бонусами '.$file_name, $message);
            return false;
        }
    }


    /**
     * Формирование и отправка Опросов на ftp-сервер
     * @return bool
     * @throws \yii\base\Exception
     */
    public function getPollInXml(){
        $model = Poll::find()->asArray()->all();
        $path = $this->getLocalPutPath(self::FILE_TYPE_POLLS);
        $now = date("Y-m-d-H-i-s");
        foreach ($model as $item){
            $file = $item['id'].'-poll_'.$now.'.xml';
            $xml = Poll::getArrXmlConsole($item['id']);
            if(!$xml->save($path.DIRECTORY_SEPARATOR.$file)){
                $message = 'Ошибка при генерации xml файла опросов '.$file.'! Файл не сформирован!';
                XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Генерация  xml файла опросов '.$file, $message);
                return false;
            }else{
                if(!$this->putOneXmlFile("In/".self::REMOTE_DIR_OUT_POLLS.DIRECTORY_SEPARATOR.$file,  $this->getLocalPutPath(self::FILE_TYPE_POLLS).DIRECTORY_SEPARATOR.$file)){
                    $message = 'Ошибка при отправке xml файла опросов'.$file.' на FTP сервер в папку In! Файл не оотправлен!';
                    XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Отправка  xml-файла опросов '.$file, $message);
                    return false;
                }
            }
            $message = 'Файл с опросами'.$file.' успешно сформирован и отправлен на FTP сервер в папку In';
            XmlLogs::addMessage( XmlLogs::TYPE_MESSAGE_INFO,  'Отправка  xml-файла опросов '.$file, $message);
        }
        return true;
    }


    /*Формирование и отправка социальных акций по ftp*/
    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function getSocialInXml(){
        $model = SocialActionRules::find()->where(['is_active' => true])->asArray()->all();
        $path = $this->getLocalPutPath(self::FILE_TYPE_SOCIAL);
        $now = date("Y-m-d-H-i-s");
        foreach ($model as $item){
            $file = $item['id'].'-social_'.$now.'.xml';
            $xml = SocialActionRules::getArrXmlInConsole($item['id']);
            if(!$xml->save($path.DIRECTORY_SEPARATOR.$file)){
                $message = 'Ошибка при генерации xml файла социальных акций '.$file.'! Файл не сформирован!';
                XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Генерация  xml файла социальных акций '.$file, $message);
                return false;
            }else{
                if(!$this->putOneXmlFile("In/".self::REMOTE_DIR_OUT_SOCIAL.DIRECTORY_SEPARATOR.$file,  $this->getLocalPutPath(self::FILE_TYPE_SOCIAL).DIRECTORY_SEPARATOR.$file)){
                    $message = 'Ошибка при отправке xml файла социальных акций'.$file.' на FTP сервер в папку In! Файл не оотправлен!';
                    XmlLogs::addMessage( XmlLogs::TYRE_MESSAGE_ERROR,  'Отправка  xml-файла социальных акций '.$file, $message);
                    return false;
                }
            }
            $message = 'Файл с социальными акциями'.$file.' успешно сформирован и отправлен на FTP сервер в папку In';
            XmlLogs::addMessage( XmlLogs::TYPE_MESSAGE_INFO,  'Отправка  xml-файла социальных акций '.$file, $message);
        }
        return true;
    }
}
