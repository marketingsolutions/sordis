<?php

use yii\db\Migration;

/**
 * Class m190423_080001_news_add_column_role_visible_to_news_table
 */
class m190423_080001_news_add_column_role_visible_to_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%news}}', 'visible_role', $this->string(125));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%news}}', 'visible_role');
    }
}
