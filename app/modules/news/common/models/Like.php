<?php

namespace modules\news\common\models;

use modules\profiles\common\models\Profile;
use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "like".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $model
 * @property integer $item_id
 */
class Like extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'like';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Like';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Статистика популярности раздела Новости';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['user_id', 'required'],
            ['user_id', 'integer'],
            ['model', 'required'],
            ['model', 'string', 'max' => 255],
            ['item_id', 'required'],
            ['item_id', 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID новости',
            'user_id' => 'User ID',
            'model' => 'Model',
            'item_id' => 'Item ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile(){
        return $this->hasOne(Profile::className(), ['id' => 'user_id']);
    }

    /**
     * @return mixed
     */
    public function getProfileFullname(){
        return isset($this->profile->full_name) ? $this->profile->full_name : null;
    }

    /**
     * @return mixed
     */
    public function getProfilePhone(){
        return isset($this->profile->phone_mobile) ? $this->profile->phone_mobile : null;
    }

    public function getNew(){
        return $this->hasOne(News::className(), ['id' => 'item_id']);
    }

    /**
     * @return mixed
     */
    public function getNewsTitle(){
        return $this->new->title;
    }

}
