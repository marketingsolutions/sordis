<?php

namespace modules\news\common\models;

use common\utils\Notifier;
use modules\profiles\common\models\Profile;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_news".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $visible_role
 * @property string $created_at
 * @property string $updated_at
 * @property integer $enabled
 */
class News extends \yz\db\ActiveRecord implements ModelInfoInterface
{

    /** @var  integer */
    public $user_id;

    const ROLE_ALL = 'all';

    public static function getRoleForNews()
    {
        $roles = Profile::getDashboardRealRole();
        $allRoles = [self::ROLE_ALL => 'Все участники'];

        return array_merge($allRoles, $roles);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Новость';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Новости';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'string', 'max' => 255],
            ['visible_role', 'string', 'max' => 125],
            ['content', 'string'],
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
            ['enabled', 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'content' => 'Содержимое',
            'created_at' => 'Размещена',
            'updated_at' => 'Обновлена',
            'visible_role' => 'Роль участников для показа новости',
            'enabled' => 'Активна',
        ];
    }

    /**
     * Меню с разбивкой по месяцам
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getMonthMenu()
    {
        $month = new \yii\db\Expression("DATE_FORMAT(created_at,'%m')");
        $year = new \yii\db\Expression("DATE_FORMAT(created_at,'%Y')");
        $month_year = new \yii\db\Expression("DATE_FORMAT(created_at, '%Y%m')");

        if(isset(\Yii::$app->user->identity->profile)){
            $profile = \Yii::$app->user->identity->profile;
            $createdAtArr =  News::find()
                ->select(["$month AS month_name", "$year AS year_name"])
                ->where(['visible_role' => $profile->role, 'enabled' => true])
                ->orWhere(['visible_role' => News::ROLE_ALL, 'enabled' => true])
                ->groupBy($month_year)
                ->orderBy(['id' =>SORT_DESC])
                ->asArray()
                ->all();
        }
        else{
            $createdAtArr =   News::find()
                ->select("$month AS `month_name`, $year AS `year_name`")
                ->where(['enabled' => true])
                ->groupBy($month_year)
                ->orderBy(['id' =>SORT_DESC])
                ->asArray()
                ->all();
        }

        $yearArr = [];
        foreach ($createdAtArr as $item){
            $yearArr[][$item['year_name']] = $item['month_name'];
        }
        return $yearArr;
    }

    public static function getMyMonth($currentMonth)
    {
        $textMonth =  [
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь',
        ];
        return $textMonth[$currentMonth];
    }

    public static function getCurrentYear()
    {
        $year = new \yii\db\Expression("DATE_FORMAT(created_at,'%Y')");
        if(isset(\Yii::$app->user->identity->profile)){
            $profile = \Yii::$app->user->identity->profile;
            $arrYear =  News::find()
                ->select(["$year AS year"])
                ->where(['visible_role' => $profile->role, 'enabled' => true])
                ->orWhere(['visible_role' => News::ROLE_ALL, 'enabled' => true])
                ->groupBy($year)
                ->orderBy(['id' =>SORT_DESC])
                ->asArray()
                ->all();
        }
        else{
            $arrYear =   News::find()
                ->select("$year AS `year_name`")
                ->where(['enabled' => true])
                ->groupBy($year)
                ->orderBy(['id' =>SORT_DESC])
                ->asArray()
                ->all();
        }
        return $arrYear;
    }

    public static function getSliderNews()
    {
        $profile = \Yii::$app->user->identity->profile;
        return News::find()
            ->where(['visible_role' => $profile->role, 'enabled' => true])
            ->orWhere(['visible_role' => News::ROLE_ALL, 'enabled' => true])
            ->orderBy(['id' => SORT_DESC])
            ->asArray()
            ->limit(4)
            ->all();
    }
}
