<?php

namespace modules\news\common\models;

use mongosoft\file\UploadBehavior;
use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_instructions".
 *
 * @property integer $id
 * @property string $image_preview
 * @property string $pdf_file
 * @property string $title
 * @property string $comment
 */
class Instruction extends \yii\db\ActiveRecord implements ModelInfoInterface
{
	public $image_preview_local;
	public $pdf_file_local;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%instructions}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Инструкция';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Инструкции';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['image_preview', 'string', 'max' => 255],
            ['pdf_file', 'string', 'max' => 255],
			['pdf_file', 'file', 'extensions' => 'pdf'],
			['title', 'string', 'max' => 255],
			['comment', 'string'],
			['image_preview_local', 'file', 'extensions' => 'jpg, png, gif, bmp'],
			['pdf_file_local', 'file', 'extensions' => 'pdf'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_preview' => 'Превью',
            'pdf_file' => 'Инструкция PDF',
			'image_preview_local' => 'Превью',
			'pdf_file_local' => 'Инструкция PDF',
			'title' => 'Заголовок',
			'comment' => 'Описание',
        ];
    }

	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);

		$this->upload(['image_preview', 'pdf_file']);
	}

	private function upload(array $fields)
	{
		$dir = Yii::getAlias('@data/instructions');
		FileHelper::createDirectory($dir, $mode = 0775, $recursive = true);

		foreach ($fields as $field) {
			$file = UploadedFile::getInstance($this, $field . '_local');

			if ($file instanceof UploadedFile) {
				$name = "{$this->id}_$field.{$file->extension}";
				$path = Yii::getAlias("@data/instructions/$name");
				$file->saveAs($path);

				$this->$field = $name;
				$this->updateAttributes([$field]);
			}
		}
	}
}
