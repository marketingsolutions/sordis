<?php

namespace modules\news\frontend\controllers;

use modules\news\backend\models\NewsSearch;
use modules\profiles\common\models\Profile;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yz\Yz;

class NewsController extends Controller
{
    public function actionIndex()
    {
        /** @var NewsSearch $searchModel */
        $searchModel = Yii::createObject(NewsSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        $dataProvider->setSort([
            'defaultOrder' => ['created_at' => SORT_DESC],

        ]);

        /** @var Profile $profile */
        $profile = \Yii::$app->user->identity->profile;
        $profile->fresh_news = 0;
        $profile->updateAttributes(['fresh_news']);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
}