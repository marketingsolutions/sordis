<?php

namespace modules\news\frontend\controllers;

use modules\news\backend\models\NewsSearch;
use modules\news\common\models\Instruction;
use modules\profiles\common\models\Profile;
use Yii;
use yii\web\Controller;

class InstructionsController extends Controller
{
    public function actionIndex()
    {
        $instructions = Instruction::find()->orderBy(['id' => SORT_ASC])->all();

        return $this->render('index', [
            'instructions' => $instructions,
        ]);
    }
}