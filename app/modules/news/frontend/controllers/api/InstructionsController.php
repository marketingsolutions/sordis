<?php

namespace modules\news\frontend\controllers\api;

use modules\news\frontend\models\ApiInstruction;
use modules\profiles\frontend\models\ApiProfile;
use ms\loyalty\api\common\models\Log;
use ms\loyalty\api\frontend\base\ApiController;
use modules\profiles\common\models\Infopage;
use Yii;

class InstructionsController extends ApiController
{
    public function actionList()
    {
        $profile_id = Yii::$app->request->post('profile_id');

        /** @var ApiProfile $profile */
        $profile = ApiProfile::findOne($profile_id);

        if ($profile == null) {
            $return = ['result' => 'FAIL', 'reason' => 'Не найден участник по profile_id=' . $profile_id];
            Yii::$app->response->statusCode = 400;
            Log::setComment("Не найден участник", 400);
            Log::setResponse($return);
            return $return;
        }

        $instructions = ApiInstruction::find()
            ->orderBy(['id' => SORT_DESC])
            ->all();

        $return = ['result' => 'OK', 'instructions' => $instructions];
        Log::setComment('Выдача списка инструкций');

        return $return;
    }

    public function actionTerms()
    {
        $rules = Infopage::find()
        ->all();
        $return = ['result' => 'OK', 'terms' => $rules];
        return $return;
    }
}