<?php

namespace modules\news\frontend\controllers\api;

use modules\news\frontend\models\ApiNews;
use modules\profiles\frontend\models\ApiProfile;
use ms\loyalty\api\common\models\Log;
use ms\loyalty\api\frontend\base\ApiController;
use Yii;

class NewsController extends ApiController
{
    public function actionList()
    {
        try {
            $profile_id = Yii::$app->request->post('profile_id');

            /** @var ApiProfile $profile */
            $profile = ApiProfile::findOne($profile_id);

            if ($profile == null) {
                return $this->error('Участник не найден', 'Ошибка при получении списка новостей');
            }

            $profile->updateAttributes(['fresh_news' => 0]);
            $news = ApiNews::find()->where(['enabled' => true])->orderBy(['created_at' => SORT_DESC])->all();

            return $this->ok(['news' => $news], 'Выдача списка новостей');
        }
        catch (\Exception $e) {
            return $this->error('Ошибка на сервере', 'Ошибка на сервере: ' . $e->getMessage(), 500);
        }
    }
}