<?php

use modules\warranty\common\models\Warranty;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;

/**
 * @var yii\web\View $this
 * @var modules\news\common\models\Instruction[] $instructions
 * @var array $columns
 */

$this->title = 'Инструкции';
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

$css = <<<CSS
    .item {
        margin-top: 26px;
        padding: 12px 14px;
        max-width: 630px;
        text-align: center;
    }
    .item img {
    	max-width: 100%;
    	margin-bottom: 15px;
    }
    .item-title {
        font-weight: bold;
        margin-bottom: 10px;
        border-bottom: 1px solid #ccc;
        font-size: 15px !important;
    }
    .item-content p {
        line-height: 13px;
    }
    .image-preview {
    	text-align: center;
    }
CSS;

$this->registerCss($css);
?>

<?php foreach ($instructions as $instruction): ?>
	<div class="items">
		<div class="item sale-item--status">
			<?php if (!empty($instruction->title)): ?>
				<div class="item-title">
					<?= Html::encode($instruction->title) ?>
				</div>
			<?php endif; ?>
			<div class="item-footer">
				<div class="modal-body">
					<?php if (!empty($instruction->image_preview)): ?>
						<div class="image-preview">
							<a href="/data/instructions/<?= $instruction->pdf_file ?>" target="_blank">
								<img src="/data/instructions/<?= $instruction->image_preview ?>"/>
							</a>
						</div>
					<?php endif; ?>
					<?= \yii\helpers\HtmlPurifier::process($instruction->comment) ?>
				</div>
				<div class="item-controls">
					<a class="btn btn-primary" target="_blank"
					   href="/data/instructions/<?= $instruction->pdf_file ?>">Прочитать</a>
				</div>
			</div>
		</div>
	</div>
<?php endforeach; ?>
