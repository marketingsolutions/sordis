<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use modules\news\common\models\News;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\news\backend\models\NewsSearch $searchModel
 * @var array $columns
 */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

?>
<div class="row">
<div class="item-news col-md-9">
  <?= \yii\widgets\ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_news',
  ]); ?>
</div>
<div class="col-md-3">
  <div class="panel_ferma archive-block">
    <h2>Архив новостей</h2>
    <?php foreach (News::getCurrentYear() as $year): ?>
      <?php echo "<div class='menu_year'>" . $year['year'] . "</div>"; ?>
      <div class="year-list">
        <?php foreach (News::getMonthMenu() as $month): ?>
          <?php
          if (isset($month[$year['year']])) {
            echo "<a class='menu_year_link' href='/news/news?year=" . $year['year'] . "&month=" . $month[$year['year']] . "'>" . News::getMyMonth($month[$year['year']]) . "</a>";
          }
          ?>
        <?php endforeach; ?>
      </div>
    <?php endforeach; ?>
  </div>
</div>
</div>
