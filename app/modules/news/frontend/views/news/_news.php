<?php

use yii\helpers\Html;
use yii\helpers\Url;
use halumein\like\widgets\LikeButton;
use modules\news\common\models\News;

/**
 * @var \yii\web\View $this
 * @var \modules\news\common\models\News $model
 */

?>

<div class="news-list items">
  <div class="item sale-item--status back_fon">
    <h2><?= Html::encode($model->title) ?></h2>
    <div class="news-content"><?= $model->content ?></div>
    <div class="news-list--button-block">
      <?php if(!\Yii::$app->request->get('id')):?>

      <?php endif;?>
    </div>
    <div class="news-footer">
      <?= (new \DateTime($model->created_at))->format('d.m.Y') ?>
      <?= LikeButton::widget(['model' => $model]) ?>
    </div>
  </div>
</div>
