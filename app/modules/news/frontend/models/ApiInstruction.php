<?php

namespace modules\news\frontend\models;

use modules\news\common\models\Instruction;
use Yii;

class ApiInstruction extends Instruction
{
    public function fields()
    {
        return [
            'id',
            'title',
            'comment',
            'image_preview' => function (ApiInstruction $model) {
                return empty($model->image_preview)
                    ? null
                    : Yii::getAlias('@frontendWeb/data/instructions/' . $model->image_preview) ;
            },
            'pdf_file' => function (ApiInstruction $model) {
                return empty($model->pdf_file)
                    ? null
                    : Yii::getAlias('@frontendWeb/data/instructions/' . $model->pdf_file) ;
            },
        ];
    }
}