<?php

namespace modules\news\frontend\models;

use modules\news\common\models\News;

class ApiNews extends News
{
    public function fields()
    {
        return [
            'id',
            'title',
            'content',
            'created_at' => function (ApiNews $model) {
                if (empty($model->created_at)) {
                    return null;
                }

                if ($model->created_at == 'NOW()') {
                    return (new \DateTime)->format('d.m.Y');
                }

                return (new \DateTime($model->created_at))->format('d.m.Y');
            }
        ];
    }
}