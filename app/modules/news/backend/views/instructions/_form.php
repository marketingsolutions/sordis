<?php

use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var modules\news\common\models\Instruction $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php  $box = FormBox::begin(['cssClass' => 'instruction-form box-primary', 'title' => '']) ?>
	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php $box->beginBody() ?>

	<?= $form->field($model, 'title')->textInput() ?>
	<?= $form->field($model, 'comment')->tinyMce([
    'rows' => 20,
    'fontsize_formats' => '8pt 9pt 10pt 11pt 12pt 14pt 16pt 18pt 26pt 36p',
    'toolbar' => "undo redo | styleselect | bold italic size | alignleft aligncenter alignright alignjustify"
        . "| bullist numlist outdent indent | link image | fontselect | fontsizeselect"
]) ?>

	<?= $this->render('_image', ['model' => $model, 'field' => 'image_preview']); ?>
    <?= $form->field($model, 'image_preview_local')->fileInput() ?>

	<?= $this->render('_image', ['model' => $model, 'field' => 'pdf_file']); ?>
    <?= $form->field($model, 'pdf_file_local')->fileInput() ?>

    <?php $box->endBody() ?>

    <?php $box->actions([
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
    ]) ?>
    <?php ActiveForm::end(); ?>

<?php  FormBox::end() ?>
