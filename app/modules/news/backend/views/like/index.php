<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\news\backend\models\LikeSearch $searchModel
 * @var array $columns
 */

$this->title = modules\news\common\models\Like::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'like-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [/*['search'],*/ ['export', 'create', /*'delete',*/ 'return']],
            'gridId' => 'like-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\news\common\models\Like',
        ]) ?>
    </div>

    <?= GridView::widget([
        'id' => 'like-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '{update}',
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
