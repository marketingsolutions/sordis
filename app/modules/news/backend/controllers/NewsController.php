<?php

namespace modules\news\backend\controllers;

use common\utils\Notifier;
use Yii;
use modules\news\common\models\News;
use modules\news\backend\models\NewsSearch;
use backend\base\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var NewsSearch $searchModel */
                    return Yii::createObject(NewsSearch::className());
                },
                'dataProvider' => function($params, NewsSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var NewsSearch $searchModel */
        $searchModel = Yii::createObject(NewsSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(NewsSearch $searchModel)
    {
        return [
			'id',
			'title',
            [
                'attribute' => 'visible_role',
                'filter' => News::getRoleForNews(),
                'value' => function($model){
                    return News::getRoleForNews()[$model->visible_role];
                }
            ],

			'created_at:datetime',
			'enabled:boolean',
        ];
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News;
        $model->enabled = true;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
		    Notifier::newsPush($model);
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, "Новость '{$model->title}' успешно создана и добавлена в очередь PUSH-уведомлений");
            \Yii::$app->db->createCommand('UPDATE yz_profiles SET fresh_news = fresh_news+1')->execute();

            return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
        ]);
	}

    public function actionPush($id)
    {
        $model = $this->findModel($id);

        Notifier::newsPush($model);
        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, "Новость '{$model->title}' добавлена в очередь PUSH-уведомлений");

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
