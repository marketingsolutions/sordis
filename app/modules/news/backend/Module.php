<?php

namespace modules\news\backend;

use yz\icons\Icons;

/**
 * Class Module
 */
class Module extends \modules\news\common\Module
{
    public function getAdminMenu()
    {
        return [
            [
                'label' => 'Новости',
                'icon' => Icons::o('list'),
                'items' => [
                [
                    'route' => ['/news/news/index'],
                    'label' => 'Новости',
                    'icon' => Icons::o('list'),
                ],
                [
                    'route' => ['/news/like/index'],
                    'label' => 'Статистика популярности Новостей',
                    'icon' => Icons::o('list'),
                ],
            ],
        ]
    ];
    }

}