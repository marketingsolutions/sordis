<?php

namespace modules\news\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\news\common\models\Like;

/**
 * LikeSearch represents the model behind the search form about `modules\news\common\models\Like`.
 */
class LikeSearch extends Like implements SearchModelInterface

{
    /**
     * @var
     */
    public $profileFullname;

    /**
     * @var
     */
    public $profilePhone;

    /**
     * @var
     */
    public $newsTitle;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'item_id'], 'integer'],
            [['model', 'profileFullname', 'profilePhone', 'newsTitle'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        return Like::find()
            ->joinWith('profile')
            ->joinWith('new');
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'profileFullname' => [
                'asc' => ['{{%profiles}}.full_name' => SORT_ASC],
                'desc' => ['{{%profiles}}.full_name' => SORT_DESC]
            ],
            'profilePhone' => [
                'asc' => ['{{%profiles}}.phone_mobile' => SORT_ASC],
                'desc' => ['{{%profiles}}.phone_mobile' => SORT_DESC]
            ],
            'newsTitle' => [
                'asc' => ['{{%news}}.title' => SORT_ASC],
                'desc' => ['{{%news}}.title' => SORT_DESC]
            ],
        ]);
        $dataProvider->setSort($sort);

        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'item_id' => $this->item_id,
        ]);

        $query->andFilterWhere(['like', '{{%news}}.title', $this->newsTitle]);
        $query->andFilterWhere(['like', '{{%profiles}}.full_name', $this->profileFullname]);
        $query->andFilterWhere(['like', '{{%profiles}}.phone_mobile', $this->profilePhone]);

    }
}
