<?php

namespace modules\news\backend\models;

use function RingCentral\Psr7\str;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\news\common\models\News;

/**
 * NewsSearch represents the model behind the search form about `modules\news\common\models\News`.
 */
class NewsSearch extends News implements SearchModelInterface

{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'enabled'], 'integer'],
            [['title', 'content', 'created_at', 'updated_at', 'visible_role'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        if(isset(\Yii::$app->user->identity->profile)){
            $profile = \Yii::$app->user->identity->profile;

            if(\Yii::$app->request->get('year') && \Yii::$app->request->get('month')){
                $year = \Yii::$app->request->get('year');
                $month = \Yii::$app->request->get('month');
                $from = $year."-".$month."-01";
                $from = date("Y-m-d 00:00:00", strtotime($from));
                $to = date("Y-m-t 23:59:59", strtotime($from));

                $query =  News::find()
                    ->where(['visible_role' => $profile->role, 'enabled' => true])
                    ->orWhere(['visible_role' => News::ROLE_ALL, 'enabled' => true])
                    ->andWhere(['between', 'created_at', $from, $to]);

            }elseif (\Yii::$app->request->get('id')){
                $query =  News::find()
                    ->where(['visible_role' => $profile->role, 'enabled' => true])
                    ->orWhere(['visible_role' => News::ROLE_ALL, 'enabled' => true])
                    ->andWhere(['id' => \Yii::$app->request->get('id')]);
            }else{
                $query =  News::find()
                    ->where(['visible_role' => $profile->role, 'enabled' => true, 'id' => $this->getLastNewsId()])
                    ->orWhere(['visible_role' => News::ROLE_ALL, 'enabled' => true, 'id' => $this->getLastNewsId()]);
            }
            return $query;
        }
        else{
            return News::find()->where(['id' => $this->getAllNewsId()]);
        }
    }

    public function getLastNewsId()
    {
        if(isset(\Yii::$app->user->identity->profile)){
            $profile = \Yii::$app->user->identity->profile;

            $id =  News::find()
                ->select('id')
                ->where(['visible_role' => $profile->role, 'enabled' => true])
                ->orWhere(['visible_role' => News::ROLE_ALL, 'enabled' => true])
                ->orderBy(['id' => SORT_DESC])
                ->asArray()
                ->one();
        }
        else{
            $id =  News::find()->select('id')->where(['enabled' => true])->orderBy(['id' => SORT_DESC])->asArray()->one();
        }
        if(!empty($id)){
            return $id['id'];
        }else{
            return [];
        }
    }
    public function getAllNewsId()
    {
        if(isset(\Yii::$app->user->identity->profile)){
            $profile = \Yii::$app->user->identity->profile;

            $id =  News::find()
                ->select('id')
                ->where(['visible_role' => $profile->role, 'enabled' => true])
                ->orWhere(['visible_role' => News::ROLE_ALL, 'enabled' => true])
                ->orderBy(['id' => SORT_DESC])
                ->asArray()
                ->one();
        }
        else{
            $id =  News::find()->select('id')->orderBy(['id' => SORT_DESC])->column();
        }
        if(!empty($id)){
            return $id;
        }else{
            return [];
        }
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'id' => $this->id,
            'enabled' => $this->enabled,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'visible_role', $this->visible_role])
            ->andFilterWhere(['like', 'content', $this->content]);
    }
}
