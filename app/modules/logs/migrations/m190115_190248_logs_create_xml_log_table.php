<?php

use yii\db\Migration;

/**
 * Class m190115_190248_logs_create_xml_log_table
 */
class m190115_190248_logs_create_xml_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%xml_log}}', [
            'id' => $this->primaryKey(),
            'operation_name' => $this->string(),
            'operation_type' => $this->string(),
            'message' => $this->text(),
            'created_at' => $this->dateTime(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%xml_log}}');
    }
}
