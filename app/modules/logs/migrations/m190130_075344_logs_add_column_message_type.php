<?php

use yii\db\Migration;

/**
 * Class m190130_075344_logs_add_column_message_type
 */
class m190130_075344_logs_add_column_message_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%xml_log}}', 'message_type' , $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%xml_log}}', 'message_type');
    }
}
