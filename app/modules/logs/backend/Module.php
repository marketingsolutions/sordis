<?php

namespace modules\logs\backend;

use yz\icons\Icons;

/**
 * Class Module
 */
class Module extends \modules\logs\common\Module
{
    public function getAdminMenu()
    {
		return [
			[
				'label' => 'Логи загрузки XML',
				'icon' => Icons::o('archive'),
				'items' => [
                    [
                        'route' => ['/logs/xml-logs/index'],
                        'label' => 'Лог',
                        'icon' => Icons::o('list'),
                    ],
//                    [
//                        'route' => ['/mobile/gcm-log/index'],
//                        'label' => 'Логи',
//                        'icon' => Icons::o('list'),
//                    ],
//                    [
//                        'route' => ['/mobile/gcm/index'],
//                        'label' => 'Устройства',
//                        'icon' => Icons::o('mobile'),
//                    ],
//                    [
//                        'route' => ['/mobile/platforms/index'],
//                        'label' => 'Приложения',
//                        'icon' => Icons::o('archive'),
//                    ],
				]
			],
		];
    }

}