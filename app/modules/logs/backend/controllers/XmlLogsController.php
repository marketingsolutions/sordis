<?php

namespace modules\logs\backend\controllers;

use Yii;
use modules\logs\common\models\XmlLogs;
use modules\logs\backend\models\XmlLogsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * XmlLogsController implements the CRUD actions for XmlLogs model.
 */
class XmlLogsController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var XmlLogsSearch $searchModel */
                    return Yii::createObject(XmlLogsSearch::className());
                },
                'dataProvider' => function($params, XmlLogsSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all XmlLogs models.
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        /** @var XmlLogsSearch $searchModel */
        $searchModel = Yii::createObject(XmlLogsSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(XmlLogsSearch $searchModel)
    {
        return [
            [
                'attribute' => 'message_type',
                'filter' => XmlLogs::getTypeValues(),
                'format' => 'html',
                'value' => function($data){
                    return $data->message_type == XmlLogs::TYPE_MESSAGE_INFO ? "<span style='font-size: 14px;' class='label label-success'>".XmlLogs::getTypeValues()[$data->message_type]."</span>"  :  "<span style='font-size: 14px;' class='label label-warning'>".XmlLogs::getTypeValues()[$data->message_type]."</span>";
                },
            ],
			'operation_name',
			'message:raw',
			'created_at:datetime',
        ];
    }

    /**
     * Creates a new XmlLogs model.
     * @return string|Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionCreate()
    {
        $model = new XmlLogs;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing XmlLogs model.
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
        ]);
	}


    /**
     * Deletes an existing XmlLogs model.
     * @param array $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the XmlLogs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return XmlLogs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = XmlLogs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
