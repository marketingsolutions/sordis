<?php

namespace modules\logs\common\models;

use modules\ftp\console\controllers\FtpExchangeController;
use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_xml_log".
 *
 * @property integer $id
 * @property string $operation_name
 * @property string $operation_type
 * @property string $message
 * @property string $message_type
 * @property string $created_at
 */
class XmlLogs extends \yii\db\ActiveRecord implements ModelInfoInterface
{

    const TYPE_MESSAGE_INFO = 'info';
    const TYRE_MESSAGE_ERROR = 'error';

    const ACTON_UPLOAD_BONUS = 'upload_bonus';
    const ACTON_UPLOAD_POLLS = 'upload_polls';
    const ACTON_UPLOAD_TARGETS = 'upload_targets';
    const ACTON_UPLOAD_RTT_DISTRIB = 'upload_rtt_distrib';
    const ACTON_PUT_BONUS = 'put_bonus';
    const ACTON_PUT_POLLS = 'put_polls';
    const ACTON_PUT_SOCIAL = 'put_social';

    /**
     * @return array
     */
    public static function getTypeValues(){
        return [
            self::TYPE_MESSAGE_INFO => 'Информационное сообщение',
            self::TYRE_MESSAGE_ERROR => 'Ошибка',

        ];
    }

    public static function getActionValues(){
        return [
            self::ACTON_UPLOAD_BONUS => 'Загрузка бонусов',
            self::ACTON_UPLOAD_POLLS => 'Загрузка опросов',
            self::ACTON_UPLOAD_TARGETS => 'Загрузка целей',
            self::ACTON_UPLOAD_RTT_DISTRIB => 'Загрузка РТТ и Дистрибьюторов',
            self::ACTON_PUT_BONUS => 'Отправка бонусов',
            self::ACTON_PUT_POLLS => 'Отправка опросов',
            self::ACTON_PUT_SOCIAL => 'Отправка социальных акций',
        ];
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xml_log}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Xml Logs';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Логи загрузки и обработки XML';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['operation_name', 'string', 'max' => 255],
            ['message', 'string'],
            ['message_type', 'string'],
            ['created_at', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message_type' => 'Тип сообщения',
            'operation_type' => 'Тип деиствия',
            'operation_name' => 'Название действия',
            'message' => 'Текст сообщения',
            'created_at' => 'Дата добавления',
        ];
    }

    public static function addMessage($message_type, $operation_name, $message)
    {
        $now = date("Y-m-d H:i:s");
        $model = new self();
        $model->message_type = $message_type;
        $model->operation_name = $operation_name;
        $model->message = $message;
        $model->created_at = $now;
        $model->save(false);
    }
}
