<?php

namespace modules\mobile\common\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class PlatformForm extends Platform
{
    public $file_local;

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->upload('file');
    }

    private function upload($field)
    {
        $file = UploadedFile::getInstance($this, $field . '_local');
        $dir = Yii::getAlias("@data/platforms/");
        FileHelper::createDirectory($dir);

        if ($file instanceof UploadedFile) {
            $name = "{$this->platform}.{$file->extension}";
            $path = Yii::getAlias("@data/platforms/$name");
            $file->saveAs($path);

            $this->$field = $name;
            $this->updateAttributes([$field]);
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(),[
            [['file_local'], 'file', 'extensions' => ['apk', 'appx'], 'maxSize' => 1024 * 1024 * 50],
        ]);
    }
}
