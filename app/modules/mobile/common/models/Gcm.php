<?php

namespace modules\mobile\common\models;

use modules\profiles\common\models\Profile;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_gcm".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $profile_id
 * @property string $platform
 * @property string $api_key
 * @property string $token
 *
 * @property Profile $profile
 */
class Gcm extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    const API_KEY = 'AAAAKbAZbmI:APA91bEC5hItOWD-hmvLGB7a_PkQ-T2me29EZ1L8QdV2j8YXUHOrLdJoMyARlZgpHKqg6tGuwJfguJfKN9TWH5hWA2vUIXlcfe_T5Xh88M0nWCIruD6-qWRJPEvf8QV9QP8CiWWp4cWj';

    const MENU_NEWS = 'news';
    const MENU_SALES = 'sales';
    const MENU_WARRANTY = 'warranty';
    const MENU_INDEX = 'index';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%gcm}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Gcm';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Gcms';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => (new \DateTime())->format('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
            ['profile_id', 'integer'],
            ['platform', 'string', 'max' => 255],
            ['api_key', 'string', 'max' => 255],
            ['token', 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'profile_id' => 'Profile ID',
            'platform' => 'Platform',
            'api_key' => 'Api Key',
            'token' => 'Token',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }

    public static function getProfileOptions()
    {
        $options = [];

        /** @var Profile[] $profiles */
        $profiles = (new Query())
            ->select('profile.*')
            ->from(['profile' => Profile::tableName()])
            ->innerJoin(['gcm' => Gcm::tableName()], 'gcm.profile_id = profile.id')
            ->orderBy(['profile.full_name' => SORT_ASC])
            ->all();

        foreach ($profiles as $profile) {
            $key = $profile['id'] . '';
            $options[$key] = $profile['full_name'] . ' (' . $profile['phone_mobile'] . ')';
        }

        return $options;
    }

    public static function sendToAll($title, $body, $menu = null, $notification_id = null)
    {
        /** @var Gcm[] $gcms */
        $gcms = $menu
            ? Gcm::find()->joinWith('profile')->where(['{{%profiles}}.push_' . $menu => true])->all()
            : Gcm::find()->orderBy(['id' => SORT_ASC])->all();

        if (empty($gcms)) {
            return false;
        }

        return self::sendBroadcast($title, $body, $gcms, $menu, $notification_id);
    }

    public static function sendToProfile($title, $body, $profile_id, $menu = null, $notification_id = null)
    {
        $gcms = $menu
            ? Gcm::find()->joinWith('profile')->where(['{{%profiles}}.push_' . $menu => true, 'profile_id' => $profile_id])->all()
            : Gcm::findAll(['profile_id' => $profile_id]);

        if (empty($gcms)) {
            return false;
        }

        return self::sendBroadcast($title, $body, $gcms, $menu, $notification_id);
    }

    /**
     * @param $title
     * @param $body
     * @param $gcms
     * @param $menu string
     * @param integer $notification_id
     * @param string $badge
     * @param string $sound
     * @return bool
     */
    public static function sendBroadcast($title, $body, $gcms, $menu = null, $notification_id = null, $badge = '1', $sound = 'default')
    {
        /** @var Gcm[] $gcms */
        $API_KEY = Gcm::API_KEY;

        try {
            $tokensAll = [];
            foreach ($gcms as $gcm) {
                $tokensAll[] = $gcm->token;
            }
            $tokensChunked = array_chunk($tokensAll, 500);
            foreach ($tokensChunked as $tokens) {
                $fields = [
                    'registration_ids' => $tokens,
                    'notification' => [
                        'title' => $title,
                        'body' => $body,
                        'badge' => $badge,
                        'sound' => $sound,
                        "click_action" => "FCM_PLUGIN_ACTIVITY",
                    ],
                    'data' => [
                        'menu' => $menu,
                        'title' => $title,
                        'body' => $body,
                    ]
                ];

                $fields = json_encode($fields, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

                $headers = [
                    'Authorization: key=' . $API_KEY,
                    'Content-Type: application/json'
                ];

                $log = new GcmLog();
                $log->title = $title;
                $log->body = $body;
                $log->request = $fields;
                $log->to = implode(', ', $tokens);

                if ($notification_id) {
                    $log->notification_id = $notification_id;
                }

                $log->save(false);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                $result = curl_exec($ch);
                curl_close($ch);

                $log->updateAttributes(['response' => $result]);
                $log = null;
            }

            return true;
        }
        catch (\Exception $e) {
            if (!empty($log)) {
                $log->updateAttributes(['response' => 'Exception: ' . $e->getMessage()]);
            }
            return false;
        }
    }
}
