<?php

namespace modules\mobile\common\models;

use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_mobile_platforms".
 *
 * @property integer $id
 * @property string $platform
 * @property string $name
 * @property string $file
 */
class Platform extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%mobile_platforms}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Мобильная платформа';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Мобильные платформы';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['platform', 'string', 'max' => 255],
            ['platform', 'required'],
            ['name', 'string', 'max' => 255],
            ['file', 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'platform' => 'Идентификатор платформы',
            'name' => 'Описание платформы',
            'file' => 'Путь к файлу',
            'file_local' => 'Файл',
        ];
    }
}
