<?php

namespace modules\mobile\common\models;

use modules\profiles\common\models\Profile;
use Yii;
use yii\behaviors\TimestampBehavior;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_mobile_notifications".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property string $type
 * @property string $title
 * @property string $body
 * @property string $created_at
 * @property string $updated_at
 *
 * @property GcmLog[] $gcmLogs
 */
class MobileNotification extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    const TYPE_INDIVIDUAL = 'individual';
    const TYPE_ALL = 'all';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yz_mobile_notifications';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'PUSH-уведомление';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'PUSH-уведомления';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => (new \DateTime())->format('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profile_id'], 'integer'],
            [['title', 'body'], 'string'],
            [['title', 'body'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Участник',
            'type' => 'Тип уведомления',
            'title' => 'Заголовок',
            'body' => 'Содержание',
            'created_at' => 'Отправлено',
            'updated_at' => 'Обновлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGcmLogs()
    {
        return $this->hasMany(GcmLog::className(), ['notification_id' => 'id']);
    }

    public function to() {
        if (empty($this->profile_id)) {
            return 'ВСЕМ';
        }
        else {
            /** @var Profile $profile */
            $profile = Profile::findOne($this->profile_id);
            return empty($profile) ? '' : $profile->full_name . ' (' . $profile->phone_mobile . ')';
        }
    }

    public function push()
    {
        if (!empty($this->profile_id)) {
            Gcm::sendToProfile(
                $this->title,
                $this->body,
                $this->profile_id,
                Gcm::MENU_INDEX,
                $this->id
            );
        }
        else {
            Gcm::sendToAll(
                $this->title,
                $this->body,
                Gcm::MENU_INDEX,
                $this->id
            );
        }
    }
}
