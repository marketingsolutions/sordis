<?php

namespace modules\mobile\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_gcm_logs".
 *
 * @property integer $id
 * @property integer $notification_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $to
 * @property string $title
 * @property string $body
 * @property string $request
 * @property string $response
 *
 * @property MobileNotification $notification
 */
class GcmLog extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%gcm_logs}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Gcm Log';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Gcm Logs';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => (new \DateTime())->format('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
            ['to', 'string'],
            ['title', 'string'],
            ['body', 'string'],
            ['request', 'string'],
            ['response', 'string'],
            ['notification_id', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'to' => 'To',
            'title' => 'Title',
            'body' => 'Body',
            'request' => 'Request',
            'response' => 'Response',
            'notification_id' => 'Notification Id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotification()
    {
        return $this->hasOne(MobileNotification::className(), ['id' => 'notification_id']);
    }
}
