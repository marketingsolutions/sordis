<?php

namespace modules\mobile\common\models;

use modules\mobile\common\models\Gcm;

class ApiGcm extends Gcm
{
    public function fields()
    {
        $fields = [
            'platform',
            'token',
            'profile_id',
        ];

        return $fields;
    }
}