<?php

use modules\mobile\common\models\GcmLog;
use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\FormBox;

/**
 * @var yii\web\View $this
 * @var modules\mobile\common\models\MobileNotification $model
 */
$this->title = 'Информация по пуш-уведомлению';
$this->params['breadcrumbs'][] = ['label' => modules\mobile\common\models\MobileNotification::modelTitlePlural(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

/** @var GcmLog[] $logs */
$logs = GcmLog::find()->orderBy(['ID' => SORT_ASC])->where(['notification_id' => $model->id])->all();
?>
<div class="mobile-notification-update">

	<div class="text-right">
        <?php Box::begin() ?>
        <?= ActionButtons::widget([
            'order' => [['index', 'return']],
            'addReturnUrl' => false,
        ]) ?>
        <?php Box::end() ?>
	</div>

    <?php $box = FormBox::begin(['cssClass' => 'mobile-notification-form box-primary', 'title' => 'Уведомление']) ?>
		<?= \yii\widgets\DetailView::widget([
            'model' => $model,
            'attributes' => [
                'title',
                'body',
                [
                    'label' => 'Кому',
                    'value' => $model->to(),
                ],
                'created_at:datetime', // creation date formatted as datetime
            ],
		]) ?>
    <?php FormBox::end() ?>

	<?php foreach ($logs as $log): ?>
        <?php $box = FormBox::begin(['cssClass' => 'mobile-notification-form box-primary', 'title' => 'Лог отправки GCM']) ?>
        <?= \yii\widgets\DetailView::widget([
            'model' => $log,
            'attributes' => [
            	'request',
                'response',
            ],
        ]) ?>
        <?php FormBox::end() ?>
	<?php endforeach; ?>
</div>
