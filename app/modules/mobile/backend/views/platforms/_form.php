<?php

use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var modules\mobile\common\models\Platform $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php $box = FormBox::begin(['cssClass' => 'platform-form box-primary', 'title' => '']) ?>
<?php $form = ActiveForm::begin(); ?>

<?php $box->beginBody() ?>
<?= $form->field($model, 'platform')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'file_local')->fileInput() ?>

<?php if ($model->file): ?>
    <?php
    $file = \Yii::getAlias('@data/platforms/' . $model->file);
    $modified = date ("d.m.Y H:i", filemtime($file));
    $bytes = filesize($file);
    $size = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
    $factor = floor((strlen($bytes) - 1) / 3);
    $fileSize = sprintf("%.2f", $bytes / pow(1024, $factor)) . @$size[$factor];
    ?>
	<div class="form-group field-platformform-name">
		<label class="control-label col-sm-2" for="platformform-name">Загруженный файл</label>
		<div class="col-sm-8">
			<a href="/data/platforms/<?= $model->file ?>" class="btn btn-success">Скачать</a>
			<span style="margin-left:30px">Размер: <?= $fileSize ?></span>
			<span style="margin-left:30px">Дата загрузки: <?= $modified ?></span>
		</div>
	</div>
<?php endif ?>

<?php $box->endBody() ?>

<?php $box->actions([
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
]) ?>
<?php ActiveForm::end(); ?>

<?php FormBox::end() ?>
