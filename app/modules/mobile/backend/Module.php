<?php

namespace modules\mobile\backend;

use yz\icons\Icons;

/**
 * Class Module
 */
class Module extends \modules\mobile\common\Module
{
    public function getAdminMenu()
    {
		return [
			[
				'label' => 'Мобильное приложение',
				'icon' => Icons::o('mobile-phone'),
				'items' => [
                    [
                        'route' => ['/mobile/mobile-notifications/index'],
                        'label' => 'Уведомления',
                        'icon' => Icons::o('envelope-open'),
                    ],
                    [
                        'route' => ['/mobile/gcm-log/index'],
                        'label' => 'Логи',
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/mobile/gcm/index'],
                        'label' => 'Устройства',
                        'icon' => Icons::o('mobile'),
                    ],
                    [
                        'route' => ['/mobile/platforms/index'],
                        'label' => 'Приложения',
                        'icon' => Icons::o('archive'),
                    ],
				]
			],
		];
    }

}