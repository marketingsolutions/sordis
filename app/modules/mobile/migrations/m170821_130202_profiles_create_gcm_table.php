<?php

use yii\db\Migration;

class m170821_130202_profiles_create_gcm_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%gcm}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'profile_id' => $this->integer(),
            'platform' => $this->string(),
            'api_key' => $this->string(),
            'token' => $this->string(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%gcm}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170821_130202_profiles_create_gcm_table cannot be reverted.\n";

        return false;
    }
    */
}
