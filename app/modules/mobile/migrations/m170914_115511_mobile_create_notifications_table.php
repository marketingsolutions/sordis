<?php

use yii\db\Migration;

/**
 * Class m170914_115511_mobile_create_notifications_table
 */
class m170914_115511_mobile_create_notifications_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%mobile_notifications}}', [
            'id' => $this->primaryKey(),
            'profile_id' => $this->integer(),
            'type' => $this->string(),
            'title' => $this->text(),
            'body' => $this->text(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%mobile_notifications}}');
    }
}
