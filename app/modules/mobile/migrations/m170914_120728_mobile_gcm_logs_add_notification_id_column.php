<?php

use yii\db\Migration;

/**
 * Class m170914_120728_mobile_gcm_logs_add_notification_id_column
 */
class m170914_120728_mobile_gcm_logs_add_notification_id_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%gcm_logs}}', 'notification_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%gcm_logs}}', 'notification_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170914_120728_mobile_gcm_logs_add_notification_id_column cannot be reverted.\n";

        return false;
    }
    */
}
