<?php

use yii\db\Migration;


class m170914_120729_mobile_gcm_logs_fk_notification_id_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_notification_id',
            '{{%gcm_logs}}', 'notification_id',
            '{{%mobile_notifications}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_notification_id', '{{%gcm_logs}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170914_120728_mobile_gcm_logs_add_notification_id_column cannot be reverted.\n";

        return false;
    }
    */
}
