<?php

use yii\db\Migration;

/**
 * Class m170906_145147_profiles_create_gcm_logs_table
 */
class m170906_145147_profiles_create_gcm_logs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%gcm_logs}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'to' => $this->text(),
            'title' => $this->text(),
            'body' => $this->text(),
            'request' => $this->text(),
            'response' => $this->text(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%gcm_logs}}');
    }
}
