<?php

use yii\db\Migration;

class m170628_152431_mobile_init extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%mobile_platforms}}', [
            'id' => $this->primaryKey(),
            'platform' => $this->string(),
            'name' => $this->string(),
            'file' => $this->string(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%mobile_platforms}}');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170628_152431_mobile_init cannot be reverted.\n";

        return false;
    }
    */
}
