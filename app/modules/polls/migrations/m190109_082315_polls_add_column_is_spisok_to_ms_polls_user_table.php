<?php

use yii\db\Migration;

/**
 * Class m190109_082315_polls_add_column_is_spisok_to_ms_polls_user_table
 */
class m190109_082315_polls_add_column_is_spisok_to_ms_polls_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%ms_polls_user}}', 'is_spisok', $this->boolean()->defaultValue(true));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%ms_polls_user}}', 'is_spisok');
    }
}
