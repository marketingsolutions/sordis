<?php

use yii\db\Migration;

/**
 * Class m181218_121129_poll_add_column_is_sms_and_mail_send
 */
class m181218_121129_poll_add_column_is_sms_and_mail_send extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%ms_polls_main}}', 'is_sms_send', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%ms_polls_main}}', 'is_email_send', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%ms_polls_main}}', 'is_sms_send');
        $this->dropColumn('{{%ms_polls_main}}', 'is_email_send');
    }
}
