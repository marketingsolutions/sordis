<?php

use yii\db\Migration;

/**
 * Class m181113_083251_polls_add_column_image_to_question_table
 */
class m181113_083251_polls_add_column_image_to_question_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%ms_polls_questions}}', 'image' , $this->binary());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%ms_polls_questions}}', 'image');
    }
}
