<?php

use yii\db\Migration;

/**
 * Class m181112_102346_polls_create_upload_polls_table
 */
class m181112_102346_polls_create_upload_polls_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%upload_polls_file}}', [
            'id' => $this->primaryKey(),
            'original_file_name' => $this->string(),
            'file_name' => $this->string(),
            'status_process' => $this->string(16),
            'created_at' => $this->dateTime(),
            'processed_at' => $this->dateTime(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%upload_polls_file}}');
    }
}
