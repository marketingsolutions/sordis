<?php

use yii\db\Migration;

/**
 * Class m181113_075702_polls_add_dordis_id_to_question_fnswer_table
 */
class m181113_075702_polls_add_dordis_id_to_question_fnswer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%ms_polls_questions}}', 'question_sordis_id', $this->string());
        $this->addColumn('{{%ms_polls_answers}}', 'answer_sordis_id', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%ms_polls_questions}}', 'question_sordis_id');
        $this->dropColumn('{{%ms_polls_answers}}', 'answer_sordis_id');
    }
}
