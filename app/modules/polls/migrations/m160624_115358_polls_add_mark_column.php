<?php

use yii\db\Migration;

class m160624_115358_polls_add_mark_column
	extends Migration
{
	public function safeUp() {
		$this->addColumn('{{%ms_polls_questions}}', 'allocate', $this->boolean()->notNull()->defaultValue(false));
	}

	public function safeDown() {
		$this->dropColumn('{{%ms_polls_questions}}', 'allocate');
	}
}
