<?php

use yii\db\Migration;

class m160615_115358_polls_init
	extends Migration
{
	public function safeUp() {

		$tableOptions = null;
		if($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%ms_polls_topics}}', [
			'id'    => $this->primaryKey(),
			'title' => $this->string(128)->notNull(),
		], $tableOptions);

		$this->createTable('{{%ms_polls_main}}', [
			'id'          => $this->primaryKey(),
			'created_at'  => $this->dateTime()->notNull(),
			'started_at'  => $this->dateTime(),
			'finished_at' => $this->dateTime(),
			'topic_id'    => $this->integer()->notNull(),
			'title'       => $this->string(128)->notNull(),
			'status'      => $this->integer()->notNull()->defaultValue(0),
			'prologue'    => $this->text(),
			'epilogue'    => $this->text(),
			'FOREIGN KEY (topic_id) REFERENCES {{%ms_polls_topics}} (id) ON DELETE CASCADE ON UPDATE CASCADE',
		], $tableOptions);
		//$this->createIndex('{{%ms_polls_main_idx_topic}}', '{{%ms_polls_main}}', 'topic_id');
		$this->createIndex('{{%ms_polls_main_idx_status}}', '{{%ms_polls_main}}', 'status');
		$this->createIndex('{{%ms_polls_main_idx_id_status}}', '{{%ms_polls_main}}', 'id, status');
		$this->createIndex('{{%ms_polls_main_idx_topic_status}}', '{{%ms_polls_main}}', 'topic_id, status');
		$this->createIndex('{{%ms_polls_main_idx_status_started_finished}}', '{{%ms_polls_main}}', 'status, started_at, finished_at');

		$this->createTable('{{%ms_polls_questions}}', [
			'id'               => $this->primaryKey(),
			'poll_id'          => $this->integer()->notNull(),
			'sequence'         => $this->integer()->notNull(),
			'question'         => $this->string(512)->notNull(),
			'answers_quantity' => $this->integer()->notNull()->defaultValue(1),
			'free_answer'      => $this->boolean()->notNull()->defaultValue(false),
			'UNIQUE (poll_id, id)',
			'UNIQUE (poll_id, sequence)',
			'FOREIGN KEY (poll_id) REFERENCES {{%ms_polls_main}} (id) ON DELETE CASCADE ON UPDATE CASCADE',
		], $tableOptions);
		//$this->createIndex('{{%ms_polls_questions_idx_poll}}', '{{%ms_polls_questions}}', 'poll_id');
		$this->createIndex('{{%ms_polls_questions_idx_id_poll_sequence}}', '{{%ms_polls_questions}}', 'id, poll_id, sequence');
		//$this->createIndex('{{%ms_polls_questions_idx_id_sequence}}', '{{%ms_polls_questions}}', 'id, sequence');

		$this->createTable('{{%ms_polls_answers}}', [
			'id'          => $this->primaryKey(),
			'question_id' => $this->integer()->notNull(),
			'sequence'    => $this->integer()->notNull(),
			'answer'      => $this->string(512)->notNull(),
			'UNIQUE (question_id, id)',
			'UNIQUE (question_id, sequence)',
			'FOREIGN KEY (question_id) REFERENCES {{%ms_polls_questions}} (id) ON DELETE CASCADE ON UPDATE CASCADE',
		], $tableOptions);
		//$this->createIndex('{{%ms_polls_answers_idx_question}}', '{{%ms_polls_answers}}', 'question_id');

		$this->createTable('{{%ms_polls_conditions}}', [
			'id'          => $this->primaryKey(),
			'question_id' => $this->integer()->notNull(),
			'answer_id'   => $this->integer()->notNull(),
			'show'        => $this->boolean()->notNull(),
			'UNIQUE (question_id, answer_id)',
			'FOREIGN KEY (question_id) REFERENCES {{%ms_polls_questions}} (id) ON DELETE CASCADE ON UPDATE CASCADE',
			'FOREIGN KEY (answer_id) REFERENCES {{%ms_polls_answers}} (id) ON DELETE CASCADE ON UPDATE CASCADE',
		], $tableOptions);

		$this->createTable('{{%ms_polls_users_polls}}', [
			'id'            => $this->bigPrimaryKey(),
			'created_at'    => $this->dateTime()->notNull(), // дата добавления
			'user_id'       => $this->bigInteger()->notNull(),
			'poll_id'       => $this->integer()->notNull(),
			'finished'      => $this->boolean()->notNull()->defaultValue(false),
			'last_question' => $this->integer(),
			'UNIQUE (user_id, poll_id)',
			//'FOREIGN KEY (user_id) REFERENCES {{%userauth_main}} (id) ON DELETE CASCADE ON UPDATE CASCADE',
			'FOREIGN KEY (poll_id) REFERENCES {{%ms_polls_main}} (id) ON DELETE CASCADE ON UPDATE CASCADE',
		], $tableOptions);
		$this->createIndex('{{%ms_polls_users_polls_idx_user}}', '{{%ms_polls_users_polls}}', 'user_id');
		$this->createIndex('{{%ms_polls_users_polls_idx_user_finished}}', '{{%ms_polls_users_polls}}', 'user_id, finished');
		$this->createIndex('{{%ms_polls_users_polls_idx_user_poll_finished}}', '{{%ms_polls_users_polls}}', 'user_id, poll_id, finished');

		$this->createTable('{{%ms_polls_users_answers}}', [
			'id'          => $this->bigPrimaryKey(),
			'created_at'  => $this->dateTime()->notNull(),
			'user_id'     => $this->bigInteger()->notNull(),
			'poll_id'     => $this->integer()->notNull(),
			'question_id' => $this->integer()->notNull(),
			'answer_id'   => $this->integer(),
			'free_answer' => $this->string(512),
			'UNIQUE (user_id, poll_id, question_id, answer_id)',
			//'FOREIGN KEY (user_id) REFERENCES {{%userauth_main}} (id) ON DELETE CASCADE ON UPDATE CASCADE',
			'FOREIGN KEY (poll_id) REFERENCES {{%ms_polls_main}} (id) ON DELETE CASCADE ON UPDATE CASCADE',
			'FOREIGN KEY (question_id) REFERENCES {{%ms_polls_questions}} (id) ON DELETE RESTRICT ON UPDATE CASCADE',
			'FOREIGN KEY (answer_id) REFERENCES {{%ms_polls_answers}} (id) ON DELETE RESTRICT ON UPDATE CASCADE',
			'FOREIGN KEY (user_id, poll_id) REFERENCES {{%ms_polls_users_polls}} (user_id, poll_id) ON DELETE CASCADE ON UPDATE CASCADE',
			'FOREIGN KEY (poll_id, question_id) REFERENCES {{%ms_polls_questions}} (poll_id, id) ON DELETE CASCADE ON UPDATE CASCADE',
			'FOREIGN KEY (question_id, answer_id) REFERENCES {{%ms_polls_answers}} (question_id, id) ON DELETE CASCADE ON UPDATE CASCADE',
		], $tableOptions);
		$this->createIndex('{{%ms_polls_users_answers_idx_user}}', '{{%ms_polls_users_answers}}', 'user_id');
		$this->createIndex('{{%ms_polls_users_answers_idx_user_poll}}', '{{%ms_polls_users_answers}}', 'user_id, poll_id');
		$this->createIndex('{{%ms_polls_users_answers_idx_user_poll_question}}', '{{%ms_polls_users_answers}}', 'user_id, poll_id, question_id');
	}

	public function safeDown() {
		$this->dropTable('{{%ms_polls_users_answers}}');
		$this->dropTable('{{%ms_polls_users_polls}}');
		$this->dropTable('{{%ms_polls_conditions}}');
		$this->dropTable('{{%ms_polls_answers}}');
		$this->dropTable('{{%ms_polls_questions}}');
		$this->dropTable('{{%ms_polls_main}}');
		$this->dropTable('{{%ms_polls_topics}}');
	}
}
