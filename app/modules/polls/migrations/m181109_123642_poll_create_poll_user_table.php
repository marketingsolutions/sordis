<?php

use yii\db\Migration;

/**
 * Class m181109_123642_poll_create_poll_user_table
 */
class m181109_123642_poll_create_poll_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%ms_polls_user}}', [
            'id' => $this->primaryKey(),
            'poll_id' => $this->string(),
            'phone_mobile' => $this->string(25),
            'code_egais' => $this->string(25),
            'inn' => $this->string(25),
            'kpp' => $this->string(25),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ms_polls_user}}');
    }
}
