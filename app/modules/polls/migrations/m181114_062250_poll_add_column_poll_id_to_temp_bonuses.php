<?php

use yii\db\Migration;

/**
 * Class m181114_062250_poll_add_column_poll_id_to_temp_bonuses
 */
class m181114_062250_poll_add_column_poll_id_to_temp_bonuses extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%temp_bonuses}}', 'poll_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%temp_bonuses}}', 'poll_id');
    }
}
