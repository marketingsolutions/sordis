<?php

use yii\db\Migration;

/**
 * Class m181109_114406_polls_add_column_to_poll_tables
 */
class m181109_114406_polls_add_column_to_poll_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%ms_polls_main}}', 'bonus_sum', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%ms_polls_main}}', 'bonus_sum');
    }
}
