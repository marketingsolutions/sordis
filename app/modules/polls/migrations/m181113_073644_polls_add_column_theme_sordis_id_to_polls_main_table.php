<?php

use yii\db\Migration;

/**
 * Class m181113_073644_polls_add_column_theme_sordis_id_to_polls_main_table
 */
class m181113_073644_polls_add_column_theme_sordis_id_to_polls_main_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%ms_polls_main}}', 'theme_sordis_id', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%ms_polls_main}}', 'theme_sordis_id');
    }
}
