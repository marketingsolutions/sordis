<?php
namespace modules\polls\console\controllers;
/**
 * Created by PhpStorm.
 * User: MihailKri
 * Date: 12.11.2018
 * Time: 15:17
 */
use console\base\Controller;
use modules\polls\common\models\Answer;
use modules\polls\common\models\Poll;
use modules\polls\common\models\PollsUser;
use modules\polls\common\models\Question;
use modules\polls\common\models\Topic;
use modules\polls\common\models\UploadPollsFile;
use yii\db\Exception;
use yii\db\Expression;
class AddNewPollController extends Controller
{
    public function actionIndex(){
        $processFile = UploadPollsFile::findOne(['status_process'=>UploadPollsFile::STATUS_NEW, 'processed_at' => null]);
        if(!$processFile){
            echo "Нет данных для обработки!\n";
            return false;
        }
        if (getenv('YII_ENV') == 'dev') {
            $xmlFilePath = \Yii::getAlias('@backend/web/upload_polls/' . $processFile->file_name);
        }else{
            $xmlFilePath = \Yii::getAlias('@data/upload_polls/' . $processFile->file_name);
        }
        $textXmlFile = simplexml_load_file($xmlFilePath);
        $now = date("Y-m-d H:i:s");

        $transaction = \Yii::$app->db->beginTransaction();
            $processFile->status_process = UploadPollsFile::STATUS_PROCESS;
            $processFile->update(false);
            $processFileId = $processFile->id;

            try {
               //Добавляем тех ТТ или Дистрибьютеров и соответствено привязанных участников, к которым применяется опрос
                $pollIdFromSordis = $textXmlFile->QuestionnaireID;  //ID опроса в системе Сордис

                //Добавляем тему опроса
                $topic = new Topic();
                $topic->title = $textXmlFile->QuestionnaireName;
                $topic->save(false);

                //Добавляем новый опрос
                $newPoll = new Poll();
                $newPoll->created_at = $now;
                $newPoll->topic_id = $topic->id;
                $newPoll->title = $topic->title;
                $newPoll->theme_sordis_id = $pollIdFromSordis;
                $newPoll->status = 2;
                $newPoll->bonus_sum = $textXmlFile->QuestionnairePoints;
                $newPoll->started_at = date("Y-m-d H:i:s", strtotime($textXmlFile->QuestionnaireDateTime));
                $newPoll->finished_at = date("Y-m-d H:i:s", strtotime($textXmlFile->QuestionnaireEndDateTime));
                $newPoll->save(false);
                $poll_id = $newPoll->id;

                //Заносим участников, которым показываем опрос в ЛК
                foreach ($textXmlFile->QuestionnaireParticipants->QuestionnaireParticipant as $dataUser){
                    $phone_mobile = '+' . $dataUser->Participant;
                    //foreach ($dataUser->Parts->Part as $part) {
                        $newUser = new PollsUser();
                        $newUser->poll_id = $poll_id;
                        $newUser->phone_mobile = $phone_mobile;
                        //$newUser->code_egais = $part->FSRARID;
                        //$newUser->inn = $part->INN;
                        //$newUser->kpp = $part->KPP;
                        $newUser->save(false);
                   // }
                }

                //Заносим вопросы и ответы для опроса
                $sequence = 0;
                foreach ($textXmlFile->QuestionnaireQuestions->Question as $quest){
                    $sequence++;
                    $newQuestion = new Question();
                    $newQuestion->poll_id = $poll_id;
                    $newQuestion->sequence = $sequence;
                    $newQuestion->question = $quest->QuestionName;
                    $newQuestion->question_sordis_id = $quest->QuestionID;
                    if(isset($quest->QuestionImg) && !empty($quest->QuestionImg)){
                        $newQuestion->image = base64_decode($quest->QuestionImg);
                    }
                    $newQuestion->save(false);
                    $newQuestionId = $newQuestion->id;
                    $seq = 0;
                    if($quest->QuestionType == 'Var'){
                        foreach ($quest->QuestionAnswerVariants->Variant as $var) {
                            $seq++;
                            $newAnswer = new Answer();
                            $newAnswer->question_id = $newQuestionId;
                            $newAnswer->sequence = $seq;
                            $newAnswer->answer = $var->AnswerName;
                            $newAnswer->answer_sordis_id = $var->AnswerID;
                            $newAnswer->save(false);
                        }
                        $addDataToQuestion = Question::findOne(['id' => $newQuestionId]);
                        $addDataToQuestion->answers_quantity = $seq;
                        $addDataToQuestion->save(false);

                    }elseif($quest->QuestionType == 'Dig' || $quest->QuestionType == 'Str'){
                        $addDataToQuestion = Question::findOne(['id' => $newQuestionId]);
                        $addDataToQuestion->free_answer = 1;
                        $addDataToQuestion->answers_quantity = 0;
                        $addDataToQuestion->save(false);
                    }
                }
            $transaction->commit();
            print_r('Опрос успешно создан!');
            $processedFile = UploadPollsFile::findOne($processFile->id);
            $processedFile->status_process = UploadPollsFile::STATUS_COMPLETE;
            $processedFile->processed_at = date("Y-m-d H:i:s");
            $processedFile->update(false);
        }catch (\Exception $e) {
            $transaction->rollBack();
            $addStatus = UploadPollsFile::findOne(['id' => $processFileId]);
            $addStatus->status_process = UploadPollsFile::STATUS_ERROR;
            $addStatus->update(false);
            throw $e;
        }
    }
}