<?php

namespace modules\polls\common\models;

use Yii;

/**
 * This is the model class for table "{{%ms_polls_answers}}".
 *
 * @property integer      $id
 * @property integer      $question_id
 * @property integer      $sequence
 * @property string       $answer
 * @property string       $answer_sordis_id
 *
 * @property Question     $question
 * @property Condition[]  $conditions
 * @property Question[]   $conditionsQuestions
 * @property UserAnswer[] $usersAnswers
 * @property UserPoll[]   $usersPolls
 */
class Answer
	extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%ms_polls_answers}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['question_id', 'answer'], 'required'],
			[['question_id', 'sequence'], 'integer'],
			[['answer'], 'string', 'max' => 512],
			[
				['question_id', 'sequence'],
				'unique',
				'targetAttribute' => ['question_id', 'sequence'],
				'message'         => 'The combination of Question ID and Sequence has already been taken.',
			],
			[['question_id'], 'exist', 'targetClass' => Question::className(), 'targetAttribute' => ['question_id' => 'id']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'          => 'ID',
			'question_id' => 'ID вопроса',
			'sequence'    => 'Порядок',
			'answer'      => 'Ответ',
		];
	}

	public function beforeValidate() {
		if($this->isNewRecord) {
			$max = (int)self::find()->where(['question_id' => $this->question_id])->max('sequence');
			$this->sequence = $max + 1;
		}

		return parent::beforeValidate();
	}

	public function afterDelete() {
		self::updateAllCounters(['sequence' => -1], ['and', ['question_id' => $this->question_id], ['>', 'sequence', $this->sequence]]);
		parent::afterDelete();
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getQuestion() {
		return $this->hasOne(Question::className(), ['id' => 'question_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getConditions() {
		return $this->hasMany(Condition::className(), ['answer_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getConditionsQuestions() {
		return $this->hasMany(Question::className(), ['id' => 'question_id'])->viaTable('conditions', ['answer_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUsersAnswers() {
		return $this->hasMany(UserAnswer::className(), ['question_id' => 'question_id', 'answer_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUsersPolls() {
		return $this->hasMany(UserPoll::className(), ['user_id' => 'user_id', 'poll_id' => 'poll_id',])->via('usersAnswers');
	}
}
