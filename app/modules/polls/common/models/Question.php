<?php

namespace modules\polls\common\models;

use modules\profiles\common\models\Profile;
use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%ms_polls_questions}}".
 *
 * @property integer $id
 * @property integer $poll_id
 * @property integer $sequence
 * @property string $question
 * @property string $image
 * @property integer $answers_quantity
 * @property boolean $free_answer
 * @property boolean $allocate
 *
 * @property Answer[] $answers
 * @property Condition[] $conditions
 * @property Answer[] $conditionsAnswers
 * @property Poll $poll
 * @property UserAnswer[] $usersAnswers
 */
class Question
    extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ms_polls_questions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['answers_quantity'], 'default', 'value' => 1],
            [['poll_id', 'question'], 'required'],
            [['poll_id', 'sequence', 'answers_quantity'], 'integer'],
            [['free_answer', 'allocate'], 'default', 'value' => false],
            [['free_answer', 'allocate'], 'boolean'],
            [['question'], 'string', 'max' => 512],
            [
                ['poll_id', 'sequence'],
                'unique',
                'targetAttribute' => ['poll_id', 'sequence'],
                'message' => 'The combination of Poll ID and Sequence has already been taken.',
            ],
            [['poll_id'], 'exist', 'targetClass' => Poll::className(), 'targetAttribute' => ['poll_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'poll_id' => 'ID Опроса',
            'sequence' => 'Порядок',
            'question' => 'Вопрос',
            'answers_quantity' => 'Допустимо ответов',
            'free_answer' => 'Свободный ответ',
            'allocate' => 'Выделить',
            //
            'countAnswers' => 'Всего ответов',
            'countConditions' => 'Условия',
        ];
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $max = (int) self::find()->where(['poll_id' => $this->poll_id])->max('sequence');
            $this->sequence = $max + 1;
        }

        return parent::beforeValidate();
    }

    public function afterDelete()
    {
        self::updateAllCounters(['sequence' => -1], ['and', ['poll_id' => $this->poll_id], ['>', 'sequence', $this->sequence]]);
        parent::afterDelete();
    }

    public static function getUserNameFromId($id)
    {
        $model = Profile::findOne($id);

        return $model['full_name'];
    }

    public function getResultTotal()
    {
        $result = [];
        $free = [];
        if (!empty($this->answers)) {
            foreach ($this->answers as $item) {
                $result[$item->id] = ['answer' => $item->answer, 'count' => 0];
            }
        }
        if (!empty($this->usersAnswers)) {
            foreach ($this->usersAnswers as $item) {
                if ($item->answer_id) {
                    $result[$item->answer_id]['count']++;
                }
                else {
                    $free[] = $item->free_answer . "///" . $item->user_id;
                }
            }
            foreach ($free as $item) {
                $result[] = ['answer' => $item, 'count' => 1];
            }
        }
        return (empty($result) ? null : $result);
    }

    public function getResultUser($user)
    {
        if (empty($this->usersAnswers)) {
            return null;
        }
        $result = [];
        /* @var $answers UserAnswer[] */
        $answers = $this->getUsersAnswers()->andWhere(['user_id' => $user])->orderBy('id')->all();
        foreach ($answers as $item) {
            $result[] = ($item->answer_id ? $item->answer->answer : $item->free_answer);
        }

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(Answer::className(), ['question_id' => 'id'])->orderBy('sequence');
    }

    public function getCountAnswers()
    {
        return $this->getAnswers()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConditions()
    {
        return $this->hasMany(Condition::className(), ['question_id' => 'id']);
    }

    public function getCountConditions()
    {
        return $this->getConditions()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConditionsAnswers()
    {
        return $this->hasMany(Answer::className(), ['id' => 'answer_id'])->via('conditions');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll()
    {
        return $this->hasOne(Poll::className(), ['id' => 'poll_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersAnswers()
    {
        return $this->hasMany(UserAnswer::className(), ['poll_id' => 'poll_id', 'question_id' => 'id']);
    }
}
