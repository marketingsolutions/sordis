<?php

namespace modules\polls\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\db\ActiveRecord;

/**
 * This is the model class for table "{{%ms_polls_users_answers}}".
 *
 * @property string   $id
 * @property string   $created_at
 * @property string   $user_id
 * @property integer  $poll_id
 * @property integer  $question_id
 * @property integer  $answer_id
 * @property string   $free_answer
 *
 * @property Poll     $poll
 * @property Question $question
 * @property Answer   $answer
 * @property UserPoll $userPoll
 * @property User     $user
 *
 */
class UserAnswer
	extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%ms_polls_users_answers}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['user_id', 'poll_id', 'question_id'], 'required'],
			[['user_id', 'poll_id', 'question_id', 'answer_id'], 'integer'],
			[['free_answer'], 'string', 'max' => 512],
			[
				['user_id', 'poll_id', 'question_id', 'answer_id'],
				'unique',
				'targetAttribute' => ['user_id', 'poll_id', 'question_id', 'answer_id'],
				'message'         => 'The combination of User ID, Poll ID, Question ID and Answer ID has already been taken.',
			],
			[['poll_id'], 'exist', 'targetClass' => Poll::className(), 'targetAttribute' => ['poll_id' => 'id']],
			[
				['user_id', 'poll_id'],
				'exist',
				'targetClass'     => UserPoll::className(),
				'targetAttribute' => ['user_id' => 'user_id', 'poll_id' => 'poll_id'],
			],
			[
				['poll_id', 'question_id'],
				'exist',
				'targetClass'     => Question::className(),
				'targetAttribute' => ['poll_id' => 'poll_id', 'question_id' => 'id'],
			],
			[
				['question_id', 'answer_id'],
				'exist',
				'targetClass'     => Answer::className(),
				'targetAttribute' => ['question_id' => 'question_id', 'answer_id' => 'id'],
				'when'            => function ($model) { return ($model->question_id && $model->answer_id); },
			],
			[['user_id'], 'exist', 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
		];
	}

	public function behaviors() {
		return [
			'timestamp' => [
				'class'      => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
				],
				'value'      => new Expression('NOW()'),
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'          => 'ID',
			'created_at'  => 'Создан',
			'user_id'     => 'ID пользователя',
			'poll_id'     => 'ID опроса',
			'question_id' => 'ID вопроса',
			'answer_id'   => 'ID ответа',
			'free_answer' => 'Свободный ответ',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPoll() {
		return $this->hasOne(Poll::className(), ['id' => 'poll_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getQuestion() {
		return $this->hasOne(Question::className(), ['id' => 'question_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAllAnswers() {
		return $this->question->getResultUser($this->user_id);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAnswer() {
		return $this->hasOne(Answer::className(), ['id' => 'answer_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUserPoll() {
		return $this->hasOne(UserPoll::className(), ['user_id' => 'user_id', 'poll_id' => 'poll_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
}
