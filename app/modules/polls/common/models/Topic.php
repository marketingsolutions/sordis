<?php

namespace modules\polls\common\models;

use Yii;

/**
 * This is the model class for table "{{%ms_polls_topics}}".
 *
 * @property integer $id
 * @property string  $title
 *
 * @property Poll[]  $polls
 */
class Topic
	extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%ms_polls_topics}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['title'], 'required'],
			[['title'], 'string', 'max' => 128],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'         => 'ID',
			'title'      => 'Название',
			'countPolls' => 'Опросы',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPolls() {
		return $this->hasMany(Poll::className(), ['topic_id' => 'id']);
	}

	public function getCountPolls() {
		return $this->getPolls()->count();
	}
}
