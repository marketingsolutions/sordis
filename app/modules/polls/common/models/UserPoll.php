<?php

namespace modules\polls\common\models;

use modules\bonuses\common\models\TempBonuses;
use modules\profiles\common\models\Profile;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\db\ActiveRecord;

/**
 * This is the model class for table "{{%ms_polls_users_polls}}".
 *
 * @property string $id
 * @property string $created_at
 * @property string $user_id
 * @property integer $poll_id
 * @property boolean $finished
 * @property integer $last_question
 *
 * @property UserAnswer[] $answers
 * @property Poll $poll
 * @property Profile $profile
 */
class UserPoll extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ms_polls_users_polls}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['finished'], 'default', 'value' => false],
            [['last_question'], 'default', 'value' => 0],
            [['user_id', 'poll_id'], 'required'],
            [['user_id', 'poll_id', 'last_question'], 'integer'],
            [['finished'], 'boolean'],
            [
                ['user_id', 'poll_id'],
                'unique',
                'targetAttribute' => ['user_id', 'poll_id'],
                'message' => 'The combination of User ID and Poll ID has already been taken.',
            ],
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создан',
            'user_id' => 'ID пользователя',
            'poll_id' => 'ID опроса',
            'finished' => 'Закончен',
            'last_question' => 'Последний вопрос',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll()
    {
        return $this->hasOne(Poll::className(), ['id' => 'poll_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(UserAnswer::className(), ['user_id' => 'user_id', 'poll_id' => 'poll_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'user_id']);
    }

    /**
     * Добавление социальных баллов за прохождение опроса
     * @param $profileId
     * @param $pollId
     * @return bool
     */
    public static function addSocialBonus($profileId, $pollId){
        //Добавляем социальные баллы, если участник прошел тест целиком
        $profile = Profile::findOne(['id' => $profileId]);
        $bonusSum = Poll::findOne(['id' => $pollId]);
        $isFinished = UserPoll::findOne(['user_id' => $profileId, 'poll_id' => $pollId]);
        if(!$profile){
            return false;
        }
         if(!$bonusSum){
            return false;
         }

        if(!$isFinished){
             return false;
         }
         if($isFinished->finished == false){
             return false;
         }
        $addSocialBonus = new TempBonuses();
        $addSocialBonus->bonus_type = TempBonuses::SOCIAL_BONUS;
        $addSocialBonus->period = date("Y-m")."-01";
        $addSocialBonus->bonus_sum = $bonusSum->bonus_sum;
        $addSocialBonus->created_at = date("Y-m-d H:i:s");
        $addSocialBonus->phone_mobile = $profile->phone_mobile;
        $addSocialBonus->poll_id = $pollId;
        $addSocialBonus->save(false);
        return true;
    }


    /**
     * Проверка участия в опросе (Учавствуют либо все либо тот кто в списке)
     * @param $profileId
     * @param $pollId
     * @return bool
     */
    public static function isPollUser($profileId, $pollId){
        $profile = Profile::findOne(['id' => $profileId]);
        if(!$profile){
            return false;
        }else{
            $spisokInPoll = PollsUser::find()->where(['phone_mobile' => $profile->phone_mobile, 'poll_id' => $pollId])->all();
            $isPolls = PollsUser::find()->where(['poll_id' => $pollId, 'is_spisok' => true])->all();

           if($spisokInPoll || !$isPolls){
               return true;
           }else{
               return false;
           }
        }
    }
}
