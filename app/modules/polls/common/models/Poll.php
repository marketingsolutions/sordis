<?php

namespace modules\polls\common\models;

use modules\profiles\common\models\Profile;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yz\admin\mailer\common\lists\ManualMailList;
use yz\admin\mailer\common\models\Mail;
use yz\db\ActiveRecord;
use modules\sms\common\models\Sms;

/**
 * This is the model class for table "{{%ms_polls_main}}".
 *
 * @property integer $id
 * @property integer $bonus_sum
 * @property string $created_at
 * @property string $theme_sordis_id
 * @property string $started_at
 * @property string $finished_at
 * @property integer $topic_id
 * @property string $title
 * @property integer $status
 * @property string $prologue
 * @property string $epilogue
 *
 * @property Topic $topic
 * @property Question[] $questions
 * @property UserAnswer[] $usersAnswers
 * @property UserPoll[] $usersPolls
 * @property User[] $users
 */
class Poll extends \yii\db\ActiveRecord
{
    public $mapStatuses
        = [
            1 => [1, 2],
            2 => [2, 3],
            3 => [3, 4, 5],
            4 => [3, 4, 5],
            5 => [5],
        ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ms_polls_main}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['started_at', 'finished_at', 'is_sms_send', 'is_email_send'], 'safe'],
            [['topic_id', 'title'], 'required'],
            [['topic_id', 'status'], 'integer'],
            ['bonus_sum', 'integer'],
            ['bonus_sum', 'required'],
            [['prologue', 'epilogue', 'theme_sordis_id'], 'string'],
            [['title'], 'string', 'max' => 128],
            [['topic_id'], 'exist', 'targetClass' => Topic::className(), 'targetAttribute' => ['topic_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создан',
            'started_at' => 'Начинается',
            'finished_at' => 'Заканчивается',
            'topic_id' => 'ID Темы',
            'title' => 'Название',
            'status' => 'Статус',
            'prologue' => 'Пролог',
            'epilogue' => 'Эпилог',
            'statusName' => 'Статус',
            'countQuestions' => 'Вопросов',
            'countUsers' => 'Пользователей',
            'countStarted' => 'Начавшие',
            'countFinished' => 'Закончившие',
            'is_sms_send' => 'Приглашение отправлено по SMS',
            'is_email_send' => 'Приглашение отправлено на Email',
            'bonus_sum' => 'Сумма бонусов за прохождение',
            'theme_sordis_id' => 'ID опроса в системе Sordis',
        ];
    }

    /**
     * @param integer $user
     *
     * @return \yii\db\ActiveQuery
     */
    public static function findActive($user = null)
    {
        $now = new Expression('NOW()');
        $query = self::find()->where(['and', ['status' => 3], ['<', 'started_at', $now], ['or', ['finished_at' => null], ['>', 'finished_at', $now]]]);

        if ($user) {
            $userPolls = UserPoll::findAll(['user_id' => $user, 'finished' => true]);
            $polls = ArrayHelper::getColumn($userPolls, 'poll_id');
            $query->andFilterWhere(['not in', 'id', $polls]);
        }

        return $query;
    }

    public static function getTopics()
    {
        return ArrayHelper::map(Topic::find()->orderBy('title')->all(), 'id', 'title');
    }

    public static function getStatuses()
    {
        return [
            1 => 'Создается',
            2 => 'Создан',
            3 => 'Запущен',
            4 => 'Приостановлен',
            5 => 'Закрыт',
        ];
    }

    public function getStatusName()
    {
        return self::getStatuses()[$this->status];
    }

    public function getPossibleStatus()
    {
        $map = (isset($this->mapStatuses[$this->status]) ? $this->mapStatuses[$this->status] : [1]);
        $result = [];
        foreach ($map as $v) {
            $result[$v] = $this->getStatuses()[$v];
        }

        return $result;
    }

    public function canChangeStart()
    {
        return in_array($this->status, [1, 2]);
    }

    public function canChangeFinish()
    {
        return in_array($this->status, [1, 2, 3, 4]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopic()
    {
        return $this->hasOne(Topic::className(), ['id' => 'topic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['poll_id' => 'id']);
    }

    public function getCountQuestions()
    {
        return $this->getQuestions()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersAnswers()
    {
        return $this->hasMany(UserAnswer::className(), ['poll_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersPolls()
    {
        return $this->hasMany(UserPoll::className(), ['poll_id' => 'id']);
    }

    public function getCountUsers()
    {
        return $this->getUsersPolls()->count();
    }

    public function getCountStarted()
    {
        return $this->getUsersPolls()->where(['finished' => false])->count();
    }

    public function getCountFinished()
    {
        return $this->getUsersPolls()->where(['finished' => true])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->via('usersPolls');
    }

    /**
     * @param $pollId
     * @return bool|string
     */
    public static function getArrXml($pollId)
    {

        $poll_user = PollsUser::find()
            ->select('{{%ms_polls_user}}.phone_mobile as Participant, {{%ms_polls_main}}.theme_sordis_id as QuestionnaireID, {{%ms_polls_main}}.bonus_sum as QuestionnairePoints')
            ->leftJoin('{{%ms_polls_main}}', '{{%ms_polls_main}}.id = {{%ms_polls_user}}.poll_id')
            ->where(['{{%ms_polls_main}}.id' => $pollId])
            ->asArray()
            ->all();

        $xml = new \DOMDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xmlQuestionnaireParticipants = $xml->createElement('QuestionnaireParticipants');
        $xml->appendChild($xmlQuestionnaireParticipants);

        foreach ($poll_user as $user) {
            $profile = Profile::findOne(['phone_mobile' => $user['Participant']]);
            if (!$user) {
                return false;
            }
            $xmlQuestionnaireParticipant = $xml->createElement('QuestionnaireParticipant');
            $xmlQuestionnaireParticipants->appendChild($xmlQuestionnaireParticipant);

            $xmlParticipant = $xml->createElement('Participant', $profile->phone_mobile);
            $xmlQuestionnaireParticipant->appendChild($xmlParticipant);

            $xmlQuestionnaireID = $xml->createElement('QuestionnaireID', $user['QuestionnaireID']);
            $xmlQuestionnaireParticipant->appendChild($xmlQuestionnaireID);

            $expirationDate = UserAnswer::find()->select('created_at')->where(['poll_id' => $pollId, 'user_id' => $profile->id])->orderBy(['created_at' => SORT_DESC])->asArray()->one();
            $xmlExpirationDate = $xml->createElement('ExpirationDate', $expirationDate['created_at']);
            $xmlQuestionnaireParticipant->appendChild($xmlExpirationDate);

            $xmlQuestionnairePoints = $xml->createElement('QuestionnairePoints', $user['QuestionnairePoints']);
            $xmlQuestionnaireParticipant->appendChild($xmlQuestionnairePoints);

            $questions = Question::find()
                ->select('{{%ms_polls_questions}}.question_sordis_id as QuestionID, {{%ms_polls_answers}}.answer_sordis_id as AnswerID, {{%ms_polls_users_answers}}.free_answer as freeAnswer')
                ->leftJoin('{{%ms_polls_users_answers}}', '{{%ms_polls_users_answers}}.question_id = {{%ms_polls_questions}}.id')
                ->leftJoin('{{%ms_polls_answers}}', '{{%ms_polls_users_answers}}.answer_id = {{%ms_polls_answers}}.id')
                ->where(['{{%ms_polls_questions}}.poll_id' => $pollId, '{{%ms_polls_users_answers}}.user_id' => $profile->id])
                ->asArray()
                ->all();
            $xmlQuestionAnswers = $xml->createElement('QuestionAnswers');
            $xmlQuestionnaireParticipant->appendChild($xmlQuestionAnswers);
            foreach ($questions as $questionAnswer) {
                $xmlQuestionAnswer = $xml->createElement('QuestionAnswer');
                $xmlQuestionAnswers->appendChild($xmlQuestionAnswer);

                $xmlQuestionID = $xml->createElement('QuestionID', $questionAnswer['QuestionID']);
                $xmlQuestionAnswer->appendChild($xmlQuestionID);
                empty($questionAnswer['AnswerID']) ? $xmlAnswerID = $xml->createElement('AnswerID') : $xmlAnswerID = $xml->createElement('AnswerID', $questionAnswer['AnswerID']);
                $xmlQuestionAnswer->appendChild($xmlAnswerID);


                empty($questionAnswer['freeAnswer']) ? $xmlFreeAnswer = $xml->createElement('FreeAnswer') : $xmlFreeAnswer = $xml->createElement('FreeAnswer', $questionAnswer['freeAnswer']);
                $xmlQuestionAnswer->appendChild($xmlFreeAnswer);
            }
        }
        return $xml->saveXML();
    }


    public static function getArrXmlConsole($pollId)
    {

        $poll_user = PollsUser::find()
            ->select('{{%ms_polls_user}}.phone_mobile as Participant, {{%ms_polls_main}}.theme_sordis_id as QuestionnaireID, {{%ms_polls_main}}.bonus_sum as QuestionnairePoints')
            ->leftJoin('{{%ms_polls_main}}', '{{%ms_polls_main}}.id = {{%ms_polls_user}}.poll_id')
            ->where(['{{%ms_polls_main}}.id' => $pollId])
            ->asArray()
            ->all();

        $xml = new \DOMDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xmlQuestionnaireParticipants = $xml->createElement('QuestionnaireParticipants');
        $xml->appendChild($xmlQuestionnaireParticipants);

        foreach ($poll_user as $user) {

            $profile = Profile::findOne(['phone_mobile' => $user['Participant']]);
            if (!$user) {
                return false;
            }
            if(!$profile){
                continue;
            }
            $xmlQuestionnaireParticipant = $xml->createElement('QuestionnaireParticipant');
            $xmlQuestionnaireParticipants->appendChild($xmlQuestionnaireParticipant);

            $xmlParticipant = $xml->createElement('Participant', $profile->phone_mobile);
            $xmlQuestionnaireParticipant->appendChild($xmlParticipant);

            $xmlQuestionnaireID = $xml->createElement('QuestionnaireID', $user['QuestionnaireID']);
            $xmlQuestionnaireParticipant->appendChild($xmlQuestionnaireID);

            $expirationDate = UserAnswer::find()->select('created_at')->where(['poll_id' => $pollId, 'user_id' => $profile->id])->orderBy(['created_at' => SORT_DESC])->asArray()->one();
            $xmlExpirationDate = $xml->createElement('ExpirationDate', $expirationDate['created_at']);
            $xmlQuestionnaireParticipant->appendChild($xmlExpirationDate);

            $xmlQuestionnairePoints = $xml->createElement('QuestionnairePoints', $user['QuestionnairePoints']);
            $xmlQuestionnaireParticipant->appendChild($xmlQuestionnairePoints);

            $questions = Question::find()
                ->select('{{%ms_polls_questions}}.question_sordis_id as QuestionID, {{%ms_polls_answers}}.answer_sordis_id as AnswerID, {{%ms_polls_users_answers}}.free_answer as freeAnswer')
                ->leftJoin('{{%ms_polls_users_answers}}', '{{%ms_polls_users_answers}}.question_id = {{%ms_polls_questions}}.id')
                ->leftJoin('{{%ms_polls_answers}}', '{{%ms_polls_users_answers}}.answer_id = {{%ms_polls_answers}}.id')
                ->where(['{{%ms_polls_questions}}.poll_id' => $pollId, '{{%ms_polls_users_answers}}.user_id' => $profile->id])
                ->asArray()
                ->all();
            $xmlQuestionAnswers = $xml->createElement('QuestionAnswers');
            $xmlQuestionnaireParticipant->appendChild($xmlQuestionAnswers);
            foreach ($questions as $questionAnswer) {
                $xmlQuestionAnswer = $xml->createElement('QuestionAnswer');
                $xmlQuestionAnswers->appendChild($xmlQuestionAnswer);

                $xmlQuestionID = $xml->createElement('QuestionID', $questionAnswer['QuestionID']);
                $xmlQuestionAnswer->appendChild($xmlQuestionID);
                empty($questionAnswer['AnswerID']) ? $xmlAnswerID = $xml->createElement('AnswerID') : $xmlAnswerID = $xml->createElement('AnswerID', $questionAnswer['AnswerID']);
                $xmlQuestionAnswer->appendChild($xmlAnswerID);


                empty($questionAnswer['freeAnswer']) ? $xmlFreeAnswer = $xml->createElement('FreeAnswer') : $xmlFreeAnswer = $xml->createElement('FreeAnswer', $questionAnswer['freeAnswer']);
                $xmlQuestionAnswer->appendChild($xmlFreeAnswer);
            }
        }
        return $xml;
    }

    /**
     * Отправка e-mail оповещений для опросов
     * @param $poll_id
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public static function sendEmail($poll_id){
        $model = self::findOne(['id' => $poll_id]);
        if(!$model){
            return false;
        }
        $profile = Profile::find()->where(['no_send_email' => true])->asArray()->all();
        $arrUser = "";
        foreach ($profile as $user){
            $arrUser .= $user['email'].";";
        }
        $arrUserSend = trim($arrUser, ';');
        $theme = $model->title;
        $dateFrom = $model->started_at;
        $dateTo = $model->finished_at;
        $transaction = \Yii::$app->db->beginTransaction();
        $send = new Mail();
        $send->status = Mail::STATUS_WAITING;
        $send->receivers_provider = ManualMailList::className();
        $send->receivers_provider_data = json_encode(['to' => $arrUserSend]);
        $send->subject = 'Приглашение к участию в опросе';
        $send->body_html = '<div class="row">
                <div class="col-md-12"><h3 align="center">Приглашаем Вас принять участие в опросе!</h3></div>
            </div>
            <div class="row">
                <div class="col-md-4">Тема опроса:</div>
                <div class="col-md-8">'.$theme.'</div>
            </div>
            <div class="row">
                <div class="col-md-4">Период проведения:</div>
                <div class="col-md-8"> C '.$dateFrom.' по '.$dateTo.'</div>
            </div>
            <div class="row">
                
                <div class="col-md-12">Для того, чтобы более не получать письма автоматической E-mail рассылки пройдите по <a href="'.getenv('FRONTEND_WEB').'profiles/profile/index">этой ссылке</a> и отключите её в настройках</div>
            </div>';
        $send->created_at = date("Y-m-d H:i:s");
        $send->save(false);
        $model->is_email_send = 1;
        $model->update(false);
        $send->updateAttributes(['status' => Mail::STATUS_WAITING]);
        $transaction->commit();
        return true;
    }

    /**
     * Отправка СМС-оповещений для опросов
     * @param $poll_id
     * @return bool
     */
    public static function sendSms($poll_id){
        $model = self::findOne(['id' => $poll_id]);
        if(!$model){
            return false;
        }
        $profile = Profile::find()->asArray()->all();
        $arrUser = "";
        foreach ($profile as $user){
            $arrUser .= $user['phone_mobile']."; ";
        }

        $transaction = \Yii::$app->db->beginTransaction();

        $arrUserSend = trim($arrUser, '; ');
        $newSms = new Sms();
        $newSms->status = Sms::STATUS_NEW;
        $newSms->type = Sms::TYPE_INDIVIDUAL;
        $newSms->to = $arrUserSend;
        $newSms->message = "Приглашаем принять участие в опросе. Подробности на портале ".getenv('FRONTEND_WEB');
        $newSms->created_at = date("Y-m-d H:i:s");
        $newSms->save(false);
        $model->updateAttributes(['is_sms_send' => true]);

        $transaction->commit();
        return true;
    }
}
