<?php

namespace modules\polls\common\models;

use Yii;

/**
 * This is the model class for table "{{%ms_polls_conditions}}".
 *
 * @property integer  $id
 * @property integer  $question_id
 * @property integer  $answer_id
 * @property boolean  $show
 *
 * @property Answer   $answer
 * @property Question $question
 */
class Condition
	extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%ms_polls_conditions}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['show'], 'default', 'value' => true],
			[['question_id', 'answer_id'], 'required'],
			[['question_id', 'answer_id'], 'integer'],
			[['show'], 'boolean'],
			[
				['question_id', 'answer_id'],
				'unique',
				'targetAttribute' => ['question_id', 'answer_id'],
				'message'         => 'The combination of Question ID and Answer ID has already been taken.',
			],
			[['question_id'], 'exist', 'targetClass' => Question::className(), 'targetAttribute' => ['question_id' => 'id']],
			[['answer_id'], 'exist', 'targetClass' => Answer::className(), 'targetAttribute' => ['answer_id' => 'id']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'          => 'ID',
			'question_id' => 'ID вопроса',
			'answer_id'   => 'ID ответа',
			'show'        => 'Показывать',
		];
	}

	public function getPossibleOptions() {
		/* @var $question Question */
		$question = Question::findOne($this->question_id);
		$data = [];
		if($question) {
			/* @var $questions Question[] */
			$questions = Question::find()->where([
				'and',
				['!=', 'id', $question->id],
				['poll_id' => $question->poll_id],
				['<', 'sequence', $question->sequence],
			])->orderBy('sequence')->all();
			foreach($questions as $q) {
				if(is_array($q->answers)) {
					foreach($q->answers as $a) {
						$data[$a->id] = $q->question . ' - ' . $a->answer;
					}
				}
			}
		}

		return $data;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getQuestion() {
		return $this->hasOne(Question::className(), ['id' => 'question_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAnswer() {
		return $this->hasOne(Answer::className(), ['id' => 'answer_id']);
	}
}
