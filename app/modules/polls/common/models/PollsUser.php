<?php

namespace modules\polls\common\models;

use modules\profiles\common\models\Profile;
use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_ms_polls_user".
 *
 * @property integer $id
 * @property integer $is_spisok
 * @property string $poll_id
 * @property string $phone_mobile
 * @property string $code_egais
 * @property string $inn
 * @property string $kpp
 */
class PollsUser extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ms_polls_user}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Polls User';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Участники опросов';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['poll_id', 'string', 'max' => 255],
            ['phone_mobile', 'string', 'max' => 25],
            ['code_egais', 'string', 'max' => 25],
            ['inn', 'string', 'max' => 25],
            ['kpp', 'string', 'max' => 25],
            ['is_spisok', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'poll_id' => 'ID опроса',
            'phone_mobile' => 'Номер мобильного телефона',
            'code_egais' => 'Код ЕГАИС',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile(){
        return $this->hasOne(Profile::className(), ['phone_mobile' => 'phone_mobile']);
    }

    /**
     * @return mixed
     */
    public function getProfileFullNmae(){
        return isset($this->profile->full_name) ? $this->profile->full_name : null;
    }

    public function getProfileEmail(){
        return isset($this->profile->email) ? $this->profile->email : null;
    }

    public function getProfileRole(){
        return isset($this->profile->role) ? $this->profile->role : null;
    }

    /**
     * @param $pollId
     * @param $userId
     * @return bool
     */
    public static function addPollUser($pollId, $userId){
        $profile = Profile::findOne(['id' => $userId]);
        $newPollUser = new self();
        $newPollUser->poll_id = $pollId;
        $newPollUser->phone_mobile = $profile->phone_mobile;
        $newPollUser->is_spisok = false;
        $newPollUser->save(false);
        return true;
    }

}
