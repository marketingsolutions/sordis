<?php

namespace modules\polls\common\models;

use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_upload_polls_file".
 *
 * @property integer $id
 * @property string $original_file_name
 * @property string $file_name
 * @property string $status_process
 * @property string $created_at
 * @property string $processed_at
 */
class UploadPollsFile extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    const STATUS_NEW = 'new';
    const STATUS_PROCESS = 'process';
    const STATUS_COMPLETE = 'complete';
    const STATUS_ERROR = 'error';


    public static function getStatus()
    {
        return [
            self::STATUS_NEW => 'Загружен на сервер',
            self::STATUS_PROCESS => 'В процессе обработки',
            self::STATUS_COMPLETE => 'Обработан (опрос загружен)',
            self::STATUS_ERROR => 'Ошибка обработки',
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%upload_polls_file}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Upload Polls File';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Загрузка опросов из xml';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['original_file_name', 'string', 'max' => 255],
            ['file_name', 'string', 'max' => 255],
            ['status_process', 'string', 'max' => 16],
            ['created_at', 'safe'],
            ['processed_at', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'original_file_name' => 'Имя файла',
            'file_name' => 'Имя файла в системе',
            'status_process' => 'Статус загрузки',
            'created_at' => 'Дата загрузки',
            'processed_at' => 'Дата обработки',
        ];
    }


}
