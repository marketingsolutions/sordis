<?php

use modules\profiles\common\models\Profile;
use yii\helpers\Html;
use yii\grid\GridView;
use modules\polls\common\models\UserPoll;

/* @var $this yii\web\View */
/* @var $profile Profile */
/* @var $notCompletedPolls \modules\polls\common\models\Poll[] */
/* @var $completedPolls \modules\polls\common\models\Poll[] */

$this->title = 'Опросы';
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>

<div class="row">
  <div class="col-md-12">
    <?php if (!empty($notCompletedPolls)): ?>
      <div class="panel panel_ferma">
        <div class="panel-heading">
          Опросы для прохождения
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="alert alert-success">
              <?php foreach ($notCompletedPolls as $poll): ?>
                <?php if (UserPoll::isPollUser($profile->id, $poll->id)): ?>

                  <div class="col-md-4">
                    Название опроса:<br/>
                    <b><?= $poll->title ?></b>
                  </div>
                  <div class="col-md-3">
                    Кол-во баллов за участие в опросе:<br/>
                    <b><?= $poll->bonus_sum ?></b>
                  </div>
                  <div class="col-md-3">
                    Период проведения:<br/>
                    <b>c <?= date("d.m.Y", strtotime($poll->started_at)) ?>
                      по <?= date("d.m.Y", strtotime($poll->finished_at)) ?></b>
                  </div>
                  <div class="col-md-2 text-right">
                        <?= Html::a('Пройти', ['/polls/poll/participate', 'id' => $poll->id], ['class' => 'btn btn-primary btn_all']) ?>
                  </div><br/><br/>
                <?php endif; ?>
                  <hr/>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>


    <?php if (!empty($completedPolls)): ?>
      <div class="panel panel_ferma">
        <h4 class="panel-heading">
          Уже пройденные опросы
        </h4>
        <div class="panel-body">
          <div class="row">
            <?php foreach ($completedPolls as $poll): ?>
              <?php if (\modules\polls\common\models\UserPoll::isPollUser($profile->id, $poll->id)): ?>
                <div class="col-md-6"><?= $poll->title ?></div>
                <div class="col-md-3 text-right mb-8">
                  <b>
                  c <?= date("d.m.Y", strtotime($poll->started_at)) ?>
                  по <?= date("d.m.Y", strtotime($poll->finished_at)) ?></b>
                </div>
                <div class="col-md-3 text-right mb-8">
                    <b>начислено <?=$poll->bonus_sum?> баллов</b>
                </div>
              <?php endif; ?>
                <hr/>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
  </div>
</div>
