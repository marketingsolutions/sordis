<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $poll modules\polls\common\models\Poll */
/* @var $question modules\polls\common\models\Question */
/* @var $opinion modules\polls\frontend\forms\Opinion */

$this->title = 'Прохождение опроса';
$this->params['breadcrumbs'][] = ['label' => 'Опросы', 'url' => ['/polls/poll/index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

$answers = \yii\helpers\ArrayHelper::map($question->answers, 'id', 'answer');

if ($question->allocate) {
    $css = <<<CSS
.poll-question-text {
	font-weight: bold;
}
CSS;

    $this->registerCss($css);
}
?>

<div class="container poll-participate poll-question back_fon">
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 poll-question-text">
			<p><?= $question->question ?></p>
		</div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <?php if($question->image):?>
                <p><img style="max-width: 1000px;" src="data:image/JPG;base64,<?=base64_encode($question->image)?>"></p>
            <?php endif;?>
        </div>
		<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
            <?php $form = ActiveForm::begin() ?>

            <?= $form->field($opinion, 'question')->hiddenInput()->label(false)->hint(false)->error(false); ?>

            <?php if ($question->answers_quantity == 1) { ?>
                <?= $form->field($opinion, 'answer')->radioList($answers)->label(false)->hint(false); ?>
            <?php } ?>
            <?php if ($question->answers_quantity > 1) { ?>
                <?= $form->field($opinion, 'answers[]')->radioList($answers)->label(false)->hint(false); ?>
            <?php } ?>

            <?php if ($question->free_answer) { ?>
                <?= $form->field($opinion, 'free')->textarea()->label($question->answers_quantity > 0 ? 'Другое' : 'Ответ'); ?>
            <?php } ?>
		</div>
		<div class="col-xs-12 text-center">
            <?= Html::submitButton('Ответить', ['class' => 'btn btn-success btn-lg',]) ?>
            <?php ActiveForm::end() ?>
		</div>
	</div>
</div>
