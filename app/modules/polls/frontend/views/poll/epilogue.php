<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model modules\polls\common\models\Poll */

$this->title = 'Пройденый опрос';
$this->params['breadcrumbs'][] = ['label' => 'Опросы', 'url' => ['/polls/poll/index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

?>


<div class="container poll-participate poll-epilogue back_fon">
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 poll-epilogue-text text-center">
			<?= $model->epilogue ?>
		</div>
		<div class="col-xs-12 text-center">
			<?= Html::a('Все опросы', ['/polls/poll/index'], ['class' => 'btn btn-success btn-lg',]) ?>
		</div>
	</div>
</div>
