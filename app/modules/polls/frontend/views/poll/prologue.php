<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\polls\common\models\Poll */

$this->title = 'Пройти опрос';
$this->params['breadcrumbs'][] = ['label' => 'Опросы', 'url' => ['/polls/poll/index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

?>

<div class="container poll-participate poll-prologue back_fon">
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 poll-prologue-text text-center">
			<?= $model->prologue ?>
		</div>
		<div class="col-xs-12 text-center">
			<?php $form = ActiveForm::begin() ?>
			<?= Html::hiddenInput('confirm', true) ?>
			<?= Html::submitButton('Я согласен принять участие', ['class' => 'btn btn-success btn-lg',]) ?>
			<?php ActiveForm::end() ?>
		</div>
	</div>
</div>

