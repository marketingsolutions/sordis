<?php

namespace modules\polls\frontend;

use yz\icons\Icons;

/**
 * mspolls module definition class
 */
class Module extends \modules\polls\common\Module
{
	public function getName() {
		return 'Опросы';
	}
}
