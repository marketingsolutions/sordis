<?php

namespace modules\polls\frontend\models;

use modules\polls\common\models\Answer;

class ApiAnswer extends Answer
{
    public function fields()
    {
        return [
            'id',
            'question_id',
            'sequence',
            'answer',
        ];
    }
}