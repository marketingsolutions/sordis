<?php

namespace modules\polls\frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\polls\common\models\Poll;

/**
 * PollSearch represents the model behind the search form about `modules\polls\common\models\Poll`.
 */
class PollSearch extends Poll
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['topic_id'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param integer $user
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($user, $params)
    {
        $query = Poll::findActive($user);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
