<?php

namespace modules\polls\frontend\models;

use modules\polls\common\models\Poll;

class ApiPoll extends Poll
{
    public function fields()
    {
        return [
            'id',
            'title',
            'status',
            'prologue',
            'epilogue',
            'questions' => function (ApiPoll $model) {
                return ApiQuestion::find()
                    ->where(['poll_id' => $model->id])
                    ->orderBy(['sequence' => SORT_ASC])
                    ->all();
            }
        ];
    }
}