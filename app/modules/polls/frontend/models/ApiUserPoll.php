<?php

namespace modules\polls\frontend\models;

use modules\polls\common\models\UserAnswer;
use modules\polls\common\models\UserPoll;

class ApiUserPoll extends UserPoll
{
    public function fields()
    {
        return [
            'id',
            'profile_id' => 'user_id',
            'poll' => function (ApiUserPoll $model) {
                return ApiPoll::findOne($model->poll_id);
            },
            'poll_id',
            'poll_title' => function (ApiUserPoll $model) {
                $poll = $model->poll;

                if ($poll == null) {
                    return null;
                }

                return $poll->title;
            },
            'finished' => function (ApiUserPoll $model) {
                return $model->finished;
            },
            'finished_at' => function (ApiUserPoll $model) {
                /** @var UserAnswer $answer */
                $answer = UserAnswer::find()
                    ->where(['poll_id' => $model->poll_id, 'user_id' => $model->user_id])
                    ->orderBy(['created_at' => SORT_DESC])
                    ->limit(1)
                    ->one();
                return empty($answer) ? null : (new \DateTime($answer->created_at))->format('d.m.Y');
            },
            'last_question',
            'created_at' => function (ApiUserPoll $model) {
                return (new \DateTime($model->created_at))->format('d.m.Y');
            },
        ];
    }
}