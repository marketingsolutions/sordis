<?php

namespace modules\polls\frontend\models;

use modules\polls\common\models\Question;

class ApiQuestion extends Question
{
    public function fields()
    {
        return [
            'id',
            'question',
            'poll_id',
            'sequence',
            'answers_quantity',
            'free_answer',
            'allocate',
            'answers' => function (ApiQuestion $model) {
                return ApiAnswer::find()
                    ->where(['question_id' => $model->id])
                    ->orderBy(['sequence' => SORT_ASC])
                    ->all();
            }
        ];
    }
}