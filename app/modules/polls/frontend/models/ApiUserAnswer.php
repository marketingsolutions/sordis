<?php

namespace modules\polls\frontend\models;

use modules\polls\common\models\UserAnswer;

class ApiUserAnswer extends UserAnswer
{
    public function fields()
    {
        return [
            'id',
            'profile_id' => 'user_id',
            'poll_id',
            'question_id',
            'answer_id',
            'free_answer',
            'created_at' => function (ApiUserPoll $model) {
                return (new \DateTime($model->created_at))->format('d.m.Y');
            },
        ];
    }
}