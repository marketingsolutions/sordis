<?php

namespace modules\polls\frontend\base;

use frontend\base\Controller as FrontendController;
use Yii;
use yii\filters\AccessControl;

/**
 * PollController implements the CRUD actions for Poll model.
 */
class Controller
	extends FrontendController
{
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
}
