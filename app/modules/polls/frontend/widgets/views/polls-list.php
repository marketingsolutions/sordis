<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;

?>
<div class="polls-list-grid">
	<?= yii\grid\GridView::widget([
		'dataProvider' => $dataProvider,
		'showHeader'   => false,
		'layout'       => "{items}\n{pager}",
		'columns'      => [
			'title',
			[
				'class'          => 'yii\grid\ActionColumn',
				'contentOptions' => ['style' => 'width: 100px;'],
				'template'       => '{participate}',
				'buttons'        => [
					'participate' => function ($url, $model) {
						return Html::a(\yz\icons\Icons::i('eye fa-lg'), ['/polls/poll/participate', 'id' => $model->id], [
							'title'     => 'Участвовать',
							'class'     => 'btn btn-info btn-sm',
							'data-pjax' => '0',
						]);
					},
				],
			],
		],
	]); ?>
</div>

