<?php
/**
 * Created by PhpStorm.
 * User: g-mayor
 * Date: 22.06.2016
 * Time: 17:01
 */

namespace modules\polls\frontend\widgets;


class PollsList
	extends \yii\base\Widget
{
	public $user = null;

	public function init() {
		parent::init();
		if(is_null($this->user)) {
			$this->user = \Yii::$app->user->id;
		}
	}

	public function run() {
		$poolsQuery = \modules\polls\common\models\Poll::findActive($this->user);
		$dataProvider = new \yii\data\ActiveDataProvider([
			'query' => $poolsQuery,
		]);

		return $this->render('polls-list', [
			'dataProvider' => $dataProvider,
		]);
	}
}