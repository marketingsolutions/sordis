<?php

namespace modules\polls\frontend\forms;

use modules\polls\common\models\Answer;
use modules\polls\common\models\Question;
use modules\polls\common\models\UserAnswer;
use modules\polls\common\models\UserPoll;
use yii\base\Model;

/**
 * This is the model class for table "{{%ms_polls_users_answers}}".
 *
 * @property integer $user
 * @property integer $poll
 * @property integer $question
 * @property integer $answer
 * @property array $answers
 * @property string $free
 *
 * @property integer $answersQuantity
 * @property boolean $freeAnswer
 * @property integer $sequence
 *
 */
class Opinion extends Model
{
    public $user;
    public $poll;
    public $question;
    public $answer;
    public $answers;
    public $free;
    public $answersQuantity = 0;
    public $freeAnswer = false;
    public $sequence = 0;
    //
    private $_question;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question',], 'required'],
            [['question',], 'validateQuestion'],
            [['answer',], 'required', 'when' => function ($model) {
                return $model->answersQuantity == 1;
            },],
            [['answer',], 'integer', 'when' => function ($model) {
                return $model->answersQuantity == 1;
            }],
            [['answers',], 'required', 'when' => function ($model) {
                return $model->answersQuantity > 1;
            }, 'message'=> 'Необходимо выбрать какой-нибудь из ответов опроса'],
            [['answers',], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return $model->answersQuantity > 1;
            }],
            [['free',], 'required', 'when' => function ($model) {
                return $model->answersQuantity == 0;
            }, 'enableClientValidation' => false],
            [['free',], 'string', 'when' => function ($model) {
                return $model->freeAnswer;
            }],
            [['answer', /*'answers', */'free',], 'validateOpinion'],
        ];
    }

    public function validateQuestion()
    {
        if ($this->user == null) {
            $this->addError('question', 'User not set');
        }
        if ($this->poll == null) {
            $this->addError('question', 'Poll not set');
        }
        /* @var $question Question */
        $question = Question::findOne($this->question);
        if ($question == null) {
            $this->addError('question', 'Question not found');
        }

        if ($this->hasErrors()) {
            return;
        }

        $this->answersQuantity = $question->answers_quantity;
        $this->freeAnswer = $question->free_answer;
        $this->sequence = $question->sequence;
        $this->_question = $question;

        $userPoll = UserPoll::findOne(['user_id' => $this->user, 'poll_id' => $this->poll]);
        if ($userPoll == null) {
            $this->addError('question', 'User have not poll');

            return;
        }

        $userAnswers = UserAnswer::findAll(['user_id' => $this->user, 'poll_id' => $this->poll, 'question_id' => $this->question]);
        if (!empty($userAnswers)) {
            $this->addError('question', 'User already answered on this question');

            return;
        }
    }

    public function validateOpinion($attribute)
    {
        switch ($attribute) {
            case 'answer':
                if ($this->answersQuantity == 1) {
                    $this->checkAnswer($attribute, $this->$attribute);
                }
                else {
                    $this->answer = null;
                }
                break;
            case 'answers':
                if ($this->answersQuantity > 1) {
                    if (count($this->answers) > $this->answersQuantity) {
                        $this->addError($attribute, 'No more than  ' . $this->answersQuantity . ' answers');
                        break;
                    }
                    foreach ($this->answers as $answer) {
                        $this->checkAnswer($attribute, $answer);
                    }
                }
                else {
                    $this->answers = null;
                }
                break;
            case 'free':
                if (!$this->freeAnswer) {
                    $this->free = null;
                }
                break;
        }
    }

    private function checkAnswer($attribute, $answer)
    {
        /* @var $model Answer */
        $model = Answer::find()->where(['id' => $answer, 'question_id' => $this->question])->one();
        if ($model == null) {
            $this->addError($attribute, 'Answer not found');
        }
    }

    private function saveAnswer($answer = null, $free = null)
    {
        $model = new UserAnswer();
        $model->user_id = $this->user;
        $model->poll_id = $this->poll;
        $model->question_id = $this->question;
        $model->answer_id = $answer;
        $model->free_answer = $free;

        return $model->save();
    }

    public function save()
    {
        if ($this->validate()) {
            $result = false;
            if ($this->answer) {
                $result = ($this->saveAnswer($this->answer, null) ? true : $result);
            }
            if ($this->answers) {
                foreach ($this->answers as $answer) {
                    if($answer) {
                        $result = ($this->saveAnswer($answer, null) ? true : $result);
                    }
                }
            }
            if ($this->free) {
                $result = ($this->saveAnswer(null, $this->free) ? true : $result);
            }

            return $result;
        }

        return false;
    }
}