<?php

namespace modules\polls\frontend\controllers;

use modules\polls\common\models\PollsUser;
use modules\polls\common\models\Question;
use modules\polls\common\models\UserAnswer;
use modules\polls\frontend\base\Controller;
use modules\polls\common\models\UserPoll;
use modules\polls\frontend\forms\Opinion;
use modules\profiles\common\models\Profile;
use Yii;
use modules\polls\common\models\Poll;
use modules\polls\frontend\models\PollSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yz\Yz;

/**
 * PollController implements the CRUD actions for Poll model.
 */
class PollController extends Controller
{
    public function actionIndex()
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity->profile;
        $completedIds = UserPoll::find()
            ->where(['user_id' => $profile->id, 'finished' => true])
            ->indexBy('poll_id')
            ->select('poll_id')
            ->column();


        $completedPolls = Poll::find()->where(['id' => $completedIds])->all();
        $notCompletedPolls = Poll::find()
            ->where(['not in', 'id', $completedIds])
            ->andWhere(['>=', 'finished_at', date("Y-m-d H:i:s")])
            ->andWhere(['<=', 'started_at', date("Y-m-d H:i:s")])
            ->all();

        return $this->render('index', compact('profile', 'completedPolls', 'notCompletedPolls'));
    }

    /**
     * @param integer $id
     *
     * @return mixed
     */
    public function actionParticipate($id)
    {
        $model = $this->findModel($id);
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity->profile;
        $userId = $profile->id;
        $userPoll = UserPoll::findOne(['user_id' => $userId, 'poll_id' => $model->id]);

        if ($userPoll == null) {
            PollsUser::addPollUser($model->id, $userId);
            if (Yii::$app->request->isPost && Yii::$app->request->post('confirm')) {
                $userPoll = new UserPoll();
                $userPoll->user_id = $userId;
                $userPoll->poll_id = $model->id;
                $userPoll->save();
            }
            else {
                return $this->render('prologue', [
                    'model' => $model,
                ]);
            }
        }

        $opinion = new Opinion();
        $opinion->user = $userId;
        $opinion->poll = $model->id;

        if ($opinion->load(Yii::$app->request->post()) && $opinion->save()) {
            $userPoll->updateAttributes(['last_question' => $opinion->sequence]);
        }

        $question = $this->findQuestion($model->id, $userPoll->last_question, $userId);

        if ($question) {
            $opinion = new Opinion();
            $opinion->question = $question->id;

            return $this->render('participate', [
                'poll' => $model,
                'question' => $question,
                'opinion' => $opinion,
            ]);
        }

        if( $userPoll->updateAttributes(['finished' => true]) && UserPoll::addSocialBonus($userId, $model->id)){
            \Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Cоциальные бонусы за прохождение опроса успешно начислены');
        }

        return $this->render('epilogue', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Poll model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Poll the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Poll::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param integer $poll
     * @param integer $last
     * @param integer $user
     *
     * @return Question the loaded model
     */
    protected function findQuestion($poll, $last, $user)
    {
        do {
            /* @var $question Question */
            $question = Question::find()->where(['and', ['poll_id' => $poll], ['>', 'sequence', $last]])->orderBy('sequence')->one();
            if ($question && !empty($question->conditions)) {
                foreach ($question->conditions as $condition) {
                    $answer = UserAnswer::findOne(['user_id' => $user, 'poll_id' => $poll, 'answer_id' => $condition->answer_id]);
                    if ($condition->show == ($answer != null)) {
                        return $question;
                    }
                }
                $last++;
                continue;
            }
            break;
        }
        while ($question);

        return $question;
    }


    public function beforeAction($action)
    {
        $profile = Yii::$app->user->identity->profile;


        if ($profile->role == Profile::ROLE_DISTRIBUTOR) {
            return $this->redirect('/');
        }
        return parent::beforeAction($action);
    }
}
