<?php

namespace modules\polls\frontend\controllers\api;

use modules\News\frontend\models\ApiNews;
use modules\polls\common\models\Question;
use modules\polls\common\models\UserAnswer;
use modules\polls\common\models\UserPoll;
use modules\polls\frontend\forms\Opinion;
use modules\polls\frontend\models\ApiAnswer;
use modules\polls\frontend\models\ApiPoll;
use modules\polls\frontend\models\ApiQuestion;
use modules\polls\frontend\models\ApiUserAnswer;
use modules\polls\frontend\models\ApiUserPoll;
use modules\profiles\frontend\models\ApiProfile;
use ms\loyalty\api\common\models\Log;
use ms\loyalty\api\frontend\base\ApiController;
use Yii;

class PollController extends ApiController
{
    public function actionByProfile()
    {
        $profile_id = Yii::$app->request->post('profile_id');

        /** @var ApiProfile $profile */
        $profile = ApiProfile::findOne($profile_id);

        if ($profile == null) {
            return $this->error("Не найден участник по profile_id=$profile_id", 'Ошибка получения опросов по участнику');
        }

        $completedIds = UserPoll::find()
            ->where(['user_id' => $profile->id, 'finished' => true])
            ->indexBy('poll_id')
            ->select('poll_id')
            ->column();

        $user_polls = ApiUserPoll::find()->where(['poll_id' => $completedIds])->orderBy(['id' => SORT_ASC])->all();
        $polls = ApiPoll::find()->where(['not in', 'id', $completedIds])->orderBy(['id' => SORT_ASC])->all();

        return $this->ok(compact('polls', 'user_polls'), 'Получение опросов по участнику');
    }

    public function actionQuestion()
    {
        $profile_id = Yii::$app->request->post('profile_id');
        $poll_id = Yii::$app->request->post('poll_id');

        /** @var ApiProfile $profile */
        $profile = ApiProfile::findOne($profile_id);
        /** @var ApiPoll $poll */
        $poll = ApiPoll::findOne($poll_id);

        if ($profile == null) {
            return $this->error("Не найден участник по profile_id=$profile_id", 'Ошибка прохождения опроса');
        }
        if ($poll == null) {
            return $this->error("Не найден опрос по poll_id=$poll_id", 'Ошибка прохождения опроса');
        }

        $userPoll = ApiUserPoll::findOne(['user_id' => $profile_id, 'poll_id' => $poll_id]);

        if ($userPoll == null) {
            $userPoll = new ApiUserPoll();
            $userPoll->user_id = $profile_id;
            $userPoll->poll_id = $poll_id;
            $userPoll->save();
            $userPoll->refresh();
        }
        elseif ($userPoll->finished) {
            return $this->error('finished', 'Опрос уже пройден участником', 406);
        }

        $question = $this->findQuestion($poll_id, $userPoll->last_question);

        if ($question == null) {
            return $this->error('Вопрос не найден', 'Ошибка прохождения опроса');
        }

        return $this->ok(compact('question', 'userPoll'), 'Успешная выдача вопроса');
    }

    public function actionAnswer()
    {
        $profile_id = Yii::$app->request->post('profile_id');
        $poll_id = Yii::$app->request->post('poll_id');
        $answer_id = Yii::$app->request->post('answer_id');

        /** @var ApiProfile $profile */
        $profile = ApiProfile::findOne($profile_id);
        $poll = ApiPoll::findOne($poll_id);
        $answer = ApiAnswer::findOne($answer_id);

        if ($profile == null) {
            return $this->error("Не найден участник по profile_id=$profile_id", 'Ошибка прохождения опроса');
        }
        if ($poll == null) {
            return $this->error("Не найден опрос по poll_id=$poll_id", 'Ошибка прохождения опроса');
        }
        if ($answer == null) {
            return $this->error("Не найден ответ", 'Ошибка прохождения опроса');
        }

        $userPoll = ApiUserPoll::findOne(['user_id' => $profile_id, 'poll_id' => $poll_id]);

        if ($userPoll == null) {
            $userPoll = new UserPoll();
            $userPoll->user_id = $profile_id;
            $userPoll->poll_id = $poll_id;
            $userPoll->save();
            $userPoll->refresh();
        }

        $question = $this->findQuestion($poll_id, $userPoll->last_question);

        if ($question == null) {
            return $this->error('Вопрос не найден', 'Ошибка прохождения опроса');
        }
        if ($answer->question_id != $question->id) {
            return $this->error('Этот ответ на другой вопрос', 'Ошибка прохождения опроса');
        }

        $userAnswer = new ApiUserAnswer();
        $userAnswer->user_id = $profile_id;
        $userAnswer->poll_id = $poll_id;
        $userAnswer->question_id = $question->id;
        $userAnswer->answer_id = $answer_id;
        $userAnswer->save(false);

        $userPoll->updateAttributes(['last_question' => $question->sequence]);
        $question = $this->findQuestion($poll_id, $userPoll->last_question);

        if ($question == null) {
            $userPoll->finished = true;
            $userPoll->save(false);
        }

        return $this->ok(compact('question', 'userPoll'), 'Успешный ответ на опрос');
    }

    /**
     * @param integer $poll_id
     * @param integer $last
     *
     * @return ApiQuestion the loaded model
     */
    protected function findQuestion($poll_id, $last)
    {
        /** @var ApiQuestion $question */
        $question = ApiQuestion::find()
            ->where(['poll_id' => $poll_id])
            ->andWhere(['>', 'sequence', $last])
            ->orderBy(['sequence' => SORT_ASC])
            ->limit(1)
            ->one();

        return $question;
    }
}