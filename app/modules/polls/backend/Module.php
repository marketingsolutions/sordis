<?php

//namespace modules\polls\backend;
namespace modules\polls\backend;
use yz\icons\Icons;

/**
 * mspolls module definition class
 */
class Module
	extends \modules\polls\common\Module
{
	public function getName() {
		return 'Опросы';
	}

	public function getAdminMenu() {
		return [
			[
				'label' => 'Опросы',
				'icon'  => Icons::o('life-ring'),
				'items' => [
					[
						'route' => ['/polls/topic/index'],
						'label' => 'Темы',
						'icon'  => Icons::o('list'),
					],
                    [
                        'route' => ['/polls/upload-polls-file/index'],
                        'label' => 'Загрузка опросов из xml',
                        'icon'  => Icons::o('list'),
                    ],
					[
						'route' => ['/polls/poll/index'],
						'label' => 'Опросы',
						'icon'  => Icons::o('list'),
					],
                    [
                        'route' => ['/polls/polls-user/index'],
                        'label' => 'Участники опросов',
                        'icon'  => Icons::o('list'),
                    ],
					[
						'route' => ['/polls/poll/current'],
						'label' => 'Текущее состояние',
						'icon'  => Icons::o('list'),
					],
				],
			],
		];
	}
}
