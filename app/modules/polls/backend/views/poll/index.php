<?php

use modules\polls\backend\models\PollSearch;
use yii\helpers\Html;
use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\icons\Icons;

/* @var $this yii\web\View */
/* @var $searchModel PollSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Опросы';
$this->params['breadcrumbs'][] = ['label' => 'Темы', 'url' => ['topic/index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'poll-index box-primary']) ?>
	<div class="text-right">
		<?php echo ActionButtons::widget([
			'order'  => [['create', 'delete']],
			'gridId' => 'poll-index-grid',
			//'searchModel' => $searchModel,
			//'modelClass'  => 'modules\polls\common\models\Poll',
		]) ?>
	</div>

<?= GridView::widget([
	'id'           => 'poll-index-grid',
	'dataProvider' => $dataProvider,
	'filterModel'  => $searchModel,
	'columns'      => array_merge([
		['class' => 'yii\grid\CheckboxColumn'],
	], [
		[
			'attribute'     => 'id',
			'headerOptions' => ['style' => 'width: 100px;'],
		],
		'created_at:date',
		'title',
		[
			'attribute' => 'statusName',
			'filter'    => $searchModel::getStatuses(),
		],
		'bonus_sum',
		'countQuestions',
		'started_at:date',
		'finished_at:date',
	], [
		[
			'class'    => 'yz\admin\widgets\ActionColumn',
			'template' => '{update} {questions} {delete}',
			'buttons'  => [
				'questions' => function ($url, $model) {
					return Html::a(Icons::i('eye fa-lg'), ['question/index', 'poll' => $model->id], [
						'title'     => 'Вопросы',
						'class'     => 'btn btn-info btn-sm',
						'data-pjax' => '0',
					]);
				},
			],
		],
	]),
]); ?>
<?php Box::end() ?>