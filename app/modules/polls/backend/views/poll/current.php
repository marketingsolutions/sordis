<?php

use yii\helpers\Html;
use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\icons\Icons;

/* @var $this yii\web\View */
/* @var $searchModel modules\polls\backend\models\PollSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Опросы';
$this->params['breadcrumbs'][] = ['label' => 'Темы', 'url' => ['topic/index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'poll-index box-primary']) ?>

<?= GridView::widget([
	'id'           => 'category-grid',
	'dataProvider' => $dataProvider,
	'filterModel'  => $searchModel,
	'columns'      => array_merge([
		[
			'attribute'     => 'id',
			'headerOptions' => ['style' => 'width: 100px;'],
		],
		'title',
		'bonus_sum',
	], [
		[
			'attribute' => 'statusName',
			'filter'    => $searchModel::getStatuses(),
		],
	], ['countUsers', 'countStarted', 'countFinished'], [
		[
			'class'    => 'yii\grid\ActionColumn',
			'template' => '{users} {total} {xml}',
			'buttons'  => [
				'users' => function ($url, $model) {
					return Html::a(Icons::i('eye fa-lg'), ['user-poll/index', 'poll' => $model->id], [
						'title'     => 'Пользователи',
						'class'     => 'btn btn-info btn-sm',
						'data-pjax' => '0',
					]);
				},
				'total' => function ($url, $model) {
					return Html::a(Icons::i('eye fa-lg'), ['user-answer/index', 'poll' => $model->id], [
						'title'     => 'Итог',
						'class'     => 'btn btn-success btn-sm',
						'data-pjax' => '0',
					]);
				},
                'xml' => function ($url, $model) {
                    return Html::a(Icons::i('angle-double-down fa-lg').' XML', ['/polls/poll/export-xml', 'id' => $model->id], [
                        'title'     => 'Скачать результаты теста в XML',
                        'class'     => 'btn btn-warning btn-sm',
                        'data-pjax' => '0',
                    ]);
                },
			],
		],
	]),
]); ?>

<?php Box::end() ?>