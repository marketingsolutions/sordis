<?php

use modules\polls\common\models\Poll;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\ActiveForm;
use yz\admin\widgets\FormBox;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

$dir = Yii::getAlias(getenv('YII_ENV') == 'dev' ? '@frontendWebroot/data/filemanager/source/' : '@data/filemanager/source/');
FileHelper::createDirectory($dir);
$thumbsDir = $dir = Yii::getAlias(getenv('YII_ENV') == 'dev' ? '@frontendWebroot/data/filemanager/thumbs/' : '@data/filemanager/thumbs/');
FileHelper::createDirectory($thumbsDir);

/* @var $this yii\web\View */
/* @var $model Poll */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $box = FormBox::begin(['cssClass' => 'poll-form box-primary', 'title' => '']) ?>
<?php $form = ActiveForm::begin(); ?>

<?php $box->beginBody() ?>
<?= $form->field($model, 'topic_id')->dropDownList($model->getTopics()) ?>

<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'started_at')->datePicker(['dateFormat' => 'yyyy-MM-dd 00:00:00', 'options' => ['disabled' => !$model->canChangeStart()]]) ?>

<?= $form->field($model, 'finished_at')->datePicker(['dateFormat' => 'yyyy-MM-dd 00:00:00', 'options' => ['disabled' => !$model->canChangeFinish()]]) ?>

<?= $form->field($model, 'status')->dropDownList($model->getPossibleStatus()) ?>

<?= $form->field($model, 'bonus_sum') ?>

<?= $form->field($model, 'prologue')->widget(\xvs32x\tinymce\Tinymce::className(), [
    'pluginOptions' => [
        'plugins' => [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
        ],
        'toolbar1' => "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
        'toolbar2' => "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor ",
        'image_advtab' => true,
        'filemanager_title' => "Filemanager",
        'language' => ArrayHelper::getValue(explode('_', Yii::$app->language), '0', Yii::$app->language),
    ],
    'fileManagerOptions' => [
        'configPath' => [
            'upload_dir' => '/data/filemanager/source/',
            'current_path' => '../../../../../frontend/web/data/filemanager/source/',
            'thumbs_base_path' => '../../../../../frontend/web/data/filemanager/thumbs/',
            'base_url' => Yii::getAlias('@frontendWeb'), // <-- uploads/filemanager path must be saved in frontend
        ]
    ]
]); ?>

<?= $form->field($model, 'epilogue')->widget(\xvs32x\tinymce\Tinymce::className(), [
    'pluginOptions' => [
        'plugins' => [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
        ],
        'toolbar1' => "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
        'toolbar2' => "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor ",
        'image_advtab' => true,
        'filemanager_title' => "Filemanager",
        'language' => ArrayHelper::getValue(explode('_', Yii::$app->language), '0', Yii::$app->language),
    ],
    'fileManagerOptions' => [
        'configPath' => [
            'upload_dir' => '/data/filemanager/source/',
            'current_path' => '../../../../../frontend/web/data/filemanager/source/',
            'thumbs_base_path' => '../../../../../frontend/web/data/filemanager/thumbs/',
            'base_url' => Yii::getAlias('@frontendWeb'), // <-- uploads/filemanager path must be saved in frontend
        ]
    ]
]); ?>
<?php if(Yii::$app->controller->action->id == 'update'):?>
    <?= $form->field($model, 'is_email_send')->staticControl(['value' => $model->is_email_send ? 'Да' : 'Нет']) ?>
    <?php if(!$model->is_email_send):?>
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <a  class="btn btn-primary" href="/polls/poll/email-send?poll_id=<?=$model->id?>"><i class="glyphicon glyphicon-envelope"></i>&nbsp; Отправить приглашение по Email</a>
            </div>
        </div>
    <?php endif;?>
    <br/><br/>
    <?= $form->field($model, 'is_sms_send')->staticControl(['value' => $model->is_sms_send ? 'Да' : 'Нет']) ?>

    <?php if(!$model->is_sms_send):?>
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <a  class="btn btn-primary disabled" href="/polls/poll/sms-send?poll_id=<?=$model->id?>"><i class="glyphicon glyphicon-earphone"></i>&nbsp; Отправить приглашение по SMS</a>
                <p style="color:green">Внимание! Отправка смс пока не активна, дабы не отправлять платные смс 10000 фэйковым юзерам</p>
            </div>
        </div>
    <?php endif;?>
    <br/><br/>
<?php endif;?>
<?php $box->endBody() ?>

<?php $box->actions([
	AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
	AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
	AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
]) ?>
<?php ActiveForm::end(); ?>

<?php FormBox::end() ?>