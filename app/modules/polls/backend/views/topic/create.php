<?php

use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;

/* @var $this yii\web\View */
/* @var $model modules\polls\common\models\Topic */

$this->title = 'Создать тему';
$this->params['breadcrumbs'][] = ['label' => 'Темы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<div class="topic-create">

	<div class="text-right">
		<?php Box::begin() ?>
		<?= ActionButtons::widget([
			'order'        => [['index', 'return']],
			'addReturnUrl' => false,
		]) ?>
		<?php Box::end() ?>
	</div>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>