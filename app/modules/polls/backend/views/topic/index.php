<?php

use yii\helpers\Html;
use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\icons\Icons;

/* @var $this yii\web\View */
/* @var $searchModel modules\polls\backend\models\TopicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Темы';
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'topic-index box-primary']) ?>
	<div class="text-right">
		<?php echo ActionButtons::widget([
			'order'  => [['create', 'delete']],
			'gridId' => 'topic-index-grid',
			//'searchModel' => $searchModel,
			//'modelClass'  => 'modules\polls\common\models\Topic',
		]) ?>
	</div>

<?= GridView::widget([
	'id'           => 'topic-index-grid',
	'dataProvider' => $dataProvider,
	//'filterModel' => $searchModel,
	'columns'      => array_merge([
		['class' => 'yii\grid\CheckboxColumn'],
	], ['id', 'title', 'countPolls'], [
		[
			'class'    => 'yz\admin\widgets\ActionColumn',
			'template' => '{update} {polls} {delete}',
			'buttons'  => [
				'polls' => function ($url, $model) {
					return Html::a(Icons::i('eye fa-lg'), ['poll/index', 'topic' => $model->id], [
						'title'     => 'Опросы',
						'class'     => 'btn btn-info btn-sm',
						'data-pjax' => '0',
					]);
				},
			],
		],
	]),
]); ?>
<?php Box::end() ?>