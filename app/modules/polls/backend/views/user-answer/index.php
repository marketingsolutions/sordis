<?php

use yii\helpers\Html;
use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\icons\Icons;

/* @var $this yii\web\View */
/* @var $searchModel modules\polls\backend\models\UserAnswerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ответы пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Опросы', 'url' => ['poll/current']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'user-answer-index box-primary']) ?>
	<div class="text-right">
		<?php echo ActionButtons::widget([
			'order'       => [['export']],
			'gridId'      => 'user-answer-index-grid',
			'searchModel' => $searchModel,
			'modelClass'  => ' modules\polls\common\models\Question',
		]) ?>
	</div>

<?= GridView::widget([
	'id'           => 'user-answer-index-grid',
	'dataProvider' => $dataProvider,
	//'filterModel'  => $searchModel,
	'columns'      => array_merge([
		'question',
		[
			'attribute' => 'resultTotal',
			'label'     => 'Ответы',
			'format'    => 'html',
			'value'     => function ($model) {
				if(empty($model->resultTotal)) {
					return null;
				}
				$answer = '<tr><th width="80%">Ответ</th><th width="80px">кол-во</th><th>Пользователь</th></tr>';
				foreach($model->resultTotal as $item) {

					$it = explode('///', $item['answer']);
						if(isset($it[1]))
							$answer .= '<tr><td>' . Html::encode($it[0]) . '</td><td>' . $item['count'] . '</td><td>'.\modules\polls\common\models\Question::getUserNameFromId($it[1]).'</td></tr>';
						else
							$answer .= '<tr><td>' . Html::encode($item['answer']) . '</td><td>' . $item['count'] . '</td><td>не указан</td></tr>';

				}
				return Html::tag('table', $answer, ['class' => 'table']);
			},
		],
	], [
	]),
]); ?>

<?php Box::end() ?>