<?php

use yii\helpers\Html;
use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\icons\Icons;

/* @var $this yii\web\View */
/* @var $searchModel modules\polls\backend\models\UserAnswerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ответы пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Опросы', 'url' => ['poll/current']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'user-answer-detail box-primary']) ?>

<?= GridView::widget([
	'id'           => 'category-grid',
	'dataProvider' => $dataProvider,
	//'filterModel'  => $searchModel,
	'columns'      => array_merge([
		'question.question',
		[
			'attribute' => 'answer_id',
			'label'     => 'Ответ',
			'format'    => 'html',
			'value'     => function ($model) {
				$answers = $model->allAnswers;
				\yii\helpers\ArrayHelper::htmlEncode($answers);

				return implode('<br><br>', $answers);
			},
		],
	], [
	]),
]); ?>

<?php Box::end() ?>