<?php

use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;

/* @var $this yii\web\View */
/* @var $model modules\polls\common\models\Condition */
/* @var $pollId integer */
/* @var $questionId integer */

$this->title = 'Создать условие';
$this->params['breadcrumbs'][] = ['label' => 'Темы', 'url' => ['topic/index']];
$this->params['breadcrumbs'][] = ['label' => 'Опросы', 'url' => ['poll/index']];
$this->params['breadcrumbs'][] = ['label' => 'Вопросы', 'url' => ['question/index', 'poll' => $pollId]];
$this->params['breadcrumbs'][] = ['label' => 'Условия', 'url' => ['index', 'question' => $questionId]];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<div class="condition-create">

	<div class="text-right">
		<?php Box::begin() ?>
		<?= ActionButtons::widget([
			'order'        => [['index', 'return']],
			'indexUrl'     => ['index', 'question' => $questionId],
			'addReturnUrl' => false,
		]) ?>
		<?php Box::end() ?>
	</div>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>