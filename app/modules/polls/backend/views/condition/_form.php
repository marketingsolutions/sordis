<?php

use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\ActiveForm;
use yz\admin\widgets\FormBox;

/* @var $this yii\web\View */
/* @var $model modules\polls\common\models\Condition */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $box = FormBox::begin(['cssClass' => 'condition-form box-primary', 'title' => '']) ?>
<?php $form = ActiveForm::begin(); ?>

<?php $box->beginBody() ?>
<?= $form->field($model, 'answer_id')->dropDownList($model->getPossibleOptions()) ?>
<?php $box->endBody() ?>

<?php $box->actions([
	//AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
	AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
	AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
]) ?>
<?php ActiveForm::end(); ?>

<?php FormBox::end() ?>