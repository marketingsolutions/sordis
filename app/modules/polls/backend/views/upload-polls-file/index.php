<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\polls\backend\models\UploadPollsFileSearch $searchModel
 * @var array $columns
 */

$this->title = modules\polls\common\models\UploadPollsFile::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'upload-polls-file-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [/*['search'],*/ ['export', 'create', 'delete', 'return']],
            'gridId' => 'upload-polls-file-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\polls\common\models\UploadPollsFile',
        ]) ?>
    </div>
    <?= GridView::widget([
        'id' => 'upload-polls-file-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '{delete}',
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
