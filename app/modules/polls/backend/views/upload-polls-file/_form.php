<?php

use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var modules\polls\common\models\UploadPollsFile $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php  $box = FormBox::begin(['cssClass' => 'upload-polls-file-form box-primary', 'title' => '']) ?>
<?php $form = ActiveForm::begin(); ?>
<?php $box->beginBody() ?>
<?= $form->field($uploadForm, 'xmlFile')->fileInput() ?>
<button class="btn btn-primary">Загрузить</button>
<?php $box->endBody() ?>
<?php ActiveForm::end(); ?>

<?php  FormBox::end() ?>
