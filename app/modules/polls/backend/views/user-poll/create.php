<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\polls\common\models\UserPoll */

$this->title = Yii::t('app', 'Create User Poll');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Polls'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-poll-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
