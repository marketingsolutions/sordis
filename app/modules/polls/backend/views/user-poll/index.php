<?php

use yii\helpers\Html;
use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\icons\Icons;


/* @var $this yii\web\View */
/* @var $searchModel modules\polls\backend\models\UserPollSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Участники';
$this->params['breadcrumbs'][] = ['label' => 'Опросы', 'url' => ['poll/current']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'user-poll-index box-primary']) ?>
<div class="text-right">
	<?php echo ActionButtons::widget([
		'order'       => [['export']],
		'gridId'      => 'user-poll-index-grid',
		'searchModel' => $searchModel,
		'modelClass'  => 'modules\polls\common\models\UserPoll',
	]) ?>
</div>

<?= GridView::widget([
	'id'           => 'user-poll-index-grid',
	'dataProvider' => $dataProvider,
	//'filterModel'  => $searchModel,
	'columns'      => array_merge([
		[
			'attribute'     => 'id',
			'headerOptions' => ['style' => 'width: 100px;'],
		],
		[
			'attribute'     => 'created_at',
			'headerOptions' => ['style' => 'width: 160px;'],
		],
		[
			'attribute' => 'profile.fullName',
			'label'     => 'Пользователь',
		],
		'finished:boolean',
	], [
		[
			'class'    => 'yz\admin\widgets\ActionColumn',
			'template' => '{answers} {delete}',
			'buttons'  => [
				'answers' => function ($url, $model) {
					return Html::a(Icons::i('eye fa-lg'), ['user-answer/detail', 'user' => $model->user_id, 'poll' => $model->poll_id], [
						'title'     => 'Ответы',
						'class'     => 'btn btn-info btn-sm',
						'data-pjax' => '0',
					]);
				},
			],
		],
	]),
]); ?>

<?php Box::end() ?>
