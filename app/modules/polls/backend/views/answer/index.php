<?php

use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel modules\polls\backend\models\AnswerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $pollId integer */
/* @var $questionId integer */


$this->title = 'Ответы';
$this->params['breadcrumbs'][] = ['label' => 'Темы', 'url' => ['topic/index']];
$this->params['breadcrumbs'][] = ['label' => 'Опросы', 'url' => ['poll/index']];
$this->params['breadcrumbs'][] = ['label' => 'Вопросы', 'url' => ['question/index', 'poll' => $pollId]];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'answer-index box-primary']) ?>
	<div class="text-right">
		<?php echo ActionButtons::widget([
			'order'         => [['create', 'delete']],
			'gridId'        => 'answer-index-grid',
			'createUrl'     => ['create', 'question' => $questionId],
			'createAjaxUrl' => ['create', 'question' => $questionId],
			//'searchModel' => $searchModel,
			//'modelClass'  => 'modules\polls\common\models\Topic',
		]) ?>
	</div>

<?= GridView::widget([
	'id'           => 'answer-index-grid',
	'dataProvider' => $dataProvider,
	//'filterModel' => $searchModel,
	'columns'      => array_merge([
		['class' => 'yii\grid\CheckboxColumn'],
	], ['id', 'sequence', 'answer'], [
		[
			'class'    => 'yz\admin\widgets\ActionColumn',
			'template' => '{update} {delete}',
		],
	]),
]); ?>
<?php Box::end() ?>