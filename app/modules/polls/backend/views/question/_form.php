<?php

use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\ActiveForm;
use yz\admin\widgets\FormBox;

/* @var $this yii\web\View */
/* @var $model modules\polls\common\models\Question */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $box = FormBox::begin(['cssClass' => 'question-form box-primary', 'title' => '']) ?>
<?php $form = ActiveForm::begin(); ?>

<?php $box->beginBody() ?>

<?= $form->field($model, 'question')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'allocate')->checkbox() ?>

<?= $form->field($model, 'answers_quantity')->dropDownList([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]) ?>

<?= $form->field($model, 'free_answer')->checkbox() ?>

<?php $box->endBody() ?>

<?php $box->actions([
	AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
	AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
	AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
]) ?>
<?php ActiveForm::end(); ?>

<?php FormBox::end() ?>