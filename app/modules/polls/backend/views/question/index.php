<?php

use yii\helpers\Html;
use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\icons\Icons;

/* @var $this yii\web\View */
/* @var $searchModel modules\polls\backend\models\QuestionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $pollId integer */

$this->title = 'Вопросы';
$this->params['breadcrumbs'][] = ['label' => 'Темы', 'url' => ['topic/index']];
$this->params['breadcrumbs'][] = ['label' => 'Опросы', 'url' => ['poll/index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

?>
<?php $box = Box::begin(['cssClass' => 'question-index box-primary']) ?>
	<div class="text-right">
		<?php echo ActionButtons::widget([
			'order'         => [['create', 'delete']],
			'gridId'        => 'question-index-grid',
			'createUrl'     => ['create', 'poll' => $pollId],
			'createAjaxUrl' => ['create', 'poll' => $pollId],
			//'searchModel' => $searchModel,
			//'modelClass'  => 'modules\polls\common\models\Question',
		]) ?>
	</div>

<?= GridView::widget([
	'id'           => 'question-index-grid',
	'dataProvider' => $dataProvider,
	//'filterModel' => $searchModel,
	'columns'      => array_merge([
		['class' => 'yii\grid\CheckboxColumn'],
	], ['id', 'sequence', 'question', 'countConditions', 'answers_quantity', 'countAnswers', 'free_answer:boolean',], [
		[
			'class'    => 'yz\admin\widgets\ActionColumn',
			'template' => '{update} {conditions} {answers} {delete}',
			'buttons'  => [
				'conditions' => function ($url, $model) {
					return Html::a(Icons::i('eye fa-lg'), ['condition/index', 'question' => $model->id], [
						'title'     => 'Условия',
						'class'     => 'btn btn-warning btn-sm',
						'data-pjax' => '0',
					]);
				},
				'answers'    => function ($url, $model) {
					return Html::a(Icons::i('eye fa-lg'), ['answer/index', 'question' => $model->id], [
						'title'     => 'Ответы',
						'class'     => 'btn btn-info btn-sm',
						'data-pjax' => '0',
					]);
				},
			],
		],
	]),
]); ?>
<?php Box::end() ?>