<?php

namespace modules\polls\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\polls\common\models\Answer;

/**
 * AnswerSearch represents the model behind the search form about `modules\polls\common\models\Answer`.
 */
class AnswerSearch
	extends Answer
{
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['id',], 'integer'],
			//[['id', 'question_id', 'sequence'], 'integer'],
			[['answer'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Answer::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id'          => $this->id,
			'question_id' => $this->question_id,
			//'sequence'    => $this->sequence,
		]);

		$query->andFilterWhere(['like', 'answer', $this->answer]);

		return $dataProvider;
	}
}
