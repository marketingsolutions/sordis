<?php

namespace modules\polls\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\polls\common\models\UserAnswer;

/**
 * UserAnswerSearch represents the model behind the search form about `modules\polls\common\models\UserAnswer`.
 */
class UserAnswerSearch
	extends UserAnswer
{
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['id', 'user_id', 'poll_id', 'question_id', 'answer_id'], 'integer'],
			[['created_at', 'free_answer'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = UserAnswer::find()->groupBy(['poll_id', 'question_id']);

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => [
				'attributes'   => [
					'id',
				],
				'defaultOrder' => ['id' => SORT_ASC],
			],
		]);

		$this->load($params);

		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id'          => $this->id,
			'created_at'  => $this->created_at,
			'user_id'     => $this->user_id,
			'poll_id'     => $this->poll_id,
			'question_id' => $this->question_id,
			'answer_id'   => $this->answer_id,
		]);

		$query->andFilterWhere(['like', 'free_answer', $this->free_answer]);

		return $dataProvider;
	}
}
