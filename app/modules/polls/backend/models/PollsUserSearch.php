<?php

namespace modules\polls\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\polls\common\models\PollsUser;

/**
 * PollsUserSearch represents the model behind the search form about `modules\polls\common\models\PollsUser`.
 */
class PollsUserSearch extends PollsUser implements SearchModelInterface

{

    /**
     * @var
     */
    public $profileFullNmae;

    /**
     * @var
     */
    public $profileEmail;

    /**
     * @var
     */
    public $profileRole;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['poll_id', 'phone_mobile', 'code_egais', 'inn', 'kpp', 'profileFullNmae', 'profileEmail', 'profileRole'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        return PollsUser::find()
            ->joinWith('profile');
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'profileFullNmae' => [
                'asc' => ['{{%profiles}}.full_name' => SORT_ASC],
                'desc' => ['{{%profiles}}.full_name' => SORT_DESC]
            ],
            'profileEmail' => [
                'asc' => ['{{%profiles}}.email' => SORT_ASC],
                'desc' => ['{{%profiles}}.email' => SORT_DESC]
            ],
            'profileRole' => [
                'asc' => ['{{%profiles}}.role' => SORT_ASC],
                'desc' => ['{{%profiles}}.role' => SORT_DESC]
            ],
        ]);
        $dataProvider->setSort($sort);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'poll_id', $this->poll_id])
            ->andFilterWhere(['like', 'phone_mobile', $this->phone_mobile])
            ->andFilterWhere(['like', '{{%profiles}}.full_name', $this->profileFullNmae])
            ->andFilterWhere(['like', '{{%profiles}}.email', $this->profileEmail])
            ->andFilterWhere(['like', '{{%profiles}}.role', $this->profileRole])
            ->andFilterWhere(['like', 'code_egais', $this->code_egais])
            ->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'kpp', $this->kpp]);

    }
}
