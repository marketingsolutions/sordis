<?php

namespace modules\polls\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\polls\common\models\Poll;

/**
 * PollSearch represents the model behind the search form about `modules\polls\common\models\Poll`.
 */
class PollSearch
	extends Poll
{
	public $statusName = null;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['id', 'topic_id', 'status', 'statusName', 'bonus_sum'], 'integer'],
			[['title'], 'safe'],
			//[['created_at', 'started_at', 'finished_at', 'title', 'prologue', 'epilogue'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Poll::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$dataProvider->setSort([
			'attributes' => [
				'id',
				'topic_id',
                'bonus_sum',
				'status',
				'created_at',
				'started_at',
				'finished_at',
				'title',
				'statusName' => [
					'asc'     => ['status' => SORT_ASC],
					'desc'    => ['status' => SORT_DESC],
					'default' => SORT_ASC,
				],
			],
		]);

		$this->load($params);

		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id'          => $this->id,
			'created_at'  => $this->created_at,
			'started_at'  => $this->started_at,
			'finished_at' => $this->finished_at,
			'topic_id'    => $this->topic_id,
			'status'      => $this->status,
			'bonus_sum'      => $this->bonus_sum,
		]);

		$query->andFilterWhere([
			'status' => $this->statusName,
		]);

		$query->andFilterWhere(['like', 'title', $this->title])//
		->andFilterWhere(['like', 'prologue', $this->prologue])//
		->andFilterWhere(['like', 'epilogue', $this->epilogue,]);

		return $dataProvider;
	}
}
