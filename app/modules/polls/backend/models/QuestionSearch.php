<?php

namespace modules\polls\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\polls\common\models\Question;

/**
 * QuestionSearch represents the model behind the search form about `modules\polls\common\models\Question`.
 */
class QuestionSearch
	extends Question
{
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['id', 'answers_quantity',], 'integer',],
			//[['id', 'poll_id', 'sequence', 'answers_quantity',], 'integer',],
			[['question'], 'safe'],
			[['free_answer'], 'boolean'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Question::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => [
				'defaultOrder' => ['sequence' => SORT_ASC],
			],
		]);

		$this->load($params);

		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id'               => $this->id,
			'poll_id'          => $this->poll_id,
			//'sequence'         => $this->sequence,
			'answers_quantity' => $this->answers_quantity,
			'free_answer'      => $this->free_answer,
			//'user_id'      => $this->user_id,
		]);

		$query->andFilterWhere(['like', 'question', $this->question]);

		return $dataProvider;
	}
}
