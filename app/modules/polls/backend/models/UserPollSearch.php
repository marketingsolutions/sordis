<?php

namespace modules\polls\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\polls\common\models\UserPoll;

/**
 * UserPollSearch represents the model behind the search form about `modules\polls\common\models\UserPoll`.
 */
class UserPollSearch extends UserPoll
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'last_question'], 'integer'],
            //[['id', 'user_id', 'poll_id', 'last_question'], 'integer'],
            [['created_at'], 'safe'],
            [['finished'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserPoll::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'user_id' => $this->user_id,
            'poll_id' => $this->poll_id,
            'finished' => $this->finished,
            'last_question' => $this->last_question,
        ]);

        return $dataProvider;
    }
}
