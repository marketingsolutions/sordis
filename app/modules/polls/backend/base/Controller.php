<?php

namespace modules\polls\backend\base;

use backend\base\Controller as BackendController;
use Yii;

/**
 * PollController implements the CRUD actions for Poll model.
 */
class Controller extends BackendController
{
}
