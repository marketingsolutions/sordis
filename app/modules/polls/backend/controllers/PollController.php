<?php

namespace modules\polls\backend\controllers;

use bupy7\xml\constructor\XmlConstructor;
use modules\polls\backend\base\Controller;
use modules\polls\backend\models\PollSearch;
use modules\polls\common\models\Poll;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Spatie\ArrayToXml\ArrayToXml;

/**
 * PollController implements the CRUD actions for Poll model.
 */
class PollController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Poll models.
     *
     * @param integer $topic
     *
     * @return mixed
     */
    public function actionIndex($topic = null)
    {
        $searchModel = new PollSearch();
        $searchModel->topic_id = $topic;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Poll models.
     *
     * @param integer $topic
     *
     * @return mixed
     */
    public function actionCurrent($topic = null)
    {
        $searchModel = new PollSearch();
        $searchModel->topic_id = $topic;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('current', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Poll model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    //public function actionView($id) {
    //	return $this->render('view', [
    //		'model' => $this->findModel($id),
    //	]);
    //}

    /**
     * Creates a new Poll model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Poll();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->getCreateUpdateResponse($model);
        }
        else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Poll model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->getCreateUpdateResponse($model);
        }
        else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Poll model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param array $id
     *
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $id = (array) $id;
        foreach ($id as $id_) {
            $this->findModel($id_)->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Poll model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Poll the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Poll::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $id
     * @return $this
     */
    public function actionExportXml($id){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
        $uploadArr = Poll::getArrXml($id);
        return \Yii::$app->response->sendContentAsFile($uploadArr, 'polls.xml');
    }

    public function actionSmsSend($poll_id){
        if ($poll_id && Poll::sendSms($poll_id)){
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Уведомления по SMS поставлены в очередь на отправку'));
            return $this->redirect(Yii::$app->request->referrer);
        }else{
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_WARNING, \Yii::t('admin/t', 'Уведомления не отправлены'));
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionEmailSend($poll_id){
        if($poll_id && Poll::sendEmail($poll_id)){
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Уведомления по E-mail поставлены в очередь на отправку'));
            return $this->redirect(Yii::$app->request->referrer);
        }else{
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_WARNING, \Yii::t('admin/t', 'Уведомления не отправлены'));
            return $this->redirect(Yii::$app->request->referrer);
        }
    }
}
