<?php

namespace modules\polls\backend\controllers;

use modules\polls\backend\base\Controller;
use modules\polls\backend\models\QuestionSearch;
use modules\polls\common\models\Question;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * QuestionController implements the CRUD actions for Question model.
 */
class QuestionController
	extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Question models.
	 *
	 * @param integer $poll
	 *
	 * @return mixed
	 */
	public function actionIndex($poll) {
		$searchModel = new QuestionSearch();
		$searchModel->poll_id = $poll;
		//$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider = $searchModel->search(null);

		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'pollId'       => $poll,
		]);
	}

	/**
	 * Displays a single Question model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	//public function actionView($id) {
	//	return $this->render('view', [
	//		'model' => $this->findModel($id),
	//	]);
	//}

	/**
	 * Creates a new Question model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $poll
	 *
	 * @return mixed
	 */
	public function actionCreate($poll) {
		$model = new Question();
		$model->poll_id = $poll;
		$model->answers_quantity = 1;

		if($model->load(Yii::$app->request->post()) && $model->save()) {
			//return $this->redirect(['view', 'id' => $model->id]);
			return $this->getCreateUpdateResponse($model);
		} else {
			return $this->render('create', [
				'model'  => $model,
				'pollId' => $poll,
			]);
		}
	}

	/**
	 * Updates an existing Question model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);

		if($model->load(Yii::$app->request->post()) && $model->save()) {
			//return $this->redirect(['view', 'id' => $model->id]);
			return $this->getCreateUpdateResponse($model);
		} else {
			return $this->render('update', [
				'model'  => $model,
				'pollId' => $model->poll_id,
			]);
		}
	}

	/**
	 * Deletes an existing Question model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param array $id
	 *
	 * @return mixed
	 */
	public function actionDelete(array $id) {
		$poll = null;
		$id = (array)$id;
		foreach($id as $id_) {
			$model = $this->findModel($id_);
			$poll = $model->poll_id;
			$model->delete();
		}

		return $this->redirect(['index', 'poll' => $poll]);
	}

	/**
	 * Finds the Question model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Question the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = Question::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
