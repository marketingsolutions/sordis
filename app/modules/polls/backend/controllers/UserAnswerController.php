<?php

namespace modules\polls\backend\controllers;

use yii\helpers\Html;
use modules\polls\backend\base\Controller;
use modules\polls\backend\models\QuestionSearch;
use modules\polls\backend\models\UserAnswerSearch;
use modules\polls\common\models\UserAnswer;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yz\admin\actions\ExportAction;

/**
 * UserAnswerController implements the CRUD actions for UserAnswer model.
 */
class UserAnswerController
	extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	public function actions() {
		return array_merge(parent::actions(), [
			'export' => [
				'class'        => ExportAction::className(),
				'searchModel'  => function ($params) {
					/** @var OrderedCardSearch $searchModel */
					return Yii::createObject(QuestionSearch::className());
				},
				'dataProvider' => function ($params, QuestionSearch $searchModel) {
					//$dataProvider = $searchModel->search($params);
					$searchModel->poll_id = Yii::$app->request->get('poll');
					$dataProvider = $searchModel->search(null);

					return $dataProvider;
				},
			],
		]);
	}

	public function getGridColumns(QuestionSearch $searchModel) {
		return [
			'question',
			[
				'attribute' => 'resultTotal',
				'label'     => 'Ответы',
				'format'    => 'html',
				'value'     => function ($model) {
					if(empty($model->resultTotal)) {
						return null;
					}
					$answer = '';
					foreach($model->resultTotal as $item) {
						$answer .= '' . Html::encode($item['answer']) . ' : ' . $item['count'] . '<br/>';
					}

					return $answer;
				},
			],
		];
	}

	/**
	 * Lists all UserAnswer models.
	 *
	 * @param $poll integer
	 *
	 * @return mixed
	 */
	public function actionIndex($poll) {
		$searchModel = new QuestionSearch();
		$searchModel->poll_id = $poll;
		//$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider = $searchModel->search(null);
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Lists all UserAnswer models.
	 *
	 * @param $user integer
	 * @param $poll integer
	 *
	 * @return mixed
	 */
	public function actionDetail($user, $poll) {
		$searchModel = new UserAnswerSearch();
		$searchModel->user_id = $user;
		$searchModel->poll_id = $poll;
		//$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider = $searchModel->search(null);

		return $this->render('detail', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single UserAnswer model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 */
	//public function actionView($id) {
	//	return $this->render('view', [
	//		'model' => $this->findModel($id),
	//	]);
	//}

	/**
	 * Creates a new UserAnswer model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	//public function actionCreate() {
	//	$model = new UserAnswer();
	//
	//	if($model->load(Yii::$app->request->post()) && $model->save()) {
	//		return $this->redirect(['view', 'id' => $model->id]);
	//	} else {
	//		return $this->render('create', [
	//			'model' => $model,
	//		]);
	//	}
	//}

	/**
	 * Updates an existing UserAnswer model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 */
	//public function actionUpdate($id) {
	//	$model = $this->findModel($id);
	//
	//	if($model->load(Yii::$app->request->post()) && $model->save()) {
	//		return $this->redirect(['view', 'id' => $model->id]);
	//	} else {
	//		return $this->render('update', [
	//			'model' => $model,
	//		]);
	//	}
	//}

	/**
	 * Deletes an existing UserAnswer model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 */
	//public function actionDelete($id) {
	//	$this->findModel($id)->delete();
	//
	//	return $this->redirect(['index']);
	//}

	/**
	 * Finds the UserAnswer model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return UserAnswer the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = UserAnswer::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
