<?php

namespace modules\polls\backend\controllers;

use modules\polls\backend\base\Controller;
use modules\polls\backend\models\ConditionSearch;
use modules\polls\common\models\Condition;
use modules\polls\common\models\Question;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ConditionController implements the CRUD actions for Condition model.
 */
class ConditionController
	extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Condition models.
	 *
	 * @return mixed
	 */
	public function actionIndex($question) {
		$searchModel = new ConditionSearch();
		$searchModel->question_id = $question;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$modelQuestion = $this->findQuestion($question);

		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'pollId'       => ($modelQuestion ? $modelQuestion->poll_id : null),
			'questionId'   => $question,
		]);
	}

	/**
	 * Displays a single Condition model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	//public function actionView($id) {
	//	return $this->render('view', [
	//		'model' => $this->findModel($id),
	//	]);
	//}

	/**
	 * Creates a new Condition model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $question
	 *
	 * @return mixed
	 */
	public function actionCreate($question) {
		$model = new Condition();
		$model->question_id = $question;

		if($model->load(Yii::$app->request->post()) && $model->save()) {
			//return $this->redirect(['view', 'id' => $model->id]);
			return $this->getCreateUpdateResponse($model);
		} else {
			$modelQuestion = $this->findQuestion($question);

			return $this->render('create', [
				'model'      => $model,
				'pollId'     => ($modelQuestion ? $modelQuestion->poll_id : null),
				'questionId' => $question,
			]);
		}
	}

	/**
	 * Updates an existing Condition model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	//public function actionUpdate($id) {
	//	$model = $this->findModel($id);
	//
	//
	//	if($model->load(Yii::$app->request->post()) && $model->save()) {
	//		//return $this->redirect(['view', 'id' => $model->id]);
	//		return $this->getCreateUpdateResponse($model);
	//	} else {
	//		$modelQuestion = $this->findQuestion($model->question_id);
	//
	//		return $this->render('update', [
	//			'model'      => $model,
	//			'pollId'     => ($modelQuestion ? $modelQuestion->poll_id : null),
	//			'questionId' => $model->question_id,
	//		]);
	//	}
	//}

	/**
	 * Deletes an existing Condition model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param array $id
	 *
	 * @return mixed
	 */
	public function actionDelete(array $id) {
		$question = null;
		$id = (array)$id;
		foreach($id as $id_) {
			$model = $this->findModel($id_);
			$question = $model->question_id;
			$model->delete();
		}

		return $this->redirect(['index', 'question' => $question]);
	}

	/**
	 * Finds the Condition model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Condition the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = Condition::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	/**
	 * Finds the Question model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $question
	 *
	 * @return Question the loaded model
	 */
	protected function findQuestion($question) {
		return Question::findOne($question);
	}
}
