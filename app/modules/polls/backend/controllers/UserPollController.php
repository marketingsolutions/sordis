<?php

namespace modules\polls\backend\controllers;

use modules\polls\backend\base\Controller;
use modules\polls\backend\models\UserPollSearch;
use modules\polls\common\models\UserPoll;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yz\admin\actions\ExportAction;

/**
 * UserPollController implements the CRUD actions for UserPoll model.
 */
class UserPollController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function ($params) {
                    /** @var UserPollSearch $searchModel */
                    return Yii::createObject(UserPollSearch::className());
                },
                'dataProvider' => function ($params, UserPollSearch $searchModel) {
                    //$dataProvider = $searchModel->search($params);
                    $searchModel->poll_id = Yii::$app->request->get('poll');
                    $dataProvider = $searchModel->search(null);

                    return $dataProvider;
                },
            ],
        ]);
    }

    public function getGridColumns(UserPollSearch $searchModel)
    {
        return [
            'id',
            'created_at',
            'profile.full_name',
            'finished:boolean',
        ];
    }

    /**
     * Lists all UserPoll models.
     *
     * @param integer $poll
     *
     * @return mixed
     */
    public function actionIndex($poll = null)
    {
        $searchModel = new UserPollSearch();
        $searchModel->poll_id = $poll;
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search(null);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Deletes an existing UserPoll model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param string $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {

        $model = $this->findModel($id);
        $poll = $model->poll_id;
        $model->delete();

        return $this->redirect(['index', 'poll' => $poll]);
    }

    /**
     * Finds the UserPoll model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param string $id
     *
     * @return UserPoll the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserPoll::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
