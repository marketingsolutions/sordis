<?php

namespace modules\calc\common\models;

use modules\bonuses\common\models\TempBonuses;
use modules\profiles\common\models\Profile;
use Yii;
use yz\admin\behaviors\UploadImageBehavior;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_calculator".
 *
 * @property integer $id
 * @property integer $sku_min
 * @property integer $sku_max
 * @property float $coefficient
 * @property integer $group
 * @property string $photo
 */
class Calc extends \yii\db\ActiveRecord implements ModelInfoInterface
{

    const GROUP_ONE = 1;
    const GROUP_TWO = 2;
    const DATA_DIR = 'photos'; // фотография будет сохраняться в папку "data/photos"

    public function behaviors() {
        return [
            'upload-image' => [
                'class' => UploadImageBehavior::class,
                'attributes' => ['photo'],
                'directory' => self::DATA_DIR,
            ],
         ];
    }
    public function getPhotoUrl() {
        return empty($this->photo)  ? null : Yii::getAlias('@frontendWeb/data/' . self::DATA_DIR . '/') . $this->photo;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%calculator}}';
    }

	public static function getGroupValue(){
        return [
            self::GROUP_ONE => 'Группа 1 (ср SKU от 8)',
            self::GROUP_TWO => 'Группа 2 (ср SKU от 0 до 8)',
        ];
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Calc';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Коэфициенты для калькулятора у участника РТТ';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['sku_min', 'integer'],
            ['photo', 'safe'],
            ['sku_max', 'integer'],
            [['coefficient'], 'double'],
            ['group', 'integer'],
            [['sku_min', 'sku_max', 'coefficient'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID#',
            'sku_min' => 'SKU от',
            'sku_max' => 'SKU до',
            'coefficient' => 'Коэфициент',
            'group' => 'Группа для PТТ',
            'photo' => 'Фото',
        ];
    }

    /**
     * Получаем Целевые баллы текущего периода
     * @param $profileId
     * @return int|mixed
     */
    public static function getTargetBonus($profileId){
        $profile = Profile::findOne(['id' => $profileId]);
        $now = date("Y-m")."-01";
        return TempBonuses::find()
            ->where(['bonus_type' => TempBonuses::TARGET_BONUS])
            ->andWhere(['period' => $now])
            ->andWhere(['phone_mobile' => $profile->phone_mobile])
            ->sum('bonus_sum');
    }

    /**
     * Получаем коэфициент исходя из группы
     * @param $group
     * @param $sku
     * @return bool|mixed
     */
    public static function getRttCoefficient($sku){
        $koeff = self::find()
            ->where("$sku >= sku_min")
            ->andWhere("$sku <= sku_max")
            ->asArray()
            ->one();
        return empty($koeff) ? false : $koeff['coefficient'];
    }
}
