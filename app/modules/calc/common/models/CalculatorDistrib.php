<?php

namespace modules\calc\common\models;

use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_calculator_distrib".
 *
 * @property integer $id
 * @property double $akbs
 * @property double $koeff_akbs
 * @property double $akbs_max
 */
class CalculatorDistrib extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%calculator_distrib}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Calculator Distrib';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Коэфициенты для калькулятора у участника Дистрибьютора';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['akbs', 'number'],
            ['akbs_max', 'number'],
            ['koeff_akbs', 'number'],
            [['akbs', 'koeff_akbs'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'akbs' => '%АКБС от',
            'akbs_max' => '%АКБС до',
            'koeff_akbs' => 'К%акбс',
        ];
    }

    /**
     * Расчет основных баллов
     * @param $sku
     * @param $sumTT
     * @param $akb4month
     * @return float
     */
    public static function process($sku, $sumTT, $akb4month){
        if(!isset($akb4month) || !$akb4month){
            return 0;
        }
        $akbs = $sumTT/$akb4month*100;
        $koefPoktit = self::find()
            ->where("akbs <= '".$akbs."'")
            ->andWhere("akbs_max >= '".$akbs."'")
            ->asArray()
            ->one();

        return round($koefPoktit['koeff_akbs']*$sku);
    }
}
