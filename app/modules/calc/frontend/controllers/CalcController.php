<?php
namespace modules\calc\frontend\controllers;

use modules\bonuses\common\models\TempConfirmedBonuses;
use modules\calc\common\models\Calc;
use modules\calc\common\models\CalculatorDistrib;
use modules\calc\frontend\forms\CalculateFormDistrib;
use modules\calc\frontend\forms\CalculateFormRtt;
use modules\profiles\common\models\Profile;
use yii\web\Controller;
use yii\web\Response;

class CalcController extends Controller
{
    /**
     * Страница калькулятора
     * @return string
     */
    public function actionIndex(){
        $profile = \Yii::$app->user->identity->profile;
        $targetBonus = Calc::getTargetBonus($profile->id);
        if ($profile->role == Profile::ROLE_MANAGER_TT){
            $calcForm = new CalculateFormRtt();
        }else{
            $calcForm = new CalculateFormDistrib();
        }
        return $this->render('index',[
            'profile' => $profile,
            'targetBonus' => $targetBonus,
            'calcForm' => $calcForm,
        ]);
    }




    /**
     * Расчет бонусов для РТТ-калькулятора через Ajax
     * @return array
     */
    public function actionCalculateRtt(){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $sku = \Yii::$app->request->post('sku');
        $volume = \Yii::$app->request->post('vol');
        if (strpos($sku, ',') || strpos($sku, '.')){
            return $this->error('Значение SKU не может быть дробным числом!');
        }

        if( $sku && $volume){
            $koeff = Calc::getRttCoefficient($sku);
            if(!$koeff){
                return ['result' => 'OK', 'bonus' => 0];
            }
            $bonus = $volume*$koeff;
            return ['result' => 'OK', 'bonus' => $bonus];
        }
        return $this->error('Ошибка в переданных данных!');
    }

    /*Расчет бонусов для Дистрибьюторов через Ajax*/
    public function actionCalculateDistrib(){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $sku = \Yii::$app->request->post('sku');  //Количество SKU
        $sumTT = \Yii::$app->request->post('sumTT'); //Количество ТТ
       // $akb4month = 10; //АКБ за 4 мес (Далее будет приходить из XML)
        /** @var  Profile */
        $profile = \Yii::$app->user->identity->profile;

        $akb4month = TempConfirmedBonuses::find()
            ->where(['phone_mobile' => $profile->phone_mobile])
            ->orderBy(['period' => SORT_DESC])
            ->asArray()
            ->one();

        if(empty($akb4month)){
            $akb4month = 0;
        }

        if (!$sku || !$sumTT){
            return [
                'result' => 'FAIL',
                'msg' => 'Суммаотгруженных SKU или количество ТТ не может пустым или быть равно 0',
            ];
        }

        return ['result' => 'OK', 'basic' => CalculatorDistrib::process($sku, $sumTT, $akb4month['state'])];
    }

    private function error($errorMsg)
    {
        \Yii::$app->response->statusCode = 400;

        return ['result' => 'FAIL', 'errors' => [$errorMsg]];
    }
}