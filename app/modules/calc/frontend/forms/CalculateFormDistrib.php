<?php
namespace modules\calc\frontend\forms;

use yii\base\Model;

class CalculateFormDistrib extends Model
{

    /**
     * @var
     */
    public $sku_sum;

    /**
     * @var
     */
    public $sum_tt;

    /**
     * @var
     */
    public $sum_basic;

    /**
     * @var
     */
    public $sum_target;

    /**
     * @var
     */
    public $v_litr;

    /**
     * @var
     */
    public $ball_litr;

    /**
     * @var
     */
    public $total_sum;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['sku_sum', 'sum_tt', 'ball_litr', 'sum_basic', 'sum_target', 'v_litr', 'total_sum', 'akbs'], 'integer', 'min' => 0],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'sku_sum' => 'Объем отгрузки, литров',
            'sum_tt' => 'Сумма отгруженных SKU',
            'sum_basic' => 'Сумма основных баллов',
            'v_litr' => 'Объем отгрузки акционного ассортимента, литров',
            'ball_litr' => 'Количество баллов за литр акционного ассортимента, согласно условиям',
            'sum_target' => 'Сумма целевых баллов',
            'total_sum' => 'ВСЕГО',
        ];
    }
}