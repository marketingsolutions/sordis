<?php
namespace modules\calc\frontend\forms;

use yii\base\Model;

class CalculateFormRtt extends Model
{

    /*Основные баллы*/
    /**
     * @var
     */
    public $basic_sku;

    /**
     * @var
     */
    public $basic_vol;

    /**
     * @var
     */
    public $basic_sum;


    /*Целевые баллы*/
    /**
     * @var
     */
    public $target_vol;

    /**
     * @var
     */
    public $target_count_ball;

    /**
     * @var
     */
    public $target_sum_ball;


    /*Призовые баллы*/
    /**
     * @var
     */
    public $poll_ball;

    /**
     * @var
     */
    public $action_ball;

    /**
     * @var
     */
    public $sum_priz_ball;


    /*Итоговые баллы*/
    public $total_sum_ball;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['basic_sku', 'basic_vol', 'basic_sum', 'target_vol', 'target_count_ball', 'target_sum_ball', 'poll_ball',  'action_ball', 'sum_priz_ball', 'total_sum_ball'], 'double', 'min' => 0, 'message' => 'Разделитель целой и дробной части должна быть точка, а не запятая'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'basic_sku' => 'SKU',
            'basic_vol' => 'Объем закупки,  литров',
            'basic_sum' => 'Сумма основных баллов',
            'target_vol' => 'Объем отгрузки акционного ассортимента, литров',
            'target_count_ball' => 'Количество баллов за литр акционного ассортимента, согласно условиям',
            'target_sum_ball' => 'Сумма целевых баллов',
            'poll_ball' => 'Баллы за прохождение опроса, согласно условиям',
            'action_ball' => 'Баллы за призовые акции, согласно условиям',
            'sum_priz_ball' => 'Сумма призовых баллов',
            'total_sum_ball' => 'ВСЕГО',
        ];
    }

}
