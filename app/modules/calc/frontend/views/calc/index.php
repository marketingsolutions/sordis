<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\calc\common\models\Calc;
use modules\profiles\common\models\Profile;

$this->title = 'Калькулятор';
$this->params['header'] = $this->title;;
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
 #reg-errors > li {
     list-style-type: none;
     font-size: 18px;
   }
.calc_calc{
    display: none;
}
CSS;
$this->registerCss($css);

$js = <<<JS
$( document ).ready(function() {
    
    /*Подсчет итоговой суммы по ртт*/
    function calculateRttTotalSum() {
        var totalRttSum = 0;
        var basic_sum=0;
        $( '.basic_summ_rtt' ).each( function(index, elem) {
              basic_sum = $( elem ).val();
              if(basic_sum == ''){
                  basic_sum=0;
              }
              totalRttSum = totalRttSum + parseFloat(basic_sum);
             // console.log(basic_sum);
        });
        
        
        var target_sum_ball=0;
         $( ".target_sum_ball_rtt" ).each( function(index, elem) {
              target_sum_ball = $( elem ).val();
              if(target_sum_ball == ''){
                  target_sum_ball=0;
              }
              totalRttSum = totalRttSum + parseInt(target_sum_ball);
        });
        var sum_priz_ball = $('#calculateformrtt-sum_priz_ball').val();
        if(sum_priz_ball==''){
            sum_priz_ball=0;
        }
        totalRttSum = totalRttSum + parseInt(sum_priz_ball);
        totalRttSum = Math.round(totalRttSum);
        console.log(totalRttSum);
        $('#calculateformrtt-total_sum_ball').val('');
        $('#calculateformrtt-total_sum_ball').val(totalRttSum);
    }
    
    /*Подсчет итоговой суммы по дистрам*/
    function calculateDistribTotalSum() {
        var toalDistribSum = 0;
       
              var basic_sum = $( "input[name*=sum_basic]" ).val();
              if(basic_sum == ""){
                  basic_sum=0;
              }
              toalDistribSum = toalDistribSum + parseInt(basic_sum);
        var sum_target=0;
         $( ".sum_target_distrib" ).each( function( index , elem) {
              sum_target = $( elem ).val();
              if(sum_target == ""){
                  sum_target=0;
              }
              //console.log(sum_target);
              toalDistribSum = toalDistribSum + parseInt(sum_target);
              toalDistribSum = Math.round(toalDistribSum);
        });
       
        $('#calculateformdistrib-total_sum').val('');
        $('#calculateformdistrib-total_sum').val(toalDistribSum);
        
    }
    
    
    var roleUser = $('#role').val();
    //=====================Если роль - участник РТТ=================================================================================
    if(roleUser == 'manager_tt'){
        
        /*ОСНОВНЫЕ БАЛЛЫ*/
        /*Добавляем макисмум 10 полей для основных баллов*/
        var ii=1;
        var j=1;
        $('body').on('click', '.add_basic', function(e){
            e.preventDefault();
             ii++;
            if(j<10){
               j++; 
            }
           
            if(ii <= 10){
                $(".row_basic_calc").clone().appendTo(".add_basic_input").removeClass('row_basic_calc').addClass('row_basic_calc'+ii);
                $('.row_basic_calc'+ii).children().children().find('input[name*=basic_sku]').val('');
                $('.row_basic_calc'+ii).children().children().find('input[name*=basic_vol]').val('');
                $('.row_basic_calc'+ii).children().children().find('input[name*=basic_sum]').val('');
            }           
             if(j==1){
                 $('.butt_del_basic').hide();
             }else{
                 $('.butt_del_basic').show();
             }     
        });
        
        /*Удаление формы расчета основных баллов*/
        $('body').on('click', '.butt_del_basic',function(e){
            e.preventDefault();
             ii--;
            $('.add_basic_row').last().remove();
            if(j>1){
                 j--;
            }
            
            if(j==1){
                 $('.butt_del_basic').hide();
            }else{
                $('.butt_del_basic').show();
            }     
            calculateRttTotalSum();
       });
        
        /*ЦЕЛЕВЫЕ БАЛЛЫ*/
        /*Добавление максимум 5 формы для целевых баллов*/        
        var iii=1;
        var jj=1;
        $('body').on('click', '.add_target',function(e){
            e.preventDefault();
             iii++;
            if(j<5){
               jj++; 
            }
           
            if(iii <= 5){
                $(".row_target_calc").clone().appendTo(".add_target_input").removeClass('row_target_calc').addClass('row_target_calc'+iii);
                $('.row_target_calc'+iii).children().children().find('input[name*=target_vol]').val('');
                $('.row_target_calc'+iii).children().children().find('input[name*=target_count_ball]').val('');
                $('.row_target_calc'+iii).children().children().find('input[name*=target_sum_ball]').val('');
            }           
             if(jj==1){
                 $('.butt_del_target').hide();
             }else{
                 $('.butt_del_target').show();
             }     
        });
        
        /*Удаление формы расчета целевых баллов*/
        $('body').on('click', '.butt_del_target',function(e){
            e.preventDefault();
             iii--;
            $('.add_target_row').last().remove();
            if(jj>1){
                 jj--;
            }
            
            if(jj==1){
                 $('.butt_del_target').hide();
            }else{
                $('.butt_del_target').show();
            }
            calculateRttTotalSum();
       });
        
        /*Вывод окошка с напоминашкой об участии в целевых акциях*/
        $('body').on('click', '.butt_target_ball, .butt_osn_ball, .btn_calc_basic, .btn_calc_target, .btn_calc_poll',function(e){
            e.preventDefault();
            var volTarget = $('.panel_ferma').find('input[name*=target_vol]').val();
            var volCount = $('.panel_ferma').find('input[name*=target_count_ball]').val();
            var messageInfo = '';
            if(volTarget == '' && volCount == ''){
                $('.calc_calc').show();
            }else{
                $('.calc_calc').hide();
            }
            
        });
        
        /*Расчет призовых баллов*/
        $('body').on('click', '.btn_calc_poll',function(e){
            e.preventDefault();
            $("#reg-errors li").remove();
            var action_ball = $(this).parent().parent().find('input[name*=action_ball]').val();
            var poll_ball = $(this).parent().parent().find('input[name*=poll_ball]').val();
            if(!action_ball || !poll_ball || action_ball==0 || poll_ball==0){
                $("#reg-errors").append("<li>" + 'Бонусы за опросы и/или призовые акции не должны быть пустыми или равны нулю' + "</li>");
            }else{
                var total_action_poll = parseFloat(action_ball) + parseFloat(poll_ball);
                $('#calculateformrtt-sum_priz_ball').val('');
                $('#calculateformrtt-sum_priz_ball').val(total_action_poll);
                calculateRttTotalSum();
            }   
        });
        
        /*Рассчет целевых баллов*/
        $('body').on('click', '.btn_calc_target', function(e){
             e.preventDefault();
             $("#reg-errors li").remove();
             var target_vol = $(this).parent().parent().find('input[name*=target_vol]').val();
             var target_count_ball = $(this).parent().parent().find('input[name*=target_count_ball]').val();
             var target_sum_ball = $(this).parent().parent().find('input[name*=target_sum_ball]');
             if(!target_vol || !target_count_ball || target_count_ball==0 || target_vol==0){
                  $("#reg-errors").append("<li>" + 'Количество баллов за литр и/или объем отгрузки не должны быть пустыми или равны нулю!' + "</li>");
             }else{
                 var total_target_poll = parseFloat(target_vol) * parseFloat(target_count_ball);
                 target_sum_ball.val('');
                 target_sum_ball.val(total_target_poll);
                 calculateRttTotalSum();
             }
             
        });
       
        /*Рассчет  основных баллов*/
        $('body').on('click', '.btn_calc_basic',function(e){
            e.preventDefault();
            var basic_sku = $(this).parent().parent().find('input[name*=basic_sku]').val();
            var basic_vol = $(this).parent().parent().find('input[name*=basic_vol]').val();
            var result_input = $(this).parent().parent().find('input[name*=basic_sum]');
             
            if(!basic_sku || !basic_vol || basic_vol==0 || basic_sku == 0){
                $("#reg-errors li").remove();
                $("#reg-errors").append("<li>" + 'SKU и/или объем отгрузки не должны быть пустыми или равны нулю' + "</li>");
            }else{
                $.ajax(
                    {
                      type: 'POST',
                      url: '/calc/calc/calculate-rtt',
                      data: {"sku": basic_sku, "vol": basic_vol},
                      success: function(data) {
                        //console.log(data.bonus);
                        $("#reg-errors li").remove();
                        result_input.val(data.bonus);
                        calculateRttTotalSum();
                       
                    },
                    error:   function(xhr) {
                        var data = xhr.responseJSON;
                        if ("errors" in data) {
                            $('#result_bonus').text('0');
                            $("#reg-errors li").remove();
                            for (var i = 0; i < data.errors.length; i++) {
                                $("#reg-errors").append("<li>" + data.errors[i] + "</li>");
                            }
                        }
                    }
                });
            }
        });
        
      
        //====================Если роль участник Дистрибьютор============================================================================
    }else{ 
        
        /*Вывод окошка с напоминашкой об участии в целевых акциях*/
        $('body').on('click', '.butt_target_ball, .butt_osn_ball',function(e){
            e.preventDefault();
            var volTarget = $('.panel_ferma').find('input[name*=v_litr]').val();
            var volCount = $('.panel_ferma').find('input[name*=ball_litr]').val();
            var messageInfo = '';
            if(volTarget == '' && volCount == ''){
               // console.log('!!!!');
                $('.calc_calc').show();
            }else{
                $('.calc_calc').hide();
            }
            
        });
        
        /*Рассчет основных баллов*/    
         $('body').on('click', '.butt_osn_ball',function(e){
             e.preventDefault();
             var sku = $("#calculateformdistrib-sku_sum").val();
             var sumTT = $("#calculateformdistrib-sum_tt").val();
             $("#reg-errors li").remove();
             if(!sku || !sumTT || sku == 0  || sumTT ==0){
                 $("#reg-errors").append("<li>" + 'Сумма отгруженных SKU и/или количество ТТ не должны быть пустыми или равным нулю' + "</li>");
             }else{ 
                 //Получение коэфициента покрытия
                 $.ajax(
                        {
                          type: 'POST',
                          url: '/calc/calc/calculate-distrib',
                          data: {"sku": sku, "sumTT": sumTT},
                          success: function(data) {
                           // console.log(data);
                            $("#reg-errors li").remove();
                            $('#calculateformdistrib-sum_basic').text('');
                            $('#calculateformdistrib-sum_basic').val(data.basic);
                            calculateDistribTotalSum();
                        },
                        error:   function(xhr) {
                            var data = xhr.responseJSON;
                            if ("errors" in data) {
                                $('#result_bonus').text('0');
                                $("#reg-errors li").remove();
                                for (var i = 0; i < data.errors.length; i++) {
                                    $("#reg-errors").append("<li>" + data.errors[i] + "</li>");
                                }
                            }
                        }
                    });             
             }
         });
         
         //Подсчет суммы целевых баллов для Дистрибьюторов
         $('body').on('click', '.butt_target_ball', function(e){
             e.preventDefault();
             var litr = $(this).parent().parent().find('input[name*=v_litr]').val();
             var ball_litr = $(this).parent().parent().find('input[name*=ball_litr]').val();
             var sum_target =  $(this).parent().parent().find('input[name*=sum_target]');
             var sum_target_ball = litr*ball_litr;
             sum_target.val(sum_target_ball);
             calculateDistribTotalSum();
         });
                 
        /*Ограничение для добавления не более 5 полей для целевых баллов и поведение динамических столбцов*/
        var j=1;
        var ii=1;
        /*Добавление формы расчета целевых баллов*/
        $('body').on('click', '.butt_add',function(e){
            e.preventDefault();
            ii++;
            if(j<5){
               j++; 
            }
           
            if(ii <= 5){
                $(".add_div").clone().appendTo(".dynamic_add").removeClass('add_div').addClass('add_div'+ii);
                $('.add_div'+ii).children().children().find('input[name*=ball_litr]').val('');
                $('.add_div'+ii).children().children().find('input[name*=sum_target]').val('');
                $('.add_div'+ii).children().children().find('input[name*=v_litr]').val('');
            }           
             if(j==1){
                 $('.butt_del').hide();
             }else{
                 $('.butt_del').show();
             }     
        });
        
       /*Удаление формы расчета целевых баллов*/
        $('body').on('click', '.butt_del',function(e){
            e.preventDefault();
             ii--;
            $('.add_target').last().remove();
            if(j>1){
                 j--;
            }
           
            if(j==1){
                 $('.butt_del').hide();
            }else{
                $('.butt_del').show();
            }  
            calculateDistribTotalSum();
       });        
    }
});
JS;

$this->registerJs($js);

?>

<input type="hidden" id="role" value="<?= $profile->role ?>">
<div class="row">
  <div class="col-md-12 calculator-page">
    <div class="panel panel_ferma">

        <div class="alert calc_calc alert-warning alert-dismissible" role="alert">
            <strong>Уважаемые участники! </strong><br/>Примите участие в целевой акции и получите дополнительные баллы!<br/>
            <strong>Примечание:</strong> <i>В поля калькулятора можно вводить и дробные значения. Разделителем целой и дробной части должна быть точка!</i>
        </div>

        <?php if ($profile->role == Profile::ROLE_MANAGER_TT): ?>

            <?php $form = ActiveForm::begin(['options' => ['id' => 'calc_rtt_form']]); ?>
          <!--Основные баллы-->
          <h4>Расчет основных баллов для Торговой точки </h4>
          <div class="add_basic_input calc-table calc-table2">
            <div class="add_basic_row row_basic_calc calc-table--line">
              <div class="calc-table--col ctc1">
                  <?= $form->field($calcForm, 'basic_sku') ?>
              </div>
              <div class="calc-table--col ctc2">
                  <?= $form->field($calcForm, 'basic_vol') ?>
              </div>

              <div class="calc-table--col ctc3">
                  <?= $form->field($calcForm, 'basic_sum')->textInput(['class' => 'form-control basic_summ_rtt']) ?>
              </div>

              <div class="calc-table--btn">
                  <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-primary btn_all btn_calc_basic']) ?>
              </div>
            </div>
          </div>

          <div class="button-row">

              <?= Html::submitButton('добавить', ['class' => 'btn btn_all add_basic']) ?>&nbsp;
              <?= Html::submitButton('удалить', ['class' => 'btn btn_blue butt_del_basic']) ?>

          </div>

          <h4>Расчет целевых баллов</h4>
          <div class="add_target_input">
            <div class="row_target_calc add_target_row calc-table">
              <div class="calc-table--line">
                <div class="calc-table--col ctc1">
                    <?= $form->field($calcForm, 'target_vol') ?>
                </div>
                <div class="calc-table--col ctc2">
                    <?= $form->field($calcForm, 'target_count_ball') ?>
                </div>
                <div class="calc-table--col ctc3">
                    <?= $form->field($calcForm, 'target_sum_ball')->textInput(['class' => 'form-control target_sum_ball_rtt']) ?>
                </div>
              </div>
              <div class="calc-table--btn">
                  <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-primary btn_all btn_calc_target']) ?>
              </div>
            </div>
          </div>
          <div class="button-row">
              <?= Html::submitButton('добавить', ['class' => 'btn btn_all add_target']) ?>&nbsp;
              <?= Html::submitButton('удалить', ['class' => 'btn btn_blue butt_del_target']) ?>
          </div>

          <h4>Расчет призовых баллов</h4>
          <div class="calc-table">
            <div class="calc-table--line">
              <div class="calc-table--col ctc1">

                  <?= $form->field($calcForm, 'poll_ball') ?>
              </div>
              <div class="calc-table--col ctc2">
                  <?= $form->field($calcForm, 'action_ball') ?>
              </div>

              <div class="calc-table--col ctc3">
                  <?= $form->field($calcForm, 'sum_priz_ball') ?>
              </div>
            </div>

            <div class="calc-table--btn">
                <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-primary btn_all btn_calc_poll']) ?>
            </div>
          </div>


          <ul style="color: red;" id="reg-errors"></ul>

          <div class="total-summ">
              <?= $form->field($calcForm, 'total_sum_ball') ?>
          </div>


            <?php ActiveForm::end(); ?>
        <?php else: ?>

          <h4>Расчет основных баллов</h4>
            <?php $form = ActiveForm::begin(['options' => ['id' => 'calc_distrib_form']]); ?>
          <div class="row_calc_basic calc-table main_balls">
            <div class="calc-table--line">
              <div class="calc-table--col ctc1">
                  <?= $form->field($calcForm, 'sku_sum') ?>
              </div>
            </div>
            <div class="calc-table--line">
              <div class="calc-table--col ctc2">
                  <?= $form->field($calcForm, 'sum_tt') ?>
              </div>
            </div>
            <div class="calc-table--line">
              <div class="calc-table--col ctc3">
                  <?= $form->field($calcForm, 'sum_basic') ?>
              </div>
            </div>
            <div class="calc-table--btn">
                <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-primary btn_all butt_osn_ball']) ?>
            </div>
          </div>

          <h4>Расчет целевых баллов</h4>
          <div class="dynamic_add">
            <div class="row_calc add_target add_div calc-table">
              <div class="calc-table--line">
                <div class="calc-table--col ctc1">
                    <?= $form->field($calcForm, 'v_litr') ?>
                </div>
              </div>
              <div class="calc-table--line">
                <div class="calc-table--col ctc2">
                    <?= $form->field($calcForm, 'ball_litr') ?>
                </div>
              </div>

              <div class="calc-table--line">
                <div class="calc-table--col ctc3">
                    <?= $form->field($calcForm, 'sum_target')->textInput(['class' => 'form-control sum_target_distrib']) ?>
                </div>
              </div>

              <div class="calc-table--btn">
                  <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-primary btn_all butt_target_ball']) ?>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3">
                <?= Html::submitButton('добавить', ['class' => 'btn btn-primary btn_all butt_add']) ?>&nbsp;
                <?= Html::submitButton('удалить', ['class' => 'btn btn_blue butt_del']) ?>
            </div>
          </div>
          <ul style="color: red;" id="reg-errors"></ul>

            <div class="total-summ">
                <?= $form->field($calcForm, 'total_sum') ?>
            </div>

            <?php ActiveForm::end(); ?>

        <?php endif; ?>
    </div>
  </div>
</div>
