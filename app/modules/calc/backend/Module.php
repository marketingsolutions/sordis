<?php

namespace modules\calc\backend;

use yz\icons\Icons;

/**
 * Class Module
 */
class Module extends \modules\calc\common\Module
{
    public function getAdminMenu()
    {
		return [
			[
				'label' => 'Калькулятор',
				'icon' => Icons::o('user'),
				'items' => [
					[
						'route' => ['/calc/calc/index'],
						'label' => 'Коэфициенты для калькулятора у участника ТТ',
						'icon' => Icons::o('calculator'),
					],
                    [
                        'route' => ['/calc/calculator-distrib/index'],
                        'label' => 'Коэфициенты для калькулятора у участника Дистрибьютора',
                        'icon' => Icons::o('calculator'),
                    ],
                    [
                        'route' => ['/calc/calc/calculator'],
                        'label' => 'Калькулятор участника ТТ',
                        'icon' => Icons::o('calculator'),
                    ],
                    [
                        'route' => ['/calc/calculator-distrib/calculator'],
                        'label' => 'Калькулятор  участника Дистрибьютора',
                        'icon' => Icons::o('calculator'),
                    ],
				]
			],
		];
    }

}