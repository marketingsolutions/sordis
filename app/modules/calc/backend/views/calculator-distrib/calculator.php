<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\calc\backend\models\CalcSearch $searchModel
 * @var array $columns
 */

$this->title = 'Калькулятор Дистрибьютора';
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
$css = <<<CSS
.butt_del{
    display: none;
}
.pos_niz{
   margin-top: 20px;
}
CSS;
$this->registerCss($css);

$js = <<<JS
$( document ).ready(function() {
     /*Подсчет итоговой суммы по дистрам*/
    function calculateDistribTotalSum() {
        var toalDistribSum = 0;
       
              var basic_sum = $( "input[name*=sum_basic]" ).val();
              if(basic_sum == ""){
                  basic_sum=0;
              }
              toalDistribSum = toalDistribSum + parseInt(basic_sum);
        var sum_target=0;
         $( ".sum_target_distrib" ).each( function( index , elem) {
              sum_target = $( elem ).val();
              if(sum_target == ""){
                  sum_target=0;
              }
              //console.log(sum_target);
              toalDistribSum = toalDistribSum + parseInt(sum_target);
        });
       
        $('#calculateformdistrib-total_sum').val('');
        $('#calculateformdistrib-total_sum').val(toalDistribSum);
        
    }
    
    /*Рассчет основных баллов*/    
         $('body').on('click', '.butt_osn_ball',function(e){
             e.preventDefault();
             var sku = $("#calculateformdistrib-sku_sum").val();
             var sumTT = $("#calculateformdistrib-sum_tt").val();
             $("#reg-errors li").remove();
             if(!sku || !sumTT || sku == 0  || sumTT ==0){
                 $("#reg-errors").append("<li>" + 'Сумма отгруженных SKU и/или количество ТТ не должны быть пустыми или равным нулю' + "</li>");
             }else{ 
                 //Получение коэфициента покрытия
                 $.ajax(
                        {
                          type: 'POST',
                          url: '/calc/calculator-distrib/calculate-distrib',
                          data: {"sku": sku, "sumTT": sumTT},
                          success: function(data) {
                           // console.log(data);
                            $("#reg-errors li").remove();
                            $('#calculateformdistrib-sum_basic').text('');
                            $('#calculateformdistrib-sum_basic').val(data.basic);
                            calculateDistribTotalSum();
                        },
                        error:   function(xhr) {
                            var data = xhr.responseJSON;
                            if ("errors" in data) {
                                $('#result_bonus').text('0');
                                $("#reg-errors li").remove();
                                for (var i = 0; i < data.errors.length; i++) {
                                    $("#reg-errors").append("<li>" + data.errors[i] + "</li>");
                                }
                            }
                        }
                    });             
             }
         });
         
         //Подсчет суммы целевых баллов для Дистрибьюторов
         $('body').on('click', '.butt_target_ball', function(e){
             e.preventDefault();
             var litr = $(this).parent().parent().find('input[name*=v_litr]').val();
             var ball_litr = $(this).parent().parent().find('input[name*=ball_litr]').val();
             var sum_target =  $(this).parent().parent().find('input[name*=sum_target]');
             var sum_target_ball = litr*ball_litr;
             sum_target.val(sum_target_ball);
             calculateDistribTotalSum();
         });
                 
        /*Ограничение для добавления не более 5 полей для целевых баллов и поведение динамических столбцов*/
        var j=1;
        var ii=1;
        /*Добавление формы расчета целевых баллов*/
        $('body').on('click', '.butt_add',function(e){
            e.preventDefault();
            ii++;
            if(j<5){
               j++; 
            }
           
            if(ii <= 5){
                $(".add_div").clone().appendTo(".dynamic_add").removeClass('add_div').addClass('add_div'+ii);
                $('.add_div'+ii).children().children().find('input[name*=ball_litr]').val('');
                $('.add_div'+ii).children().children().find('input[name*=sum_target]').val('');
                $('.add_div'+ii).children().children().find('input[name*=v_litr]').val('');
            }           
             if(j==1){
                 $('.butt_del').hide();
             }else{
                 $('.butt_del').show();
             }     
        });
        
       /*Удаление формы расчета целевых баллов*/
        $('body').on('click', '.butt_del',function(e){
            e.preventDefault();
             ii--;
            $('.add_target').last().remove();
            if(j>1){
                 j--;
            }
           
            if(j==1){
                 $('.butt_del').hide();
            }else{
                $('.butt_del').show();
            }  
            calculateDistribTotalSum();
       });        
    
});
JS;
$this->registerJs($js);

?>
<?php $box = Box::begin(['cssClass' => 'calc-index box-primary']) ?>
<h4>Расчет основных баллов</h4>
<?php $form = ActiveForm::begin(['options' => ['id' => 'calc_distrib_form']]); ?>
<div class="row row_calc_basic">
    <div class="col-md-3">
        <?= $form->field($calcForm, 'sku_sum') ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($calcForm, 'sum_tt') ?>
    </div>
    <div class="col-md-3 button_distr"><br/>
        <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-primary btn_all butt_osn_ball']) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($calcForm, 'sum_basic') ?>
    </div>
</div>

<h4>Расчет целевых баллов</h4>
<div class="dynamic_add">
    <div class="row row_calc add_target add_div">
        <div class="col-md-3 pos_niz">
            <?= $form->field($calcForm, 'v_litr') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($calcForm, 'ball_litr') ?>
        </div>
        <div class="col-md-3 button_distr"><br/><br/>
            <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-primary btn_all butt_target_ball']) ?>
        </div>

        <div class="col-md-3 pos_niz">
            <?= $form->field($calcForm, 'sum_target')->textInput(['class' => 'form-control sum_target_distrib']) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <?= Html::submitButton('добавить', ['class' => 'btn btn_all butt_add']) ?>&nbsp;
        <?= Html::submitButton('удалить', ['class' => 'btn  btn_all butt_del']) ?>
    </div>
</div>
<ul style="color: red;" id="reg-errors"></ul>

<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <?= $form->field($calcForm, 'total_sum') ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php Box::end() ?>
