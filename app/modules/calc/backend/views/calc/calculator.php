<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;

use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\calc\backend\models\CalcSearch $searchModel
 * @var array $columns
 */

$this->title = 'Калькулятор участника ТТ';
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

$css=<<<CSS
.butt_del_basic, .butt_del_target{
    display: none;
}
.pos_niz{
   margin-top: 20px;
}
CSS;
$this->registerCss($css);


$js = <<<JS
$( document ).ready(function() {

 /*Подсчет итоговой суммы по ртт*/
    function calculateRttTotalSum() {
        var totalRttSum = 0;
        var basic_sum=0;
        $( '.basic_summ_rtt' ).each( function(index, elem) {
              basic_sum = $( elem ).val();
              if(basic_sum == ''){
                  basic_sum=0;
              }
              totalRttSum = totalRttSum + parseInt(basic_sum);
              console.log(basic_sum);
        });
        
        
        var target_sum_ball=0;
         $( ".target_sum_ball_rtt" ).each( function(index, elem) {
              target_sum_ball = $( elem ).val();
              if(target_sum_ball == ''){
                  target_sum_ball=0;
              }
              totalRttSum = totalRttSum + parseInt(target_sum_ball);
        });
        var sum_priz_ball = $('#calculateformrtt-sum_priz_ball').val();
        if(sum_priz_ball==''){
            sum_priz_ball=0;
        }
        totalRttSum = totalRttSum + parseInt(sum_priz_ball);
        $('#calculateformrtt-total_sum_ball').val('');
        $('#calculateformrtt-total_sum_ball').val(totalRttSum);
    }
    
       /*ОСНОВНЫЕ БАЛЛЫ*/
        /*Добавляем макисмум 10 полей для основных баллов*/
        var ii=1;
        var j=1;
        $('body').on('click', '.add_basic', function(e){
            e.preventDefault();
             ii++;
            if(j<10){
               j++; 
            }
           
            if(ii <= 10){
                $(".row_basic_calc").clone().appendTo(".add_basic_input").removeClass('row_basic_calc').addClass('row_basic_calc'+ii);
                $('.row_basic_calc'+ii).children().children().find('input[name*=basic_sku]').val('');
                $('.row_basic_calc'+ii).children().children().find('input[name*=basic_vol]').val('');
                $('.row_basic_calc'+ii).children().children().find('input[name*=basic_sum]').val('');
            }           
             if(j==1){
                 $('.butt_del_basic').hide();
             }else{
                 $('.butt_del_basic').show();
             }     
        });
        
        /*Удаление формы расчета основных баллов*/
        $('body').on('click', '.butt_del_basic',function(e){
            e.preventDefault();
             ii--;
            $('.add_basic_row').last().remove();
            if(j>1){
                 j--;
            }
            
            if(j==1){
                 $('.butt_del_basic').hide();
            }else{
                $('.butt_del_basic').show();
            }     
            calculateRttTotalSum();
       });
        
        /*ЦЕЛЕВЫЕ БАЛЛЫ*/
        /*Добавление максимум 5 формы для целевых баллов*/        
        var iii=1;
        var jj=1;
        $('body').on('click', '.add_target',function(e){
            e.preventDefault();
             iii++;
            if(j<5){
               jj++; 
            }
           
            if(iii <= 5){
                $(".row_target_calc").clone().appendTo(".add_target_input").removeClass('row_target_calc').addClass('row_target_calc'+iii);
                $('.row_target_calc'+iii).children().children().find('input[name*=target_vol]').val('');
                $('.row_target_calc'+iii).children().children().find('input[name*=target_count_ball]').val('');
                $('.row_target_calc'+iii).children().children().find('input[name*=target_sum_ball]').val('');
            }           
             if(jj==1){
                 $('.butt_del_target').hide();
             }else{
                 $('.butt_del_target').show();
             }     
        });
        
        /*Удаление формы расчета целевых баллов*/
        $('body').on('click', '.butt_del_target',function(e){
            e.preventDefault();
             iii--;
            $('.add_target_row').last().remove();
            if(jj>1){
                 jj--;
            }
            
            if(jj==1){
                 $('.butt_del_target').hide();
            }else{
                $('.butt_del_target').show();
            }
            calculateRttTotalSum();
       });    
        
        /*Расчет призовых баллов*/
        $('body').on('click', '.btn_calc_poll',function(e){
            e.preventDefault();
            $("#reg-errors li").remove();
            var action_ball = $(this).parent().parent().find('input[name*=action_ball]').val();
            var poll_ball = $(this).parent().parent().find('input[name*=poll_ball]').val();
            if(!action_ball || !poll_ball || action_ball==0 || poll_ball==0){
                $("#reg-errors").append("<li>" + 'Бонусы за опросы и/или призовые акции не должны быть пустыми или равны нулю' + "</li>");
            }else{
                var total_action_poll = parseInt(action_ball) + parseInt(poll_ball);
                $('#calculateformrtt-sum_priz_ball').val('');
                $('#calculateformrtt-sum_priz_ball').val(total_action_poll);
                calculateRttTotalSum();
            }   
        });
        
        /*Рассчет целевых баллов*/
        $('body').on('click', '.btn_calc_target', function(e){
             e.preventDefault();
             $("#reg-errors li").remove();
             var target_vol = $(this).parent().parent().find('input[name*=target_vol]').val();
             var target_count_ball = $(this).parent().parent().find('input[name*=target_count_ball]').val();
             var target_sum_ball = $(this).parent().parent().find('input[name*=target_sum_ball]');
             if(!target_vol || !target_count_ball || target_count_ball==0 || target_vol==0){
                  $("#reg-errors").append("<li>" + 'Количество баллов за литр и/или объем отгрузки не должны быть пустыми или равны нулю' + "</li>");
             }else{
                 var total_target_poll = parseInt(target_vol) * parseInt(target_count_ball);
                 target_sum_ball.val('');
                 target_sum_ball.val(total_target_poll);
                 calculateRttTotalSum();
             }
             
        });
       
        /*Рассчет  основных баллов*/
        $('body').on('click', '.btn_calc_basic',function(e){
            e.preventDefault();
            var basic_sku = $(this).parent().parent().find('input[name*=basic_sku]').val();
            var basic_vol = $(this).parent().parent().find('input[name*=basic_vol]').val();
            var result_input = $(this).parent().parent().find('input[name*=basic_sum]');
             
            if(!basic_sku || !basic_vol || basic_vol==0 || basic_sku == 0){
                $("#reg-errors li").remove();
                $("#reg-errors").append("<li>" + 'SKU и/или объем отгрузки не должны быть пустыми или равны нулю' + "</li>");
            }else{
                $.ajax(
                    {
                      type: 'POST',
                      url: '/calc/calc/calculate-rtt',
                      data: {"sku": basic_sku, "vol": basic_vol},
                      success: function(data) {
                        //console.log(data.bonus);
                        $("#reg-errors li").remove();
                        result_input.val(data.bonus);
                        calculateRttTotalSum();
                       
                    },
                    error:   function(xhr) {
                        var data = xhr.responseJSON;
                        if ("errors" in data) {
                            $('#result_bonus').text('0');
                            $("#reg-errors li").remove();
                            for (var i = 0; i < data.errors.length; i++) {
                                $("#reg-errors").append("<li>" + data.errors[i] + "</li>");
                            }
                        }
                    }
                });
            }
        });
});


JS;
$this->registerJs($js);

?>
<?php $box = Box::begin(['cssClass' => 'calc-index box-primary']) ?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'calc_rtt_form']]); ?>
<!--Основные баллы-->
<h4>Расчет основных баллов для Торговой точки </h4>
<div class="add_basic_input calc-table">
    <div class="row add_basic_row row_basic_calc calc-table--line">
        <div class="col-md-3 calc-table--col ctc1">
            <?= $form->field($calcForm, 'basic_sku') ?>
        </div>
        <div class="col-md-3 calc-table--col ctc2">
            <?= $form->field($calcForm, 'basic_vol') ?>
        </div>

        <div class="col-md-3 calc-table--col ctc3">
            <?= $form->field($calcForm, 'basic_sum')->textInput(['class' => 'form-control basic_summ_rtt']) ?>
        </div>
        <div class="col-md-3 calc-table--btn"><br/>
            <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-primary btn_all btn_calc_basic']) ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-2 button-row">

        <?= Html::submitButton('добавить', ['class' => 'btn btn_all add_basic']) ?>&nbsp;
        <?= Html::submitButton('удалить', ['class' => 'btn btn_blue butt_del_basic']) ?>

    </div>
</div>




<h4>Расчет целевых баллов</h4>
<div class="add_target_input">
    <div class="row add_target_row row_target_calc add_target_row calc-table">
        <div class="calc-table--line">
            <div class="col-md-3 calc-table--col ctc1 pos_niz">
                <?= $form->field($calcForm, 'target_vol') ?>
            </div>
            <div class="col-md-3 calc-table--col ctc2">
                <?= $form->field($calcForm, 'target_count_ball') ?>
            </div>
            <div class="col-md-3 calc-table--col ctc3 pos_niz">
                <?= $form->field($calcForm, 'target_sum_ball')->textInput(['class' => 'form-control target_sum_ball_rtt']) ?>
            </div>
        </div>
        <div class="col-md-3 calc-table--btn"><br/><br/>
            <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-primary btn_all btn_calc_target']) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 button-row">
        <?= Html::submitButton('добавить', ['class' => 'btn btn_all add_target']) ?>&nbsp;
        <?= Html::submitButton('удалить', ['class' => 'btn btn_blue butt_del_target']) ?>
    </div>
</div>

<h4>Расчет призовых баллов</h4>
<div class="calc-table">
    <div class="row calc-table--line">
        <div class="col-md-3 calc-table--col ctc1">

            <?= $form->field($calcForm, 'poll_ball') ?>
        </div>
        <div class="col-md-3 calc-table--col ctc2">
            <?= $form->field($calcForm, 'action_ball') ?>
        </div>

        <div class="col-md-3 calc-table--col ctc3">
            <?= $form->field($calcForm, 'sum_priz_ball') ?>
        </div>
        <div class="col-md-3 calc-table--btn"><br/>
            <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-primary btn_all btn_calc_poll']) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-2 calc-table--btn">

    </div>
</div>

<ul style="color: red;" id="reg-errors"></ul>

<div class="total-summ">
    <?= $form->field($calcForm, 'total_sum_ball') ?>
</div>


<?php ActiveForm::end(); ?>


<?php Box::end() ?>
