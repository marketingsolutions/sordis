<?php

use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;
use modules\calc\common\models\Calc;
use ms\files\attachments\common\utils\FileUtil;

/**
 * @var yii\web\View $this
 * @var modules\calc\common\models\Calc $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php  $box = FormBox::begin(['cssClass' => 'calc-form box-primary', 'title' => '']) ?>
    <?php $form = ActiveForm::begin(); ?>

    <?php $box->beginBody() ?>
    <?= $form->field($model, 'sku_min')->textInput() ?>

    <?= $form->field($model, 'sku_max')->textInput() ?>

    <?= $form->field($model, 'coefficient')->textInput() ?>


<?= $form->field($model, 'photo')->fileInput() ?>
<?= \ms\files\attachments\common\utils\FileUtil::render(['url' => $model->photoUrl]) ?>

    <?php $box->endBody() ?>

    <?php $box->actions([
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
    ]) ?>
    <?php ActiveForm::end(); ?>

<?php  FormBox::end() ?>
