<?php

namespace modules\calc\backend\controllers;

use modules\calc\frontend\forms\CalculateFormDistrib;
use Yii;
use modules\calc\common\models\CalculatorDistrib;
use modules\calc\backend\models\CalculatorDistribSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * CalculatorDistribController implements the CRUD actions for CalculatorDistrib model.
 */
class CalculatorDistribController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var CalculatorDistribSearch $searchModel */
                    return Yii::createObject(CalculatorDistribSearch::className());
                },
                'dataProvider' => function($params, CalculatorDistribSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all CalculatorDistrib models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var CalculatorDistribSearch $searchModel */
        $searchModel = Yii::createObject(CalculatorDistribSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function actionCalculator()
    {
        $calcForm = new CalculateFormDistrib();
        return $this->render('calculator', [
            'calcForm' => $calcForm,
        ]);
    }

    /*Расчет бонусов для Дистрибьюторов через Ajax*/
    public function actionCalculateDistrib(){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $sku = \Yii::$app->request->post('sku');  //Количество SKU
        $sumTT = \Yii::$app->request->post('sumTT'); //Количество ТТ
        $akb4month = 10; //АКБ за 4 мес (Далее будет приходить из XML)
        if (!$sku || !$sumTT){
            return [
                'result' => 'FAIL',
                'msg' => 'Суммаотгруженных SKU или количество ТТ не может пустым или быть равно 0',
            ];
        }
        return ['result' => 'OK', 'basic' => CalculatorDistrib::process($sku, $sumTT, $akb4month)];


    }

    public function getGridColumns(CalculatorDistribSearch $searchModel)
    {
        return [
			//'id',
			'akbs',
			'akbs_max',
			'koeff_akbs',
        ];
    }

    /**
     * Creates a new CalculatorDistrib model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CalculatorDistrib;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CalculatorDistrib model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
        ]);
	}


    /**
     * Deletes an existing CalculatorDistrib model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the CalculatorDistrib model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CalculatorDistrib the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CalculatorDistrib::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
