<?php

namespace modules\calc\backend\controllers;

use ms\files\attachments\common\utils\FileUtil;
use Yii;
use modules\calc\common\models\Calc;
use modules\calc\backend\models\CalcSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;
use modules\calc\frontend\forms\CalculateFormRtt;

/**
 * CalcController implements the CRUD actions for Calc model.
 */
class CalcController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var CalcSearch $searchModel */
                    return Yii::createObject(CalcSearch::className());
                },
                'dataProvider' => function($params, CalcSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all Calc models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var CalcSearch $searchModel */
        $searchModel = Yii::createObject(CalcSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    /**
     * Расчет бонусов для РТТ-калькулятора через Ajax
     * @return array
     */
    public function actionCalculateRtt(){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $sku = \Yii::$app->request->post('sku');
        $volume = \Yii::$app->request->post('vol');
        if( $sku && $volume){
            $koeff = Calc::getRttCoefficient($sku);
            if(!$koeff){
                return $this->error('Неправильное значение SKU');
            }
            $bonus = $volume*$koeff;
            return ['result' => 'OK', 'bonus' => $bonus];
        }
        return [
            'result' => 'FAIL',
            'msg' => 'Ошибка в переданных данных!',
        ];
    }

    public function actionCalculator()
    {
        $calcForm = new CalculateFormRtt();
        return $this->render('calculator', [
            'calcForm' => $calcForm,
        ]);
    }

    public function getGridColumns(CalcSearch $searchModel)
    {
        return [
			'sku_min',
			'sku_max',
            'coefficient',
            [
                'attribute' => 'photo',
                'label' => 'Фото монетки',
                'format' =>'html',
                'value' => function($model){
                    return FileUtil::render(['url' => $model->photoUrl]);
                }
            ],
        ];
    }

    /**
     * Creates a new Calc model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Calc;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Calc model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
        ]);
	}


    /**
     * Deletes an existing Calc model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Calc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Calc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Calc::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
