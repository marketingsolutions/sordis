<?php

use yii\db\Migration;

/**
 * Class m190115_082543_calc_add_calc_table
 */
class m190115_082543_calc_add_calc_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%calculator}}', [
            'id' => $this->primaryKey(),
            'sku_min' => $this->integer(),
            'sku_max' => $this->integer(),
            'coefficient' => $this->double(),
            'group' => $this->integer(),

        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%calculator}}');
    }

}
