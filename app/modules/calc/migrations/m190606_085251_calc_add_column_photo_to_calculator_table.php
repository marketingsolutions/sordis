<?php

use yii\db\Migration;

/**
 * Class m190606_085251_calc_add_column_photo_to_calculator_table
 */
class m190606_085251_calc_add_column_photo_to_calculator_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%calculator}}', 'photo', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%calculator}}', 'photo');
    }
}
