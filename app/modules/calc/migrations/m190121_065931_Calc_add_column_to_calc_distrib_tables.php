<?php

use yii\db\Migration;

/**
 * Class m190121_065931_Calc_add_column_to_calc_distrib_tables
 */
class m190121_065931_Calc_add_column_to_calc_distrib_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%calculator_distrib}}', 'akbs_max' , $this->double());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%calculator_distrib}}', 'akbs_max');
    }
}
