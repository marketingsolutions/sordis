<?php

use yii\db\Migration;

/**
 * Class m190118_101819_Calc_add_table_calculation_distrib
 */
class m190118_101819_Calc_add_table_calculation_distrib extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%calculator_distrib}}', [
            'id' => $this->primaryKey(),
            'akbs' => $this->double(),
            'koeff_akbs' => $this->double(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%calculator_distrib}}');
    }
}
