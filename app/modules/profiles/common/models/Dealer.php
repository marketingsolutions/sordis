<?php

namespace modules\profiles\common\models;

use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_dealers".
 *
 * @property integer $id
 * @property string $name
 * @property integer $region_id
 * @property integer $action_id
 * @property integer $leader_id
 *
 * @property Profile[] $profiles
 * @property Region $region
 * @property Action $action
 * @property Profile $leader
 */
class Dealer extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dealers}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Дистрибьютор';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Дистрибьюторы';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string', 'max' => 255],
            ['name', 'required'],
            ['region_id', 'integer'],
            ['region_id', 'required'],
            ['action_id', 'integer'],
            ['leader_id', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Дистрибьютор',
            'region_id' => 'Регион',
            'action_id' => 'Акция',
            'leader_id' => 'Руководитель',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(Profile::className(), ['dealer_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::className(), ['id' => 'action_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeader()
    {
        return $this->hasOne(Profile::className(), ['id' => 'leader_id']);
    }

    public static function getOptions()
    {
        return self::find()->indexBy('id')->select('name, id')->orderBy(['name' => SORT_ASC])->column();
    }

    public static function getNameOptions()
    {
        return self::find()->indexBy('name')->select('name')->orderBy(['name' => SORT_ASC])->column();
    }
}
