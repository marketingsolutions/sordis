<?php

namespace modules\profiles\common\models;

use marketingsolutions\datetime\DateTimeBehavior;
use marketingsolutions\finance\models\Purse;
use modules\sales\common\models\Product;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_actions".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property boolean $enabled
 * @property string $loyalty_1c_name
 * @property string $created_at
 * @property string $updated_at
 * @property string $date_start
 * @property string $date_end
 *
 * @property string $date_start_formatted
 * @property string $date_end_formatted
 * @property Product[] $products
 */
class Action extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%actions}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Акция';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Акции';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            'datetime' => [
                'class' => DateTimeBehavior::class,
                'attributes' => [
                    'date_start' => [
                        'originalFormat' => ['date', 'yyyy-MM-dd'],
                        'targetFormat' => ['date', 'dd.MM.yyyy'],
                    ],
                    'date_end' => [
                        'originalFormat' => ['date', 'yyyy-MM-dd'],
                        'targetFormat' => ['date', 'dd.MM.yyyy'],
                    ],
                ]
            ],
            [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => ['product_ids' => 'products'],
            ],
        ];
    }

    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
            ->viaTable(ActionProduct::tableName(), ['action_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string', 'max' => 255],
            ['name', 'required'],
            ['loyalty_1c_name', 'string', 'max' => 255],
            ['description', 'string'],
            ['enabled', 'integer'],
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
            [['date_start', 'date_end'], 'string'],
            [['date_start_local', 'date_end_local'], 'match', 'pattern' => '/^\d\d\.\d\d\.\d\d\d\d$/', 'message' => 'Дата должна быть в формате дд.мм.гггг'],
            [['product_ids'], 'each', 'rule' => ['integer']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'loyalty_1c_name' => '1С-идентификатор',
            'description' => 'Описание',
            'enabled' => 'Активна',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'date_start' => 'Начало акции',
            'date_end' => 'Окончание акции',
            'date_start_local' => 'Начало акции',
            'date_end_local' => 'Окончание акции',
            'date_start_formatted' => 'Начало акции',
            'date_end_formatted' => 'Окончание акции',
            'product_ids' => 'Продукция, участвующая в акции',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $this->refresh();
            $query = Profile::find();
            Action::getCompanyPurse($this);
            foreach ($query->each() as $profile) {
                /** @var Profile $profile */
                Action::getProfilePurse($this, $profile);
            }
        }

        if ($this->date_start == '1970-01-01') {
            $this->updateAttributes(['date_start' => null]);
        }
        if ($this->date_end == '1970-01-01') {
            $this->updateAttributes(['date_end' => null]);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param Action $action
     * @param Profile $profile
     * @return Purse
     */
    public static function getProfilePurse(Action $action, Profile $profile)
    {
        /** @var Purse $purse */
        $purse = Purse::findOne(['owner_type' => 'profile_action#' . $action->id, 'owner_id' => $profile->id]);

        if ($purse === null) {
            $purse = new Purse();
            $purse->owner_type = 'profile_action#' . $action->id;
            $purse->owner_id = $profile->id;
            $purse->balance = 0;
            $purse->title = "Счет пользователя #{$profile->id} ({$profile->full_name}) по акции #{$action->id} ({$action->name})";
            $purse->save(false);
        }

        return $purse;
    }

    /**
     * @param Action $action
     * @return Purse
     */
    public static function getCompanyPurse(Action $action)
    {
        /** @var Purse $purse */
        $purse = Purse::find()->where([
            'owner_type' => Action::className(),
            'owner_id' => $action->id
        ])->one();

        if ($purse === null) {
            $purse = new Purse();
            $purse->owner_type = Action::className();
            $purse->owner_id = $action->id;
            $purse->balance = 0;
            $purse->title = "Счет акции '{$action->name}' #{$action->id}";
            $purse->save(false);
        }

        return $purse;
    }

    public static function getOptions()
    {
        return self::find()->indexBy('id')->select('name, id')->orderBy(['name' => SORT_ASC])->column();
    }

    public static function getPurseOptions()
    {
        $result = [];
        /** @var Action[] $actions */
        $actions = Action::find()->orderBy(['id' => SORT_ASC])->all();
        foreach ($actions as $action) {
            $purse = Action::getCompanyPurse($action);
            $key = $purse->id . '';
            $result[$key] = $action->name;
        }

        return $result;
    }

    public function getDate_start_formatted()
    {
        return empty($this->date_start) ? null : (new \DateTime($this->date_start))->format('d.m.Y');
    }

    public function getDate_end_formatted()
    {
        return empty($this->date_end) ? null : (new \DateTime($this->date_end))->format('d.m.Y');
    }
}
