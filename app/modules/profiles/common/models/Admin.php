<?php

namespace modules\profiles\common\models;

use Yii;
use yz\admin\models\User;

/**
 * @property Region[] $regions
 */
class Admin extends User
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegions()
    {
        return $this->hasMany(Region::className(), ['admin_user_id' => 'id']);
    }

    public static function getOptions()
    {
        return self::find()->indexBy('id')->select('name, id')->orderBy(['name' => SORT_ASC])->column();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['name'] = 'Региональный менеджер';

        return $labels;
    }
}
