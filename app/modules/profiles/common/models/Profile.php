<?php

namespace modules\profiles\common\models;

use modules\sales\common\models\Distributor;
use modules\sales\common\models\DistributorProfile;
use modules\sales\common\models\SalePointProfile;
use ms\loyalty\contracts\identities\IdentityInterface;
use ms\loyalty\contracts\identities\IdentityRegistrarInterface;
use ms\loyalty\contracts\identities\IdentityRoleInterface;
use ms\loyalty\contracts\prizes\PrizeRecipientInterface;
use ms\loyalty\contracts\profiles\HasEmail;
use ms\loyalty\contracts\profiles\HasEmailInterface;
use ms\loyalty\contracts\profiles\HasPhoneMobile;
use ms\loyalty\contracts\profiles\HasPhoneMobileInterface;
use ms\loyalty\contracts\profiles\ProfileInterface;
use ms\loyalty\contracts\profiles\UserName;
use ms\loyalty\contracts\profiles\UserNameInterface;
use ms\loyalty\feedback\common\models\Message;
use ms\loyalty\identity\phonesEmails\common\models\Identity;
use marketingsolutions\finance\models\Purse;
use marketingsolutions\finance\models\PurseInterface;
use marketingsolutions\finance\models\PurseOwnerInterface;
use marketingsolutions\finance\models\PurseOwnerTrait;
use marketingsolutions\phonenumbers\PhoneNumberBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yz\admin\models\AuthAssignment;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_profiles".
 *
 * @property integer $id
 * @property integer $no_send_email
 * @property integer $is_xml_upload
 * @property integer $identity_id
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $inn
 * @property string $full_name
 * @property string $phone_mobile
 * @property string $email
 * @property string $created_at
 * @property string $updated_at
 * @property string $blocked_at
 * @property integer $is_allow_personal
 * @property string $distrib_name
 * @property string $rtt_name
 * @property string $blocking_reason Reason of the user blocking
 * @property string $external_id ID of this profile in the external system
 * @property integer $dealer_id
 * @property integer $fresh_news Quantity of unreaded news
 * @property string $avatar
 * @property boolean push_news
 * @property boolean push_sales
 * @property string $role
 * @property string $role_label
 * @property string $is_xml_upload_at
 *
 * @property IdentityInterface $identity
 * @property string $phone_mobile_local
 * @property Dealer $dealer
 */
class Profile extends \yz\db\ActiveRecord implements ModelInfoInterface,
                                                     HasEmailInterface, HasPhoneMobileInterface, UserNameInterface, IdentityRoleInterface,
                                                     PurseOwnerInterface, PrizeRecipientInterface, ProfileInterface
{
    use HasEmail, HasPhoneMobile, UserName, PurseOwnerTrait;

    const SCENARIO_FRONTEND = 'scenario_frontend';
    const ROLE_MANAGER_TT = 'manager_tt';
    const ROLE_DISTRIBUTOR = 'distributor';
    const ROLE_MULTI_MANAGER_TT = 'multi_manager_tt';  //В системе эта роль  одно и тоже с ROLE_MANAGER_TT, нужна для подгрузки форм при регистрации

    public $avatar_local;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profiles}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Профиль участника';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        if(\Yii::$app->controller->id == 'profiles-distrib'){
            return 'Профили дистрибьюторов';
        }else{
            return 'Профили участников РТТ';
        }
    }

    /**
     * Returns purse's owner by owner's id
     *
     * @param int $id
     * @return $this
     */
    public static function findPurseOwnerById($id)
    {
        return static::findOne($id);
    }

    protected static function purseOwnerType()
    {
        return self::class;
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            'phonenumber' => [
                'class' => PhoneNumberBehavior::className(),
                'attributes' => [
                    'phone_mobile_local' => 'phone_mobile',
                ],
                'defaultRegion' => 'RU',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'distrib_name', 'rtt_name', 'is_allow_personal', 'is_xml_upload', 'is_xml_upload_at'], 'safe'],
            [['first_name', 'last_name', 'middle_name'], 'string', 'max' => 32],
            [['full_name'], 'string', 'max' => 255],
            [['phone_mobile'], 'string', 'max' => 16],
            [['phone_mobile'], 'unique'],
            [['email'], 'string', 'max' => 64],
            ['phone_mobile_local', 'required'],
            ['phone_mobile_local', 'unique', 'targetAttribute' => ['phone_mobile' => 'phone_mobile']],
            ['email', 'email'],
            ['dealer_id', 'integer'],
            ['no_send_email', 'integer'],
            ['fresh_news', 'integer'],
            ['avatar', 'string'],
            [['avatar_local'], 'file', 'extensions' => ['png', 'jpg', 'jpeg', 'gif', 'ico'], 'maxSize' => 1024 * 1024 * 15],
            [['push_news', 'push_sales'], 'safe'],
            ['role', 'string'],
        ];
    }

    public static function getTestIds()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'middle_name' => 'Отчество',
            'full_name' => 'Полное имя',
            'phone_mobile' => 'Номер телефона',
            'phone_mobile_local' => 'Номер телефона',
            'email' => 'Email',
            'created_at' => 'Дата добавления',
            'updated_at' => 'Дата изменения',
            'blocked_at' => 'Дата блокировки',
            'blocking_reason' => 'Причина блокировки',
            'dealer_id' => 'Дилер',
            'fresh_news' => 'Свежих новостей',
            'avatar' => 'Аватар',
            'avatar_local' => 'Аватар',
            'push_news' => 'Включены пуш-уведомления по новостям',
            'push_sales' => 'Включены пуш-уведомления по продажам',
            'role' => 'Ваша роль',
            'role_label' => 'Должность',
            'distrib_name' => 'Дистрибьютор',
            'no_send_email' => 'Получать рассылки с портала по E-mail',
            'rtt_name' => 'Торговые точки',
            'is_allow_personal' => 'Разрешено или нет менять персональные даные профиля',
            'is_xml_upload' => 'Данные об участнике отосланы в Сордис',
            'is_xml_upload_at' => 'Дата отправки файла в Сордис',
        ];
    }

    public function beforeSave($insert)
    {
        $this->full_name = $this->first_name . ' ' . $this->last_name;
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDealer()
    {
        return $this->hasOne(Dealer::className(), ['id' => 'dealer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdentity()
    {
        return $this->hasOne(Identity::className(), ['id' => 'identity_id']);
    }

    /**
     * @return string
     */
    public function getIdentityRole()
    {
        return 'profile';
    }

    /**
     * Returns id of the recipient
     *
     * @return integer
     */
    public function getRecipientId()
    {
        return $this->getPrimaryKey();
    }

    public function getUserFullName()
    {
        return $this->full_name;
    }

    /**
     * Returns purce for the recipient, that should contain enough money
     *
     * @return PurseInterface
     */
    public function getRecipientPurse()
    {
        return $this->purse;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $this->createPurse();
        }

        if (!$insert && $this->isAttributeChanged('full_name')) {
            $this->updatePurse();
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function getAvatarUrl()
    {
        return empty($this->avatar)
            ? Yii::getAlias('@frontendWeb/images/blank_avatar.png')
            : Yii::getAlias('@frontendWeb/data/photo/' . $this->avatar);
    }

    public function createPurse()
    {
        Purse::create(self::class, $this->id, strtr('Счет пользователя #{id} ({name})', [
            '{id}' => $this->id,
            '{name}' => $this->full_name,
        ]));
    }

    protected function updatePurse()
    {
        $this->purse->updateAttributes([
            'title' => strtr('Счет пользователя #{id} ({name})', [
                '{id}' => $this->id,
                '{name}' => $this->full_name,
            ]),
        ]);
    }

    public function getDiscount()
    {
        return $this->hasOne(Discount::className(), ['profile_id' => 'id']);
    }

    public function getDiscountSum()
    {
        return isset($this->discount->remainder_sum) ? $this->getDistribDiscount($this->id) : 0;
    }

    public function getPremium()
    {
        return $this->hasOne(Premium::className(), ['profile_id' => 'id']);
    }

    public function getPremiumSum()
    {
        return isset($this->premium->remainder_sum) ? $this->getDistribPremium($this->id) : 0;
    }

    public function getDistribPremium($profile_id)
    {
        $model = Premium::find()
            ->where(['profile_id' => $profile_id])
            ->sum('remainder_sum');
        if($model){
            return $model;
        }
        return 0;
    }

    public function getDistribDiscount($profile_id)
    {
        $model = Discount::find()
            ->where(['profile_id' => $profile_id])
            ->sum('remainder_sum');
        if($model){
            return $model;
        }
        return 0;
    }

    public function afterDelete()
    {
        $this->removeIdentity();
        Purse::remove(self::class, $this->id);

        parent::afterDelete();
    }

    private function removeIdentity()
    {
        /** @var IdentityRegistrarInterface $registrar */
        $registrar = Yii::$container->get(IdentityRegistrarInterface::class);
        $registrar->removeIdentity($this->identity_id);
    }

    /**
     * @return integer
     */
    public function getProfileId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Block profile from be able to login
     *
     * @param string $reason
     */
    public function block($reason = '')
    {
        $this->updateAttributes([
            'blocked_at' => new Expression('NOW()'),
            'blocking_reason' => $reason,
        ]);
    }

    /**
     * Unblock profile from be able to login
     */
    public function unblock()
    {
        $this->updateAttributes([
            'blocked_at' => null,
            'blocking_reason' => null
        ]);
    }


    public function upload(array $fields)
    {
        foreach ($fields as $field) {
            $file = UploadedFile::getInstance($this, $field . '_local');

            if ($file instanceof UploadedFile) {
                $dir = Yii::getAlias("@data/photo");
                FileHelper::createDirectory($dir);
                $name = "{$this->id}_$field.{$file->extension}";
                $path = Yii::getAlias($dir . DIRECTORY_SEPARATOR . $name);
                $file->saveAs($path);
                $this->$field = $name;
                $this->updateAttributes([$field]);
            }
        }
    }

    /**
     * @return array
     */
    public static function getRoles()
    {
        return [
            self::ROLE_MANAGER_TT => 'Я управляю одним магазином',
            self::ROLE_MULTI_MANAGER_TT => 'Я управляю двумя и более магазинами',
            self::ROLE_DISTRIBUTOR => 'Я - Дистрибьютор',
        ];
    }

    /**
     * @return mixed|null
     */
    public function getRole_label()
    {
        $roles = self::getRoles();

        return $this->role && isset($roles[$this->role]) ? $roles[$this->role] : null;
    }

    /**
     * @return array
     */
    public static function getDashboardRealRole(){
        return [
            self::ROLE_MANAGER_TT => 'Участник - владелец РТТ',
            self::ROLE_DISTRIBUTOR => 'Участник Дистрибьютор',
        ];
    }

    /**
     * @param $profileId
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getMySalePoint($profileId){
        return SalePointProfile::find()
            ->select('{{%sale_point_profile}}.id as id_sale_point, {{%sale_point}}.name as name, {{%sale_point}}.address as address, {{%sale_point}}.license_number as license_number, {{%sale_point}}.code_egais as code_egais, {{%sale_point}}.inn as inn, {{%sale_point}}.kpp as kpp,{{%sale_point}}.work_time_from as work_time_from , {{%sale_point}}.work_time_to as work_time_to,')
            ->leftJoin('{{%sale_point}}', '{{%sale_point}}.id={{%sale_point_profile}}.sale_point_id')
            ->where(['{{%sale_point_profile}}.profile_id' => $profileId])
            ->asArray()
            ->all();
    }

    /**
     * Проверка и обновление статуса в РТТ на согласование, если админ пользуется автокомплитом
     * @param $code_egais
     * @param $profileId
     */
    public static function checkRttFromConfirm($code_egais, $profileId){
        $model = ProfileConfirmRtt::findOne(['profile_id' => $profileId, 'code_egais' => $code_egais]);
        if($model){
            $model->status = ProfileConfirmRtt::STATUS_CONFIRM;
            $model->save(false);
        }
    }


    /**
     * Выделение непрочитанных сообщений
     * @param $adminId
     * @return array
     */
    public static function adminReadMessages($adminId){
        $arrMessagesIsAdminView = [];
        $model = Message::find()->asArray()->all();
        foreach ($model as $message){
            if(!$message['admin_is_read']){
                $arrMessagesIsAdminView[] = $message['id'];
            }elseif($message['admin_is_read']){
                $arrAdmin = unserialize($message['admin_is_read']);
                if(!in_array($adminId, $arrAdmin)){
                    $arrMessagesIsAdminView[] = $message['id'];
                }
            }
        }
        return implode( ",", $arrMessagesIsAdminView);
    }

    public static function adminReadMessagesMenu($adminId){
        $arrMessagesIsAdminView = [];
        $model = Message::find()->asArray()->all();
        foreach ($model as $message){
            if(!$message['admin_is_read']){
                $arrMessagesIsAdminView[] = $message['id'];
            }elseif($message['admin_is_read']){
                $arrAdmin = unserialize($message['admin_is_read']);
                if(!in_array($adminId, $arrAdmin)){
                    $arrMessagesIsAdminView[] = $message['id'];
                }
            }
        }
        return  count($arrMessagesIsAdminView);
    }

    public static function getNotProcessingMessage()
    {
      return Message::find()->where(['is_processed' => false])->count();
    }

    /**
     * Добавление ID админов в массив админов, просмотревших сообщение
     */
    public static function isReadMessage($messageId, $adminId){
        $model = Message::findOne(['id' => $messageId]);
        if($model && $model->admin_is_read) {
            $arrIsRead = unserialize($model->admin_is_read);
            if (is_array($arrIsRead) && !in_array($adminId, $arrIsRead)) {
                $arrAdmin[] = $adminId;
                $add = array_merge($arrIsRead, $arrAdmin);
                $model->admin_is_read = serialize($add);
                $model->update(false);
            }
        }else{
            $add=[];
            $add[]=$adminId;
            $model->admin_is_read = serialize($add);
            $model->update(false);
        }
    }

    public static function getCountUnread($adminId)
    {
        $messages = Message::find()->select('admin_is_read')->asArray()->all();
        $i=0;
        if(!empty($messages)) {
            foreach ($messages as $message) {
                $arrIsRead = unserialize($message['admin_is_read']);
                if($arrIsRead) {
                    if (!in_array($adminId, $arrIsRead)) {
                        $i++;
                    }
                }else{
                    $i++;
                }
            }
        }
        return $i;
    }

    /**
     * @param $profile_id
     * @return bool
     */
    public static function isAllow($profile_id)
    {
        $profile = Profile::findOne($profile_id);
        if($profile->is_allow_personal){
            return true;
        }
        return false;
    }

    public static function getProdileData($profile_id)
    {
        $model = Profile::findOne(['id' => $profile_id]);
        if(!$model){
            return false;
        }
        return $model;
    }

    /**
     * Проверка на менеджера ТО
     * @return bool
     */
    public static function isManagerTo()
    {
        $adminUser = AuthAssignment::findOne(['user_id' => Yii::$app->user->id, 'item_name' => 'MANAGER_TO']);
        if($adminUser){
            return true;
        }
        return false;
    }


    /**
     * Функция для редиректа под ролью Менеджер ТО
     * @return bool
     */
    public static function redirectManagerTO()
    {
        $currentUrl = Yii::$app->request->pathInfo;
        $restricted = [
            'profiles/profile/index',
            'profiles/profile/change-pass',
            'dashboard/prizes',
            'catalog/catalog/index',
            'social/social/add',
            'polls/poll/participate',
        ];
        if (in_array($currentUrl, $restricted)) {
            return true;
        }
        return false;
    }
}
