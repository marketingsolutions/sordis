<?php

namespace modules\profiles\common\models;

use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_discount".
 *
 * @property integer $id
 * @property integer $bonus_sum
 * @property integer $discount_sum
 * @property integer $profile_id
 * @property integer $remainder_sum
 * @property string $created_at
 */
class Discount extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%discount}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Discount';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Скидки';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['bonus_sum', 'integer'],
            ['profile_id', 'integer'],
            ['discount_sum', 'integer'],
            ['remainder_sum', 'integer'],
            ['created_at', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bonus_sum' => 'Сумма потраченных баллов',
            'discount_sum' => 'Сумма скидки',
            'remainder_sum' => 'Остаток',
            'created_at' => 'Created At',
        ];
    }
}
