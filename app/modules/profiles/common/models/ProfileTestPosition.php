<?php

namespace modules\profiles\common\models;

use modules\courses\common\models\CourseTest;
use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_profile_test_positions".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property integer $test_id
 * @property integer $position
 *
 * @property CourseTest $test
 * @property Profile $profile
 */
class ProfileTestPosition extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile_test_positions}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Profile Test Position';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Profile Test Positions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['profile_id', 'integer'],
            ['test_id', 'integer'],
            ['position', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Profile ID',
            'test_id' => 'Test ID',
            'position' => 'Position',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(CourseTest::className(), ['id' => 'test_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }

	public static function updatePosition($profile_id, $test_id, $position)
	{
		$testPosition = ProfileTestPosition::findOne(['profile_id' => $profile_id, 'test_id' => $test_id]);

		if (null == $testPosition) {
			$testPosition = new ProfileTestPosition();
			$testPosition->profile_id = $profile_id;
			$testPosition->test_id = $test_id;
			$testPosition->position = 1;
			$testPosition->save();
			$testPosition->refresh();

			return 1;
		}

		if ($position) {
			$testPosition->updateAttributes(['position' => $position]);
		}

		return $testPosition->position;
	}
}
