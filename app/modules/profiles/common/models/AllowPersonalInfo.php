<?php

namespace modules\profiles\common\models;

use Yii;
use yz\interfaces\ModelInfoInterface;
use marketingsolutions\phonenumbers\PhoneNumberBehavior;

/**
 * This is the model class for table "yz_allow_personal_info".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property string $user_message
 * @property string $admin_message
 * @property string $admin_allow_message
 * @property string $created_at
 * @property string $updated_at
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $email
 * @property string $phone_mobile
 */
class AllowPersonalInfo extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    public $isAllow;
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'phonenumber' => [
                'class' => PhoneNumberBehavior::className(),
                'attributes' => [
                    'phone_mobile_local' => 'phone_mobile',
                ],
                'defaultRegion' => 'RU',
            ],
        ];
    }
    public static function tableName()
    {
        return '{{%allow_personal_info}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Allow Personal Info';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Заявки на редактирование профиля';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['profile_id', 'integer'],
            ['user_message', 'string'],
            ['admin_message', 'string'],
            ['admin_allow_message', 'string'],
            ['first_name', 'string'],
            ['phone_mobile_local', 'required'],
            //['phone_mobile_local', 'unique', 'targetAttribute' => ['phone_mobile' => 'phone_mobile']],
            ['last_name', 'string'],
            ['middle_name', 'string'],
            ['email', 'string'],
            ['phone_mobile', 'string'],
            [['created_at', 'updated_at', 'isAllow'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Участник',
            'user_message' => 'Сообщение участника',
            'admin_message' => 'Старые данные участника',
            'admin_allow_message' => 'Ответ модератора',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата подтверждения администратором',
            'isAllow' => 'Разрешить редактировать профиль',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'middle_name' => 'Отчество',
            'email' => 'E-mail',
            'phone_mobile' => 'Телефон',
        ];
    }

    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' =>'profile_id']);
    }

    public function getProfileFullName()
    {
        return $this->profile->full_name;
    }

    public function getProfilePhone()
    {
        return $this->profile->phone_mobile;
    }

    public function getProfileRole()
    {
        return $this->profile->role;
    }

    public function getIsProfileAllow()
    {
        return $this->profile->is_allow_personal;
    }

    public function process()
    {
        $profile = Profile::findOne($this->profile_id);
        $oldPersonalInformation = "<b>Имя: </b>".$profile->first_name."<br/>";
        $oldPersonalInformation .= "<b>Фамилия: </b>".$profile->last_name."<br/>";
        $oldPersonalInformation .= "<b>Отчество: </b>".$profile->middle_name."<br/>";
        $oldPersonalInformation .= "<b>Телефон: </b>".$profile->phone_mobile."<br/>";
        $oldPersonalInformation .= "<b>email: </b>".$profile->email."<br/>";
        $oldInfo = self::findOne(['id' =>$this->id]);
        $oldInfo->admin_message = $oldPersonalInformation;
        $profile->first_name = \Yii::$app->request->post('AllowPersonalInfo')['first_name'];
        $profile->middle_name = \Yii::$app->request->post('AllowPersonalInfo')['middle_name'];
        $profile->last_name = \Yii::$app->request->post('AllowPersonalInfo')['last_name'];
        $profile->phone_mobile_local = \Yii::$app->request->post('AllowPersonalInfo')['phone_mobile'];
        $profile->email = \Yii::$app->request->post('AllowPersonalInfo')['email'];
        if($profile->save() && $oldInfo->save()){
            return true;
        }
        return false;
    }

    public static function getLastInfo($profile_id)
    {
        $model = self::find()
            ->where(['profile_id' => $profile_id])
            ->orderBy(['id' => SORT_DESC])
            ->limit(1)
            ->asArray()
            ->one();
        if(empty($model)){
            return false;
        }
        return ($model['admin_allow_message'] && !$model['admin_message']) ? $model['admin_allow_message'] : false;
    }
}
