<?php

namespace modules\profiles\common\models;

use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_premium_history".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $premium_sum
 * @property string $document_number
 * @property string $document_act
 */
class PremiumHistory extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%premium_history}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Premium History';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Premium Histories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['order_id', 'integer'],
            ['premium_sum', 'integer'],
            ['document_number', 'string', 'max' => 255],
            ['document_act', 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'premium_sum' => 'Premium Sum',
            'document_number' => 'Document Number',
            'document_act' => 'Document Act',
        ];
    }

    /**
     * Таблица списаний остатка премии
     * @param $profile_id
     * @param $order_id
     * @return array|bool|\yii\db\ActiveRecord[]
     */
    public static function getCompensationTable($order_id)
    {
        $model = self::find()
            ->where(['order_id' =>$order_id])
            ->asArray()
            ->all();
        if(empty($model)){
            return false;
        }
        return $model;
    }
}
