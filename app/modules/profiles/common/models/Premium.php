<?php

namespace modules\profiles\common\models;

use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_premium".
 *
 * @property integer $id
 * @property integer $bonus_sum
 * @property integer $premium_sum
 * @property integer $remainder_sum
 * @property integer $profile_id
 * @property string $created_at
 */
class Premium extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%premium}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Premium';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Премии';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['bonus_sum', 'integer'],
            ['premium_sum', 'integer'],
            ['profile_id', 'integer'],
            ['remainder_sum', 'integer'],
            ['created_at', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bonus_sum' => 'Сумма потраченных баллов',
            'premium_sum' => 'Сумма премии, руб.',
            'remainder_sum' => 'Остаток',
            'created_at' => 'Дата списания баллов',
        ];
    }
}
