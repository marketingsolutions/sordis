<?php

namespace modules\profiles\common\models;

use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_discount_history".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $discount_sum
 * @property string $document_number
 * @property string $document_id
 * @property string $document_created_at
 */
class DiscountHistory extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%discount_history}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Discount History';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Discount Histories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['order_id', 'integer'],
            ['discount_sum', 'integer'],
            ['document_id', 'string'],
            ['document_number', 'string', 'max' => 255],
            ['document_created_at', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'discount_sum' => 'Discount Sum',
            'document_number' => 'Document Number',
            'document_created_at' => 'Document Created At',
        ];
    }

    /**
     * Таблица с компенсациея по скидкам
     * @param $order_id
     * @return array|bool|\yii\db\ActiveRecord[]
     */
    public static function getCompensationTable($order_id)
    {
        $model = self::find()
            ->where(['order_id' =>$order_id])
            ->asArray()
            ->all();
        if(empty($model)){
            return false;
        }
        return $model;
    }
}
