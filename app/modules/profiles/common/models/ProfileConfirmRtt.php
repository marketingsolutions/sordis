<?php

namespace modules\profiles\common\models;

use modules\sales\common\models\SalePoint;
use modules\sales\common\models\SalePointProfile;
use Yii;
use yz\interfaces\ModelInfoInterface;
use modules\profiles\common\models\Profile;

/**
 * This is the model class for table "yz_profile_confirm_rtt".
 *
 * @property integer $id
 * @property string $code_egais
 * @property string $inn
 * @property string $name
 * @property string $address
 * @property string $license_number
 * @property string $work_time_from
 * @property string $work_time_to
 * @property string $created_at
 * @property string $add_delele_at
 * @property integer $profile_id
 * @property string $status
 * @property integer $is_xml_upload
 * @property string $type
 * @property string $is_xml_upload_at
 * @property integer $is_view
 *
 * @property Profiles $profile
 */
class ProfileConfirmRtt extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    const STATUS_NEW = 'new';
    const STATUS_CONFIRM = 'confirm';
    const STATUS_CANCEL = 'cancel';
    const STATUS_DELETED = 'deleted';

    const TYPE_ADD = 'add';
    const TYPE_DELETE = 'delete';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile_confirm_rtt}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Profile Confirm Rtt';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        if (\Yii::$app->controller->id == 'profile-req') {
            return 'Заявки участников на добавление/удаление РТТ';
        } else {
            return 'Список добавленных на модерацию РТТ';
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['code_egais', 'string', 'max' => 25],
            ['type', 'string', 'max' => 15],
            ['inn', 'string', 'max' => 25],
            ['name', 'string', 'max' => 255],
            ['address', 'string', 'max' => 255],
            ['license_number', 'string', 'max' => 25],
            ['work_time_from', 'safe'],
            ['work_time_to', 'safe'],
            ['created_at', 'safe'],
            ['add_delele_at', 'safe'],
            ['is_xml_upload', 'integer'],
            ['is_xml_upload_at', 'safe'],
            ['profile_id', 'integer'],
            ['status', 'string', 'max' => 12],
            ['is_view', 'string', 'max' => 1],
            [['profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['profile_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID#',
            'code_egais' => 'Код ЕГАИС',
            'type' => 'Тип модерации',
            'inn' => 'ИНН',
            'name' => 'Название торговой точки',
            'address' => 'Адрес торговой точки',
            'license_number' => 'Номер лицензии',
            'work_time_from' => 'Дата работы магазина С',
            'work_time_to' => 'Дата работы магазина По',
            'created_at' => 'Дата создания заявки',
            'add_delele_at' => 'Дата подтверждения запроса на добавление/удаление',
            'profile_id' => 'Profile ID',
            'status' => 'Статус обработки заявки',
            'is_view' => 'Is View',
            'is_xml_upload' => 'Данные об участнике отосланы в Сордис',
            'is_xml_upload_at' => 'Дата отправки файла в Сордис',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }

    /**
     * @return mixed
     */
    public function getProfileName()
    {
        return $this->profile->full_name;
    }

    /**
     * @return mixed
     */
    public function getProfilePhone()
    {
        return $this->profile->phone_mobile;
    }

    /**
     * @return mixed
     */
    public function getProfileMail()
    {
        return $this->profile->email;
    }

    public static function getRttCount($profileId)
    {
        return self::find()->where(['profile_id' => $profileId, 'status'=>self::STATUS_NEW])->count();
    }

    /**
     * @return array
     */
    public static function getStatus()
    {
        return [
            self::STATUS_NEW => 'На модерации',
            self::STATUS_CONFIRM => 'Точка добавлена',
            self::STATUS_CANCEL => 'Отклонена',
            self::STATUS_DELETED => 'Удалена',
        ];
    }

    public static function getType(){
        return [
            self::TYPE_ADD  => 'Добавление',
            self::TYPE_DELETE  => 'Удаление',
        ];
    }

    /**
     * Список РТТ в списке на согласовании
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getModerationRtt()
    {
        $profile = \Yii::$app->user->identity->profile;
        $model = self::find()
            ->where(['profile_id' => $profile->id,'status' => self::STATUS_NEW])
            ->orWhere(['profile_id' => $profile->id,'status' => self::STATUS_CANCEL])
            ->orWhere(['profile_id' => $profile->id,'status' => self::STATUS_DELETED])
            ->asArray()
            ->all();
        if ($model) {
            return $model;
        } else {
            return false;
        }
    }

    /**
     * Список РТТ по ID пользователя
     * @param $profileId
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getMyConfirmDeleteRtt($profileId, $type = self::TYPE_ADD )
    {
        if($type != self::TYPE_ADD){
            $type = self::TYPE_DELETE;
        }
        $model = self::find()
            ->where(['profile_id' => $profileId])
            ->andWhere(['status' => self::STATUS_NEW, 'type' => $type])
            ->asArray()
            ->all();
        if ($model) {
            return $model;
        } else {
            return false;
        }
    }

    /**
     * Проверка на существование введенных ЕГАИС РТТ находящихся на согласовании
     * @param $arrRtt
     * @param $profileId
     * @return array|bool
     */
    public static function isAllRtt($arrRtt, $profileId)
    {
        $model = ProfileConfirmRtt::find()
            ->select('{{%profile_confirm_rtt}}.code_egais as egais_rtt, {{%sale_point}}.id as sale_point_id')
            ->leftJoin('{{%sale_point}}', '{{%sale_point}}.code_egais={{%profile_confirm_rtt}}.code_egais')
            ->where(['{{%profile_confirm_rtt}}.id' => $arrRtt, '{{%profile_confirm_rtt}}.profile_id' => $profileId])
            ->asArray()
            ->all();

        $noRttEgais = [];
        foreach ($model as $rtt) {
            if (!$rtt['sale_point_id']) {
                $noRttEgais[] = $rtt['egais_rtt'];
            }
        }
        if (count($noRttEgais) > 0) {
            return $noRttEgais;
        } else {
            return false;
        }
    }

    /**
     * Проверка на принадлежность РТТ какому либо участнику
     * @param $arrRtt
     * @param $profileId
     * @return array|bool
     */
    public static function isArrRtt($arrRtt, $profileId)
    {
        $model = ProfileConfirmRtt::find()
            ->select('{{%profile_confirm_rtt}}.code_egais as egais_rtt, {{%sale_point}}.id as sale_point_id, {{%sale_point_profile}}.id as useId')
            ->leftJoin('{{%sale_point}}', '{{%sale_point}}.code_egais={{%profile_confirm_rtt}}.code_egais')
            ->leftJoin('{{%sale_point_profile}}', '{{%sale_point_profile}}.sale_point_id={{%sale_point}}.id')
            ->where(['{{%profile_confirm_rtt}}.id' => $arrRtt, '{{%profile_confirm_rtt}}.profile_id' => $profileId])
            ->asArray()
            ->all();
        $rttInUse = [];
        foreach ($model as $rtt) {
            if ($rtt['useId']) {
                $rttInUse[] = $rtt['egais_rtt'];
            }
        }
        if (count($rttInUse) > 0) {
            return $rttInUse;
        } else {
            return false;
        }
    }


    /**
     * Добавление валидных РТТ
     * @param $arrRtt
     * @param $profileId
     * @return bool
     */
    public static function addValidRtt($arrRtt, $profileId)
    {
        $model = ProfileConfirmRtt::find()
            ->select('{{%profile_confirm_rtt}}.work_time_to as to_t,{{%profile_confirm_rtt}}.work_time_from as from_t,{{%profile_confirm_rtt}}.code_egais as egais_rtt,{{%profile_confirm_rtt}}.license_number as license ,{{%sale_point}}.id as sale_point_id, {{%profile_confirm_rtt}}.id as idConfirm')
            ->leftJoin('{{%sale_point}}', '{{%sale_point}}.code_egais={{%profile_confirm_rtt}}.code_egais')
            ->leftJoin('{{%sale_point_profile}}', '{{%sale_point_profile}}.sale_point_id={{%sale_point}}.id')
            ->where(['{{%profile_confirm_rtt}}.id' => $arrRtt, '{{%profile_confirm_rtt}}.profile_id' => $profileId])
            ->asArray()
            ->all();
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            foreach ($model as $rtt) {
                $newRtt = new SalePointProfile();
                $newRtt->profile_id = $profileId;
                $newRtt->sale_point_id = $rtt['sale_point_id'];
                $newRtt->save(false);
                $confirmRtt = self::findOne(['id' => $rtt['idConfirm']]);
                $confirmRtt->status = self::STATUS_CONFIRM;
                //Проводим проверку, была ли удалена ТТ сегодня, если да, то дата добавления не сегодня, а зватра
                $isTodayDelete = self::find()
                    ->where(['code_egais' => $confirmRtt->code_egais, 'type' => self::TYPE_DELETE])
                    ->orderBy(['id' =>SORT_DESC])
                    ->asArray()
                    ->one();
                $now = date("Y-m-d");
                if(!empty($isTodayDelete) && $isTodayDelete['add_delele_at'] && date("Y-m-d", strtotime($isTodayDelete['add_delele_at']))==$now){
                    $datePlus1 = date("Y-m-d H:i:s", strtotime($isTodayDelete['add_delele_at']. ' + 1 day'));
                    $confirmRtt->add_delele_at = $datePlus1;
                }else{
                    $confirmRtt->add_delele_at = date("Y-m-d H:i:s");
                }
                $confirmRtt->save(false);
                $updateSalePoint = SalePoint::findOne(['id' => $rtt['sale_point_id']]);
                $updateSalePoint->license_number = $rtt['license'];
                $updateSalePoint->work_time_from = $rtt['from_t'];
                $updateSalePoint->work_time_to = $rtt['to_t'];
                $updateSalePoint->registred_at = isset($datePlus1) ? $datePlus1 : date("Y-m-d H:i:s");
                $updateSalePoint->save(false);
            }
            $transaction->commit();
            return true;
        }catch (\Exception $e){
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * Отмена добавления РТТ
     * @param $arrRtt
     * @param $profileId
     * @return bool
     */
    public static function declineRtt($arrRtt, $profileId){
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            foreach ($arrRtt as $rtt) {
                $model = self::findOne(['id' => $rtt, 'profile_id' => $profileId]);
                $model->status = self::STATUS_CANCEL;
                $model->add_delele_at = date("Y-m-d H:i:s");
                $model->save(false);
            }
            $transaction->commit();
            return true;
        } catch (\Exception $e){
            $transaction->rollBack();
            return false;
        }
    }

    public static function deleteRtt($arrRtt, $profileId){
        $transaction = \Yii::$app->db->beginTransaction();
        $arrReturn=[];
        $arrSalePointId = [];
        foreach ($arrRtt as $rtt){
            $model = self::findOne(['id' => $rtt, 'profile_id'=>$profileId]);
            $model->status = self::STATUS_DELETED;
            $model->add_delele_at = date("Y-m-d H:i:s");
            $model->save(false);
            $salePoint = SalePoint::findOne(['code_egais'=> $model->code_egais]);
            $salePoint_id = $salePoint->id;
            $salePoint->registred_at = null;
            $salePoint->save(false);
            $salePointProfile_id = SalePointProfile::findOne(['sale_point_id' => $salePoint_id, 'profile_id' => $profileId]);
            $arrSalePointId[]=$salePointProfile_id->id;
            $salePointProfile_id->delete();
        }
        $transaction->commit();
        $arrReturn['list']=$arrSalePointId;
        $arrReturn['del_list']=$arrRtt;

        return $arrReturn;
    }

    public static function cancelRtt($arrRtt, $profileId){
        $transaction = \Yii::$app->db->beginTransaction();
        $arrReturn=[];
        $arrSalePointId = [];
        foreach ($arrRtt as $rtt){
            $model = self::findOne(['id' => $rtt, 'profile_id'=>$profileId]);
            $model->status = self::STATUS_CANCEL;
            $model->add_delele_at = date("Y-m-d H:i:s");
            $model->save(false);
            $salePoint = SalePoint::findOne(['code_egais'=> $model->code_egais]);
            $salePointProfile_id = SalePointProfile::findOne(['sale_point_id' => $salePoint->id, 'profile_id' => $profileId]);
            $arrSalePointId[]=$salePointProfile_id->id;

        }
        $transaction->commit();
        $arrReturn['list']=$arrSalePointId;
        $arrReturn['del_list']=$arrRtt;

        return $arrReturn;
    }

    /**
     * Список подтвержденных РТТ для добавления после массового добавления в общий список РТТ
     * @param $arrRtt
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getRttAfterAdd($arrRtt){
        $model = self::find()
            ->select(['{{%sale_point_profile}}.id as del_rtt,{{%profile_confirm_rtt}}.code_egais','{{%profile_confirm_rtt}}.id as confirm_id', '{{%profile_confirm_rtt}}.address', '{{%profile_confirm_rtt}}.name', '{{%profile_confirm_rtt}}.inn' , '{{%sale_point}}.kpp', '{{%profile_confirm_rtt}}.license_number',"CONCAT(HOUR({{%profile_confirm_rtt}}.work_time_from),':',MINUTE({{%profile_confirm_rtt}}.work_time_from)) as from_t", "CONCAT(HOUR({{%profile_confirm_rtt}}.work_time_to),':',MINUTE({{%profile_confirm_rtt}}.work_time_to)) as to_t"])
            ->leftJoin('{{%sale_point}}', '{{%sale_point}}.code_egais={{%profile_confirm_rtt}}.code_egais')
            ->leftJoin('{{%sale_point_profile}}', '{{%sale_point_profile}}.sale_point_id={{%sale_point}}.id')
            ->where(['{{%profile_confirm_rtt}}.id' => $arrRtt])
            ->asArray()
            ->all();
        return $model;
    }

    /**
     * @param $profileId
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getRttDeleteData($profileId){
        $rttInConfirm = ProfileConfirmRtt::find()->select('code_egais')->where(['profile_id' => $profileId, 'type' => self::TYPE_DELETE])->asArray()->all();
        $arrEgais=[];
        foreach ($rttInConfirm as $egais){
            $arrEgais[]=$egais['code_egais'];
        }
        $model = SalePointProfile::find()
            ->select('{{%sale_point}}.id as ids, {{%sale_point}}.name as sale_point_name, {{%sale_point}}.address as sale_point_adderss, {{%sale_point}}.license_number as license, {{%sale_point}}.code_egais as egais, {{%sale_point}}.inn as sale_point_inn, {{%sale_point}}.kpp as sale_point_kpp, {{%sale_point}}.work_time_from as time_from, {{%sale_point}}.work_time_to as time_to')
            ->leftJoin('{{%sale_point}}', '{{%sale_point}}.id={{%sale_point_profile}}.sale_point_id')
            ->where(['{{%sale_point_profile}}.profile_id' => $profileId])
           // ->andWhere(['not in', '{{%sale_point}}.code_egais', $arrEgais ])
            ->asArray()
            ->all();
        return $model;
    }

}
