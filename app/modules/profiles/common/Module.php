<?php

namespace modules\profiles\common;


/**
 * Class Module
 */
class Module extends \yz\Module
{
    /**
     * If true, it will be possible to register new user not from the list
     * @var bool
     */
    public $enableFreeRegistration = false;
}