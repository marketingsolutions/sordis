<?php

use marketingsolutions\datetime\DatePickerConfig;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var modules\profiles\common\models\Action $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php $box = FormBox::begin(['cssClass' => 'action-form box-primary', 'title' => '']) ?>
<?php $form = ActiveForm::begin(); ?>

<?php $box->beginBody() ?>
<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

<?= $form->field($model, 'enabled')->checkbox() ?>

<?= $form->field($model, 'date_start_local')->widget(\yii\widgets\MaskedInput::class, [
    'mask' => '99.99.9999',
    'options' => [
        'class' => 'form-control',
        'placeholder' => 'дд.мм.гггг',
    ]
]) ?>

<?= $form->field($model, 'date_end_local')->widget(\yii\widgets\MaskedInput::class, [
    'mask' => '99.99.9999',
    'options' => [
        'class' => 'form-control',
        'placeholder' => 'дд.мм.гггг',
    ]
]) ?>

<?= $form->field($model, 'product_ids')
    ->select2(\modules\sales\common\models\Product::getOptions(), ['multiple' => true]) ?>

<?php $box->endBody() ?>

<?php $box->actions([
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
]) ?>
<?php ActiveForm::end(); ?>

<?php FormBox::end() ?>
