<?php

use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;
use yz\admin\widgets\ActiveForm;
use modules\profiles\backend\forms\AddRttForm;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use  modules\profiles\common\models\ProfileConfirmRtt;

/**
 * @var yii\web\View $this
 * @var modules\profiles\common\models\Profile $model
 */
$this->title = \Yii::t('admin/t', 'Update {item}', ['item' => modules\profiles\common\models\Profile::modelTitle()]);
$this->params['breadcrumbs'][] = ['label' => modules\profiles\common\models\Profile::modelTitlePlural(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
$js = <<<JS
$( document ).ready(function() {
    /*Нумерация строк*/
    function autoNumRow() {
        var i=0;
      $('.rowNumeric').each(function( index ) {
          i++;
          $(this).text(i);
      });
    }
    /*Модальное окно подтверждения удаления*/
    $("body").on("click", ".delete_rtt", function () {
        var deletedId = $(this).attr('id');
        $('.delete_this').attr('id', deletedId);
        $('#my-modal').show(); 
     });
    /*Удаление РТТ*/
    $("body").on("click", ".delete_this", function () { 
        var idForDelete= $(this).attr('id');
        $.ajax(
            {
              type: 'POST',
              url: '/profiles/profiles/delete-rtt',
              data: {'id': idForDelete},
              success: function(data) {
                $('#delPosition'+idForDelete).remove();
                $('#my-modal').modal('hide');
                autoNumRow();
            },
            error:   function(xhr) {
                var data = xhr.responseJSON;
                if ("errors" in data) {
                    $("#reg-errors li").remove();
                    for (var i = 0; i < data.errors.length; i++) {
                        $("#reg-errors").append("<li>" + data.errors[i] + "</li>");
                    }
                }
            }
            });
    });
    
    $('#add_sale_point').click(function() {
        $('#hiddenAddSelect').show();
        $('#add_sale_point').hide();
        $('#add_rtt, #no_add').show();
    });
    
    $('#no_add').click(function() {
        $('#hiddenAddSelect, #add_rtt, #no_add').hide();
        $('#addrttform-salepoint').val("");
        $("#addrttform-salepoint [value='']").attr("selected", true);
        $("#select2-chosen-1").text("Выберите ...");
        $('#add_sale_point').show();
    });
    
    /*Добавление РТТ*/
    $("body").on("click", "#add_rtt", function () {    
        var addAddRtt = $('#addrttform-id_sale_point').val();
        var userId = $('#user_id').val();
        $.ajax(
            {
              type: 'POST',
              url: '/profiles/profiles/add-rtt',
              data: {'id': addAddRtt, 'user_id': userId},
              success: function(data) {
                  var newAddRtt = '<tr id="delPosition'+data.id_sale_point+'">'+
                                    '<th scope="row" class="rowNumeric"></th>'+
                                    '<td>'+data.name+'</td>'+
                                    '<td>'+data.address+'</td>';
                                    if(data.license_number == null){
                                       newAddRtt += '<td></td>';
                                    }else{
                                        newAddRtt += '<td>'+data.license_number+'</td>';
                                    }
                                    newAddRtt += '<td>'+data.code_egais+'</td>'+
                                    '<td>'+data.inn+'</td>'+
                                    '<td>'+data.kpp+'</td>'+
                                    '<td>'+data.time+'</td>'+
                                    '<td width="50"><div id="'+data.id_sale_point+'" data-toggle="modal" data-target=".bd-example-modal-sm" class="delete_rtt"><i data-toggle="tooltip" data-placement="right" title="Удалить эту РТТ у участника. Ртт останется в списке и не будет принадлежать данному участнику" style="color:red; cursor:pointer;"  class="glyphicon glyphicon-remove"></i></div></td></tr>';
                  $('.tableRtt').append(newAddRtt);
                  $("#addrttform-salepoint [value='']").attr("selected", true);
                  $("#addrttform-salepoint").val("");
                  autoNumRow();
            },
            error:   function(xhr) {
                var data = xhr.responseJSON;
                if ("errors" in data) {
                    $("#reg-errors2 li").remove();
                    for (var i = 0; i < data.errors.length; i++) {
                        $("#reg-errors2").append("<li>" + data.errors[i] + "</li>");
                    }
                }
            }
            });
    });
    
    /*Добавление списка новых РТТ*/
    $("body").on("click", "#add_all_rtt", function () {
        var arr = [];
        arr["points"] = [];
        var userId = $('#user_id').val();
        $( ".my_position" ).each(function() {
            if($(this).prop('checked') == true){
                var newRtt = $(this).attr('id');
                arr.points.push(newRtt);
            }
            
        });
         if(arr["points"].length == 0){
             console.log('Нужно что то выделить');
             $("#reg-errors_allrtt li").remove();
             $("#reg-errors_allrtt").append("<li>Выделите хотя бы одну позицию</li>");
         }else{
             $.ajax(
            {
                type: 'POST',
                url: '/profiles/profiles/add-all-rtt',
                data: {'id': arr["points"], 'user_id': userId},
                success: function(data) {
                  console.log(data);
                  $.each(data.id_deleted, function(index, value) {
                      $('#decline'+value.confirm_id).remove();
                      var newAddRtt = '<tr id="delPosition'+value.del_rtt+'">'+
                                    '<th scope="row" class="rowNumeric"></th>'+
                                    '<td>'+value.name+'</td>'+
                                    '<td>'+value.address+'</td>'+
                                    '<td>'+value.license_number+'</td>'+
                                    '<td>'+value.code_egais+'</td>'+
                                    '<td>'+value.inn+'</td>'+
                                    '<td>'+value.kpp+'</td>'+
                                    '<td>'+value.from_t+'0'+'-'+value.to_t+'0</td>'+
                                    '<td width="50"><div id="'+value.del_rtt+'" data-toggle="modal" data-target=".bd-example-modal-sm" class="delete_rtt"><i data-toggle="tooltip" data-placement="right" title="Удалить эту РТТ у участника. Ртт останется в списке и не будет принадлежать данному участнику" style="color:red; cursor:pointer;"  class="glyphicon glyphicon-remove"></i></div></td></tr>';
                       $('.tableRtt').append(newAddRtt);
                       autoNumRow();
                  });
                },
                error:   function(xhr) {
                    var data = xhr.responseJSON;
                    if ("errors" in data) {
                       $("#reg-errors_allrtt li").remove();
                        for (var i = 0; i < data.errors.length; i++) {
                            $("#reg-errors_allrtt").append("<li>" + data.errors[i] + "</li>");
                        }
                    }
                }
            });
         } 
    });
    
    /*Отклонение привязки списка РТТ*/
    $("body").on("click", "#decline_all_rtt", function () {
        var arr = [];
        arr["points"] = [];
        var userId = $('#user_id').val();
        $( ".my_position" ).each(function() {
            if($(this).prop('checked') == true){
                var newRtt = $(this).attr('id');
                arr.points.push(newRtt);
                }
        });
            if(arr["points"].length == 0){
                console.log('Нужно что то выделить');
                $("#reg-errors_allrtt li").remove();
                $("#reg-errors_allrtt").append("<li>Выделите хотя бы одну позицию</li>");
            }else{
                $.ajax(
                {
                    type: 'POST',
                    url: '/profiles/profiles/decline-all-rtt',
                    data: {'id': arr["points"], 'user_id': userId},
                    success: function(data) {
                        $("#reg-errors_allrtt li").remove();
                        $.each(arr["points"], function(index, value) {
                            $('#decline'+value).remove();
                        });  
                    },
                    error:   function(xhr) {
                        var data = xhr.responseJSON;
                        if ("errors" in data) {
                           $("#reg-errors_allrtt li").remove();
                            for (var i = 0; i < data.errors.length; i++) {
                                $("#reg-errors_allrtt").append("<li>" + data.errors[i] + "</li>");
                            }
                        }
                    }
                });  
            }
       });
        /*Удаление списка РТТ*/
        $("body").on("click", "#delete_all_rtt", function () {
            var arr = [];
            arr["points"] = [];
            var userId = $('#user_id').val();
            
            $(".my_del_position").each(function() {
            if($(this).prop('checked') == true){
                var delRtt = $(this).attr('id');
                arr.points.push(delRtt);
                }
            });
            if(arr["points"].length == 0){
                console.log('Нужно что то выделить');
                $("#reg-errors_allrttdell li").remove();
                $("#reg-errors_allrttdell").append("<li>Выделите хотя бы одну позицию</li>");
            }else{
                $.ajax(
                {
                    type: 'POST',
                    url: '/profiles/profiles/delete-all-rtt',
                    data: {'id': arr["points"], 'user_id': userId},
                    success: function(data) {
                        $("#reg-errors_allrttdel li").remove();
                        $.each(data.sale_point_profile_id.del_list, function(index, value) {
                            $('#delete'+parseInt(value)).remove();
                            console.log(value);
                        });  
                        $.each(data.sale_point_profile_id.list, function(index, value) {
                            $('#delPosition'+parseInt(value)).remove();
                            console.log(value);
                        }); 
                    },
                    error:   function(xhr) {
                        var data = xhr.responseJSON;
                        if ("errors" in data) {
                           $("#reg-errors_allrtt li").remove();
                            for (var i = 0; i < data.errors.length; i++) {
                                $("#reg-errors_allrttdel").append("<li>" + data.errors[i] + "</li>");
                            }
                        }
                    }
                });  
            }
        });
        
        //Отклонение удаления всех РТТ
        $("body").on("click", "#cancel_all_rtt", function () {
            var arr = [];
            arr["points"] = [];
            var userId = $('#user_id').val();
            
            $(".my_del_position").each(function() {
            if($(this).prop('checked') == true){
                var delRtt = $(this).attr('id');
                arr.points.push(delRtt);
                }
            });
            if(arr["points"].length == 0){
                console.log('Нужно что то выделить');
                $("#reg-errors_allrttdell li").remove();
                $("#reg-errors_allrttdell").append("<li>Выделите хотя бы одну позицию</li>");
            }else{
                $.ajax(
                {
                    type: 'POST',
                    url: '/profiles/profiles/cancel-all-rtt',
                    data: {'id': arr["points"], 'user_id': userId},
                    success: function(data) {
                        $("#reg-errors_allrttdel li").remove();
                        $.each(data.sale_point_profile_id.del_list, function(index, value) {
                            $('#delete'+parseInt(value)).remove();
                            console.log(value);
                        });  
                        $.each(data.sale_point_profile_id.list, function(index, value) {
                            $('#delPosition'+parseInt(value)).remove();
                            console.log(value);
                        }); 
                    },
                    error:   function(xhr) {
                        var data = xhr.responseJSON;
                        if ("errors" in data) {
                           $("#reg-errors_allrtt li").remove();
                            for (var i = 0; i < data.errors.length; i++) {
                                $("#reg-errors_allrttdel").append("<li>" + data.errors[i] + "</li>");
                            }
                        }
                    }
                });  
            }
        });
        
        
});
JS;
$this->registerJs($js);

?>
<input type="hidden" id="user_id" value="<?= \Yii::$app->request->get('id') ?>">
<div class="profile-update">
  <div id="my-modal" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog"
       aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h3 align="center">Внимание!</h3>
        </div>
        <div class="modal-body">
          <p style="font-size: 15px;">Вы уверены, что хотите удалить у данного участника данную РТТ? Данная РТТ
            останется в общем списке РТТ!</p>
          <ul style="color: red;" id="reg-errors"></ul>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
          <button id="" type="button" class="btn btn-primary delete_this">Удалить</button>
        </div>
      </div>
    </div>
  </div>

  <div class="text-right">
      <?php Box::begin() ?>
      <?= ActionButtons::widget([
          'order' => [['index', /*'update',*/ 'return']],
          'addReturnUrl' => false,
      ]) ?>
      <?php Box::end() ?>
  </div>

    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
<div class="row">
  <div class="col-md-12">
      <?php Box::begin() ?>
    <h3>Розничные торговые точки участника</h3>
    <table class="table table-hover tableRtt">
      <thead>
      <tr>
        <th>#</th>
        <th>Наименование РТТ</th>
        <th>Адрес</th>
        <th>Номер лицензии</th>
        <th>Код ЕГАИС</th>
        <th>ИНН</th>
        <th>КПП</th>
        <th>Время работы (с - по)</th>
<!--        <th>Удаление РТТ</th>-->

      </tr>
      </thead>
      <tbody>
      <?php $i = 0; ?>
      <?php foreach ($salePoint as $point): ?>
          <?php $i++; ?>
        <tr id="delPosition<?= $point['id_sale_point'] ?>">
          <th scope="row" class="rowNumeric"><?= $i ?></th>
          <td><?= $point['name'] ?></td>
          <td><?= $point['address'] ?></td>
          <td><?= $point['license_number'] ?></td>
          <td><?= $point['code_egais'] ?></td>
          <td><?= $point['inn'] ?></td>
          <td><?= $point['kpp'] ?></td>
          <td><?= date("H:i", strtotime($point['work_time_from'])) . " - " . date("H:i", strtotime($point['work_time_to'])) ?></td>
<!--          <td width="50">-->
<!--            <div id="-->
            <?php
                //= $point['id_sale_point'];
            ?><!--" data-toggle="modal" data-target=".bd-example-modal-sm"-->
<!--                 class="delete_rtt"><i data-toggle="tooltip" data-placement="right"-->
<!--                                       title="Удалить эту РТТ у участника. Ртт останется в списке и не будет принадлежать данному участнику"-->
<!--                                       style="color:red; cursor:pointer;" class="glyphicon glyphicon-remove"></i></div>-->
<!--          </td>-->
        </tr>
      <?php endforeach; ?>
      </tbody>
    </table>
    <div id="hiddenAddSelect" style="display: none">
        <?php $formAdd = ActiveForm::begin(); ?>
      <p style="font-size: 15px; color:green;"><i>Начните вводить в поле поиска название торговой точки, либо адрес
          торговой точки либо код ЕГАИС</i></p>
        <?= $formAdd->field($formRtt, 'salePoint')->textInput()->widget(
            \yii\jui\AutoComplete::className(), [
            'clientOptions' => [
                'source' => AddRttForm::getAllRtt(),
                'minLength' => '6',
                'autoFill' => true,
                'select' => new JsExpression("function( event, ui ) {
                    console.log(ui);
			        $('#addrttform-salepoint').val(ui.item.label);
			        $('#addrttform-id_sale_point').val(ui.item.ids);
			     }")
            ],
            'options' => [
                'class' => 'form-control'
            ]
        ]); ?>
        <?= $formAdd->field($formRtt, 'id_sale_point')->hiddenInput()->label(false) ?>

        <?php ActiveForm::end(); ?>
    </div>
    <ul style="color: red;" id="reg-errors2"></ul>
    <button id="add_sale_point" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;Добавление РТТ участнику
    </button>
    <button id="add_rtt" style="display: none" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;Добавить
    </button>
    <button id="no_add" style="display: none" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;Отмена
    </button>
      <?php Box::end() ?>
  </div>
</div>
<?php if ($confirmRtt): ?>
  <div class="row">
    <div class="col-md-12">
        <?php Box::begin() ?>
      <h3 style="color:green;">Заявки на добавление торговых точек</h3>
      <table class="table table-hover tableConfirmRtt">
        <thead>
        <tr>
          <th>#</th>
          <th>Наименование РТТ</th>
          <th>Адрес</th>
          <th>Номер лицензии</th>
          <th>Код ЕГАИС</th>
          <th>ИНН</th>
          <th>КПП</th>
          <th>Время работы (с - по)</th>
          <th>Статус</th>

        </tr>
        </thead>
        <tbody>
        <?php foreach ($confirmRtt as $newRtt): ?>
          <tr id="decline<?= $newRtt['id'] ?>">
            <td><input id="<?= $newRtt['id'] ?>" type="checkbox" class="my_position" value="1"/></td>
            <td><span class="rtt__new_name"><?= $newRtt['name'] ?></span></td>
            <td><span class="rtt__new_address"><?= $newRtt['address'] ?></span></td>
            <td><span class="rtt__new_license_number"><?= $newRtt['license_number'] ?></span></td>
            <td><span class="rtt__new_code_egais"><?= $newRtt['code_egais'] ?></span></td>
            <td><span class="rtt__new_inn"><?= $newRtt['inn'] ?></span></td>
            <td></td>
            <td>
              <span><?= "C " . date("H:i", strtotime($newRtt['work_time_from'])) . " по " . date("H:i", strtotime($newRtt['work_time_to'])) ?></span>
            </td>
            <td>
                <?php if ($newRtt['status'] == ProfileConfirmRtt::STATUS_NEW): ?>
              <span style="font-size: 13px" class="label label-warning">
                    <?php elseif ($newRtt['status'] == ProfileConfirmRtt::STATUS_CANCEL): ?>
                        <span style="font-size: 13px" class="label label-danger">
                    <?php endif; ?>
                    <?= ProfileConfirmRtt::getStatus()[$newRtt['status']] ?>
                    </span>
            </td>

          </tr>
        <?php endforeach; ?>
        </tbody>
      </table>
      <ul style="color: red;" id="reg-errors_allrtt"></ul>
      <button id="add_all_rtt" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;Добавить
        выделенные точки участнику
      </button>
      <button id="decline_all_rtt" class="btn btn-danger"><i class="glyphicon glyphicon-ban-circle"></i>&nbsp;&nbsp;Отклонить
        добавление выделенным
      </button>
        <?php Box::end() ?>
    </div>
  </div>
<?php endif; ?>
<?php if ($deleteRtt): ?>
  <div class="row">
    <div class="col-md-12">
        <?php Box::begin() ?>
      <h3 style="color:green;">Заявки на удаление торговых точек</h3>
      <table class="table table-hover tableDeleteRtt">
        <thead>
        <tr>
          <th>#</th>
          <th>Наименование РТТ</th>
          <th>Адрес</th>
          <th>Номер лицензии</th>
          <th>Код ЕГАИС</th>
          <th>ИНН</th>
          <th>КПП</th>
          <th>Время работы (с - по)</th>
          <th>Статус</th>

        </tr>
        </thead>
        <tbody>
        <?php foreach ($deleteRtt as $newRtt): ?>
          <tr id="delete<?= $newRtt['id'] ?>">
            <td><input id="<?= $newRtt['id'] ?>" type="checkbox" class="my_del_position" value="1"/></td>
            <td><span class="rtt__new_name"><?= $newRtt['name'] ?></span></td>
            <td><span class="rtt__new_address"><?= $newRtt['address'] ?></span></td>
            <td><span class="rtt__new_license_number"><?= $newRtt['license_number'] ?></span></td>
            <td><span class="rtt__new_code_egais"><?= $newRtt['code_egais'] ?></span></td>
            <td><span class="rtt__new_inn"><?= $newRtt['inn'] ?></span></td>
            <td></td>
            <td>
              <span><?= "C " . date("H:i", strtotime($newRtt['work_time_from'])) . " по " . date("H:i", strtotime($newRtt['work_time_to'])) ?></span>
            </td>
            <td>
                <?php if ($newRtt['status'] == ProfileConfirmRtt::STATUS_NEW): ?>
              <span style="font-size: 13px" class="label label-warning">
                    <?php elseif ($newRtt['status'] == ProfileConfirmRtt::STATUS_CANCEL): ?>
                                <span style="font-size: 13px" class="label label-danger">
                    <?php endif; ?>
                    <?= ProfileConfirmRtt::getStatus()[$newRtt['status']] ?>
                    </span>
            </td>

          </tr>
        <?php endforeach; ?>
        </tbody>
      </table>
      <ul style="color: red;" id="reg-errors_allrttdell"></ul>
      <button id="delete_all_rtt" class="btn btn-danger"><i class="glyphicon glyphicon-ban-circle"></i>&nbsp;&nbsp;Удалить
        выделенные
      </button>
        <button id="cancel_all_rtt" class="btn btn-danger"><i class="glyphicon glyphicon-ban-circle"></i>&nbsp;&nbsp;Отклонить
            удаление выделенным
        </button>
        <?php Box::end() ?>
    </div>
  </div>

<?php endif; ?>


