<?php

use modules\profiles\common\models\Profile;
use yii\bootstrap\Html;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;
use yz\icons\Icons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\profiles\backend\models\ProfileSearch $searchModel
 * @var array $columns
 */

$this->title = modules\profiles\common\models\Profile::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
$this->registerCssFile('/css/my.css');
?>

<?php $box = Box::begin(['cssClass' => 'profile-index box-primary']) ?>
<div class="text-right">
    <?php echo ActionButtons::widget([
        'order' => [[/*'file', 'import', */'export', /*'create',  'delete', */'return']],
        'gridId' => 'profile-grid',
        'searchModel' => $searchModel,
        'modelClass' => \modules\profiles\common\models\Profile::class,
        'buttons' => [
            'file' => [
                'label' => 'Файл для загрузки',
                'icon' => Icons::o('file'),
                'route' => ['file'],
                'class' => 'btn btn-info',
            ],
            'import' => [
                'label' => 'Загрузить участников РТТ из XLS',
                'icon' => Icons::o('upload'),
                'route' => ['/profiles/import-profiles/index'],
                'class' => 'btn btn-info',
            ],
        ]
    ]) ?>
</div>

<?= GridView::widget([
    'id' => 'profile-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => array_merge([
        ['class' => 'yii\grid\CheckboxColumn'],
    ], $columns, [
        [
            'class' => 'yz\admin\widgets\ActionColumn',
            'template' => '{login} {update}',
            'buttons' => [
                'bonus' => function ($url, Profile $model) {
                    return Html::a(Icons::i('dollar'), \yii\helpers\Url::to(['/bonuses/bonuses/create/', 'profile_id' => $model->id]), [
                        'title' => 'Добавить бонус',
                        'data-method' => 'post',
                        'class' => 'btn btn-info btn-sm',
                        'data-pjax' => '0',
                    ]);
                },
                'login' => function ($url, Profile $model) {
                    return Html::a(Icons::i('key'), '/profiles/profiles/login?id=' . $model->id, [
                        'target' => '_blank',
                        'title' => 'Войти под участником',
                        'data-method' => 'post',
                        'class' => 'btn btn-warning btn-sm',
                        'data-pjax' => '0',
                    ]);
                },
            ],
        ],
    ]),
]); ?>
<?php Box::end() ?>


