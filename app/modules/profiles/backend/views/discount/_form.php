<?php

use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;
use modules\profiles\common\models\DiscountHistory;

/**
 * @var yii\web\View $this
 * @var modules\profiles\common\models\Discount $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php  $box = FormBox::begin(['cssClass' => 'discount-form box-primary', 'title' => '']) ?>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Номер заказа</th>
        <th>Дата списания баллов</th>
        <th>Сумма потраченных баллов</th>
        <th>Сумма скидки</th>
        <th>Остаток</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?=$model->id?></td>
        <td><?=date("d.m.Y H:i", strtotime($model->created_at))?></td>
        <td><?=$model->bonus_sum?></td>
        <td><?=$model->discount_sum?></td>
        <td><?=$model->remainder_sum?></td>
    </tr>
    </tbody>
</table>
<?php  FormBox::end() ?>

<?php if(DiscountHistory::getCompensationTable($model->id)):?>
    <?php  $box = FormBox::begin(['cssClass' => 'discount-form box-primary', 'title' => 'Начисление компенсаций']) ?>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Номер заказа</th>
            <th>Сумма компенсации, руб.</th>
            <th>Дополнительное соглашение (№, дата) </th>
            <th>Акт (номер, дата) </th>
        </tr>
        </thead>
        <tbody>
        <?php foreach (DiscountHistory::getCompensationTable($model->id) as $comp):?>
            <tr>
                <td><?=$comp['document_id']?></td>
                <td><?=$comp['discount_sum']?></td>
                <td><?=$comp['document_number']?></td>
                <td><?=date("d.m.Y", strtotime($comp['document_created_at']))?></td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
    <?php  FormBox::end() ?>
<?php endif;?>

<?php  $box = FormBox::begin(['cssClass' => 'discount-form box-primary', 'title' => '']) ?>
<?php $form = ActiveForm::begin([
    'id' => 'discount-form',
    'options' => ['class' => 'form-horizontal'],
]) ?>
<?= $form->field($discountModel, 'order_id')->hiddenInput(['value' => \Yii::$app->request->get('id')])->label(false) ?>
<?= $form->field($discountModel, 'document_id') ?>
<?= $form->field($discountModel, 'discount_sum') ?>
<?= $form->field($discountModel, 'document_number') ?>
<?= $form->field($discountModel, 'document_created_at')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '99.99.9999',
]) ?>

<?php $box->actions([
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
]) ?>
<?php ActiveForm::end() ?>
<?php FormBox::end()?>
