<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\profiles\backend\models\ProfileConfirmRttSearch $searchModel
 * @var array $columns
 */

$this->title = modules\profiles\common\models\ProfileConfirmRtt::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'profile-confirm-rtt-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [['export', /*'delete',*/ 'return']],
            'gridId' => 'profile-confirm-rtt-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\profiles\common\models\ProfileConfirmRtt',
        ]) ?>
    </div>
    <?= GridView::widget([
        'id' => 'profile-confirm-rtt-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '',
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
