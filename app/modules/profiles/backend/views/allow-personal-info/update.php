<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\widgets\ActionButtons;

/**
 * @var yii\web\View $this
 * @var modules\profiles\common\models\AllowPersonalInfo $model
 */
$this->title ="Рассмотрение заявки на редактирование профиля";
$this->params['breadcrumbs'][] = ['label' => modules\profiles\common\models\AllowPersonalInfo::modelTitlePlural(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;


?>
<div class="allow-personal-info-update">

    <div class="text-right">
        <?php  Box::begin() ?>
        <?=  ActionButtons::widget([
            'order' => [['index', 'return']],
            'addReturnUrl' => false,
        ]) ?>
        <?php  Box::end() ?>
    </div>

    <?php echo $this->render('_form', [
        'model' => $model,
        'adminAllow' => $adminAllow
    ]); ?>

</div>
