<?php

use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;

$profile = \modules\profiles\common\models\Profile::getProdileData($model->profile_id);
?>
<?php if($profile):?>
    <?php  $box = FormBox::begin() ?>
        <table class="table table-hover">
            <tbody>
            <tr>
                <td><b>Фамилия участника</b></td>
                <td><?=$profile->last_name?></td>

            </tr>
            <tr>
                <td><b>Имя участника</b></td>
                <td><?=$profile->first_name?></td>

            </tr>
            <tr>
                <td><b>Отчество участника</b></td>
                <td><?=$profile->middle_name?></td>

            </tr>
            <tr>
                <td><b>Телефон участника</b></td>
                <td><?=$profile->phone_mobile?></td>

            </tr>
            <tr>
                <td><b>E-mail участника</b></td>
                <td><?=$profile->email?></td>

            </tr>
            <tr>
                <td><b>Роль участника</b</td>
                <td><?=\modules\profiles\common\models\Profile::getDashboardRealRole()[$profile->role]?></td>
            </tr>
            <tr>
                <td><b>Телефон участника</b</td>
                <td><?=$profile->phone_mobile?></td>
            </tr>
            <tr>
                <td><b>Сообщение создано</b</td>
                <td><?=date("d.m.Y H:i:s", strtotime($model->created_at))?></td>
            </tr>
            <tr>
                <td><b>Сообщение обработано администратором</b</td>
                <td><?=$model->updated_at ? date("d.m.Y H:i:s", strtotime($model->updated_at)) : 'Сообщение не обработано'?></td>
            </tr>
            </tbody>
        </table>
    <?php  FormBox::end() ?>
<?php endif;?>
<?php  $box = FormBox::begin(['cssClass' => 'allow-personal-info-form box-primary', 'title' => '']) ?>
    <?php $form = ActiveForm::begin(); ?>
<h3 align="center">Новая личная информация</h3>
    <?php $box->beginBody() ?>
    <?php $model->isAllow = $profile->is_allow_personal;?>
    <?= $form->field($model, 'first_name') ?>
    <?= $form->field($model, 'last_name') ?>
    <?= $form->field($model, 'middle_name') ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'phone_mobile')->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+7 999 999-99-99',
    ]) ?>

    <?= $form->field($model, 'user_message')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'updated_at')->hiddenInput(['value' => date("Y-m-d H:I:s")])->label(false) ?>

    <?php $box->endBody() ?>

    <?php $box->actions([
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
    ]) ?>
    <?php ActiveForm::end(); ?>
<?php  FormBox::end() ?>

<?php if($model->admin_message):?>
    <?php $box = FormBox::begin();?>
    <h4>Информация до изменения</h4>
        <?=$model->admin_message?>
    <?php  FormBox::end() ?>
<?php else:?>
    <?php if($model->admin_allow_message):?>
        <div class="row">
            <div class="col-md-6">
                <div class="alert calc_calc alert-info alert-dismissible" role="alert">
                    <strong>Ваш ответ при отказе:</strong><br/><br/>
                    <?=$model->admin_allow_message?>
                </div>
            </div>
        </div>
    <?php else:?>
        <?php $box = FormBox::begin();?>
        <h4>Добавить ответ участнику для объяснения отказа редактирования</h4>
        <?php $formAdm = ActiveForm::begin(['action' => '/profiles/allow-personal-info/admin-add-allow']); ?>
        <?= $formAdm->field($adminAllow, 'admin_message')->textarea(['rows' => 6]) ?>
        <?= $formAdm->field($adminAllow, 'id')->hiddenInput(['value' => $model->id])->label(false) ?>
        <div class="col-sm-offset-2">
            <button class="btn btn-primary" type="submit">Отказать и отправить сообщение участнику</button>
        </div>
        <?php ActiveForm::end(); ?>
        <?php  FormBox::end() ?>
    <?php endif;?>
<?php endif;?>
