<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\ActiveForm;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use common\utils\Notifier;

/**
 * @var yii\web\View $this
 * @var modules\profiles\common\models\Profile $model
 * @var yz\admin\widgets\ActiveForm $form
 */

\marketingsolutions\assets\AngularAsset::register($this);
?>

<div ng-app="App" ng-controller="Controller">

    <div class="row col-md-9">
        <?php $box = FormBox::begin(['cssClass' => 'profile-form box-primary', 'title' => '']) ?>
        <?php $form = ActiveForm::begin(); ?>

        <?php $box->beginBody() ?>

        <?= $form->field($model, 'first_name')->textInput(['maxlength' => 32]) ?>
        <?= $form->field($model, 'middle_name')->textInput(['maxlength' => 32]) ?>
        <?= $form->field($model, 'last_name')->textInput(['maxlength' => 32]) ?>
        <?= $form->field($model, 'phone_mobile_local')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '+7 999 999-99-99',
        ]) ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => 64]) ?>
		<?= $form->field($model, 'role')->hiddenInput(['value' => \modules\profiles\common\models\Profile::ROLE_DISTRIBUTOR])->label(false); ?>

        <?php $box->endBody() ?>

        <?php if(!Notifier::checkRole($admin=null,'SORDIS_ADMIN')): ?>
        <?php $box->actions([
            AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
            AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
            AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
        ]) ?>
        <?php endif; ?>
        <?php ActiveForm::end(); ?>

        <?php FormBox::end() ?>
    </div>
    <div class="col-md-3">

        <?php Box::begin(['title' => 'Регистрация участника']) ?>

        <table class="table">
            <tr>
                <th>Участник зарегистрирован</th>
                <td><?= Yii::$app->formatter->asBoolean($model->identity_id) ?></td>
            </tr>
            <?php if ($model->identity_id): ?>
                <tr>
                    <td colspan="2">

                        <a href="<?= Url::to(['/profiles/identities/delete', 'id' => $model->identity_id]) ?>"
                           class="btn btn-danger">
                            Удалить регистрацию участника
                        </a>

                        <span class="label label-info">Все данные участника, включая его баланс, сохранятся</span>
                    </td>
                </tr>
            <?php endif ?>

            <?php if ($model->isNewRecord == false): ?>
                <?php if ($model->blocked_at): ?>
                    <tr>
                        <th>Причина блокировки</th>
                        <td><?= Html::encode($model->blocking_reason) ?></td>
                    </tr>
                <?php endif ?>
                <tr>
                    <td colspan="2">
                        <?php if ($model->blocked_at): ?>
                            <a href="<?= Url::to(['unblock', 'id' => $model->id]) ?>"
                               class="btn btn-danger">
                                Разблокировать участника
                            </a>
                        <?php else: ?>
                            <a href="<?= Url::to(['block', 'id' => $model->id]) ?>"
                               class="btn btn-danger">
                                Заблокировать участника
                            </a>
                        <?php endif ?>
                    </td>
                </tr>
            <?php endif ?>
        </table>

        <?php Box::end() ?>

        <?php Box::begin(['title' => 'Дополнительная информация']) ?>

        <?php if ($model->isNewRecord == false): ?>

            <div class="panel panel-info">
                <div class="panel-heading">
                    Информация
                </div>
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <th>Баллы</th>
                            <td><a href="<?= Url::to(['profile-transactions/index',
                                    'ProfileTransactionSearch[purse__owner_id]' => $model->id,
                                ]) ?>"><?= Html::encode($model->purse->balance) ?></a></td>
                        </tr>
                    </table>
                </div>
            </div>

        <?php endif ?>

        <?php Box::end() ?>

        <?= $this->render('partials/profile-sidebar', compact('model')) ?>
    </div>


</div>
