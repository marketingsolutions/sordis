<?php

use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;
use modules\profiles\common\models\PremiumHistory;

/**
 * @var yii\web\View $this
 * @var modules\profiles\common\models\Premium $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php  $box = FormBox::begin(['cssClass' => 'premium-form box-primary', 'title' => '']) ?>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Номер заказа</th>
        <th>Дата списания баллов</th>
        <th>Сумма премии</th>
        <th>Сумма потраченных баллов</th>
        <th>Остаток</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?=$model->id?></td>
        <td><?=date("d.m.Y H:i", strtotime($model->created_at))?></td>
        <td><?=$model->premium_sum?></td>
        <td><?=$model->bonus_sum?></td>
        <td><?=$model->remainder_sum?></td>
    </tr>
    </tbody>
</table>
<?php  FormBox::end() ?>

<?php if(PremiumHistory::getCompensationTable($model->id)):?>
    <?php  $box = FormBox::begin(['cssClass' => 'premium-form box-primary', 'title' => 'Начисления компенсаций']) ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Номер заказа</th>
                <th>Сумма компенсации, руб.</th>
                <th>Дополнительное соглашение (№, дата) </th>
                <th>Акт (номер, дата) </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach (PremiumHistory::getCompensationTable($model->id) as $comp):?>
            <tr>
                <td><?=$comp['document_id']?></td>
                <td><?=$comp['premium_sum']?></td>
                <td><?=$comp['document_number']?></td>
                <td><?=$comp['document_act']?></td>
            </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    <?php  FormBox::end() ?>
<?php endif;?>

<?php  $box = FormBox::begin(['cssClass' => 'premium-form box-primary', 'title' => '']) ?>
<?php $form = ActiveForm::begin([
    'id' => 'premium-form',
    'options' => ['class' => 'form-horizontal'],
]) ?>
<?= $form->field($premiumModel, 'order_id')->hiddenInput(['value' => \Yii::$app->request->get('id')])->label(false) ?>
<?= $form->field($premiumModel, 'document_id') ?>
<?= $form->field($premiumModel, 'premium_sum') ?>
<?= $form->field($premiumModel, 'document_number') ?>
<?= $form->field($premiumModel, 'document_act') ?>

<?php $box->actions([
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
]) ?>
<?php ActiveForm::end() ?>
<?php  FormBox::end() ?>