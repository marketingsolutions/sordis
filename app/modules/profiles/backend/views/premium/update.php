<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\widgets\ActionButtons;

/**
 * @var yii\web\View $this
 * @var modules\profiles\common\models\Premium $model
 */
$this->title = "Рассчитать премию";
$this->params['breadcrumbs'][] = ['label' => modules\profiles\common\models\Premium::modelTitlePlural(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<div class="premium-update">

    <div class="text-right">
        <?php  Box::begin() ?>
        <?=  ActionButtons::widget([
            'order' => [[/*'index',*/ 'return']],
            'addReturnUrl' => '/profiles/premium/index?id='.$model->id,
        ]) ?>
        <?php  Box::end() ?>
    </div>

    <?php echo $this->render('_form', [
        'model' => $model,
        'premiumModel' => $premiumModel,
    ]); ?>

</div>
