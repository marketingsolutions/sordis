<?php
use yz\admin\widgets\Box;
use yii\helpers\Html;


$this->title = 'Правила для РТТ и для Дистрибьютера';
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>


<?php $box = Box::begin(['cssClass' => 'region-index box-primary']) ?>

<div class="row">

    <div class="col-md-3">
        <?=Html::a('Правила для РТТ', '@frontendWeb/media/uploads/rules_rtt.pdf', ['target' => '_blank', 'class' =>'btn bth-primary btn-success'])?>
    </div>

    <div class="col-md-3">
        <?=Html::a('Правила для Дистрибьюторов', '@frontendWeb/media/uploads/rules_distr.pdf', ['target' => '_blank', 'class' =>'btn bth-primary btn-success'])?>
    </div>
</div>

<?php Box::end() ?>


