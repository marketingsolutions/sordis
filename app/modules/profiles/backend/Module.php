<?php

namespace modules\profiles\backend;

use modules\profiles\backend\models\AllowPersonalInfoSearch;
use modules\profiles\backend\models\ProfileReqSearch;
use modules\profiles\backend\rbac\Rbac;
use yz\icons\Icons;

/**
 * Class Module
 */
class Module extends \modules\profiles\common\Module
{
    public function getName()
    {
        return 'Профили участников';
    }

    public function getAdminMenu()
    {
        return [
            [
                'label' => 'Участники программы',
                'icon' => Icons::o('user'),
                'items' => [
                    [
                        'route' => ['/profiles/profiles/index'],
                        'label' => 'Профили участников РТТ',
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/profiles/profiles-distrib/index'],
                        'label' => 'Профили Дистрибьюторов',
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/profiles/profile-transactions/index'],
                        'label' => 'Движение баллов',
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/profiles/rules/index'],
                        'label' => 'Правила для РТТ и для Дистрибьютера',
                        'icon' => Icons::o('list'),
                    ],
                ]
            ],
            [
                'label' => 'Заявки участников на добавление РТТ и редактирование профиля',
                'icon' => Icons::o('user'),
                'items' => [
                    [
                        'route' => ['/profiles/profile-req/index'],
                        'label' => 'Заявки участников на добавление/удаление' . ProfileReqSearch::getCountConfirmRtt() ,
                        'icon' => Icons::o('user'),
                    ],
                    [
                        'route' => ['/profiles/allow-personal-info/index'],
                        'label' => 'Заявки на редактирование профиля' . AllowPersonalInfoSearch::getCountAllow(),
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/profiles/profile-confirm-rtt/index'],
                        'label' => 'Список всех добавленных РТТ',
                        'icon' => Icons::o('list'),
                    ],
                ]
            ],
//            [
//                'label' => 'Скидки и премии',
//                'icon' => Icons::o('archway'),
//                'items' => [
//                    [
//                        'route' => ['/profiles/discount/index'],
//                        'label' => 'Скидки',
//                        'icon' => Icons::o('percent'),
//                    ],
//                    [
//                        'route' => ['/profiles/premium/index'],
//                        'label' => 'Премии',
//                        'icon' => Icons::o('dollar'),
//                    ],
//                ]
//            ],
        ];
    }

    public function getAuthItems()
    {
        return array_merge(parent::getAuthItems(), Rbac::dependencies());
    }
}