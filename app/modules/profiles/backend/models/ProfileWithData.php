<?php

namespace modules\profiles\backend\models;

use common\utils\Notifier;
use modules\profiles\common\models\Action;
use modules\profiles\common\models\Admin;
use modules\profiles\common\models\Profile;
use marketingsolutions\finance\models\Purse;
use modules\profiles\common\models\Region;
use yii\db\ActiveQuery;
use yz\admin\models\User;
use yz\admin\search\WithExtraColumns;

/**
 * Class ProfileWithData
 */
class ProfileWithData extends Profile
{
    use WithExtraColumns;

    protected static function extraColumns()
    {
        return [
            'purse__balance',
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), static::extraColumns());
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'purse__balance' => 'Баланс участника',
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public static function find()
    {
        $query = parent::find()
            ->select(static::selectWithExtraColumns(['profile.*']))
            ->from(['profile' => self::tableName()])
            ->joinWith('purse purse');
        $query->where(['profile.role' => Profile::ROLE_MANAGER_TT]);

        return $query;
    }
}