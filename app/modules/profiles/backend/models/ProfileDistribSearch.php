<?php

namespace modules\profiles\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;

/**
 * ProfileSearch represents the model behind the search form about `ms\loyalty\profiles\simple\common\models\Profile`.
 */
class ProfileDistribSearch extends ProfileDistribWithData implements SearchModelInterface
{

    public $discountSum;

    public $premiumSum;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['first_name', 'last_name', 'middle_name', 'full_name', 'phone_mobile', 'role', 'email', 'created_at', 'updated_at', 'distrib_name', 'discountSum', 'premiumSum'], 'safe'],
            [static::extraColumns(), 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        return ProfileDistribWithData::find();
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'profile.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'profile.first_name', $this->first_name])
            ->andFilterWhere(['like', 'profile.last_name', $this->last_name])
            ->andFilterWhere(['like', 'profile.middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'profile.full_name', $this->full_name])
            ->andFilterWhere(['{{%discount}}.remainder_sum' => $this->discountSum])
            ->andFilterWhere(['{{%premium}}.remainder_sum' =>  $this->premiumSum])
            ->andFilterWhere(['like', 'profile.phone_mobile', $this->phone_mobile])
            ->andFilterWhere(['like', 'profile.distrib_name', $this->distrib_name])
            ->andFilterWhere(['like', 'profile.email', $this->email]);

        static::filtersForExtraColumns($query);
    }
}
