<?php

namespace modules\profiles\backend\models;

use marketingsolutions\finance\models\Transaction;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;


/**
 * Class ProfileTransactionSearch
 */
class ProfileTransactionSearch extends ProfileTransaction implements SearchModelInterface
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['type', 'amount', 'balance_before', 'balance_after', 'title', 'comment'], 'safe'],
            [static::extraColumns(), 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        return ProfileTransaction::find();
    }


    /*Остаток баллов на начало периода*/
    public static function getRemainderBalanceStartPeriod($profileId, $period)
    {
        $dateFrom = date("Y-m-01 00:00:00" , strtotime($period));
        $dateTo = date("Y-m-t 23:59:59");

        $query = Transaction::find()
            ->select('{{%finance_transactions}}.balance_before as balance_start_period')
            ->leftJoin('{{%finance_purses}}' , '{{%finance_purses}}.id={{%finance_transactions}}.purse_id')
            ->where([ '>=','{{%finance_transactions}}.created_at',  $dateFrom])
            ->andWhere([ '<=','{{%finance_transactions}}.created_at',  $dateTo])
            ->andWhere([ '{{%finance_purses}}.owner_id' => $profileId])
            ->orderBy(['{{%finance_transactions}}.id' => SORT_ASC])
            ->limit(1)
            ->asArray()
            ->one();
        if(empty($query)){
            return 0;
        }

        return $query['balance_start_period'];
    }

    /**
     * Потрачено баллов за период
     * @param $profileId
     * @param null $period
     * @return int|mixed
     */
    public static function getOutBalance($profileId, $period = null){


        if($period == null) {
            $query =Transaction::find()
                ->leftJoin('{{%finance_purses}}', '{{%finance_purses}}.id={{%finance_transactions}}.purse_id')
                ->where(['{{%finance_transactions}}.type' => Transaction::OUTBOUND])
                ->andWhere(['{{%finance_purses}}.owner_id' => $profileId])
                ->sum('{{%finance_transactions}}.amount');
        }else{
            $query =Transaction::find()
                ->leftJoin('{{%finance_purses}}', '{{%finance_purses}}.id={{%finance_transactions}}.purse_id')
                ->where(['{{%finance_transactions}}.type' => Transaction::OUTBOUND])
                ->andWhere(['{{%finance_purses}}.owner_id' => $profileId])
                ->andWhere([ '>=','{{%finance_transactions}}.created_at', date("Y-m-01 00:00:00", strtotime($period))])
                ->andWhere([ '<=','{{%finance_transactions}}.created_at', date("Y-m-t 23:59:59", strtotime($period))])
                ->sum('{{%finance_transactions}}.amount');
        }
//        print_r(date("Y-m-01 00:00:00", strtotime($period)));
//        print_r("!!!!!!");
//        print_r($period);
//        exit;
        if($query){
            return $query;
        }

        return 0;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->defaultOrder = [
            'created_at' => SORT_DESC,
        ];
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
        ]);

        static::filtersForExtraColumns($query);

    }
}