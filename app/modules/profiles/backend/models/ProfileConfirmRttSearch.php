<?php

namespace modules\profiles\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\profiles\common\models\ProfileConfirmRtt;

/**
 * ProfileConfirmRttSearch represents the model behind the search form about `modules\profiles\common\models\ProfileConfirmRtt`.
 */
class ProfileConfirmRttSearch extends ProfileConfirmRtt implements SearchModelInterface

{
    /**
     * @var
     */
    public $profilePhone;
    /**
     * @var
     */
    public $profileName;
    /**
     * @var
     */
    public $profileMail;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'profile_id'], 'integer'],
            [['code_egais', 'inn', 'name', 'address', 'license_number', 'add_delele_at' , 'type','work_time_from', 'work_time_to', 'created_at', 'status', 'is_view', 'profilePhone', 'profileMail', 'profileName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        return ProfileConfirmRtt::find()
            ->joinWith('profile');
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder'=>['id' => SORT_DESC]]
        ]);
        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'profilePhone' => [
                'asc' => ['{{%profiles}}.phone_mobile' => SORT_ASC],
                'desc' => ['{{%profiles}}.phone_mobile' => SORT_DESC]
            ],
            'profileName' => [
                'asc' => ['{{%profiles}}.full_name' => SORT_ASC],
                'desc' => ['{{%profiles}}.full_name' => SORT_DESC]
            ],
            'profileMail' => [
                'asc' => ['{{%profiles}}.email' => SORT_ASC],
                'desc' => ['{{%profiles}}.email' => SORT_DESC]
            ],
        ]);
        $dataProvider->setSort($sort);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'id' => $this->id,
            'work_time_from' => $this->work_time_from,
            'work_time_to' => $this->work_time_to,
            'profile_id' => $this->profile_id,
        ]);

        $query->andFilterWhere(['like', 'code_egais', $this->code_egais])
            ->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'license_number', $this->license_number])
            ->andFilterWhere(['like', '{{%profiles}}.phone_mobile', $this->profilePhone])
            ->andFilterWhere(['like', '{{%profiles}}.email', $this->profileMail])
            ->andFilterWhere(['like', '{{%profiles}}.full_name', $this->profileName])
            //->andFilterWhere(['between', 'created_at', Yii::$app->formatter->asDate($this->created_at, 'YYYY-MM-d 00:00:00'), Yii::$app->formatter->asDate($this->created_at, 'YYYY-MM-d 23:59:59')])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'is_view', $this->is_view]);

        if(\Yii::$app->request->get('ProfileConfirmRttSearch')['created_at'] && \Yii::$app->request->get('ProfileConfirmRttSearch')['created_at'] != ""){
            $query->andWhere(['>=', '{{%profile_confirm_rtt}}.created_at', date('Y-m-d 00:00:00', strtotime(\Yii::$app->request->get('ProfileConfirmRttSearch')['created_at']))]);
            $query->andWhere(['<=', '{{%profile_confirm_rtt}}.created_at', date('Y-m-d 23:59:59', strtotime(\Yii::$app->request->get('ProfileConfirmRttSearch')['created_at']))]);
        }

        if(\Yii::$app->request->get('ProfileConfirmRttSearch')['add_delele_at'] && \Yii::$app->request->get('ProfileConfirmRttSearch')['add_delele_at'] != ""){
            $query->andWhere(['>=', '{{%profile_confirm_rtt}}.add_delele_at', date('Y-m-d 00:00:00', strtotime(\Yii::$app->request->get('ProfileConfirmRttSearch')['add_delele_at']))]);
            $query->andWhere(['<=', '{{%profile_confirm_rtt}}.add_delele_at', date('Y-m-d 23:59:59', strtotime(\Yii::$app->request->get('ProfileConfirmRttSearch')['add_delele_at']))]);
        }

    }
}
