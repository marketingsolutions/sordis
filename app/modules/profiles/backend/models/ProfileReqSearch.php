<?php

namespace modules\profiles\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\profiles\common\models\ProfileConfirmRtt;

/**
 * ProfileReqSearch represents the model behind the search form about `modules\profiles\common\models\ProfileConfirmRtt`.
 */
class ProfileReqSearch extends ProfileConfirmRtt implements SearchModelInterface
{
    /**
     * @var
     */
    public $profilePhone;
    /**
     * @var
     */
    public $profileName;
    /**
     * @var
     */
    public $profileMail;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'profile_id'], 'integer'],
            [['code_egais', 'inn', 'name', 'address', 'license_number', 'work_time_from', 'work_time_to', 'created_at', 'status', 'is_view', 'profilePhone', 'profileName', 'profileMail'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        return ProfileConfirmRtt::find()
            ->joinWith('profile')
            ->groupBy('profile_id')
            ->where(['{{%profile_confirm_rtt}}.status'=> ProfileConfirmRtt::STATUS_NEW])
            ;
    }

    public static function getCountConfirmRtt()
    {
        $count =  ProfileConfirmRtt::find()
            ->joinWith('profile')
            ->groupBy('profile_id')
            ->where(['{{%profile_confirm_rtt}}.status'=> ProfileConfirmRtt::STATUS_NEW])
            ->count().'</span>';
        if($count != 0 ){
            return ' &nbsp;&nbsp;&nbsp;<span style="background-color: red;" class="badge badge-warning">'.$count.'</span>';
        }
        return '';
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'profilePhone' => [
                'asc' => ['{{%profiles}}.phone_mobile' => SORT_ASC],
                'desc' => ['{{%profiles}}.phone_mobile' => SORT_DESC]
            ],
            'profileName' => [
                'asc' => ['{{%profiles}}.full_name' => SORT_ASC],
                'desc' => ['{{%profiles}}.full_name' => SORT_DESC]
            ],
            'profileMail' => [
                'asc' => ['{{%profiles}}.email' => SORT_ASC],
                'desc' => ['{{%profiles}}.email' => SORT_DESC]
            ],
        ]);
        $dataProvider->setSort($sort);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'id' => $this->id,
            'work_time_from' => $this->work_time_from,
            'work_time_to' => $this->work_time_to,
            'profile_id' => $this->profile_id,
        ]);

        $query->andFilterWhere(['like', '{{%profiles}}.phone_mobile', $this->profilePhone])
            ->andFilterWhere(['like', '{{%profiles}}.full_name', $this->profileName])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'is_view', $this->is_view]);

    }
}
