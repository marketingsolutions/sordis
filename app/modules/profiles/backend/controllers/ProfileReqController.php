<?php

namespace modules\profiles\backend\controllers;

use Yii;
use modules\profiles\common\models\ProfileConfirmRtt;
use modules\profiles\backend\models\ProfileReqSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * ProfileReqController implements the CRUD actions for ProfileConfirmRtt model.
 */
class ProfileReqController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var ProfileReqSearch $searchModel */
                    return Yii::createObject(ProfileReqSearch::className());
                },
                'dataProvider' => function($params, ProfileReqSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all ProfileConfirmRtt models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var ProfileReqSearch $searchModel */
        $searchModel = Yii::createObject(ProfileReqSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(ProfileReqSearch $searchModel)
    {
        return [
            [
                'attribute' => 'profile_id',
                'label' => 'ID участника',
            ],
			[
                'attribute' => 'profilePhone',
                'label' => 'Телефон',
            ],
            [
                'attribute' => 'profileName',
                'label' => 'ФИО',
            ],
			[
                'attribute' => 'profileMail',
                'label' =>  'E-mail',
            ],
            [
                'label' => 'Кол-во точек для добавления',
                'value' => function($data){
                    return ProfileConfirmRtt::getRttCount($data->profile_id);
                }
            ],
            [
                'label' => 'Добавить/удалить точки',
                'format' => 'html',
                'value' => function($data){
                    return '<a class="" href="/profiles/profiles/update?id='.$data->profile_id.'"><span style="font-size: 12px;" class="label label-success">Перейти к добавлению/удалению &nbsp;&nbsp;<i class="glyphicon glyphicon-arrow-right" ></i></span></a>';
                }
            ],
        ];
    }

    /**
     * Creates a new ProfileConfirmRtt model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProfileConfirmRtt;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProfileConfirmRtt model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
        ]);
	}


    /**
     * Deletes an existing ProfileConfirmRtt model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProfileConfirmRtt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProfileConfirmRtt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProfileConfirmRtt::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
