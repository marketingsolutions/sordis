<?php

namespace modules\profiles\backend\controllers;

use backend\base\Controller;
use modules\profiles\backend\models\ProfileTransactionSearch;
use marketingsolutions\finance\models\Transaction;
use Yii;
use yz\admin\actions\ExportAction;
use yz\admin\grid\columns\DataColumn;


/**
 * Class ProfileTransactionsController
 */
class ProfileTransactionsController extends Controller
{
    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'dataProvider' => function ($params) {
                    $searchModel = Yii::createObject(ProfileTransactionSearch::class);
                    $dataProvider = $searchModel->search($params);
                    return $dataProvider;
                },
            ]
        ]);
    }

    public function actionIndex()
    {
        /** @var ProfileTransactionSearch $searchModel */
        $searchModel = Yii::createObject(ProfileTransactionSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns()
    {
        return [
            'id',
            [
                'attribute' => 'type',
                'filter' => Transaction::getTypeValues(),
                'titles' => Transaction::getTypeValues(),
                'labels' => [
                    Transaction::INCOMING => DataColumn::LABEL_SUCCESS,
                    Transaction::OUTBOUND => DataColumn::LABEL_DANGER,
                ]
            ],
            'profile__full_name',
            'profile__phone_mobile',
            'amount',
            'balance_before',
            'balance_after',
            'title',
            'comment',
            'created_at:datetime',
        ];
    }
}