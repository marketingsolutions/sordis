<?php

namespace modules\profiles\backend\controllers;

use common\utils\Notifier;
use modules\manual\common\finances\BackendUserPartner;
use modules\profiles\backend\models\ProfileSearch;
use modules\profiles\common\models\Dealer;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\ProfileConfirmRtt;
use modules\sales\common\models\SalePoint;
use modules\sales\common\models\SalePointProfile;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yz\admin\actions\ExportAction;
use yz\admin\contracts\AccessControlInterface;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yii\helpers\Html;
use yz\icons\Icons;
use yii\web\Response;
use modules\profiles\backend\forms\AddRttForm;

/**
 * ProfilesController implements the CRUD actions for Profile model.
 */
class ProfilesController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'dataProvider' => function ($params) {
                    $searchModel = Yii::createObject(ProfileSearch::class);
                    $dataProvider = $searchModel->search($params);
                    return $dataProvider;
                },
            ]
        ]);
    }

    /**
     * Lists all Profile models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var ProfileSearch $searchModel */
        $searchModel = Yii::createObject(ProfileSearch::class);
        /** @var ActiveDataProvider $dataProvider */
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $dataProvider->setSort(['defaultOrder' => ['id' => SORT_ASC]]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns(),
        ]);
    }

    public function getGridColumns()
    {
        $adminColumn =  [
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width:60px;'],
            ],
            'phone_mobile',
            [
                'attribute' => 'full_name',
                'contentOptions' => ['style' => 'width:160px;'],
            ],
            [
                'attribute' => 'rtt_name',
                'format' => 'html',
            ],
            'email:email',
            'purse__balance',
            ];
        $superAdminColumn = [
            [
                'label' => 'Блокировка участника',
                'format' => 'raw',
                'contentOptions' => ['style' => 'width:140px;'],
                'value' => function (Profile $model) {
                    if ($model->blocked_at) {
                        return 'заблокирован ' . (new \DateTime($model->blocked_at))->format('d.m.Y') . '<br/>' .
                            Html::a(
                                'разблокировать',
                                ['unblock', 'id' => $model->id],
                                ['title' => 'Снять частичную блокировку', 'class' => 'user_unblock']
                            );
                    }
                    else {
                        return Html::a(
                            'блокировать',
                            ['block', 'id' => $model->id],
                            ['title' => 'Частичная блокировка участника', 'class' => 'user_block']
                        );
                    }
                }
            ],
            [
                'label' => 'Ручное начисление баллов',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Icons::i('rub'), ['/manual/manage-bonuses/index', 'id' => $model->id], [
                        'class' => 'btn btn-default btn-sm',
                        'title' => 'Изменить баланс участника',
                    ]);
                }
            ],
        ];

        if(Notifier::checkRole($admin=null,'MANAGER_TO')){
            return $adminColumn;
        }else{
            return array_merge($adminColumn, $superAdminColumn);
        }
    }

    /**
     * Creates a new Profile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Profile;

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
            return $this->getCreateUpdateResponse($model);
        }
        else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Profile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $salePoint = Profile::getMySalePoint($id);
        $formRtt = new AddRttForm();
        $confirmRtt = ProfileConfirmRtt::getMyConfirmDeleteRtt($id);
        $deleteRtt = ProfileConfirmRtt::getMyConfirmDeleteRtt($id, ProfileConfirmRtt::TYPE_DELETE);
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
            return $this->getCreateUpdateResponse($model);
        }
        else {
            return $this->render('update', [
                'model' => $model,
                'salePoint' => $salePoint,
                'formRtt' => $formRtt,
                'confirmRtt' => $confirmRtt,
                'deleteRtt' => $deleteRtt,
            ]);
        }
    }

    /**
     * Finds the Profile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Profile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Profile::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing Profile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array) $id;

        foreach ($id as $id_) {
            $this->findModel($id_)->delete();
        }

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Полная блокировка участника
     *
     * @param $id
     * @param string $reason
     * @return \yii\web\Response
     */
    public function actionBlock($id, $reason = '')
    {
        $model = $this->findModel($id);
        $model->block($reason);

        return $this->goPreviousUrl();
    }

    public function actionUnblock($id)
    {
        $model = $this->findModel($id);
        $model->unblock();

        return $this->goPreviousUrl();
    }

    public function actionLogin($id)
    {
        $profile = $this->findModel($id);
        //Добавляем  параметр для роли Менеджер ТО для запрещения доступа к некоторым разделам в ЛК
        if (Profile::isManagerTo()){
            $managerTo = true;
        }else{
            $managerTo = false;
        }

        $url = Url::to([
            '/' . \Yii::getAlias('@frontendWeb/site/login'),
            'id' => $profile->id,
            'hash' => md5($profile->id),
            'managerTo' => $managerTo,
        ]);
        $url = substr($url, 1);

        return $this->redirect($url);
    }

    public function actionFile()
    {
        $path = Yii::getAlias('@modules/profiles/common/files/profiles.xlsx');

        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        }

        throw new NotFoundHttpException();
    }

    /**
     * Удаление РТТ через Ajax
     * @return array
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteRtt(){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $deleteId =  Yii::$app->request->post('id');
        $model = SalePointProfile::findOne(['id' => $deleteId]);
        if($this->isOneRtt($model->profile_id)){
            $rtt = SalePointProfile::findOne(['id' => $deleteId]);
            if($rtt->delete()) {
                $salePoint = SalePoint::findOne($model->sale_point_id);
                $salePoint->registred_at = null;
                $salePoint->save(false);
                return ['result' => 'OK'];
            }else{
                return $this->error('Ошибка при удалении');
            }
        }else{
            return $this->error('Нельзя удалять все РТТ у участника! У участника должнабыть минимум одна РТТ');
        }

    }

    /**
     * Добавление РТТ через Ajax
     * @return array
     */
    public function actionAddRtt(){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $addRttId =  Yii::$app->request->post('id');
        $userId =  Yii::$app->request->post('user_id');
        if($this->isRtt($addRttId)) {
            $newRtt = new SalePointProfile();
            $newRtt->sale_point_id = $addRttId;
            $newRtt->profile_id = $userId;
                if($newRtt->save()){
                    $salePoint = SalePoint::findOne(['id' => $addRttId]);
                    Profile::checkRttFromConfirm($salePoint->code_egais, $userId);
                    $salePoint->registred_at = date("Y-m-d H:i:s");
                    $salePoint->save(false);
                    return [
                        'result' => 'OK',
                        'id_sale_point'=> $newRtt->id,
                        'name' => $salePoint->name,
                        'address' => $salePoint->address,
                        'license_number' => $salePoint->license_number,
                        'code_egais' => $salePoint->code_egais,
                        'inn' => $salePoint->inn,
                        'kpp' => $salePoint->kpp,
                        'time' => date("H:i", strtotime($salePoint->work_time_from)).' - '.date("H:i", strtotime($salePoint->work_time_to)),
                    ];

                }else{
                    return $this->error('Ошибка при сохранении РТТ!');
                }
        }else{
            return $this->error('Данная РТТ занята другим участником.');
        }
    }

    /**
     * Добавление выделенных РТТ через Ajax
     * @return array
     */
    public function actionAddAllRtt()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $userArrId = [];
        $userArrId = Yii::$app->request->post('id');
        $profileId = Yii::$app->request->post('user_id');
        //Проверяем по егаис есть ли вообще такие РТТ в базе
        $isRtt = ProfileConfirmRtt::isAllRtt($userArrId, $profileId);
        if ($isRtt) {
            $badEgais = implode(", ", $isRtt);
            return $this->error('Торговые точки с кодом ЕГАИС -'.$badEgais.' отсутствуют в системе!');
        } else {
            $isRttInUse = ProfileConfirmRtt::isArrRtt($userArrId, $profileId);
            if($isRttInUse){
                $badRttInUse = implode(", ", $isRttInUse);
                return $this->error('Торговые точки с кодом ЕГАИС - '.$badRttInUse.' заняты другими участниками!');
            }else{
                $newRtt = ProfileConfirmRtt::addValidRtt($userArrId, $profileId);
                if(!$newRtt){
                    return $this->error('Ошибка в добавлении торговой точки');
                }else{
                    $arrReturnRtt = ProfileConfirmRtt::getRttAfterAdd($userArrId);
                    return [
                        'result' => 'OK',
                        'id_deleted' => $arrReturnRtt,
                    ];
                }
            }
        }
    }

    public function actionDeclineAllRtt(){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $rttArrId = [];
        $rttArrId = Yii::$app->request->post('id');
        $profileId = Yii::$app->request->post('user_id');
        if(ProfileConfirmRtt::declineRtt($rttArrId, $profileId)){
            return [
                'result' => 'OK',
            ];
        }else{
            return $this->error('Ошибка в отмене добавления торговой точки');
        }

    }

    public function actionDeleteAllRtt(){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $rttArrId = [];
        $rttArrId = Yii::$app->request->post('id');
        $profileId = Yii::$app->request->post('user_id');
        $del = ProfileConfirmRtt::deleteRtt($rttArrId, $profileId);
        if($del){
            return [
                'result' => 'OK',
                'sale_point_profile_id' => $del,
            ];
        }else{
            return $this->error('Ошибка в отмене добавления торговой точки');
        }
    }

    public function actionCancelAllRtt(){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $rttArrId = [];
        $rttArrId = Yii::$app->request->post('id');
        $profileId = Yii::$app->request->post('user_id');
        $cansel = ProfileConfirmRtt::cancelRtt($rttArrId, $profileId);
        if($cansel){
            return [
                'result' => 'OK',
                'sale_point_profile_id' => $cansel,
            ];
        }else{
            return $this->error('Ошибка при отклонении удаления торговой точки');
        }
    }

    /**
     * @param $profileId
     * @return bool
     */
    public function isOneRtt($profileId){
        $model = SalePointProfile::find()->where(['profile_id' => $profileId])->count();
        if($model && $model > 1){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Проверка, занята ли РТТ другим участником
     * @param $rttId
     * @return bool
     */
    public function isRtt($rttId){
        $model = SalePointProfile::findOne(['sale_point_id' => $rttId]);
        if($model){
            return false;
        }else{
            return true;
        }
    }

    private function error($errorMsg)
    {
        \Yii::$app->response->statusCode = 400;
        return ['result' => 'FAIL', 'errors' => [$errorMsg]];
    }
}
