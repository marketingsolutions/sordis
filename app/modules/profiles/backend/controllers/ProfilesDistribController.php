<?php

namespace modules\profiles\backend\controllers;

use common\utils\Notifier;
use modules\manual\common\finances\BackendUserPartner;
use modules\profiles\backend\models\ProfileDistribSearch;
use modules\profiles\common\models\Dealer;
use modules\profiles\common\models\Profile;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yz\admin\actions\ExportAction;
use yz\admin\contracts\AccessControlInterface;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yii\helpers\Html;
use yz\icons\Icons;

/**
 * ProfilesController implements the CRUD actions for Profile model.
 */
class ProfilesDistribController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'dataProvider' => function ($params) {
                    $searchModel = Yii::createObject(ProfileDistribSearch::class);
                    $dataProvider = $searchModel->search($params);
                    return $dataProvider;
                },
            ]
        ]);
    }

    /**
     * Lists all Profile models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var ProfileDistribSearch $searchModel */
        $searchModel = Yii::createObject(ProfileDistribSearch::class);
        /** @var ActiveDataProvider $dataProvider */
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $dataProvider->setSort(['defaultOrder' => ['id' => SORT_ASC]]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns(),
        ]);
    }

    public function getGridColumns()
    {
        $adminColumn =  [
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width:60px;'],
            ],
            'phone_mobile',
            [
                'attribute' => 'full_name',
                'contentOptions' => ['style' => 'width:160px;'],
            ],
            [
                'attribute' => 'distrib_name',
                'format' => 'html',
            ],

            'email:email',
            'purse__balance',
            [
                'attribute' => 'discountSum',
                'label' => 'Скидка',
            ],
            [
                'label' => 'Рассчитать скидку',
                'format' => 'raw',
                'value' => function($model){
                    if($model->discountSum){
                        return "<a target='_blank' class='label label-success' href='/profiles/discount/index?id=".$model->id."' target='_blank'>РАССЧИТАТЬ</a>";
                    }
                    return null;
                }
            ],
            [
                'attribute' => 'premiumSum',
                'label' => 'Премия',
            ],
            [
                'label' => 'Рассчитать премию',
                'format' => 'raw',
                'value' => function($model){
                    if($model->premiumSum){
                        return "<a target='_blank' class='label label-success' href='/profiles/premium/index?id=".$model->id."' target='_blank' >РАССЧИТАТЬ</a>";
                    }
                    return null;
                }
            ],
        ];

        $superAdminColumn = [
            [
                'label' => 'Блокировка участника',
                'format' => 'raw',
                'contentOptions' => ['style' => 'width:140px;'],
                'value' => function (Profile $model) {
                    if ($model->blocked_at) {
                        return 'заблокирован ' . (new \DateTime($model->blocked_at))->format('d.m.Y') . '<br/>' .
                            Html::a(
                                'разблокировать',
                                ['unblock', 'id' => $model->id],
                                ['title' => 'Снять частичную блокировку', 'class' => 'user_unblock']
                            );
                    }
                    else {
                        return Html::a(
                            'блокировать',
                            ['block', 'id' => $model->id],
                            ['title' => 'Часичная блокировка участника', 'class' => 'user_block']
                        );
                    }
                }
            ],
                [
                    'label' => 'Ручное начисление баллов',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a(Icons::i('rub'), ['/manual/manage-bonuses/index', 'id' => $model->id], [
                            'class' => 'btn btn-default btn-sm',
                            'title' => 'Изменить баланс участника',
                        ]);
                    }
                ],
            ];

        if(Notifier::checkRole($admin=null,'MANAGER_TO')){
            return $adminColumn;
        }else{
            return array_merge($adminColumn, $superAdminColumn);
        }
    }

    /**
     * Creates a new Profile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Profile;

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
            return $this->getCreateUpdateResponse($model);
        }
        else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Profile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
            return $this->getCreateUpdateResponse($model);
        }
        else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Profile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Profile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Profile::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing Profile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array) $id;

        foreach ($id as $id_) {
            $this->findModel($id_)->delete();
        }

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Полная блокировка участника
     *
     * @param $id
     * @param string $reason
     * @return \yii\web\Response
     */
    public function actionBlock($id, $reason = '')
    {
        $model = $this->findModel($id);
        $model->block($reason);

        return $this->goPreviousUrl();
    }

    public function actionUnblock($id)
    {
        $model = $this->findModel($id);
        $model->unblock();

        return $this->goPreviousUrl();
    }

    public function actionLogin($id)
    {
        $profile = $this->findModel($id);

        $url = Url::to([
            '/' . \Yii::getAlias('@frontendWeb/site/login'),
            'id' => $profile->id,
            'hash' => md5($profile->id),
        ]);
        $url = substr($url, 1);

        return $this->redirect($url);
    }

    public function actionFile()
    {
        $path = Yii::getAlias('@modules/profiles/common/files/profiles.xlsx');

        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        }

        throw new NotFoundHttpException();
    }
}
