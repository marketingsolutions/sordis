<?php

namespace modules\profiles\backend\controllers;

use modules\profiles\backend\forms\AdminAllow;
use modules\profiles\common\models\Profile;
use modules\profiles\frontend\forms\AddAllow;
use Yii;
use modules\profiles\common\models\AllowPersonalInfo;
use modules\profiles\backend\models\AllowPersonalInfoSearch;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * AllowPersonalInfoController implements the CRUD actions for AllowPersonalInfo model.
 */
class AllowPersonalInfoController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var AllowPersonalInfoSearch $searchModel */
                    return Yii::createObject(AllowPersonalInfoSearch::className());
                },
                'dataProvider' => function($params, AllowPersonalInfoSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all AllowPersonalInfo models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var AllowPersonalInfoSearch $searchModel */
        $searchModel = Yii::createObject(AllowPersonalInfoSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(AllowPersonalInfoSearch $searchModel)
    {
        return [

            [
                'attribute' => 'profile_id',
                'label' => 'ID профиля участника'
            ],
            [
                'attribute' => 'profileFullName',
                'label' => 'ФИО участника'
            ],
            [
                'attribute' => 'profilePhone',
                'label' => 'Телефон участника'
            ],
            [
                'attribute' => 'profileRole',
                'label' => 'Роль участника',
                'format' => 'html',
                'value' => function($model){
                    return Profile::getDashboardRealRole()[$model->profileRole];
                },
                'filter' => function($model){
                    return Profile::getDashboardRealRole();
                },
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Заявка создана',
                'format' => ['date', 'php:d.m.Y H:i:m']
            ],
            [
                'attribute' => 'updated_at',
                'label' => 'Заявка обработана модератором',
                'format' => ['date', 'php:d.m.Y H:i:m']
            ],
        ];
    }

    /**
     * Creates a new AllowPersonalInfo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AllowPersonalInfo;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AllowPersonalInfo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $adminAllow = new AdminAllow();
		if ($model->load(\Yii::$app->request->post()) && $model->save() && $model->process()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
            'adminAllow' => $adminAllow
        ]);
	}


    /**
     * Deletes an existing AllowPersonalInfo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the AllowPersonalInfo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AllowPersonalInfo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AllowPersonalInfo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return Response
     */
    public function actionAdminAddAllow()
    {
        $model = new AdminAllow();
        if($model->load(\Yii::$app->request->post()) && $model->validate() && $model->process()){
            Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, 'Ответ успешно отправлен участнику');
        }else{
            Yii::$app->session->setFlash(\yz\Yz::FLASH_ERROR, 'Ошибка при отправке ответа участнику');
        }
        return $this->redirect(\Yii::$app->request->referrer);

    }
}
