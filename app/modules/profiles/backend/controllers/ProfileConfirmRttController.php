<?php

namespace modules\profiles\backend\controllers;

use Yii;
use modules\profiles\common\models\ProfileConfirmRtt;
use modules\profiles\backend\models\ProfileConfirmRttSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * ProfileConfirmRttController implements the CRUD actions for ProfileConfirmRtt model.
 */
class ProfileConfirmRttController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var ProfileConfirmRttSearch $searchModel */
                    return Yii::createObject(ProfileConfirmRttSearch::className());
                },
                'dataProvider' => function($params, ProfileConfirmRttSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all ProfileConfirmRtt models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var ProfileConfirmRttSearch $searchModel */
        $searchModel = Yii::createObject(ProfileConfirmRttSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(ProfileConfirmRttSearch $searchModel)
    {
        return [
            'id',
            [
                'attribute' => 'status',
                'value' => function($data){
                    if($data->status == ProfileConfirmRtt::STATUS_NEW) {
                        return '<span style="font-size: 13px" class="label label-warning">'.ProfileConfirmRtt::getStatus()[$data->status].'</span>';
                    }elseif($data->status == ProfileConfirmRtt::STATUS_CONFIRM){
                        return '<span style="font-size: 13px" class="label label-success">'.ProfileConfirmRtt::getStatus()[$data->status].'</span>';
                    }elseif ($data->status == ProfileConfirmRtt::STATUS_CANCEL){
                        return '<span style="font-size: 13px" class="label label-danger">'.ProfileConfirmRtt::getStatus()[$data->status].'</span>';
                    }elseif ($data->status == ProfileConfirmRtt::STATUS_DELETED){
                        return '<span style="font-size: 13px" class="label label-danger">'.ProfileConfirmRtt::getStatus()[$data->status].'</span>';
                    }
                },
                'filter'=>ProfileConfirmRtt::getStatus(),
                'options' => ['style' => 'width: 170px;'],
                'format' => 'html',
            ],
            [
                'attribute' => 'type',
                'options' => ['style' => 'width: 140px;'],
                'format' => 'html',
                'filter' => [
                    ProfileConfirmRtt::TYPE_ADD => 'Добавление',
                    ProfileConfirmRtt::TYPE_DELETE => 'Удаление',
                ],
                'value' => function($data){
                    if($data->type == ProfileConfirmRtt::TYPE_ADD){
                        return "Добавление";
                    }elseif($data->type == ProfileConfirmRtt::TYPE_DELETE){
                        return "Удаление";
                    }
                }

            ],
            [
                'attribute' => 'profilePhone',
                'label' => 'Телефон участника',
            ],
            [
                'attribute' => 'profileName',
                'label' => 'ФИО',
            ],
            [
                'attribute' => 'profileMail',
                'label' => 'E-mail участника',
            ],
            'name',
            'address',
            'code_egais',
            'inn',
            'license_number',
            'work_time_from',
            'work_time_to',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y H:i'],
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'дд.мм.гггг',
                ],
            ],
            [
                'attribute' => 'add_delele_at',
                'format' => ['date', 'php:d.m.Y H:i'],
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'дд.мм.гггг'
                ],
            ],

        ];
    }

    /**
     * Creates a new ProfileConfirmRtt model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProfileConfirmRtt;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
        ]);
	}


    /**
     * Deletes an existing ProfileConfirmRtt model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProfileConfirmRtt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProfileConfirmRtt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProfileConfirmRtt::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
