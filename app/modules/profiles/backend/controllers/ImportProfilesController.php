<?php

namespace modules\profiles\backend\controllers;

use backend\base\Controller;
use libphonenumber\PhoneNumberFormat;
use modules\profiles\common\models\Dealer;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\Region;
use modules\sales\common\models\Product;
use marketingsolutions\phonenumbers\PhoneNumber;
use yz\admin\import\BatchImportAction;
use yz\admin\import\ImportForm;
use yz\admin\import\InterruptImportException;


/**
 * Class ImportProfilesController
 */
class ImportProfilesController extends Controller
{
    const FIELD_FIRST_NAME = 'имя';
    const FIELD_LAST_NAME = 'фамилия';
    const FIELD_PHONE = 'телефон';
    const FIELD_EMAIL = 'email';
    const FIELD_REGION = 'регион';
    const FIELD_DEALER = 'дистрибьютор';

    public function actions()
    {
        return [
            'index' => [
                'class' => BatchImportAction::className(),
                'extraView' => '@modules/profiles/backend/views/import-profiles/index.php',
                'importConfig' => [
                    'availableFields' => [
                        self::FIELD_FIRST_NAME => 'Имя участника',
                        self::FIELD_LAST_NAME => 'Фамилия участника',
                        self::FIELD_PHONE => 'Номер телефона',
                        self::FIELD_EMAIL => 'Электронная почта участника',
                        self::FIELD_REGION => 'Регион',
                        self::FIELD_DEALER => 'Дистрибьютор',
                    ],
                    'rowImport' => [$this, 'rowImport'],
                    'skipFirstLine' => true,
                ]
            ]
        ];
    }

    public function rowImport(ImportForm $form, array $row)
    {
        if (empty($row[self::FIELD_PHONE])) {
            return;
        }

        if (PhoneNumber::validate($row[self::FIELD_PHONE], 'RU') == false) {
            throw new InterruptImportException('Неверный номер телефона: ' . $row[self::FIELD_PHONE], $row);
        }

        $region = $this->importRegion($row);
        $dealer = $this->importDealer($row, $region);
        $profile = $this->importProfile($row, $dealer);
    }

    /**
     * @param $row
     * @return Region|null|static
     * @throws InterruptImportException
     */
    private function importRegion($row)
    {
        $name = trim($row[self::FIELD_REGION]);
        $model = Region::findOne(['name' => $name]);

        if ($model === null) {
            $model = new Region();
            $model->name = $name;

            if ($model->save() == false) {
                throw new InterruptImportException('Ошибка при импорте региона: ' . implode(', ', $model->getFirstErrors()), $row);
            }
        }

        return $model;
    }

    /**
     * @param $row
     * @param Region $region
     * @return Dealer|null|static
     * @throws InterruptImportException
     */
    private function importDealer($row, Region $region)
    {
        $name = trim($row[self::FIELD_DEALER]);
        $model = Dealer::findOne(['name' => $name]);

        if ($model === null) {
            $model = new Dealer();
            $model->name = $name;
            $model->region_id = $region->id;

            if ($model->save() == false) {
                throw new InterruptImportException('Ошибка при импорте дистрибьютора: ' . implode(', ', $model->getFirstErrors()), $row);
            }
        }

        return $model;
    }

    /**
     * @param $row
     * @param Dealer $dealer
     * @return Profile|null|static
     * @throws InterruptImportException
     */
    private function importProfile($row, Dealer $dealer)
    {
        if (PhoneNumber::validate($row[self::FIELD_PHONE], 'RU') == false) {
            throw new InterruptImportException('Неверный номер телефона: ' . $row[self::FIELD_PHONE], $row);
        }

        $profile = Profile::findOne([
            'phone_mobile' => PhoneNumber::format($row[self::FIELD_PHONE], PhoneNumberFormat::E164, 'RU')
        ]);

        if ($profile === null) {
            $profile = new Profile();
            $profile->first_name = $row[self::FIELD_FIRST_NAME];
            $profile->last_name = $row[self::FIELD_LAST_NAME];
            $profile->phone_mobile_local = $row[self::FIELD_PHONE];
            $profile->email = $row[self::FIELD_EMAIL];
            $profile->dealer_id = $dealer->id;

            if ($profile->save() == false) {
                throw new InterruptImportException('Ошибка при импорте участника: ' . implode(', ', $profile->getFirstErrors()), $row);
            }
        }

        return $profile;
    }
}