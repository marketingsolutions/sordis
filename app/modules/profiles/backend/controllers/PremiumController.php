<?php

namespace modules\profiles\backend\controllers;

use modules\profiles\backend\forms\CalculatePremiunForm;
use Yii;
use modules\profiles\common\models\Premium;
use modules\profiles\backend\models\PremiumSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yz\admin\actions\ExportAction;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * PremiumController implements the CRUD actions for Premium model.
 */
class PremiumController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var PremiumSearch $searchModel */
                    return Yii::createObject(PremiumSearch::className());
                },
                'dataProvider' => function($params, PremiumSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all Premium models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var PremiumSearch $searchModel */
        $searchModel = Yii::createObject(PremiumSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(PremiumSearch $searchModel)
    {
        return [
			'id',
			'bonus_sum',
			'premium_sum',
			'remainder_sum',
			'created_at:datetime',
			// 'profile_id',
        ];
    }

    /**
     * Creates a new Premium model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Premium;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $premiumModel = new CalculatePremiunForm();
        if(\Yii::$app->request->post()) {
            if ($premiumModel->load(\Yii::$app->request->post()) && $premiumModel->validate() && $premiumModel->process()) {
                \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, 'Начисление сохранено успешно');
                return $this->getCreateUpdateResponse($model);
            }else{
                \Yii::$app->session->setFlash(\yz\Yz::FLASH_ERROR, \Yii::t('admin/t', 'Ошибка при начислении компенсации: '.$premiumModel->getErrors('premium_sum')[0]));
            }
        }

        return $this->render('update', [
            'model' => $model,
            'premiumModel' => $premiumModel,
        ]);
	}


    /**
     * Deletes an existing Premium model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Premium model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Premium the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Premium::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
