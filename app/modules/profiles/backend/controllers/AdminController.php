<?php

namespace modules\profiles\backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yz\admin\contracts\AccessControlInterface;
use yz\admin\models\User;

class AdminController extends Controller implements AccessControlInterface
{
    public function actionLoginSuperadmin()
    {
        $this->login(1, 'Вы вошли под cуперадминистратором. Вам открыты все разделы админки.');
    }

    public function actionLoginSupervisor()
    {
        $this->login(2, 'Вы вошли под супервайзером. Вам открыты все разделы админки, за исключением управления администраторскими записями и финансами, управление API');
    }

    public function actionLoginRegional()
    {
        $this->login(3, 'Вы вошли под региональным менеджером. Вам открыты продажи и участники Вашего региона');
    }

    private function login($adminId, $message)
    {
        $adminUser = User::findOne($adminId);

        if ($adminUser == null) {
            throw new NotFoundHttpException();
        }

        @\Yii::$app->user->logout();
        \Yii::$app->user->login($adminUser, 0);

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);
        return $this->redirect('/');
    }
}
