<?php

namespace modules\profiles\backend\controllers;

use modules\profiles\backend\forms\CalculateDiscountForm;
use Yii;
use modules\profiles\common\models\Discount;
use modules\profiles\backend\models\DiscountSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yz\admin\actions\ExportAction;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * DiscountController implements the CRUD actions for Discount model.
 */
class DiscountController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var DiscountSearch $searchModel */
                    return Yii::createObject(DiscountSearch::className());
                },
                'dataProvider' => function($params, DiscountSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all Discount models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var DiscountSearch $searchModel */
        $searchModel = Yii::createObject(DiscountSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(DiscountSearch $searchModel)
    {
        return [
			'id',
			'bonus_sum',
			'discount_sum',
			'remainder_sum',
			//'created_at:datetime',
			// 'profile_id',
        ];
    }

    /**
     * Creates a new Discount model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Discount;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Discount model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $discountModel = new CalculateDiscountForm();
        if (\Yii::$app->request->post()) {
            if ($discountModel->load(\Yii::$app->request->post()) && $discountModel->validate() && $discountModel->process()) {
                \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, 'Скидка успешно рассчитана');
                return $this->getCreateUpdateResponse($model);
            } else {
                \Yii::$app->session->setFlash(\yz\Yz::FLASH_ERROR, 'Ошибка при начислении: '.$discountModel->getErrors('discount_sum')[0]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'discountModel' => $discountModel,
        ]);
	}


    /**
     * Deletes an existing Discount model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Discount model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Discount the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Discount::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
