<?php
/**
 * Created by PhpStorm.
 * User: Mihon
 * Date: 30.09.2018
 * Time: 14:20
 */
namespace modules\profiles\backend\forms;

use modules\sales\common\models\SalePoint;
use modules\sales\common\models\SalePointProfile;
use yii\base\Model;


class AddRttForm extends Model
{
    public $salePoint;
    public $id_sale_point;

    public function rules()
    {
        return [
            ['salePoint', 'safe'],
            ['id_sale_point', 'integer'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'salePoint' => 'Выберите РТТ',
        ];
    }

    /**
     * @return array
     */
    public static function getAllRtt(){
        $rttBusy = SalePointProfile::find()->groupBy('sale_point_id')->asArray()->all();
        $arrRttBusy = [];
        foreach ($rttBusy as $rtt){
            $arrRttBusy[]=$rtt['sale_point_id'];
        }
        $salePoint =SalePoint::find()->select(['id as ids' , "CONCAT(name, ' | ЕГАИС: ',code_egais, ' | адрес: ',address) as label"])->where(['not in', 'id' , $arrRttBusy])->asArray()->all();
        return $salePoint;
    }
}