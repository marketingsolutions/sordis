<?php
namespace modules\profiles\backend\forms;
/**
 * Created by PhpStorm.
 * User: MihailKri
 * Date: 12.09.2018
 * Time: 13:42
 */
use modules\profiles\common\models\AllowPersonalInfo;
use modules\profiles\common\models\Profile;


use yii\base\Model;

class AdminAllow extends Model
{

    /**
     * @var
     */
    public $admin_message;
    /**
     * @var
     */
    public $id;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['admin_message'], 'required'],
            [['id'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'admin_message' => 'Ответ участнику от администратора',
        ];
    }

    public function process()
    {
        $model = AllowPersonalInfo::findOne($this->id);
        if(!$model){
            return false;
        }
        $model->admin_allow_message = $this->admin_message;
        $model->updated_at = date("Y-m-d H:i:s");
        if($model->save()){
            return true;
        }
        return false;
    }
}
