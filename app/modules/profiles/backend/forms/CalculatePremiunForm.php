<?php
/**
 * Created by PhpStorm.
 * User: Mihon
 * Date: 30.09.2018
 * Time: 14:20
 */
namespace modules\profiles\backend\forms;

use modules\profiles\common\models\Premium;
use modules\profiles\common\models\PremiumHistory;
use modules\sales\common\models\SalePoint;
use modules\sales\common\models\SalePointProfile;
use yii\base\Model;


class CalculatePremiunForm extends Model
{
    public $order_id;
    public $document_id;
    public $premium_sum;
    public $document_number;
    public $document_act;


    public function rules()
    {
        return [
            [['document_number', 'document_act', 'document_id'], 'safe'],
            [['order_id', 'premium_sum'], 'integer'],
            [['document_id', 'document_number', 'document_act', 'premium_sum'], 'required'],
            ['premium_sum', 'number', 'min' =>1 ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Id заказа',
            'document_id' => 'Номер заказа',
            'document_number' => 'Дополнительное соглашение (№, дата) ',
            'document_act' => 'Акт (номер, дата)',
            'premium_sum' => 'Сумма компенсации, руб.',
        ];
    }

    public function process()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            //Сохраняем премию
            $newCompensation = new PremiumHistory();
            $newCompensation->order_id = $this->order_id;
            $newCompensation->document_id = $this->document_id;
            $newCompensation->premium_sum = $this->premium_sum;
            $newCompensation->document_number = $this->document_number;
            $newCompensation->document_act = $this->document_act;
            $newCompensation->save();
            //Пересчитываем остаток
            $remainder_sum = Premium::findOne($this->order_id);
            if ($remainder_sum->remainder_sum < $this->premium_sum) {
                return $this->addError('premium_sum', 'Сумма премии не должна превышать суммы остатка!');
            }
            $remainder_sum->remainder_sum = $remainder_sum->remainder_sum - $this->premium_sum;
            $remainder_sum->save();
            $transaction->commit();
            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            $this->addError('premium_sum', 'Ошибка при начислении компенсации!');
        }
    }
}