<?php
/**
 * Created by PhpStorm.
 * User: Mihon
 * Date: 30.09.2018
 * Time: 14:20
 */
namespace modules\profiles\backend\forms;

use modules\profiles\common\models\Discount;
use modules\profiles\common\models\DiscountHistory;
use modules\sales\common\models\SalePoint;
use modules\sales\common\models\SalePointProfile;
use yii\base\Model;


class CalculateDiscountForm extends Model
{
    public $order_id;
    public $document_id;
    public $discount_sum;
    public $document_number;
    public $document_created_at;


    public function rules()
    {
        return [
            [['document_created_at', 'document_number', 'document_id'], 'safe'],
            [['order_id', 'discount_sum'], 'integer'],
            ['discount_sum', 'number', 'min' => 1],
            [['discount_sum', 'order_id', 'document_id','document_created_at','discount_sum', ], 'required'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'document_created_at' => 'Дата накладной',
            'document_id' => 'Номер заказа',
            'document_number' => '№ накладной',
            'id' => 'id заказа',
            'discount_sum' => 'Сумма компенсации, руб. ',
        ];
    }

    public function process()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            //Сохраняем премию
            $newCompensation = new DiscountHistory();
            $newCompensation->order_id = $this->order_id;
            $newCompensation->document_id = $this->document_id;
            $newCompensation->discount_sum = $this->discount_sum;
            $newCompensation->document_number = $this->document_number;
            $newCompensation->document_created_at = date("Y-m-d H:i:s", strtotime($this->document_created_at));
            $newCompensation->save();
            //Пересчитываем остаток
            $remainder_sum = Discount::findOne($this->order_id);
            if ($remainder_sum->remainder_sum < $this->discount_sum) {
                return $this->addError('discount_sum', 'Сумма премии не должна превышать суммы остатка!');
            }
            $remainder_sum->remainder_sum = $remainder_sum->remainder_sum - $this->discount_sum;
            $remainder_sum->save();
            $transaction->commit();
            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            $this->addError('discount_sum', 'Ошибка при начислении компенсации!');
        }
    }
}