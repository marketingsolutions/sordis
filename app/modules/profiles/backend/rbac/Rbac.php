<?php

namespace modules\profiles\backend\rbac;
use yii\rbac\Item;


/**
 * Class Rbac
 */
class Rbac
{
    const ROLE_RECEIVE_PROFILE_CHANGE_NOTIFICATIONS = 'RECEIVE_PROFILE_CHANGE_NOTIFICATIONS';

    public static function dependencies()
    {
        return [
            self::ROLE_RECEIVE_PROFILE_CHANGE_NOTIFICATIONS => ['Получает уведомления об изменении профилей', Item::TYPE_ROLE, []],
        ];
    }
}