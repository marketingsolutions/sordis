<?php

use yii\db\Migration;

/**
 * Class m190613_084039_profiles_create_discount_premium_table
 */
class m190613_084039_profiles_create_discount_premium_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%discount}}', [
            'id' => $this->primaryKey(),
            'bonus_sum' => $this->integer(),
            'discount_sum' => $this->integer(),
            'remainder_sum' => $this->integer(),
            'created_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createTable('{{%premium}}', [
            'id' => $this->primaryKey(),
            'bonus_sum' => $this->integer(),
            'premium_sum' => $this->integer(),
            'remainder_sum' => $this->integer(),
            'created_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createTable('{{%discount_history}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'document_id' => $this->string(),
            'discount_sum' => $this->integer(),
            'document_number' => $this->string(),
            'document_created_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createTable('{{%premium_history}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'document_id' => $this->string(),
            'premium_sum' => $this->integer(),
            'document_number' => $this->string(),
            'document_act' => $this->string(),
        ], $tableOptions);

        $this->createIndex('order_id' , '{{%discount_history}}', 'order_id');
        $this->createIndex('order_id' , '{{%premium_history}}', 'order_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('order_id' , '{{%discount_history}}');
        $this->dropIndex('order_id' , '{{%premium_history}}');
        $this->dropTable('{{%discount}}');
        $this->dropTable('{{%premium}}');
        $this->dropTable('{{%discount_history}}');
        $this->dropTable('{{%premium_history}}');
    }
}
