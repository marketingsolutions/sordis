<?php

use yii\db\Migration;

/**
 * Class m190613_124813_profiles_add_columns_profile_id_to_discount_premium_table
 */
class m190613_124813_profiles_add_columns_profile_id_to_discount_premium_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%discount}}' , 'profile_id',  $this->integer());
        $this->addColumn('{{%premium}}', 'profile_id', $this->integer());
        $this->createIndex('profile_id', '{{%discount}}', 'profile_id');
        $this->createIndex('profile_id', '{{%premium}}', 'profile_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('profile_id', '{{%discount}}');
        $this->dropIndex('profile_id', '{{%premium}}');
        $this->dropColumn('{{%discount}}' , 'profile_id');
        $this->dropColumn('{{%premium}}', 'profile_id');
    }

}
