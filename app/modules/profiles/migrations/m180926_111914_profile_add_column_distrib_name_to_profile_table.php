<?php

use yii\db\Migration;

/**
 * Class m180926_111914_profile_add_column_distrib_name_to_profile_table
 */
class m180926_111914_profile_add_column_distrib_name_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'distrib_name', $this->text());
        $this->addColumn('{{%profiles}}', 'rtt_name', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'distrib_name');
        $this->dropColumn('{{%profiles}}', 'rtt_name');
    }
}
