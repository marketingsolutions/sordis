<?php

use yii\db\Migration;

/**
 * Class m171011_082822_profiles_add_avatar_column
 */
class m171011_082822_profiles_add_avatar_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'avatar', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'avatar');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171011_082822_profiles_add_avatar_column cannot be reverted.\n";

        return false;
    }
    */
}
