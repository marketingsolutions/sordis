<?php

use yii\db\Schema;
use yii\db\Migration;

class m150615_111931_profiles_create_profiles_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%profiles}}', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(32),
            'last_name' => $this->string(32),
            'middle_name' => $this->string(32),
            'full_name' => $this->string(),
            'phone_mobile' => $this->string(16),
            'email' => $this->string(64),
            'dealer_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%profiles}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
