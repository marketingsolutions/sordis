<?php

use yii\db\Migration;

/**
 * Class m171123_063158_profiles_create_actions_table
 */
class m171123_063158_profiles_create_actions_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%actions}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'loyalty_1c_name' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'date_start' => $this->date(),
            'date_end' => $this->date(),
            'enabled' => $this->boolean()->defaultValue(true),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%actions}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171123_063158_profiles_create_actions_table cannot be reverted.\n";

        return false;
    }
    */
}
