<?php

use yii\db\Migration;

class m160219_151314_profiles_create_table_dealers extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%dealers}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'region_id' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
       $this->dropTable('{{%dealers}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
