<?php

use yii\db\Migration;

/**
 * Class m181004_112306_profiles_create_confirm_rtt_table
 */
class m181004_112306_profiles_create_confirm_rtt_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%profile_confirm_rtt}}', [
            'id' => $this->primaryKey(),
            'code_egais'=>$this->string(25),
            'inn'=>$this->string(25),
            'name'=>$this->string(),
            'address'=>$this->string(),
            'license_number'=>$this->string(25),
            'work_time_from'=>$this->time(),
            'work_time_to'=>$this->time(),
            'created_at'=>$this->dateTime(),
            'profile_id'=>$this->integer(),
            'status'=>$this->string(12),
        ], $tableOptions);

        $this->addForeignKey('fk_confirm_rtt_profile', '{{%profile_confirm_rtt}}', 'profile_id', '{{%profiles}}', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_confirm_rtt_profile', '{{%profile_confirm_rtt}}');
        $this->dropTable('{{%profile_confirm_rtt}}');
    }
}
