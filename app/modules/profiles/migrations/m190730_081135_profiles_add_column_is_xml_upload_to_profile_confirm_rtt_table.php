<?php

use yii\db\Migration;

/**
 * Class m190730_081135_profiles_add_column_is_xml_upload_to_profile_confirm_rtt_table
 */
class m190730_081135_profiles_add_column_is_xml_upload_to_profile_confirm_rtt_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile_confirm_rtt}}', 'is_xml_upload', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%profile_confirm_rtt}}', 'is_xml_upload_at', $this->dateTime());
        $this->addColumn('{{%profiles}}', 'is_xml_upload_at', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile_confirm_rtt}}', 'is_xml_upload');
        $this->dropColumn('{{%profile_confirm_rtt}}', 'is_xml_upload_at');
        $this->dropColumn('{{%profiles}}', 'is_xml_upload_at');
    }

}
