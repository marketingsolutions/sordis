<?php

use yii\db\Migration;

/**
 * Class m180910_082226_profiles_add_column_role_to_profiles_table
 */
class m180910_082226_profiles_add_column_role_to_profiles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%profiles}}', 'role');
        $this->addColumn('{{%profiles}}', 'role', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'role');
        $this->addColumn('{{%profiles}}', 'role', $this->string()->defaultValue('manager'));
    }
}
