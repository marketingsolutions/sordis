<?php

use yii\db\Migration;

class m160219_151315_profiles_fk_table_dealers extends Migration
{
    public function up()
    {
        $this->addForeignKey('fk_region_id',
            '{{%dealers}}', 'region_id',
            '{{%regions}}', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey('fk_dealer_id',
            '{{%profiles}}', 'dealer_id',
            '{{%dealers}}', 'id',
            'RESTRICT', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk_region_id', '{{%dealers}}');
        $this->dropForeignKey('fk_dealer_id', '{{%profiles}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
