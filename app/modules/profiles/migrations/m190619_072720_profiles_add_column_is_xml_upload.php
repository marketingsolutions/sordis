<?php

use yii\db\Migration;

/**
 * Class m190619_072720_profiles_add_column_is_xml_upload
 */
class m190619_072720_profiles_add_column_is_xml_upload extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'is_xml_upload', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'is_xml_upload');
    }

}
