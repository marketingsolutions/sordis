<?php

use yii\db\Migration;

/**
 * Class m171130_084730_actions_products_create
 */
class m171130_084730_actions_products_create extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%actions_products}}', [
            'action_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('pk_actions_products', '{{%actions_products}}', ['action_id', 'product_id']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%actions_products}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171130_084730_actions_products_create cannot be reverted.\n";

        return false;
    }
    */
}
