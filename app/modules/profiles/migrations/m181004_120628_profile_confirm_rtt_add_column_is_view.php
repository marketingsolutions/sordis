<?php

use yii\db\Migration;

/**
 * Class m181004_120628_profile_confirm_rtt_add_column_is_view
 */
class m181004_120628_profile_confirm_rtt_add_column_is_view extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile_confirm_rtt}}', 'is_view', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile_confirm_rtt}}', 'is_view');
    }
}
