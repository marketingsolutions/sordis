<?php

use yii\db\Migration;

class m160218_151316_profiles_create_table_regions extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%regions}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'admin_user_id' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
       $this->dropTable('{{%regions}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
