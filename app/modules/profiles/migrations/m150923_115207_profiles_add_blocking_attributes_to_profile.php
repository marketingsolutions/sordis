<?php

use yii\db\Migration;

class m150923_115207_profiles_add_blocking_attributes_to_profile extends Migration
{
    public function up()
    {
        $this->addColumn('{{%profiles}}', 'blocked_at', $this->dateTime());
        $this->addColumn('{{%profiles}}', 'blocking_reason', $this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%profiles}}', 'blocking_reason');
        $this->dropColumn('{{%profiles}}', 'blocked_at');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
