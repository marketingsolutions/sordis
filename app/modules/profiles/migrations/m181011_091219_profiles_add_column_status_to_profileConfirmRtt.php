<?php

use yii\db\Migration;

/**
 * Class m181011_091219_profiles_add_column_status_to_profileConfirmRtt
 */
class m181011_091219_profiles_add_column_status_to_profileConfirmRtt extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile_confirm_rtt}}', 'type', $this->string(15));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile_confirm_rtt}}', 'type');
    }
}
