<?php

use yii\db\Migration;

/**
 * Class m190605_140112_profile_add_user_data_to_allow_table
 */
class m190605_140112_profile_add_user_data_to_allow_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%allow_personal_info}}', 'first_name', $this->string());
        $this->addColumn('{{%allow_personal_info}}', 'last_name', $this->string());
        $this->addColumn('{{%allow_personal_info}}', 'middle_name', $this->string());
        $this->addColumn('{{%allow_personal_info}}', 'email', $this->string());
        $this->addColumn('{{%allow_personal_info}}', 'phone_mobile', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%allow_personal_info}}', 'first_name');
        $this->dropColumn('{{%allow_personal_info}}', 'last_name');
        $this->dropColumn('{{%allow_personal_info}}', 'middle_name');
        $this->dropColumn('{{%allow_personal_info}}', 'email');
        $this->dropColumn('{{%allow_personal_info}}', 'phone_mobile');
    }
}
