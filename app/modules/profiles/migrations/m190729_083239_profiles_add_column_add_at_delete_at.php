<?php

use yii\db\Migration;

/**
 * Class m190729_083239_profiles_add_column_add_at_delete_at
 */
class m190729_083239_profiles_add_column_add_at_delete_at extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile_confirm_rtt}}', 'add_delele_at', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile_confirm_rtt}}', 'add_delele_at');
    }
}
