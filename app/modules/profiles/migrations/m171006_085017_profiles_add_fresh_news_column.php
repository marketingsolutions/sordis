<?php

use yii\db\Migration;

/**
 * Class m171006_085017_profiles_add_fresh_news_column
 */
class m171006_085017_profiles_add_fresh_news_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'fresh_news', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'fresh_news');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171006_085017_profiles_add_fresh_news_column cannot be reverted.\n";

        return false;
    }
    */
}
