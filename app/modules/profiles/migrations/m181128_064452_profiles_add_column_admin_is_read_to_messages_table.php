<?php

use yii\db\Migration;

/**
 * Class m181128_064452_profiles_add_column_admin_is_read_to_messages_table
 */
class m181128_064452_profiles_add_column_admin_is_read_to_messages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%feedback_messages}}', 'admin_is_read', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%feedback_messages}}', 'admin_is_read');
    }
}
