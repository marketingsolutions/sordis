<?php

use yii\db\Migration;

/**
 * Class m171123_081104_profiles_dealers_add_action_leader_columns
 */
class m171123_081104_profiles_dealers_add_action_leader_columns extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%dealers}}', 'action_id', $this->integer());
        $this->addColumn('{{%dealers}}', 'leader_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%dealers}}', 'action_id');
        $this->dropColumn('{{%dealers}}', 'leader_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171123_081104_profiles_dealers_add_action_leader_columns cannot be reverted.\n";

        return false;
    }
    */
}
