<?php

use yii\db\Migration;

/**
 * Class m180910_083437_profiles_add_column_inn_to_profile_and_add_egais_table
 */
class m180910_083437_profiles_add_column_inn_to_profile_and_add_egais_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'inn', $this->string(25));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'inn');
    }
}
