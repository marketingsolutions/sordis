<?php

use yii\db\Migration;

/**
 * Class m190621_123439_profiles_add_column_admin_allow_message_to_allow_personal_info_table
 */
class m190621_123439_profiles_add_column_admin_allow_message_to_allow_personal_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%allow_personal_info}}', 'admin_allow_message' , $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%allow_personal_info}}', 'admin_allow_message');
    }
}
