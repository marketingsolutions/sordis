<?php

use yii\db\Migration;

class m171108_114213_profiles_add_push_columns extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'push_news', $this->boolean()->defaultValue(true));
        $this->addColumn('{{%profiles}}', 'push_sales', $this->boolean()->defaultValue(true));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'push_news');
        $this->dropColumn('{{%profiles}}', 'push_sales');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170908_114213_profiles_add_sub_columns cannot be reverted.\n";

        return false;
    }
    */
}
