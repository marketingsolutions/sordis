<?php

use yii\db\Migration;

/**
 * Class m190604_065515_profiles_add_column_block_personal_to_profiles_table
 */
class m190604_065515_profiles_add_column_block_personal_to_profiles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'is_allow_personal', $this->boolean()->defaultValue(false));
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%allow_personal_info}}', [
            'id' => $this->primaryKey(),
            'profile_id' => $this->integer(),
            'user_message' => $this->text(),
            'admin_message' => $this->text(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);
        $this->createIndex('profile_id', '{{%allow_personal_info}}', 'profile_id');
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('profile_id', '{{%allow_personal_info}}');
        $this->dropColumn('{{%profiles}}', 'is_allow_personal');
        $this->dropTable('{{%allow_personal_info}}');

    }
}
