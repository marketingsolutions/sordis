<?php

use yii\db\Migration;

class m161208_114801_profiles_fk_profile_course_visits_table extends Migration
{
    public function up()
    {
		$this->addForeignKey('profileid',
			'{{%profile_course_visits}}', 'profile_id',
			'{{%profiles}}', 'id',
			'RESTRICT', 'CASCADE'
		);

		$this->addForeignKey('courseid',
			'{{%profile_course_visits}}', 'course_id',
			'{{%courses}}', 'id',
			'RESTRICT', 'CASCADE'
		);
    }

    public function down()
    {
		$this->dropForeignKey('profileid', '{{%profile_course_visits}}');
		$this->dropForeignKey('testid', '{{%profile_course_visits}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
