<?php

use yii\db\Schema;
use yii\db\Migration;

class m150615_145312_profiles_add_identity_id_to_profiles extends Migration
{
    public function up()
    {
        $this->addColumn('{{%profiles}}', 'identity_id', Schema::TYPE_STRING);
        $this->createIndex('identity_id', '{{%profiles}}', 'identity_id');
    }

    public function down()
    {
        $this->dropColumn('{{%profiles}}', 'identity_id');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
