<?php

namespace modules\profiles\frontend\controllers;

use modules\profiles\common\models\AllowPersonalInfo;
use modules\profiles\common\models\Profile;
use modules\profiles\frontend\models\ChangepassForm;
use modules\profiles\frontend\models\ProfileEdit;
use ms\loyalty\contracts\prizes\PrizeRecipientInterface;
use ms\loyalty\feedback\common\models\Message;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yz\Yz;
use modules\profiles\frontend\forms\AddAllow;

/**
 * Class ProfileController
 */
class ProfileController extends Controller
{
    /**
     * @var PrizeRecipientInterface
     */
    private $prizeRecipient;

    public function __construct($id, $module, PrizeRecipientInterface $prizeRecipient, $config = [])
    {
        $this->prizeRecipient = $prizeRecipient;
        parent::__construct($id, $module, $config);
    }

    public function behaviors()
    {
        return [
            'accessControl' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        /** @var ProfileEdit $model */
        $model = ProfileEdit::findOne($this->prizeRecipient->recipientId);
        $profile = \Yii::$app->user->identity->profile;
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            $model->upload(['avatar']);
            \Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Профиль успешно сохранен');
            return $this->redirect(['index']);
        }

        return $this->render('index', [
            'model' => $model,
            'profile' => $profile,
        ]);
    }

    public function actionAllow()
    {
        $profile = \Yii::$app->user->identity->profile;

        $model = new AddAllow();
        if(\Yii::$app->request->post() && $model->process()){
            \Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Ваш запрос успешнео отправлен на согласование администратору.');
            return $this->redirect(['index']);
        }else{
            print_r($model->getErrors());
        }
        return $this->render('allow', [
            'model' => $model,
            'profile' => $profile,
        ]);
    }

    public function actionChangePass()
    {
        $model = new ChangepassForm();

        if ($model->load(\Yii::$app->request->post()) && $model->process()) {
            \Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Пароль успешно изменён');
            return $this->redirect(['change-pass']);
        }
        return $this->render('pass', [
            'model' => $model,
        ]);
    }

    public function actionMessage(){
        $profile = \Yii::$app->user->identity->profile;
        $myMessage = Message::find()->where(['user_id' => $profile->id])->asArray()->all();
        return $this->render('message', [
            'profile' => $profile,
            'myMessage' => $myMessage,
        ]);
    }
}