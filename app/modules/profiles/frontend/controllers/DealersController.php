<?php

namespace modules\profiles\frontend\controllers;

use modules\profiles\common\models\Profile;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * DealersController implements the CRUD actions for Transaction model.
 */
class DealersController extends Controller
{
    public function actionUsers()
    {
        /** @var Profile $profile */
        $profile = \Yii::$app->user->identity->profile;

        if ($profile->isLeader() == false) {
            throw new ForbiddenHttpException();
        }

        $profiles = Profile::find()
            ->where(['dealer_id' => $profile->dealer_id])
            ->andWhere(['!=', 'id', $profile->id])
            ->orderBy(['id' => SORT_ASC])
            ->all();

        return $this->render('users', compact('profiles', 'profile'));
    }

    public function actionAction()
    {
        /** @var Profile $profile */
        $profile = \Yii::$app->user->identity->profile;

        if (null == ($action = $profile->dealer->action)) {
            throw new NotFoundHttpException();
        }

        return $this->render('action', compact('profile', 'action'));
    }
}