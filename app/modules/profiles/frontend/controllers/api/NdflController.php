<?php

namespace modules\profiles\frontend\controllers\api;


use modules\profiles\common\models\Profile;
use ms\loyalty\api\common\models\Log;
use ms\loyalty\api\frontend\base\ApiController;
use modules\profiles\frontend\models\ApiProfile;
use Yii;
use marketingsolutions\phonenumbers\PhoneNumber;
use libphonenumber\PhoneNumberFormat;
use ms\loyalty\taxes\common\models\Country;
use ms\loyalty\taxes\common\models\DocumentType;
use ms\loyalty\taxes\common\forms\AccountProfileForm;
use ms\loyalty\taxes\common\models\Account;

/**
 * Class NdflController
 */
class NdflController extends ApiController
{

    public function actionNdflInfo()
    {
        $profile_id = \Yii::$app->request->post('profile_id', null);
        $profile = ApiProfile::findOne(['id' => $profile_id]);

        $countries = Country::find()
            ->orderBy(['name' => SORT_ASC])
            ->all();
        $doc_types = DocumentType::find()
            ->all();

        $ndflprofile = $this->findAccount($profile);
        $acc = AccountProfileForm::findOne(['account_id' => $ndflprofile->id]);

        $return = ['result' => 'OK','form' => $acc, 'countries' => $countries, 'doc_types' => $doc_types];
        Log::setComment("Выдача НДФЛ-данных пользователю $profile->full_name");
        Log::setResponse($return);

        return $return;

    }

    public function actionNdflForm()
    {
        $postData = Yii::$app->request->post();
        $profile_id = \Yii::$app->request->post('profile_id', null);
        $profile = ApiProfile::findOne(['id' => $profile_id]);

        $postData = ['AccountProfileForm' => $postData];

        $account = $this->findAccount($profile);

        if ($account->hasActiveAccountProfile() == false) {
            $form = new AccountProfileForm(['account' => $account]);
        }
        else {
            $activeAccount = $account->activeAccountProfile;
            $form = AccountProfileForm::findOne($activeAccount->id);
            $form->account = $account;
        }

        $form->loadValuesFromProfile($profile);


        if ($form->load($postData) && $form->process()) {
            $return = ['result' => 'OK', 'message' => 'Анкета отправлена!'];
            Log::setComment("Получена анкета НДФЛ от пользователя $profile->full_name");
            Log::setResponse($return);
        } else {
            $return = ['result' => 'FAIL', 'reason' => 'Ошибка при отправке анкеты!', 'errors' => $form];
            Yii::$app->response->statusCode = 400;
            Log::setComment("Ошибка при отправке анкеты НДФЛ пользователя $profile->full_name", 400);
            Log::setResponse($return);

        }
        return $return;

    }

    private function findAccount(Profile $profile)
    {
        $acc = Account::find()
            ->where(['profile_id' => $profile->getProfileId()])
            ->one();

        if ($acc === null) {
            $acc = Account::create($profile);
        }
        return $acc;
    }

}