<?php

namespace modules\profiles\frontend\controllers\api;

use modules\mobile\common\models\ApiGcm;
use modules\profiles\common\models\Dealer;
use modules\profiles\frontend\models\ApiProfile;
use modules\profiles\frontend\models\ApiRegistrationForm;
use modules\sales\frontend\models\ChangePasswordForm;
use ms\loyalty\api\common\models\Log;
use ms\loyalty\api\frontend\base\ApiController;
use ms\loyalty\identity\phonesEmails\common\models\Identity;
use Yii;
use libphonenumber\PhoneNumberFormat;
use modules\profiles\common\models\Profile;
use marketingsolutions\phonenumbers\PhoneNumber;

/**
 * Class AuthController
 */
class AuthController extends ApiController
{
    public function actionLogin()
    {
        try {
            $phoneNumber = \Yii::$app->request->post('phone', null);
            $password = \Yii::$app->request->post('password', null);
            $phone = null;

            if (PhoneNumber::validate($phoneNumber, 'RU') == false) {
                return $this->error('Неверно указан номер телефона', 'Ошибка авторизации');
            }

            $phone = PhoneNumber::format($phoneNumber, PhoneNumberFormat::E164, 'RU');

            /** @var ApiProfile $profile */
            if ($profile = ApiProfile::findOne(['phone_mobile' => $phone])) {
                /** @var Identity $identity */
                if ($identity = $profile->identity) {
                    if ($identity->validatePassword($password)) {
                        return $this->ok(['profile' => $profile->toArray()], 'Успешная авторизация');
                    }
                }
            }

            return $this->error('Неверный логин или пароль', 'Ошибка авторизации');
        }
        catch (\Exception $e) {
            return $this->error('Ошибка на сервере', 'Ошибка на сервере: ' . $e->getMessage(), 500);
        }
    }

    public function actionToken()
    {
        $digits = 4;
        $token = rand(pow(10, $digits - 1), pow(10, $digits) - 1);

        $phone = Yii::$app->request->post('phone', null);

        if (PhoneNumber::validate($phone, 'RU') == false) {
            return $this->error('Ошибка в номере телефона', 'Получение токена неудачно');
        }

        $phoneNumber = PhoneNumber::format($phone, PhoneNumberFormat::E164, 'RU');

        if ($profile = Profile::findOne(['phone_mobile' => $phoneNumber])) {
            return $this->error('Номер уже зарегистрирован', 'Получение токена неудачно');
        }

        Yii::$app->sms->send($phoneNumber, 'Код доступа: ' . $token);

        return $this->ok(['token' => $token, 'phoneNumber' => $phoneNumber], 'Успешное получение токена');
    }

    public function actionRemindPassword()
    {
        $phone = Yii::$app->request->post('phone', null);

        if (PhoneNumber::validate($phone, 'RU') == false) {
            return $this->error('Ошибка в номере телефона', 'Ошибка при восстановлении пароля');
        }

        $phoneNumber = PhoneNumber::format($phone, PhoneNumberFormat::E164, 'RU');
        $profile = ApiProfile::findOne(['phone_mobile' => $phoneNumber]);

        if ($profile === null) {
            return $this->error('Не найден участник по данному номеру', 'Ошибка при восстановлении пароля');
        }

        $model = new ChangePasswordForm(['profile' => $profile]);
        $postData = Yii::$app->request->post();
        $postData = ['ChangePasswordForm' => $postData];

        if ($model->load($postData) && $model->validate() && $model->changePassword()) {
            return $this->ok(['profile' => $profile->toArray()], 'Успешное восстановление пароля');
        }
        else {
            return $this->error($model->getFirstErrors(), 'Ошибка при восстановлении пароля');
        }
    }

    public function actionTokenRemind()
    {
        $digits = 4;
        $token = rand(pow(10, $digits - 1), pow(10, $digits) - 1);

        $phone = Yii::$app->request->post('phone', null);

        if (PhoneNumber::validate($phone, 'RU') == false) {
            return $this->error('Ошибка в номере телефона', 'Ошибка получения токена для восстановления пароля');
        }

        $phoneNumber = PhoneNumber::format($phone, PhoneNumberFormat::E164, 'RU');
        $profile = Profile::findOne(['phone_mobile' => $phoneNumber]);

        if ($profile === null) {
            return $this->error('Не найден участник по данному номеру', 'Ошибка получения токена для восстановления пароля');
        }

        Yii::$app->sms->send($phoneNumber, 'Код доступа: ' . $token);

        return $this->ok(['token' => $token, 'phoneNumber' => $phoneNumber], 'Успешное получение токена для восстановления пароля');
    }

    public function actionRegisterInfo()
    {
        $dealers = Dealer::find()
            ->select('id, name')
            ->orderBy(['name' => SORT_ASC])
            ->all();

        return $this->ok(['dealers' => $dealers], 'Получение данных для регистрации');
    }

    public function actionRegister()
    {
        $form = new ApiRegistrationForm();
        $postData = Yii::$app->request->post();

        if (empty($postData)) {
            return $this->error('Пустой запрос', 'Ошибка регистрации участника');
        }

        $postData = ['ApiRegistrationForm' => $postData];

        if ($form->load($postData) && $form->validate() && $form->createProfile()) {
            return $this->ok(['profile' => $form->getApiProfile()], 'Успешная регистрация участника');
        }
        else {
            return $this->error($form->getFirstErrors(), 'Ошибка регистрации участника');
        }
    }

    public function actionProfileInfo()
    {
        $profile_id = \Yii::$app->request->post('profile_id', null);

        if ($profile = ApiProfile::findOne(['id' => $profile_id])) {
            return $this->ok(['profile' => $profile->toArray(), 'Получение данных по участнику']);
        }

        return $this->error('Не найден участник по этому номеру телефона');
    }

    public function actionProfileEdit()
    {
        $profiles = Profile::find()->all();
        foreach ($profiles as $profile) {
            /** @var Profile $profile */
            $profile->createPurse();
        }

        $postData = Yii::$app->request->post();
        $profile_id = \Yii::$app->request->post('profile_id', null);
        $profile = ApiProfile::findOne(['id' => $profile_id]);

        if ($profile == null) {
            return $this->error('Участник не найден', 'Ошибка обновления профиля участника');
        }

        if (empty($postData)) {
            return $this->error('Пустой запрос', 'Ошибка обновления профиля участника');
        }

        if ($profile->load(['ApiProfile' => $postData]) && $profile->save()) {
            return $this->ok(['profile' => $profile], 'Успешное обновление профиля участников');
        }

        return $this->error($profile->getFirstErrors(), 'Ошибка обновления профиля участника');
    }

    public function actionGcm()
    {
        $platform = Yii::$app->request->post('platform');
        $token = Yii::$app->request->post('token');
        $profile_id = Yii::$app->request->post('profile_id');

        if (!$platform || !$token || !$profile_id) {
            return $this->error('Необходимо указать поля: platform, token, profile_id', 'Ошибка добавления токена GCM');
        }

        $gcm = ApiGcm::findOne(['token' => $token]);

        if ($gcm == null) {
            $gcm = new ApiGcm();
            $gcm->profile_id = $profile_id;
            $gcm->token = $token;
            $gcm->platform = $platform;
            $gcm->api_key = ApiGcm::API_KEY;
            $gcm->save(false);
        }

        return $this->ok(['gcm' => $gcm], 'Токен GCM добавлен');
    }

    public function actionResetData()
    {
        $site = Yii::$app->request->post('site');

        # боевой сервер, либо же локальный
        if ($site == 'http://sistemapromo.dv.msforyou.ru/') {
            $user = getenv('DB_USER');
            $pw = getenv('DB_PASSWORD');
            exec("mysql -u{$user} -p{$pw} d_sistemapromo < /home/webuser/demo_versions/sistemapromo/current/dump.sql");
        }
        else {
            exec("mysql -uroot -proot yz2app < /vagrant/dump.sql");
        }

        return $this->ok(['site' => $site], 'Данные успешно сброшены');
    }

    public function actionUsers()
    {
        # TODO: comment out after Apple approval
        # return $this->ok(['profiles' => []], 'Получение списка участников, доступных для свободного входа');

        $ids = [508, 510, 511, 513];
        $profiles = ApiProfile::find()
            ->where(['id' => $ids])
            ->orderBy(['id' => SORT_ASC])
            ->all();

        return $this->ok(['profiles' => $profiles], 'Получение списка участников, доступных для свободного входа');
    }

    public function actionProfilesByDealer()
    {
        $profile_id = \Yii::$app->request->post('profile_id', null);
        $errorLog = 'Ошибка получения списка участников по дистрибьютору';

        $profile = ApiProfile::findOne(['id' => $profile_id]);
        if ($profile === null) {
            return $this->error('Участник не найден по profile_id', $errorLog);
        }

        if ($profile->isLeader() == false) {
            return $this->error('Участник не является лидером для просмотра списка', $errorLog);
        }

        $profiles = ApiProfile::find()
            ->where(['dealer_id' => $profile->dealer_id])
            ->andWhere(['!=', 'id', $profile->id])
            ->orderBy(['id' => SORT_ASC])
            ->all();

        return $this->ok(compact('profiles'), 'Получение списка участников по дистрибьютору');
    }
}