<?php

namespace modules\profiles\frontend\controllers\api;

use Yii;
use ms\loyalty\api\frontend\base\ApiController;
use marketingsolutions\finance\models\Purse;
use modules\profiles\frontend\models\ApiProfile;
use modules\profiles\frontend\models\ApiTransactions;

class RpcController extends ApiController
{
    public function actionShutdown()
    {
        $code = Yii::$app->request->post('code');
        if (empty($code) || $code != getenv('RPC_CODE')) {
            return $this->error('Empty or wrong code', 'Ошибка выключения удаленки: неверный код');
        }

        $ip = getenv('RPC_IP');
        $user = getenv('RPC_USER');
        $password = getenv('RPC_PASSWORD');

        $command = "net rpc shutdown -S {$ip} -U'{$user}%{$password}' -t 1 -f";
        $output = shell_exec($command);

        if ($output === null) {
            return $this->error('apt-get install samba-common-bin', 'Ошибка выключения удаленки: на сервере отсутствует пакет samba-common-bin', 500);
        }

        var_dump($output);
        exit;

        return $this->ok(['transactions' => $transactions], 'Получение истории транзакций участника');
    }


}