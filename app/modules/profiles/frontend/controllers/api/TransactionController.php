<?php

namespace modules\profiles\frontend\controllers\api;

use Yii;
use ms\loyalty\api\frontend\base\ApiController;
use marketingsolutions\finance\models\Purse;
use modules\profiles\frontend\models\ApiProfile;
use modules\profiles\frontend\models\ApiTransactions;

/**
 * Class TransactionController
 */
class TransactionController extends ApiController
{

    public function actionList()
    {
        $profile_id = Yii::$app->request->post('profile_id', null);

        $profile = ApiProfile::findOne($profile_id);

        if(null === $profile) {
            return $this->error("Не найден участник с ID $profile_id");
        }

        $purse = Purse::findOne(['owner_id' => $profile->id]);

        if(null === $purse) {
            return $this->error("Не найден кошелёк участника с ID $profile_id");
        }

        $transactions = ApiTransactions::find()
            ->where(['purse_id' => $purse->id])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();

        return $this->ok(['transactions' => $transactions], 'Получение истории транзакций участника');
    }


}