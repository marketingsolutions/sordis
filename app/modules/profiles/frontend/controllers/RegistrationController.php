<?php

namespace modules\profiles\frontend\controllers;

use modules\profiles\common\models\Dealer;
use modules\profiles\frontend\models\RegistrationForm;
use modules\sales\common\models\SalePoint;
use ms\loyalty\contracts\identities\IdentityRegistrarInterface;
use ms\loyalty\contracts\identities\RegistrationTokenProviderInterface;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yz\Yz;
use modules\profiles\frontend\forms\RegisterTTForm;
use modules\profiles\frontend\forms\RegisterDistribForm;
use modules\profiles\frontend\forms\RegisterMultiTTForm;


/**
 * Class RegistrationController
 */
class RegistrationController extends Controller
{
    /**
     * @var RegistrationTokenProviderInterface
     */
    private $registrationTokenProvider;
    /**
     * @var IdentityRegistrarInterface
     */
    private $registrar;

    public function __construct($id, $module,
                                RegistrationTokenProviderInterface $registrationTokenProvider,
                                IdentityRegistrarInterface $registrar,
                                $config = [])
    {
        $this->registrationTokenProvider = $registrationTokenProvider;
        $this->registrar = $registrar;

        parent::__construct($id, $module, $config);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => false,
						'roles' => ['@'],
						'denyCallback' => function ($rule, $action) {
							\Yii::$app->response->redirect(Url::home());
						}
					],
                    [
                        'allow' => false,
                        'matchCallback' => function ($rule, $action) {
                            $tokenManager = $this->registrationTokenProvider->findFromRequest(\Yii::$app->request);
                            return $tokenManager === null;
                        },
                        'denyCallback' => function ($rule, $action) {
                            \Yii::$app->session->setFlash(Yz::FLASH_ERROR, 'Извините, Вы должны подтвердить свои данные');
                            \Yii::$app->response->redirect(Url::home());
                        }
                    ],
                    [
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => false,
                    ]
                ]
            ]
        ];
    }


    public function actionIndex()
    {
        $tokenManager = $this->registrationTokenProvider->findFromRequest(\Yii::$app->request);
        $model = new RegistrationForm($tokenManager, $this->registrar);
        if(\Yii::$app->request->get('role') && \Yii::$app->request->get('role')==\modules\profiles\common\models\Profile::ROLE_MANAGER_TT){
            $roleModel = new RegisterTTForm();
            $roleModel->attributes = \Yii::$app->request->post('RegisterTTForm');
        }elseif(\Yii::$app->request->get('role') && \Yii::$app->request->get('role')==\modules\profiles\common\models\Profile::ROLE_DISTRIBUTOR) {
            $roleModel = new RegisterDistribForm();
            $roleModel->attributes = \Yii::$app->request->post('RegisterDistribForm');
        }elseif(\Yii::$app->request->get('role') && \Yii::$app->request->get('role')==\modules\profiles\common\models\Profile::ROLE_MULTI_MANAGER_TT){
            $roleModel = new RegisterMultiTTForm();
            $roleModel->attributes = \Yii::$app->request->post('RegisterMultiTTForm');
        }else{
            $roleModel = false;
        }

        if ($model->loadAll(\Yii::$app->request->post()) && $roleModel->validate() && $model->process()) {
            \Yii::$app->user->login($model->identity);
            return $this->redirect(['/dashboard/index']);
        }

        return $this->render('index',[
            'model' => $model,
            'roleModel' => $roleModel,
        ]);
    }
}