<?php

namespace modules\profiles\frontend\models;

use modules\profiles\common\models\Action;

class ApiAction extends Action
{
    public function fields()
    {
        $fields = [
            'id',
            'name',
            'description',
            'starts' => 'date_start_formatted',
            'ends' => 'date_end_formatted',
        ];

        return $fields;
    }
}