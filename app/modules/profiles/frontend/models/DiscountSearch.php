<?php

namespace modules\profiles\frontend\models;

use modules\profiles\common\models\Discount;
use modules\profiles\common\models\Profile;
use ms\loyalty\contracts\prizes\PrizeRecipientInterface;
use marketingsolutions\finance\models\Transaction;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BonusesTransactionSearch represents the model behind the search form about `marketingsolutions\finance\models\Transaction`.
 */
class DiscountSearch extends Discount
{


    /** @var  Profile */
    public $profile;

    public function __construct($profile, $config = [])
    {
        $this->profile = \Yii::$app->user->identity->profile;
        parent::__construct($config);
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'bonus_sum', 'discount_sum', 'remainder_sum', 'profile_id'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Discount::find();
        $query->where(['profile_id' => $this->profile->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'bonus_sum' => $this->bonus_sum,
            'discount_sum' => $this->discount_sum,
            'remainder_sum' => $this->remainder_sum,
            'profile_id' => $this->profile_id,
        ]);

//        $query->andFilterWhere(['like', 'type', $this->type])
//            ->andFilterWhere(['like', 'partner_type', $this->partner_type])
//            ->andFilterWhere(['like', 'title', $this->title])
           // ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
