<?php

namespace modules\profiles\frontend\models;

use modules\profiles\common\models\Dealer;
use modules\profiles\common\models\Profile;
use modules\profiles\common\Module;
use ms\loyalty\contracts\identities\IdentityRegistrarInterface;
use ms\loyalty\contracts\identities\RegistrationTokenManagerInterface;
use ms\loyalty\contracts\identities\TokenProvidesEmailInterface;
use ms\loyalty\contracts\identities\TokenProvidesFieldInterface;
use ms\loyalty\contracts\identities\TokenProvidesPhoneMobileInterface;
use ms\loyalty\identity\phonesEmails\common\models\Identity;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\web\IdentityInterface;
use Yii;


/**
 * Class RegistrationForm
 * @property Profile $profile
 */
class ChangepassForm extends Model
{

    public $password;

    public $passwordCompare;




    public function rules()
    {
        return [
            ['password', 'string'],
            ['password', 'required'],
            ['passwordCompare', 'string'],
            ['passwordCompare', 'required'],
            [['passwordCompare'], 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'passwordCompare' => 'Подтверждение пароля',

        ];
    }

    public function process(){
        $identityId = Yii::$app->user->identity->id;
        $model = Identity::findOne(['id' => $identityId]);
        $model->passhash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        if($model->save(false)){
            return true;
        }else{
            return false;
        }

    }









}