<?php

namespace modules\profiles\frontend\models;

use modules\profiles\common\models\Dealer;
use modules\profiles\common\models\Profile;
use modules\profiles\common\Module;
use ms\loyalty\contracts\identities\IdentityRegistrarInterface;
use ms\loyalty\contracts\identities\RegistrationTokenManagerInterface;
use ms\loyalty\contracts\identities\TokenProvidesEmailInterface;
use ms\loyalty\contracts\identities\TokenProvidesFieldInterface;
use ms\loyalty\contracts\identities\TokenProvidesPhoneMobileInterface;
use ms\loyalty\identity\phonesEmails\common\models\Identity;
use ms\loyalty\identity\phonesEmails\common\models\IdentityType;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\web\IdentityInterface;
use Yii;


/**
 * Class ApiRegistrationForm
 */
class ApiRegistrationForm extends Profile
{
	/** @var string */
    public $password;

	/** @var string */
    public $passwordCompare;

	/** @var bool */
    public $agreeWithTerms = false;

	/** @var bool */
    public $allowPersonalDataProcessing = false;

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['password', 'string'],
            ['password', 'required'],
            ['passwordCompare', 'string'],
            ['passwordCompare', 'required'],
			['email', 'required'],
			['email', 'email'],
            ['passwordCompare', 'compare', 'compareAttribute' => 'password'],
            // ['agreeWithTerms', 'compare', 'compareValue' => 1, 'message' => 'Вы должны согласиться с условиями участния в программе'],
            // ['allowPersonalDataProcessing', 'compare', 'compareValue' => 1, 'message' => 'Вы должны разрешить обработку своих персональных данных для участния в программе'],
        ]);
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'passwordCompare' => 'Подтверждение пароля',
            'agreeWithTerms' => 'Согласен / согласна с правилами участия в программе',
            'allowPersonalDataProcessing' => 'Даю согласие на обработку своих персональных данных',
        ];
    }

    public function createProfile()
    {
        $transaction = \Yii::$app->db->beginTransaction();

        $this->save(false);

        $identity = new Identity();
        $identity->role = 'identity';
        $identity->passhash = Identity::hashPassword($this->password);
        $identity->login = $this->phone_mobile;
        $identity->type = IdentityType::PHONE;
        $identity->save();

        $this->updateAttributes(['identity_id' => $identity->id]);

        $transaction->commit();

        return true;
    }

	public function getApiProfile()
	{
		$this->refresh();

		return ApiProfile::findOne($this->id);
	}
}