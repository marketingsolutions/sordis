<?php

namespace modules\profiles\frontend\models;

use modules\profiles\common\models\Premium;
use modules\profiles\common\models\PremiumHistory;
use ms\loyalty\contracts\prizes\PrizeRecipientInterface;
use marketingsolutions\finance\models\Transaction;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BonusesTransactionSearch represents the model behind the search form about `marketingsolutions\finance\models\Transaction`.
 */
class PremiumHistorySearch extends PremiumHistory
{

    /** @var  Profile */
    public $profile;

    public function __construct($profile, $config = [])
    {
        $this->profile = \Yii::$app->user->identity->profile;
        parent::__construct($config);
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'document_id', 'premium_sum', 'document_number', 'document_act'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PremiumHistory::find();
        if(\Yii::$app->request->get('order_id')){
            $query->where(['order_id' => \Yii::$app->request->get('order_id')]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'document_id' => $this->document_id,
            'premium_sum' => $this->premium_sum,
        ]);

        $query->andFilterWhere(['like', 'document_number', $this->document_number])
            ->andFilterWhere(['like', 'document_act', $this->document_act]);

        return $dataProvider;
    }
}
