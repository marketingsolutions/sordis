<?php

namespace modules\profiles\frontend\models;

use modules\profiles\common\models\Discount;
use modules\profiles\common\models\DiscountHistory;
use modules\profiles\common\models\Profile;
use ms\loyalty\contracts\prizes\PrizeRecipientInterface;
use marketingsolutions\finance\models\Transaction;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BonusesTransactionSearch represents the model behind the search form about `marketingsolutions\finance\models\Transaction`.
 */
class DiscountHistorySearch extends DiscountHistory
{


    /** @var  Profile */
    public $profile;

    public function __construct($profile, $config = [])
    {
        $this->profile = \Yii::$app->user->identity->profile;
        parent::__construct($config);
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'document_id', 'discount_sum', 'document_number', 'document_created_at'], 'safe'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DiscountHistory::find();
        if(\Yii::$app->request->get('order_id')){
            $query->where(['order_id' => \Yii::$app->request->get('order_id')]);
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'document_id' => $this->document_id,
            'discount_sum' => $this->discount_sum,
            //'document_number' => $this->document_number,
        ]);

        $query->andFilterWhere(['like', 'document_number', $this->document_number]);
//            ->andFilterWhere(['like', 'partner_type', $this->partner_type])
//            ->andFilterWhere(['like', 'title', $this->title])
           // ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
