<?php

namespace modules\profiles\frontend\models;

use modules\profiles\common\models\Dealer;
use modules\profiles\common\models\Profile;
use modules\profiles\common\Module;
use modules\profiles\frontend\forms\RegisterDistribForm;
use modules\profiles\frontend\forms\RegisterMultiTTForm;
use modules\profiles\frontend\forms\RegisterTTForm;
use ms\loyalty\contracts\identities\IdentityRegistrarInterface;
use ms\loyalty\contracts\identities\RegistrationTokenManagerInterface;
use ms\loyalty\contracts\identities\TokenProvidesEmailInterface;
use ms\loyalty\contracts\identities\TokenProvidesFieldInterface;
use ms\loyalty\contracts\identities\TokenProvidesPhoneMobileInterface;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\web\IdentityInterface;
use Yii;


/**
 * Class RegistrationForm
 * @property Profile $profile
 */
class RegistrationForm extends Model
{
    /**
     * @var IdentityInterface | \ms\loyalty\contracts\identities\IdentityInterface
     */
    public $identity;
    /**
     * @var string user password
     */
    public $password;
    /**
     * @var string user password repeat
     */
    public $passwordCompare;
    /**
     * @var bool
     */
    public $agreeWithTerms = true;
    /**
     * @var bool
     */
    public $allowPersonalDataProcessing = true;
    /**
     * @var \ms\loyalty\contracts\identities\RegistrationTokenManagerInterface
     */
    private $tokenManager;
    /**
     * @var RegistrationProfile
     */
    private $profile;
    /**
     * @var IdentityRegistrarInterface
     */
    private $registrar;

    public function __construct(RegistrationTokenManagerInterface $tokenManager, IdentityRegistrarInterface $registrar, $config = [])
    {
        $this->tokenManager = $tokenManager;
        $this->registrar = $registrar;
        parent::__construct($config);
    }

    public function init()
    {
        $this->profile = $this->findProfile();

        if ($this->profile === null && $this->getModule()->enableFreeRegistration) {
            $this->profile = $this->createProfile();
        }

        if ($this->profile === null) {
            throw new InvalidParamException('Profile with given properties was not found');
        }

        parent::init();
    }

    /**
     * @return RegistrationProfile | null
     */
    protected function findProfile()
    {
        if ($this->tokenManager instanceof TokenProvidesPhoneMobileInterface) {
            return RegistrationProfile::findOne(['phone_mobile' => $this->tokenManager->getPhoneMobile()]);
        }

        if ($this->tokenManager instanceof TokenProvidesEmailInterface) {
            return RegistrationProfile::findOne(['email' => $this->tokenManager->getEmail()]);
        }

        return null;
    }

    /**
     * @return Module
     */
    private function getModule()
    {
        return \Yii::$app->getModule('profiles');
    }

    protected function createProfile()
    {
        $profile = new RegistrationProfile();
        if ($this->tokenManager instanceof TokenProvidesPhoneMobileInterface) {
            $profile->phone_mobile = $this->tokenManager->getPhoneMobile();
        }
        if ($this->tokenManager instanceof TokenProvidesEmailInterface) {
            $profile->email = $this->tokenManager->getEmail();
        }
        if ($this->tokenManager instanceof TokenProvidesFieldInterface) {
            $profile->{$this->tokenManager->getFieldName()} = $this->tokenManager->getFieldValue();
        }

        return $profile;
    }

    public function rules()
    {
        return [
            ['password', 'string'],
            ['password', 'required'],
            ['passwordCompare', 'string'],
            ['passwordCompare', 'required'],
            ['passwordCompare', 'compare', 'compareAttribute' => 'password', /*'when' => function() { return $this->askPassword; }*/],
            ['agreeWithTerms', 'compare', 'compareValue' => 1,
                'message' => 'Вы должны согласиться с условиями участния в программе'],
            ['allowPersonalDataProcessing', 'compare', 'compareValue' => 1,
                'message' => 'Вы должны разрешить обработку своих персональных данных для участния в программе'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'passwordCompare' => 'Подтверждение пароля',
            'agreeWithTerms' => 'Согласен / согласна с правилами участия в программе',
            'allowPersonalDataProcessing' => 'Даю согласие на обработку своих персональных данных',
        ];
    }

    public function loadAll($data, $formName = null)
    {
        $result = true;
        $result = $this->load($data, $formName) && $result;
        $result = $this->profile->load($data) && $result;

        return $result;
    }

    public function process()
    {
        if ($this->validateAll() === false) {
            return false;
        }

        $transaction = \Yii::$app->db->beginTransaction();

        $this->profile->save();
        $role =  \Yii::$app->request->get('role');
        if($role == Profile::ROLE_MANAGER_TT) {
            $roleRtt = new RegisterTTForm();
            if($roleRtt->process($this->profile->id) == false){
                return false;
            };
            $this->profile->updateAttributes(['role' => $role]);
        }elseif ($role == Profile::ROLE_MULTI_MANAGER_TT){
            $roleRtt = new RegisterMultiTTForm();
            if($roleRtt->process($this->profile->id) == false){
                return false;
            };
            $this->profile->updateAttributes(['role' => Profile::ROLE_MANAGER_TT]); // ROLE_MULTI_MANAGER_TT по сути тоже самое, что и ROLE_MANAGER_TT (Нужно для разграничения форм)
        }else{
            $roleRtt = new RegisterDistribForm();
            if($roleRtt->process($this->profile->id) == false){
                return false;
            };
            $this->profile->updateAttributes(['role' => $role]);
        }

        $this->profile->refresh();

        $this->identity = $this->registrar->createForProfile($this->profile, $this->password, $this->tokenManager);

        $this->profile->updateAttributes([
            'identity_id' => $this->identity->getId(),
        ]);

        $transaction->commit();

        $this->tokenManager->remove();

        return true;
    }

    public function validateAll()
    {
        $result = true;
        $result = $this->validate() && $result;
        $result = $this->profile->validate() && $result;
        return $result;
    }

    /**
     * @return Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @return RegistrationTokenManagerInterface
     */
    public function getTokenManager()
    {
        return $this->tokenManager;
    }
}