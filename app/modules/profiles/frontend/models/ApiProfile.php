<?php

namespace modules\profiles\frontend\models;

use modules\profiles\common\models\Profile;

class ApiProfile extends Profile
{

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['first_name', 'last_name'], 'required'],
        ]);
    }

    public function fields()
    {
        $fields = [
            'profile_id' => 'id',
            'phone_mobile',
            'action' => function (ApiProfile $model) {
                $dealer = $model->dealer;
                if (empty($dealer)) {
                    return null;
                }
                $action = ApiAction::findOne($dealer->action_id);
                if (empty($action)) {
                    return null;
                }

                return $action;
            },
            'role' => 'role_label',
            'email',
            'dealer_id' => function (ApiProfile $model) {
                $dealer = $model->dealer;
                if (empty($dealer)) {
                    return null;
                }

                return $dealer->id;
            },
            'dealer_name' => function (ApiProfile $model) {
                $dealer = $model->dealer;
                if (empty($dealer)) {
                    return null;
                }

                return $dealer->name;
            },
            'full_name',
            'first_name',
            'last_name',
            'middle_name',
            'balance' => function (ApiProfile $model) {
                return $model->purse->balance;
            },
            'created_at',
            'blocked_at',
            'blocking_reason',
            'push_news',
            'push_sales',
        ];

        return $fields;
    }
}