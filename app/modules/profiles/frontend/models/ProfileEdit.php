<?php

namespace modules\profiles\frontend\models;

use modules\profiles\backend\rbac\Rbac;
use modules\profiles\common\models\Profile;
use yz\admin\models\User;


/**
 * Class ProfileEdit
 */
class ProfileEdit extends Profile
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['first_name', 'required'],
            ['last_name', 'required'],
        ]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        /** @var User[] $admins */
        $admins = User::find()->byRoles(Rbac::ROLE_RECEIVE_PROFILE_CHANGE_NOTIFICATIONS)->all();

        foreach ($admins as $admin) {
            \Yii::$app->mailer->compose('@modules/profiles/frontend/mail/profileChanged', [
                'admin' => $admin,
                'profile' => $this,
            ])
                ->setTo($admin->email)
                ->send();
        }
    }


}