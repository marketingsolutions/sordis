<?php

namespace modules\profiles\frontend\widgets;
use modules\profiles\common\models\ProfileConfirmRtt;
use modules\profiles\frontend\forms\AddRttForm;
use modules\profiles\frontend\forms\DeleteRttForm;
use modules\sales\common\models\SalePoint;
use yii\base\Widget;

/**
 * Class ProfileTopMenu
 */
class AddRtt extends Widget
{
    public function run()
    {
        $newRtt = new AddRttForm();
        $dellRtt = new DeleteRttForm();
        if(\Yii::$app->request->post('AddRttForm') && $newRtt->process()){
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Ваши точки отправлены на согласованние модератору портала'));
        }
        if(\Yii::$app->request->post('DeleteRttForm') && $dellRtt->process()){
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Ваши точки отправлены на согласованние модератору портала'));
        }
        $moderationRtt = ProfileConfirmRtt::getModerationRtt();
        $profile = \Yii::$app->user->identity->profile;
        $myRtt = ProfileConfirmRtt::getRttDeleteData($profile->id);
        return $this->render('add-rtt',[
            'newRtt' => $newRtt,
            'dellRtt' => $dellRtt,
            'moderationRtt' => $moderationRtt,
            'profile' => $profile,
            'myRtt' => $myRtt,
        ]);
    }

}