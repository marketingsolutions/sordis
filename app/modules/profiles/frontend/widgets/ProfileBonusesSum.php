<?php

namespace modules\profiles\frontend\widgets;
use modules\bonuses\common\models\TempBonuses;
use modules\bonuses\common\models\TempConfirmedBonuses;
use modules\profiles\common\models\Profile;
use ms\loyalty\contracts\identities\IdentityInterface as loyaltyIdentityInterface;
use yii\base\Widget;
use yii\web\IdentityInterface;


/**
 * Class ProfileDashboard
 */
class ProfileBonusesSum extends Widget
{
    public function run()
    {
        $user = \Yii::$app->user;
        /** @var IdentityInterface|loyaltyIdentityInterface $identity */
        $identity = $user->identity;
        $profile = $identity->profile;

        return $this->render('profile-bonuses-sum', compact('user', 'identity', 'profile'));
    }

    /**
     * Бонусы по видам (неподтвержденные)
     * @param $profilePhoneMobile
     * @param $bonusType
     * @param null $period
     * @return int|mixed
     */
    public static function getMyBonus($profilePhoneMobile, $bonusType, $period=null)
    {
        if(!$period){
            $period = date("Y-m")."-01";
        }
        $bonusTypeSum = TempBonuses::find()
            ->where(['phone_mobile' => $profilePhoneMobile, 'bonus_type' => $bonusType, 'period' => $period])
            ->sum('bonus_sum');
        if(!$bonusTypeSum){
            return 0;
        }
        return $bonusTypeSum;
    }

    /**
     * Бонусы по видам (подтвержденные)
     * @param $profilePhoneMobile
     * @param $bonusType
     * @param null $period
     * @return int|mixed
     */
    public static function getMyConfirmBonus($profilePhoneMobile, $bonusType, $period=null)
    {
        if(!$period){
            $period = date("Y-m")."-01";
        }
        $bonusTypeSum = TempConfirmedBonuses::find()
            ->where(['phone_mobile' => $profilePhoneMobile, 'bonus_type' => $bonusType, 'period' => $period])
            ->sum('bonus_sum');
        if(!$bonusTypeSum){
            return 0;
        }
        return $bonusTypeSum;
    }

    /**
     * Неподтвержденные бонусы  по типам и РТТ (код ЕГАИС)
     * @param $profilePhoneMobile
     * @param $bonusType
     * @param null $period
     * @param $codeEgais
     * @return int|mixed
     */
    public static function getMyRttBonus($profilePhoneMobile, $bonusType, $period=null, $codeEgais)
    {
        if(!$period){
            $period = date("Y-m")."-01";
        }
        $bonusTypeSum = TempBonuses::find()
            ->where(['phone_mobile' => $profilePhoneMobile, 'bonus_type' => $bonusType, 'period' => $period, 'code_egais' => $codeEgais])
            ->sum('bonus_sum');
        if(!$bonusTypeSum){
            return 0;
        }
        return $bonusTypeSum;

    }

    public static function getMyConfirmRttBonus($profilePhoneMobile, $bonusType, $period=null, $codeEgais)
    {
        if(!$period){
            $period = date("Y-m")."-01";
        }
        $bonusTypeSum = TempConfirmedBonuses::find()
            ->where(['phone_mobile' => $profilePhoneMobile, 'bonus_type' => $bonusType, 'period' => $period, 'code_egais' => $codeEgais])
            ->sum('bonus_sum');
        if(!$bonusTypeSum){
            return 0;
        }
        return $bonusTypeSum;

    }



    /**
     * @return array
     */
    public static function getPeriodForSelect()
    {
        $periods = TempBonuses::find()
            ->select('period')
            ->groupBy('period')
            ->orderBy(['period' => SORT_DESC])
            ->asArray()
            ->all();
        $returnPeriod = [];
        $confirmPeriods = TempConfirmedBonuses::find()
            ->select('period')
            ->groupBy('period')
            ->orderBy(['period' => SORT_DESC])
            ->asArray()
            ->all();
        $month = [
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь',
        ];
        $period = array_merge($periods, $confirmPeriods);
        if($period) {
            foreach ($period as $date){
                $returnPeriod[$date['period']] = $month[date("m", strtotime($date['period']))]."-".date("Y", strtotime($date['period']));
            }
        }
        return array_unique($returnPeriod);
    }

}