<?php

use yii\helpers\Url;
use modules\profiles\frontend\widgets\ProfileBonusesSum;
use modules\bonuses\common\models\TempBonuses;
use modules\bonuses\common\models\TempConfirmedBonuses;
use modules\profiles\common\models\Profile;
use modules\sales\common\models\SalePoint;

/**
 * @var \yii\web\View $this
 * @var \yii\web\User $user
 * @var \yii\web\IdentityInterface $identity
 * @var \modules\profiles\common\models\Profile $profile
 */
$js = <<<JS
   $( document ).ready(function() {
    $( "#change_period" ).change(function() {
        var valPeriodGet = $(this).val();
        var urlTo = location.href;
        urlTo = urlTo.split("?", 1);
        console.log(urlTo);
        window.location = urlTo+"?period="+valPeriodGet;
    });
    $('.btn-bonus').click(function() {
      // $(this).toggleClass('active');
      if($(this).hasClass('bb1') && !$(this).hasClass('active')){
        $('.bb2').removeClass('active');
        $('.bb1').addClass('active');
        $(this).parents('section.content').find('.block-bb2').hide('fast');
        $(this).parents('section.content').find('.block-bb1').slideToggle('fast');
        $("html, body").animate({scrollTop: $(".block-bb1").offset().top },"slow");
      } else {
        $('.bb1').removeClass('active');
        $(this).parents('section.content').find('.block-bb1').hide('fast');
      }
      if($(this).hasClass('bb2') && !$(this).hasClass('active')){
        $('.bb1').removeClass('active');
        $('.bb2').addClass('active');
        $(this).parents('section.content').find('.block-bb1').hide('fast');
        $(this).parents('section.content').find('.block-bb2').slideToggle('fast');
        $("html, body").animate({scrollTop: $(".block-bb2").offset().top },"slow");
      } else {
        $('.bb2').removeClass('active');
        $(this).parents('section.content').find('.block-bb2').hide('fast');
      }
    });
});
JS;
$this->registerJs($js);
?>

<div class="col-lg-4 col-md-5 col-sm-6">
  <div class="buttons-wrap">
    <a href="#award-points" class="ancor-award">
      <?php if ($profile->role == Profile::ROLE_MANAGER_TT): ?>
      <div class="btn-bonus bb1">
      <?php else: ?>
      <div class="btn-bonus bb1">
      <?php endif; ?>
        <div class="btn-bonus--info">
          <?php if ($profile->role == Profile::ROLE_MANAGER_TT): ?>
            <span class="bonus-title"><?=$profile->purse->balance?></span>
          <?php else: ?>
            <span class="bonus-title"><?=$profile->purse->balance?></span>
          <?php endif; ?>
          подтвержденные баллы
        </div>
        <div class="btn-bonus--purse">
          <img src="/images/icon-purse.png" alt=""/>
        </div>
      </div>
    </a>


    <a href="#confirm-points">
        <?php if ($profile->role == Profile::ROLE_MANAGER_TT): ?>
      <div class="btn-bonus bb2">
          <?php else: ?>
        <div class="btn-bonus bb2">
            <?php endif; ?>
          <div class="btn-bonus--info">
              <?php if ($profile->role == Profile::ROLE_MANAGER_TT): ?>
                <span class="bonus-title"><?= TempBonuses::calculateBonusSum($bonusType = null, $period = null, $profile->phone_mobile, $codeEGAIS = null) ?></span>
              <?php else: ?>
                <span class="bonus-title"><?= TempBonuses::calculateBonusSum($bonusType = null, $period = null, $profile->phone_mobile, $codeEGAIS = null) ?></span>
              <?php endif; ?>
            начисленные баллы
          </div>
          <div class="btn-bonus--pig">
            <img src="/images/icon-pig.png" alt=""/>
          </div>
        </div>

      </div>
    </a>
  </div>

      <!--  Баллы за период: &nbsp;-->
      <!--  <select id="change_period">-->
      <!--    --><?php //foreach (ProfileBonusesSum::getPeriodForSelect() as $item => $value): ?>
      <!--      <option-->
      <!--        --><?php //if ($item == \Yii::$app->request->get('period')) { echo "selected"; } ?>
      <!--              value="--><? //= $item ?><!--">--><? //= $value ?>
      <!--      </option>-->
      <!--    --><?php //endforeach; ?>
      <!--  </select>-->

