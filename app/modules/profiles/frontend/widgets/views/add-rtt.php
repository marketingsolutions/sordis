<?php

use yii\helpers\Url;
use unclead\multipleinput\MultipleInput;
use modules\profiles\common\models\ProfileConfirmRtt;


/**
 * @var \yii\web\View $this
 * @var \yii\web\User $user
 * @var \yii\web\IdentityInterface $identity
 * @var \modules\profiles\common\models\Profile $profile
 */
$js=<<<JS
$( document ).ready(function() { 
    /*Добавление галочки в модальном окне при нажатии */
    $('.close-point').click(function() {
        $('.checkbox_rtt').each(function() {
            $(this).prop('checked', false);
        });
        var idSalePoint = parseInt($(this).data('idsalepoint'));
        $("#"+idSalePoint).prop('checked', true);
    });
});

JS;
$this->registerJs($js);
?>
<?php if ($profile->role == \modules\profiles\common\models\Profile::ROLE_MANAGER_TT): ?>
  <div id="add_rtt" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
       aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h2 id="gridModalLabel" class="modal-title">Добавить мои торговые точки</h2>
        </div>
        <div class="modal-body">
          <?php $form = \yii\bootstrap\ActiveForm::begin(['layout' => 'horizontal']) ?>

          <?= $form->field($newRtt, 'multiRtt')->widget(MultipleInput::className(), [
            'max' => 10,
            'rendererClass' => \unclead\multipleinput\renderers\ListRenderer::className(),
            'columns' => [
              [
                'name' => 'code_egais',
                'type' => \yii\widgets\MaskedInput::className(),
                'title' => 'Код ЕГАИС',
                'options' => [
                  'clientOptions' => [
                    'alias' => '999999999999',
                  ]
                ]
              ],
              [
                'name' => 'inn',
                'title' => 'ИНН',
                'type' => \yii\widgets\MaskedInput::className(),
                'options' => [
                  'clientOptions' => [
                    'alias' => '9999999999',
                  ]
                ]
              ],
              [
                'name' => 'license_number',
                'title' => 'Номер лицензии',
              ],
              [
                'name' => 'name',
                'title' => 'Наименование ТТ',
              ],
              [
                'name' => 'address',
                'title' => 'Адрес ТТ',
              ],
              [
                'name' => 'work_time_from',
                'type' => \yii\widgets\MaskedInput::className(),
                'title' => 'Время с',
                'options' => [
                  'clientOptions' => [
                    'alias' => '99:99',
                  ]
                ]
              ],
              [
                'name' => 'work_time_to',
                'type' => \yii\widgets\MaskedInput::className(),
                'title' => 'Время по',
                'options' => [
                  'clientOptions' => [
                    'alias' => '99:99',
                  ]
                ]
              ],
            ]
          ])->label(false);
          ?>

        </div>
        <ul style="color: red;font-size: 20px;" id="reg-errors"></ul>
        <div class="modal-footer">
          <button class="btn btn_all go">Отправить на согласование модератору</button>
          <button class="btn btn_blue" data-dismiss="modal">Закрыть</button>
        </div>
        <?php \yii\bootstrap\ActiveForm::end() ?>
      </div>
    </div>
  </div>
  <div class="modal fade bd-example-modal-lg2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
       aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h3 class="modal-title" id="gridModalLabel">Удалить мои торговые точки</h3>
        </div>
        <div class="modal-body">
          <h4 style="text-decoration: underline;" align="center">Выберите торговые точки, которые хотите удалить</h4>
          <?php $form2 = \yii\bootstrap\ActiveForm::begin(['layout' => 'horizontal']) ?>
          <?php foreach ($myRtt as $rtt): ?>
            <b><?= $form2->field($dellRtt, 'rtt_id[]')->checkbox(['value' => $rtt['ids'], 'id'=> $rtt['ids'], 'class'=> 'checkbox_rtt','label' => $rtt['sale_point_name'] . "| ЕГАИС:" . $rtt['egais'] . "|ИНН:" . $rtt['sale_point_inn'], ]) ?></b>
          <?php endforeach; ?>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn_all">Отправить на согласование модератору</button>
          <button class="btn btn_blue" data-dismiss="modal">Закрыть</button>
        </div>
        <?php \yii\bootstrap\ActiveForm::end() ?>
      </div>
    </div>
  </div>

  <div class="container">

      <!--<div class="alert alert-warning">-->
      <!--<h4>Уважаемый участник!</h4>-->
      <!--Если в процессе регистрации вами были добавили не все свои торговые точки, то Вы можете заполнить заявку на-->
      <!--их добавление.-->
      <!--После рассмотрения данной заявки модератором портала и успешного подтверждения данных торговых точек Вы-->
      <!--увидите их в списке своих торговых точек.<br/><br/>-->
      <!--<span data-toggle="modal" data-target=".bd-example-modal-lg" class="add_my_rtt">Добавить мои торговые точки</span>-->
      <!--<br>-->
      <!--<hr>-->
      <!--Если при регистрации Вы ввели данные по какой то торговой точке ошибочно, то можно заполнить заявку на-->
      <!--удаление лишних торговых точек.<br/><br/>-->
      <!--<span data-toggle="modal" data-target=".bd-example-modal-lg2" class="add_my_rtt">Удалить мои торговые точки</span>-->
      <!--</div>-->
      <?php if ($moderationRtt): ?>
      <div class="col-md-12">
        <div class="row">
        <h2>Торговые точки на модерации</h2>
          <?php $i = 0; ?>
          <?php foreach ($moderationRtt as $rtt): ?>
            <?php $i++; ?>
            <div class="moderation-block">
              <!--<span class="td1">--><?//= $i ?><!--</span>-->
              <?php //if ($rtt['type'] == ProfileConfirmRtt::TYPE_ADD): ?>
              <!--<span class="td2">--><?//= ProfileConfirmRtt::getType()[$rtt['type']] ?><!--</span>-->
              <?php //else: ?>
              <!--<span class="td2">--><?//= ProfileConfirmRtt::getType()[$rtt['type']] ?><!--</span>-->
              <?php //endif; ?>
              <div class="moderation-block--title"><?= $rtt['name'] ?></div>

              <div class="moderation-block--status">
                <?php if ($rtt['status'] == ProfileConfirmRtt::STATUS_NEW): ?>
                  <span class="label label-warning"><?= ProfileConfirmRtt::getStatus()[$rtt['status']] ?></span>
                <?php elseif ($rtt['status'] == ProfileConfirmRtt::STATUS_CANCEL): ?>
                  <span class="label label-danger"><?= ProfileConfirmRtt::getStatus()[$rtt['status']] ?></span>
                <?php elseif ($rtt['status'] == ProfileConfirmRtt::STATUS_DELETED): ?>
                  <span class="label label-danger"><?= ProfileConfirmRtt::getStatus()[$rtt['status']] ?></span>
                <?php endif; ?>
              </div>

              <div class="moderation-block--info">
                <div><?= $rtt['address'] ?></div>
                <div>Код ЕГАИС: <?= $rtt['code_egais'] ?>, Номер лицензии: <?= $rtt['license_number'] ?></div>
              </div>


            </div>
          <?php endforeach; ?>
        </div>
      </div>
      <?php endif; ?>

  </div>
<?php endif; ?>
