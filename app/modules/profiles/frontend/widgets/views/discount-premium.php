<?php

use yii\helpers\Html;

?>
<div class="dealer-forms">

  <div class="col-md-6">
    <h3 class="header_top">Скидка на поставку продукции</h3>
    <div class="panel panel_ferma">
        <?php $form = \yii\bootstrap\ActiveForm::begin(['layout' => 'horizontal', 'action' => ['/dashboard/add-discount']]) ?>
        <?= $form->field($discount, 'discountSum')->label(false) ?>
      <button type="submit" class="btn btn_blue">Списать</button>
      <div class="button-bottom">
        <a href="/dashboard/discount" class="btn btn_all">История списаний на скидку</a>
      </div>
        <?php \yii\bootstrap\ActiveForm::end() ?>
    </div>
  </div>

  <div class="col-md-6">
    <h3 class="header_top">Премия к договору поставки</h3>
    <div class="panel panel_ferma">
        <?php $form = \yii\bootstrap\ActiveForm::begin(['layout' => 'horizontal', 'action' => ['/dashboard/add-premium']]) ?>
        <?= $form->field($premium, 'premiumSum')->label(false) ?>
      <button type="submit" class="btn btn_blue">Списать</button>
      <div class="button-bottom">
        <a href="/dashboard/premium" class="btn btn_all">История списаний на премию</a>
      </div>
        <?php \yii\bootstrap\ActiveForm::end() ?>
    </div>
  </div>
</div>

