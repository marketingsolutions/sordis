<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \yii\web\User $user
 * @var \yii\web\IdentityInterface $identity
 * @var \modules\profiles\common\models\Profile $profile
 */
?>
<a class="name-link" href="<?= Url::to(['/profiles/profile/index']) ?>">
  <span><?= Html::encode($profile->first_name) ?></span>
  <span><?= Html::encode($profile->last_name) ?></span>
</a>
