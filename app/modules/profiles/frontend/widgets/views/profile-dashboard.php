<?php
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \yii\web\User $user
 * @var \yii\web\IdentityInterface $identity
 * @var \modules\profiles\common\models\Profile $profile
 */
?>

<div class="col-md-3">
    <div class="panel panel-info">
        <div class="panel-heading">
            Бонусный баланс
        </div>
        <div class="panel-body">
            <a href="<?= Url::to(['/profiles/bonuses-transactions/index']) ?>"><?= $profile->purse->balance ?>
                баллов</a>
        </div>
    </div>
</div>