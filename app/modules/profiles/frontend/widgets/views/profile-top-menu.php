<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 */

$js=<<<JS
$( document ).ready(function() {
    $('.list-group-item2, .list-group-item2 > ul').mouseover(function() {
        $('.list-group-item2 > ul').show();
    });
     $('.list-group-item2, .list-group-item2 > ul').mouseout(function() {
        $('.list-group-item2 > ul').hide();
    });
     /*Корявая подсветка меню*/
    var currentUrl = window.location.pathname;
    if(currentUrl=='/news/news'){
        $("a[href^='/news/news']").parent().addClass('active')
    }
    //Подмена ссылок при заказе ЭПС
    if(currentUrl=='/catalog/orders/create'){
        $('a:contains("Продолжить выбор")').attr('href', '/dashboard/prizes')
    }
    
});

JS;
$this->registerJs($js);

/*Файлик с правилами акции по ролям*/
if(isset(\Yii::$app->user->identity->profile->role) && \Yii::$app->user->identity->profile->role==\modules\profiles\common\models\Profile::ROLE_DISTRIBUTOR){
    $fileLink = '/media/uploads/rules_distr.pdf';
}elseif(isset(\Yii::$app->user->identity->profile->role) && \Yii::$app->user->identity->profile->role==\modules\profiles\common\models\Profile::ROLE_MANAGER_TT){
    $fileLink = '/media/uploads/rules_rtt.pdf';
}else{
    $fileLink = '/media/uploads/rules.pdf';
}

?>

<?= \yii\widgets\Menu::widget([
    'options' => [
        'tag' => false
    ],
    'items' => [
       // ['label' => 'Главная', 'url' => ['/dashboard/index'], 'visible' => ! Yii::$app->user->isGuest],
        ['label' => 'Тратить баллы', 'url' => ['/dashboard/prizes'], 'visible' => ! Yii::$app->user->isGuest],
        ['label' => 'Дополнительные баллы', 'url' => ['#'], 'options'=>['class'=>'list-group-item2 active'],'visible' => ! Yii::$app->user->isGuest, 'items' =>[
            ['label' => 'Призовые акции', 'url' => ['/social/social/index'], 'visible' => !Yii::$app->user->isGuest && isset(\Yii::$app->user->identity->profile->role) && \Yii::$app->user->identity->profile->role!=\modules\profiles\common\models\Profile::ROLE_DISTRIBUTOR ],
            ['label' => 'Целевые акции', 'url' => ['/bonuses/target-bonus/index'], 'visible' => ! Yii::$app->user->isGuest],
            ['label' => 'Опросы', 'url' => ['/polls/poll'], 'visible' => !Yii::$app->user->isGuest && isset(\Yii::$app->user->identity->profile->role) && \Yii::$app->user->identity->profile->role!=\modules\profiles\common\models\Profile::ROLE_DISTRIBUTOR ]
            ],
        ],
        ['label' => 'Калькулятор', 'url' => ['/calc/calc/index'], 'visible' => ! Yii::$app->user->isGuest],
        ['label' => 'Новости', 'url' => ['/news/news'], 'visible' => ! Yii::$app->user->isGuest],
        ['label' => 'Об акции', 'url' => [$fileLink], 'visible' => ! Yii::$app->user->isGuest],
    ]
]) ?>
