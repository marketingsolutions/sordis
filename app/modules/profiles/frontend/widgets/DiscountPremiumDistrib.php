<?php

namespace modules\profiles\frontend\widgets;
use modules\profiles\common\models\ProfileConfirmRtt;
use modules\profiles\frontend\forms\AddRttForm;
use modules\profiles\frontend\forms\DeleteRttForm;
use modules\profiles\frontend\forms\DiscountForm;
use modules\profiles\frontend\forms\PremiumForm;
use modules\sales\common\models\SalePoint;
use yii\base\Widget;

/**
 * Class ProfileTopMenu
 */
class DiscountPremiumDistrib extends Widget
{
    public function run()
    {
        $premium = new PremiumForm();
        $discount = new DiscountForm();

        return $this->render('discount-premium', [
            'premium' => $premium,
            'discount' => $discount,
        ]);
    }

}