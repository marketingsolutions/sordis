<?php
namespace modules\profiles\frontend\forms;
/**
 * Created by PhpStorm.
 * User: MihailKri
 * Date: 12.09.2018
 * Time: 13:43
 */
use modules\sales\common\models\Distributor;
use modules\sales\common\models\DistributorProfile;
use yii\base\Model;

class RegisterDistribForm extends Model
{
    /**
     * @var
     */
    public $license_number;
    /**
     * @var
     */
    public $inn;
    /**
     * @var
     */
    public $distrib_name;
    /**
     * @var
     */
    public $distrib_address;
    /**
     * @var
     */
    public $egais;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['license_number','inn', 'distrib_name', 'distrib_address', 'egais'], 'required'],
            ['license_number', 'string', 'max' => 12, 'min' =>12],
            ['egais', 'string', 'max' => 12, 'min' =>12],
            ['inn', 'string', 'max' => 10, 'min' =>10],
            ['inn', 'validInn', 'skipOnEmpty'=> true],
            ['egais', 'validEgais', 'skipOnEmpty'=> false],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(){
        return [
            'license_number' => 'Номер лицензии',
            'inn' => 'ИНН',
            'distrib_name' => 'Наименование дистрибьютора',
            'distrib_address' => 'Юр. адрес',
            'egais' => 'Код ЕГАИС',
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validInn($attribute, $params){
        $idDistrib = Distributor::findOne(['inn' => $this->$attribute]);
        if(!$idDistrib){
            $this->addError($attribute,'Данный номер ИНН не найден в системе!');
        }else{
            $isDistrib = DistributorProfile::findOne(['distrib_id' => $idDistrib->id]);
            if($isDistrib){
                $this->addError($attribute,'Данный номер ИНН занят другим участником!');
            }
        }
    }
     public function validEgais($attribute, $params){
         $idDistrib = Distributor::findOne(['inn' => $this->inn]);

         if($idDistrib && $idDistrib->egais != $this->$attribute){
             $this->addError($attribute,'У дистрибьютора с ИНН - '.$this->inn.' неверный код ЕГАИС!');
         }
     }

    /**
     * @param $profileId
     * @return bool
     */
    public function process($profileId){
        $codeEgais = trim(\Yii::$app->request->post('RegisterDistribForm')['egais']);
        $inn = trim(\Yii::$app->request->post('RegisterDistribForm')['inn']);
        $idDistrib = $this->getDistribId($codeEgais, $inn);
        if($idDistrib){
            //Добавляем связь между дистрибьютором и профилем
            $transaction = \Yii::$app->db->beginTransaction();
            $newDistribProfile = new DistributorProfile();
            $newDistribProfile->profile_id = $profileId;
            $newDistribProfile->distrib_id = $idDistrib;
            $newDistribProfile->save(false);
            //Редактируем данные у дистрибьютора
            $distrib = Distributor::findOne(['id' => $idDistrib]);
            $distrib->distrib_name = trim(\Yii::$app->request->post('RegisterDistribForm')['distrib_name']);
            $distrib->distrib_address = \Yii::$app->request->post('RegisterDistribForm')['distrib_address'];
            $distrib->license_number = \Yii::$app->request->post('RegisterDistribForm')['license_number'];
            $distrib->updated_at = date("Y-m-d H:i:s");
            $distrib->save(false);
            $transaction->commit();
            return true;
        }else{
            return false;
        }
    }
    public function getDistribId($code_egais, $inn){
        $model = Distributor::findOne(['egais' => $code_egais, 'inn' => $inn]);
        if($model && $this->isRealDistrib($model->id)){
            return $model->id;
        }else{
            return false;
        }
    }

    public function isRealDistrib($distribId){
        $model = DistributorProfile::findOne(['distrib_id' => $distribId]);
        if(!$model){
            return true;
        }else{
            return false;
        }
    }

}