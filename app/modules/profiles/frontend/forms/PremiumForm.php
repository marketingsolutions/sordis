<?php
namespace modules\profiles\frontend\forms;
/**
 * Created by PhpStorm.
 * User: MihailKri
 * Date: 12.09.2018
 * Time: 13:42
 */
use modules\profiles\common\models\AllowPersonalInfo;
use modules\profiles\common\models\Premium;
use modules\profiles\common\models\Profile;
use modules\sales\common\models\Managers;
use ms\loyalty\finances\common\models\Transaction;


use yii\base\Model;

class PremiumForm extends Model
{

    /**
     * @var
     */
    public $premiumSum;

    /** @var  Profile */
    public $profile;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['premiumSum'], 'required'],
            [['premiumSum'], 'number', 'min' => 1],
            ['premiumSum', 'checkProfileBalance']
        ];
    }

    public function checkProfileBalance()
    {
        $this->profile = \Yii::$app->user->identity->profile;

        if($this->premiumSum > $this->profile->purse->balance ){
            return $this->addError('premiumSum', 'ПРЕМИЯ НЕ СПИСАНА!!! Сумма скидки превышает сумму баланса Вашего баланса кошелька!');
        }

    }
    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'premiumSum' => 'Сумма премии',
        ];
    }

    public function process()
    {
        $now = date("Y-m-d H:i:s");
        $transactionNow = date("d.m.Y H:i", strtotime($now));
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            //Добавляем новую премию
            $model = new Premium();
            $model->profile_id = $this->profile->id;
            $model->premium_sum = $this->premiumSum;
            $model->bonus_sum = $this->premiumSum;
            $model->remainder_sum = $this->premiumSum;
            $model->created_at = $now;
            $model->save();

            //Списываем баллы
            $t = new Transaction();
            $t->type = Transaction::OUTBOUND;
            $t->amount = $this->premiumSum;
            $t->partner_type = Premium::class;
            $t->partner_id = $this->profile->id;
            $t->title = "Списание баллов по участнику #{$this->profile->id} за премию  {$this->premiumSum} баллов  от {$transactionNow}";
            $t->purse_id = $this->profile->purse->id;
            $t->save(false);

            //Отправляем письмо своему менеджеру ТО
            $subject ='Новая премия у участника Дистрибютора на портале kupi-nakopi.msforyou.ru';
            $message = "<h2>Новая премия у участника Дистрибютора</h2> <p>Участник - дистрибьютор {$this->profile->full_name} оформил новую премию в размере {$this->premiumSum}</p>";
            $message .= "<br/> Для более подробной информации зайдите на портал в раздел Скидки-премии";
            Managers::sendMailToManagerTo($this->profile->id, $subject ,$message);

            $transaction->commit();
            return true;
        }
        catch (\Exception $e) {
            $transaction->rollBack();
            $this->addError('premiumSum', 'ПРЕМИЯ НЕ СПИСАНА!!! Ошибка при сохранении премии');
        }

    }
}
