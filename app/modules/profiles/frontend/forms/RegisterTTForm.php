<?php
namespace modules\profiles\frontend\forms;
/**
 * Created by PhpStorm.
 * User: MihailKri
 * Date: 12.09.2018
 * Time: 13:42
 */
use modules\sales\common\models\SalePoint;
use modules\sales\common\models\SalePointProfile;
use yii\base\Model;

class RegisterTTForm extends Model
{
    /**
     * @var
     */
    public $code_egais;
    /**
     * @var
     */
    public $inn;
    /**
     * @var
     */
    public $name;
    /**
     * @var
     */
    public $address;
    /**
     * @var
     */
    public $work_time_from;
    /**
     * @var
     */
    public $work_time_to;
    /**
     * @var
     */
    public $license_number;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['code_egais', 'inn', 'name', 'address', 'license_number', 'work_time_from', 'work_time_to'], 'required'],
            ['license_number', 'string', 'max' => 12, 'min' =>12],
            ['code_egais', 'string', 'max' => 12, 'min' =>12],
            ['code_egais', 'validEgais', 'skipOnEmpty'=> false],
            ['inn', 'validInn', 'skipOnEmpty'=> false],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'code_egais' => 'Код ЕГАИС',
            'inn' => 'ИНН',
            'name' => 'Наименование ТТ',
            'address' => 'Адрес ТТ',
            'license_number' => 'Номер лицензии',
            'work_time_from' => 'Время с',
            'work_time_to' => 'Время по',
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validEgais($attribute, $params){
        $isBusyEgais = SalePoint::findOne(['code_egais' => $this->$attribute]);
        if(!$isBusyEgais){
            $this->addError($attribute,'Данный код ЕГАИС не найден в системе!');
        }else{
            $model = SalePointProfile::findOne(['sale_point_id' => $isBusyEgais->id]);
            if($model){
                $this->addError($attribute,'Данный код ЕГАИС занят другим пользователем!');
            }
        }
    }

    public function validInn($attribute, $params){
        $isBusyEgais = SalePoint::findOne(['code_egais' => $this->code_egais]);
        if($isBusyEgais && $isBusyEgais->inn != $this->$attribute ){
            $this->addError($attribute,'Введенный ИНН - '.$this->$attribute.' не соответствует введенному коду ЕГАИС - '.$this->code_egais.' у РТТ');
        }
    }

    /**
     * @param $profileId
     * @return bool
     * @throws \yii\db\Exception
     */
    public function process($profileId){
        $codeEgais = trim(\Yii::$app->request->post('RegisterTTForm')['code_egais']);
        $inn = trim(\Yii::$app->request->post('RegisterTTForm')['inn']);
        $idTT = $this->getTT($codeEgais, $inn);
        if($idTT){
            $transaction = \Yii::$app->db->beginTransaction();
            //Добавляем связь с РТТ
            $newTT = new SalePointProfile();
            $newTT->profile_id = $profileId;
            $newTT->sale_point_id = $idTT;
            $newTT->save(false);
            //Обновляем данные по РТТ
            $newDataForRtt = SalePoint::findOne(['code_egais' => $codeEgais]);
            $newDataForRtt->name =\Yii::$app->request->post('RegisterTTForm')['name'];
            $newDataForRtt->address = \Yii::$app->request->post('RegisterTTForm')['address'];
            $newDataForRtt->license_number = \Yii::$app->request->post('RegisterTTForm')['license_number'];
            $newDataForRtt->work_time_from = \Yii::$app->request->post('RegisterTTForm')['work_time_from'].":00";
            $newDataForRtt->work_time_to = \Yii::$app->request->post('RegisterTTForm')['work_time_to'].":00";
            $now = date("Y-m-d H:i:s");
            $newDataForRtt->updated_at = $now;
            $newDataForRtt->save(false);
            $transaction->commit();
            return true;
        }else{
            return false;
        }

    }

    /**
     * Получаем id ТТ или false
     * @param $codeEgais
     * @param $inn
     * @return bool|int
     */
    public function getTT($codeEgais, $inn){
        $model = SalePoint::findOne(['code_egais'=>$codeEgais]);
        if($model && $this->isRealRtt($model->id)){
            return $model->id;
        }else{
            return false;
        }
    }

    /**
     * Проверка занята ли эта РТТ другим пользователем
     * @param $idTT
     * @return bool
     */
    public function isRealRtt($idTT){
        $model = SalePointProfile::findOne(['sale_point_id' => $idTT]);
        if($model){
            return false;
        }else{
            return true;
        }
    }
}