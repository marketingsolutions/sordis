<?php
namespace modules\profiles\frontend\forms;
/**
 * Created by PhpStorm.
 * User: MihailKri
 * Date: 12.09.2018
 * Time: 13:42
 */
use modules\sales\common\models\SalePoint;
use modules\sales\common\models\SalePointProfile;
use modules\profiles\common\models\ProfileConfirmRtt;

use yii\base\Model;

class DeleteRttForm extends Model
{
    public $rtt_id=[];



    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['rtt_id', 'safe'],
            ['rtt_id', 'isEmptyArrRtt', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'rtt_id' => 'ID РТТ',
        ];
    }

    public function isEmptyArrRtt($attribute, $params){
        print_r($this->rtt_id);
        exit;
        if(empty($this->rtt_id)){
            $this->addError('rtt_id', 'Необходимо выбрать хотя бы одну точку для удаления!');
        }
    }
    public function process(){
        $arrDelRtt = \Yii::$app->request->post('DeleteRttForm')['rtt_id'];
        $profile = \Yii::$app->user->identity->profile;
        $now = date("Y-m-d H:i:s");
        foreach ($arrDelRtt as $idSalePoint){
            if($idSalePoint) {
                $rttData = SalePoint::findOne(['id' => $idSalePoint]);
                $del = new ProfileConfirmRtt();
                $del->code_egais = $rttData->code_egais;
                $del->inn = $rttData->inn;
                $del->name = $rttData->name;
                $del->address = $rttData->address;
                $del->license_number = $rttData->license_number;
                $del->created_at = $now;
                $del->profile_id = $profile->id;
                $del->type = ProfileConfirmRtt::TYPE_DELETE;
                $del->status = ProfileConfirmRtt::STATUS_NEW;
                $del->save(false);
            }
        }
        return true;
    }
}
