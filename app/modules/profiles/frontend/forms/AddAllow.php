<?php
namespace modules\profiles\frontend\forms;
/**
 * Created by PhpStorm.
 * User: MihailKri
 * Date: 12.09.2018
 * Time: 13:42
 */
use modules\profiles\common\models\AllowPersonalInfo;
use modules\profiles\common\models\Profile;


use yii\base\Model;

class AddAllow extends Model
{

    /**
     * @var
     */
    public $first_name;
    /**
     * @var
     */
    public $last_name;
    /**
     * @var
     */
    public $middle_name;
    /**
     * @var
     */
    public $email;
    /**
     * @var
     */
    public $phone_mobile;
    /**
     * @var
     */
    public $message;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['first_name'], 'required'],
            [['last_name'], 'required'],
            [['middle_name'], 'required'],
            [['email'], 'required'],
            [['phone_mobile'], 'required'],
            [['message'], 'required'],
            [['message'], 'trim'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'message' => 'Сообщение администратору',
            'first_name' => 'Имя',
            'middle_name' => 'Отчество',
            'email' => 'E-mail',
            'phone_mobile' => 'Телефон',
            'last_name' => 'Фамилия',
        ];
    }

    public function process()
    {
        $now = date("Y-m-d H:i:s");
        $profile = \Yii::$app->user->identity->profile;
        $addNewMessage = new AllowPersonalInfo();
        $addNewMessage->profile_id = $profile->id;
        $addNewMessage->last_name = \Yii::$app->request->post('AddAllow')['last_name'];
        $addNewMessage->first_name = \Yii::$app->request->post('AddAllow')['first_name'];
        $addNewMessage->middle_name = \Yii::$app->request->post('AddAllow')['middle_name'];
        $addNewMessage->email = \Yii::$app->request->post('AddAllow')['email'];
        $addNewMessage->phone_mobile_local = \Yii::$app->request->post('AddAllow')['phone_mobile'];
        $addNewMessage->user_message = \Yii::$app->request->post('AddAllow')['message'];
        $addNewMessage->created_at = $now;
        if($addNewMessage->save()){
            return true;
        }
        return false;
    }
}
