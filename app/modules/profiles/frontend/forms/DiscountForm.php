<?php
namespace modules\profiles\frontend\forms;
/**
 * Created by PhpStorm.
 * User: MihailKri
 * Date: 12.09.2018
 * Time: 13:42
 */
use modules\profiles\common\models\AllowPersonalInfo;
use modules\profiles\common\models\Discount;
use modules\profiles\common\models\Profile;
use modules\sales\common\models\DistributorProfile;
use modules\sales\common\models\Managers;
use ms\loyalty\finances\common\models\Transaction;

use yii\base\Model;

class DiscountForm extends Model
{

    /**
     * @var
     */
    public $discountSum;

    /** @var  Profile */
    public $profile;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['discountSum'], 'required'],
            [['discountSum'], 'number', 'min' => 1],
            ['discountSum', 'checkProfileBalance']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'discountSum' => 'Сумма скидки',
        ];
    }


    public function checkProfileBalance()
    {
        $this->profile = \Yii::$app->user->identity->profile;

        if($this->discountSum > $this->profile->purse->balance ){
            return $this->addError('discountSum', 'СКИДКА НЕ СПИСАНА!!! Сумма скидки превышает сумму баланса Вашего баланса кошелька!');
        }

    }

    /**
     * @return bool
     */
    public function process()
    {
        $now = date("Y-m-d H:i:s");
        $transactionNow = date("d.m.Y H:i", strtotime($now));
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            //Добавляем новую скидку
            $model = new Discount();
            $model->profile_id = $this->profile->id;
            $model->discount_sum = $this->discountSum;
            $model->bonus_sum = $this->discountSum;
            $model->remainder_sum = $this->discountSum;
            $model->created_at = $now;
            $model->save();

            //Списываем с баланса
            $t = new Transaction();
            $t->type = Transaction::OUTBOUND;
            $t->amount = $this->discountSum;
            $t->partner_type = Discount::class;
            $t->partner_id = $this->profile->id;
            $t->title = "Списание баллов по участнику #{$this->profile->id} за скидку  {$this->discountSum} баллов  от {$transactionNow}";
            $t->purse_id = $this->profile->purse->id;
            $t->save(false);

            //Отправляем письмо своему менеджеру ТО
            $subject ='Новая скидка у участника Дистрибютора на портале kupi-nakopi.msforyou.ru';
            $message = "<h2>Новая скидка у участника Дистрибютора</h2> <p>Участник - дистрибьютор {$this->profile->full_name} оформил новую скидку в размере {$this->discountSum}</p>";
            $message .= "<br/> Для более подробной информации зайдите на портал в раздел Скидки-премии";
            Managers::sendMailToManagerTo($this->profile->id, $subject ,$message);
            $transaction->commit();
            return true;
        }
        catch (\Exception $e) {
            $transaction->rollBack();
            $this->addError('discountSum', 'СКИДКА НЕ СПИСАНА!!! Ошибка при сохранении скидки');
        }
    }
}
