<?php
namespace modules\profiles\frontend\forms;
/**
 * Created by PhpStorm.
 * User: MihailKri
 * Date: 12.09.2018
 * Time: 13:42
 */
use modules\sales\common\models\SalePoint;
use modules\sales\common\models\SalePointProfile;
use yii\base\Model;

class RegisterMultiTTForm extends Model
{
    public $multiRtt;
    /**
     * @var
     */
    public $code_egais=[];
    /**
     * @var
     */
    public $inn;
    /**
     * @var
     */
    public $name;
    /**
     * @var
     */
    public $address;
    /**
     * @var
     */
    public $work_time_from;
    /**
     * @var
     */
    public $work_time_to;
    /**
     * @var
     */
    public $license_number;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['multiRtt'], 'required'],
            [['code_egais', 'inn', 'license_number'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            ['multiRtt', 'validRtt', 'skipOnEmpty'=> false],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'code_egais' => 'Код ЕГАИС',
            'inn' => 'ИНН',
            'name' => 'Наименование ТТ',
            'address' => 'Адрес ТТ',
            'license_number' => 'Номер лицензии',
            'work_time_from' => 'Время с',
            'work_time_to' => 'Время по',
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validRtt($attribute, $params){
       $arrDynamicField = $this->$attribute;
       $arrValidEgais = [];
       foreach ($arrDynamicField as $valitItem){
           $codeEgais = trim($valitItem['code_egais']);
           $arrValidEgais[]=$codeEgais;
           $inn = trim($valitItem['inn']);
           $licenseNum = trim($valitItem['license_number']);
           $model = SalePoint::findOne(['code_egais' => $codeEgais]);
           if(!$model){
               $this->addError($attribute, 'Код ЕГАИС '.$codeEgais.' не найден в системе!');
           }else{
                $relModel = SalePointProfile::findOne(['sale_point_id' => $model->id]);
                if($relModel){
                    $this->addError($attribute, 'Код ЕГАИС '.$codeEgais.' занят другим участником!');
                }
                if($model->inn != $inn){
                    $this->addError($attribute, 'Инн - '.$inn.' не соответствует введенному коду ЕГАИС - '.$codeEgais.' занят другим участником!');
                }
                if(strlen($licenseNum)!=12){
                    $this->addError($attribute, 'Номер лицензии - '.$licenseNum.' должен содержать 12 символов!');
                }
           }
       }
        /*Проверка на дубли массива с ЕГАИС*/
        $egaisArrIsDuble=[];
        foreach ($arrValidEgais as $egais){
            if(in_array( $egais, $egaisArrIsDuble)){
                $this->addError($attribute, 'Проверьте правильность заполнения номеров ЕГАИС! Номер ЕГАИС - '.$egais.' не должен повторяться у нескольких торговых точек!');
            }
            $egaisArrIsDuble[]=$egais;
        }

    }

    /**
     * @param $profileId
     * @return bool
     * @throws \yii\db\Exception
     */
    public function process($profileId){
//        $codeEgais = trim(\Yii::$app->request->post('RegisterTTForm')['code_egais']);
//        $inn = trim(\Yii::$app->request->post('RegisterTTForm')['inn']);
        $rttDataArr = \Yii::$app->request->post('RegisterMultiTTForm')['multiRtt'];
//        echo "<pre>";
//        print_r($rttDataArr);
//        echo "</pre>";
//        exit;
        $idTT = $this->getTT($rttDataArr);
        if($idTT){
            $transaction = \Yii::$app->db->beginTransaction();
            //Добавляем связь с несколькими РТТ
            foreach ($idTT as $item) {
                $newTT = new SalePointProfile();
                $newTT->profile_id = $profileId;
                $newTT->sale_point_id = $item;
                $newTT->save(false);
            }
            foreach ($rttDataArr as $rtt){
            //Обновляем данные по РТТ
            $newDataForRtt = SalePoint::findOne(['id' => $item]);
            $newDataForRtt->name =$rtt['name'];
            $newDataForRtt->address = $rtt['address'];
            $newDataForRtt->license_number = $rtt['license_number'];
            $newDataForRtt->work_time_from = $rtt['work_time_from'].":00";
            $newDataForRtt->work_time_to = $rtt['work_time_to'].":00";
            $now = date("Y-m-d H:i:s");
            $newDataForRtt->updated_at = $now;
            $newDataForRtt->save(false);
            }
            $transaction->commit();
            return true;
        }else{
            return false;
        }

    }

    /**
     * Получаем id ТТ или false
     * @param $codeEgais
     * @param $inn
     * @return bool|int
     */
    public function getTT($rttDataArr){
        $arrId = [];
        $countRtt = 0;
        foreach ($rttDataArr as $rttItem){
            $countRtt++;
            $arrId[]= $rttItem['code_egais'];
        }
        $model = SalePoint::find()->where(['code_egais'=>$arrId])->asArray()->all();
        $countModel = SalePoint::find()->where(['code_egais'=>$arrId])->count();
       if(!$model || $countModel != $countRtt){
           return false;
       }
       $salePointId=[];
       foreach ($model as $item){
           $salePointId[]=$item['id'];
       }
       return $salePointId;
    }
}