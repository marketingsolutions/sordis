<?php
namespace modules\profiles\frontend\forms;
/**
 * Created by PhpStorm.
 * User: MihailKri
 * Date: 12.09.2018
 * Time: 13:42
 */
use modules\sales\common\models\SalePoint;
use modules\sales\common\models\SalePointProfile;
use modules\profiles\common\models\ProfileConfirmRtt;

use yii\base\Model;

class AddRttForm extends Model
{
    public $multiRtt;
    /**
     * @var
     */


    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['multiRtt'], 'required'],
            [['code_egais', 'inn', 'license_number'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            //['multiRtt', 'validRtt', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'code_egais' => 'Код ЕГАИС',
            'inn' => 'ИНН',
            'name' => 'Наименование ТТ',
            'address' => 'Адрес ТТ',
            'license_number' => 'Номер лицензии',
            'work_time_from' => 'Время с',
            'work_time_to' => 'Время по',
        ];
    }

    public function process(){
        $arrNewRtt = \Yii::$app->request->post('AddRttForm')['multiRtt'];
        $profile = \Yii::$app->user->identity->profile;
        foreach ($arrNewRtt as $rtt){
            $new = new ProfileConfirmRtt();
            $new->code_egais = $rtt['code_egais'];
            $new->inn = $rtt['inn'];
            $new->name = $rtt['name'];
            $new->address = $rtt['address'];
            $new->license_number = $rtt['license_number'];
            $new->work_time_from = date("H:i:s", strtotime($rtt['work_time_from']));
            $new->work_time_to = date("H:i:s", strtotime($rtt['work_time_to']));
            $new->created_at = date("Y-m-d H:i:s");
            $new->profile_id = $profile->id ;
            $new->type = ProfileConfirmRtt::TYPE_ADD ;
            $new->status = ProfileConfirmRtt::STATUS_NEW;
            $new->save(false);
        }
        return true;
    }
}
