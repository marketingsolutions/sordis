<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \yz\admin\models\User $admin
 * @var \modules\profiles\frontend\models\ProfileEdit $profile
 * @var \yii\mail\BaseMessage $message instance of newly created mail message
 */
$message->setSubject('Уведомление об изменении профиля');
?>

<p>Уважаемый (ая), <?= Html::encode($admin->name) ?></p>

<p>Уведомляем Вас о том, что участник <i><?= Html::encode($profile->full_name) ?></i> изменил свой профиль.</p>


