<?php
/** @var Profile $model */

use modules\profiles\common\models\Profile;

?>
<div class="row" style="margin-bottom:20px; text-align: center">
	<div class="col-sm-12">
		<a href="<?= $model->getAvatarUrl() ?>" target="_blank"
		   class="btn btn-default">
			<img src="<?= $model->getAvatarUrl() ?>"
				 style="max-width:200px; max-height:100px;"/>
		</a>
	</div>
</div>