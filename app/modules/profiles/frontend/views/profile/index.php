<?php

use modules\profiles\common\models\Profile;
use modules\profiles\common\models\AllowPersonalInfo;

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\frontend\models\ProfileEdit $model
 */

$this->title = 'Персональные данные';
$this->params['header'] = $this->title;
$this->params['breadcrumb'] = $this->title;
?>

<div class="col-md-8">
    <?php if (Profile::isAllow($model->id)): ?>
      <div class="alert alert-success" role="alert">Модератор одобрил изменение Ваших личных данных.</div>
    <?php else: ?>
      <div class="alert alert-danger" role="alert">Изменение личных данных запрещено Модератором. Для отправки запроса
        на изменение личных данных нажмите на кнопку "Изменить данные" или пройдите по этой
        <a href="/profiles/profile/allow">ссылке</a>
      </div>
    <?php endif; ?>
    <?php if(AllowPersonalInfo::getLastInfo($profile->id)):?>
        <div class="alert alert-info" role="alert"><strong>Сообщение от модератора</strong><br/><?=AllowPersonalInfo::getLastInfo($profile->id)?></div>
    <?php endif;?>
    <?php $form = \yii\bootstrap\ActiveForm::begin(['layout' => 'horizontal']) ?>

  <div class="panel-body panel_ferma padding_top_bottom">
      <?= $form->field($model, 'last_name') ?>
      <?= $form->field($model, 'first_name') ?>
      <?= $form->field($model, 'middle_name') ?>
      <?= $form->field($model, 'email') ?>
      <?= $form->field($model, 'phone_mobile_local')->widget(\yii\widgets\MaskedInput::className(), [
          'mask' => '+7 999 999-99-99',
      ]) ?>
      <?= $form->field($model, 'no_send_email')->checkbox(['readonly' => true,]) ?>
    <div class="col-sm-6 col-sm-offset-3 update-buttons-block">
      <a href="/profiles/profile/allow" class="btn btn_blue">Изменить данные</a>
      <a href="/profiles/profile/change-pass" class="btn btn-primary btn_all">Изменить пароль</a>
    </div>
  </div>

    <?php \yii\bootstrap\ActiveForm::end() ?>

</div>
<div class="col-md-4">
  <h4>Основная информация по операциям</h4>
  <div class="info-buttons-block">
    <a class="btn btn_all" href="/payments/payments/index">История&nbsp;переводов</a>
    <a class="btn btn_all" href="/catalog/orders/index">Совершенные заказы</a>
    <a class="btn btn_all" href="/profiles/profile/message">Мои сообщения</a>
    <?php if($profile->role == Profile::ROLE_DISTRIBUTOR):?>
        <a class="btn btn_all" href="/dashboard/discount">История списаний на скидку</a>
        <a class="btn btn_all" href="/dashboard/premium">История списаний на премию</a>
    <?php endif;?>
  </div>
</div>
