<?php

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\frontend\models\ProfileEdit $model
 */

$this->title = 'Редактирование профиля';
$this->params['header'] = $this->title;
$this->params['breadcrumb'] = $this->title;
?>

<div class="col-md-12">
  <h2>Мои сообщения в раздел Помощь</h2>

  <div class="panel_ferma message-block">
    <div class="message-block--head">
      <div class="message-block--th1">Дата</div>
      <div class="message-block--th2">Сообщение</div>
      <div class="message-block--th3">История и комментарии</div>
    </div>

    <?php foreach ($myMessage as $message): ?>
      <div class="message-block--body">
        <div class="message-block--td1"><?= date("d.m.Y H:i:s", strtotime($message['created_at'])) ?></div>
        <div class="message-block--td2"><?= $message['content'] ?></div>
        <div class="message-block--td3">
          <a class="btn btn_all" href="/feedback/feedback/comment?message_id=<?= $message['id'] ?>&created_at=<?= $message['created_at'] ?>">
            Подробнее
          </a>
        </div>
      </div>
    <?php endforeach; ?>


  </div>


</div>
