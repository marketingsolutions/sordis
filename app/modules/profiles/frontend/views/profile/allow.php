<?php

use modules\profiles\common\models\Profile;
/**
 * @var \yii\web\View $this
 * @var \modules\profiles\frontend\models\ProfileEdit $model
 */

$this->title = 'Запрос на возможность изменения личных данных';
$this->params['header'] = $this->title;
$this->params['breadcrumb'] = $this->title;
?>

<div class="col-md-8">
    <div class="panel-body panel_ferma padding_top_bottom">
           <?php $form = \yii\bootstrap\ActiveForm::begin(['layout' => 'horizontal']) ?>
           <?php
           $model->last_name = $profile->last_name;
           $model->first_name = $profile->first_name;
           $model->middle_name = $profile->middle_name;
           $model->email = $profile->email;
           $model->phone_mobile = $profile->phone_mobile;
           ?>
           <?= $form->field($model, 'last_name') ?>
           <?= $form->field($model, 'first_name') ?>
           <?= $form->field($model, 'middle_name') ?>
           <?= $form->field($model, 'email') ?>
           <?= $form->field($model, 'phone_mobile')->widget(\yii\widgets\MaskedInput::className(), [
               'mask' => '+7 999 999-99-99',
           ])?>
           <?= $form->field($model, 'message')->textarea() ?>
           <button type="submit" class="btn btn-primary btn_pay">Отправить запрос Модератору на разрешение изменеия личных данных</button>
           <?php \yii\bootstrap\ActiveForm::end() ?>
    </div>
</div>

<div class="col-md-4">
    <h4>Основная информация по операциям</h4>
    <a class="btn btn-primary btn_all" href="/payments/payments/index">&nbsp;История&nbsp;&nbsp; переводов&nbsp;</a><br/><br/>
    <a class="btn btn-primary btn_all" href="/catalog/orders/index">Совершенные заказы</a><br/><br/>
    <a class="btn btn-primary btn_all" href="/profiles/profile/message">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Мои сообщения&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><br/><br/>
</div>
