<?php

use ms\loyalty\contracts\identities\TokenProvidesEmailInterface;
use ms\loyalty\contracts\identities\TokenProvidesPhoneMobileInterface;
use yii\helpers\Html;
use yii\helpers\Url;
use unclead\multipleinput\MultipleInput;

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\frontend\models\RegistrationForm $model
 */
$profile = $model->profile;
$this->title = 'Регистрация';
$this->params['header'] = $this->title;
$this->params['breadcrumb'] = $this->title;
$this->registerCssFile('https://cdn.jsdelivr.net/npm/suggestions-jquery@18.3.3/dist/css/suggestions.min.css');
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('https://cdn.jsdelivr.net/npm/suggestions-jquery@18.3.3/dist/js/jquery.suggestions.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$js = <<<JS

$( document ).ready(function() {
    /*Динамическая подгрузка формы в зависимости от роли*/
    $( "#registrationprofile-role" ).change(function() {
        var roleUser = $('#registrationprofile-role').val();
        var urlTo = location.href;
        var get1='&role=manager_tt';
        var get2='&role=distributor';
        var get3='&role=multi_manager_tt';
        var newUrl=urlTo.replace(get1, '').replace(get2, '').replace(get3, '');
        if(roleUser){
            window.location = newUrl+'&role='+roleUser;
        }else{
            window.location = newUrl;
        }
    });
    /*Окно - пояснялка для роли много магазинов*/
    var registerRole = $('#registrationprofile-role').val();
    if(registerRole == 'multi_manager_tt'){
        $('.multi_rtt_info').show();
    }else{
        $('.multi_rtt_info').hide();
    }
    
    /*Дадата для адреса*/
    var dadataToken = $('#dadataToken').val();
    $("#registerttform-address, #registerdistribform-distrib_address", ".add_multi_rtt_address").suggestions({
        token: dadataToken,
        type: "ADDRESS",
        count: 5,
        /* Вызывается, когда пользователь выбирает одну из подсказок */
        onSelect: function(suggestion) {
            console.log(suggestion);
            var cityMy = suggestion.data.city;
            console.log(cityMy);
            $('#registrationmanagerttform-city_region').val(cityMy);
        }
    });
    
    //Правка для кнопки добавления РТТ
    $('#plus_rtt').click(function() {
        $('.js-input-plus').click();
    });
    
    //Добавляем класс для полей с маской
    $('.multiple-input').find('input[name*=multiRtt]').addClass('form-control');
    $('body').on('click', '#plus_rtt',function(e){
        $('.multiple-input').find('input[name*=multiRtt]').addClass('form-control');
    });
    
    /*Автоматическое подтягивание адреса и названия РТТ с динамическими полями и без них*/
    $('body').on('input', '.form-control', function(e) {
        var str1='-code_egais';
        var str2='-inn';
        var inputId = $(this).attr('id').replace(str1, '').replace(str2, '');
        var egais = $('#'+inputId+str1).val();
        var inn = $('#'+inputId+str2).val();
        // console.log(egais);
        // console.log(inn);
        // console.log("!!!!!");
        if(typeof egais != "undefined" && typeof inn != "undefined" && egais.replace('_', '').length == 12 && inn.replace('_', '').length ==10){
             $.ajax(
                    {
                      type: 'POST',
                      url: '/site/get-address',
                      data: {"egais": egais, "inn": inn},
                      success: function(data) {
                            
                                var responceData = jQuery.parseJSON( data );
                                if(responceData){
                                    $('#'+inputId+'-name').val('');
                                    $('#'+inputId+'-name').val(responceData.name);
                                    $('#'+inputId+'-address').val('');    
                                    $('#'+inputId+'-address').val(responceData.address);
                                }else{
                                    $('#'+inputId+'-name').val('');
                                    $('#'+inputId+'-address').val(''); 
                                }
                            
                                              
                    }
                });
        }
     });
    
    $('body').on('input', '.form-control', function(e) {
        
        var egais = $('#registerdistribform-egais').val();
        var inn = $('#registerdistribform-inn').val();
        
        if(typeof egais != "undefined" && typeof inn != "undefined" && egais.length == 12 && inn.length ==10){
              $.ajax(
                    {
                      type: 'POST',
                      url: '/site/get-distrib-address',
                      data: {"egais": egais, "inn": inn},
                      success: function(data) {
                            
                                var responceData = jQuery.parseJSON( data );
                                if(responceData){
                                    $('#registerdistribform-distrib_name').val('');
                                    $('#registerdistribform-distrib_name').val(responceData.name);
                                    $('#registerdistribform-distrib_address').val('');    
                                    $('#registerdistribform-distrib_address').val(responceData.address);
                                }else{
                                    $('#registerdistribform-distrib_name').val('');
                                    $('#registerdistribform-distrib_address').val(''); 
                                }
                            
                                              
                    }
                });  
        }
    });
    
    
  /*Добавляем класс в динамические поля*/
  $('.multiple-input').parent().removeClass('col-sm-6 col-sm-offset-3').addClass('col-sm-12');
  
 //Выводим алерт, если пытаются ввести более 30 точек
 
 $('body').on('click', '.glyphicon-plus', function(e) {
     var ii=0;
      $('.glyphicon-remove').each(function(){
          ii++;
      });
      console.log(ii);
      if(ii>=29){
              alert("Дополнительные ТТ Вы можете добавить в Личном кабинете!");
          }
 });

    
    
});
JS;
$this->registerJs($js);

$css = <<<CSS
.js-input-plus{
    display: none;
}

@media only screen and (min-width: 769px) {

    .list-cell__work_time_to {
        width: 720px !important;
    }
    .multiple-input-list__btn{
        margin-left: -70px;
        position: absolute;
    }
}
CSS;
$this->registerCss($css);
?>

<input type="hidden" id="dadataToken" value="<?=getenv('DADATA_TOKEN')?>">

	<div class="col-md-8 col-md-offset-2">

        <?php $form = \yii\bootstrap\ActiveForm::begin(['layout' => 'horizontal']) ?>

		<p>
			Поля, отмеченные <span class="required"><label for=""></label></span>, являются обязательными к заполнению
		</p>

		<div class="panel panel-default">
			<div class="panel-heading">
				Я регистрируюсь на портале как:
			</div>
			<div class="panel-body">
                <?php if ($model->getTokenManager() instanceof TokenProvidesPhoneMobileInterface): ?>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Ваш номер телефона</label>

                        <div class="col-sm-6">
                            <p class="form-control-static">
                                <?= Html::encode($profile->phone_mobile_local) ?>
                            </p>
                        </div>
                    </div>
                <?php endif ?>
                <?php $profile->role = \Yii::$app->request->get('role')?>
                <?= $form->field($profile, 'role')
					->dropDownList(\modules\profiles\common\models\Profile::getRoles(), ['prompt' => 'выберите...']) ?>
                <div style="display: none;" class="alert alert-warning multi_rtt_info">
                    <b>Если у Вас большое количество торговых точек, то Вы можете добавить их после регистрации в своем Личном кабинете!</b><br/>
                    При выборе данной роли  все Ваши магазины будут объединены в один личный кабинет,
                    который будет управляться только Вами. Все бонусы, которые получают все Ваши магазины будут всегда суммироваться
                    и отбражаться в этом личном кабинете. Если Вы хотите, чтобы бонусы получал Ваш каждый магазин, то следует выбрать роль
                    "Я управляю одним магазином" и зарегистрировать каждый магазин отдельно.
                </div>
			</div>
		</div>
    <?php if(\Yii::$app->request->get('role') && \Yii::$app->request->get('role')==\modules\profiles\common\models\Profile::ROLE_MANAGER_TT):?>
		<div class="panel panel-default">
			<div class="panel-heading">
				Данные по розничной торговой точке
			</div>
			<div class="panel-body">
                <?= $form->field($roleModel, 'code_egais')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '999999999999',
                    'options' => [
                        'class' => 'form-control',
                    ]
                ]) ?>
                <?= $form->field($roleModel, 'inn')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '9999999999',
                ]) ?>
                <?= $form->field($roleModel, 'license_number') ?>
                <?= $form->field($roleModel, 'name') ?>
                <?= $form->field($roleModel, 'address') ?>
                <h5 align="center">Время работы РТТ</h5>
                <?= $form->field($roleModel, 'work_time_from')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '99:99',
                ]) ?>
                <?= $form->field($roleModel, 'work_time_to')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '99:99',
                ]) ?>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                Данные о  представителе розничной торговой точки
            </div>
            <div class="panel-body">
                <?= $form->field($profile, 'last_name') ?>
                <?= $form->field($profile, 'first_name') ?>
                <?= $form->field($profile, 'middle_name') ?>
                <?php if (!($model->getTokenManager() instanceof TokenProvidesPhoneMobileInterface)): ?>
                    <?= $form->field($profile, 'phone_mobile_local')->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '+7 999 999-99-99',
                    ]) ?>
                <?php endif ?>
                <?php if (!($model->getTokenManager() instanceof TokenProvidesEmailInterface)): ?>
                    <?= $form->field($profile, 'email')->input('email') ?>
                <?php endif ?>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                    Пароль для доступа в личный кабинет
                </div>
                <div class="panel-body">
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'passwordCompare')->passwordInput() ?>
			</div>
		</div>
		<div  class="panel panel-default">
			<div class="panel-heading">
				Дополнительные условия
			</div>
			<div class="panel-body">
                <?= $form->field($model, 'agreeWithTerms')->hiddenInput()
                    ->label(false) ?>
                <?= $form->field($model, 'allowPersonalDataProcessing')->checkbox()->label('Даю ' . Html::a('согласие', '@frontendWeb/media/uploads/pers.pdf', ['target' => '_blank']) . ' на обработку своих персональных данных') ?>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
				<div class="col-sm-6 col-sm-offset-3">
					<button type="submit" class="btn btn-primary"><i class="fa fa-signin"></i> Зарегистрироваться и
						подтвердить данные
					</button>
				</div>
			</div>
		</div>
    <?php elseif(\Yii::$app->request->get('role') && \Yii::$app->request->get('role')==\modules\profiles\common\models\Profile::ROLE_MULTI_MANAGER_TT):?>
        <div class="panel panel-default">
            <div class="panel-heading">
                Данные по розничной торговой точке
            </div>
            <div class="panel-body add_multi_row">
                <?= $form->field($roleModel, 'multiRtt')->widget(MultipleInput::className(), [
                    'max' => 30,
                    'rendererClass' => \unclead\multipleinput\renderers\ListRenderer::className(),
                    'columns' => [
                        [
                            'name'  => 'code_egais',
                            'type'  => \yii\widgets\MaskedInput::className(),
                            'title' => 'Код ЕГАИС',
                            'options' => [
                                'class' => 'add_multi_rtt',
                                'clientOptions' => [
                                'alias' => '999999999999',
                                ]
                            ]
                        ],
                        [
                            'name'  => 'inn',
                            'title' => 'ИНН',
                            'type'  => \yii\widgets\MaskedInput::className(),
                            'options' => [
                                'class' => 'add_multi_rtt',
                                'clientOptions' => [
                                    'alias' => '9999999999',
                                ]
                            ]
                        ],
                        [
                            'name'  => 'license_number',
                            'title' => 'Номер лицензии',
                            'options' => [
                                'class' => 'add_multi_rtt',
                            ]
                        ],
                        [
                            'name'  => 'name',
                            'title' => 'Наименование ТТ',
                            'options' => [
                                'class' => 'add_multi_rtt'
                            ]
                        ],
                        [
                            'name'  => 'address',
                            'title' => 'Адрес ТТ',
                            'options' => [
                                'class' => 'add_multi_rtt_address'
                            ]
                        ],
                        [
                            'name'  => 'work_time_from',
                            'type'  => \yii\widgets\MaskedInput::className(),
                            'title' => 'Время с',
                            'options' => [
                                'class' => 'add_multi_rtt',
                                'clientOptions' => [
                                    'alias' => '99:99',
                               ]
                            ]
                        ],
                        [
                            'name'  => 'work_time_to',
                            'type'  => \yii\widgets\MaskedInput::className(),
                            'title' => 'Время по',
                            'options' => [
                                'class' => 'add_multi_rtt',
                                'clientOptions' => [
                                    'alias' => '99:99',
                                ]
                            ]
                        ],
                    ]
                ])->label(false);
                ?>
                <div class="pull-right">
                    <div id="plus_rtt" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i></div>
                </div>
            </div>
        </div>
        <!--Личные данные-->
        <div class="panel panel-default">
            <div class="panel-heading">
                Данные о  представителе розничной торговой точки
            </div>
            <div class="panel-body">
                <?= $form->field($profile, 'last_name') ?>
                <?= $form->field($profile, 'first_name') ?>
                <?= $form->field($profile, 'middle_name') ?>
                <?php if (!($model->getTokenManager() instanceof TokenProvidesPhoneMobileInterface)): ?>
                    <?= $form->field($profile, 'phone_mobile_local')->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '+7 999 999-99-99',
                    ]) ?>
                <?php endif ?>
                <?php if (!($model->getTokenManager() instanceof TokenProvidesEmailInterface)): ?>
                    <?= $form->field($profile, 'email')->input('email') ?>
                <?php endif ?>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                Пароль для доступа в личный кабинет
            </div>
            <div class="panel-body">
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'passwordCompare')->passwordInput() ?>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                Дополнительные условия
            </div>
            <div class="panel-body">
                <?= $form->field($model, 'agreeWithTerms')->hiddenInput()
                    ->label(false) ?>
                <?= $form->field($model, 'allowPersonalDataProcessing')->checkbox()->label('Даю ' . Html::a('согласие', '@frontendWeb/media/uploads/pers.pdf', ['target' => '_blank']) . ' на обработку своих персональных данных') ?>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-sm-6 col-sm-offset-3">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-signin"></i> Зарегистрироваться и
                        подтвердить данные
                    </button>
                </div>
            </div>
        </div>
    <?php elseif (\Yii::$app->request->get('role') && \Yii::$app->request->get('role')==\modules\profiles\common\models\Profile::ROLE_DISTRIBUTOR):?>
        <div class="panel panel-default">
            <div class="panel-heading">
                Данные для доступа в личный кабинет
            </div>
            <div class="panel-body">
                <?= $form->field($roleModel, 'egais') ?>
                <?= $form->field($roleModel, 'inn') ?>
                <?= $form->field($roleModel, 'distrib_name') ?>
                <?= $form->field($roleModel, 'distrib_address') ?>
                <?= $form->field($roleModel, 'license_number') ?>
                <?= $form->field($profile, 'last_name') ?>
                <?= $form->field($profile, 'first_name') ?>
                <?= $form->field($profile, 'middle_name') ?>
                <?php if (!($model->getTokenManager() instanceof TokenProvidesPhoneMobileInterface)): ?>
                    <?= $form->field($profile, 'phone_mobile_local')->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '+7 999 999-99-99',
                    ]) ?>
                <?php endif ?>
                <?php if (!($model->getTokenManager() instanceof TokenProvidesEmailInterface)): ?>
                    <?= $form->field($profile, 'email')->input('email') ?>
                <?php endif ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'passwordCompare')->passwordInput() ?>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                Дополнительные условия
            </div>
            <div class="panel-body">
                <?= $form->field($model, 'agreeWithTerms')->hiddenInput()
                    ->label(false) ?>
                <?= $form->field($model, 'allowPersonalDataProcessing')->checkbox()->label('Даю ' . Html::a('согласие', '@frontendWeb/media/uploads/pers.pdf', ['target' => '_blank']) . ' на обработку своих персональных данных') ?>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-sm-6 col-sm-offset-3">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-signin"></i> Зарегистрироваться и
                        подтвердить данные
                    </button>
                </div>
            </div>
        </div>

    <?php endif;?>
        <?php \yii\bootstrap\ActiveForm::end() ?>
	</div>