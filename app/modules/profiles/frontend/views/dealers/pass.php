<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 26.04.2017
 * Time: 17:40
 */
$this->title = 'Смена пароля';
$this->params['header'] = $this->title;
$this->params['breadcrumb'] = $this->title;
?>

<div class="col-md-8 col-md-offset-2">
    <?php $form = \yii\bootstrap\ActiveForm::begin(['layout' => 'horizontal']) ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            Введите новый пароль
        </div>
        <div class="panel-body">
            <?= $form->field($model, 'password')->passwordInput(); ?>
            <?= $form->field($model, 'passwordCompare')->passwordInput(); ?>


            <div class="col-sm-6 col-sm-offset-3">
                <button type="submit" class="btn btn_all"> Изменить пароль
                </button>
            </div>
        </div>
    </div>
    <?php \yii\bootstrap\ActiveForm::end() ?>
</div>
