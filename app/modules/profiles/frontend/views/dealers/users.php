<?php

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\common\models\Profile $profile
 * @var \modules\profiles\common\models\Profile[] $profiles
 */

$this->title = 'Список участников дистрибьютера';
$this->params['header'] = $this->title;
$this->params['breadcrumb'] = $this->title;
?>

<div class="col-md-8 col-md-offset-2" style="margin-bottom:60px;">
	<h2><?= $profile->dealer->name ?></h2>

	<table class="table">
		<thead>
			<tr>
				<th>Участник</th>
				<th>Телефон</th>
				<th>E-mail</th>
				<td>Должность</td>
			</tr>
		</thead>
		<tbody>
		<tr style="background: beige">
			<td><?= $profile->full_name ?></td>
			<td><?= $profile->phone_mobile ?></td>
			<td><?= $profile->email ?></td>
			<td><?= $profile->role_label ?> </td>
		</tr>
        <?php foreach ($profiles as $p): ?>
			<tr>
				<td><?= $p->full_name ?></td>
				<td><?= $p->phone_mobile ?></td>
				<td><?= $p->email ?></td>
				<td><?= $p->role_label ?> </td>
			</tr>
        <?php endforeach; ?>
		</tbody>
	</table>
</div>
