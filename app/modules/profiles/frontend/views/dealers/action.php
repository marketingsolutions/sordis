<?php

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\common\models\Profile $profile
 * @var \modules\profiles\common\models\Action $action
 */

$this->title = 'Акция дистрибьютора';
$this->params['header'] = $this->title;
$this->params['breadcrumb'] = $this->title;
?>

<div class="col-md-8 col-md-offset-2" style="margin-bottom:60px;">
	<h2><?= $profile->dealer->name ?>: <?= $action->name ?></h2>

	<p><b>Сроки проведения: с <?= $action->date_start_formatted ?> по <?= $action->date_end_formatted ?></b></p>
	<div class="content" style="margin-top: 20px;">
		<?= $action->description ?>
	</div>
</div>
