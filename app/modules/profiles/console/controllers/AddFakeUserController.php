<?php
namespace modules\profiles\console\controllers;

use console\base\Controller;
use modules\profiles\common\models\Profile;
use modules\sales\common\models\Distributor;
use modules\sales\common\models\DistributorProfile;
use modules\sales\common\models\SalePoint;
use modules\sales\common\models\SalePointProfile;
use ms\loyalty\identity\phonesEmails\common\models\Identity;

/**
 * Created by PhpStorm.
 * User: MihailKri
 * Date: 01.11.2018
 * Time: 9:27
 */

class AddFakeUserController extends Controller
{

    public function actionAddRttUser()
    {
        $fakePhoneNumeralStart = 10000;
        $fakePhoneNumeralFinish = 20000;
        $fakePhones = '+792121';
        $passhash = '$2y$13$dB1N1pMk3ALtRhCMB0IjHOqi77k8Vm8Bq.iphH4VJs.2D5y2aoRyi';
        $auth_key = 'dDPzmmhx8TbxlI6nb7CsfEtmaj8mxks_';
        $type = 'phone';
        $roleP = 'profile';
        $firstName = 'Имя';
        $lastName = 'Фамилия';
        $middleName = 'Отчество';
        $email = '@mail.ru';
        $role = Profile::ROLE_MANAGER_TT;

        //Добавляем нового участника РТТ
        $transaction = \Yii::$app->db->beginTransaction();

        $now = date("Y-m-d H:i:s");

        for( $i=$fakePhoneNumeralStart; $i<=$fakePhoneNumeralFinish; $i++ ){
            //Заносим данные в Identtity
            $newIdentity = new Identity();
            $newIdentity->role = $roleP;
            $newIdentity->login = $fakePhones.$i;
            $newIdentity->passhash = $passhash;
            $newIdentity->auth_key = $auth_key;
            $newIdentity->created_at = $now;
            $newIdentity->updated_at = $now;
            $newIdentity->type = $type;
            $newIdentity->save();

            //Заносим данные в Profiles
            $newProfile = new Profile();
            $newProfile->first_name = $firstName . $i;
            $newProfile->last_name = $lastName . $i;
            $newProfile->middle_name = $middleName . $i;
            $newProfile->phone_mobile = $fakePhones . $i;
            $newProfile->email = 'user' . $i . $email;
            $newProfile->role = $role;
            $newProfile->created_at = $now;
            $newProfile->updated_at = $now;
            $newProfile->identity_id = $newIdentity->id;
            $newProfile->save();

            //Цепляем РТТ участнику
            $addRtt = new SalePointProfile();
            $addRtt->profile_id = $newProfile->id;
            $addRtt->sale_point_id = $this->getSalePoint();
            $addRtt->save();
            echo "Юзер №".$i." успешно  создан \n";
        }
        $transaction->commit();
    }

    public function actionAddDistribUser()
    {
        $fakePhoneNumeralStart = 20002;
        $fakePhoneNumeralFinish = 20104;
        $fakePhones = '+792121';
        $passhash = '$2y$13$dB1N1pMk3ALtRhCMB0IjHOqi77k8Vm8Bq.iphH4VJs.2D5y2aoRyi';
        $auth_key = 'dDPzmmhx8TbxlI6nb7CsfEtmaj8mxks_';
        $type = 'phone';
        $roleP = 'profile';
        $firstName = 'Имя';
        $lastName = 'Фамилия';
        $middleName = 'Отчество';
        $email = '@mail.ru';
        $role = Profile::ROLE_DISTRIBUTOR;

        //Добавляем нового участника Дистрибьютора
        $transaction = \Yii::$app->db->beginTransaction();
        $now = date("Y-m-d H:i:s");

        for( $i=$fakePhoneNumeralStart; $i<=$fakePhoneNumeralFinish; $i++ ) {
            //Данные в Identtity
            $newIdentity = new Identity();
            $newIdentity->role = $roleP;
            $newIdentity->login = $fakePhones . $i;
            $newIdentity->passhash = $passhash;
            $newIdentity->auth_key = $auth_key;
            $newIdentity->created_at = $now;
            $newIdentity->updated_at = $now;
            $newIdentity->type = $type;
            $newIdentity->save();

            //данные в Profiles
            $newProfile = new Profile();
            $newProfile->first_name = $firstName . $i;
            $newProfile->last_name = $lastName . $i;
            $newProfile->middle_name = $middleName . $i;
            $newProfile->phone_mobile = $fakePhones . $i;
            $newProfile->email = 'user' . $i . $email;
            $newProfile->role = $role;
            $newProfile->created_at = $now;
            $newProfile->updated_at = $now;
            $newProfile->identity_id = $newIdentity->id;
            $newProfile->save();

            //Добавляем Дистрибьютора
            $newDistr = new DistributorProfile();
            $newDistr->profile_id = $newProfile->id;
            $newDistr->distrib_id = $this->getDistrib();
            $newDistr->save();
        }
        echo "Юзер №".$i." успешно  создан \n";
        $transaction->commit();

    }

    /**
     * @return mixed
     */
    public function getSalePoint(){
        $arrFreeSalePoint = [];
        $freeSalePoint = SalePointProfile::find()->asArray()->all();
        foreach ($freeSalePoint as $point){
            $arrFreeSalePoint[] = $point['sale_point_id'];
        }
        return SalePoint::find()->where(['not in', 'id', $arrFreeSalePoint])->asArray()->one()['id'];
    }

    /**
     * @return mixed
     */
    public function getDistrib(){
        $arrFreeDistrib = [];
        $freeDistrib = DistributorProfile::find()->asArray()->all();
        foreach ($freeDistrib as $distr){
            $arrFreeDistrib[] = $distr['distrib_id'];
        }
        return Distributor::find()->where(['not in', 'id', $arrFreeDistrib])->asArray()->one()['id'];
    }

}