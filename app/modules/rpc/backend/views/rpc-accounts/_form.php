<?php

use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var modules\rpc\common\models\RpcAccount $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php  $box = FormBox::begin(['cssClass' => 'rpc-account-form box-primary', 'title' => '']) ?>
    <?php $form = ActiveForm::begin(); ?>

    <?php $box->beginBody() ?>

    <?= $form->field($model, 'user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'profile_id')->textInput()->staticControl() ?>

    <?= $form->field($model, 'try_code')->textInput(['maxlength' => true])->staticControl() ?>

    <?= $form->field($model, 'try_at')->textInput(['maxlength' => true])->staticControl() ?>

    <?= $form->field($model, 'try_result')->textarea(['rows' => 6])->staticControl() ?>

    <?= $form->field($model, 'try_shutdown')->textInput()->staticControl() ?>

    <?= $form->field($model, 'created_at')->textInput()->staticControl() ?>

    <?= $form->field($model, 'updated_at')->textInput()->staticControl() ?>

    <?php $box->endBody() ?>

    <?php $box->actions([
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
    ]) ?>
    <?php ActiveForm::end(); ?>

<?php  FormBox::end() ?>
