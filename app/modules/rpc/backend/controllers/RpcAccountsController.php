<?php

namespace modules\rpc\backend\controllers;

use modules\profiles\common\models\Profile;
use Yii;
use modules\rpc\common\models\RpcAccount;
use modules\rpc\backend\models\RpcAccountSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * RpcAccountsController implements the CRUD actions for RpcAccount model.
 */
class RpcAccountsController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var RpcAccountSearch $searchModel */
                    return Yii::createObject(RpcAccountSearch::className());
                },
                'dataProvider' => function($params, RpcAccountSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all RpcAccount models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var RpcAccountSearch $searchModel */
        $searchModel = Yii::createObject(RpcAccountSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(RpcAccountSearch $searchModel)
    {
        return [
			'id',
            'user',
            'code',
            [
                'label' => 'Кто активировал',
                'format' => 'raw',
                'value' => function (RpcAccount $model) {
                    /** @var Profile $profile */
                    if ($profile = Profile::findOne($model->profile_id)){
                        $html = [];
                        $html[] = $profile->full_name;
                        $html[] = '#' . $profile->id;
                        $html[] = $profile->phone_mobile;
                        return implode('<br>', $html);
                    }
                    return '';
                }
            ],
			'try_code',
            [
                'attribute' => 'try_at',
                'value' => function (RpcAccount $model) {
                    return empty($model->try_at) ? null : (new \DateTime($model->try_at))->format('d.m.Y H:i');
                }
            ],
			'try_result:ntext',
			'try_shutdown:boolean',
        ];
    }

    /**
     * Creates a new RpcAccount model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RpcAccount;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RpcAccount model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
        ]);
	}


    /**
     * Deletes an existing RpcAccount model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the RpcAccount model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RpcAccount the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RpcAccount::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
