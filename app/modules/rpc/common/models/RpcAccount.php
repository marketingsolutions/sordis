<?php

namespace modules\rpc\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_rpc_accounts".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property string $user
 * @property string $code
 * @property string $try_code
 * @property string $try_at
 * @property string $try_result
 * @property integer $try_shutdown
 * @property string $created_at
 * @property string $updated_at
 */
class RpcAccount extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    public static function getDb()
    {
        return Yii::$app->get('db2'); // second database
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rpc_accounts}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Аккаунт удаленки';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Аккаунты удаленки';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['profile_id', 'integer'],
            ['user', 'string', 'max' => 255],
            ['code', 'string', 'max' => 255],
            ['try_code', 'string', 'max' => 255],
            ['try_at', 'string', 'max' => 255],
            ['try_result', 'string'],
            ['try_shutdown', 'integer'],
            ['created_at', 'safe'],
            ['updated_at', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Участник',
            'user' => 'Сотрудник',
            'code' => 'Код',
            'try_code' => 'Последний введенный, код',
            'try_at' => 'Последний введенный, время',
            'try_result' => 'Последний введенный, результат',
            'try_shutdown' => 'Последний введенный, выключена',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }
}
