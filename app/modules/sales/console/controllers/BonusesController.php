<?php

namespace modules\sales\console\controllers;

use common\utils\Notifier;
use console\base\Controller;
use modules\sales\common\finances\SalePartner;
use modules\sales\common\models\Sale;
use modules\sales\common\sales\statuses\Statuses;
use ms\loyalty\contracts\prizes\PrizeRecipientInterface;
use ms\loyalty\contracts\profiles\ProfileFinderInterface;
use marketingsolutions\finance\models\Transaction;
use yii\db\Expression;


/**
 * Class BonusesController
 */
class BonusesController extends Controller
{
    public function actionPay()
    {
        $query = Sale::find()
            ->where(['status' => Statuses::PAID])
            ->andWhere('bonuses_paid_at IS NULL');

        foreach ($query->each() as $sale) {
            /** @var Sale $sale */
            $transaction = \Yii::$app->db->beginTransaction();

            /** @var ProfileFinderInterface $profileFinder */
            $profileFinder = \Yii::$container->get(ProfileFinderInterface::class);

            $profile = $profileFinder->findByProfileId($sale->recipient_id);

            if (!($profile instanceof PrizeRecipientInterface)) {
                continue;
            }

            $profile->getRecipientPurse()->addTransaction(Transaction::factory(
                Transaction::INCOMING,
                $sale->bonuses,
                new SalePartner($sale->id),
                'Бонусы за продажу #' . $sale->id
            ));

            $sale->updateAttributes([
                'bonuses_paid_at' => new Expression('NOW()'),
            ]);

            $transaction->commit();

            Notifier::salePaid($sale);
        }
    }
}