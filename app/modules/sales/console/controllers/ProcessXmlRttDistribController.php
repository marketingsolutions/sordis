<?php

namespace modules\sales\console\controllers;

use console\base\Controller;
use modules\sales\common\models\Distributor;
use modules\sales\common\models\DistributorProfile;
use modules\sales\common\models\DownloadRtt;
use modules\sales\common\models\SalePoint;
use modules\sales\common\models\SalePointDistrib;
use modules\sales\common\models\SalePointProfile;
use modules\sales\common\models\UploadSalePointDistrib;
use marketingsolutions\finance\models\Transaction;
use yii\db\Expression;
use yz\admin\models\User;
use yii\helpers\FileHelper;


/**
 * Class BonusesController
 */
class ProcessXmlRttDistribController extends Controller
{

    public function actionIndex()
    {
        $processFile = UploadSalePointDistrib::findOne(['status_process'=>UploadSalePointDistrib::STATUS_NEW, 'processed_at' => null]);
        if(!$processFile){
            echo "Нет данных для обработки!\n";
            return false;
        }
        if (getenv('YII_ENV') == 'dev') {
            $xmlFilePath = \Yii::getAlias('@backend/web/upload_rtt_distrib/' . $processFile->file_name);
        }else{
            $xmlFilePath = \Yii::getAlias('@data/upload_rtt_distrib/' . $processFile->file_name);
        }
        $textXmlFile = simplexml_load_file($xmlFilePath);
        $now = date("Y-m-d H:i:s");
        $processFile->status_process = UploadSalePointDistrib::STATUS_PROCESS;
        $processFile->update(false);

        //Выбираем всех дистрибьюторов с привязкой к профилям
        $allDistrib = Distributor::find()
            ->select('{{%distributor}}.id as idd, {{%distributor}}.manager_id, {{%distributor}}.distrib_name, {{%distributor}}.distrib_address, {{%distributor}}.egais, {{%distributor}}.inn, {{%distributor}}.license_number, {{%distributor}}.kpp, {{%distributor_profile}}.profile_id, {{%distributor_profile}}.distrib_id')
            ->leftJoin('{{%distributor_profile}}', '{{%distributor_profile}}.distrib_id={{%distributor}}.id')
            ->indexBy('egais')
            ->asArray()
            ->all();


        //Выбираем все торговые точки
        $allSalePoint = SalePoint::find()
            ->select('{{%sale_point}}.id as ids, {{%sale_point}}.name, {{%sale_point}}.address, {{%sale_point}}.license_number, {{%sale_point}}.code_egais, {{%sale_point}}.inn, {{%sale_point}}.kpp, {{%sale_point_profile}}.profile_id, {{%sale_point_profile}}.sale_point_id')
            ->leftJoin('{{%sale_point_profile}}', '{{%sale_point_profile}}.sale_point_id={{%sale_point}}.id')
            ->indexBy('code_egais')
            ->asArray()
            ->all();

        //Выбираем всех менеджеров ТО
        $managersTO = User::find()
            ->select('{{%admin_users}}.name, {{%admin_auth_assignment}}.user_id')
            ->leftJoin('{{%admin_auth_assignment}}' , '{{%admin_auth_assignment}}.user_id={{%admin_users}}.id')
            ->where(['{{%admin_auth_assignment}}.item_name' => 'MANAGER_TO'])
            ->indexBy('name')
            ->asArray()
            ->all();

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            //Разбираем Дистрибьюторов с привязкой к РТТ и менеджерам ТО
            foreach ($textXmlFile->Distrs->Distr as $distr) {

                //Получаем id дистрибьютора для вставки в таблицу с РТТ
                $nameManagerTo = (string)$distr->DistrMan;

                //Если Дистрибьютора нету в списках Дистрибьюторов то добавляем его
                if (!$this->isDistrib($distr->DistrFSRARID, $allDistrib)) {
                    echo "Новый Дистр \n";
                    $newDistrib = new Distributor();
                    $newDistrib->distrib_name = $distr->DistrName;
                    $newDistrib->distrib_address = $distr->DistrAddress;
                    $newDistrib->egais = $distr->DistrFSRARID;
                    $newDistrib->inn = $distr->DistrINN;
                    $newDistrib->kpp = $distr->DistrKPP;
                    $newDistrib->manager_id = $managersTO[$nameManagerTo]['user_id'];
                    $newDistrib->created_at = $now;
                    $newDistrib->save(false);
                    $distribId = $newDistrib->id;

                    echo "Добавлен новый дистрибьютор " . $distr->DistrName . ", ЕГАИС-" . $distr->DistrFSRARI . "\n";

                } elseif ($this->isDistrib($distr->DistrFSRARID, $allDistrib) && !$this->isDistribInProfile($distr->DistrFSRARID, $allDistrib)) {
                    //Если дистрибьютор есть и он не привязан к профилю участника, то обновляем по нему данные
                    if ($allDistrib[$distr->DistrFSRARID]['distrib_name'] != $distr->DistrName || $allDistrib[$distr->DistrFSRARID]['distrib_address'] != $distr->distrib_address || $allDistrib[$distr->DistrFSRARID]['inn'] != $distr->DistrINN || $allDistrib[$distr->DistrFSRARID]['kpp'] != $distr->DistrKPP) {
                        //Если данные поменялись , то обновляем
                        echo "Дистр не привязан, данные изменились \n";
                        $updateDistrib = Distributor::findOne(['egais' => $distr->DistrFSRARID]);
                        $updateDistrib->distrib_name = $distr->DistrName;
                        $updateDistrib->distrib_address = $distr->DistrAddress;
                        $updateDistrib->inn = $distr->DistrINN;
                        $updateDistrib->kpp = $distr->DistrKPP;
                        $updateDistrib->updated_at = $now;
                        $updateDistrib->manager_id = $managersTO[$nameManagerTo]['user_id'];
                        $updateDistrib->update(false);
                        $distribId = $updateDistrib->id;

                        echo "Обновлены менеджер ТО и данные по дистрибьютору " . $distr->DistrName . ", ЕГАИС-" . $distr->DistrFSRARI . " \n";
                    } else {
                        //Если не изменились, то пропускаем таких
                        $this->updateManagerTo($distr->DistrFSRARID,  $managersTO[$nameManagerTo]['user_id']);
                        echo "Дистрибьютор не привязан, обновлен Менеджер ТО,  данные по дистрибьютору " . $distr->DistrName . ", ЕГАИС-" . $distr->DistrFSRARI . " НЕ ИЗМЕНИЛИСЬ!!! \n";
                    }
                } else {
                    $distribId = $allDistrib[(string)$distr->DistrFSRARID]['distrib_id'];
                    $this->updateManagerTo($distr->DistrFSRARID,  $managersTO[$nameManagerTo]['user_id']);
                    echo "Дистр ЕГАИС-".$distr->DistrFSRARID."- привязан, обновлен  Менеджер ТО \n";
                }

                //Разбираем торговые точки у Дистрибьютора
                foreach ($distr->RTTs->RTT as $rtt) {
                    //Если нету такой РТТ , то добавляем её
                    if (!$this->isRtt($rtt->FSRARID, $allSalePoint)) {
                        $newRtt = new SalePoint();
                        $newRtt->name = $rtt->Name;
                        $newRtt->address = $rtt->Address;
                        $newRtt->code_egais = $rtt->FSRARID;
                        $newRtt->created_at = $now;
                        $newRtt->inn = $rtt->INN;
                        if ($rtt->KPP) $newRtt->kpp = $rtt->KPP;
                        $newRtt->save(false);
                        $rttId = $newRtt->id;

                        echo "Добавлена новая РТТ " . $rtt->Name . ", ЕГАИС-" . $rtt->FSRARID . "\n";

                    } elseif ($this->isRtt($rtt->FSRARID, $allSalePoint) && !$this->isRttInProfile($rtt->FSRARID, $allSalePoint)) {
                        //Если РТТ есть и нет привязки к профилю участника, то обновляем данные по этой РТТ
                        if ($allSalePoint[(string)$rtt->FSRARID]['name'] != $rtt->Name || $allSalePoint[(string)$rtt->FSRARID]['address'] != $rtt->Address || $allSalePoint[(string)$rtt->FSRARID]['inn'] != $rtt->INN) {
                            $updateRtt = SalePoint::findOne(['code_egais' => $rtt->FSRARID]);
                            $updateRtt->name = $rtt->Name;
                            $updateRtt->address = $rtt->Address;
                            $updateRtt->inn = $rtt->INN;
                            if (!empty($rtt->KPP)) $updateRtt->kpp = $rtt->KPP; //КПП есть в файле не всегда, т к у ИП-ов нету КПП
                            $updateRtt->updated_at = $now;
                            $updateRtt->update(false);
                            $rttId = $updateRtt->id;

                            echo "Обновлены данные по РТТ " . $rtt->Name . ", ЕГАИС-" . $rtt->FSRARID . " \n";
                        } else {
                            echo "Данные по РТТ " . $rtt->Name . ", ЕГАИС-" . $rtt->FSRARID . " НЕ ИЗМЕНИЛИСЬ!!!\n";
                        }
                    } else {
                        $rttId = SalePoint::findOne(['code_egais' => $rtt->FSRARID])->id;
                    }

                    //Добавляем связь между РТТ и дистрибьютором
                    $this->addRelationRttDistrib($rttId, $distribId);
                }
            }

            $transaction->commit();
            $statusComplete = UploadSalePointDistrib::findOne(['id' => $processFile->id]);
            $statusComplete->status_process = UploadSalePointDistrib::STATUS_COMPLETE;
            $statusComplete->processed_at = date("Y-m-d H:i:s");
            $statusComplete->update(false);
        }catch (\Exception $e){
            $transaction->rollBack();
            $statusError = UploadSalePointDistrib::findOne($processFile->id);
            $statusError->status_process = UploadSalePointDistrib::STATUS_ERROR;
            $statusError->update(false);
            \Yii::error('НЕ удалось добавить ртт, менеджеров ТО и дистрибьюторов #');
            \Yii::error("Error: " . $e->getMessage());
            throw $e;
        }
    }


    public function actionDownloadTt()
    {

        $processFile = DownloadRtt::findOne(['status_process'=>UploadSalePointDistrib::STATUS_NEW, 'processed_at' => null]);
        if(!$processFile){
            echo "Нет данных для обработки!\n";
            return false;
        }
        $dir = \Yii::getAlias(getenv('YII_ENV') == 'dev' ? '@backend/web/download_rtt_distrib/' : '@data/download_rtt_distrib/');
        FileHelper::createDirectory($dir);
        if (getenv('YII_ENV') == 'dev') {
            $xmlFilePath = \Yii::getAlias('@backend/web/download_rtt_distrib/' . $processFile->file_name);
        }else{
            $xmlFilePath = \Yii::getAlias('@data/download_rtt_distrib/' . $processFile->file_name);
        }
        $processFile->status_process = UploadSalePointDistrib::STATUS_PROCESS;
        $processFile->update(false);
        $fileName = $processFile->id.'rtt.xml';

        $isDownload = DownloadRtt::uploadRttToXml($xmlFilePath, $fileName);

//        print_r($salesPoints);
//        print_r($fileName);
//        exit;

        if($isDownload){
          $endProcessFile = DownloadRtt::findOne($processFile->id);
          $endProcessFile->status_process = DownloadRtt::STATUS_COMPLETE;
          $endProcessFile->file_name = $fileName;
          $endProcessFile->original_file_name = $xmlFilePath;
          $endProcessFile->processed_at = date("Y-m-d H:i:s");
          $endProcessFile->update(false);
          echo "Выгрузка закончена!";
        }else{
            $endProcessFile = DownloadRtt::findOne($processFile->id);
            $endProcessFile->status_process = DownloadRtt::STATUS_ERROR;
            $endProcessFile->processed_at = date("Y-m-d H:i:s");
            $endProcessFile->update(false);
            echo "Ошибка выгрузки!";
        }
    }

    /**
     * Проверка привязан ди Дистрибьютор к профилям (если привязан - true, если нет - false)
     * @param $code_egais
     * @return bool
     */
    public function isDistribInProfile($code_egais, $allDistrib){
        $distrib = $this->isDistrib($code_egais, $allDistrib);
        if(!$distrib){
            return false;
        }
        $isInProfile = $allDistrib[(string)$code_egais];
        if(!$isInProfile){
            return false;
        }
        return true;
    }

    /**
     * Есть ли Дистрибьютор в списке Дистрибьюторов (Если есть - true, нет - false)
     * @param $code_egais
     * @return bool
     */
    public function isDistrib($code_egais, $allDistrib){
        $code_egais = (string)$code_egais;
        return array_key_exists($code_egais, $allDistrib);

    }

    /**
     * Проверка есть ли привязка у РТТ с участником (если есть привязка, то - true, если нет, то false)
     * @param $code_egais
     * @return bool
     */
    public function isRttInProfile($code_egais, $allSalePoint){
        $code_egais = (string)$code_egais;
        $rtt = array_key_exists($code_egais, $allSalePoint);
        if(!$rtt){
            return false;
        }
        $isInProfile = $allSalePoint[$code_egais]['profile_id'];
        if(!$isInProfile){
            return false;
        }
        return true;
    }

    /**
     * Есть ли такая РТТ в таблице yz_sale_point  (если есть, то true, если нет , то false)
     * @param $code_egais
     * @return bool
     */
    public function isRtt($code_egais, $allSalePoint){
        $code_egais = (string)$code_egais;
        return array_key_exists($code_egais, $allSalePoint);
    }

    /**
     * Добавляем связь между РТТ и Дистрибьютором
     * @param $rttId
     * @param $distribId
     */
    public function addRelationRttDistrib($rttId, $distribId){
        $isRelation = SalePointDistrib::findOne(['sale_point_id' =>$rttId, 'distributor_id' => $distribId]);
        if(!$isRelation) {
            $newRelationRttDistrib = new SalePointDistrib();
            $newRelationRttDistrib->sale_point_id = $rttId;
            $newRelationRttDistrib->distributor_id = $distribId;
            $newRelationRttDistrib->save(false);
        }
    }

    /**
     * Обновление менеджера ТО
     * @param $code_egais
     * @param $id_manager_to
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function updateManagerTo($code_egais, $id_manager_to){
        $updateDistrib = Distributor::findOne(['egais' => $code_egais]);
        $updateDistrib->manager_id = $id_manager_to;
        $updateDistrib->update(false);
    }


    /**
     * Список Менеджеров ТО
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionNewDistrib(){
        $processFile = UploadSalePointDistrib::findOne(['status_process'=>UploadSalePointDistrib::STATUS_NEW, 'processed_at' => null]);
        if(!$processFile){
            echo "Нет данных для обработки!\n";
            return false;
        }
        if (getenv('YII_ENV') == 'dev') {
            $xmlFilePath = \Yii::getAlias('@backend/web/upload_rtt_distrib/' . $processFile->file_name);
        }else{
            $xmlFilePath = \Yii::getAlias('@data/upload_rtt_distrib/' . $processFile->file_name);
        }
        $textXmlFile = simplexml_load_file($xmlFilePath);
        foreach ($textXmlFile->Distrs->Distr as $distr) {
            $nameManagerTo = (string)$distr->DistrMan;
            echo $nameManagerTo."\n";
        }
    }
}


