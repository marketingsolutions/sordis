<?php

namespace modules\sales\console\controllers;

use common\utils\Notifier;
use console\base\Controller;
use modules\profiles\common\models\Profile;
use modules\sales\common\finances\SalePartner;
use modules\sales\common\models\Sale;
use modules\sales\common\models\SalePoint;
use modules\sales\common\models\SalePointProfile;
use modules\sales\common\models\SalePointUpload;
use modules\sales\common\sales\statuses\Statuses;
use ms\loyalty\contracts\prizes\PrizeRecipientInterface;
use ms\loyalty\contracts\profiles\ProfileFinderInterface;
use marketingsolutions\finance\models\Transaction;
use yii\db\Expression;



/**
 * Class BonusesController
 */
class UpdateRttDataController extends Controller
{
    public function actionIndex()
    {
        $processFile = SalePointUpload::findOne(['status_process'=>SalePointUpload::STATUS_NEW, 'processed_at' => null]);
        if(!$processFile){
            echo "Нет данных для обработки!\n";
            return false;
        }
        if (getenv('YII_ENV') == 'dev') {
            $xmlFilePath = \Yii::getAlias('@backend/web/upload_rtt/' . $processFile->file_name);
        }else{
            $xmlFilePath = \Yii::getAlias('@data/upload_rtt/' . $processFile->file_name);
        }
//        $file_type = \PHPExcel_IOFactory::identify($xmlFilePath);
//        $objReader = \PHPExcel_IOFactory::createReader($file_type);
//        $chunkSize = 500;
//        $chunkFilter = new chunkReadFilter();
//        while ($startRow <= 65000) {
//            $chunkFilter->setRows($startRow,$chunkSize);
//            $objReader->setReadFilter($chunkFilter);
//            $objReader->setReadDataOnly(true);
//            $objPHPExcel = $objReader->load($fileName);
//            //Что-то с этими строками делаем
//            $startRow += $chunkSize;
//            $_SESSION['startRow'] = $startRow;
//
//            unset($objReader);
//
//            unset($objPHPExcel);
//
//        }
//
//        echo "------Начало чтения файла--------";


    }

    public function actionSetRegistredRttTime()
    {
        $salesPoints = SalePoint::find()->select('id')->asArray()->all();
        //Просматриваем все РТТ
        foreach ($salesPoints as $point){
            //Смотрим привязку к профилю
            $isRegistred = SalePointProfile::findOne(['sale_point_id' => $point['id']]);
            //Если есть привязка, то исправляем дату регистрации в Копилке
            if($isRegistred){
                $profile = Profile::findOne(['id' => $isRegistred->profile_id]);
                $salePointUpdate = SalePoint::findOne(['id' => $point['id']]);
                $salePointUpdate->registred_at = $profile->created_at;
                $salePointUpdate->save(false);
                echo "Дарта регистрации исправлена у участника - ".$profile->full_name."/t";
            }
        }
    }
}


