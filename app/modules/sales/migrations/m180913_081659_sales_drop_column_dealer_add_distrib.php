<?php

use yii\db\Migration;

/**
 * Class m180913_081659_sales_drop_column_dealer_add_distrib
 */
class m180913_081659_sales_drop_column_dealer_add_distrib extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%distributor}}', [
            'id' => $this->primaryKey(),
            'distrib_name' => $this->string(),
            'distrib_address' => $this->string(),
            'egais' => $this->string(25),
            'inn' => $this->string(25),
            'license_number' => $this->string(25),
        ], $tableOptions);

        $this->createTable('{{%distributor_profile}}', [
            'id' => $this->primaryKey(),
            'profile_id' => $this->integer(),
            'distrib_id' => $this->integer(),
        ], $tableOptions);
        $this->addForeignKey('fk_profile_id_profile', '{{%distributor_profile}}', 'profile_id', '{{%profiles}}', 'id' , 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk_distrib_id_distributor', '{{%distributor_profile}}', 'distrib_id', '{{%distributor}}', 'id' , 'RESTRICT', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_profile_id_profile', '{{%distributor_profile}}');
        $this->dropForeignKey('fk_distrib_id_distributor', '{{%distributor_profile}}');
        $this->dropTable('{{%distributor}}');
        $this->dropTable('{{%distributor_profile}}');
    }
}
