<?php

use yii\db\Migration;

/**
 * Class m181115_110543_sales_add_column_manager_to_distrib_table
 */
class m181115_110543_sales_add_column_manager_to_distrib_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%managers}}',[
            'id' => $this->primaryKey(),
            'fio' => $this->string(),
            'email' => $this->string(),
        ], $tableOptions);
        $this->addColumn('{{%distributor}}', 'manager_id', $this->integer());
        $this->addForeignKey('fk-manager-distrib', '{{%distributor}}', 'manager_id', '{{%admin_users}}', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-manager-distrib', '{{%distributor}}');
        $this->dropColumn('{{%distributor}}', 'manager_id');
        $this->dropTable('{{%managers}}');

    }
}
