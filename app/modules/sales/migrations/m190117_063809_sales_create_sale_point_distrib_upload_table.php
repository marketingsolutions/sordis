<?php

use yii\db\Migration;

/**
 * Class m190117_063809_sales_create_sale_point_distrib_upload_table
 */
class m190117_063809_sales_create_sale_point_distrib_upload_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%upload_sale_point_distrib_file}}', [
            'id' => $this->primaryKey(),
            'original_file_name' => $this->string(),
            'file_name' => $this->string(),
            'status_process' => $this->string(16),
            'created_at' => $this->dateTime(),
            'processed_at' => $this->dateTime(),
        ], $tableOptions);
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%upload_sale_point_distrib_file}}');
    }
}
