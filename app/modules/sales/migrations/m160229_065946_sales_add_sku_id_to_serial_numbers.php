<?php

use yii\db\Migration;

class m160229_065946_sales_add_sku_id_to_serial_numbers extends Migration
{
    public function up()
    {
        $this->addColumn('{{%serial_numbers}}', 'product_id', $this->integer());
        $this->createIndex('product_id', '{{%serial_numbers}}', 'product_id');
        $this->addForeignKey(
            '{{%fk-serial_numbers-products}}',
            '{{%serial_numbers}}', 'product_id',
            '{{%sales_products}}', 'id',
            'SET NULL', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropColumn('{{%serial_numbers}}', 'product_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
