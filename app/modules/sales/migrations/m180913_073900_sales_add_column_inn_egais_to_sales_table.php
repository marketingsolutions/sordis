<?php

use yii\db\Migration;

/**
 * Class m180913_073900_sales_add_column_inn_egais_to_sales_table
 */
class m180913_073900_sales_add_column_inn_egais_to_sales_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%sale_point}}', 'inn', $this->string(25));
        $this->addColumn('{{%sale_point}}', 'work_time_from' , $this->time());
        $this->addColumn('{{%sale_point}}', 'work_time_to' , $this->time());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%sale_point}}', 'inn');
        $this->dropColumn('{{%sale_point}}', 'work_time_from');
        $this->dropColumn('{{%sale_point}}', 'work_time_to');
    }
}
