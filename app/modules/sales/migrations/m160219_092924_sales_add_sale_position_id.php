<?php

use yii\db\Migration;

class m160219_092924_sales_add_sale_position_id extends Migration
{
    public function up()
    {
        $this->addColumn('{{%sale_documents}}', 'sale_position_id', $this->integer());
        $this->addColumn('{{%sales_positions}}', 'validation_method', $this->string(16));

    }

    public function down()
    {
        $this->dropColumn('{{%sales_positions}}', 'validation_method');
        $this->dropColumn('{{%sale_documents}}', 'sale_position_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
