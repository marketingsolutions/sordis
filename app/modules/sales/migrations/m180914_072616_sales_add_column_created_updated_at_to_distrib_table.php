<?php

use yii\db\Migration;

/**
 * Class m180914_072616_sales_add_column_created_updated_at_to_distrib_table
 */
class m180914_072616_sales_add_column_created_updated_at_to_distrib_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%distributor}}', 'kpp', $this->string(25));
        $this->addColumn('{{%distributor}}', 'created_at', $this->dateTime());
        $this->addColumn('{{%distributor}}', 'updated_at', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%distributor}}', 'kpp');
        $this->dropColumn('{{%distributor}}', 'created_at');
        $this->dropColumn('{{%distributor}}', 'updated_at');
    }
}
