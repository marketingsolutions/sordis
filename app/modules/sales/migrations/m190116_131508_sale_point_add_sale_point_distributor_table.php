<?php

use yii\db\Migration;

/**
 * Class m190116_131508_sale_point_add_sale_point_distributor_table
 */
class m190116_131508_sale_point_add_sale_point_distributor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%sale_point_distrib}}', [
            'id' => $this->primaryKey(),
            'sale_point_id' => $this->integer(),
            'distributor_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_sale_point_distrib-sale_point', '{{%sale_point_distrib}}', 'sale_point_id', '{{%sale_point}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk_sale_point_distrib-distributor', '{{%sale_point_distrib}}', 'distributor_id', '{{%distributor}}', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_sale_point_distrib-sale_point', '{{%sale_point_distrib}}');
        $this->dropForeignKey('fk_sale_point_distrib-distributor', '{{%sale_point_distrib}}');
        $this->dropTable('{{%sale_point_distrib}}');
    }
}
