<?php

use yii\db\Migration;

class m160219_063528_sales_add_sku_serial_numbers extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%serial_numbers}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'activated_at' => $this->dateTime(),
            'created_at' => $this->dateTime(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%serial_numbers}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
