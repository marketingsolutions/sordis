<?php

require_once(Yii::getAlias('@marketingsolutions/files/migrations/m150410_074156_files_init.php'));

class m150623_095542_sales_create_sale_documents_table extends m150410_074156_files_init
{
    const FILES_TABLE = '{{%sale_documents}}';

    public function up()
    {
        parent::up();
    }

    public function down()
    {
        parent::down();
    }
}
