<?php

use yii\db\Migration;

class m160408_141059_sales_add_comment_to_sales extends Migration
{
    public function up()
    {
        $this->addColumn('{{%sales}}', 'review_comment', $this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%sales}}', 'review_comment');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
