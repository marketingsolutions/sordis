<?php

use yii\db\Migration;
use yii\db\Schema;

class m150619_125851_sale_add_approved_by_admin_at_to_sale extends Migration
{
    public function up()
    {
        $this->addColumn('{{%sales}}', 'approved_by_admin_at', Schema::TYPE_DATETIME);
    }

    public function down()
    {
        $this->dropColumn('{{%sales}}', 'approved_by_admin_at');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
