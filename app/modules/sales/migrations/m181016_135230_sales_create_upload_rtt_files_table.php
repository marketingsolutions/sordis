<?php

use yii\db\Migration;

/**
 * Class m181016_135230_sales_create_upload_rtt_files_table
 */
class m181016_135230_sales_create_upload_rtt_files_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%sale_point_upload_files}}', [
            'id' => $this->primaryKey(),
            'original_file_name' => $this->string(),
            'file_path' => $this->string(),
            'file_name' => $this->string(),
            'status_process' => $this->string(25),
            'created_at' => $this->dateTime(),
            'processed_at' => $this->dateTime(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sale_point_upload_files}}');
    }
}
