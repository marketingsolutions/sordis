<?php

use yii\db\Migration;
use yii\db\Schema;

class m150623_124340_sales_add_bonuses_paid_at_to_sales extends Migration
{
    public function up()
    {
        $this->addColumn('{{%sales}}', 'bonuses_paid_at', Schema::TYPE_DATETIME);
    }

    public function down()
    {
        $this->dropColumn('{{%sales}}', 'bonuses_paid_at');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
