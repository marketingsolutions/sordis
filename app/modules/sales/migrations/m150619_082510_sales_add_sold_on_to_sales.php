<?php

use yii\db\Migration;
use yii\db\Schema;

class m150619_082510_sales_add_sold_on_to_sales extends Migration
{
    public function up()
    {
        $this->addColumn('{{%sales}}', 'sold_on', Schema::TYPE_DATETIME);
    }

    public function down()
    {
        $this->dropColumn('{{%sales}}', 'sold_on');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
