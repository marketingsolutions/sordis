<?php

use yii\db\Migration;

class m170711_135035_create_table_competition extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%competition}}', [
            'id' => $this->primaryKey(),
            'description'=>$this->string(255),
            'item1'=>$this->string(255),
            'item2'=>$this->string(255),
            'item3'=>$this->string(255),
            'item4'=>$this->string(255),
            'item5'=>$this->string(255),
            'date_from'=>$this->dateTime(),
            'date_to'=>$this->dateTime(),
            'is_active'=>$this->boolean()->defaultValue(0),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%competition}}');
    }
}
