<?php

use yii\db\Migration;

/**
 * Class m180913_131515_sales_add_column_kpp_to_sale_point_table
 */
class m180913_131515_sales_add_column_kpp_to_sale_point_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%sale_point}}', 'kpp', $this->string(25));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%sale_point}}', 'kpp');
    }
}
