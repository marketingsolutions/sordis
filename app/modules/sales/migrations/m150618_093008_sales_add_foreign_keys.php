<?php

use yii\db\Migration;

class m150618_093008_sales_add_foreign_keys extends Migration
{
    public function up()
    {
        $this->addForeignKey('{{%fk-sales-products-units}}', '{{%sales_products}}', 'unit_id', '{{%sales_units}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('{{%fk-sales-products-categories}}', '{{%sales_products}}', 'category_id', '{{%sales_categories}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('{{%fk-sales-products-categories}}', '{{%sales_products}}');
        $this->dropForeignKey('{{%fk-sales-products-units}}', '{{%sales_products}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
