<?php

use yii\db\Migration;

/**
 * Class m190813_105654_sales_create_table_download_rtt_table
 */
class m190813_105654_sales_create_table_download_rtt_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%download_rtt_to_xml}}', [
            'id' => $this->primaryKey(),
            'original_file_name' => $this->string(),
            'file_name' => $this->string(),
            'status_process' => $this->string(16),
            'created_at' => $this->dateTime(),
            'processed_at' => $this->dateTime(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%download_rtt_to_xml}}');
    }
}
