<?php

use yii\db\Migration;
use yii\db\Schema;

class m150618_114946_sales_add_sales_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%sales}}', [
            'id' => Schema::TYPE_PK,
            'status' => Schema::TYPE_STRING . '(16)',
            'recipient_id' => Schema::TYPE_INTEGER,
            'bonuses' => Schema::TYPE_INTEGER,
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);

        $this->createTable('{{%sales_positions}}', [
            'id' => Schema::TYPE_PK,
            'sale_id' => Schema::TYPE_INTEGER,
            'product_id' => Schema::TYPE_INTEGER,
            'quantity' => Schema::TYPE_INTEGER,
            'bonuses' => Schema::TYPE_INTEGER,
        ], $tableOptions);

        $this->createIndex('sale_id', '{{%sales_positions}}', 'sale_id');
        $this->createIndex('product_id', '{{%sales_positions}}', 'product_id');
    }

    public function down()
    {
        $this->dropTable('{{%sales}}');
        $this->dropTable('{{%sales_positions}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
