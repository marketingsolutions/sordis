<?php

use yii\db\Migration;

class m170503_085548_add_column_custom_serial_to_sales_position_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%sales_positions}}', 'custom_serial', $this->string(50));
    }

    public function down()
    {
        $this->dropColumn('{{%sales_positions}}', 'custom_serial');
    }
}
