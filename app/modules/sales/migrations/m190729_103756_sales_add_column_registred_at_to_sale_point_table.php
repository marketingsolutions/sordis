<?php

use yii\db\Migration;

/**
 * Class m190729_103756_sales_add_column_registred_at_to_sale_point_table
 */
class m190729_103756_sales_add_column_registred_at_to_sale_point_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%sale_point}}', 'registred_at', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%sale_point}}', 'registred_at');
    }
}
