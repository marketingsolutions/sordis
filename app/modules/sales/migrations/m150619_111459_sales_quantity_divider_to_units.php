<?php

use yii\db\Migration;
use yii\db\Schema;

class m150619_111459_sales_quantity_divider_to_units extends Migration
{
    public function up()
    {
        $this->addColumn('{{%sales_units}}', 'quantity_divider', Schema::TYPE_INTEGER . ' DEFAULT 1');
    }

    public function down()
    {
        $this->dropColumn('{{%sales_units}}', 'quantity_divider');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
