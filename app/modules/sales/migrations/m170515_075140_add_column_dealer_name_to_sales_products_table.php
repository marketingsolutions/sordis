<?php

use yii\db\Migration;

class m170515_075140_add_column_dealer_name_to_sales_products_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%sales_products}}', 'dealer_name', $this->string(150));
    }

    public function down()
    {
        $this->dropColumn('{{%sales_products}}', 'dealer_name');
    }

}
