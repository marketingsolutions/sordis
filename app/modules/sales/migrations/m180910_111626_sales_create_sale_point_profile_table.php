<?php

use yii\db\Migration;

/**
 * Class m180910_111626_sales_create_sale_point_profile_table
 */
class m180910_111626_sales_create_sale_point_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%sale_point_profile}}', [
            'id' => $this->primaryKey(),
            'profile_id' => $this->integer(),
            'sale_point_id' => $this->integer(),
        ], $tableOptions);
        $this->addForeignKey('fk_sale_point_profile_profile', '{{%sale_point_profile}}', 'profile_id', '{{%profiles}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk_sale_point_profile_sale_point', '{{%sale_point_profile}}', 'sale_point_id', '{{%sale_point}}', 'id', 'RESTRICT', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_sale_point_profile_profile', '{{%sale_point_profile}}');
        $this->dropForeignKey('fk_sale_point_profile_sale_point', '{{%sale_point_profile}}');
        $this->dropTable('{{%sale_point_profile}}');
    }
}
