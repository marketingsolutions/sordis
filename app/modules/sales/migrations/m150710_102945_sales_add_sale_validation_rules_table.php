<?php

use yii\db\Migration;

class m150710_102945_sales_add_sale_validation_rules_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%sale_validation_rules}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128),
            'is_enabled' => $this->boolean(),
            'rule' => $this->string(255),
            'error' => $this->string(255),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%sale_validation_rules}}');
    }
}
