<?php

use yii\db\Migration;
use yii\db\Schema;

class m150623_095720_sales_add_sale_id extends Migration
{
    public function up()
    {
        $this->addColumn('{{%sale_documents}}', 'sale_id', Schema::TYPE_INTEGER);
        $this->createIndex('sale_id', '{{%sale_documents}}', 'sale_id');
        $this->addForeignKey('{{%fk-sale_documents-sales}}', '{{%sale_documents}}', 'sale_id', '{{%sales}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('{{%fk-sale_documents-sales}}', '{{%sale_documents}}');
        $this->dropColumn('{{%sale_documents}}', 'sale_id');
    }
}
