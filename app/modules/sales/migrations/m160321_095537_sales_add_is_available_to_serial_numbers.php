<?php

use yii\db\Migration;

class m160321_095537_sales_add_is_available_to_serial_numbers extends Migration
{
    public function up()
    {
        $this->addColumn('{{%serial_numbers}}', 'is_available', $this->boolean()->defaultValue(1));
    }

    public function down()
    {
        $this->dropColumn('{{%serial_numbers}}', 'is_available');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
