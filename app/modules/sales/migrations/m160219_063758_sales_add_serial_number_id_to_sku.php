<?php

use yii\db\Migration;

class m160219_063758_sales_add_serial_number_id_to_sku extends Migration
{
    public function up()
    {
        $this->addColumn('{{%sales_positions}}', 'serial_number_id', $this->integer());
        $this->createIndex('serial_number_id', '{{%sales_positions}}', 'serial_number_id');
        $this->addForeignKey(
            '{{%fk-sales_positions-serial_numbers}}',
            '{{%sales_positions}}', 'serial_number_id',
            '{{%serial_numbers}}', 'id',
            'RESTRICT', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropColumn('{{%sales_positions}}', 'serial_number_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
