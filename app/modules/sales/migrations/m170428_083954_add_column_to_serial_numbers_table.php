<?php

use yii\db\Migration;

class m170428_083954_add_column_to_serial_numbers_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%serial_numbers}}', 'sale_point_name', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('{{%serial_numbers}}', 'sale_point_name');
    }


}
