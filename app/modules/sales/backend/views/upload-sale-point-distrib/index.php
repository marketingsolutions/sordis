<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use yz\icons\Icons;
use modules\sales\common\models\UploadSalePointDistrib;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\sales\backend\models\UploadSalePointDistribSearch $searchModel
 * @var array $columns
 */

$this->title = modules\sales\common\models\UploadSalePointDistrib::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'upload-sale-point-distrib-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [/*['search'],*/ ['export', 'create', 'delete', 'return']],
            'gridId' => 'upload-sale-point-distrib-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\sales\common\models\UploadSalePointDistrib',
        ]) ?>
    </div>

    <?= GridView::widget([
        'id' => 'upload-sale-point-distrib-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '{rollback} {delete}',
                'buttons' => [
                    'rollback' => function ($url, UploadSalePointDistrib $model) {
                        $url = '/sales/upload-sale-point-distrib/rollback?id='.$model->id;
                        if ($model->status_process == UploadSalePointDistrib::STATUS_NEW || $model->status_process == UploadSalePointDistrib::STATUS_COMPLETE) {
                            return '';
                        }
                        return Html::a(Icons::i('refresh'), $url, [
                            'title' => 'Обработать заново',
                            'data-confirm' => 'Вы действительно хотите обработать данный файл заново?',
                            'data-method' => 'post',
                            'class' => 'btn btn-warning btn-sm',
                            //'data-pjax' => '0',
                        ]);
                    },
                ],
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
