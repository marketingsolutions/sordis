<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\widgets\ActionButtons;

/**
 * @var yii\web\View $this
 * @var modules\sales\common\models\UploadSalePointDistrib $model
 */
$this->title = 'Загрузка РТТ и дистрибьюторов из XML файла';
$this->params['breadcrumbs'][] = ['label' => modules\sales\common\models\UploadSalePointDistrib::modelTitlePlural(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<div class="upload-sale-point-distrib-create">

    <div class="text-right">
        <?php  Box::begin() ?>
        <?=  ActionButtons::widget([
            'order' => [['index', 'create', 'return']],
            'addReturnUrl' => false,
        ]) ?>
        <?php  Box::end() ?>
    </div>

    <?php echo $this->render('_form', [
        'model' => $model,
        'uploadForm' => $uploadForm,
    ]); ?>

</div>
