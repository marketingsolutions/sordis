<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use yz\icons\Icons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\sales\backend\models\DistributorSearch $searchModel
 * @var array $columns
 */

$this->title = modules\sales\common\models\Distributor::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'distributor-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [/*['search'],*/ [ 'manager-import', 'import', 'export', 'create', /*'delete',*/ 'return']],
            'gridId' => 'distributor-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\sales\common\models\Distributor',
            'buttons' =>[
                'import' => [
                    'label' => 'Загрузка Дистрибьюторов из XLS-файла',
                    'icon' => Icons::o('upload'),
                    'route' => ['/sales/import-distrib/index'],
                    'class' => 'btn btn-info',
                ],
                'manager-import' => [
                    'label' => 'Загрузка Менеджеров из XLS-файла',
                    'icon' => Icons::o('upload'),
                    'route' => ['/sales/import-manager/index'],
                    'class' => 'btn btn-info',
                ],
            ]
        ]) ?>
    </div>
    <?= GridView::widget([
        'id' => 'distributor-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '{update}',
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
