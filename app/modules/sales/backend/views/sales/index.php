<?php

use modules\sales\common\models\Sale;
use modules\sales\common\sales\statuses\Statuses;
use yii\helpers\Html;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;
use yz\icons\Icons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\sales\backend\models\SaleSearch $searchModel
 * @var array $columns
 */

$this->title = modules\sales\common\models\Sale::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'sale-index box-primary']) ?>
<div class="text-right">
    <?php
//    echo "<pre>";
//    print_r($model);
//    echo "</pre>";
    ?>

    <?php echo ActionButtons::widget([
        'order' => [['export', /*'approve',*/ 'create', 'delete', 'return']],
        'gridId' => 'sale-grid',
        'searchModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'modelClass' => 'modules\sales\common\models\Sale',
        'buttons' => [
            'approve' => function () {
                return \yii\bootstrap\ButtonDropdown::widget([
                    'tagName' => 'span',
                    'label' => 'Изменить статус у выбранных',
                    'encodeLabel' => false,
                    'split' => false,
                    'dropdown' => [
                        'encodeLabels' => false,
                        'items' => [
                            [
                                'label' => 'Одобрить выбранные',
                                'url' => ['change-status-selected', 'status' => Statuses::APPROVED],
                                'linkOptions' => [
                                    'data' => [
                                        'grid' => 'sale-grid',
                                        'grid-bind' => 'selection',
                                        'grid-param' => 'ids',
                                        'confirm' => 'Вы уверены, что одобрить выбранные записи?',
                                    ],
                                ]
                            ],
                            [
                                'label' => 'Начислить баллы',
                                'url' => ['change-status-selected', 'status' => Statuses::PAID],
                                'linkOptions' => [
                                    'data' => [
                                        'grid' => 'sale-grid',
                                        'grid-bind' => 'selection',
                                        'grid-param' => 'ids',
                                        'confirm' => 'Вы уверены, что одобрить выбранные записи?',
                                    ],
                                ]
                            ]
                        ]
                    ],
                    'options' => [
                        'class' => 'btn btn-default',
                    ],
                ]);
            }
        ]
    ]) ?>
</div>

<?= GridView::widget([
    'id' => 'sale-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => array_merge([
        ['class' => 'yii\grid\CheckboxColumn'],
    ], $columns, [
        [
            'class' => 'yz\admin\widgets\ActionColumn',
            'template' => '{pay} {view} {delete}',
            'buttons' => [
            	'pay' => function ($url, Sale $model, $key) {
					if ($model->status != 'approved') {
						return '';
					}

					$url = ['/sales/sales/change-status', 'id' => $model->id, 'status' => Statuses::PAID];

                    return Html::a(Icons::i('usd'), $url, [
                        'title' => 'Зачислить бонусы по одобренной продаже',
                        'data-confirm' => 'Вы уверены, что хотите зачислить бонусы?',
                        'data-method' => 'post',
                        'class' => 'btn btn-success btn-sm',
                        'data-pjax' => '0',
                    ]);
                },
                'delete' => function ($url, Sale $model, $key) {
                    if ($model->statusManager->canBeDeleted() == false) {
                        return '';
                    }

                    return Html::a(Icons::i('trash-o fa-lg'), $url, [
                        'title' => Yii::t('admin/t', 'Delete'),
                        'data-confirm' => Yii::t('admin/t', 'Are you sure to delete this item?'),
                        'data-method' => 'post',
                        'class' => 'btn btn-danger btn-sm',
                        'data-pjax' => '0',
                    ]);
                }
            ]
        ],
    ]),
]); ?>
<?php Box::end() ?>

