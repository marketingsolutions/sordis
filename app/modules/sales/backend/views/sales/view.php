<?php

use modules\sales\backend\assets\FancyBoxAsset;
use modules\sales\common\sales\statuses\Statuses;
use marketingsolutions\thumbnails\Thumbnail;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;

/**
 * @var yii\web\View $this
 * @var modules\sales\common\models\Sale $model
 */
$this->title = \Yii::t('admin/t', 'Update {item}', ['item' => modules\sales\common\models\Sale::modelTitle()]);
$this->params['breadcrumbs'][] = ['label' => modules\sales\common\models\Sale::modelTitlePlural(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

FancyBoxAsset::register($this);
?>
	<div class="sale-update">

		<div class="text-right">
            <?php Box::begin() ?>
            <?= ActionButtons::widget([
                'order' => [['index', 'return']],
                'addReturnUrl' => false,
            ]) ?>
            <?php Box::end() ?>
		</div>

		<div class="row">
			<div class="col-md-8">
                <?php $box = Box::begin(['cssClass' => 'sale-form box-primary', 'title' => '']) ?>

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'created_at',
                        [
                            'attribute' => 'status',
                            'value' => $model->getStatusText(),
                        ],
                        [
                            'attribute' => 'review_comment',
                            'visible' => $model->review_comment !== null,
                        ],
                        'bonuses',
                        'sold_on:date',
                    ],
                ]) ?>

				<h3>Состав продажи</h3>

				<table class="table">
					<tr>
						<th>Группа товаров</th>
						<th>Товар</th>
						<th>Количество</th>
						<th>Бонусы</th>
					</tr>
                    <?php foreach ($model->positions as $position): ?>
						<tr>
							<td><?= Html::encode($position->category->name) ?></td>
							<td><?= Html::encode($position->product->name) ?></td>
							<td><?= $position->quantityLocal ?> <?= $position->unit->short_name ?></td>
							<td><?= $position->bonuses ?></td>
						</tr>
                    <?php endforeach ?>

					<tr>
						<th colspan="4" class="text-center">Подтверждающие документы</th>
					</tr>
					<tr>

						<th colspan="4">
							<div class="row">
                                <?php foreach ($model->documents as $document): ?>
									<div class="col-md-3">
										<a href="<?= Url::to(['download-document', 'id' => $document->id]) ?>"
										   rel="group"
										   target="_blank"
										   class="thumbnail <?= $document->isImage ? 'fancybox' : '' ?>">
                                            <?php if ($document->isImage): ?>
												<img
													src="<?= (new Thumbnail())->url($document->getFileName(), Thumbnail::thumb(300, 600), '300x600') ?>"
													alt=""/>
                                            <?php else: ?>
                                                <?= Html::encode($document->original_name) ?>
                                            <?php endif ?>
										</a>
									</div>
                                <?php endforeach ?>
							</div>

						</th>
					</tr>
                    <?php
                    //endif;
                    ?>
					<tr>
						<th colspan="4" class="text-center">Итого бонусов: <?= $model->bonuses ?></th>
					</tr>
				</table>

                <?php $box = Box::end() ?>
			</div>
			<div class="col-md-4">
                <?php Box::begin(['cssClass' => 'box-primary', 'title' => 'Действия']) ?>

                <?php if ($model->status == Statuses::PAID): ?>
					<div>
						<span class="label label-success">БОНУСЫ ЗАЧИСЛЕНЫ</span>
					</div>
                <?php endif; ?>

                <?php if ($model->statusManager->adminCanEdit()): ?>
					<div class="panel panel-success">
						<div class="panel-heading">
							Редактирование продажи
						</div>
						<div class="panel-body">
							<a class="btn btn-success btn-block"
							   href="<?= Url::to(['app', 'id' => $model->id]) ?>">
								<i class="fa fa-spin"></i> Изменить состав продажи
							</a>
						</div>
					</div>
                <?php endif ?>

                <?php if ($model->statusManager->adminCanSetStatus(Statuses::PAID)): ?>
					<div class="panel panel-success">
						<div class="panel-heading">
							Начисление бонусов
						</div>
						<div class="panel-body">
							<a class="btn btn-success btn-block"
							   href="<?= Url::to(['change-status', 'id' => $model->id, 'status' => Statuses::PAID]) ?>">
								<i class="fa fa-dollar"></i> Начислить баллы
							</a>
						</div>
					</div>
                <?php endif ?>

                <?php if ($model->statusManager->adminCanSetStatus(Statuses::APPROVED)): ?>
					<div class="panel panel-success">
						<div class="panel-heading">
							Одобрение продажи
						</div>
						<div class="panel-body">
							<a class="btn btn-success btn-block"
							   href="<?= Url::to(['change-status', 'id' => $model->id, 'status' => Statuses::APPROVED]) ?>">
								<i class="fa fa-check"></i> Одобрить продажу
							</a>
						</div>
					</div>
                <?php endif ?>

                <?php if ($model->statusManager->adminCanSetStatus(Statuses::DECLINED)): ?>
					<div class="panel panel-danger">
						<div class="panel-heading">
							Отклонение продажи
						</div>
						<div class="panel-body">
                            <?= Html::beginForm(['change-status'], 'post') ?>
                            <?= Html::hiddenInput('id', $model->id) ?>
                            <?= Html::hiddenInput('status', Statuses::DECLINED) ?>
							<div class="form-group">
								<label for="">Комментарий</label>
                                <?= Html::textInput('comment', null, ['class' => 'form-control', 'style' => 'margin-bottom: 10px']) ?>
							</div>
							<button type="submit" class="btn btn-danger btn-block">
								<i class="fa fa-cross"></i> Отклонить продажу
							</button>
						</div>
					</div>
                <?php endif ?>

                <?php if ($model->statusManager->adminCanSetStatus(Statuses::DRAFT)): ?>
					<div class="panel panel-success">
						<div class="panel-heading">
							Возврат участнику
						</div>
						<div class="panel-body">
                            <?= Html::beginForm(['change-status'], 'post') ?>
                            <?= Html::hiddenInput('id', $model->id) ?>
                            <?= Html::hiddenInput('status', Statuses::DRAFT) ?>
                            <?= Html::textInput('comment', null, ['class' => 'form-control', 'style' => 'margin-bottom: 10px']) ?>
							<button type="submit" class="btn btn-default btn-block">
								<i class="fa fa-cross"></i> Перевести в статус "Черновик" и вернуть участнику
							</button>
						</div>
					</div>
                <?php endif ?>

                <?php Box::end() ?>
			</div>
		</div>

	</div>

<?php

$js = <<<JS
$('.fancybox').fancybox();
JS;
$this->registerJs($js);
