<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use yz\icons\Icons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\sales\backend\models\SalePointSearch $searchModel
 * @var array $columns
 */

$this->title = modules\sales\common\models\SalePoint::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'sale-point-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [['import', 'export', 'create', /*'delete', */'return']],
            'gridId' => 'sale-point-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\sales\common\models\SalePoint',
            'buttons' =>[
                'import' => [
                    'label' => 'Загрузка РТТ из CSV-файла',
                    'icon' => Icons::o('upload'),
                    'route' => ['/sales/import-rtt/index'],
                    'class' => 'btn btn-info',
                ],
            ]
        ]) ?>
    </div>
    <?= GridView::widget([
        'id' => 'sale-point-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '{update}',
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
