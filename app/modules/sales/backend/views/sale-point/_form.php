<?php

use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var modules\sales\common\models\SalePoint $model
 * @var yz\admin\widgets\ActiveForm $form
 */
$this->registerCssFile('https://cdn.jsdelivr.net/npm/suggestions-jquery@18.3.3/dist/css/suggestions.min.css');
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('https://cdn.jsdelivr.net/npm/suggestions-jquery@18.3.3/dist/js/jquery.suggestions.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$js = <<<JS
    $( document ).ready(function() {
         /*Дадата для адреса*/
    var dadataToken = $('#dadataToken').val();
    $("#salepoint-address").suggestions({
        token: dadataToken,
        type: "ADDRESS",
        count: 5,
        /* Вызывается, когда пользователь выбирает одну из подсказок */
        onSelect: function(suggestion) {
            console.log(suggestion);
            var cityMy = suggestion.data.city;
            console.log(cityMy);
            $('#registrationmanagerttform-city_region').val(cityMy);
        }
    });
});
JS;
$this->registerJs($js);
?>
<input type="hidden" id="dadataToken" value="<?=getenv('DADATA_TOKEN')?>">
<?php  $box = FormBox::begin(['cssClass' => 'sale-point-form box-primary', 'title' => '']) ?>
    <?php $form = ActiveForm::begin(); ?>

    <?php $box->beginBody() ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'license_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'code_egais')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'inn')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'kpp')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'work_time_from')->textInput()->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '99:99',
    ]) ?>
    <?= $form->field($model, 'work_time_to')->textInput()->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '99:99',
    ]) ?>


    <?= $form->field($model, 'created_at')->hiddenInput(['value' => date("Y-m-d H:i:s")])->label(false) ?>

    <?= $form->field($model, 'updated_at')->hiddenInput(['value' => date("Y-m-d H:i:s")])->label(false) ?>

    <?php $box->endBody() ?>

    <?php $box->actions([
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
    ]) ?>
    <?php ActiveForm::end(); ?>

<?php  FormBox::end() ?>
