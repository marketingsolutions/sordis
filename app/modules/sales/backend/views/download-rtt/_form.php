<?php

use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var modules\sales\common\models\DownloadRtt $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php  $box = FormBox::begin(['cssClass' => 'download-rtt-form box-primary', 'title' => '']) ?>
    <?php $form = ActiveForm::begin(); ?>

    <?php $box->beginBody() ?>
<h4>Новая выгрузка начнется в фоновом режиме после нажатия на кнопку <i><b>Сохранить</b></i>. Скачать файл можно по ссылке в разделе <a href="/sales/download-rtt/index"><i><b>Выгрузка РТТ в xml</b></i></a> </h4>

    <?= $form->field($model, 'status_process')->hiddenInput(['value' => \modules\sales\common\models\DownloadRtt::STATUS_NEW])->label(false) ?>

    <?= $form->field($model, 'created_at')->hiddenInput(['value' => date("Y-m-d H:i:s")])->label(false) ?>

    <?php $box->endBody() ?>

    <?php $box->actions([
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
    ]) ?>
    <?php ActiveForm::end(); ?>

<?php  FormBox::end() ?>
