<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\sales\backend\models\DownloadRttSearch $searchModel
 * @var array $columns
 */

$this->title = modules\sales\common\models\DownloadRtt::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

$js=<<<JS
setTimeout(function() {
    window.location.reload();
}, 30000);

JS;
$this->registerJs($js);
?>
<?php $box = Box::begin(['cssClass' => 'download-rtt-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [/*['search'],*/ ['export', 'create', 'delete', 'return']],
            'gridId' => 'download-rtt-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\sales\common\models\DownloadRtt',
        ]) ?>
    </div>

    <?= GridView::widget([
        'id' => 'download-rtt-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '{delete}',
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
