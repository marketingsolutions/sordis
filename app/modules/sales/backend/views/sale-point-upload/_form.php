<?php

use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var modules\sales\common\models\SalePointUpload $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php  $box = FormBox::begin(['cssClass' => 'sale-point-upload-form box-primary', 'title' => '']) ?>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <?= $form->field($uploadForm, 'imageFile')->fileInput() ?>
        <button class="btn btn-primary">Загрузить</button>
    <?php ActiveForm::end(); ?>
<?php $box->endBody() ?>

<?php  FormBox::end() ?>
