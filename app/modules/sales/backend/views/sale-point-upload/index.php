<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\sales\backend\models\SalePointUploadSearch $searchModel
 * @var array $columns
 */

$this->title = modules\sales\common\models\SalePointUpload::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
?>
<?php $box = Box::begin(['cssClass' => 'sale-point-upload-index box-primary']) ?>
    <div class="text-right">
        <?php echo ActionButtons::widget([
            'order' => [/*['search'],*/ ['export', 'create', 'delete', 'return']],
            'gridId' => 'sale-point-upload-grid',
            'searchModel' => $searchModel,
            'modelClass' => 'modules\sales\common\models\SalePointUpload',
        ]) ?>
    </div>
    <?= GridView::widget([
        'id' => 'sale-point-upload-grid',
        'dataProvider' => $dataProvider,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'filterModel' => $searchModel,
        'columns' => array_merge([
            ['class' => 'yii\grid\CheckboxColumn'],
        ], $columns, [
            [
                'class' => 'yz\admin\widgets\ActionColumn',
                'template' => '{delete}',
            ],
        ]),
    ]); ?>
<?php Box::end() ?>
