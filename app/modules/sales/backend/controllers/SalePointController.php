<?php

namespace modules\sales\backend\controllers;

use Yii;
use modules\sales\common\models\SalePoint;
use modules\sales\backend\models\SalePointSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\widgets\MaskedInput;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * SalePointController implements the CRUD actions for SalePoint model.
 */
class SalePointController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var SalePointSearch $searchModel */
                    return Yii::createObject(SalePointSearch::className());
                },
                'dataProvider' => function($params, SalePointSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all SalePoint models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var SalePointSearch $searchModel */
        $searchModel = Yii::createObject(SalePointSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(SalePointSearch $searchModel)
    {
        return [
			'id',
			'name',
			'address',
			'license_number',
			'code_egais',
            'inn',
            'kpp',
            [
                'attribute' => 'work_time_from',
                'format' => 'time',
            ],
            [
                'attribute' =>  'work_time_to',
                 'format' => 'time',
            ],
            [
                'label' => 'Участник мотивационной программы «Копилка»',
                'attribute' => 'isParticipant',
                'filter' =>[
                    0 =>  'НЕТ',
                    1 => 'ДА',
                ],
                'value' => function($model){
                    if($model->isParticipant == 1){
                        return 'ДА';
                    }else{
                        return 'НЕТ';
                    }
                }

            ],
//            [
//                'label' => 'Дата регистрации в МП «Копилка»',
//                'attribute' => 'profileDateAdd',
//                'format' => ['date', 'php:d.m.Y H:i'],
//                'filterInputOptions' => [
//                    'class'       => 'form-control',
//                    'placeholder' => 'дд.мм.гггг'
//                    ],
//            ],
            [
                'attribute' => 'registred_at',
                'format' => ['date', 'php:d.m.Y H:i'],
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'дд.мм.гггг'
                ],
            ],

            [
                'label' => 'Дистрибьютор',
                'attribute' => 'distribs',
                'format' => 'raw',
                'value' => function($model){
                    $distribArr = [];
                    $distribArr = SalePoint::getDistributorName($model->id);
                    if(empty($distribArr)){
                        return null;
                    }
                    $returnTable = "<table>";
                    foreach ($distribArr as $distrib_name){
                        $returnTable .="<tr><td>".$distrib_name."</td></tr>";
                    }
                    $returnTable .="</table>";
                    return $returnTable;
                }

            ],
        ];
    }

    /**
     * Creates a new SalePoint model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SalePoint;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SalePoint model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
        ]);
	}


    /**
     * Deletes an existing SalePoint model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the SalePoint model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SalePoint the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SalePoint::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
