<?php

namespace modules\sales\backend\controllers;

use backend\base\Controller;
use modules\sales\common\models\Category;
use modules\sales\common\models\Product;
use modules\sales\common\models\Unit;
use yii\helpers\Html;
use yz\admin\import\BatchImportAction;
use yz\admin\import\ImportForm;
use yz\admin\import\InterruptImportException;

/**
 * Class ImportProfilesController
 */
class ImportCatalogController extends Controller
{
    const FIELD_CATEGORY = 'категория';
    const FIELD_NAME = 'модель_товара';
    const FIELD_FORMULA = 'бонусов_за_1шт';

    public function actions()
    {
        return [
            'index' => [
                'class' => BatchImportAction::className(),
                'extraView' => '@modules/sales/backend/views/import-catalog/index.php',
                'importConfig' => [
                    'availableFields' => [
                        self::FIELD_CATEGORY => 'Категория товара',
                        self::FIELD_NAME => 'Модель товара',
                        self::FIELD_FORMULA => 'Количество бонусов за единицу товара',
                    ],
                    'rowImport' => [$this, 'rowImport'],
                    'skipFirstLine' => true,
                ]
            ]
        ];
    }

    public function rowImport(ImportForm $form, array $row)
    {
        $unit = $this->findUnit();
        $category = $this->importCategory($row);
        $product = $this->importProduct($row, $category, $unit);
    }

    /**
     * @param $row
     * @return Category
     * @throws InterruptImportException
     */
    private function importCategory($row)
    {
        $name = trim($row[self::FIELD_CATEGORY]);
        $category = Category::findOne(['name' => $name]);

        if ($category === null) {
            $category = new Category();
            $category->name = $name;
            if ($category->save() === false) {
                throw new InterruptImportException('Ошибка при импорте категории: ' . implode(', ', $category->getFirstErrors()), $row);
            }
        }

        return $category;
    }

    /**
     * @return Unit
     * @throws InterruptImportException
     */
    private function findUnit()
    {
        /** @var Unit $unit */
        $unit = Unit::find()->one();
        if ($unit === null) {
            throw new InterruptImportException('Не могу найти подходящую под описание единицу измерения: ' . Html::encode($row[self::FIELD_UNIT]), $row);
        }
        return $unit;
    }

    /**
     * @param Category $category
     * @param $row
     * @return Product
     * @throws InterruptImportException
     */
    private function importProduct($row, Category $category, Unit $unit)
    {
        $product = Product::findOne(['category_id' => $category->id, 'name' => $row[self::FIELD_NAME]]);

        if ($product === null) {
            $product = new Product();
            $product->name = $row[self::FIELD_NAME];
            $product->category_id = $category->id;
            $product->unit_id = $unit->id;
            $product->bonuses_formula = $row[self::FIELD_FORMULA] . "*q";

            if ($product->save() == false) {
                throw new InterruptImportException('Ошибка при импорте товара: ' . implode(', ', $product->getFirstErrors()), $row);
            }
        }
        else {
            $product->bonuses_formula = $row[self::FIELD_FORMULA] . "*q";
            $product->update(false);
        }

        return $product;
    }
}