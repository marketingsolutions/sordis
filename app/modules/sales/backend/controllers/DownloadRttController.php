<?php

namespace modules\sales\backend\controllers;

use Yii;
use modules\sales\common\models\DownloadRtt;
use modules\sales\backend\models\DownloadRttSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yz\admin\actions\ExportAction;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * DownloadRttController implements the CRUD actions for DownloadRtt model.
 */
class DownloadRttController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var DownloadRttSearch $searchModel */
                    return Yii::createObject(DownloadRttSearch::className());
                },
                'dataProvider' => function($params, DownloadRttSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all DownloadRtt models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var DownloadRttSearch $searchModel */
        $searchModel = Yii::createObject(DownloadRttSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(DownloadRttSearch $searchModel)
    {
        return [
			'id',
            [
                'attribute' => 'original_file_name',
                'label' => 'Ссылка на скачивание',
                'format' => 'raw',
                'value' => function($model){
                    if($model->status_process == DownloadRtt::STATUS_COMPLETE) {
                        return "<span class='label label-info' ><a style='color:white;font-size: 13px;' href='/sales/download-rtt/file?id=".$model->id."'>" . $model->file_name . "-скачать</a></span>";
                    }
                }
            ],
			'file_name',
            [
                'attribute' => 'status_process',
                'filter' => DownloadRtt::getStatus(),
                'format' => 'raw',
                'value' => function($data){
                    if($data->status_process == DownloadRtt::STATUS_NEW) {
                        return "<span  style='font-size: 12px;' class='label label-info'>".DownloadRtt::getStatus()[$data->status_process]."</span>";
                    }elseif($data->status_process == DownloadRtt::STATUS_PROCESS){
                        return  "<span style='font-size: 12px;' class='label label-warning'>".DownloadRtt::getStatus()[$data->status_process]."</span>";
                    }elseif($data->status_process == DownloadRtt::STATUS_COMPLETE){
                        return  "<span style='font-size: 12px;' class='label label-success'>".DownloadRtt::getStatus()[$data->status_process]."</span>";
                    }elseif ($data->status_process == DownloadRtt::STATUS_ERROR){
                        return  "<span style='font-size: 12px;' class='label label-danger'>".DownloadRtt::getStatus()[$data->status_process]."</span>";
                    }
                }
            ],
			'created_at:datetime',
        ];
    }

    public function actionFile()
    {
        //$path = Yii::getAlias('@modules/bonuses/common/files/bonuses.xls');
        $id = Yii::$app->request->get('id');
        $model = DownloadRtt::findOne($id);
        $path = $model->original_file_name.$model->file_name;

        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        }

        throw new NotFoundHttpException();
    }

    /**
     * Creates a new DownloadRtt model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DownloadRtt;

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
			return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DownloadRtt model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
        ]);
	}


    /**
     * Deletes an existing DownloadRtt model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the DownloadRtt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DownloadRtt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DownloadRtt::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
