<?php

namespace modules\sales\backend\controllers;

use backend\base\Controller;
use marketingsolutions\finance\models\Transaction;
use modules\profiles\common\models\Profile;
use modules\sales\backend\models\SaleSearch;
use modules\sales\common\actions\DownloadDocumentAction;
use modules\sales\common\finances\SalePartner;
use modules\sales\common\models\Sale;
use modules\sales\common\sales\statuses\Statuses;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yz\admin\actions\ExportAction;
use yz\admin\grid\columns\DataColumn;
use yz\admin\grid\filters\DateRangeFilter;
use yz\Yz;
use yii\helpers\Html;

/**
 * SalesController implements the CRUD actions for Sale model.
 */
class SalesController extends Controller
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'dataProvider' => function ($params) {
                    $searchModel = Yii::createObject(SaleSearch::className());
                    $dataProvider = $searchModel->search($params);

                    return $dataProvider;
                },
            ],
            'download-document' => [
                'class' => DownloadDocumentAction::className(),
            ],
            'download-small-document' => [
                'class' => DownloadDocumentAction::className(),
                'format' => DownloadDocumentAction::TYPE_SMALL,
            ]
        ]);
    }

    /**
     * Lists all Sale models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = Yii::createObject(SaleSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns(),
        ]);
    }

    public function getGridColumns()
    {
        return [
            'id',
            [
                'attribute' => 'status',
                'titles' => Sale::getStatusValues(),
                'filter' => Sale::getStatusValues(),
                'labels' => [
                    Statuses::DRAFT => DataColumn::LABEL_DEFAULT,
                    Statuses::ADMIN_REVIEW => DataColumn::LABEL_INFO,
                    Statuses::APPROVED => DataColumn::LABEL_SUCCESS,
                    Statuses::PAID => DataColumn::LABEL_DEFAULT,
                    Statuses::DECLINED => DataColumn::LABEL_DANGER,
                ],
                'contentOptions' => ['style' => 'width:200px;'],
            ],
            'profile__full_name',
            'profile__phone_mobile',
            'dealer__name',
            'total_positions',
            [
                'label' => 'Товар',
                'format' => 'raw',
                'value' => function ($model) {
                    return implode('<br><hr>', Sale::getTovars($model->id));
                }
            ],
            'bonuses',
            [
                'attribute' => 'sold_on',
                'filter' => DateRangeFilter::instance(),
                'value' => function ($model) {
                    return (new \DateTime($model->sold_on))->format('d.m.Y');
                }
            ],
            [
                'attribute' => 'created_at',
                'filter' => DateRangeFilter::instance(),
                'value' => function ($model) {
                    return (new \DateTime($model->created_at))->format('d.m.Y');
                }
            ],
        ];
    }

    /**
     * Updates an existing Sale model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionApp($id)
    {
        $model = $this->findModel($id);

        if ($model->statusManager->adminCanEdit() == false) {
            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('app', compact('model'));
    }

    /**
     * Deletes an existing Sale model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $status = Yz::FLASH_SUCCESS;
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array) $id;

        foreach ($id as $id_) {
            $model = $this->findModel($id_);
            if ($model->statusManager->canBeDeleted()) {
                $this->findModel($id_)->delete();
            }
            else {
                $status = Yz::FLASH_WARNING;
                $message = 'Некоторые (или все) из выбранных продаж невозможно удалить, т.к. их статус не равен "Черновик"/"Новая"';
            }
        }

        \Yii::$app->session->setFlash($status, $message);

        return $this->redirect(['index']);
    }

    public function actionChangeStatus($id = null, $status = null)
    {
        $this->enableCsrfValidation = false;
        if (!$id) {
            $id = Yii::$app->request->post('id');
        }
        if (!$status) {
            $status = Yii::$app->request->post('status');
        }

        $model = $this->findModel($id);

        if ($model->statusManager->adminCanSetStatus($status)) {
            switch ($status) {
                case Statuses::DRAFT:
                    $this->draft($model);
                    Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Статус успешно изменен, продажа возвращена на доработку участнику');
                    break;
                case Statuses::APPROVED:
                    $this->approved($model);
                    Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Продажа подтверждена, баллы за продажу подсчитаны');
                    break;
                case Statuses::PAID:
                    $this->paid($model);
                    Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Продажа выплачена, начислены баллы за продажу');
                    break;
                case Statuses::DECLINED:
                    $this->declined($model);
                    Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Продажа отклонена');
                    break;
                default:
                    throw new NotFoundHttpException();
            }
        }
        else {
            Yii::$app->session->setFlash(Yz::FLASH_ERROR, 'Статус не может быть изменен');
        }

        return $this->redirect(\Yii::$app->request->referrer ?: Url::home());
    }

    /**
     * @param Sale $sale
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     *
     * @return bool
     */
    private function paid(Sale $sale)
    {
        $transaction = Yii::$app->db->beginTransaction();

        /** @var Profile $profile */
        $profile = Profile::findOne($sale->recipient_id);

        $profile->purse->addTransaction(Transaction::factory(
            Transaction::INCOMING,
            $sale->bonuses,
            new SalePartner($sale->id),
            'Бонусы участнику "' . $profile->full_name . '" за продажу #' . $sale->id
        ));

        $sale->bonuses_paid_at = new Expression('NOW()');
        $sale->status = Statuses::PAID;
        $sale->save(false);

        $transaction->commit();

        return true;
    }

    /**
     * @param Sale $sale
     * @return bool
     */
    private function approved(Sale $sale)
    {
        $sale->status = Statuses::APPROVED;
        $sale->updateBonuses();
        $sale->save();

        return true;
    }

    /**
     * @param Sale $sale
     * @return bool
     */
    private function draft(Sale $sale)
    {
        $comment = \Yii::$app->request->post('comment');
        if (!empty($comment)) {
            $sale->review_comment = $comment;
        }
        $sale->status = Statuses::DRAFT;
        $sale->save();

        return true;
    }

    /**
     * @param Sale $sale
     * @return bool
     */
    private function declined(Sale $sale)
    {
        $comment = Yii::$app->request->post('comment');

        if (!empty($comment)) {
            $sale->review_comment = $comment;
        }
        $sale->status = Statuses::DECLINED;
        $sale->save();

        return true;
    }

    public function actionChangeStatusSelected(array $ids, $status)
    {
        $transaction = Sale::getDb()->beginTransaction();
        foreach ($ids as $id) {
            $model = $this->findModel($id);
            if ($model->statusManager->adminCanSetStatus($status)) {
                $model->statusManager->changeStatus($status);
            }
            else {
                \Yii::$app->session->setFlash(Yz::FLASH_ERROR, 'У ряда закупок статус не может быть изменен. Все изменения отменены');
                $transaction->rollBack();

                return $this->redirect(\Yii::$app->request->referrer ?: Url::home());
            }
        }
        $transaction->commit();
        Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Статус успешно изменен');

        return $this->redirect(\Yii::$app->request->referrer ?: Url::home());
    }

    /**
     * Finds the Sale model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Sale the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sale::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionFileCatalog()
    {
        $path = Yii::getAlias('@modules/sales/common/files/catalog.xlsx');

        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        }

        throw new NotFoundHttpException();
    }

    public function actionFileSales()
    {
        $path = Yii::getAlias('@modules/sales/common/files/sales.xlsx');

        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        }

        throw new NotFoundHttpException();
    }
}
