<?php

namespace modules\sales\backend\controllers;

use backend\base\Controller;
use modules\sales\common\models\Category;
use modules\sales\common\models\Product;
use modules\sales\common\models\SalePoint;
use modules\sales\common\models\SalePointProfile;
use modules\sales\common\models\Unit;
use yii\helpers\Html;
use yz\admin\import\BatchImportAction;
use yz\admin\import\ImportForm;
use yz\admin\import\InterruptImportException;
use yz\admin\import\SkipRowException;

/**
 * Class ImportProfilesController
 */
class ImportRttController extends Controller
{
    const FIELD_TT_NAME = 'имя_ртт';
    const FIELD_INN = 'инн';
    const FIELD_KPP = 'кпп';
    const FIELD_EGAIS = 'егаис';
    const FIELD_ADDRESS = 'адрес';


    public function actions()
    {
        return [
            'index' => [
                'class' => BatchImportAction::className(),
                //'extraView' => '@modules/sales/backend/views/import-catalog/index.php',
                'importConfig' => [
                    'availableFields' => [
                        self::FIELD_TT_NAME => 'Наименование РТТ',
                        self::FIELD_INN => 'ИНН',
                        self::FIELD_KPP => 'КПП',
                        self::FIELD_EGAIS => 'Код ЕГАИС',
                        self::FIELD_ADDRESS => 'Адрес РТТ',
                    ],
                    'rowImport' => [$this, 'rowImport'],
                    'skipFirstLine' => true,
                ]
            ]
        ];
    }

    public function rowImport(ImportForm $form, array $row)
    {
        $rtt = $this->importRtt($row);
    }


    private function importRtt($row)
    {
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', '0');
        $egaisCode = trim($row[self::FIELD_EGAIS]);
        $salePoint = SalePoint::findOne(['code_egais' =>$egaisCode ]);
        if(strlen($egaisCode)!=12){
            throw new SkipRowException('PTT c кодом ЕГАИС '.$egaisCode.' пропущена, так как данный код содержит менее 12 символов');
        }elseif($salePoint){
            $isUserSalePoint = SalePointProfile::findOne(['sale_point_id' => $salePoint->id]);
            if($isUserSalePoint) {
                throw new SkipRowException('PTT c кодом ЕГАИС ' . $egaisCode . ' пропущена, так как уже загружена в системе и занята участником');
            } else {
                $salePoint->name = $row[self::FIELD_TT_NAME];
                $salePoint->address =  $row[self::FIELD_ADDRESS];
                $salePoint->updated_at = date("Y-m-d H:i:s");
                $salePoint->inn = trim($row[self::FIELD_INN]);
                $salePoint->kpp = trim($row[self::FIELD_KPP]);
                $salePoint->update(false);
            }
        }else{
            $now = date("Y-m-d H:i:s");
            $newRtt = new SalePoint();
            $newRtt->name = $row[self::FIELD_TT_NAME];
            $newRtt->address = trim($row[self::FIELD_ADDRESS], ",");
            $newRtt->code_egais = $row[self::FIELD_EGAIS];
            $newRtt->created_at = $now;
            $newRtt->updated_at = $now;
            $newRtt->kpp = trim($row[self::FIELD_KPP]);
            $newRtt->inn = trim($row[self::FIELD_INN]);
            $newRtt->save(false);
        }
        return true;
    }
}