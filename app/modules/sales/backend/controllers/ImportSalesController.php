<?php

namespace modules\sales\backend\controllers;

use backend\base\Controller;
use libphonenumber\PhoneNumberFormat;
use modules\profiles\common\models\Profile;
use modules\sales\common\models\Category;
use modules\sales\common\models\Product;
use modules\sales\common\models\Sale;
use modules\sales\common\models\SalePosition;
use modules\sales\common\models\Unit;
use modules\sales\common\sales\statuses\Statuses;
use yii\helpers\Html;
use yz\admin\import\BatchImportAction;
use yz\admin\import\ImportForm;
use yz\admin\import\InterruptImportException;
use marketingsolutions\phonenumbers\PhoneNumber;

/**
 * Class ImportSalesController
 */
class ImportSalesController extends Controller
{
    const FIELD_CATEGORY = 'категория';
    const FIELD_NAME = 'модель_товара';
    const FIELD_FORMULA = 'бонусов_за_1шт';
    const FIELD_QTY = 'количество';
    const FIELD_AMOUNT = 'бонусов_за_продажу';
    const FIELD_PHONE = 'телефон_участника';

    public function actions()
    {
        return [
            'index' => [
                'class' => BatchImportAction::className(),
                'extraView' => '@modules/sales/backend/views/import-sales/index.php',
                'importConfig' => [
                    'availableFields' => [
                        self::FIELD_CATEGORY => 'Категория товара',
                        self::FIELD_NAME => 'Модель товара',
                        self::FIELD_FORMULA => 'Количество бонусов за единицу товара',
                        self::FIELD_QTY => 'Количество бонусов за единицу товара',
                        self::FIELD_AMOUNT => 'Бонусов за продажу',
                        self::FIELD_PHONE => 'Телефон участника',
                    ],
                    'rowImport' => [$this, 'rowImport'],
                    'skipFirstLine' => true,
                ]
            ]
        ];
    }

    public function rowImport(ImportForm $form, array $row)
    {
        if (empty($row[self::FIELD_QTY]) || empty($row[self::FIELD_PHONE])) {
            return;
        }

        $unit = $this->findUnit();
        $category = $this->importCategory($row);
        $product = $this->importProduct($row, $category, $unit);
        $profile = $this->findProfile($row);
        $sale = $this->importSale($row, $profile, $product);
    }

    private function importSale($row, Profile $profile, Product $product)
    {
        $sold_on = (new \DateTime('now'))->format('Y-m-d H:i:s');
        $sale = new Sale();
        $sale->recipient_id = $profile->id;
        $sale->sold_on = $sold_on;
        $sale->status = Statuses::ADMIN_REVIEW;
        $sale->save(false);

        $salePosition = new SalePosition();
        $salePosition->product_id = $product->id;
        $salePosition->sale_id = $sale->id;
        $salePosition->quantity = intval($row[self::FIELD_QTY]);
        $salePosition->validation_method = SalePosition::VALIDATION_METHOD_SERIAL;
        $salePosition->save(false);

        $sale->updateBonuses();

        return $sale;
    }

    private function findProfile($row)
    {
        if (PhoneNumber::validate($row[self::FIELD_PHONE], 'RU') == false) {
            throw new InterruptImportException('Неверный номер телефона: ' . $row[self::FIELD_PHONE], $row);
        }

        $phone = PhoneNumber::format($row[self::FIELD_PHONE], PhoneNumberFormat::E164, 'RU');
        $profile = Profile::findOne(['phone_mobile' => $phone]);

        if ($profile === null) {
            throw new InterruptImportException('Не найден участник по номеру: ' . $phone, $row);
        }

        return $profile;
    }

    /**
     * @param $row
     * @return Category
     * @throws InterruptImportException
     */
    private function importCategory($row)
    {
        $name = trim($row[self::FIELD_CATEGORY]);
        $category = Category::findOne(['name' => $name]);

        if ($category === null) {
            $category = new Category();
            $category->name = $name;
            if ($category->save() === false) {
                throw new InterruptImportException('Ошибка при импорте категории: ' . implode(', ', $category->getFirstErrors()), $row);
            }
        }

        return $category;
    }

    /**
     * @return Unit
     * @throws InterruptImportException
     */
    private function findUnit()
    {
        /** @var Unit $unit */
        $unit = Unit::find()->one();
        if ($unit === null) {
            throw new InterruptImportException('Не могу найти подходящую под описание единицу измерения: ' . Html::encode($row[self::FIELD_UNIT]), $row);
        }
        return $unit;
    }

    /**
     * @param Category $category
     * @param $row
     * @return Product
     * @throws InterruptImportException
     */
    private function importProduct($row, Category $category, Unit $unit)
    {
        $product = Product::findOne(['category_id' => $category->id, 'name' => $row[self::FIELD_NAME]]);

        if ($product === null) {
            $product = new Product();
            $product->name = $row[self::FIELD_NAME];
            $product->category_id = $category->id;
            $product->unit_id = $unit->id;
            $product->bonuses_formula = $row[self::FIELD_FORMULA] . "*q";

            if ($product->save() == false) {
                throw new InterruptImportException('Ошибка при импорте товара: ' . implode(', ', $product->getFirstErrors()), $row);
            }
        }
        else {
            $product->bonuses_formula = $row[self::FIELD_FORMULA] . "*q";
            $product->update(false);
        }

        return $product;
    }
}