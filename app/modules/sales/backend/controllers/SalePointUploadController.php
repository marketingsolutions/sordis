<?php

namespace modules\sales\backend\controllers;

use modules\sales\backend\forms\UploadRttForm;
use Yii;
use modules\sales\common\models\SalePointUpload;
use modules\sales\backend\models\SalePointUploadSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;
use yii\web\UploadedFile;

/**
 * SalePointUploadController implements the CRUD actions for SalePointUpload model.
 */
class SalePointUploadController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var SalePointUploadSearch $searchModel */
                    return Yii::createObject(SalePointUploadSearch::className());
                },
                'dataProvider' => function($params, SalePointUploadSearch $searchModel) {
                        $dataProvider = $searchModel->search($params);
                        return $dataProvider;
                    },
            ]
        ]);
    }

    /**
     * Lists all SalePointUpload models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var SalePointUploadSearch $searchModel */
        $searchModel = Yii::createObject(SalePointUploadSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(SalePointUploadSearch $searchModel)
    {
        return [
			'id',
			'original_file_name',
			//'file_path',
			'file_name',
            [
                'attribute' => 'status_process',
                'filter' => SalePointUpload::getStatus(),
                'format' => 'html',
                'value' => function($data){
                    if($data->status_process == SalePointUpload::STATUS_NEW) {
                        return "<span  style='font-size: 12px;' class='label label-info'>".SalePointUpload::getStatus()[$data->status_process]."</span>";
                    }elseif($data->status_process == SalePointUpload::STATUS_PROCESS){
                        return  "<span style='font-size: 12px;' class='label label-warning'>".SalePointUpload::getStatus()[$data->status_process]."</span>";
                    }elseif($data->status_process == SalePointUpload::STATUS_COMPLETE){
                        return  "<span style='font-size: 12px;' class='label label-success'>".SalePointUpload::getStatus()[$data->status_process]."</span>";
                    }elseif ($data->status_process == SalePointUpload::STATUS_ERROR){
                        return  "<span style='font-size: 12px;' class='label label-danger'>".SalePointUpload::getStatus()[$data->status_process]."</span>";
                    }
                }
            ],

			 'created_at:datetime',
			 'processed_at:datetime',
        ];
    }

    /**
     * Creates a new SalePointUpload model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SalePointUpload;
        $uploadForm = new UploadRttForm();
        if (Yii::$app->request->isPost) {

            $uploadForm->imageFile = UploadedFile::getInstance($uploadForm, 'imageFile');

            if ($uploadForm->upload()) {
                \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Файл загружен и передан на обработку'));
                return $this->redirect(['index']);

            }
        }

        return $this->render('create', [
            'model' => $model,
            'uploadForm' => $uploadForm,
        ]);
    }

    /**
     * Updates an existing SalePointUpload model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
			return $this->getCreateUpdateResponse($model);
		}

        return $this->render('update', [
            'model' => $model,
        ]);
	}


    /**
     * Deletes an existing SalePointUpload model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array)$id;

        foreach ($id as $id_)
            $this->findModel($id_)->delete();

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the SalePointUpload model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SalePointUpload the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SalePointUpload::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
