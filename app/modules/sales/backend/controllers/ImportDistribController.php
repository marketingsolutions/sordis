<?php

namespace modules\sales\backend\controllers;

use backend\base\Controller;
use modules\sales\common\models\Category;
use modules\sales\common\models\Distributor;
use modules\sales\common\models\Product;
use modules\sales\common\models\SalePoint;
use modules\sales\common\models\Unit;
use yii\helpers\Html;
use yz\admin\import\BatchImportAction;
use yz\admin\import\ImportForm;
use yz\admin\import\InterruptImportException;
use yz\admin\import\SkipRowException;

/**
 * Class ImportProfilesController
 */
class ImportDistribController extends Controller
{
    const FIELD_NAME = 'имя_дистриб';
    const FIELD_INN = 'инн';
    const FIELD_KPP = 'кпп';
    const FIELD_EGAIS = 'егаис';
    const FIELD_ADDRESS = 'адрес';

    public function actions()
    {
        return [
            'index' => [
                'class' => BatchImportAction::className(),
                //'extraView' => '@modules/sales/backend/views/import-catalog/index.php',
                'importConfig' => [
                    'availableFields' => [
                        self::FIELD_NAME => 'Наименование Дистрибьютора',
                        self::FIELD_INN => 'ИНН',
                        self::FIELD_KPP => 'КПП',
                        self::FIELD_EGAIS => 'Код ЕГАИС',
                        self::FIELD_ADDRESS => 'Юр. адрес дистрибьютора',
                    ],
                    'rowImport' => [$this, 'rowImport'],
                    'skipFirstLine' => true,
                ]
            ]
        ];
    }

    public function rowImport(ImportForm $form, array $row)
    {
        $rtt = $this->importDistrib($row);
    }


    private function importDistrib($row)
    {
        ini_set('memory_limit', '2048M');
        $egaisCode = trim($row[self::FIELD_EGAIS]);
        $inn = trim($row[self::FIELD_INN]);
        $distributor = Distributor::findOne(['egais' => $egaisCode, 'inn' => $inn]);
        if(strlen($egaisCode) != 12){
            throw new SkipRowException('PTT c кодом ЕГАИС '.$egaisCode.' пропущена, так как данный код содержит менее 12 символов');
        }else{
            $now = date("Y-m-d H:i:s");
            if(!$distributor){
                $newRtt = new Distributor();
            }else{
                $newRtt = Distributor::findOne(['egais' => $egaisCode, 'inn' => $inn]);
            }
                $newRtt->distrib_name = $row[self::FIELD_NAME];
                $newRtt->distrib_address = $row[self::FIELD_ADDRESS];
                $newRtt->egais = $egaisCode;
                $newRtt->created_at = $now;
                $newRtt->updated_at = $now;
                $newRtt->kpp = trim($row[self::FIELD_KPP]);
                $newRtt->inn = $inn;
                if(!$distributor){
                    $newRtt->save(false);
                }else{
                    $newRtt->update(false);
                }
            }
        return $newRtt;
    }
}