<?php

namespace modules\sales\backend\controllers;

use backend\base\Controller;
use modules\sales\common\models\Distributor;
use modules\sales\common\models\Managers;
use yz\admin\import\BatchImportAction;
use yz\admin\import\ImportForm;
use yz\admin\import\InterruptImportException;
use yz\admin\import\SkipRowException;

/**
 * Class ImportProfilesController
 */
class ImportManagerController extends Controller
{
    const FIELD_EGAIS = 'егаис';
    const FIELD_INN = 'инн';
    const FIELD_MANAGER_FIO = 'фио';
    const FIELD_MANAGER_MAIL = 'email';

    public function actions()
    {
        return [
            'index' => [
                'class' => BatchImportAction::className(),
                //'extraView' => '@modules/sales/backend/views/import-catalog/index.php',
                'importConfig' => [
                    'availableFields' => [
                        self::FIELD_EGAIS => 'Код ЕГАИС',
                        self::FIELD_INN => 'ИНН',
                        self::FIELD_MANAGER_FIO => 'ФИО менеджера ТО',
                        self::FIELD_MANAGER_MAIL => 'E-mail менеджера ТО',
                    ],
                    'rowImport' => [$this, 'rowImport'],
                    'skipFirstLine' => true,
                ]
            ]
        ];
    }

    public function rowImport(ImportForm $form, array $row)
    {
        $manager = $this->importManager($row);
    }


    private function importManager($row)
    {
        ini_set('memory_limit', '2048M');
        $egaisCode = trim($row[self::FIELD_EGAIS]);
        $inn = trim($row[self::FIELD_INN]);
        $mail = trim($row[self::FIELD_MANAGER_MAIL]);
        $distributor = Distributor::findOne(['egais' => $egaisCode, 'inn' => $inn]);
        $manager = Managers::findOne(['email' => $mail]);
        if(strlen($egaisCode) < 12 || strlen($egaisCode) > 12){
            throw new SkipRowException('Менеджер ТО c кодом ЕГАИС '.$egaisCode.' пропущен, так как его код содержит менее 12 символов');
        }else{
            $now = date("Y-m-d H:i:s");
            if(!$distributor){
                throw new SkipRowException('Дистрибьютор c кодом ЕГАИС '.$egaisCode.' и ИНН '.$inn.' не найден среди дистрибьюторов');
            }else{
                if(!$manager){
                    $newManager = new Managers();
                    $newManager->fio = $row[self::FIELD_MANAGER_FIO];
                    $newManager->email = $mail;
                    $newManager->save(false);
                    $distributor->manager_id = $newManager->id;
                    $distributor->update(false);
                }else{
                    $distributor->manager_id = $manager->id;
                    $distributor->update(false);
                }
            }
        }
    }
}