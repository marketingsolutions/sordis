<?php
namespace modules\sales\backend\forms;
/**
 * Created by PhpStorm.
 * User: MihailKri
 * Date: 17.10.2018
 * Time: 9:52
 */
use modules\bonuses\common\models\UploadBonusFile;
use modules\sales\common\models\UploadSalePointDistrib;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadRttDistribForm extends Model
{
    public $xmlFile;

    public function rules()
    {
        return [
            [['xmlFile'], 'file', 'extensions' => ['xml'], 'maxSize'=>1024 * 1024 * 100,
                'skipOnEmpty' => false]
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $model = new UploadSalePointDistrib();
            $model->original_file_name = $this->xmlFile->baseName . '.' . $this->xmlFile->extension;
            $model->status_process = UploadSalePointDistrib::STATUS_NEW;
            $model->created_at = date("Y-m-d H:i:s");
            $model->save(false);
            if (getenv('YII_ENV') == 'dev') {
                $path = \Yii::getAlias('@backend/web/upload_rtt_distrib/');
                if (\yii\helpers\FileHelper::createDirectory($path, $mode = 0775, $recursive = true)) {
                    $this->xmlFile->saveAs($path . $model->id . "upload." . $this->xmlFile->extension);
                }
            }else{
                $path = \Yii::getAlias('@data/upload_rtt_distrib/');
                if (\yii\helpers\FileHelper::createDirectory($path, $mode = 0775, $recursive = true)) {
                    $this->xmlFile->saveAs($path . $model->id . "upload." . $this->xmlFile->extension);
                }
            }
            $model->updateAttributes(['file_name'=>$model->id."upload.".$this->xmlFile->extension]);
            return true;
        } else {
            return false;
        }
    }
    public function attributeLabels()
    {
        return [
            'xmlFile' => 'Выберите XML файл',
        ];
    }

}
