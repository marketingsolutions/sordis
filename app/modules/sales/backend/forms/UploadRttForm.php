<?php
namespace modules\sales\backend\forms;
/**
 * Created by PhpStorm.
 * User: MihailKri
 * Date: 17.10.2018
 * Time: 9:52
 */
use modules\sales\common\models\SalePointUpload;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadRttForm extends Model
{
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'extensions' => ['xls', 'xlsx'], 'maxSize'=>1024 * 1024 * 100,
            'skipOnEmpty' => false]
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $model = new SalePointUpload();
            $model->original_file_name = $this->imageFile->baseName . '.' . $this->imageFile->extension;
            $model->status_process = SalePointUpload::STATUS_NEW;
            $model->created_at = date("Y-m-d H:i:s");
            $model->save(false);
            $this->imageFile->saveAs('upload_rtt/' . $model->id."upload.".$this->imageFile->extension);
            $model->updateAttributes(['file_name' => $model->id."upload.".$this->imageFile->extension]);
            return true;
        } else {
            return false;
        }
    }
    public function attributeLabels()
    {
        return [
            'imageFile' => 'Выберите CSV файл',
        ];
    }

}

