<?php

namespace modules\sales\backend;

use modules\sales\backend\rbac\Rbac;
use modules\sales\common\app\models\ApiSale;
use modules\sales\common\app\SaleApplicationModule;
use modules\sales\common\models\Sale;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yz\icons\Icons;


/**
 * Class Module
 */
class Module extends \modules\sales\common\Module
{
    public function getName()
    {
        return 'Бонусы за продажи';
    }

    public function getAdminMenu()
    {
        return [
            [
                'label' => 'РТТ и Дистрибьюторы',
                'icon' => Icons::o('archive'),
                'items' => [


                    [
                        'route' => ['/sales/sale-point/index'],
                        'label' => 'Список РТТ',
                        'icon' => Icons::o('bars'),
                    ],
                    [
                        'route' => ['/sales/distributor/index'],
                        'label' => 'Список Дистрибьюторов',
                        'icon' => Icons::o('bars'),
                    ],
                    [
                        'route' => ['/sales/managers/index'],
                        'label' => 'Список Менеджеров ТО',
                        'icon' => Icons::o('bars'),
                    ],
                    [
                        'route' => ['/sales/sale-point-all/index'],
                        'label' => 'Выгрузка точек с привязкой к участникам',
                        'icon' => Icons::o('bars'),
                    ],
                    [
                        'route' => ['/sales/distributor-profille-upload/index'],
                        'label' => 'Выгрузка дистрибьюторов с привязкой к участникам',
                        'icon' => Icons::o('bars'),
                    ],
                    [
                        'route' => ['/sales/sale-point-upload/index'],
                        'label' => 'Обновление данных по торговым точкам',
                        'icon' => Icons::o('check-square-o'),
                    ],
                    [
                        'route' => ['/sales/upload-sale-point-distrib/index'],
                        'label' => 'Загрузка РТТ с привязкой к дистрибьюторам из XML',
                        'icon' => Icons::o('check-square-o'),
                    ],
                    [
                        'route' => ['/sales/download-rtt/index'],
                        'label' => 'Выгрузка ТТ в XML',
                        'icon' => Icons::o('upload'),
                    ],
//                    [
//                        'route' => ['/sales/import-sales/index'],
//                        'label' => 'Импорт продаж',
//                        'icon' => Icons::o('upload'),
//                    ],
                ]
            ],
        ];
    }

    public function init()
    {
        \Yii::configure($this, [
            'modules' => [
                'sale-app' => [
                    'class' => SaleApplicationModule::class,
                    'allowCreation' => false,
                    'afterSaleProcess' => function(Sale $sale) {
                        return Url::to(['/sales/sales/view', 'id' => $sale->id]);
                    },
                    'findSale' => function($id) {
                        if (($sale = ApiSale::findOne($id)) === null) {
                            throw new NotFoundHttpException();
                        }
                        return $sale;
                    },
                    'useValidationRules' => false, // Admin can create any sale
                ]
            ]
        ]);

        parent::init();
    }

    public function getAuthItems()
    {
        return array_merge(parent::getAuthItems(), Rbac::dependencies());
    }

}