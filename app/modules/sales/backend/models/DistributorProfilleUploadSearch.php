<?php

namespace modules\sales\backend\models;

use common\utils\Notifier;
use modules\sales\common\models\Managers;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\sales\common\models\Distributor;

/**
 * DistributorProfilleUploadSearch represents the model behind the search form about `modules\sales\common\models\Distributor`.
 */
class DistributorProfilleUploadSearch extends Distributor implements SearchModelInterface

{

    public $profilePhone;

    public $profileFullName;

    public $profileEmail;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['distrib_name', 'distrib_address', 'egais', 'inn', 'license_number', 'kpp', 'created_at', 'updated_at', 'profilePhone', 'profileFullName', 'profileEmail'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        $query =  Distributor::find()
            ->joinWith('distributorProfiles')
            ->joinWith('profile');
        if(Notifier::checkRole($admin=null,'MANAGER_TO')){
            $query->where(['{{%profiles}}.id' => Managers::getManagerToProfileId()]);
        }
        return $query;
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'profilePhone' => [
                'asc' => ['{{%profiles}}.phone_mobile' => SORT_ASC],
                'desc' => ['{{%profiles}}.phone_mobile' => SORT_DESC]
            ],
            'profileFullName' => [
                'asc' => ['{{%profiles}}.full_name' => SORT_ASC],
                'desc' => ['{{%profiles}}.full_name' => SORT_DESC]
            ],
            'profileEmail' => [
                'asc' => ['{{%profiles}}.email' => SORT_ASC],
                'desc' => ['{{%profiles}}.email' => SORT_DESC]
            ],
        ]);
        $dataProvider->setSort($sort);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', '{{%distributor}}.distrib_name', $this->distrib_name])
            ->andFilterWhere(['like', '{{%distributor}}.distrib_address', $this->distrib_address])
            ->andFilterWhere(['like', '{{%distributor}}.egais', $this->egais])
            ->andFilterWhere(['like', '{{%distributor}}.inn', $this->inn])
            ->andFilterWhere(['like', 'license_number', $this->license_number])
            ->andFilterWhere(['like', '{{%profiles}}.phone_mobile', $this->profilePhone])
            ->andFilterWhere(['like', '{{%profiles}}.full_name', $this->profileFullName])
            ->andFilterWhere(['like', '{{%profiles}}.email', $this->profileEmail])
            ->andFilterWhere(['like', 'kpp', $this->kpp]);
         }
}
