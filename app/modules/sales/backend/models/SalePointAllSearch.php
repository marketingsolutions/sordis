<?php

namespace modules\sales\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\sales\common\models\SalePoint;

/**
 * SalePointAllSearch represents the model behind the search form about `modules\sales\common\models\SalePoint`.
 */
class SalePointAllSearch extends SalePoint implements SearchModelInterface

{
    /**
     * @var
     */
    public $profileName;
    /**
     * @var
     */
    public $profilePhone;
    /**
     * @var
     */
    public $profileEmail;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'address', 'license_number', 'code_egais', 'created_at', 'updated_at', 'inn', 'work_time_from', 'work_time_to', 'kpp', 'profileName', 'profilePhone', 'profileEmail'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        return SalePoint::find();
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery()
        ->joinWith('salePointProfiles')
        ->joinWith('profiles');
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'profilePhone' => [
                'asc' => ['{{%profiles}}.phone_mobile' => SORT_ASC],
                'desc' => ['{{%profiles}}.phone_mobile' => SORT_DESC]
            ],
            'profileName' => [
                'asc' => ['{{%profiles}}.full_name' => SORT_ASC],
                'desc' => ['{{%profiles}}.full_name' => SORT_DESC]
            ],
            'profileEmail' => [
                'asc' => ['{{%profiles}}.email' => SORT_ASC],
                'desc' => ['{{%profiles}}.email' => SORT_DESC]
            ],
        ]);
        $dataProvider->setSort($sort);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'id' => $this->id,
            'work_time_from' => $this->work_time_from,
            'work_time_to' => $this->work_time_to,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'license_number', $this->license_number])
            ->andFilterWhere(['like', 'code_egais', $this->code_egais])
            ->andFilterWhere(['like', '{{%sale_point}}.inn', $this->inn])
            ->andFilterWhere(['like', '{{%profiles}}.full_name', $this->profileName])
            ->andFilterWhere(['like', '{{%profiles}}.phone_mobile', $this->profilePhone])
            ->andFilterWhere(['like', '{{%profiles}}.email', $this->profileEmail])
            ->andFilterWhere(['like', 'kpp', $this->kpp]);

    }
}
