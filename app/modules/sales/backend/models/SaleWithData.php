<?php

namespace modules\sales\backend\models;
use common\utils\Notifier;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\Region;
use modules\sales\common\models\Sale;
use modules\sales\common\models\SalePosition;
use yii\db\ActiveQuery;
use yz\admin\models\User;
use yz\admin\search\WithExtraColumns;


/**
 * Class SaleWithData
 */
class SaleWithData extends Sale
{
    use WithExtraColumns;

    public $total_positions;

    protected static function extraColumns()
    {
        return [
            'profile__full_name',
            'profile__phone_mobile',
            'dealer__name',
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), static::extraColumns());
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'profile__full_name' => 'Участник',
            'profile__phone_mobile' => 'Телефон участника',
            'dealer__name' => 'Название магазина (ТТ)',
            'total_positions' => 'Кол-во позиций',
        ]);
    }


    /**
     * @return ActiveQuery
     */
    public static function find()
    {
        $query = parent::find()
            ->select(static::selectWithExtraColumns([
                'sale.*',
                'total_positions' => 'COUNT(position.id)'
            ]))
            ->from(['sale' => self::tableName()])
            ->joinWith('profile profile')
            ->joinWith('profile.dealer dealer')
            ->leftJoin(['position' => SalePosition::tableName()], 'position.sale_id = sale.id')
            ->leftJoin(['region' => Region::tableName()], 'region.id = dealer.region_id')
            ->groupBy(['sale.id']);

        /** @var User $admin */
        $admin = \Yii::$app->user->identity;
        if (Notifier::isRegional()) {
            $regionIds = Region::find()
                ->select('id')
                ->indexBy('id')
                ->where(['admin_user_id' => $admin->id])
                ->column();
            if (!empty($regionIds)) {
                $query->andWhere(['region.id' => $regionIds]);
            }
        }

        return $query;
    }

    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['id' => 'recipient_id']);
    }
}