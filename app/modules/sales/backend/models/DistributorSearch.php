<?php

namespace modules\sales\backend\models;

use modules\sales\common\models\Managers;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\sales\common\models\Distributor;
use common\utils\Notifier;

/**
 * DistributorSearch represents the model behind the search form about `modules\sales\common\models\Distributor`.
 */
class DistributorSearch extends Distributor implements SearchModelInterface

{

    /**
     * @var
     */
    public $managerEmail;

    /**
     * @var
     */
    public $managerFullName;

    /**
     * @var
     */
    public $isProfile;

    /**
     * @var
     */
    public $profileCreate;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['distrib_name', 'distrib_address', 'egais', 'inn', 'license_number', 'kpp', 'managerEmail', 'managerFullName', 'isProfile', 'profileCreate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        $query =  Distributor::find()
            ->joinWith('manager')
            ->joinWith('profiles');
        if(Notifier::checkRole($admin=null,'MANAGER_TO')){
            $query->where(['manager_id' => Managers::getManagerTo()]);
        }
        if(\Yii::$app->request->get('DistributorSearch')['isProfile']== 1){
            $query->where('{{%profiles}}.id is not null');
        }elseif(\Yii::$app->request->get('DistributorSearch')['isProfile']== 0 && \Yii::$app->request->get('DistributorSearch')['isProfile']!=""){
            $query->where('{{%profiles}}.id is null');
        }
        return $query;
    }



    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'managerEmail' => [
                'asc' => ['{{%managers}}.email' => SORT_ASC],
                'desc' => ['{{%managers}}.email' => SORT_DESC]
            ],
            'managerFullName' => [
                'asc' => ['{{%managers}}.fio' => SORT_ASC],
                'desc' => ['{{%managers}}.fio' => SORT_DESC]
            ],
        ]);
        $dataProvider->setSort($sort);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', '{{%distributor}}.distrib_name', $this->distrib_name])
            ->andFilterWhere(['like', '{{%distributor}}.distrib_address', $this->distrib_address])
            ->andFilterWhere(['like', '{{%distributor}}.egais', $this->egais])
            ->andFilterWhere(['like', '{{%distributor}}.inn', $this->inn])
            ->andFilterWhere(['like', '{{%distributor}}.kpp', $this->kpp])
            ->andFilterWhere(['like', '{{%managers}}.fio', $this->managerFullName])
            ->andFilterWhere(['like', '{{%managers}}.email', $this->managerEmail])
            ->andFilterWhere(['like', 'license_number', $this->license_number]);
        if($this->profileCreate && $this->profileCreate!=""){
            $query->andFilterWhere(['>=', '{{%profiles}}.created_at', date("Y-m-d 00:00:00", strtotime($this->profileCreate))])
                ->andFilterWhere(['<=', '{{%profiles}}.created_at', date("Y-m-d 23:59:59", strtotime($this->profileCreate))]);
        }

    }
}
