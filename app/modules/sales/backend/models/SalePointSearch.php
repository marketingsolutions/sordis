<?php

namespace modules\sales\backend\models;

use modules\sales\common\models\Distributor;
use modules\sales\common\models\SalePointDistrib;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\sales\common\models\SalePoint;
use common\utils\Notifier;

/**
 * SalePointSearch represents the model behind the search form about `modules\sales\common\models\SalePoint`.
 */
class SalePointSearch extends SalePoint implements SearchModelInterface

{

    public $isParticipant;

    public $profileDateAdd;

    public $distribs;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'address', 'license_number', 'code_egais', 'created_at', 'registred_at','updated_at','work_time_from', 'work_time_to', 'kpp', 'inn', 'isParticipant', 'profileDateAdd', 'distribs'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        $query =  SalePoint::find()
            ->joinWith('salePointProfiles')
            ->joinWith('profiles');
        if(\Yii::$app->request->get('SalePointSearch')['distribs'] != ""){
            $query->andWhere(['{{%sale_point}}.id' => $this->distribFilterId(\Yii::$app->request->get('SalePointSearch')['distribs'])]);
        }
        if(Notifier::checkRole($admin=null,'MANAGER_TO')){
            $query->where(['{{%sale_point}}.id' => SalePoint::getSalePointIdForManagerTo()]);
        }

//        if(\Yii::$app->request->get('SalePointSearch')['profileDateAdd'] && \Yii::$app->request->get('SalePointSearch')['profileDateAdd'] != ""){
//
//            $query->andWhere(['>=', '{{%profiles}}.created_at', date('Y-m-d 00:00:00', strtotime(\Yii::$app->request->get('SalePointSearch')['profileDateAdd']))]);
//            $query->andWhere(['<=', '{{%profiles}}.created_at', date('Y-m-d 23:59:59', strtotime(\Yii::$app->request->get('SalePointSearch')['profileDateAdd']))]);
//        }

        if(\Yii::$app->request->get('SalePointSearch')['registred_at'] && \Yii::$app->request->get('SalePointSearch')['registred_at'] != ""){
            $query->andWhere(['>=', '{{%sale_point}}.registred_at', date('Y-m-d 00:00:00', strtotime(\Yii::$app->request->get('SalePointSearch')['registred_at']))]);
            $query->andWhere(['<=', '{{%sale_point}}.registred_at', date('Y-m-d 23:59:59', strtotime(\Yii::$app->request->get('SalePointSearch')['registred_at']))]);
        }

        if(\Yii::$app->request->get('SalePointSearch')['isParticipant']==''){
            return $query;
        }

        if(\Yii::$app->request->get('SalePointSearch')['isParticipant'] == 1 ){
            $query->andWhere(['{{%sale_point}}.id' => $this->getCurrentSalePoints($notCurrent=true)]);
        }elseif(\Yii::$app->request->get('SalePointSearch')['isParticipant'] == 0){
            $query->andWhere(['{{%sale_point}}.id' => $this->getCurrentSalePoints($notCurrent=false)]);
        }
        return $query;
    }

    /**
     * Работа фильтра
     * @param $distrName
     * @return array
     */
    public function distribFilterId($distrName)
    {
        $distrArrId = Distributor::find()
            ->select('id')
            ->where('distrib_name LIKE "%'.$distrName.'%"')
            ->column();
        return SalePointDistrib::find()
            ->select('sale_point_id')
            ->where(['distributor_id' => $distrArrId])
            ->column();
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();

        return $query;
    }

    public function getCurrentSalePoints($notCurrent=true)
    {
        if($notCurrent){
            return SalePoint::find()
                ->select('{{%sale_point}}.id')
                ->leftJoin('{{%sale_point_profile}}', '{{%sale_point_profile}}.sale_point_id={{%sale_point}}.id')
                ->where('{{%sale_point_profile}}.sale_point_id is NOT NULL')
                ->distinct()
                ->column();
        }else{
            return SalePoint::find()
                ->select('{{%sale_point}}.id')
                ->leftJoin('{{%sale_point_profile}}', '{{%sale_point_profile}}.sale_point_id={{%sale_point}}.id')
                ->where('{{%sale_point_profile}}.sale_point_id is NULL')
                ->distinct()
                ->column();
        }
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'id' => $this->id,
            //'isParticipant' => $this->isParticipant,
        ]);
//        var_dump($this->profileDateAdd);
//        die;
        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'license_number', $this->license_number])
            ->andFilterWhere(['like', 'kpp', $this->kpp])
            ->andFilterWhere(['like', '{{%sale_point}}.inn', $this->inn])
            ->andFilterWhere(['like', 'code_egais', $this->code_egais]);
//        if($this->profileDateAdd) {
//            $query->andFilterWhere(['>=', '{{%profiles}}.created_at', date("Y-m-d 00:00:00", strtotime($this->profileDateAdd))]);
//            $query->andFilterWhere(['<=', '{{%profiles}}.created_at', date("Y-m-d 23:59:59", strtotime($this->profileDateAdd))]);
//        }

    }
}
