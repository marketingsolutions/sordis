<?php

namespace modules\sales\backend\models;

use modules\sales\common\models\SalePosition;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\sales\common\models\SerialNumber;

/**
 * SerialNumberSearch represents the model behind the search form about `\modules\sales\common\models\SerialNumber`.
 */
class SerialNumberSearch extends SerialNumber implements SearchModelInterface
{
    /**
     * @var int
     */
    public $is_activated;
    public $sale_id;
    public $full_name;
    public $name;
    public $status;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id',], 'integer'],
            [['number', 'activated_at', 'created_at' , 'sale_id' , 'full_name' , 'name', 'status'], 'safe'],
            [['is_activated'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        /*Попытка передаем для фильтра*/
        //$query = SalePosition::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [

                'attributes'=>[
                   // 'name',
                    //'position',
                    'id',
                    'product_id',
                    'number',
                    'activated_at',
                    'status',
                    'created_at',
                    'sale_id'=>[
                        'asc' => [
                            'yz_sales_positions.sale_id'=>SORT_ASC
                        ],
                        'desc' => [
                            'yz_sales_positions.sale_id'=>SORT_DESC
                        ],
                        //'label' => 'Аккаунт',
                        'default' => SORT_ASC

                    ],
                    'full_name'=>[
                        'asc' => [
                            'yz_profiles.full_name'=>SORT_ASC
                        ],
                        'desc' => [
                            'yz_profiles.full_name'=>SORT_DESC
                        ],
                        //'label' => 'Аккаунт',
                        'default' => SORT_ASC
                    ],
                    'name' => [
                        'asc' => [
                            'yz_dealers.name'=>SORT_ASC
                        ],
                        'desc' => [
                            'yz_dealers.name'=>SORT_DESC
                        ],
                        //'label' => 'Аккаунт',
                        'default' => SORT_ASC
                    ],

                ]

            ]

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,

        ]));
       // $this->addCondition($query, 'saleId');

        /* Настроим правила фильтрации */

        // фильтр по имени
       // $query->andWhere('sale_id LIKE "%' . $this->saleId .'%"' );


        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {

        return SerialNumber::find()
            ->joinWith(['salePosition'])
            ->joinWith(['sale'])
            ->joinWith(['profile'])
            ->joinWith(['dealer']);

    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);



        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'id' => $this->id,
            'product_id' => $this->product_id,
            '(NOT (activated_at IS NULL) OR is_available = 0)' => $this->is_activated,
        ]);

        if ($this->sale_id) {
            $query->joinWith(['salePosition' => function ($q) {
                $q->andWhere('yz_sales_positions.sale_id =' . $this->sale_id);
            }]);
        }
        if($this->full_name) {
            $query->joinWith(['profile' => function ($q) {
                $q->andWhere('yz_profiles.full_name LIKE "%'.$this->full_name.'%"');
            }]);
        }
        if($this->name) {
            $query->joinWith(['dealer' => function ($q) {
                $q->andWhere('yz_dealers.name LIKE "%'.$this->name.'%"');
            }]);
        }
         if($this->status) {
             $query->andWhere('yz_serial_numbers.status = '.$this->status);
         }

        $query->andFilterWhere(['like', 'number', $this->number]);
        //$query->andFilterWhere(['like', 'number', $this->number]);
    }

    protected function addCondition($query, $attribute, $partialMatch = false)
    {
        if (($pos = strrpos($attribute, '.')) !== false) {
            $modelAttribute = substr($attribute, $pos + 1);
        } else {
            $modelAttribute = $attribute;
        }

        $value = $this->$modelAttribute;
        if (trim($value) === '') {
            return;
        }
        if ($partialMatch) {
            $query->andWhere(['like', $attribute, $value]);
        } else {
            $query->andWhere([$attribute => $value]);
        }
    }
}
