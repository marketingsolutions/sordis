<?php

namespace modules\sales\frontend\controllers;

use modules\sales\common\actions\DownloadDocumentAction;
use modules\sales\common\models\Sale;
use modules\sales\common\models\SaleDocument;
use modules\sales\frontend\base\Controller;
use modules\sales\frontend\models\SalePointDistribSearch;
use modules\sales\frontend\models\SaleSearch;
use modules\sales\SalesModuleTrait;
use ms\loyalty\contracts\prizes\PrizeRecipientInterface;
use Yii;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yz\admin\actions\ExportAction;

/**
 * SalesController implements the CRUD actions for Sale model.
 */
class SalesController extends Controller
{
    use SalesModuleTrait;

    /**
     * @var PrizeRecipientInterface
     */
    private $recipient;

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {

        return [
            'download-document' => [
                'class' => DownloadDocumentAction::className(),
                'checkAccess' => function (SaleDocument $document) {
                    if ($document->sale_id === null) {
                        return true;
                    }

                    return $document->sale->recipient_id === $this->recipient->recipientId;
                }
            ],
            'download-small-document' => [
                'class' => DownloadDocumentAction::className(),
                'format' => DownloadDocumentAction::TYPE_SMALL,
                'checkAccess' => function (SaleDocument $document) {
                    if ($document->sale_id === null) {
                        return true;
                    }

                    return $document->sale->recipient_id === $this->recipient->recipientId;
                }
            ],
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function($params) {
                    /** @var SalePointDistribSearch $searchModel */
                    return Yii::createObject(SalePointDistribSearch::className());
                },
                'dataProvider' => function($params, SalePointDistribSearch $searchModel) {
                    $dataProvider = $searchModel->search($params);
                    return $dataProvider;
                },
            ]
        ];
    }


    public function __construct($id, $module, PrizeRecipientInterface $recipient, $config = [])
    {
        $this->recipient = $recipient;
        parent::__construct($id, $module, $config);
    }


    /**
     * Lists all Sale models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SaleSearch($this->recipient);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $profileId = \modules\profiles\common\models\Profile::findOne(['identity_id'=>Yii::$app->user->identity->id])->id;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /*----------------------------------*/
    public function getGridColumns(SalePointDistribSearch $searchModel)
    {
        return  [
            [
                'attribute' => 'salePointName',
                'label' => 'Название торговой точки',
            ],
            [
                'attribute' => 'salePointEgais',
                'label' => 'Код ЕГАИС',
            ],
            [
                'attribute' => 'salePointInn',
                'label' => 'ИНН',
            ],
            [
                'attribute' => 'isCoin',
                'label' => 'Участник мотивационной программы «Копилка»',
                'value' => function($model){
                    if($model->isCoin == 0){
                        return 'нет';
                    }else{
                        return 'да';
                    }
                },
                'filter' => [
                    0 => 'нет',
                    1 => 'да',
                ],
            ],
            [
                'attribute' => 'profileRttDate',
                'format' => ['date', 'php:d.m.Y H:i:s'],
                'label' => 'Дата регистрации в МП «Копилка»',
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'дд.мм.гггг'
                ],
            ],
            [
                'attribute' => 'salePointAddress',
                'label' => 'Адрес торговой точки',
            ],
        ];

    }
/*---------------------------------*/
    public function actionMyRtt(){
        //print_r(Yii::$app->request->queryParams);
       // exit;
        $searchModel = new SalePointDistribSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('myrtt', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function actionApp()
    {

        $id = \Yii::$app->request->get('id');

        if ($id !== null) {
            $this->findModel($id);
        } else {
            if (self::getSalesModule()->allowNewSalesCreation == false) {
                throw new ForbiddenHttpException();
            }
        }

		//return $this->render('closed');  для блокировки регистрации продаж

        return $this->render('app', compact('id'));
    }

    /**
     * Displays a single Sale model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', compact('model'));
    }



    /**
     * Deletes an existing Sale model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->statusManager->canBeDeleted()) {
            $model->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sale model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sale the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sale::findOne(['id' => $id, 'recipient_id' => $this->recipient->getRecipientId()])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
