<?php

namespace modules\sales\frontend\controllers\api;

use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use ms\loyalty\api\frontend\base\ApiController;
use modules\sales\frontend\models\ApiSale;
use modules\sales\common\app\forms\SaleEditForm;
use modules\sales\frontend\models\ApiSaleDocument;
use modules\profiles\frontend\models\ApiProfile;

/**
 * Class SaleController
 */
class SaleController extends ApiController
{

    public function actionList()
    {
        $profile_id = Yii::$app->request->post('profile_id', null);

        $profile = ApiProfile::findOne($profile_id);

        if (null === $profile) {
            return $this->error("Не найден участник с ID $profile_id");
        }

        $sales = ApiSale::find()
            ->where(['recipient_id' => $profile->id])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();

        return $this->ok(['sales' => $sales], "Получение списка продаж участника $profile->full_name");
    }

    public function actionView()
    {
        $profile_id = Yii::$app->request->post('profile_id', null);
        $sale_id = Yii::$app->request->post('sale_id', null);

        $profile = ApiProfile::findOne($profile_id);

        if (null === $profile) {
            return $this->error("Не найден участник с ID $profile_id");
        }

        $sale = ApiSale::find()->where([
            'id' => $sale_id,
            'recipient_id' => $profile_id
        ])->one();

        if (null === $sale) {
            return $this->error("У участника с ID $profile_id не найдена продажа с ID $sale_id");
        }

        return $this->ok(['sale' => $sale], "Получение конкретной продаж участника $profile->full_name");
    }

    public function actionCreate()
    {
        $post = Yii::$app->request->post();
        $profile_id = ArrayHelper::getValue($post, 'profile_id');
        $documents = ArrayHelper::getValue($post, 'documents');

        $profile = ApiProfile::findOne(['id' => $profile_id]);

        if (null == $profile) {
            return $this->error("Не найден участник с ID $profile_id");
        }

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $form = new SaleEditForm(new \modules\sales\common\app\models\ApiSale, $profile); // ['useValidationRules' => true]

            if ($form->loadApi(['model' => $post]) && $form->process()) {
                $sale = ApiSale::findOne($form->sale->id);
                $sale->saveDocuments($documents);

                $transaction->commit();
                return $this->ok(['sale' => $sale], 'Успешное добавление продажи');

            } else {
                $transaction->rollBack();
                return $this->error($form->getFirstErrors(), 'Не удалось добавить новую продажу');
            }

        } catch(Exception $e) {
            $transaction->rollBack();
            return $this->error($e->getMessage(), 'Не удалось добавить новую продажу');
        }
    }

    public function actionUpdate()
    {
        $post = Yii::$app->request->post();
        $sale_id = ArrayHelper::getValue($post, 'sale_id');
        $documents = ArrayHelper::getValue($post, 'documents');

        $sale = ApiSale::findOne($sale_id);

        if (null === $sale) {
            return $this->error("Не найдена продажа с ID $sale_id");
        }

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $form = new SaleEditForm($sale, null); // ['useValidationRules' => true]

            if ($form->loadApi(['model' => $post]) && $form->process()) {
                if (!empty($documents)) {
                    ApiSaleDocument::deleteAll(['sale_id' => $sale_id]);
                    $sale->saveDocuments($documents);
                }

                $transaction->commit();
                return $this->ok(['sale' => $sale], 'Успешное обновление продажи');

            } else {
                $transaction->rollBack();
                return $this->error($form->getFirstErrors(), 'Не удалось обновить продажу');
            }

        } catch(Exception $e) {
            $transaction->rollBack();
            return $this->error($e->getMessage(), 'Не удалось обновить продажу');
        }
    }

}