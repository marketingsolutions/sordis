<?php

namespace modules\sales\frontend\controllers\api;

use Yii;
use ms\loyalty\api\frontend\base\ApiController;
use modules\sales\frontend\models\ApiSaleProduct;

/**
 * Class ProductController
 */
class ProductController extends ApiController
{

    public function actionList()
    {
        $category_id = Yii::$app->request->post('category_id', null);
        $dealer_name = Yii::$app->request->post('dealer_name', null);

        $query = ApiSaleProduct::find();

        if ($category_id) {
            $query->andWhere(['category_id' => $category_id]);
        }

        if ($dealer_name) {
            $query->andWhere(['dealer_name' => $dealer_name]);
        }

        $products = $query->all();

        return $this->ok(['products' => $products], 'Получение списка продуктов');
    }

    public function actionView()
    {
        $product_id = Yii::$app->request->post('product_id', null);

        $product = ApiSaleProduct::findOne($product_id);

        if (null === $product) {
            return $this->error("Не найден товар с ID $product_id");
        }

        return $this->ok(['product' => $product], 'Получение конкретного товара');
    }



}