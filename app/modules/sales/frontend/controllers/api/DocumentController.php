<?php

namespace modules\sales\frontend\controllers\api;

use Yii;
use ms\loyalty\api\frontend\base\ApiController;
use modules\sales\frontend\models\ApiSaleDocument;
use modules\sales\frontend\models\ApiSale;

/**
 * Class DocumentController
 */
class DocumentController extends ApiController
{

    public function actionUpload()
    {
        $data = ['ApiSaleDocument' => Yii::$app->request->post()];

        $model = new ApiSaleDocument;
        $model->scenario = ApiSaleDocument::SCENARIO_API_UPLOAD;

        if ($model->load($data) && $model->validate() && $model->upload()) {
            return $this->ok([
                'filename' => $model->filename,
                'webpath' => $model->webpath,
            ], 'Успешная загрузка изображения');
        }

        return $this->error($model->getFirstErrors(), 'Не удалось сохранить изображение');
    }

    public function actionDelete()
    {
        $document_id = Yii::$app->request->post('document_id', null);

        $document = ApiSaleDocument::findOne($document_id);

        if (null === $document) {
            return $this->error("Не найден подтверждающий документ с ID $document_id");
        }

        $rowCount = ApiSaleDocument::deleteAll(['id' => $document_id]);

        return $this->ok(['rowCount' => $rowCount], "Удаление подтверждающего документа");
    }

}