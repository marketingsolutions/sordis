<?php

namespace modules\sales\frontend\controllers\api;

use Yii;
use ms\loyalty\api\frontend\base\ApiController;
use modules\sales\frontend\models\ApiSaleCategory;

/**
 * Class CategoryController
 */
class CategoryController extends ApiController
{

    public function actionList()
    {
        $categories = ApiSaleCategory::find()->all();

        return $this->ok(['categories' => $categories], 'Получение списка категорий');
    }

    public function actionView()
    {
        $category_id = Yii::$app->request->post('category_id', null);

        $category = ApiSaleCategory::findOne($category_id);

        if (null === $category) {
            return $this->error("Не найдена категория с ID $category_id");
        }

        return $this->ok(['category' => $category], 'Получение конкретной категории');
    }

}