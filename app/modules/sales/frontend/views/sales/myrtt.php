<?php

use modules\sales\common\models\Sale;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yz\icons\Icons;
use yz\admin\widgets\ActionButtons;


/* @var $this yii\web\View */
/* @var $searchModel modules\sales\frontend\models\SaleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мои торговые точки';
$this->params['header'] = 'Мои торговые точки';
$this->params['breadcrumbs'][] = $this->title;

\yz\icons\FontAwesomeAsset::register($this);

$js=<<<JS
$( document ).ready(function() {
    var strGET = window.location.search.replace( '?', ''); 
    $('#exportUrl').attr('href', '/sales/sales/export?'+strGET);
});

JS;

$this->registerJs($js);

?>
<div class="row">
    <div class="col-md-10"></div>
    <div class="col-md-2 text-right"><a id="exportUrl" href="">Сохранить в XLS</a>&nbsp;&nbsp;&nbsp;&nbsp;</div>
</div>
<div class="row container">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?= GridView::widget([
                    'id' => 'sales-grid',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                    'columns' => array_merge([
                    ], $columns, [
                        [
                            'class' => 'yz\admin\widgets\ActionColumn',
                            'template' => '{update}',
                        ],
                    ]),
                ]); ?>


            </div>
        </div>
    </div>
</div>