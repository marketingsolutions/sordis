<?php

namespace modules\sales\frontend\models;

use modules\sales\common\models\Product;
use modules\sales\frontend\models\ApiSaleCategory;
use modules\sales\frontend\models\ApiSaleUnit;

class ApiSaleProduct extends Product
{
    public function fields()
    {
        return parent::fields() + [
                'category',
                'unit',
            ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ApiSaleCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(ApiSaleUnit::className(), ['id' => 'unit_id']);
    }
}