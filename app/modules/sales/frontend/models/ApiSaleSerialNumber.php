<?php

namespace modules\sales\frontend\models;

use modules\sales\common\models\SerialNumber;

class ApiSaleSerialNumber extends SerialNumber
{
    public function fields() {
        return parent::fields();
    }
}