<?php

namespace modules\sales\frontend\models;

use Yii;
use modules\sales\common\models\SalePosition;
use modules\sales\frontend\models\ApiSaleSerialNumber;
use modules\sales\frontend\models\ApiSaleProduct;

class ApiSalePosition extends SalePosition
{
    public function fields()
    {
        $fields = [
            'id',
            'product_id',
            'quantity',
            'quantityLocal',
            'bonuses',
            'serial_number_id',
            'validation_method',
            'custom_serial',
            'product',
            'serialNumber',
        ];

        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(ApiSaleProduct::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSerialNumber()
    {
        return $this->hasOne(ApiSaleSerialNumber::class, ['id' => 'serial_number_id']);
    }

}