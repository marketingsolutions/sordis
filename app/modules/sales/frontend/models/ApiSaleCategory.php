<?php

namespace modules\sales\frontend\models;

use modules\sales\common\models\Category;

class ApiSaleCategory extends Category
{
    public function fields() {
        return array_merge(parent::fields(), [
            'created_at' => function(ApiSaleCategory $model) {
                /* @var $model ApiSaleCategory */
                return $model->created_at ? (new \DateTime($model->created_at))->format('d.m.Y') : null;
            },
            'updated_at' => function(ApiSaleCategory $model) {
                /* @var $model ApiSaleCategory */
                return $model->updated_at ? (new \DateTime($model->updated_at))->format('d.m.Y') : null;
            },
        ]);
    }
}