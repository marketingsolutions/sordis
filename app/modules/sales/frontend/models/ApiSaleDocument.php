<?php

namespace modules\sales\frontend\models;

use Yii;
use yii\helpers\FileHelper;
use yii\base\Exception;
use marketingsolutions\thumbnails\Thumbnail;
use modules\sales\common\models\SaleDocument;

/**
 * @property string $type
 * @property string $image
 * @property string $filename
 * @property string $webpath
 */
class ApiSaleDocument extends SaleDocument
{

    const SCENARIO_API_UPLOAD = 'api_upload';

    public $type;
    public $image;
    public $filename;

    public function rules()
    {
        return [
            ['type', 'required', 'on' => self::SCENARIO_API_UPLOAD],
            ['image', 'required', 'on' => self::SCENARIO_API_UPLOAD],
            [['filename'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'type' => 'Расширение картинки',
            'image' => 'Изображение в Base64'
        ];
    }

    public function fields() {
        return [
            'id',
            'original_name',
            'name',
            'file_size',
            'image_url' => function(ApiSaleDocument $model) {
                /* @var $model ApiSaleDocument */

//                $thumbnail = (new Thumbnail())->url($fileName, Thumbnail::thumb(1000, 1000), '1000x1000');
                return Yii::getAlias('@frontendWeb/data/sales/documents/').$model->name;
            },
        ];
    }

    public function getWebpath()
    {
        return  Yii::getAlias('@frontendWeb/data/temp/' . $this->filename);
    }

    public function upload()
    {
        $dir = Yii::getAlias('@data/temp');
        FileHelper::createDirectory($dir);

        $name = uniqid() . '.' . $this->type;
        $path = Yii::getAlias($dir.'/'.$name);
        $size = (int)file_put_contents($path, base64_decode($this->image));

        if (!$size) {
            $this->addError('image', 'Ошибка при загрузке файла, неверное значение size');

            return false;
        }

        $this->filename = $name;

        return true;
    }

    public function moveTempFile($filename)
    {
        $tmpDir = Yii::getAlias('@data/temp');
        $dir = Yii::getAlias('@data/sales/documents');
        FileHelper::createDirectory($dir);

        $tmpFilepath =  $tmpDir . '/' . $filename;

        if(!file_exists($tmpFilepath)) {
            throw new Exception("Не найден временный файл $filename");
        }

        $pathinfo = pathinfo($tmpFilepath);
        $newFilename = $this->sale_id . '_' . $pathinfo['filename'] . '.' . $pathinfo['extension'];
        $newFilepath = $dir . '/' . $newFilename;

        if (!copy($tmpFilepath, $newFilepath)) {
            throw new Exception("Не удалось скопировать временный файл $filename");
        }

        $this->original_name = $filename;
        $this->name = $newFilename;
        $this->file_size = filesize($newFilepath);
    }

}