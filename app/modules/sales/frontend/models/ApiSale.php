<?php

namespace modules\sales\frontend\models;

use yii\base\Exception;
use modules\sales\frontend\models\ApiSalePosition;
use modules\sales\frontend\models\ApiSaleDocument;

class ApiSale extends \modules\sales\common\app\models\ApiSale
{
    public function fields()
    {
        $fields = [
            'id',
            'recipient_id',
            'status',
            'bonuses',
            'review_comment',
            'sold_on' => function(ApiSale $model) {
                return $model->sold_on ? (new \DateTime($model->sold_on))->format('d.m.Y') : null;
            },
            'approved_by_admin_at' => function(ApiSale $model) {
                return $model->approved_by_admin_at ? (new \DateTime($model->approved_by_admin_at))->format('d.m.Y') : null;
            },
            'bonuses_paid_at' => function(ApiSale $model) {
                return $model->bonuses_paid_at ? (new \DateTime($model->bonuses_paid_at))->format('d.m.Y') : null;
            },
            'created_at' => function(ApiSale $model) {
                return $model->created_at ? (new \DateTime($model->created_at))->format('d.m.Y') : null;
            },
            'positions',
            'documents',
        ];

        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasMany(ApiSalePosition::className(), ['sale_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(ApiSaleDocument::className(), ['sale_id' => 'id']);
    }

    public function saveDocuments($documents = null)
    {
        if (!is_array($documents) || empty($documents)) {
            throw new Exception('Необходимо прикрепить подтверждающий документ');
        }

        foreach ($documents as $filename) {
            $document = new ApiSaleDocument;
            $document->sale_id = $this->id;
            $document->moveTempFile($filename);

            if (!$document->save()) {
                throw new Exception($document->getFirstErrors());
            }
        }
    }

}