<?php

namespace modules\sales\frontend\models;

use modules\profiles\common\models\Profile;
use ms\loyalty\identity\phonesEmails\common\models\Identity;
use yii\base\Model;

class ChangePasswordForm extends Model
{
    /** @var string */
    public $password;

    /** @var string */
    public $passwordCompare;

    /** @var Profile */
    public $profile;

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['password', 'string'],
            ['password', 'required'],
            ['passwordCompare', 'string'],
            ['passwordCompare', 'required'],
            ['passwordCompare', 'compare', 'compareAttribute' => 'password'],
        ]);
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'passwordCompare' => 'Подтверждение пароля',
        ];
    }

    public function changePassword()
    {
        /** @var Identity $identity */
        $identity = $this->profile->identity;

        if ($this->validate() == false) {
            return false;
        }

        if ($identity == null) {
            $this->addError('password', 'Не найдена идентификация участника');
            return false;
        }

        $identity->updateAttributes([
            'passhash' => Identity::hashPassword($this->password)
        ]);

        return true;
    }
}
