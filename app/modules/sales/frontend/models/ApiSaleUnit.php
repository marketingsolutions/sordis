<?php

namespace modules\sales\frontend\models;

use modules\sales\common\models\Unit;

class ApiSaleUnit extends Unit
{
    public function fields() {
        return array_merge(parent::fields(), [
            'created_at' => function(ApiSaleUnit $model) {
                /* @var $model ApiSaleUnit */
                return $model->created_at ? (new \DateTime($model->created_at))->format('d.m.Y') : null;
            },
            'updated_at' => function(ApiSaleUnit $model) {
                /* @var $model ApiSaleUnit */
                return $model->updated_at ? (new \DateTime($model->updated_at))->format('d.m.Y') : null;
            },
        ]);
    }
}