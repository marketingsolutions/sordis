<?php

namespace modules\sales\frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\sales\common\models\SalePointDistrib;

/**
 * SalePointDistribSearch represents the model behind the search form about `modules\sales\common\models\SalePointDistrib`.
 */
class SalePointDistribSearch extends SalePointDistrib implements SearchModelInterface

{

    public $salePointName;

    public $salePointAddress;

    public $salePointEgais;

    public $salePointFrom;

    public $salePointTo;

    public $salePointInn;

    public $isCoin;

    public $profileRttDate;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sale_point_id', 'distributor_id'], 'integer'],
            [['salePointName', 'salePointAddress', 'salePointEgais', 'salePointFrom', 'salePointTo', 'salePointInn', 'isCoin', 'profileRttDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        $distribId = \Yii::$app->request->get('id');
        if($distribId) {
            $query = SalePointDistrib::find()
                ->joinWith('salePoint')
                ->joinWith('profileRtt')
                ->joinWith('profileRttUser')
                ->where(['distributor_id' => $distribId]);
            if (\Yii::$app->request->get('SalePointDistribSearch')['profileRttDate'] && \Yii::$app->request->get('SalePointDistribSearch')['profileRttDate']!=""){
                $query->andWhere(['>=', '{{%profiles}}.created_at', date("Y-m-d 00:00:00", strtotime(\Yii::$app->request->get('SalePointDistribSearch')['profileRttDate']))]);
                $query->andWhere(['<=', '{{%profiles}}.created_at', date("Y-m-d 23:59:59", strtotime(\Yii::$app->request->get('SalePointDistribSearch')['profileRttDate']))]);
            }

            if ( \Yii::$app->request->get('SalePointDistribSearch')['isCoin'] == '') {

                return $query;
            }elseif( \Yii::$app->request->get('SalePointDistribSearch')['isCoin']==1){

                $query->andWhere('{{%profiles}}.id is not null');
            }elseif( \Yii::$app->request->get('SalePointDistribSearch')['isCoin']==0){

                $query->andWhere('{{%profiles}}.id is null');
            }
            return $query;
        }
        return SalePointDistrib::find()->joinWith('salePoint');

    }

    public function getDistribIdRtt($distribId=null)
    {
        if(!$distribId){
            return [];
        }
        return  SalePointDistrib::find()
            ->select('sale_point_id')
            ->where(['distributor_id' => $distribId])
            ->column();
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'id' => $this->id,
            'sale_point_id' => $this->sale_point_id,
            'distributor_id' => $this->distributor_id,
        ]);
        $query->andFilterWhere(['like', '{{%sale_point}}.name',$this->salePointName]);
        $query->andFilterWhere(['like', '{{%sale_point}}.address',$this->salePointAddress]);
        $query->andFilterWhere(['like', '{{%sale_point}}.code_egais',$this->salePointEgais]);
       // $query->andFilterWhere(['like', '{{%profiles}}.created_at',$this->profileRttDate]);
        $query->andFilterWhere(['like', '{{%sale_point}}.inn',$this->salePointInn]);

    }
}
