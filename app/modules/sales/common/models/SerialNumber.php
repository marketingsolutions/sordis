<?php

namespace modules\sales\common\models;

use modules\profiles\common\models\Dealer;
use modules\profiles\common\models\Profile;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;
use modules\sales\common\models\SalePosition;

/**
 * This is the model class for table "yz_serial_numbers".
 *
 * @property integer $id
 * @property string $number
 * @property string $activated_at
 * @property string $created_at
 * @property string $sale_point_name
 * @property integer $product_id
 * @property integer $is_available
 *
 * @property SalePosition[] $salesPositions
 * @property Product $product
 * @property SalePosition $salesPosition
 */
class SerialNumber extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    public $sale_id;
    public $profiles;
    public $first_name;
    public $name;
    public $recipient_id;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%serial_numbers}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Серийный номер';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Серийные номера';
    }

    public static function getProductIdValues()
    {
        return Product::find()->select('name, id')->indexBy('id')->column();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['number', 'unique'],
            ['number', 'required'],
            ['number', 'string', 'max' => 255],
            ['sale_point_name', 'string', 'max' => 255],
            ['product_id', 'required'],
            ['product_id' ,  'integer'],
            ['status' ,  'integer'],
            ['product_id', 'in', 'range' => array_keys(self::getProductIdValues())],
            ['is_available', 'boolean'],
            ['first_name', 'save'],
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => null,
            ]
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Номер',
            'activated_at' => 'Дата активации',
            'created_at' => 'Дата добавления',
            'product_id' => 'Товар',
            'is_available' => 'Доступен для использования',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /*public function getSalesPositions()
    {
        return $this->hasMany(SalePosition::class, ['serial_number_id' => 'id']);
    }*/

    /**
     * @TODO Must change this behaviour.
     * @return \yii\db\ActiveQuery
     */
    public function getSalesPosition()
    {
        return $this->hasOne(SalePosition::className(), ['serial_number_id' => 'id']);
    }
/*
    public function getSales(){
        return $this->hasOne(Sale::className(), ['id' => 'sale_id'])
            ->viaTable(SalePosition::tableName(), ['serial_number_id' => 'id']);
    }

    public function getProfile(){
        return $this->hasOne(Profile::className(), ['id'=>'recipient_id'])
            ->viaTable(Sale::tableName(), ['recipient_id' => 'id'])
            ->via('sales');
    }
*/
    public function getSalePosition()
    {
    return $this->hasOne(SalePosition::className(), ['serial_number_id' => 'id']);
    }

    public function getSale()
    {
        return $this->hasOne(Sale::className(), ['id' => 'sale_id'])
            ->via('salePosition');
    }

    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'recipient_id'])
            ->via('sale');
    }

    public function getPtofileFullname(){
        return $this->getProfile();
    }

    public function getDealerNamefull(){
        return $this->getDealer();
    }

    public function getDealer(){
        return $this->hasOne(Dealer::className(), ['id'=>'dealer_id'])
            ->via('profile');
    }
    /*Обновление статусов серийных номеров*/
    public static function updateAllStatus(){
        $model = SerialNumber::find()
            ->select('`yz_serial_numbers`.`id` as `item_status_id` , `yz_sales`.`status` as `ststus_serial_number`')
            ->leftJoin('`yz_sales_positions`' , '`yz_serial_numbers`.`id`=`yz_sales_positions`.`serial_number_id`')
            ->leftJoin('`yz_sales`' , '`yz_sales`.`id`=`yz_sales_positions`.`sale_id`')
            ->where('`yz_serial_numbers`.`activated_at` IS NOT NULL')
            ->asArray()
            ->all();
        $approved = [];
        $paid = [];
        foreach ($model as $item){
            if($item['ststus_serial_number'] == 'approved')
                $approved[]= $item['item_status_id'];
            elseif($item['ststus_serial_number'] == 'paid')
                $paid[] = $item['item_status_id'];
        }
        SerialNumber::updateAll(['status' => 1], ['id'=> $approved]);
        SerialNumber::updateAll(['status' => 2], ['id'=> $paid]);
        return true;
    }


    /**
     * @return SerialNumberQuery
     */
    public static function find()
    {
        return Yii::createObject(SerialNumberQuery::className(), [get_called_class()]);
    }

    /**
     * @return ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }





}
