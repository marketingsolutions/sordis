<?php

namespace modules\sales\common\models;

use Yii;
use yz\interfaces\ModelInfoInterface;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "yz_download_rtt_to_xml".
 *
 * @property integer $id
 * @property string $original_file_name
 * @property string $file_name
 * @property string $status_process
 * @property string $created_at
 * @property string $processed_at
 */
class DownloadRtt extends \yii\db\ActiveRecord implements ModelInfoInterface
{

    const STATUS_NEW = 'new';
    const STATUS_PROCESS = 'process';
    const STATUS_COMPLETE = 'complete';
    const STATUS_ERROR = 'error';


    public static function getStatus()
    {
        return [
            self::STATUS_NEW => 'Новая выгрузка',
            self::STATUS_PROCESS => 'В процессе обработки',
            self::STATUS_COMPLETE => 'Выгружен (доступен для скачивания) ',
            self::STATUS_ERROR => 'Ошибка обработки',
        ];
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%download_rtt_to_xml}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Download Rtt';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Выгрузка РТТ в xml';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['original_file_name', 'string', 'max' => 255],
            ['file_name', 'string', 'max' => 255],
            ['status_process', 'string', 'max' => 16],
            ['created_at', 'safe'],
            ['processed_at', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'original_file_name' => 'Имя файла',
            'file_name' => 'Имя файла в системе',
            'status_process' => 'Статус выгрузки',
            'created_at' => 'Дата загрузки',
            'processed_at' => 'Дата обработки',
        ];
    }

    /**
     * Выгрузка РТТ с привязкой к Дистру для дальнейшего скачивания
     * @param $filePath
     * @param $fileName
     * @return bool
     */
    public static function uploadRttToXml($filePath, $fileName)
    {
        $salesPoints = SalePoint::find()
            ->select('{{%sale_point}}.name as sp_name, {{%sale_point}}.address as sp_address, {{%sale_point}}.license_number as sp_license_number, {{%sale_point}}.code_egais as sp_code_egais, {{%sale_point}}.inn as sp_inn, {{%sale_point}}.work_time_from as sp_work_time_from, {{%sale_point}}.work_time_to as sp_work_time_to, {{%sale_point}}.kpp as sp_kpp, {{%sale_point}}.registred_at as sp_registred_at, {{%sale_point_profile}}.sale_point_id as sp_sale_point_id, {{%distributor}}.distrib_name as sp_distrib_name ')
            ->leftJoin('{{%sale_point_profile}}', '{{%sale_point}}.id={{%sale_point_profile}}.sale_point_id')
            ->leftJoin('{{%sale_point_distrib}}', '{{%sale_point_distrib}}.sale_point_id={{%sale_point}}.id')
            ->leftJoin('{{%distributor}}', '{{%distributor}}.id={{%sale_point_distrib}}.distributor_id')
            //->limit(1000)
            ->asArray()
            ->all();


        $xml = new \DOMDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xmlSalesPoints = $xml->createElement('SalesPoints');
        $xml->appendChild($xmlSalesPoints);
        foreach ($salesPoints as $sp){
            $xmlSalesPoint = $xml->createElement('SalesPoint');
            $xmlSalesPoints->appendChild($xmlSalesPoint);

            $nameSP = htmlspecialchars($sp['sp_name']);
            $xmlName = $xml->createElement('Name', $nameSP);
            $xmlSalesPoint->appendChild($xmlName);

            $address = htmlspecialchars($sp['sp_address']);
            $xmlAddress = $xml->createElement('Address', $address);
            $xmlSalesPoint->appendChild($xmlAddress);

            $xmLlicenseNumber = $xml->createElement('LicenseNumber', $sp['sp_license_number']);
            $xmlSalesPoint->appendChild($xmLlicenseNumber);

            $xmlEGAIS = $xml->createElement('EGAIS', $sp['sp_code_egais']);
            $xmlSalesPoint->appendChild($xmlEGAIS);

            $xmlINN = $xml->createElement('INN', $sp['sp_inn']);
            $xmlSalesPoint->appendChild($xmlINN);

            $xmlKPP = $xml->createElement('KPP', $sp['sp_kpp']);
            $xmlSalesPoint->appendChild($xmlKPP);

            $xmlTimeFrom = $xml->createElement('TimeFrom', $sp['sp_work_time_from']);
            $xmlSalesPoint->appendChild($xmlTimeFrom);

            $xmlTimeTo = $xml->createElement('TimeTo', $sp['sp_work_time_to']);
            $xmlSalesPoint->appendChild($xmlTimeTo);

            if ($sp['sp_sale_point_id']){
                $xmlIsParticipant = $xml->createElement('IsParticipant', 'ДА');
            }else{
                $xmlIsParticipant = $xml->createElement('IsParticipant', 'НЕТ');
            }
            $xmlSalesPoint->appendChild($xmlIsParticipant);

            if ($sp['sp_registred_at']){
                $xmlRegistredAt = $xml->createElement('RegistredAt', $sp['sp_registred_at']);
            }else{
                $xmlRegistredAt = $xml->createElement('RegistredAt');
            }
            $xmlSalesPoint->appendChild($xmlRegistredAt);

            if($sp['sp_distrib_name']){
                $xmlDealerName = $xml->createElement('DealerName', $sp['sp_distrib_name']);
            }else{
                $xmlDealerName = $xml->createElement('DealerName');
            }
            $xmlSalesPoint->appendChild($xmlDealerName);

        }
        if($xml->save($filePath.$fileName)){
            return true;
        }

        return false;
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $dir = \Yii::getAlias(getenv('YII_ENV') == 'dev' ? '@backend/web/download_rtt_distrib/' : '@data/download_rtt_distrib/');
            if(file_exists($dir.$this->id."rtt.xml")){
                unlink($dir.$this->id."rtt.xml");
            }
            return true;
        }
        return false;
    }
}
