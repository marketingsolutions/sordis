<?php

namespace modules\sales\common\models;

use modules\profiles\common\models\Profile;
use modules\sales\common\sales\statuses\Statuses;
use modules\sales\common\sales\statuses\StatusManager;
use marketingsolutions\datetime\DateTimeBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_sales".
 *
 * @property integer $id
 * @property string $status
 * @property integer $recipient_id
 * @property integer $bonuses
 * @property string $created_at
 * @property string $updated_at
 * @property string $sold_on
 * @property string $bonuses_paid_at
 * @property string $approved_by_admin_at
 * @property string $review_comment
 *
 * @property SalePosition[] $positions
 * @property StatusManager $statusManager
 * @property SaleDocument[] $documents
 *
 * @property string $sold_on_local
 */
class Sale extends \yz\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @var StatusManager
     */
    private $_statusManager;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sales}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Продажа';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Продажи продукции';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            'datetime' => [
                'class' => DateTimeBehavior::className(),
                'attributes' => [
                    'sold_on' => [
                        'targetAttribute' => 'sold_on_local',
                        'originalFormat' => ['date', 'yyyy-MM-dd'],
                        'targetFormat' => ['date', 'dd.MM.yyyy'],
                    ]
                ]
            ]
        ];
    }


    public static function getStatusValues()
    {
        return Statuses::statusesValues();
    }

    public function getStatusText()
    {
        if ($this->status == Statuses::PAID) {
            if ($this->bonuses_paid_at === null) {
                return 'Бонусы будут начислены в течении нескольких минут';
            }
        }
        return self::getStatusValues()[$this->status];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['sold_on_local', 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'recipient_id' => 'Получатель приза',
            'bonuses' => 'Бонусы',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'sold_on' => 'Дата продажи',
            'bonuses_paid_at' => 'Дата начисления бонусов',
            'approved_by_admin_at' => 'Дата одобрения администратором',
            'review_comment' => 'Комментарий',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasMany(SalePosition::className(), ['sale_id' => 'id']);
    }

    public function updateBonuses()
    {
        $this->bonuses = $this->getPositions()->sum('bonuses');
        $this->updateAttributes(['bonuses']);
    }

    public static function getTovars($saleId){
        $model = Sale::find()
            ->select('yz_sales_products.name as name_product')
            ->leftJoin('yz_sales_positions' , 'yz_sales_positions.sale_id=yz_sales.id')
            ->leftJoin('yz_sales_products' , 'yz_sales_products.id=yz_sales_positions.product_id')
            ->where("yz_sales.id=$saleId")
            ->asArray()
            ->all();
        $arrReturn=[];
        foreach ($model as $item){
            $arrReturn[] = $item['name_product'];
        }
        return $arrReturn;
    }

    public function getStatusManager()
    {
        if ($this->_statusManager === null) {
            $this->_statusManager = new StatusManager($this);
        }
        return $this->_statusManager;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(SaleDocument::className(), ['sale_id' => 'id']);
    }

    public function fields()
    {
        return parent::fields() + ['sold_on_local'];
    }

    public function getRecipient(){
        return $this->hasOne(Profile::tableName(), ['id'=>'recipient_id']);
    }

}
