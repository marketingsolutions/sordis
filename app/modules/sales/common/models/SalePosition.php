<?php

namespace modules\sales\common\models;

use modules\sales\common\sales\bonuses\UnitsConverter;
use Yii;
use yii\db\ActiveQuery;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_sales_positions".
 *
 * @property integer $id
 * @property integer $sale_id
 * @property integer $product_id
 * @property integer $serial_number_id
 * @property integer $custom_serial
 * @property integer $quantity
 * @property integer $bonuses
 * @property string $validation_method
 *
 * @property float $quantityLocal
 *
 * @property Sale $sale
 * @property Product $product
 * @property Unit $unit
 * @property Category category
 * @property SerialNumber $serialNumber
 * @property SaleDocument[] $documents
 */
class SalePosition extends \yz\db\ActiveRecord implements ModelInfoInterface
{
    const VALIDATION_METHOD_SERIAL = 'serial';
    const VALIDATION_METHOD_DOCUMENTS = 'documents';

    /**
     * @var UnitsConverter
     */
    private $unitsConverter;
    /**
     * @var float
     */
    private $_quantityLocal;



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sales_positions}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Товарная позиция';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Товарные позиции';
    }

    private static function getValidationMethodValues()
    {
        return [
            self::VALIDATION_METHOD_SERIAL => 'Серийный номер',
            self::VALIDATION_METHOD_DOCUMENTS => 'Подтверждающие документы'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['product_id', 'required', 'message' => 'Не выбран товар'],
            ['product_id', 'in', 'range' => array_keys(self::getProductIdValues()), 'message' => 'Выбранный товар не доступен'],

            ['quantityLocal', 'required'],
            ['quantityLocal', 'number'],
            ['quantityLocal', 'compare', 'compareValue' => 0, 'type' => 'number', 'operator' => '>',
                'message' => 'Количество должо быть больше нуля'],

            ['validation_method', 'required', 'message' => 'Необходимо выбрать способ подтверждения'],
            ['validation_method', 'in', 'range' => array_keys(self::getValidationMethodValues())],
        ];
    }

    public static function getProductIdValues()
    {
        return Product::find()->select('name, id')->indexBy('id')->column();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sale_id' => 'Sale ID',
            'product_id' => 'Product ID',
            'quantity' => 'Количество',
            'quantityLocal' => 'Количество',
            'bonuses' => 'Bonuses',
            'validation_method' => 'Способ подтверждения',
            'saleId'=>'ID продажи!',
        ];
    }

    public function fields()
    {
        return [
            'id',
            'sale_id',
            'product_id',
            'quantityLocal',
            'bonuses',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->quantityLocal !== null) {
            $this->quantity = $this->getUnitsConverter()->fromLocalToStored($this->quantityLocal);
        }

        if ($insert || $this->isAttributeChanged('quantity')) {
            $this->bonuses = $this->product->bonusesCalculator->calculateForStoredQuantity($this->quantity);
        }

        return parent::beforeSave($insert);
    }

    public function getUnitsConverter()
    {
        if ($this->unitsConverter === null && $this->unit) {
            $this->unitsConverter = new UnitsConverter($this->unit);
        }
        return $this->unitsConverter;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    public function getSaleId(){
        return $this->sale_id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit_id'])->via('product');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSale()
    {
        return $this->hasOne(Sale::className(), ['id' => 'sale_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id'])->via('product');
    }

    /**
     * @return float
     */
    public function getQuantityLocal()
    {
        if ($this->_quantityLocal !== null) {
            return $this->_quantityLocal;
        }
        if ($this->getUnitsConverter()) {
            return $this->getUnitsConverter()->fromStoredToLocal($this->quantity);
        }

        return null;
    }

    /**
     * @param float $quantityLocal
     */
    public function setQuantityLocal($quantityLocal)
    {
        $this->_quantityLocal = $quantityLocal;

        if ($this->getUnitsConverter()) {
            $this->quantity = $this->getUnitsConverter()->fromLocalToStored($quantityLocal);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSerialNumber()
    {
        return $this->hasOne(SerialNumber::class, ['id' => 'serial_number_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(SaleDocument::class, ['sale_position_id' => 'id']);
    }
}
