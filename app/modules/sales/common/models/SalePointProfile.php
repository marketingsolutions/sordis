<?php

namespace modules\sales\common\models;

use modules\profiles\common\models\Profile;
use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_sale_point_profile".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property integer $sale_point_id
 *
 * @property SalePoint $salePoint
 * @property Profiles $profile
 */
class SalePointProfile extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sale_point_profile}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Sale Point Profile';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Sale Point Profiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['profile_id', 'integer'],
            ['sale_point_id', 'integer'],
            [['sale_point_id'], 'exist', 'skipOnError' => true, 'targetClass' => SalePoint::className(), 'targetAttribute' => ['sale_point_id' => 'id']],
            [['profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['profile_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Profile ID',
            'sale_point_id' => 'Sale Point ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalePoint()
    {
        return $this->hasOne(SalePoint::className(), ['id' => 'sale_point_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profiles::className(), ['id' => 'profile_id']);
    }

    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        if($insert){
            $profile = Profile::findOne(['id' => $this->profile_id]);
            $salePoint = SalePoint::findOne(['id' => $this->sale_point_id]);
            if($profile && $salePoint){
                $salePointArr = self::find()->where(['profile_id' => $this->profile_id])->asArray()->all();
                $salePointData = "<table class='tableInTable'>";
                foreach ($salePointArr as $point){
                        $salePointData .= "<tr><td><a target='_blank' href='/sales/sale-point/update?id=".$point['sale_point_id']."'>".SalePoint::findOne(['id' => $point['sale_point_id']])->name."</a></td></tr>";
                }
                $salePointData .= "</table>";
                $profile->rtt_name = $salePointData;
                $profile->save(false);
            }
        }
    }

    public function beforeDelete()
    {
        $profile = Profile::findOne(['id' => $this->profile_id]);
        $newRttArr = self::find()
            ->where(['profile_id'=> $this->profile_id])
            ->andWhere("sale_point_id <> $this->sale_point_id")
            ->asArray()
            ->all();
        $salePointData = "<table class='tableInTable'>";
        foreach ($newRttArr as $rtt){
            $salePointData .= "<tr><td><a target='_blank' href='/sales/sale-point/update?id=".$rtt['sale_point_id']."'>".SalePoint::findOne(['id' => $rtt['sale_point_id']])->name."</a></td></tr>";
        }
        $salePointData .= "</table>";
        $profile->rtt_name = $salePointData;
        $profile->save(false);
        return parent::beforeDelete();
    }
}
