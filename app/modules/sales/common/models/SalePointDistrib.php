<?php

namespace modules\sales\common\models;

use modules\profiles\common\models\Profile;
use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_sale_point_distrib".
 *
 * @property integer $id
 * @property integer $sale_point_id
 * @property integer $distributor_id
 *
 * @property Distributor $distributor
 * @property SalePoint $salePoint
 */
class SalePointDistrib extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sale_point_distrib}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Sale Point Distrib';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Sale Point Distribs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['sale_point_id', 'integer'],
            ['distributor_id', 'integer'],
            [['distributor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Distributor::className(), 'targetAttribute' => ['distributor_id' => 'id']],
            [['sale_point_id'], 'exist', 'skipOnError' => true, 'targetClass' => SalePoint::className(), 'targetAttribute' => ['sale_point_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sale_point_id' => 'Sale Point ID',
            'distributor_id' => 'Distributor ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributor()
    {
        return $this->hasOne(Distributor::className(), ['id' => 'distributor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalePoint()
    {
        return $this->hasOne(SalePoint::className(), ['id' => 'sale_point_id']);
    }

    public function getSalePointName(){
        return $this->salePoint->name;
    }

    public function getSalePointAddress(){
        return $this->salePoint->address;
    }

    public function getSalePointEgais(){
        return $this->salePoint->code_egais;
    }

    public function getSalePointInn(){
        return $this->salePoint->inn;
    }

    public function getSalePointFrom(){
        return $this->salePoint->work_time_from;
    }

    public function getSalePointTo(){
        return $this->salePoint->work_time_to;
    }

    public function getProfileRtt()
    {
        return $this->hasOne(SalePointProfile::className(), ['sale_point_id' => 'sale_point_id']);
    }

    public function getProfileRttUser()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id'])
            ->via('profileRtt');
    }

    public function getProfileRttDate()
    {
        return isset($this->profileRttUser->created_at) ? $this->profileRttUser->created_at : null;
    }

    public function getIsCoin()
    {
        return isset($this->profileRtt->profile_id) ? 1 : 0;
    }
}
