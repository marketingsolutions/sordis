<?php

namespace modules\sales\common\models;

use modules\profiles\common\models\Profile;
use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_distributor_profile".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property integer $distrib_id
 *
 * @property Distributor $distrib
 * @property Profiles $profile
 */
class DistributorProfile extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%distributor_profile}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Distributor Profile';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Distributor Profiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['profile_id', 'integer'],
            ['distrib_id', 'integer'],
            [['distrib_id'], 'exist', 'skipOnError' => true, 'targetClass' => Distributor::className(), 'targetAttribute' => ['distrib_id' => 'id']],
            [['profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['profile_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Profile ID',
            'distrib_id' => 'Distrib ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrib()
    {
        return $this->hasOne(Distributor::className(), ['id' => 'distrib_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profiles::className(), ['id' => 'profile_id']);
    }

    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        if($insert){
            $profile = Profile::findOne(['id' => $this->profile_id]);
            $distrib = Distributor::findOne(['id' => $this->distrib_id]);
            if($profile && $distrib){
                //Пока дистрибьютор один и связь будет одна
                $profile->distrib_name = $distrib->distrib_name . "<br/>";
                $profile->save(false);
            }
        }
    }
}
