<?php

namespace modules\sales\common\models;

use modules\profiles\common\models\Dealer;
use modules\profiles\common\models\Profile;
use modules\sales\common\sales\bonuses\BonusesCalculator;
use modules\sales\common\sales\bonuses\FormulaValidator;
use Yii;
use yii\db\ActiveQuery;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_sales_products".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property integer $unit_id
 * @property string $bonuses_formula
 * @property string $dealer_name
 *
 * @property Category $category
 * @property Unit $unit
 *
 * @property BonusesCalculator $bonusesCalculator
 * @property SerialNumber[] $serialNumbers
 */
class Product extends \yz\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @var BonusesCalculator
     */
    private $bonusesCalculator;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sales_products}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Товар';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Продукция';
    }

    public static function getCategoryIdValues()
    {
        return Category::find()->select('name, id')->indexBy('id')->column();
    }

    public static function getUnitIdValues()
    {
        return Unit::find()->select('name, id')->indexBy('id')->column();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'unit_id'], 'integer'],
            [['name', 'bonuses_formula'], 'string', 'max' => 255],
            ['category_id', 'required'],
            ['unit_id', 'required'],
            ['name', 'required'],
            ['bonuses_formula', 'required'],
            ['bonuses_formula', FormulaValidator::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'name' => 'Название',
            'dealer_name' => 'Наименование сети магазинов',
            'unit_id' => 'Единица измерения',
            'bonuses_formula' => 'Формула для бонусов',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit_id']);
    }

    public function getBonusesCalculator()
    {
        if ($this->bonusesCalculator === null) {
            $this->bonusesCalculator = new BonusesCalculator($this);
        }
        return $this->bonusesCalculator;
    }

    /**
     * @return ActiveQuery
     */
    public function getSerialNumbers()
    {
        return $this->hasMany(SerialNumber::class, ['product_id' => 'id']);
    }

    public static function getOptions()
    {
        return self::find()->indexBy('id')->select('name, id')->orderBy(['name' => SORT_ASC])->column();
    }
}
