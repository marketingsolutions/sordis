<?php

namespace modules\sales\common\models;

use yii\db\ActiveQuery;


/**
 * Class SerialNumberQuery
 */
class SerialNumberQuery extends ActiveQuery
{
    public function notActivated()
    {
        $this->andWhere('activated_at IS NULL');
        return $this;
    }

    public function isAvailable()
    {
        $this->andWhere('is_available = 1');
        return $this;
    }

    public function byNumber($number)
    {
        $this->andWhere('LOWER(number) = :by_number', [
            ':by_number' => strtolower($number),
        ]);
        return $this;
    }

    public function byNumberLike($number)
    {
        $this->andWhere('
            LOWER(number) = :number OR
            IF (
                LEFT(number, 3) = "KOR",
                LENGTH(:number) > 13 AND RIGHT(LOWER(number), LENGTH(:number)) = :number,
                LENGTH(:number) > 6 AND RIGHT(LOWER(number), LENGTH(:number)) = :number
            )
        ', [
            ':number' => strtolower($number),
        ]);
        return $this;
    }

    public function bySale(Sale $sale)
    {
        return $this->bySaleId($sale->id);
    }

    public function bySaleId($saleId)
    {
        $this->andWhere([
            'id' => SalePosition::find()
                ->select('serial_number_id')
                ->where([
                    'sale_id' => $saleId,
                ])
        ]);
        return $this;
    }
}