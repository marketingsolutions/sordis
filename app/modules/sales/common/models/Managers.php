<?php

namespace modules\sales\common\models;

use Yii;
use yz\admin\mailer\common\lists\MailerListProfiles;
use yz\admin\mailer\common\lists\ManualMailList;
use yz\admin\mailer\common\models\Mail;
use yz\admin\mailer\common\models\Mailer;
use yz\admin\models\User;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_managers".
 *
 * @property integer $id
 * @property string $fio
 * @property string $email
 *
 * @property Distributor[] $distributors
 */
class Managers extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%managers}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Managers';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Менеджеры ТО';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['fio', 'string', 'max' => 255],
            ['email', 'string', 'max' => 255],
            ['email', 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID менеджера',
            'fio' => 'ФИО',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributors()
    {
        return $this->hasMany(Distributor::className(), ['manager_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function optionManager(){
        return self::find()->select('fio')->indexBy('id')->column();
    }

    /**
     * Получаем ID менеджера ТО
     * @return bool|int
     */
    public static function getManagerTo(){
        $adminId = \Yii::$app->user->getId();
        $adminMail = User::findOne(['id' => $adminId]);
        if(!$adminMail){
            return false;
        }else{
            return $adminMail->id;
        }
    }

    /**
     * Получаем ID профилей в разрезе Менеджеров ТО
     * @return array|bool
     */
    public static function getManagerToProfileId(){
        $adminId = \Yii::$app->user->getId();
        $arrProfile = [];
        $distrib = Distributor::find()
            ->select('{{%distributor_profile}}.profile_id as id_profile')
            ->leftJoin('{{%distributor_profile}}', '{{%distributor_profile}}.distrib_id={{%distributor}}.id')
            ->where(['{{%distributor}}.manager_id' => $adminId])
            ->asArray()
            ->all();
        foreach ($distrib as $item){
            $arrProfile[] = $item['id_profile'];
        }
        return $arrProfile;
        }

    /**
     * Отправка письма менеджеру ТО
     * @param $profile_id
     * @param $subject
     * @param $message
     */
    public static function sendMailToManagerTo($profile_id, $subject ,$message)
    {
        $distrib = DistributorProfile::find()
            ->select('{{%managers}}.email')
            ->leftJoin('{{%distributor}}', '{{%distributor}}.id = {{%distributor_profile}}.distrib_id')
            ->leftJoin('{{%managers}}', '{{%managers}}.id = {{%distributor}}.manager_id')
            ->where(['profile_id' => $profile_id])
            ->column();
        if(!empty($distrib)){
            $addMessage = new Mail();
            $addMessage->receivers_provider = ManualMailList::class;
            $addMessage->status = Mail::STATUS_NEW;
            $to=[];
            $to['to'] = implode(";", $distrib);
            $addMessage->receivers_provider_data = json_encode($to);
            $addMessage->subject = $subject;
            $addMessage->body_html = $message;
            $addMessage->created_at = date("Y-m-d H:i:s");
            $addMessage->save(false);
            $sendMessage = Mail::findOne($addMessage->id);
            $sendMessage->updateAttributes(['status' => Mail::STATUS_WAITING]);
        }
    }

}
