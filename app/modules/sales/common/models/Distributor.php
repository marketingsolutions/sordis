<?php

namespace modules\sales\common\models;

use modules\profiles\common\models\Profile;
use Yii;
use yz\admin\models\User;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_distributor".
 *
 * @property integer $id
 * @property integer $manager_id
 * @property string $distrib_name
 * @property string $distrib_address
 * @property string $egais
 * @property string $inn
 * @property string $kpp
 * @property string $license_number
 * @property string $created_at
 * @property string $updated_at
 *
 * @property DistributorProfile[] $distributorProfiles
 */
class Distributor extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%distributor}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Distributor';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Список Дистрибьюторов';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['distrib_name', 'string', 'max' => 255],
            ['manager_id', 'integer'],
            ['distrib_address', 'string', 'max' => 255],
            ['egais', 'string', 'max' => 12, 'min'=>12],
            ['inn', 'string', 'max' => 10, 'min'=>10],
            ['kpp', 'string', 'max' => 25],
            ['license_number', 'string', 'max' => 12, 'min'=>12],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'distrib_name' => 'Наименование Дистрибьютора',
            'distrib_address' => 'Юридический адрес',
            'manager_id' => 'Менеджер ТО',
            'egais' => 'Код ЕГАИС',
            'inn' => 'ИНН',
            'license_number' => 'Номер лицензии',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'kpp' => 'КПП',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributorProfiles()
    {
        return $this->hasMany(DistributorProfile::className(), ['distrib_id' => 'id']);
    }

    public function getProfiles()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id'])
            ->via('distributorProfiles');
    }

    public function getProfileCreate()
    {
        return isset($this->profiles->created_at) ? $this->profiles->created_at : null;
    }

    public function getIsProfile()
    {
        return isset($this->profiles->id) ? 1 : 0;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager(){
        return $this->hasOne(User::className(), ['id' =>'manager_id']);
    }

    /**
     * @return mixed
     */
    public function getManagerFullName(){
        return empty($this->manager->name) ? null : $this->manager->name;
    }

    /**
     * @return mixed
     */
    public function getManagerEmail(){
        return empty($this->manager->email) ? null : $this->manager->email;
    }

    /**
     * @return $this
     */
    public function getProfile(){
        return $this->hasOne(Profile::className(), ['id' => 'profile_id'])
            ->via('distributorProfiles');
    }




    /**
     * @return null
     */
    public function getProfilePhone(){
        return isset($this->profile->phone_mobile) ? $this->profile->phone_mobile : null;
    }

    /**
     * @return null
     */
    public function getProfileFullName(){
        return isset($this->profile->full_name) ? $this->profile->full_name : null;
    }

    /**
     * @return null
     */
    public function getProfileEmail(){
        return isset($this->profile->email) ? $this->profile->email : null;
    }

    /**
     * Данные дистрибьютора
     * @param $profileId
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getDistribData($profileId){
        $model = DistributorProfile::find()
            ->select('{{%distributor}}.id as distrib_id, {{%distributor}}.distrib_name as name, {{%distributor}}.distrib_address as address, {{%distributor}}.egais as code_egais, {{%distributor}}.inn as code_inn, {{%distributor}}.license_number as license, {{%distributor}}.kpp as code_kpp')
            ->leftJoin('{{%distributor}}', '{{%distributor}}.id={{%distributor_profile}}.distrib_id')
            ->where(['{{%distributor_profile}}.profile_id' => $profileId])
            ->asArray()
            ->all();
        return $model;
    }

    /**
     * Получаем список РТТ Дистрибьютора
     * @param $distrib_id
     * @return array|bool|\yii\db\ActiveRecord[]
     */
    public static function getDistribRtt($distrib_id){
        $model = SalePointDistrib::find()
            ->select('{{%sale_point}}.name as name_rtt, {{%sale_point}}.address as address_rtt, {{%sale_point}}.code_egais as egais_rtt, {{%sale_point}}.work_time_from as time_from_rtt, {{%sale_point}}.work_time_to as work_time_to_rtt')
            ->leftJoin('{{%sale_point}}', '{{%sale_point}}.id = {{%sale_point_distrib}}.sale_point_id')
            ->where(['{{%sale_point_distrib}}.distributor_id' => $distrib_id])
            ->asArray()
            ->all();
        return !empty($model) ? $model : false;
    }

    /**
     * Получаем профили учатников РТТ у определенного Менеджера ТО
     * @return array
     */
    public static function getProfileForManagerTo(){
        $adminId = \Yii::$app->user->getId();
        $model = Distributor::find()
            ->select('{{%sale_point_profile}}.profile_id as profile_id')
            ->leftJoin('{{%sale_point_distrib}}', '{{%sale_point_distrib}}.distributor_id={{%distributor}}.id')
            ->leftJoin('{{%sale_point_profile}}', '{{%sale_point_profile}}.sale_point_id={{%sale_point_distrib}}.sale_point_id')
            ->where(['{{%distributor}}.manager_id' => $adminId])
            ->groupBy('{{%sale_point_profile}}.profile_id')
            ->asArray()
            ->all();
        $arrReturn = [];
        foreach ($model as $item){
            $arrReturn[]=$item['profile_id'];
        }
        return $arrReturn;
    }
}
