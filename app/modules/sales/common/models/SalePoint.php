<?php

namespace modules\sales\common\models;

use modules\bonuses\common\models\TempBonuses;
use modules\bonuses\common\models\TempConfirmedBonuses;
use modules\calc\common\models\Calc;
use modules\calc\common\models\CalculatorDistrib;
use modules\profiles\common\models\Profile;
use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_sale_point".
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $license_number
 * @property string $code_egais
 * @property string $created_at
 * @property string $updated_at
 * @property string $registred_at
 * @property string $inn
 * @property string $kpp
 * @property string $egais
 * @property string $work_time_from
 * @property string $work_time_to
 *
 * @property SalePointProfile[] $salePointProfiles
 */
class SalePoint extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sale_point}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Sale Point';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Список розничных торговых точек';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string', 'max' => 255],
            ['address', 'string', 'max' => 255],
            ['license_number', 'string', 'max' => 12, 'min' => 12],
            ['code_egais', 'string', 'max' => 12, 'min' => 12],
            ['code_egais', 'unique'],
            ['inn', 'string', 'max' => 10, 'min' => 10],
            ['kpp', 'string', 'max' => 25],
            [['work_time_from', 'work_time_to'], 'safe'],
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
            ['registred_at', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID ТТ',
            'name' => 'Наименование ТТ',
            'address' => 'Адрес ТТ',
            'license_number' => 'Номер лицензии',
            'code_egais' => 'Код ЕГАИС',
            'kpp' => 'КПП',
            'inn' => 'ИНН',
            'work_time_from' => 'Часы работы магазина С',
            'work_time_to' => 'Часы работы магазина По',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'registred_at' => 'Дата регистрации в МП «Копилка»',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalePointProfiles()
    {
        return $this->hasMany(SalePointProfile::className(), ['sale_point_id' => 'id']);
    }



    /**
     * @return $this
     */
    public function getProfiles(){
        return $this->hasOne(Profile::className(), ['id' =>'profile_id'])
            ->via('salePointProfiles');
    }

    public function getProfileDateAdd()
    {
        return isset($this->profiles->created_at) ? $this->profiles->created_at : null;
    }

    /**
     * @return null
     */
    public function getProfileName(){
        return !empty($this->profiles->full_name) ? $this->profiles->full_name : null;
    }

    /**
     * @return null
     */
    public function getProfilePhone(){
        return !empty($this->profiles->phone_mobile) ? $this->profiles->phone_mobile : null;
    }

    /**
     * @return null
     */
    public function getProfileEmail(){
        return !empty($this->profiles->email) ? $this->profiles->email : null;
    }

    /**
     * Получаем все РТТ Менеджера ТО
     * @param $managerId
     * @return array
     */
    public static function getSalePointIdForManagerTo(){
        $adminId = \Yii::$app->user->getId();
        $model = Distributor::find()
            ->select('{{%sale_point_distrib}}.sale_point_id as id_sale_point')
            ->leftJoin('{{%sale_point_distrib}}', '{{%sale_point_distrib}}.distributor_id={{%distributor}}.id')
            ->where(['{{%distributor}}.manager_id' => $adminId])
            ->asArray()
            ->all();
        $arrReturn = [];
        foreach ($model as $item){
            $arrReturn[]=$item['id_sale_point'];
        }
        return $arrReturn;
    }


    /**
     * Получаем данные по РТТ
     * @param $profileId
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getRttData($profileId){
        $model = SalePointProfile::find()
            ->select('{{%sale_point}}.id as ids, {{%sale_point}}.name as sale_point_name, {{%sale_point}}.address as sale_point_adderss, {{%sale_point}}.license_number as license, {{%sale_point}}.code_egais as egais, {{%sale_point}}.inn as sale_point_inn, {{%sale_point}}.kpp as sale_point_kpp, {{%sale_point}}.work_time_from as time_from, {{%sale_point}}.work_time_to as time_to')
            ->leftJoin('{{%sale_point}}', '{{%sale_point}}.id={{%sale_point_profile}}.sale_point_id')
            ->where(['{{%sale_point_profile}}.profile_id' => $profileId])
            ->asArray()
            ->all();
        return $model;
    }

    /**
     * Получение фото монетки в дашборд у участника РТТ
     * @param $code_egais
     * @return bool|null|static
     */
    public static function getPhotoCoin($code_egais)
    {
        $model = TempConfirmedBonuses::find()
            ->where(['code_egais' => $code_egais])
            ->orderBy(['period' => SORT_DESC])
            ->asArray()
            ->one();

        if(empty($model)){
            $model = TempBonuses::find()
                ->where(['code_egais' => $code_egais])
                ->orderBy(['period' => SORT_DESC])
                ->asArray()
                ->one();

            if(empty($model)){
                $model['state']=0;
            }
        }

        //Находим нужную монетку по переданному в xml sku (sku это и есть state)
        $calc = Calc::find()
        ->where(["<=","sku_min" , $model['state']])
        ->andWhere([">=","sku_max" , $model['state']])
        ->asArray()
        ->one();

        if(empty($calc)){
            return false;
        }
        return  Calc::findOne($calc['id']);
    }


    /**
     * Получение коэфициента АКБC для дажборда у участника Дистрибьютора
     * @param $code_egais
     * @return bool|mixed
     */
    public static function getAkbs($code_egais)
    {
        $model = TempConfirmedBonuses::find()
            ->where(['code_egais' => $code_egais])
            ->orderBy(['period' => SORT_DESC])
            ->asArray()
            ->one();


        if(!empty($model)){
            return $model['state'];
        }else{
            $tempModel = TempBonuses::find()
                ->where(['code_egais' => $code_egais])
                ->orderBy(['period' => SORT_DESC])
                ->asArray()
                ->one();

            if(!empty($tempModel)){
                return $tempModel['state'];
            }
            return false;
        }
    }

    /**
     * @param $salePointId
     * @return array|bool
     */
    public static function getDistributorName($salePointId)
    {
        $model = SalePointDistrib::find()
            ->select('{{%distributor}}.distrib_name as name')
            ->leftJoin('{{%distributor}}', '{{%distributor}}.id={{%sale_point_distrib}}.distributor_id')
            ->where(['{{%sale_point_distrib}}.sale_point_id' => $salePointId])
            ->column();

        if(empty($model)){
            return false;
        }
        return $model;
    }

    public function getIsParticipant()
    {
        return SalePointProfile::findOne(['sale_point_id' => $this->id]) ? 1 : 0;
    }

}
