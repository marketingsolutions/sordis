<?php

namespace modules\sales\common\sales\bonuses;

use modules\sales\common\models\Unit;
use yii\base\Object;


/**
 * Class UnitsConverter
 */
class UnitsConverter extends Object
{
    /**
     * @var Unit
     */
    private $unit;

    public function __construct(Unit $unit, $config = [])
    {
        $this->unit = $unit;

        parent::__construct($config);
    }

    /**
     * @param int $stored
     * @return float
     */
    public function fromStoredToLocal($stored)
    {
        return $stored / $this->unit->quantity_divider;
    }

    /**
     * @param $local
     * @return integer
     */
    public function fromLocalToStored($local)
    {
        return intval($local * $this->unit->quantity_divider);
    }

}