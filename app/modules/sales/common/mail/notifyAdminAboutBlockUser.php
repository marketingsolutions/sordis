<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \yii\mail\BaseMessage $message instance of newly created mail message
 * @var \modules\sales\common\models\SerialNumber $serialNumber
 * @var \modules\profiles\common\models\Profile $profile
 */
$message->setSubject('Уведомление блокировке участника.');
?>

<p>Уважаемый администратор,</p>

<p>
    Участник <?= $profile->full_name ?> (<?= $profile->phone_mobile_local ?>) заблокирован по причине неправильного ввода серийного номера с 3-х поппыток.
</p>

