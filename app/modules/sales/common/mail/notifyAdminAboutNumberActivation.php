<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \yii\mail\BaseMessage $message instance of newly created mail message
 * @var \modules\sales\common\models\SerialNumber $serialNumber
 * @var \modules\profiles\common\models\Profile $profile
 */
$message->setSubject('Уведомление о попытке повторной активации серийного номера');
?>

<p>Уважаемый администратор,</p>

<p>
    Участник <?= $profile->full_name ?> (<?= $profile->phone_mobile_local ?>) попытался
    активировать серийный номер <?= $serialNumber->number ?>, который до него было активирован другим участником. ID продажи для этого серийного номера <?=$saleId?>
</p>

