<?php

namespace modules\sales\common\app\widgets;
use yii\base\Widget;


/**
 * Class SaleApp
 */
class SaleApp extends Widget
{
    /**
     * Sale id
     * @var int
     */
    public $id;

    /**
     * Configuration for sale application
     * @var array
     */
    public $config;

    /**
     * Executes the widget.
     * @return string the result of widget execution to be outputted.
     */
    public function run()
    {
        $id = $this->id;
        $config = $this->config;
        return $this->render('sale-app', compact('id', 'config'));
    }


}