<?php

/*
 * @var int | null $id
 * @var $this yii\web\View
 * @var $form yii\widgets\ActiveForm
 * @var array $config
 */

use modules\sales\common\sales\statuses\Statuses;

\modules\sales\common\app\assets\SaleAppAsset::register($this);

$settings = \yii\helpers\Json::encode(\yii\helpers\ArrayHelper::merge([
    'id' => $id,
    'saveUrl' => is_null($id) ? '/create' : '/update',
], $config));

$js = <<<JS
saleEditApplication.configure({$settings});
JS;
$this->registerJs($js, \yii\web\View::POS_END);

?>

<div class="sale-form ng-cloak" ng-app="SaleEditApplication" ng-controller="SaleEdit">
    <?php if (YII_ENV_DEV): ?>
        {{ model }}
    <?php endif ?>

    <div class="alert alert-danger" ng-show="errors.length > 0">
        <strong><i class="fa fa-exclamation-circle fa-lg"></i></strong> Обнаружены следующие ошибки:<br><br>
        <ul>
            <li ng-repeat="error in errors">{{ error.message }}</li>
        </ul>
    </div>

    <form class="form-horizontal">

        <div class="panel panel-default">
            <div class="panel-heading">
                Общая информация
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-md-3">Дата продажи</label>

                    <div class="col-md-3">
                        <?= \marketingsolutions\widgets\DatePicker::widget([
                            'dateFormat' => 'dd.MM.yyyy',
                            'name' => '',
                            'options' => [
                                'class' => 'form-control',
                                'ng-model' => 'model.sale.sold_on_local',
                            ],
                            'clientEvents' => [
                                'changeDate' => 'function (e) {
                            angular.element((e.target||e.srcElement)).triggerHandler("input");
                        }'
                            ]
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
<!--Способы подтверждения-->
<!--        <div class="panel panel-default">-->
<!--            <div class="panel-heading">-->
<!--                Способ подтверждения продажи-->
<!--            </div>-->
<!--            <div class="panel-body">-->
<!--                <div ng-show="global_validation_method == null || global_validation_method == ''">-->
<!--                    <a class="link-primary link-local"-->
<!--                       ng-click="setGlobalValidationMethod('serial')">Серийные номера</a>-->
<!--                    или <a class="link-primary link-local"-->
<!--                           ng-click="setGlobalValidationMethod('documents')">подтверждающий документ</a>-->
<!--                </div>-->
<!--                <div ng-show="global_validation_method == 'serial'">-->
<!--                    <h3>Серийные номера</h3>-->
<!--                    или <a class="link-primary link-local"-->
<!--                           ng-click="setGlobalValidationMethod('documents')">подтверждающие документы</a>-->
<!--                </div>-->
<!--                <div ng-show="global_validation_method == 'documents'">-->
<!--                    <h3>Подтверждающие документы</h3>-->
<!--                    или <a class="link-primary link-local"-->
<!--                           ng-click="setGlobalValidationMethod('serial')">серийные номера</a>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--Способы подтверждения-->
        <div class="panel panel-default">
            <div class="panel-heading">
                Товарные позиции
            </div>
            <div class="panel-body">

                <button type="button" class="btn btn-success" ng-click="setGlobalValidationMethod('serial'); addPosition(); ">
                    <i class="fa fa-plus"></i> Добавить позицию
                </button>

                <table class="table" ng-show="model.sale.positions.length > 0">
                    <tr>
                        <th class="col-md-3" ng-if="categories.length > 1">Категория</th>
                        <th class="col-md-3">Выберите товар</th>
                        <th class="col-md-7">Введите серийный номер </th>
                        <!--<th class="col-md-2">Количество</th>-->
                        <th class="col-md-1"></th>
                    </tr>
                    <tr ng-repeat="position in model.sale.positions">
                        <td ng-if="categories.length > 1">
                            <select class="form-control"
                                    ng-model="position.category_id"
                                    ng-change="position.changeCategory()">
                                <option ng-repeat="category in categories" value="{{ category.id }}">
                                    {{ category.name }}
                                </option>
                            </select>
                        </td>
                        <td>
                            <select class="form-control"
                                    ng-model="position.product_id"
                                    ng-change="position.changeProduct()"
                                    ng-disabled="position.getProducts().length == 0">
                                <option ng-repeat="product in position.getProducts()"
                                        value="{{ product.id }}">
                                    {{ product.name }}
                                </option>
                            </select>
                        </td>

                        <td ng-show="position.validation_method == null || position.validation_method == ''">
                            <i>Выберите способ подтверждения проданных товарных позиций</i>
                        </td>
                        <td ng-show="position.validation_method == 'serial'">
                            <input type="text" class="form-control"
                                   ng-model="position.serialNumberValue"
                                   ng-disabled="position.product_id == null || position.product_id == 0"
                            />
                        </td>
                        <!--                        <td ng-show="position.validation_method == 'documents'">-->
                        <!--                            <div class="row">-->
                        <!--                                <div class="col-md-6" ng-repeat="document in position.documents">-->
                        <!--                                    <div class="thumbnail">-->
                        <!--                                        <a ng-href="/sales/sales/download-document?id={{ document.id }}" target="_blank"-->
                        <!--                                           ng-if="document.uploaded">-->
                        <!---->
                        <!--                                            <img ng-src="/sales/sales/download-document?id={{ document.id }}"-->
                        <!--                                                 ng-if="document.isImage"/>-->
                        <!---->
                        <!--                                            <span ng-if="!document.isImage">-->
                        <!--                                                {{ document.original_name }}-->
                        <!--                                            </span>-->
                        <!--                                        </a>-->
                        <!---->
                        <!---->
                        <!--                                        <div class="btn btn-default"-->
                        <!--                                             ngf-select=""-->
                        <!--                                             ng-model="document.document"-->
                        <!--                                             ngf-change="document.upload()"-->
                        <!--                                             ng-disabled="document.uploading"-->
                        <!--                                             ng-show="! document.uploaded"-->
                        <!--                                             ngf-multiple="false"-->
                        <!--                                            >-->
                        <!--                                            <i class="fa fa-btn fa-spinner fa-spin" ng-if="document.uploading"></i>-->
                        <!--                                            Выбрать файл-->
                        <!--                                        </div>-->
                        <!---->
                        <!--                                        <div class="caption">-->
                        <!---->
                        <!--                                            <div ng-show="document.errors.length > 0">-->
                        <!--                                            <span class="label label-danger" ng-repeat="error in document.errors">-->
                        <!--                                                {{ error.message }}-->
                        <!--                                            </span>-->
                        <!--                                            </div>-->
                        <!---->
                        <!--                                            <button type="button" class="btn btn-danger btn-sm"-->
                        <!--                                                    ng-click="removeDocument(position, document)">-->
                        <!--                                                <i class="fa fa-trash"></i>-->
                        <!--                                            </button>-->
                        <!--                                        </div>-->
                        <!--                                    </div>-->
                        <!--                                </div>-->
                        <!---->
                        <!--                                <div class="col-md-6">-->
                        <!--                                    <button type="button" class="btn btn-success"-->
                        <!--                                            ng-click="addDocument(position)"-->
                        <!--                                            ng-disabled="position.product_id == null || position.product_id == 0"-->
                        <!--                                        >-->
                        <!--                                        <i class="fa fa-plus"></i> Добавить документ-->
                        <!--                                    </button>-->
                        <!--                                </div>-->
                        <!--                            </div>-->
                        <!--                        </td>-->

                        <!--                        <td>-->
                        <!--                            <div class="input-group">-->
                        <!--                                <input class="form-control" type="number"-->
                        <!--                                       ng-model="position.quantityLocal"-->
                        <!--                                       ng-change="position.changeQuantity()"-->
                        <!--                                       ng-disabled="position.product_id == null"/>-->
                        <!---->
                        <!--                                <div class="input-group-addon">-->
                        <!--                                    {{ position.getUnit().short_name }}-->
                        <!--                                </div>-->
                        <!--                            </div>-->
                        <!--                            {{ position.bonuses }} бонусов-->
                        <!--                        </td>-->
                        <td>
                            <button type="button" class="btn btn-danger"
                                    ng-click="removePosition(position)">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" ng-click="position.validation_method == 'serial'" ng-show="global_validation_method == 'serial'" ng-hide="global_validation_method == 'documents' || global_validation_method == null || global_validation_method == ''">
                            <div class="row">
                                <div class="col-md-6" ng-repeat="document in model.documents">
                                    <div class="thumbnail">
                                        <a ng-href="/sales/sales/download-document?id={{ document.id }}" target="_blank"
                                           ng-if="document.uploaded">

                                            <img ng-src="/sales/sales/download-document?id={{ document.id }}"
                                                 ng-if="document.isImage"/>

                                            <span ng-if="!document.isImage">
                                                {{ document.original_name }}
                                            </span>
                                        </a>


                                        <div class="btn btn-default"
                                             ngf-select=""
                                             ng-model="document.document"
                                             ngf-change="document.upload()"
                                             ng-disabled="document.uploading"
                                             ng-show="! document.uploaded"
                                             ngf-multiple="false"
                                        >
                                            <i class="fa fa-btn fa-spinner fa-spin" ng-if="document.uploading"></i>
                                            Выбрать файл
                                        </div>

                                        <div class="caption">

                                            <div ng-show="document.errors.length > 0">
                                            <span class="label label-danger" ng-repeat="error in document.errors">
                                                {{ error.message }}
                                            </span>
                                            </div>

                                            <button type="button" class="btn btn-danger btn-sm"
                                                    ng-click="removeDocument(document)">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <button type="button" class="btn btn-success"
                                            ng-click="addDocument(position)"
                                            ng-disabled="false"
                                    >

                                        <i class="fa fa-plus"></i> Прикрепить скан чека
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary"
                        ng-click="save('<?= Statuses::ADMIN_REVIEW ?>')"
                        ng-disabled="model.sale.points.length == 0 || disabled">
                    <i class="fa fa-btn fa-check" ng-if=" ! disabled"></i>
                    <i class="fa fa-btn fa-spinner fa-spin" ng-if="disabled"></i>
                    Отправить на проверку
                </button>

                <button type="button" class="btn btn-default" ng-click="save('<?= Statuses::DRAFT ?>')"
                        ng-disabled="model.sale.points.length == 0 || disabled">
                    <i class="fa fa-btn fa-circle-o" ng-if=" ! disabled"></i>
                    <i class="fa fa-btn fa-spinner fa-spin" ng-if="disabled"></i>
                    Сохранить в черновики
                </button>
            </div>
            <div class="col-md-8">

            </div>
        </div>
    </form>

</div>

