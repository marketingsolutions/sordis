<?php

namespace modules\sales\common\app\models;

use modules\profiles\common\models\Profile;
use modules\sales\backend\rbac\Rbac;
use modules\sales\common\models\SaleDocument;
use modules\sales\common\models\SalePosition;
use modules\sales\common\models\SerialNumber;
use yii\base\InvalidValueException;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yz\admin\models\User;
use Yii;


/**
 * Class ApiPosition
 * @property string $serialNumberValue
 */
class ApiPosition extends SalePosition
{
    /**
     * @var string
     */
    private $_serialNumberValue;

    public function fields()
    {
        return [
            'product_id',
            'bonuses',
            'documents',
            'quantityLocal',
            'serialNumberValue',
            'validation_method',
        ];
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['serialNumberValue', 'string'],
            ['serialNumberValue', 'validateSerialNumberValue']
        ]);
    }

    public function validateSerialNumberValue()
    {
        if ($this->validation_method == self::VALIDATION_METHOD_DOCUMENTS) {
            return;
        }

        if ($this->_serialNumberValue === null) {
            return;
        }

        if ($this->_serialNumberValue === '') {
            $this->addError('serialNumberValue', 'Необходимо ввести серийный номер');
        }

        /** @var SerialNumber $serialNumber */
        $serialNumber = SerialNumber::findOne(['number' => $this->_serialNumberValue]);

//        if ($serialNumber == null) {
//            $this->addError('serialNumberValue', "Серийный номер {$this->_serialNumberValue} не найден");
//            return;
//        }
        if($serialNumber) {

            if ($serialNumber->is_available == false) {
                $this->addError('serialNumberValue', "Серийный номер {$this->_serialNumberValue} уже был активирован");
                return;
            }

            $isThisSaleSerialNumber = SerialNumber::find()
                ->byNumberLike($this->_serialNumberValue)
                ->bySaleId($this->sale_id)
                ->exists();

            if ($isThisSaleSerialNumber) {
                return;
            }
        }

        /** @var SerialNumber $serialNumber */
        $serialNumber = SerialNumber::find()
            ->byNumberLike($this->_serialNumberValue)
            ->one();
        if($serialNumber) {
            if ($serialNumber === null) {
                $this->addError('serialNumberValue', 'Серийный номер ' . $this->_serialNumberValue . ' не найден в базе в автоматическом режиме. Просим вас зарегистрировать продажу, прикрепив фотографию серийного номера либо скан отгрузочных документов');
            }

            if (
            (
                $serialNumber !== null
                && $serialNumber->activated_at !== null
                && ArrayHelper::getValue($serialNumber, 'salesPosition.sale.recipient_id', $this->sale->recipient_id) != $this->sale->recipient_id
            )
            ) {
                $this->addError('serialNumberValue', 'Серийный номер ' . $this->_serialNumberValue . ' уже был зарегистрирован!');
                $this->notifyAdminAboutNumberActivation($serialNumber, $this->sale->recipient_id);
            }

            if ($serialNumber !== null
                && $serialNumber->activated_at !== null
                && ArrayHelper::getValue($serialNumber, 'salesPosition.sale.recipient_id', $this->sale->recipient_id) == $this->sale->recipient_id
            ) {
                $this->addError('serialNumberValue', 'Серийный номер ' . $this->_serialNumberValue . ' уже был зарегистрирован!');
                $this->notifyAdminAboutNumberActivation($serialNumber, $this->sale->recipient_id);
            }

            if ($serialNumber !== null && $serialNumber->product_id !== null && $serialNumber->product_id != $this->product_id) {
                $this->addError('serialNumberValue', 'Серийный номер ' . $this->_serialNumberValue . ' не соответствует выбранному товару');
            }
        }
    }

    /**
     * @param $serialNumber
     * @return mixed|null
     */
    public function mySaleId($serialNumber){
        $model = SerialNumber::find(['number' => $serialNumber])->asArray()->one();
        $sale = SalePosition::find(['serial_number_id' => $model['id']])->asArray()->one();
        if($sale['sale_id'])
            return $sale['sale_id'];
        else
            return null;
    }

    private function notifyAdminAboutNumberActivation($serialNumber, $recipientId)
    {
        $profile = Profile::findOne($recipientId);
        $saleId = $this->mySaleId($serialNumber);
        foreach (User::find()->byRoles(Rbac::ROLE_RECEIVE_ACTIVATIONS_NOTIFICATIONS)->each() as $admin) {
            /** @var User $admin */
            \Yii::$app->mailer
                ->compose('@modules/sales/common/mail/notifyAdminAboutNumberActivation', [
                    'serialNumber' => $serialNumber,
                    'profile' => $profile,
                    'saleId' => $saleId,
                ])
                ->setTo($admin->email)
                //->setTo('km@msforyou.ru')
                ->send();
        }
    }

    public function isCount(){
        if(!\Yii::$app->request->post())
            return true;

        $postData = \Yii::$app->request->post('model', null);

        if(!isset($postData['documents'])) {
            return false;
        }

        $docId = [];
        foreach ($postData['documents'] as $doc) {
            if($doc['isImage']) {
                $docId[] = $doc['id'];
            }
        }

        if(count($docId) > 0)
            return false;
        else
            return true;
    }

    public function afterValidate()
    {
        parent::afterValidate();

        if ($this->validation_method == '') {
            $this->addError('serialNumberValue', 'Необходимо подтвердить продажу с помощью либо серийного номера, либо - подтверждающих документов');
        }

        //if ($this->validation_method == self::VALIDATION_METHOD_DOCUMENTS && count($this->documents) == 0) {
        if ($this->validation_method == self::VALIDATION_METHOD_SERIAL && $this->isCount()) {
            $this->addError('documents', 'Необходимо прикрепить подтверждающие документы');
        }

    }

    public function loadApi($positionData)
    {
        $result = $this->load($positionData, '');

        $this->documents = [];
        foreach (ArrayHelper::getValue($positionData, 'documents', []) as $documentData) {
            $document = ApiDocument::find()
                ->where([
                    'id' => ArrayHelper::getValue($documentData, 'id'),
                ])
                ->andWhere('sale_id IS NULL OR sale_id = :sale_id', [':sale_id' => $this->sale_id ? $this->sale_id : 0])
                ->one();

            if ($document === null) {
                continue;
            }
            $this->documents[] = $document;
            Yii::$app->session->set('count_document', $this->documents);
        }

        return $result;
    }


    public function validateApi()
    {
        return $this->validate();
    }

    public function saveApi()
    {
        if ($this->validation_method == self::VALIDATION_METHOD_SERIAL) {
            // Current serial number
            $currentSerialNumberValue = $this->getSerialNumberValue();
//            var_dump($currentSerialNumberValue);
//            exit;

            // Find new serial number id
            $this->serial_number_id = SerialNumber::find()
                ->notActivated()
                ->isAvailable()
                ->byNumberLike($currentSerialNumberValue)
                ->select('id')
                ->scalar();

            if ($this->serial_number_id != 0) {
                // Activate selected serial number
                SerialNumber::updateAll([
                    'activated_at' => new Expression('NOW()'),
                ], [
                    'id' => $this->serial_number_id,
                ]);
            }else{
                $this->custom_serial = $currentSerialNumberValue;
                $this->serial_number_id = null;
            }
        }

        $this->save(false);

        $postData = \Yii::$app->request->post('model', null);

        if(!isset($postData['documents'])) {
            return false;
        }

        $docId = [];
        
        foreach ($postData['documents'] as $doc) {
            $docId[] = $doc['id'];
        }

        SaleDocument::updateAll([
            'sale_id' => $this->sale_id,
            'sale_position_id' => null,
        ], [
            //'id' => ArrayHelper::getColumn($this->documents, 'id')
            'id' => $docId
        ]);

        return true;
    }



    /**
     * @return string
     */
    public function getSerialNumberValue()
    {
        if ($this->_serialNumberValue === null) {
            $serialNumber = $this->serialNumber;
            if ($serialNumber !== null) {
                return $serialNumber->number;
            }
            return '';
        }
        return $this->_serialNumberValue;
    }

    /**
     * @param string $serialNumberValue
     */
    public function setSerialNumberValue($serialNumberValue)
    {
        $this->_serialNumberValue = $serialNumberValue;
    }

    public function getDocuments()
    {
        return $this->hasMany(ApiDocument::class, ['sale_position_id' => 'id']);
    }

    public function setDocuments($documents)
    {
        $this->documents = $documents;
    }

    public function getSale()
    {
        return $this->hasOne(ApiSale::class, ['id' => 'sale_id']);
    }

    public function setSale($sale)
    {
        $this->sale = $sale;
    }


}