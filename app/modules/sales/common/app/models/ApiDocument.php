<?php

namespace modules\sales\common\app\models;
use modules\sales\common\models\SaleDocument;


/**
 * Class ApiDocument
 */
class ApiDocument extends SaleDocument
{
    public function validateApi()
    {
        return $this->validate();
    }
}