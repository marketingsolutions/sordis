<?php

namespace modules\sales\common\app\models;

use modules\sales\common\models\Sale;
use modules\sales\common\models\SaleDocument;
use modules\sales\common\models\SalePosition;
use modules\sales\common\models\SerialNumber;
use modules\sales\common\sales\statuses\Statuses;
use yii\helpers\ArrayHelper;


/**
 * Class ApiSale
 * @property ApiPosition[] $positions
 * @property ApiDocument[] $documents
 */
class ApiSale extends Sale
{
    public function fields()
    {
        return [
            'status',
            'sold_on_local',
            'positions',
        ];
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['status', 'required'],
            ['status', 'in', 'range' => [Statuses::DRAFT, Statuses::ADMIN_REVIEW]],
            ['sold_on_local', 'validateSoldOnLocal'],
        ]);
    }

    public function validateSoldOnLocal()
    {
        $soldOn = date_create($this->sold_on)->setTime(0,0,0);
        $today = date_create()->setTime(0,0,0);

        if ($soldOn > $today) {
            $this->addError('sold_on_local', 'Дата продажи не должна быть больше сегодняшего дня');
        }
    }

    public function getPositions()
    {
        return $this->hasMany(ApiPosition::class, ['sale_id' => 'id']);
    }

    public function setPositions($positions)
    {
        $this->positions = $positions;
    }

    public function loadApi($data)
    {
        $result = $this->load($data, '');
        $this->positions = [];
        foreach (ArrayHelper::getValue($data, 'positions', []) as $positionsData) {
            $this->positions[] = $position = new ApiPosition();
            $position->sale = $this;
            $position->sale_id = $this->id;
            $result = $position->loadApi($positionsData) && $result;
        }

        return $result;
    }

    public function validateApi()
    {
        $result = $this->validate();
        foreach ($this->positions as $model) {
            if ($model->validateApi() == false) {
                $this->addErrors($model->getFirstErrors());
                $result = false;
            }
        }
        return $result;
    }

    public function saveApi()
    {
        if ($this->isNewRecord == false) {
            SerialNumber::updateAll([
                'activated_at' => null
            ], [
                'id' => SalePosition::find()
                    ->select('serial_number_id')
                    ->where(['sale_id' => $this->id])
            ]);
            SalePosition::deleteAll(['sale_id' => $this->id]);
//            SaleDocument::updateAll(['sale_id' => null, 'sale_position_id' => null], ['sale_id' => $this->id]);
        }

        $this->save(false);

        foreach ($this->positions as $position) {
            $position->sale_id = $this->id;
            $position->saveApi();
        }

        $this->updateBonuses();
    }
}