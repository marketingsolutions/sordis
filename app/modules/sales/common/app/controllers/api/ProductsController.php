<?php

namespace modules\sales\common\app\controllers\api;

use modules\profiles\common\models\Profile;
use modules\sales\common\models\Product;
use modules\sales\frontend\base\ApiController;
use ms\loyalty\identity\phonesEmails\common\models\Identity;
use yii\web\NotFoundHttpException;
use Yii;


/**
 * Class ProductsController
 */
class ProductsController extends ApiController
{
    public function actionIndex($categoryId = null)
    {
        if (Yii::$app->user->identity && Yii::$app->user->identity instanceof Identity) {
            /** @var Profile $profile */
            $profile = Yii::$app->user->identity->profile;
            if ($action = $profile->dealer->action) {
                return $action->products;
            }
        }

        return Product::find()->orderBy(['name' => SORT_ASC])->all();
    }

    public function actionView($id)
    {
        return $this->loadModel($id);
    }

    public function actionCalculateBonuses($id, $quantity)
    {
        $product = $this->loadModel($id);

        return $product->bonusesCalculator->calculateForLocalQuantity($quantity);
    }

    /**
     * @param $id
     * @return Product
     * @throws NotFoundHttpException
     */
    protected function loadModel($id)
    {
        if (($model = Product::findOne($id)) === null) {
            throw new NotFoundHttpException();
        }

        return $model;
    }
}