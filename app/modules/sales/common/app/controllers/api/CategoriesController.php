<?php

namespace modules\sales\common\app\controllers\api;

use modules\sales\common\models\Category;
use modules\sales\frontend\base\ApiController;


/**
 * Class CategoriesController
 */
class CategoriesController extends ApiController
{
    public function actionIndex()
    {
        return Category::find()->all();
    }
}