<?php

namespace modules\sales\common\app\controllers\api;

use modules\sales\common\app\forms\SaleEditForm;
use modules\sales\common\app\models\ApiSale;
use modules\sales\common\app\SaleApplicationModule;
use modules\sales\frontend\base\ApiController;
use ms\loyalty\contracts\prizes\PrizeRecipientInterface;
use yii\web\NotFoundHttpException;


/**
 * Class SalesController
 *
 * @property SaleApplicationModule $module
 */
class SalesController extends ApiController
{
    protected function verbs()
    {
        return [
            'post' => ['create', 'update']
        ];
    }

    public function actionNew()
    {
        return new SaleEditForm(new ApiSale(), \Yii::$container->get(PrizeRecipientInterface::class));
    }

    public function actionView($id)
    {
        return new SaleEditForm($this->findModel($id));
    }

    /**
     * Creates a new Sale model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        if ($this->module->allowCreation == false) {
            throw new NotFoundHttpException();
        }

        $model = new SaleEditForm(new ApiSale(), \Yii::$container->get(PrizeRecipientInterface::class), [
            'useValidationRules' => $this->module->useValidationRules,
        ]);

        $model->loadApi(\Yii::$app->request->post());

        if ($model->process()) {
            return ['url' => call_user_func($this->module->afterSaleProcess, $model->sale)];
        }

        return $model;
    }

    /**
     * Updates an existing Sale model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = new SaleEditForm($this->findModel($id), null, [
            'useValidationRules' => $this->module->useValidationRules,
        ]);

        $model->loadApi(\Yii::$app->request->post());

        if ($model->process()) {
            return ['url' => call_user_func($this->module->afterSaleProcess, $model->sale)];
        }

        return $model;
    }

    /**
     * Finds the Sale model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApiSale the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if ($this->module->findSale !== null) {
            return call_user_func($this->module->findSale, $id);
        }

        /** @var PrizeRecipientInterface $recipient */
        $recipient = \Yii::$container->get(PrizeRecipientInterface::class);

        /** @var ApiSale $model  */
        if (($model = ApiSale::findOne(['id' => $id, 'recipient_id' => $recipient->getRecipientId()])) === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }
}