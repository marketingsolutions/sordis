
var saleEditApplication = (function () {
    var settings = {
        api: {
            products: '/api/products',
            categories: '/api/categories',
            units: '/api/units',
            files: '/api/files',
            sales: '/api/sales'
        },
        apiUrlPrefix: '',
        id: null,
        saveUrl: ''
    };

    function apiUrl(api, action) {
        action = action || '';
        return settings.apiUrlPrefix + api + action;
    }

    angular

        .module('SaleEditApplication', ['ngFileUpload'])

        .factory('API', ['$http', 'Upload', function ($http, Upload) {
            return {
                createSale: function () {
                    return $http.get(apiUrl(settings.api.sales, '/new'));
                },
                loadSale: function (id) {
                    return $http.get(apiUrl(settings.api.sales, '/view'), {params: {id: id}});
                },
                loadProduct: function (id) {
                    return $http.get(apiUrl(settings.api.products, '/view'), {params: {id: id}});
                },
                loadProducts: function (category_id) {
                    return $http.get(apiUrl(settings.api.products), {params: {categoryId: category_id}});
                },
                loadCategories: function () {
                    return $http.get(apiUrl(settings.api.categories));
                },
                loadUnit: function (id) {
                    return $http.get(apiUrl(settings.api.units, '/view'), {params: {id: id}})
                },
                calculateBonuses: function (product_id, quantity) {
                    return $http.get(apiUrl(settings.api.products, '/calculate-bonuses'), {
                        params: {
                            id: product_id,
                            quantity: quantity
                        }
                    })
                },
                uploadDocument: function (document) {
                    return Upload.upload({
                        url: apiUrl(settings.api.files, '/upload'),
                        sendFieldsAs: 'form',
                        file: document
                    })
                }
            };
        }])

        .controller('SaleEdit', ['$scope', '$http', 'Upload', 'API',
            function ($scope, $http, Upload, API) {
                function Position(position) {

                    var products = [],
                        product = {},
                        unit = {},
                        _self = this;

                    position = position || {};

                    this.id = position.id;
                    this.category_id = null;
                    this.product_id = position.product_id || null;
                    this.quantityLocal = position.quantityLocal || 1;
                    this.serialNumberValue = position.serialNumberValue || '';
                    this.bonuses = position.bonuses || 0;
                    this.documents = [];
                    this.validation_method = position.validation_method || null;

                    angular.forEach(position.documents || [], function (value) {
                        _self.documents.push(new Document(value));
                    });

                    this.getProducts = function () {
                        return products;
                    };

                    this.getProduct = function () {
                        return product;
                    };

                    this.getUnit = function () {
                        return unit;
                    };

                    this.loadProducts = function () {
                        return API.loadProducts(_self.category_id)
                            .success(function (data) {
                                products = data;
                                if (products.length == 1) {
                                    _self.product_id = products[0].id
                                }
                            });
                    };

                    this.loadProduct = function () {
                        return API.loadProduct(_self.product_id)
                            .success(function (data) {
                                product = data;
                            });
                    };

                    this.loadUnit = function () {
                        return API.loadUnit(product.unit_id)
                            .success(function (data) {
                                unit = data;
                            });
                    };

                    this.calculateBonuses = function () {
                        return API.calculateBonuses(_self.product_id, _self.quantityLocal)
                            .success(function (data) {
                                _self.bonuses = data;
                            });
                    };

                    this.changeCategory = function () {
                        _self.product_id = null;
                        _self.loadProducts();
                    };

                    this.changeProduct = function () {
                        _self.loadProduct()
                            .then(_self.loadUnit)
                            .then(_self.calculateBonuses)
                    };

                    this.changeQuantity = function () {
                        _self.calculateBonuses();
                    };

                    // this.addDocument = function (document) {
                    //     this.documents.push(document);
                    // };
                    //
                    // this.removeDocument = function (document) {
                    //     var index = this.documents.indexOf(document);
                    //     if (index > -1) {
                    //         this.documents.splice(index, 1);
                    //     }
                    // };

                    if (this.product_id) {
                        this.loadProduct()
                            .then(function () {
                                _self.category_id = product.category_id;
                            })
                            .then(_self.loadProducts)
                            .then(_self.loadUnit);
                    }
                }

                function Document(document) {
                    document = document || {};
                    var _self = this;

                    this.id = document.id || null;
                    this.name = document.name || '';
                    this.original_name = document.original_name || '';
                    this.uploaded = this.id != null;
                    this.isImage = document.isImage || false;

                    this.upload = function () {
                        if (!this.document || this.document.length == 0) {
                            return;
                        }

                        this.uploading = true;
                        this.uploaded = false;
                        this.errors = [];

                        API.uploadDocument(this.document)
                            .success(function (info) {
                                _self.uploading = false;
                                _self.uploaded = true;
                                _self.id = info.id;
                                _self.name = info.name;
                                _self.original_name = info.original_name;
                                _self.isImage = info.isImage;
                            })
                            .error(function (info) {
                                _self.uploading = false;
                                _self.uploaded = false;
                                _self.errors = info;
                            });
                    };
                }

                function Sale(data) {
                    var _self = this;
                    data = data || {};

                    this.status = data.status || '';
                    this.sold_on_local = data.sold_on_local || '';
                    this.positions = [];

                    angular.forEach(data.positions || [], function (value) {
                        _self.positions.push(new Position(value));
                    });
                }

                function Model(data) {
                    data = data || {};
                    var _self = this;
                    this.documents = [];
                    this.sale = new Sale(data.sale || []);

                    this.addDocument = function (document) {
                        this.documents.push(document);
                    };

                    this.removeDocument = function (document) {
                        var index = this.documents.indexOf(document);
                        if (index > -1) {
                            this.documents.splice(index, 1);
                        }
                    };

                    this.addPosition = function (position) {
                        this.sale.positions.push(position);
                    };

                    this.removePosition = function (position) {
                        var index = this.sale.positions.indexOf(position);
                        if (index > -1) {
                            this.sale.positions.splice(index, 1);
                        }
                    };
                }

                $scope.model = null;
                $scope.global_validation_method = null;
                $scope.categories = [];
                $scope.disabled = false;
                $scope.errors = {};

                (settings.id ? API.loadSale(settings.id) : API.createSale())
                    .success(function (sale) {
                        API
                            .loadCategories()
                            .success(function (data) {
                                $scope.categories = data;
                            })
                            .then(function () {
                                $scope.model = new Model(sale);
                                if ($scope.model.sale.positions.length > 0) {
                                    $scope.global_validation_method =  $scope.model.sale.positions[0].validation_method;
                                }
                            });
                    })
                    .error(function () {
                        //window.location = '/';
                    });

                $scope.addPosition = function () {
                    var position = new Position();
                    position.validation_method = $scope.global_validation_method;

                    $scope.model.addPosition(position);
                    if ($scope.categories.length == 1) {
                        position.category_id = $scope.categories[0].id;
                        position.changeCategory();
                    }
                };

                $scope.removePosition = function (position) {
                    $scope.model.removePosition(position)
                };

                // $scope.addDocument = function (position) {
                //     var document = new Document();
                //     position.addDocument(document);
                // };
                //
                // $scope.removeDocument = function (position, document) {
                //     position.removeDocument(document)
                // };
                $scope.addDocument = function() {
                    var document = new Document();
                    $scope.model.addDocument(document);
                };

                $scope.removeDocument = function(document) {
                    $scope.model.removeDocument(document)
                };

                $scope.save = function (status) {
                    $scope.disabled = true;
                    $scope.model.sale.status = status;

                    $http.post(apiUrl(settings.api.sales, settings.saveUrl), {model: $scope.model}, {params: {id: settings.id}})
                        .success(function (result) {
                            window.location = result.url;
                        })
                        .error(function (errors) {
                            $scope.errors = errors;
                            $scope.disabled = false;
                        })
                };

                $scope.setGlobalValidationMethod = function (method) {
                    //$scope.global_validation_method = method;
                    $scope.global_validation_method = 'serial';
                    angular.forEach($scope.model.sale.positions, function (position) {
                        position.validation_method = method;
                    });
                };
            }
        ]);

    return {
        configure: function (config) {
            angular.extend(settings, config);
        }
    };
})();