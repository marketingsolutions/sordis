<?php

namespace modules\sales\common\app\forms;

use Carbon\Carbon;
use modules\profiles\common\models\Dealer;
use modules\profiles\common\models\Profile;
use modules\sales\common\app\models\ApiSale;
use modules\sales\common\models\SalePosition;
use modules\sales\common\models\SaleValidationRule;
use modules\sales\common\sales\statuses\Statuses;
use ms\loyalty\contracts\prizes\PrizeRecipientInterface;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use modules\sales\common\models\SerialNumber;
use Yii;
use yz\admin\models\User;
use modules\sales\backend\rbac\Rbac;

/**
 * Class SaleEditForm
 */
class SaleEditForm extends Model
{
    /**
     * @var ApiSale
     */
    public $sale;
    /**
     * @var PrizeRecipientInterface
     */
    public $recipient;
    /**
     * @var bool
     */
    public $useValidationRules = true;

    public function __construct(ApiSale $sale, PrizeRecipientInterface $recipient = null, $config = [])
    {
        $this->sale = $sale;
        $this->recipient = $recipient;

        if ($this->recipient === null && $this->sale->isNewRecord) {
            throw new InvalidParamException('You did not provide recipient info for new sale');
        }

        parent::__construct($config);
    }

    public function init()
    {
        if ($this->sale->isNewRecord) {
            $this->sale->sold_on = Carbon::today()->toDateString();
        }
    }

    public function fields()
    {
        return [
            'sale',
        ];
    }

    public function rules()
    {
        return [
        ];
    }

    /* Проверяем серийный номер на наличие в БД */
    public function isSerialNumber($serial)
    {
        return SerialNumber::findOne(['number' => $serial]);
    }

    public function afterValidate()
    {
        parent::afterValidate();

        if (count($this->sale->positions) == 0) {
            $this->addError('points', 'Вы должны добавить хотя бы одну позицию');
        }

        if ($this->sale->validateApi() == false) {
            $this->addErrors($this->sale->getFirstErrors());
        }

        $existedSerials = [];
        $nonSerial = 0;  //Счетчик для отсутствующих серийников
        foreach ($this->sale->positions as $position) {

            /*Делаем проверку на наличие сирийника в БД*/
            if ($this->isSerialNumber($position->serialNumberValue) == null && $position->serialNumberValue != "") {
                $nonSerial++;
            }
            if ($position->validation_method != SalePosition::VALIDATION_METHOD_SERIAL) {
                continue;
            }
            if ($position->hasErrors()) {
                continue;
            }
            if (in_array($position->serialNumberValue, $existedSerials)) {
                $this->addError('sales', 'Нельзя использовать один и тот же серийный номер несколько раз');
            }
            $existedSerials[] = $position->serialNumberValue;

        }

        if ($this->useValidationRules) {
            /** @var SaleValidationRule[] $validationRules */
            $validationRules = SaleValidationRule::find()->all();

            foreach ($validationRules as $rule) {
                if ($rule->evaluator->evaluate($this->sale, $this->sale->positions, $this->sale->documents) == true) {
                    continue;
                }

                $this->addError('sale', $rule->error);
            }
        }
    }

    public function loadApi($data)
    {
        $result = $this->load($data, 'model');

        if ($this->recipient) {
            $this->sale->recipient_id = $this->recipient->getRecipientId();
        }
        $result = $this->sale->loadApi(ArrayHelper::getValue($data, 'model.sale')) || $result;

        return $result;
    }

    public function process()
    {
        if ($this->validateAll() == false) {
            return false;
        }

        $transaction = \Yii::$app->db->beginTransaction();

        $this->sale->saveApi();

        $this->checkAutoApprove();

        $transaction->commit();

        return true;
    }

    public function validateAll()
    {
        $result = true;
        $result = $this->validate() && $result;
        $result = $this->sale->validateApi() && $result;

        /** @var Profile $profile */
        $profile = Profile::findOne($this->sale->recipient_id);

        if (!empty($profile->parcel_blocked_at)) {
            $this->addError('sales', 'К сожалению, данная учетная запись не подтверждена администратором. До подтверждения оформлять продажи запрещено.');
            $result = false;
        }

        return $result;
    }

    /**
     * Проверка принадлежности данного серийника торговой точке, к которой принадлежит клиент
     *
     * @param $serial_number
     * @return bool
     */
    public function isBelongSerialSalePoint($serial_number_id)
    {
        //        $model = Profile::find()
        //            ->select('yz_dealers.dealer_name as dealer_name')
        //            ->leftJoin('yz_dealers' , 'yz_dealers.id = yz_profiles.dealer_id')
        //            ->where(['yz_profiles.identity_id' => Yii::$app->user->identity->id])
        //            ->asArray()
        //            ->one();
        $serialModel = SerialNumber::find()
            ->select('sale_point_name')
            ->where(['id' => $serial_number_id])
            ->asArray()
            ->one();
        if ($serialModel['sale_point_name']) {
            return true;
        }
        else {
            return false;
        }
    }

    private function checkAutoApprove()
    {
        if ($this->sale->status !== Statuses::ADMIN_REVIEW) {
            return;
        }

        foreach ($this->sale->positions as $position) {
            if ($position->validation_method == SalePosition::VALIDATION_METHOD_DOCUMENTS) {
                return;
            }
        }

        /*Проверяем принадлежит ли товар данной торговой точке, если нет, то ставим статус уточнения данной продаже*/
        $i = 0;
        foreach ($this->sale->positions as $position) {
            if (!$this->isBelongSerialSalePoint($position->serial_number_id)) {
                $i++;
            }
        }

        if ($i == 0) {
            $this->sale->status = Statuses::APPROVED;
        }
        else {
            $this->sale->status = Statuses::ADMIN_REVIEW;
        }

        $this->sale->updateAttributes(['status']);
    }
}