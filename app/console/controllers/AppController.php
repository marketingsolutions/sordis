<?php

namespace console\controllers;

use ms\loyalty\catalog\common\cards\CardItem;
use ms\loyalty\catalog\common\models\CatalogOrder;
use ms\loyalty\catalog\common\models\OrderedCard;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Application configuration controller
 *
 * @author Eugene Terentev <eugene@terentev.net>
 */
class AppController extends Controller
{
    public $writablePaths = [
        '@common/runtime',
        '@frontend/runtime',
        '@frontend/web/assets',
        '@backend/runtime',
        '@backend/web/assets',
    ];
    public $executablePaths = [
        '@base/yii',
    ];
    public $generateKeysPaths = [
        '@base/.env'
    ];

    public function actionSetup()
    {
        $this->setWritable($this->writablePaths);
        $this->setExecutable($this->executablePaths);
        $this->setGeneratedKey($this->generateKeysPaths);
    }

    public function setWritable($paths)
    {
        foreach ($paths as $writable) {
            $writable = Yii::getAlias($writable);
            Console::output("Setting writable: {$writable}");
            @chmod($writable, 0777);
        }
    }

    public function setExecutable($paths)
    {
        foreach ($paths as $executable) {
            $executable = Yii::getAlias($executable);
            Console::output("Setting executable: {$executable}");
            @chmod($executable, 0755);
        }
    }

    public function setGeneratedKey($paths)
    {
        foreach ($paths as $file) {
            $file = Yii::getAlias($file);
            Console::output("Generating keys in {$file}");
            $content = file_get_contents($file);
            $content = preg_replace_callback('/<generated_key>/', function () {
                $length = 32;
                $bytes = openssl_random_pseudo_bytes(32, $cryptoStrong);
                return strtr(substr(base64_encode($bytes), 0, $length), '+/', '_-');
            }, $content);
            file_put_contents($file, $content);
        }
    }

    public function actionEmail()
    {
        $mailbox = new \PhpImap\Mailbox('{imap.yandex.ru:993/imap/ssl}INBOX', 'b24leads@msforyou.ru', 'leadsb24');

        $mailsIds = $mailbox->searchMailbox('ALL');
        if (!$mailsIds) {
            die('Mailbox is empty');
        }

        foreach ($mailsIds as $mailId) {
            $mail = $mailbox->getMail($mailId);
            $fromEmail = $mail->fromAddress;
            $messageId = $mail->messageId;
            $messageHtml = $mail->textHtml;

            preg_match_all("|(.*)Имя: (.*)<(.*)|U", $messageHtml, $out, PREG_PATTERN_ORDER);
            $name = $out[2][0];
            preg_match_all("|(.*)Телефон: (.*)<(.*)|U", $messageHtml, $out, PREG_PATTERN_ORDER);
            $phone = $out[2][0];
            preg_match_all("|(.*)Email: (.*)<(.*)|U", $messageHtml, $out, PREG_PATTERN_ORDER);
            $email = $out[2][0];
            preg_match_all("|(.*)Адрес страницы: (.*)<(.*)|U", $messageHtml, $out, PREG_PATTERN_ORDER);
            $url = $out[2][0];
            preg_match_all("|(.*)Страница: (.*)<(.*)|U", $messageHtml, $out, PREG_PATTERN_ORDER);
            $source = $out[2][0];

            echo implode(', ', [$fromEmail, $messageId, $name, $phone, $email, $url, $source]) . PHP_EOL;
        }
    }
}