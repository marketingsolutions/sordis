<?php

return [
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'console\controllers',
    'controllerMap' => [
        'migrate' => [
            'class' => dmstr\console\controllers\MigrateController::class,
            'migrationTable' => '{{%migrations}}',
            'migrationPath' => '@migrations',
        ],
    ],
    'components' => [
        'schedule' => [
            'class' => \marketingsolutions\scheduling\Schedule::class,
        ],
    ],
    'modules' => [
        'mailing' => [
            'class' => \yz\admin\mailer\console\Module::class,
        ],
        'catalog' => [
            'class' => \ms\loyalty\catalog\console\Module::class,
        ],
        'sales' => [
            'class' => \modules\sales\console\Module::class,
        ],
        'payments' => [
            'class' => ms\loyalty\prizes\payments\console\Module::class,
        ],
        'rsbcards' => [
            'class' => \ms\loyalty\rsb\cards\console\Module::class,
        ],
        'checker' => [
            'class' => \ms\loyalty\checker\console\Module::class,
        ],
        'social' => [
            'class' => modules\social\console\Module::class,
        ],
        'bonuses' => [
            'class' => modules\bonuses\console\Module::class,
        ],
        'ftp' => [
            'class' => modules\ftp\console\Module::class,
        ],
        'polls' => [
            'class' => modules\polls\console\Module::class,
        ],
        'profiles' => [
            'class' => modules\profiles\console\Module::class,
        ],
    ],
    'params' => [
        'yii.migrations' => [
            '@modules/profiles/migrations',
            '@modules/sales/migrations',
            '@modules/courses/migrations',
            '@modules/manual/migrations',
            '@modules/mobile/migrations',
            '@modules/news/migrations',
            '@modules/polls/migrations',
            '@modules/bonuses/migrations',
            '@modules/sms/migrations',
            '@modules/pictures/migrations',
            '@modules/rpc/migrations',
            '@modules/social/migrations',
            '@modules/calc/migrations',
            '@modules/logs/migrations',
        ],
    ],
];
