<?php
/**
 * @var \marketingsolutions\scheduling\Schedule $schedule
 */

/**
 * Send emails from admin panel
 */
$schedule->command('mailing/mails/send');
$schedule->command('mailing/mailer/send')->everyTenMinutes();

/** Sales */
$schedule->command('sales/bonuses/pay')->everyMinute();

/** Bonuses add & pay */
$schedule->command('bonuses/pay-bonuses/index')->everyMinute();
$schedule->command('bonuses/sms-bonuses/send')->everyNMinutes(5);
$schedule->command('bonuses/process-xml-upload/index')->everyNMinutes(10);
$schedule->command('bonuses/add-bonuses/index')->everyNMinutes(5);

/**Targets upload*/
$schedule->command('social/process-xml-upload/index')->everyNMinutes(10);
/** Certificates */
$schedule->command('catalog/zakazpodarka-orders/create-soap --method=processed')->cron('30 * * * * *');
$schedule->command('catalog/zakazpodarka-orders/check-status --interval=600');
$schedule->command('catalog/card-items/send-to-user')->everyTenMinutes();

/** Payments */
$schedule->command('payments/process/index')->everyMinute();
$schedule->command('payments/process/check')->everyNMinutes(3);

$schedule->command('polls/add-new-poll/index')->everyNMinutes(3);

/*FTP exchange*/
/**Получаем файлы с бонусами, обрабатываем и отправляем в архив */
$schedule->command('ftp/ftp-exchange/get-bonus-xml-file')->cron('0 * * * *');
/**Получаем файлы с опросами, обрабатываем и отправляем в архив*/
$schedule->command('ftp/ftp-exchange/get-polls-xml-file')->cron('0 * * * *');
/** Получаем файлы со списком РТТ, Дистров и Менеджеров ТО*/
$schedule->command('ftp/ftp-exchange/get-rtt-distrib-xml-file')->cron('0 0 * * *');
/**Получаем файлы с целями, обрабатываем и отправляем в архив*/
$schedule->command('ftp/ftp-exchange/get-target-xml-file')->cron('0 * * * *');
/**Отправляем потраченные бонусы по ftp*/
$schedule->command('ftp/ftp-exchange/put-bonus-ftp-file')->cron('0 0 * * *');
/**Отправляем пройденные опросы по ftp*/
$schedule->command('ftp/ftp-exchange/put-polls-ftp-file')->cron('0 0 * * *');
/**Отправляем пройденные Призовые (социальные) акции по ftp*/
$schedule->command('ftp/ftp-exchange/put-social-ftp-file')->cron('0 0 * * *');
/**Отправляем новых зарегенных участников ТТ*/
$schedule->command('ftp/ftp-exchange/put-new-user-tt')->cron('0 */1 * * *');
/**Отправляем новых зарегенных участников дистрибьюторов*/
$schedule->command('ftp/ftp-exchange/put-new-user-distrib')->cron('0 */1 * * *');

/**Обновление торговых точек, Дистрибьюторов и Менеджеров ТО*/
$schedule->command('sales/process-xml-rtt-distrib/index')->cron('0 0 * * *');

/**Скачивание списка РТТ в xml с привязкой к дистру */
$schedule->command('sales/process-xml-rtt-distrib/download-tt')->everyMinute();

/** Check-balance */
// $schedule->command('checker/check/check-card')->cron('00 08 * * *');