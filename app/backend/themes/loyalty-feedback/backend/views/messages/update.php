<?php

use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;

/**
 * @var yii\web\View $this
 * @var ms\loyalty\feedback\common\models\Message $model
 * @var ms\loyalty\feedback\common\models\Reply $reply
 * @var \ms\loyalty\feedback\common\models\Comment $comment
 */
$this->title = \Yii::t('admin/t', 'Update {item}', ['item' => ms\loyalty\feedback\common\models\Message::modelTitle()]);
$this->params['breadcrumbs'][] = ['label' => ms\loyalty\feedback\common\models\Message::modelTitlePlural(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;
\modules\profiles\common\models\Profile::isReadMessage(\Yii::$app->request->get('id'), \Yii::$app->user->id);
?>
<div class="message-update">


    <div class="text-right">
        <?php  Box::begin() ?>
        <?=  ActionButtons::widget([
            'order' => [['index', 'update', 'return']],
            'addReturnUrl' => false,
        ]) ?>
        <?php  Box::end() ?>
    </div>

    <?php echo $this->render('_form', [
        'model' => $model,
        'reply' => $reply,
        'comment' => $comment,
    ]); ?>

</div>
