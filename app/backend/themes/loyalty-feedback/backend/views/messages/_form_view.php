<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yz\admin\widgets\ActiveForm;
use yz\admin\widgets\FormBox;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var ms\loyalty\feedback\common\models\Message $model
 * @var ms\loyalty\feedback\common\models\Reply $reply
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php $box = FormBox::begin(['cssClass' => 'message-form box-primary', 'title' => '']) ?>
<?php $form = ActiveForm::begin(); ?>

<?php $box->beginBody() ?>
<div class="panel panel-default">
    <div class="panel-heading">Вопрос участника</div>
    <div class="panel-body">
        <?= $form->field($model, 'name')->staticInput() ?>
        <?= $form->field($model, 'phone_mobile_local')->staticInput() ?>
        <?= $form->field($model, 'email')->staticInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'content')->staticInput(['format' => 'nText']) ?>
        <div class="row">
            <label class="control-label col-sm-2" for="message-content">Прикрепленные документы</label>
            <div class="col-sm-8">
                <?= \ms\files\attachments\common\widgets\FileAttachmentsView::widget([
                    'attachmentsQuery' => $model->getAttachedFiles(),
                    'type' => 'userFile',
                ]) ?>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Ответы участнику</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-offset-2 col-sm-8">
                <table class="table">
                    <tr>
                        <th class="col-sm-2">Дата ответа</th>
                        <th class="col-sm-2">Администратор</th>
                        <th class="col-sm-10">Ответ</th>
                        <th class="col-sm-2">Файл</th>
                    </tr>
                    <?php foreach ($model->replies as $previousReply): ?>
                        <tr>
                            <td><?= Yii::$app->formatter->asDatetime($previousReply->created_at) ?></td>
                            <td><?= Html::encode($previousReply->adminUser->name) ?></td>
                            <td><?= Yii::$app->formatter->asHtml($previousReply->reply) ?></td>
                            <td>
                                <table>
                                    <?php foreach ($previousReply->getAttachedFiles()->all() as $attach): ?>
                                        <tr><td><a href="<?= Url::to(['/files-attachments/files/download', 'id' => $attach->id]) ?>">
                                            <?= Html::encode($attach->original_name) ?>
                                        </td></tr></a>
                                    <?php endforeach ?>
                                </table>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Комментарии</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-offset-2 col-sm-8">
                <table class="table">
                    <tr>
                        <th class="col-sm-2">Дата комментария</th>
                        <th class="col-sm-2">Администратор</th>
                        <th class="col-sm-10">Ответ</th>
                    </tr>
                    <?php foreach ($model->comments as $previousComment): ?>
                        <tr>
                            <td><?= Yii::$app->formatter->asDatetime($previousComment->created_at) ?></td>
                            <td><?= Html::encode(ArrayHelper::getValue($previousComment, 'adminUser.name')) ?></td>
                            <td><?= Yii::$app->formatter->asHtml($previousComment->comment) ?></td>
                        </tr>
                    <?php endforeach ?>
                </table>
            </div>
        </div>
    </div>
</div>

<?php $box->endBody() ?>

<?php ActiveForm::end(); ?>

<?php FormBox::end() ?>
