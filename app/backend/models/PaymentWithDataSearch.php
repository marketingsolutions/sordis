<?php

namespace backend\models;
use modules\profiles\common\models\Profile;
use ms\loyalty\prizes\payments\backend\models\PaymentSearch;
use yii\db\ActiveQuery;
use yz\admin\search\WithExtraColumns;


/**
 * Class PaymentWithDataSearch
 */
class PaymentWithDataSearch extends PaymentSearch
{
    use WithExtraColumns, WithProfileFieldsSearch {
        WithProfileFieldsSearch::extraColumns insteadOf WithExtraColumns;
    }

    protected static function alias()
    {
        return 'payment';
    }


    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        return PaymentWithDataSearch::find();
    }

    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['id' => 'recipient_id']);
    }
}