<?php

namespace backend\models;
use yii\db\ActiveQuery;


/**
 * Trait WithProfileFieldsSearch
 */
trait WithProfileFieldsSearch
{
    protected static function extraColumns()
    {
        return [
            'profile__full_name',
            'profile__phone_mobile',
            'dealer__name',
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), static::extraColumns());
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'profile__full_name' => 'Участник',
            'profile__phone_mobile' => 'Телефон участника',
            'dealer__name' => 'Дилер',
        ]);
    }


    /**
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()
            ->select(static::selectWithExtraColumns([static::alias().'.*']))
            ->joinWith('profile profile')
            ->joinWith('profile.dealer dealer');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [static::extraColumns(), 'safe'],
        ]);
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        parent::prepareFilters($query);
        static::filtersForExtraColumns($query);
    }

    protected static function alias()
    {
        return 'model';
    }
}