<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var string $content
 */

/**
 * By default this layout simply renders layout
 * of the admin module. But if you need some customizations
 * you can overwrite this file
 */
$this->registerMetaTag([
    'name' => 'robots',
    'content' => 'noindex,nofollow'
]);

$js=<<<JS

var currentLocation = window.location.pathname;
if(currentLocation == "/catalog/ordered-cards/index"){
    //Скрываем колонки в разделе заказанные сертификаты
    var num=12;
    var num2=18;
    $(".table-bordered thead th:nth-child("+num+"), .table-bordered tbody td:nth-child("+num+")").hide();
    $(".table-bordered thead th:nth-child("+num2+"), .table-bordered tbody td:nth-child("+num2+")").hide();
}


JS;
$this->registerJs($js);
?>

<?php $this->beginBlock('header-extra-logo') ?>
    <!-- Client logo here -->
<?php $this->endBlock() ?>
<?php $this->beginContent('@ms/loyalty/theme/backend/views/layouts/main.php'); ?>
<?= $content ?>
<?php $this->endContent() ?>