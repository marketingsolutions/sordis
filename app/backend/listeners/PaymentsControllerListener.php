<?php

namespace backend\listeners;

use backend\models\PaymentWithDataSearch;
use ms\loyalty\prizes\payments\backend\models\PaymentSearch;
use yii\base\Event;
use yz\admin\grid\GridView;


/**
 * Class PaymentsControllerListener
 */
class PaymentsControllerListener
{
    use WithProfileFields;

    public static function whenBeforeAction(Event $event)
    {
        \Yii::$container->set(
            PaymentSearch::class,
            PaymentWithDataSearch::class
        );

        Event::on(
            GridView::class,
            GridView::EVENT_SETUP_GRID,
            [static::class, 'whenSetupGrid']
        );
    }

    public static function whenSetupGrid(Event $event)
    {
		/** @var GridView $grid */
		$grid = $event->sender;

		$grid->columns = array_merge(array_slice($grid->columns, 0, 2), [
			'profile__full_name',
			'profile__phone_mobile',
		],  array_slice($grid->columns, 2, 1),
            array_slice($grid->columns, 3, 1),
            array_slice($grid->columns, 4, 1),
            array_slice($grid->columns, 5, 1),
            array_slice($grid->columns, 7, 1),
            array_slice($grid->columns, 8, 1),
           // array_slice($grid->columns, 9, 1),
            array_slice($grid->columns, 10, 1),
            array_slice($grid->columns, 11, 1),
            array_slice($grid->columns, 12)
            //array_slice($grid->columns, 8)
        );

    }
}