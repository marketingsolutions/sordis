<?php

namespace backend\listeners;


/**
 * Trait WithProfileFields
 */
trait WithProfileFields
{
    public static function profileFields()
    {
        return [
            'profile__full_name',
            'profile__phone_mobile',
            'dealer__name',
        ];
    }
}