<?php

namespace backend\reports;

use modules\profiles\common\models\Dealer;
use modules\profiles\common\models\Profile;
use modules\sales\common\models\Product;
use modules\sales\common\models\Sale;
use modules\sales\common\models\SalePosition;
use ms\loyalty\reports\contracts\base\ReportInterface;
use ms\loyalty\reports\contracts\ByQueryInterface;
use ms\loyalty\reports\contracts\ReportSearchModelInterface;
use ms\loyalty\reports\contracts\types\TableReportInterface;
use ms\loyalty\reports\support\SelfSearchModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yz\admin\behaviors\DateRangeFilteringBehavior;
use yz\admin\grid\filters\ActiveTextInputFilter;
use yz\admin\grid\filters\DateRangeFilter;
use yz\admin\search\SearchModelInterface;


/**
 * Class SalesByDealerReport
 */
class SalesByDealerReport extends Model implements
    ReportInterface,
    TableReportInterface,
    ByQueryInterface,
    SearchModelInterface,
    ReportSearchModelInterface
{
    use SelfSearchModel;

    public $dealer__name;
    public $product__name;
    public $sold_on;
    public $sold_on_range;
    public $region__name;
    public $name;

    public function init()
    {
        ini_set('memory_limit', '512M');
        parent::init();
    }


    public function rules()
    {
        return [
            ['dealer__name', 'safe'],
            ['product__name', 'safe'],
            ['sold_on_range', 'safe'],
            ['name', 'string'],
        ];
    }

    public function behaviors()
    {
        return [
            'filterRange' => [
                'class' => DateRangeFilteringBehavior::class,
                'appendFilters' => false,
                'attributes' => [
                    'sold_on',
                ],
                'initAttribute' => function ($attribute) {
                    if ($attribute == 'sold_on_start') {
                        return Sale::find()->min('sold_on');
                    } else {
                        return Sale::find()->max('sold_on');
                    }
                }
            ]
        ];
    }

    public function prepareDataProvider(Query $query)
    {
        $joiningSort = [];
        foreach ($this->getJoiningFields() as $field) {
            $joiningSort[$field] = [
                'asc' => [$field => SORT_ASC],
                'desc' => [$field => SORT_DESC],
            ];
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => array_merge([
                    'dealer__name' => [
                        'asc' => ['dealer.name' => SORT_ASC],
                        'desc' => ['dealer.name' => SORT_DESC],
                    ],
                    'product__name' => [
                        'asc' => ['product.name' => SORT_ASC],
                        'desc' => ['product.name' => SORT_DESC],
                    ],
                    'quantity_total' => [
                        'asc' => ['quantity_total' => SORT_ASC],
                        'desc' => ['quantity_total' => SORT_DESC],
                    ]
                ], $joiningSort),
                'defaultOrder' => [
                    'quantity_total' => SORT_DESC,
                ]
            ]
        ]);

        return $dataProvider;
    }


    public function prepareFilters(Query $query)
    {
        $query->andFilterWhere(['like', 'dealer.name', $this->name]);
    }


    /**
     * @return Query
     */
    public function query()
    {
        $query = (new Query())
            ->select([
                'dealer__id' => 'dealer.id',
                'dealer__name' => 'dealer.name',
            ])
            ->from([
                'dealer' => Dealer::tableName(),
            ]);

        /** @var Product[] $products */
        $products = Product::find()->all();
        $totalQuantityFields = [];
        $data = \Yii::$app->request->get('SalesByDealerReport');

        if (!empty($data['sold_on_range'])) {
            list ($from, $to) = explode(' - ', $data['sold_on_range']);
            $from = (new \DateTime($from))->format('Y-m-d 00:00:00');
            $to = (new \DateTime($to))->format('Y-m-d 23:59:59');

            foreach ($products as $product) {
                $alias = 'salePosition_' . $product->id;
                $query->leftJoin([
                    $alias => (new Query())
                        ->select([
                            'dealer_id' => 'profile.dealer_id',
                            'quantity_total' => 'SUM(salePosition.quantity)',
                        ])
                        ->from([
                            'salePosition' => SalePosition::tableName(),
                            'sale' => Sale::tableName(),
                            'profile' => Profile::tableName(),
                        ])
                        ->andWhere([
                            'salePosition.product_id' => $product->id,
                        ])
                        ->andWhere('salePosition.sale_id = sale.id')
                        ->andWhere('sale.recipient_id = profile.id')
                        ->andWhere(['>', 'sale.created_at', $from])
                        ->andWhere(['<', 'sale.created_at', $to])
                        ->andFilterWhere(['between', 'DATE(sale.sold_on)', $this->sold_on_start, $this->sold_on_end])
                        ->groupBy([
                            'profile.dealer_id'
                        ])
                ], "{$alias}.dealer_id = dealer.id");

                $query->addSelect([
                    "{$alias}__quantity_total" => "{$alias}.quantity_total",
                    "qt" => "{$alias}.quantity_total",
                ]);
                $totalQuantityFields[] = "{$alias}.quantity_total";
            }

            $query->addSelect([
                'quantity_total' => new Expression(implode('+', array_map(function ($field) {
                    return "IF({$field} IS NULL, 0, {$field})";
                }, $totalQuantityFields))),
            ]);
        }
        else {
            foreach ($products as $product) {
                $alias = 'salePosition_' . $product->id;
                $query->leftJoin([
                    $alias => (new Query())
                        ->select([
                            'dealer_id' => 'profile.dealer_id',
                            'quantity_total' => 'SUM(salePosition.quantity)',
                        ])
                        ->from([
                            'salePosition' => SalePosition::tableName(),
                            'sale' => Sale::tableName(),
                            'profile' => Profile::tableName(),
                        ])
                        ->andWhere([
                            'salePosition.product_id' => $product->id,
                        ])
                        ->andWhere('salePosition.sale_id = sale.id')
                        ->andWhere('sale.recipient_id = profile.id')
                        ->andFilterWhere(['between', 'DATE(sale.sold_on)', $this->sold_on_start, $this->sold_on_end])
                        ->groupBy([
                            'profile.dealer_id'
                        ])
                ], "{$alias}.dealer_id = dealer.id");

                $query->addSelect([
                    "{$alias}__quantity_total" => "{$alias}.quantity_total"
                ]);
                $totalQuantityFields[] = "{$alias}.quantity_total";
            }

            $query->addSelect([
                'quantity_total' => new Expression(implode('+', array_map(function ($field) {
                    return "IF({$field} IS NULL, 0, {$field})";
                }, $totalQuantityFields))),
            ]);
        }

        return $query;
    }

    /**
     * Returns title of the report
     * @return string
     */
    public function title()
    {
        return 'Продажи по дилерам';
    }

    /**
     * @return array
     */
    public function gridColumns()
    {
        $columns = [
            [
                'attribute' => 'sold_on_range',
                'label' => 'Дата',
                'filter' => DateRangeFilter::instance([
                    'attribute' => 'sold_on',
                ]),
                'contentOptions' => ['style' => 'width:200px;'],
            ],
            [
                'attribute' => 'dealer__name',
                'label' => 'Дилер',
                'filter' => ActiveTextInputFilter::instance([
                    'attribute' => 'name'
                ]),
            ],
            [
                'attribute' => 'quantity_total',
                'label' => 'Общая сумма',
                'value' => function ($data) {
                    return $data['quantity_total'] ?: '';
                }
            ],
        ];

        /** @var Product[] $products */
        $products = Product::find()->all();

        foreach ($products as $product) {
            $alias = 'salePosition_' . $product->id;
            $field = "{$alias}__quantity_total";
            $columns[] = [
                'attribute' => $field,
                'label' => $product->name,
                'value' => function ($data) use ($field) {
                    return $data[$field] ?: '';
                }
            ];
        }


        return $columns;
    }

    private $_joiningFields;

    protected function getJoiningFields()
    {
        if ($this->_joiningFields === null) {
            $this->_joiningFields = [];
            /** @var Product[] $products */
            $products = Product::find()->all();

            foreach ($products as $product) {
                $this->_joiningFields[] = 'salePosition_' . $product->id . '__quantity_total';
            }
        }
        return $this->_joiningFields;
    }
}