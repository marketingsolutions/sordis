<?php

return [
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'components' => [
        'request' => [
            'cookieValidationKey' => getenv('BACKEND_COOKIE_VALIDATION_KEY'),
        ],
        'session' => [
            'name' => 'MSSESSIDBACK',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'admin/main/index',
                'login' => 'admin/main/login',
                'logout' => 'admin/main/logout',
                'profile' => 'admin/profile/index',
            ],
        ],
        'user' => [
            'identityClass' => '\yz\admin\models\User',
            'enableAutoLogin' => false,
            'loginUrl' => ['admin/main/login'],
            'on afterLogin' => ['\yz\admin\models\User', 'onAfterLoginHandler'],
        ],
        'authManager' => [
            'class' => \yz\admin\components\AuthManager::class,
        ],
        'errorHandler' => [
            'errorAction' => 'admin/main/error',
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@vendor/marketingsolutions/loyalty-feedback' => '@app/themes/loyalty-feedback'
                ]
            ]
        ]

    ],
    'modules' => [
        'admin' => [
            'class' => \yz\admin\Module::class,
            'allowLoginViaToken' => true,
        ],
        'profiles' => [
            'class' => \modules\profiles\backend\Module::class,
        ],
        'bonuses' => [
            'class' => \modules\bonuses\backend\Module::class,
        ],
        'filemanager' => [
            'class' => \yz\admin\elfinder\Module::class,
            'roots' => [
                [
                    'baseUrl' => '@frontendWeb',
                    'basePath' => '@frontendWebroot',
                    'path' => 'media/uploads',
                    'name' => 'Файлы на сайте',
                ]
            ]
        ],
        'mailing' => [
            'class' => \yz\admin\mailer\backend\Module::class,
        ],
        'identity' => [
            'class' => ms\loyalty\identity\phonesEmails\backend\Module::class,
        ],
        'sales' => [
            'class' => modules\sales\backend\Module::class,
        ],
        'codes' => [
            'class' => \ms\loyalty\bonuses\codes\backend\Module::class,
        ],
        'payments' => [
            'class' => \ms\loyalty\prizes\payments\backend\Module::class,
        ],
        'catalog' => [
            'class' => \ms\loyalty\catalog\backend\Module::class,
        ],
//        'rsbcards' => [
//            'class' => \ms\loyalty\rsb\cards\backend\Module::class,
//        ],
        'feedback' => [
            'class' => \modules\feedback\backend\Module::class,
        ],
        'manual' => [
            'class' => \modules\manual\backend\Module::class,
        ],
        'finances' => [
            'class' => \ms\loyalty\finances\backend\Module::class,
        ],
        'courses' => [
            'class' => \modules\courses\backend\Module::class,
        ],
        'polls' => [
            'class' => \modules\polls\backend\Module::class,
        ],
        'social' => [
            'class' => modules\social\backend\Module::class,
        ],
        'calc' => [
            'class' => modules\calc\backend\Module::class,
        ],
        'taxes' => [
            'class' => \ms\loyalty\taxes\backend\Module::class,
        ],
        'logs' => [
            'class' => modules\logs\backend\Module::class,
        ],
        'reports' => [
            'class' => \ms\loyalty\reports\backend\Module::class,
            'reports' => [
                \modules\profiles\backend\reports\ProfilesStat::class,
                \ms\loyalty\identity\phonesEmails\backend\reports\IdentitiesRegistrations::class,
                \ms\loyalty\feedback\backend\reports\FeedbackStat::class,
                \backend\reports\SalesByDealerReport::class,
                \modules\profiles\backend\reports\NDFLReport::class,
            ]
        ],
        'api' => [
            'class' => \ms\loyalty\api\backend\Module::class,
        ],
        'checker' => [
            'class' => \ms\loyalty\checker\backend\Module::class,
        ],
        'mobile' => [
            'class' => \modules\mobile\backend\Module::class,
        ],
        'news' => [
            'class' => \modules\news\backend\Module::class,
        ],
        'files-attachments' => [
            'class' => \ms\files\attachments\backend\Module::class,
        ],
        'sms' => [
            'class' => \modules\sms\backend\Module::class,
        ],
        'pictures' => [
            'class' => \modules\pictures\backend\Module::class,
        ],
        'rpc' => [
            'class' => \modules\rpc\backend\Module::class,
        ],
    ],
    'params' => [

    ],
];


