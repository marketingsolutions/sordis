<?php

use yii\base\Event;
use yz\admin\grid\GridView;

$listener = new \marketingsolutions\events\Listener(
    new \marketingsolutions\events\PatternEventsProvider(),
    new \marketingsolutions\events\PrefixMethodFinder()
);

// LOYALTY_GENERATOR_MARK BOOTSTRAP

$listener->bind(
    \ms\loyalty\prizes\payments\backend\controllers\PaymentsController::class,
    \backend\listeners\PaymentsControllerListener::class
);

\yii\base\Event::on(
	\ms\loyalty\finances\backend\controllers\TransactionsController::class,
	\ms\loyalty\finances\backend\controllers\TransactionsController::EVENT_BEFORE_ACTION,
	function (Event $event) {
		Event::on(GridView::class, GridView::EVENT_SETUP_GRID, function (Event $event) {
			$grid = $event->sender;
			$grid->columns = array_merge($grid->columns, [
				['attribute' => 'created_at', 'value' => function ($m) {
					return (new \DateTime($m->created_at))->format('d.m.Y');
				}]
			]);
		});
	}
);

\yii\base\Event::on(
	\ms\loyalty\prizes\payments\backend\controllers\PaymentsController::class,
	\ms\loyalty\prizes\payments\backend\controllers\PaymentsController::EVENT_BEFORE_ACTION,
	function (Event $event) {
		Event::on(GridView::class, GridView::EVENT_SETUP_GRID, function (Event $event) {
			$grid = $event->sender;
			$grid->columns = array_merge($grid->columns, [
				['attribute' => 'created_at', 'value' => function ($m) {
					return (new \DateTime($m->created_at))->format('d.m.Y');
				}]
			]);
		});
	}
);

\yii\base\Event::on(
	\ms\loyalty\catalog\backend\controllers\CatalogOrdersController::class,
	\ms\loyalty\catalog\backend\controllers\CatalogOrdersController::EVENT_BEFORE_ACTION,
	function (Event $event) {
		Event::on(GridView::class, GridView::EVENT_SETUP_GRID, function (Event $event) {
			$grid = $event->sender;
			$grid->columns = array_merge($grid->columns, [
				['attribute' => 'created_at', 'value' => function ($m) {
					return (new \DateTime($m->created_at))->format('d.m.Y');
				}]
			]);
		});
	}
);

\yii\base\Event::on(
	\ms\loyalty\catalog\backend\controllers\OrderedCardsController::class,
	\ms\loyalty\catalog\backend\controllers\OrderedCardsController::EVENT_BEFORE_ACTION,
	function (Event $event) {
		Event::on(GridView::class, GridView::EVENT_SETUP_GRID, function (Event $event) {
			$grid = $event->sender;
			$grid->columns = array_merge($grid->columns, [
				['attribute' => 'created_at', 'value' => function ($m) {
					return (new \DateTime($m->created_at))->format('d.m.Y');
				}]
			]);
		});
	}
);

\yii\base\Event::on(
	\ms\loyalty\catalog\backend\controllers\CardItemsController::class,
	\ms\loyalty\catalog\backend\controllers\CardItemsController::EVENT_BEFORE_ACTION,
	function (Event $event) {
		Event::on(GridView::class, GridView::EVENT_SETUP_GRID, function (Event $event) {
			$grid = $event->sender;
			$grid->columns = array_merge($grid->columns, [
				['attribute' => 'created_at', 'value' => function ($m) {
					return (new \DateTime($m->created_at))->format('d.m.Y');
				}]
			]);
		});
	}
);