<?php

return [
    'id' => 'yz2-app-standard',
    'language' => 'ru',
    'sourceLanguage' => 'en-US',
    'extensions' => require(YZ_VENDOR_DIR . '/yiisoft/extensions.php'),
    'vendorPath' => YZ_VENDOR_DIR,
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'bootstrap' => [
        'log',
    ],
    'components' => [
        'db' => [
            'class' => yii\db\Connection::class,
            'charset' => 'utf8',
            'dsn' => getenv('DB_DSN'),
            'username' => getenv('DB_USER'),
            'password' => getenv('DB_PASSWORD'),
            'tablePrefix' => getenv('DB_TABLE_PREFIX'),
            'attributes' => [
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));",
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\DummyCache',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'timeZone' => 'Europe/Moscow',
            'dateFormat' => 'dd.MM.yyyy',
            'timeFormat' => 'HH:mm:ss',
            'datetimeFormat' => 'dd.MM.yyyy HH:mm',
        ],
        'i18n' => [
            'translations' => [
                'loyalty' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'ru-RU',
                ],
                'common' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'en-US',
                ],
                'frontend' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@frontend/messages',
                    'sourceLanguage' => 'en-US',
                ],
                'backend' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@backend/messages',
                    'sourceLanguage' => 'en-US',
                ],
                'console' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@console/messages',
                    'sourceLanguage' => 'en-US',
                ],
            ]
        ],
        'taxesManager' => [
            'class' => \ms\loyalty\taxes\common\components\TaxesManager::class,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => yii\log\FileTarget::class,
                    'levels' => ['error', 'warning', 'trace'],
                ],
            ],
        ],
        'mailer' => [
            'class' => \yii\swiftmailer\Mailer::class,
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'messageConfig' => [
                'from' => ['kopilka.help@yandex.ru' => 'Kopilka'],
            ],
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'kopilka.help@yandex.ru',
                'password' => 'eqhlzkeroxlklada',
                'port' => '465',
                'encryption' => 'SSL',
            ],
        ],
        'sms' => [
            'class' => \marketingsolutions\sms\Sms::class,
            'services' => [
                'service' => [
                    'class' => \marketingsolutions\sms\services\Smsc::class,
                    'login' => getenv('SMSC_LOGIN'),
                    'password' => getenv('SMSC_PASSWORD'),
                    'from' => getenv('SMS_FROM'),
                ]
            ]
        ],
        'financeChecker' => [
            'class' => \ms\loyalty\finances\common\components\FinanceChecker::class,
            'moneyDifferenceThreshold' => 50000,
            'emailNotificationThreshold' => 100000,
            'email' => 'alex@zakazpodarka.ru',
        ],
        'zp1c' => [
            'class' => \ms\zp1c\client\Client::class,
            'url' => getenv('ZP_1C_HOST'),
            'login' => getenv('ZP_1C_LOGIN'),
            'password' => getenv('ZP_1C_PASSWORD'),
        ],
    ],
    'modules' => [
        'pdfjs' => [
            'class' => '\yii2assets\pdfjs\Module',
        ],
        'profiles' => [
            'class' => \modules\profiles\common\Module::class,
            'enableFreeRegistration' => true,
        ],
        'sales' => [
            'class' => \modules\sales\common\Module::class,
        ],
        'bonuses' => [
            'class' => \modules\bonuses\common\Module::class,
        ],
        'courses' => [
            'class' => \modules\courses\common\Module::class,
        ],
        'feedback' => [
            'class' => modules\feedback\common\Module::class,
        ],
        'catalog' => [
            'class' => ms\loyalty\catalog\common\Module::class,
            'classMap' => [
                'prizeRecipient' => \modules\profiles\common\models\Profile::class,
            ],
            'loyaltyName' => getenv('LOYALTY_1C_NAME'),
            'disableOrderingForNoTaxAccount' => false,
        ],
        'payments' => [
            'class' => ms\loyalty\prizes\payments\common\Module::class,
            'loyaltyName' => getenv('LOYALTY_1C_NAME'),
            'disablePaymentsForNoTaxAccounts' => false,
        ],
//        'rsbcards' => [
//            'class' => \ms\loyalty\rsb\cards\common\Module::class,
//            'loyaltyName' => getenv('LOYALTY_1C_NAME'),
//        ],
        'codes' => [
            'class' => \ms\loyalty\bonuses\codes\common\Module::class,
            'showActivatedCodes' => true,
            'enableBonusesPaymentByList' => false,
            'enableBonusesPaymentAtActivation' => true,
        ],
        'files-attachments' => [
            'class' => \ms\files\attachments\common\Module::class,
        ],
        'like' => [
            'class' => halumein\like\Module::class,
        ],
        'social' => [
            'class' => modules\social\common\Module::class,
        ],
        'taxes' => [
            'class' => \ms\loyalty\taxes\common\Module::class,
            'incomeTaxPaymentMethod' => \ms\loyalty\taxes\common\Module::INCOME_TAX_PAYMENT_METHOD_NOBODY,
            'documentImageUploadRequired' => false,
            'accountProfileValidationRequired' => false,
            //'innIsRequired' => false,
        ],
        'mailing' => [
            'class' => \yz\admin\mailer\common\Module::class,
            'mailLists' => [
                \modules\profiles\common\mailing\ProfileMailingList::class,
            ]
        ],
        'demo-link' => [
            'class' => \ms\loyalty\demo\link\common\Module::class,
            'enabled' => getenv('DEMO_LINK_ENABLED') == 'true',
            'accessToken' => getenv('DEMO_LINK_ACCESS_TOKEN'),
        ],
        'api' => [
            'class' => \ms\loyalty\api\common\Module::class,
            'authType' => \ms\loyalty\api\common\Module::AUTH_TOKEN,
            'hashSecret' => getenv('API_HASH_SECRET'),
            'tokenLiveMinutes' => getenv('API_TOKEN_LIVE_MINUTES'),
        ],
        'checker' => [
            'class' => \ms\loyalty\checker\common\Module::class,
            'emails' => 'd.trofimov@msforyou.ru'
        ],
        'mobile' => [
            'class' => \modules\mobile\common\Module::class,
        ],
        'calc' => [
            'class' => \modules\calc\common\Module::class,
        ],
        'ftp' => [
            'class' => \modules\ftp\common\Module::class,
        ],
        'news' => [
            'class' => \modules\news\common\Module::class,
        ],
        'polls' => [
            'class' => \modules\polls\common\Module::class,
        ],
        'logs' => [
            'class' => \modules\logs\common\Module::class,
        ],
        'pictures' => [
            'class' => \modules\pictures\common\Module::class,
        ],
        'rpc' => [
            'class' => \modules\rpc\common\Module::class,
        ],
    ],
    'params' => [
    ],
];
