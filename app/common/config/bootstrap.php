<?php

/**
 * Aliases configuration
 *
 * All aliases that are used in application are placed here
 */

Yii::setAlias('base', YZ_BASE_DIR);

// Applications
Yii::setAlias('common', YZ_APP_DIR . '/common');
Yii::setAlias('frontend', YZ_APP_DIR . '/frontend');
Yii::setAlias('backend', YZ_APP_DIR . '/backend');
Yii::setAlias('console', YZ_APP_DIR . '/console');

// Modules
Yii::setAlias('modules', YZ_APP_DIR . '/modules');

// Migrations
Yii::setAlias('migrations', YZ_APP_DIR . '/migrations');

// Data
Yii::setAlias('data',  YZ_BASE_DIR . '/data');

// Web
Yii::setAlias('frontendWebroot', '@frontend/web');
Yii::setAlias('backendWebroot', '@backend/web');
Yii::setAlias('frontendWeb', getenv('FRONTEND_WEB'));
Yii::setAlias('backendWeb', getenv('BACKEND_WEB'));

// App version
if (file_exists(YZ_BASE_DIR.'/.version')) {
    define('APP_VERSION', file_get_contents(YZ_BASE_DIR.'/.version'));
} else {
    define('APP_VERSION', 'latest');
}


/**
 * Dependency injections
 */


//Yii::$container->set('acme\DummyInterface', 'acme\DummyClass');

// Profile class information
Yii::$container->set(
    \ms\loyalty\contracts\profiles\ProfileFinderInterface::class,
    \modules\profiles\common\models\ProfileFinder::class
);

// Identity registrar
Yii::$container->set(
    \ms\loyalty\contracts\identities\IdentityRegistrarInterface::class,
    \ms\loyalty\identity\phonesEmails\common\models\IdentityRegistrar::class
);

// Registration token provider
Yii::$container->set(
    \ms\loyalty\contracts\identities\RegistrationTokenProviderInterface::class,
    \ms\loyalty\identity\phonesEmails\common\registration\RegistrationTokenProvider::class
);


/**
 * Event handlers
 */

$listener = new \marketingsolutions\events\Listener(
    new \marketingsolutions\events\PatternEventsProvider(),
    new \marketingsolutions\events\PrefixMethodFinder()
);

//\yii\base\Event::on(SomeClass::class, 'onEvent', [SomeListener::class, 'whenEvent']);

// LOYALTY_GENERATOR_MARK BOOTSTRAP

/**
 * Token generation form
 */
$listener->bind(
    \ms\loyalty\identity\phonesEmails\common\forms\TokenGenerationForm::class,
    \common\listeners\TokenGenerationFormListener::class
);