<?php
namespace common\utils;

use yii2mod\ftp\FtpClient;

class Ftp
{
    /**
     * FTP коннект для обмена файлами
     * @param $host
     * @param $login
     * @param $password
     * @return FtpClient
     */
    public static function FtpConnect($host, $login, $password){
        $ftp = new FtpClient();
        $ftp->connect($host);
        $ftp->login($login, $password);
        return $ftp;
    }
}
