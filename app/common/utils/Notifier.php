<?php

namespace common\utils;

use modules\mobile\common\models\Gcm;
use modules\news\common\models\News;
use modules\sales\common\models\Sale;
use yz\admin\models\User;

class Notifier
{
    public static function salePaid(Sale $model)
    {
        Gcm::sendToProfile(
            'Продажа подтверждена',
            "Продажа #{$model->id} подтверждена. Вам начислено {$model->bonuses} баллов",
            $model->recipient_id,
            Gcm::MENU_SALES
        );
    }

    public static function newsPush(News $model)
    {
        Gcm::sendToAll(
            'Добавлена новость',
            $model->title,
            Gcm::MENU_NEWS
        );
    }


    public static function isSuperAdmin()
    {
        return \Yii::$app->user->identity->is_super_admin;
    }

    public static function isAdmin(User $admin = null)
    {
        return self::checkRole($admin, 'ADMIN');
    }

    public static function isRegional(User $admin = null)
    {
        return self::checkRole($admin, 'REGION_MANAGER');
    }

    public static function checkRole($admin, $roleName)
    {
        /** @var User $admin */
        if ($admin === null) {
            $admin = \Yii::$app->user->identity;
        }

        if (!$admin) {
            return false;
        }

        foreach ($admin->roles as $role) {
            if ($role->name == $roleName) {
                return true;
            }
        }

        return false;
    }
}