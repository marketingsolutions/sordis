<?php
namespace common\utils;

use marketingsolutions\finance\models\Purse;

class Checker
{
	public static function companyBalance()
	{
		/** @var Purse $purse */
		$purse = Purse::findOne(['owner_id' => null]);

		return $purse->balance / 100;
	}
}