<?php

namespace common\listeners;

use modules\profiles\common\models\Profile;
use ms\loyalty\identity\phonesEmails\common\forms\TokenGenerationForm;
use ms\loyalty\identity\phonesEmails\common\models\IdentityType;
use yii\base\Event;


/**
 * Class TokenGenerationFormListener
 */
class TokenGenerationFormListener
{
    public static function whenAfterValidate(Event $event)
    {
        /** @var TokenGenerationForm $sender */
        $sender = $event->sender;

        if ($sender->type == IdentityType::PHONE) {
            $profile = Profile::find()
                ->andWhere(['phone_mobile' => $sender->phoneMobile])
                ->one();

            if ($profile !== null) {
                $sender->addError('phoneMobileLocal', 'Участник с данным номером телефона уже существует в системе');
            }
        } else {
            $profile = Profile::find()
                ->andWhere(['email' => $sender->email])
                ->one();

            if ($profile !== null) {
                $sender->addError('email', 'Участник с данным адресом электронной почты уже зарегистрирован');
            }
        }
    }
}