
VAGRANTFILE_API_VERSION = "2"
Vagrant.require_version ">= 1.7.0"

require 'yaml'
options = YAML.load_file File.join(File.dirname(__FILE__), 'vagrant.yaml')

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

   config.vm.box = "ubuntu/trusty64"
   config.ssh.insert_key = false

   # Change ip of the machine to make it available via xip.io
   config.vm.network "private_network", ip: options['vm']['ip'], netmask: "255.255.255.0"

   # Use nfs to speed up
   config.vm.synced_folder '.', '/vagrant', nfs: true


   config.vm.provider :virtualbox do |virtualbox|
       virtualbox.customize ["modifyvm", :id, "--cableconnected1", "on"]
       virtualbox.customize ["modifyvm", :id, "--name", options['vm']['name']]
       virtualbox.customize ["modifyvm", :id, "--nicpromisc2", "allow-all"]
       virtualbox.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
       virtualbox.customize ["modifyvm", :id, "--memory", options['vm']['memory']]
       virtualbox.customize ["setextradata", :id, "--VBoxInternal2/SharedFoldersEnableSymlinksCreate/v-root", "1"]
   end

   config.vm.provision :shell, :path => "vm/bootstrap.sh", args: [
      options['vm']['ip'],
       options['ports']['backend']
   ]
end
