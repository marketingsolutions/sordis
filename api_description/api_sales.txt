ВСЕ запросы отправляются методом POST с заголовками:
Content-Type: application/json
X-Token: b-hiDb8xWIv1WN4yiy5sWy5Fke33gOS4Oph-M1Yz43Kqv5DlJdbeis8tI-n1tqaT

-----------------------------------------------------------------------------------------------------------------

Получение списка всех продаж участника, запрос: /sales/api/sale/list
{"profile_id": 516}

Успешно код 200:
{"result":"OK","sales":[{"id":2,"recipient_id":516,"status":"adminReview","bonuses":1000,"review_comment":null,"sold_on":"13.10.2017","approved_by_admin_at":null,"bonuses_paid_at":null,"created_at":"13.10.2017","positions":[{"id":2,"product_id":1288,"quantity":1,"quantityLocal":1,"bonuses":500,"serial_number_id":null,"validation_method":"serial","custom_serial":"BHNA02157","product":{"id":1288,"category_id":1,"name":"M.ZUIKO DIGITAL ED 30mm 1:3.5 Macro","unit_id":1,"bonuses_formula":"500*q","dealer_name":"Mont","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},"serialNumber":null},{"id":3,"product_id":1289,"quantity":1,"quantityLocal":1,"bonuses":500,"serial_number_id":null,"validation_method":"serial","custom_serial":"ACA201672","product":{"id":1289,"category_id":1,"name":"TG-Tracker","unit_id":1,"bonuses_formula":"500*q","dealer_name":"Mont","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},"serialNumber":null}],"documents":[{"id":3,"original_name":"tovanye-i-kassovie-cheki-5.jpg","name":"3.jpg","file_size":204690,"image_url":"http://sistemapromo/assets/a473e165486bed9ba725cb8b845a82d3/3.jpg"}]},{"id":1,"recipient_id":516,"status":"adminReview","bonuses":500,"review_comment":null,"sold_on":"13.10.2017","approved_by_admin_at":null,"bonuses_paid_at":null,"created_at":"13.10.2017","positions":[{"id":1,"product_id":1288,"quantity":1,"quantityLocal":1,"bonuses":500,"serial_number_id":null,"validation_method":"serial","custom_serial":"123","product":{"id":1288,"category_id":1,"name":"M.ZUIKO DIGITAL ED 30mm 1:3.5 Macro","unit_id":1,"bonuses_formula":"500*q","dealer_name":"Mont","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},"serialNumber":null}],"documents":[{"id":1,"original_name":"IQ allround A4.jpg","name":"1.jpg","file_size":49078,"image_url":"http://sistemapromo/assets/a473e165486bed9ba725cb8b845a82d3/1.jpg"}]}]}

Ошибка код 400:
{"result": "FAIL","error": "Не найден участник с ID 5081"}

-----------------------------------------------------------------------------------------------------------------

Получение конкретной продажи, запрос: /sales/api/sale/view
{"profile_id": 516, "sale_id": 123}

Успешно код 200:
{"result":"OK","sale":{"id":2,"recipient_id":516,"status":"adminReview","bonuses":1000,"review_comment":null,"sold_on":"13.10.2017","approved_by_admin_at":null,"bonuses_paid_at":null,"created_at":"13.10.2017","positions":[{"id":2,"product_id":1288,"quantity":1,"quantityLocal":1,"bonuses":500,"serial_number_id":null,"validation_method":"serial","custom_serial":"BHNA02157","product":{"id":1288,"category_id":1,"name":"M.ZUIKO DIGITAL ED 30mm 1:3.5 Macro","unit_id":1,"bonuses_formula":"500*q","dealer_name":"Mont","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},"serialNumber":null},{"id":3,"product_id":1289,"quantity":1,"quantityLocal":1,"bonuses":500,"serial_number_id":null,"validation_method":"serial","custom_serial":"ACA201672","product":{"id":1289,"category_id":1,"name":"TG-Tracker","unit_id":1,"bonuses_formula":"500*q","dealer_name":"Mont","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},"serialNumber":null}],"documents":[{"id":3,"original_name":"tovanye-i-kassovie-cheki-5.jpg","name":"3.jpg","file_size":204690,"image_url":"http://sistemapromo/assets/a473e165486bed9ba725cb8b845a82d3/3.jpg"}]}}

Ошибка код 400:
{"result": "FAIL","error": "Не найден участник с ID 5161"}
{"result": "FAIL","error": "У участника с ID 516 не найдена продажа с ID 12"}

-----------------------------------------------------------------------------------------------------------------

Добавление продажи, запрос: /sales/api/sale/create
{
	"profile_id": 516,
    "sale": {
      	"status": "adminReview",
      	"sold_on_local": "15.10.2017",
    	"positions": [{
			"product_id": 1378,
			"quantityLocal": 1,
			"serialNumberValue": "33333",
          	"validation_method": "serial"
        }]
    },
    "documents": ["59e4bf61eaaa7.gif","59e49efb79f3a.gif"]
}

Успешно код 200:
{"result":"OK","sale":{"id":32,"recipient_id":516,"status":"adminReview","bonuses":1000,"review_comment":null,"sold_on":"15.10.2017","approved_by_admin_at":null,"bonuses_paid_at":null,"created_at":"16.10.2017","positions":[{"id":35,"product_id":1378,"quantity":1,"quantityLocal":1,"bonuses":1000,"serial_number_id":null,"validation_method":"serial","custom_serial":"33333","product":{"id":1378,"category_id":1,"name":"M.ZUIKO DIGITAL ED 7-14mm 1:2.8 PRO","unit_id":1,"bonuses_formula":"1000*q","dealer_name":"Soft-Tronik","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},"serialNumber":null}],"documents":[{"id":20,"original_name":"59e4bf61eaaa7.gif","name":"32_59e4bf61eaaa7.gif","file_size":1951,"image_url":"http://sistemapromo/data/sales/documents/32_59e4bf61eaaa7.gif"},{"id":21,"original_name":"59e49efb79f3a.gif","name":"32_59e49efb79f3a.gif","file_size":1951,"image_url":"http://sistemapromo/data/sales/documents/32_59e49efb79f3a.gif"}]}}

Ошибка код 400:
{"result": "FAIL","error": "Не найден участник с ID 32"}
{"result": "FAIL","errors": {"status": "Необходимо заполнить «Статус»."}}
{"result": "FAIL","errors": {"points": "Вы должны добавить хотя бы одну позицию"}}
{"result": "FAIL","errors": {"product_id": "Не выбран товар"}}
{"result": "FAIL","errors": {"validation_method": "Необходимо выбрать способ подтверждения","serialNumberValue": "Необходимо подтвердить продажу с помощью либо серийного номера, либо - подтверждающих документов"}}
{"result": "FAIL","error": "Необходимо прикрепить подтверждающие документы"}
{"result": "FAIL","error": "Не найден временный файл 59e4bf61eaaa11117.gif"}
{"result": "FAIL","error": "Не удалось скопировать временный файл 59e4bf61eaaa11117.gif"}

-----------------------------------------------------------------------------------------------------------------

Обновление продажи, запрос: /sales/api/sale/update
{
	"sale_id": 33,
    "sale": {
      	"status": "adminReview",
      	"sold_on_local": "14.10.2017",
    	"positions": [{
			"product_id": 1395,
			"quantityLocal": 1,
			"serialNumberValue": "111111",
          	"validation_method": "serial"
        }]
    },
    "documents": ["59e4c7b8922e5.gif"]
}

Успешно код 200:
{"result":"OK","sale":{"id":32,"recipient_id":516,"status":"adminReview","bonuses":1000,"review_comment":null,"sold_on":"15.10.2017","approved_by_admin_at":null,"bonuses_paid_at":null,"created_at":"16.10.2017","positions":[{"id":35,"product_id":1378,"quantity":1,"quantityLocal":1,"bonuses":1000,"serial_number_id":null,"validation_method":"serial","custom_serial":"33333","product":{"id":1378,"category_id":1,"name":"M.ZUIKO DIGITAL ED 7-14mm 1:2.8 PRO","unit_id":1,"bonuses_formula":"1000*q","dealer_name":"Soft-Tronik","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},"serialNumber":null}],"documents":[{"id":20,"original_name":"59e4bf61eaaa7.gif","name":"32_59e4bf61eaaa7.gif","file_size":1951,"image_url":"http://sistemapromo/data/sales/documents/32_59e4bf61eaaa7.gif"},{"id":21,"original_name":"59e49efb79f3a.gif","name":"32_59e49efb79f3a.gif","file_size":1951,"image_url":"http://sistemapromo/data/sales/documents/32_59e49efb79f3a.gif"}]}}

Ошибка код 400:
{"result": "FAIL","error": "Не найдена продажа с ID 516"}
{"result": "FAIL","errors": {"points": "Вы должны добавить хотя бы одну позицию"}}

{"result": "FAIL","error": "Не найден временный файл 59e4bf61eaaa11117.gif"}
{"result": "FAIL","error": "Не удалось скопировать временный файл 59e4bf61eaaa11117.gif"}

-----------------------------------------------------------------------------------------------------------------

Загрузка подтверждающего документа, запрос: /sales/api/document/upload
{
  "type": "jpg",
  "image" : "R0lGODlhPQBEAPeoAJosM..."
}

Успешно код 200:
{"result": "OK", "filename": "59e4bf61eaaa7.gif", "webpath": "http://sistemapromo/data/temp/59e4bf61eaaa7.gif"}

Ошибка код 400:
{"result": "FAIL","errors": {"type": "Необходимо заполнить «Расширение картинки»."}}
{"result": "FAIL","errors": {"image": "Необходимо заполнить «Изображение в Base64»."}}
{"result": "FAIL","errors": {"image": "Ошибка при загрузке файла, неверное значение size"}}

-----------------------------------------------------------------------------------------------------------------

Удаление подтверждающего документа, запрос: /sales/api/document/delete
{"document_id": 38}

Успешно код 200:
{"result": "OK","rowCount": 1}

Ошибка код 400:
{"result": "FAIL","error": "Не найден подтверждающий документ с ID 381"}

-----------------------------------------------------------------------------------------------------------------

Получение списка товаров, запрос: /sales/api/product/list
{"dealer_name": "Mont", "category_id": 1} - dealer_name и category_id необязательный параметр

Успешно код 200:
{"result":"OK","products":[{"id":1257,"category_id":1,"name":"E-M1 Mark II BODY","unit_id":1,"bonuses_formula":"4000*q","dealer_name":"Mont","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},{"id":1258,"category_id":1,"name":"E-M1 Mark II Kit 12-40mm","unit_id":1,"bonuses_formula":"6000*q","dealer_name":"Mont","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},{"id":1288,"category_id":1,"name":"M.ZUIKO DIGITAL ED 30mm 1:3.5 Macro","unit_id":1,"bonuses_formula":"500*q","dealer_name":"Mont","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},{"id":1289,"category_id":1,"name":"TG-Tracker","unit_id":1,"bonuses_formula":"500*q","dealer_name":"Mont","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},{"id":1359,"category_id":1,"name":"E-M1 Kit 12-50mm","unit_id":1,"bonuses_formula":"2000*q","dealer_name":"Soft-Tronik","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},{"id":1378,"category_id":1,"name":"M.ZUIKO DIGITAL ED 7-14mm 1:2.8 PRO","unit_id":1,"bonuses_formula":"1000*q","dealer_name":"Soft-Tronik","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},{"id":1395,"category_id":1,"name":"E-M5 Mark II Kit 12-40mm","unit_id":1,"bonuses_formula":"2000*q","dealer_name":"Treolan","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},{"id":1419,"category_id":1,"name":"M.ZUIKO DIGITAL ED 60mm 1:2.8 Macro","unit_id":1,"bonuses_formula":"500*q","dealer_name":"Treolan","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},{"id":1588,"category_id":1,"name":"E-M1 Mark II Kit 12-40mm","unit_id":1,"bonuses_formula":"6000*q","dealer_name":"OCS","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},{"id":1778,"category_id":1,"name":"M.ZUIKO DIGITAL ED 12mm 1:2.0","unit_id":1,"bonuses_formula":"550*q","dealer_name":"Treolan","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},{"id":2009,"category_id":1,"name":"E-M1 BODY","unit_id":1,"bonuses_formula":"2000*q","dealer_name":"OCS","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},{"id":2010,"category_id":1,"name":"E-M1 Kit 12-50mm","unit_id":1,"bonuses_formula":"2000*q","dealer_name":"OCS","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},{"id":2043,"category_id":1,"name":"E-M1 Kit 12-50mm","unit_id":1,"bonuses_formula":"2000*q","dealer_name":"А1 ТИС","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},{"id":2044,"category_id":1,"name":"E-M1 Kit 12-40mm","unit_id":1,"bonuses_formula":"3000*q","dealer_name":"А1 ТИС","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},{"id":2286,"category_id":1,"name":"PEN-F Kit 17mm 1:1.8","unit_id":1,"bonuses_formula":"3000*q","dealer_name":"Soft-Tronik","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},{"id":2311,"category_id":1,"name":"E-M5 Mark II Kit 12-40mm","unit_id":1,"bonuses_formula":"2000*q","dealer_name":"Soft-Tronik","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}},{"id":2361,"category_id":1,"name":"M.ZUIKO DIGITAL ED 40-150mm 1:2.8 PRO","unit_id":1,"bonuses_formula":"1500*q","dealer_name":"OCS","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}}]}

-----------------------------------------------------------------------------------------------------------------

Получение конкретного товара, запрос: /sales/api/product/view
{"product_id": 1}

Успешно код 200:
{"result":"OK","product":{"id":1289,"category_id":1,"name":"TG-Tracker","unit_id":1,"bonuses_formula":"500*q","dealer_name":"Mont","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"},"unit":{"id":1,"name":"Штуки","short_name":"шт.","created_at":"13.10.2017","updated_at":"13.10.2017","quantity_divider":1}}}

Ошибка код 400:
{"result": "FAIL","error": "Не найден товар с ID 1"}

-----------------------------------------------------------------------------------------------------------------

Получение списка категорий, запрос: /sales/api/category/list

Успешно код 200:
{"result":"OK","categories":[{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"}]}

-----------------------------------------------------------------------------------------------------------------

Получение конкретной категории, запрос: /sales/api/category/view
{"category_id": 1}

Успешно код 200:
{"result":"OK","category":{"id":1,"name":"Фототехника","created_at":"13.10.2017","updated_at":"13.10.2017"}}

Ошибка код 400:
{"result": "FAIL","error": "Не найдена категория с ID 33"}

-----------------------------------------------------------------------------------------------------------------