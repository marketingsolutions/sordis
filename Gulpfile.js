var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var less = require('gulp-less');

gulp.task('watch', function () {
    gulp.watch('app/frontend/web/less/*.less', ['less-main']);
});

gulp.task ('less-main', function() {
    return gulp.src(['app/frontend/web/less/main.less'])
        .pipe(less())
        .pipe(autoprefixer({browsers: ['last 2 versions']}))
        .pipe(gulp.dest('app/frontend/web/css'));
});

gulp.task('build', ['less-main'], function() {
});

